const webpackConfig = require('./webpack.config');
webpackConfig.devtool = 'inline-source-map';
webpackConfig.entry = undefined;

module.exports = function (config) {
    config.set({
        browsers: ['jsdom'],
        files: [
            'tests.bundle.js',
        ],
        frameworks: ['chai', 'mocha'],
        plugins: [
            'karma-jsdom-launcher',
            'karma-chai-plugins',
            'karma-mocha',
            'karma-sourcemap-loader',
            'karma-webpack',
        ],
        preprocessors: {
            'tests.bundle.js': ['webpack', 'sourcemap'],
        },
        reporters: ['dots'],
        singleRun: true,
        webpack: webpackConfig,
        webpackMiddleware: {
            noInfo: true,
            quiet: true,
        },
    });
}
