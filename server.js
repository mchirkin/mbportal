const Hapi = require('hapi');
const path = require('path');
const crypto = require('crypto');
const chalk = require('chalk');
const CONFIG = require('./server/config/config.js');
const env = require('./server/env.json');

const server = new Hapi.Server();
server.connection({
    port: process.env.PORT || 3000,
});

/**
 * Проверка на валидность csrf токена
 * @param {string} token csrf токен для проверки
 * @return {boolean} валидность токена
 */
function checkCSRFToken(token) {
    const [salt, saltedSecret] = token.split(':');
    const serverSaltedSecret = crypto.createHash('md5').update(`${salt}:${env.csrfPassword}`).digest('hex');
    return serverSaltedSecret === saltedSecret;
}

const networkCache = require('./mb-network.json').log;

server.ext('onPreHandler', (request, reply) => {
    const user = request.yar.get('user');

    console.log(request);

    const method = request.method.toUpperCase();

    // заглушки
    if (/\/api\//.test(request.path)) {
        const cacheEntry = networkCache.entries.find(entry => {
            const { request: cacheRequest, response: cacheResponse } = entry;

            const requestPath = cacheRequest.url.replace('https://mb.rosevrobank.ru', '');

            if (cacheRequest.method === method && request.path === requestPath) {
                return true;
            }

            return false;
        });

        if (cacheEntry) {
            const cacheResponseContent = cacheEntry.response.content;
            if (cacheResponseContent) {
                return reply(cacheResponseContent.text || '').code(200).type(cacheResponseContent.mimeType);
            }
            return reply('').code(200);
        }

        return reply().code(400);
    }

    // Проверка csrf токена
    /*
    if (request.headers['x-csrf-token']) {
        const isTokenValid = checkCSRFToken(request.headers['x-csrf-token']);
        if (!isTokenValid) {
            request.yar.reset();
            return reply().code(401);
        }
    }
    */

    // Если пришел запрос к api без csrf token'a, то отправляем ошибку
    /*
    if (/\/api\//.test(request.path) && !request.headers['x-csrf-token']) {
        return reply().code(401);
    }
    */

    const ip = request.headers['x-forwarded-for']
        ? request.headers['x-forwarded-for'].split(',')[0]
        : request.info.remoteAddress;

    if (user) {
        /*
        if (/^\/login/.test(request.path)) {
            return reply.redirect('/main');
        }
        */
        const userAgent = request.plugins.scooter.toString();
        if (userAgent !== user.useragent) {
            request.yar.reset();
            return reply.redirect('/login');
        }

        if (ip !== user.ClientIP) {
            request.yar.reset();
            return reply().code(400);
        }

        // Если csrf токен не соответствует сессии, то отправляем ошибку
        if (
            !/login/.test(request.path) &&
            /\/api\//.test(request.path) &&
            !/captcha/.test(request.path) &&
            user.csrfToken !== request.headers['x-csrf-token']
        ) {
            return reply().code(400);
        }

        request.auth = {
            JSESSIONID: user.JSESSIONID,
            ClientIP: ip,
            login: user.login,
            userAgent: request.headers['user-agent'],
        };
    } else {
        request.ip = ip;
        request.userAgent = request.headers['user-agent'];
    }

    return reply.continue();
});

server.ext('onPostHandler', (request, reply) => {
    console.log(chalk.cyan('[Time]'), new Date().toLocaleString());
    console.log(chalk.green('[Request path]'), request.path);
    console.log(chalk.green('[Request method]'), request.method.toUpperCase());
    console.log(chalk.green('[Request info]'), request.info);
    console.log(chalk.green('[Request headers]'), request.headers);
    console.log(chalk.green('[Request payload]'), request.payload);
    console.log(chalk.yellow('[Response statusCode]'), request.response.statusCode);
    console.log(chalk.yellow('[Response headers]'), request.response.headers);
    console.log(chalk.yellow('[Response body]'), request.response.source);
    console.log(
        '\n',
        chalk.bgCyan('---------------------------------------------------------------------------'),
        '\n'
    );
    if (request.response.statusCode === 401) {
        // request.yar.reset();
    }
    if (request.response.headers) {
        request.response.headers['strict-transport-security'] = 'max-age=31536000; includeSubDomains';
    }
    return reply.continue();
});

server.register([
    {
        register: require('vision'),
    },
    {
        register: require('inert'),
    },
    {
        register: require('yar'),
        options: {
            storeBlank: false,
            cookieOptions: {
                password: CONFIG.session.passwordForCookie,
                isSecure: process.env.NODE_ENV === 'production',
                isHttpOnly: true,
            },
        },
    },
    {
        register: require('scooter'),
    },
    {
        register: require('h2o2'),
    },
    {
        register: require('./server/routes/auth.js'),
    },
    {
        register: require('./server/routes/static.js'),
    },
    {
        register: require('./server/routes/client.js'),
    },
    {
        register: require('./server/routes/products.js'),
    },
    {
        register: require('./server/routes/templates.js'),
    },
    {
        register: require('./server/routes/ministatement.js'),
    },
    {
        register: require('./server/routes/documents.js'),
    },
    {
        register: require('./server/routes/crypto.js'),
    },
    {
        register: require('./server/routes/statement.js'),
    },
    {
        register: require('./server/routes/msg.js'),
    },
    {
        register: require('./server/routes/payments.js'),
    },
    {
        register: require('./server/routes/contragents.js'),
    },
    {
        register: require('./server/routes/client_request.js'),
    },
    {
        register: require('./server/routes/cancel_documents.js'),
    },
    {
        register: require('./server/routes/client_info.js'),
    },
    {
        register: require('./server/routes/piwik.js'),
    },
    {
        register: require('./server/routes/captcha.js'),
    },
    {
        register: require('./server/routes/invoice.js'),
    },
    {
        register: require('./server/routes/dictionary.js'),
    },
    {
        register: require('./server/routes/tariff_requests.js'),
    },
    {
        register: require('./server/routes/dadata.js'),
    },
    {
        register: require('./server/routes/bfm.js'),
    },
    {
        register: require('./server/routes/user_notes.js'),
    },
], () => {
    server.views({
        engines: {
            html: require('handlebars'),
        },
        relativeTo: path.resolve(__dirname, 'server'),
        path: 'templates',
    });

    server.start((err) => {
        if (err) {
            throw err;
        }
        console.log('Server running at:', server.info.uri);
    });
});
