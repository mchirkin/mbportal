### Запуск проекта на локальной машине
Сборка и запуск
```
$ cd /path/to/project
$ npm install
$ webpack
$ node make_env.js
$ node server.js
```

Для локальной разработки рекомендуется использовать пакет nodemon  
Запуск проекта с помощью nodemon
```
$ npm run start:dev
```
### Используемые технологии
- React
- React-router
- Redux
- PostCSS
- Hapi
- Webpack

### HMR
Для запуска Hot Module Replacement необходимо запустить три процесса в терминале

1. Старт сервера
```
$ npm run start:dev:hmr
```
2. Старт сборки Webpack
```
$ npm run build:dev
```
3. Старт Webpack Dev Server
```
$ npm run build:hmr
```

### Prettier

Для работы с prettier в VS Code необходимо установить расширение prettier.
Для отработки prettier при сохранении файлов в параметры VS Code необходимо добавить
```json
"[javascriptreact]": {
    "editor.formatOnSave": true
},
"[javascript]": {
    "editor.formatOnSave": true
}
```

### Backend
Backend-системой для проекта выступает система ДБО iSimpleBank  
Обмен данными происходит через REST API в формате JSON  
Спецификация API iSimple https://wiki.isimplelab.com/pages/viewpage.action?pageId=7864808 (login: bank, pass: client)  
Адреса системы iSimple:
- Dev: 10.1.20.100, port: 8080
- Test: 10.1.16.100, port: 8080
- Prod: 10.1.2.181, port: 8080

### CI
Описание CI находится в Wiki http://vbrepo.main.roseurobank.ru/mchirkin/mbportal/wikis/home
