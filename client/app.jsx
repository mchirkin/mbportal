import React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import App from 'containers/App';

const renderApp = Component => {
    render(
        <AppContainer>
            <Component />
        </AppContainer>,
        document.getElementById('app')
    );
};

if (module.hot) {
    renderApp(App);

    module.hot.accept('containers/App', () => {
        const App = require('containers/App').default;
        renderApp(App);
    });
} else {
    render(<App />, document.getElementById('app'));
}
