import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import Perf from 'react-addons-perf';
import moment from 'moment';
import HTML5Backend from 'react-dnd-html5-backend';
import { DragDropContext } from 'react-dnd';

import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';
import 'css/react_dates_overrides.css';

/* eslint no-unused-vars: "off" */
import fonts from 'css/fonts.css';
import styles from './styles.css';
/* eslint no-unused-vars: "error" */

import Routes from './Routes';

moment.locale('ru');

/**
 * Main Application container
 */
@DragDropContext(HTML5Backend)
class App extends Component {
    static propTypes = {
        orgInfo: PropTypes.object,
    };

    static defaultProps = {
        orgInfo: null,
    };

    componentDidMount() {
        window.Perf = Perf;
    }

    render() {
        return (
            <Router>
                <div style={{ width: '100%', height: '100%' }}>
                    <Routes />
                </div>
            </Router>
        );
    }
}

function mapStateToProps(state) {
    return {
        orgInfo: state.user.orgInfo.data,
    };
}

export default connect(mapStateToProps)(App);
