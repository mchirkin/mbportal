import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    Route,
    Redirect,
    Switch,
    withRouter,
} from 'react-router-dom';
import { Helmet } from 'react-helmet';

import LoginPage from 'routes/login/containers/LoginPage';
import GetLoginPage from 'routes/get_login/containers/GetLoginPage';
import Home from 'scenes/Home';
import NotFound from 'components/NotFound';

class Routes extends Component {
    static propTypes = {
        history: PropTypes.object.isRequired,
    };

    getPageTitle = (pathname) => {
        let title;
        switch (pathname) {
            case '/login':
                title = 'Авторизация';
                break;
            case '/home':
                title = 'Главная';
                break;
            case '/home/mail':
                title = 'Связь с банком';
                break;
            case '/home/contacts':
                title = 'Контакты';
                break;
            case '/contacts/add':
                title = 'Новый контакт';
                break;
            case '/home/payment':
                title = 'Платеж';
                break;
            case '/home/special':
                title = 'Спецпредложения';
                break;
            case '/home/requisites':
                title = 'Реквизиты';
                break;
            case '/home/statement':
                title = 'Выписка';
                break;
            case '/home/sign_payments':
                title = 'Платежи на подпись';
                break;
            case '/home/error_payments':
                title = 'Платежи с ошибкой';
                break;
            case '/home/settings/profile':
                title = 'Профиль';
                break;
            case '/home/settings/tarif':
                title = 'Тариф';
                break;
            case '/home/settings/certificates':
                title = 'Сертификаты';
                break;
            default:
                title = 'РосЕвроБанк';
        }
        return title;
    }

    componentWillMount() {
    }

    componentDidMount() {
    }

    render() {
        return (
            <div style={{ width: '100%', height: '100%' }}>
                <Helmet
                    defaultTitle="РосЕвроБанк"
                    titleTemplate="РосЕвроБанк - %s"
                    title={this.getPageTitle(this.props.history.location.pathname)}
                />
                <Switch>
                    <Route exact path="/get_login" component={GetLoginPage} />
                    <Route path="/home" component={Home} />
                    <Route path="/login" component={LoginPage} />
                    <Redirect exact from="/" to="/login" />
                    <Route path="/" component={NotFound} />
                </Switch>
            </div>
        );
    }
}

export default withRouter(Routes);
