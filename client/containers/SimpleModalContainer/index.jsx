import React from 'react';
import { unmountComponentAtNode } from 'react-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import showModal from 'modules/modal/actions/show_modal';
import ModalWrapper from 'components/ModalWrapper';
import SimpleModal from '../../components/SimpleModal';

/**
 * Контейнер простого модального окна(текст, кнопки подтверждения и отмены)
 */
class SimpleModalContainer extends React.Component {
    componentWillUnmount() {
        const container = document.getElementById('simple-modal');
        unmountComponentAtNode(container);
        container.parentNode.removeChild(container);
    }

    render() {
        return (
            <ModalWrapper
                header={this.props.header}
                isVisible={this.props.isVisible}
                width={500}
                closeModal={() => this.props.showModal({
                    isVisible: false,
                })}
                id="simple-modal"
                zIndex={10000000}
            >
                <SimpleModal {...this.props.modal.data} />
            </ModalWrapper>
        );
    }
}

/**
 * propTypes
 * @property {string} header - заголовок модального окна
 * @property {Object} modal - данные для модального окна
 * @property {boolean} isVisible - видимость модального окна
 * @property {Function} showModal - установка данных для модального окна
 */
SimpleModalContainer.propTypes = {
    header: React.PropTypes.string,
    modal: React.PropTypes.object.isRequired,
    isVisible: React.PropTypes.bool.isRequired,
    showModal: React.PropTypes.func.isRequired,
};

SimpleModalContainer.defaultProps = {
    header: '',
};

function mapStateToProps(state) {
    return {
        modal: state.modal,
        isVisible: state.modal.isVisible,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        showModal: bindActionCreators(showModal, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(SimpleModalContainer);
