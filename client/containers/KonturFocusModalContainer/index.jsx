/** Third party */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
/** Global */
import KonturFocusModal from 'components/KonturFocusModal';

export default class KonturFocusModalContainer extends Component {
    static propTypes = {
        modalData: PropTypes.object,
        setKonturFocusModalData: PropTypes.func,
        setKonturFocusModalVisibility: PropTypes.func,
        isVisible: PropTypes.bool,
    };

    static defaultProps = {
        modalData: {},
        setKonturFocusModalData: undefined,
        setKonturFocusModalVisibility: undefined,
        isVisible: false,
    };

    render() {
        let facts = [{
            statement: 'Информация об ИНН не найдена. Рекомендуем проверить правильность введенных данных.',
        }];
        let factsColor;
        if (this.props.modalData && !this.props.modalData.nodata) {
            factsColor = 'grey';
            facts = [{
                statement: 'Существенной информации по ИНН не найдено.',
            }];
            if (this.props.modalData.ActivityFacts.facts.length > 0) {
                facts = this.props.modalData.ActivityFacts.facts;
                factsColor = 'green';
            }
            if (this.props.modalData.PayAttentionFacts.facts.length > 0) {
                facts = this.props.modalData.PayAttentionFacts.facts;
                factsColor = 'yellow';
            }
            if (this.props.modalData.CriticalFacts.facts.length > 0) {
                facts = this.props.modalData.CriticalFacts.facts;
                factsColor = 'red';
            }
        }
        if (this.props.modalData && this.props.modalData.nodata) {
            factsColor = 'grey';
        }

        let href;
        if (this.props.modalData && this.props.modalData.CompanyInfo) {
            href = this.props.modalData.CompanyInfo.href;
        }
        if (this.props.modalData && this.props.modalData.IpInfo) {
            href = this.props.modalData.IpInfo.href;
        }

        let inn;
        let ogrn;
        if (this.props.modalData) {
            if (this.props.modalData.inn) {
                inn = this.props.modalData.inn;
            }
            if (this.props.modalData.CompanyInfo) {
                inn = this.props.modalData.CompanyInfo.inn;
                ogrn = this.props.modalData.CompanyInfo.ogrn;
            }
            if (this.props.modalData.IpInfo) {
                inn = this.props.modalData.IpInfo.inn;
                ogrn = this.props.modalData.IpInfo.ogrn;
            }
        }

        return (
            <KonturFocusModal
                factsColor={factsColor}
                facts={facts}
                href={href}
                inn={inn}
                ogrn={ogrn}
                nodata={this.props.modalData && this.props.modalData.nodata}
                closeModal={this.closeModal}
            />
        );
    }
}

/*
function mapStateToProps(state) {
    return {
        modalData: state.settings.contragents.konturFocusModalData,
        isVisible: state.settings.contragents.konturFocusModalVisible,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        setKonturFocusModalData: bindActionCreators(setKonturFocusModalData, dispatch),
        setKonturFocusModalVisibility: bindActionCreators(setKonturFocusModalVisibility, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(KonturFocusModalContainer);
*/
