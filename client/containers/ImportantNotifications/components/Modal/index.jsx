import React from 'react';
import PropTypes from 'prop-types';

import styles from './modal.css';

const Modal = ({ children, isVisible, closeModal }) => (
    <div
        className={styles.modal}
        data-visible={isVisible}
    >
        <div className={styles.content}>
            <div className={styles.header}>
                <div>Важные сообщения!</div>
                <div
                    className={styles.close}
                    onClick={closeModal}
                />
            </div>
            <div>
                {children}
            </div>
        </div>
    </div>
);

export default Modal;
