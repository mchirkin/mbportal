import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './notification.css';

export default class Notification extends Component {
    static propTypes = {
        id: PropTypes.string,
        text: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.node,
        ]).isRequired,
        btnText: PropTypes.string,
        handleBtnClick: PropTypes.func,
        moreContent: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.node,
        ]),
        color: PropTypes.oneOf(['blue', 'red']),
        btnTextValue: PropTypes.string,
    };

    static defaultProps = {
        id: '',
        btnText: '',
        handleBtnClick: undefined,
        moreContent: '',
        color: 'blue',
        btnTextValue: 'Выполнить',
    };

    constructor(props) {
        super(props);

        this.state = {
            moreContentIsVisible: false,
        };
    }

    toggleMore = () => {
        this.setState({
            moreContentIsVisible: !this.state.moreContentIsVisible,
        });
    }

    showMoreAndShake = () => {
        this.setState({
            moreContentIsVisible: true,
            shake: true,
        }, () => {
            setTimeout(() => {
                this.setState({
                    shake: false,
                });
            }, 1500);
        });
    }

    render() {
        const {
            id,
            text,
            btnText,
            handleBtnClick,
            moreContent,
            color,
        } = this.props;

        const { moreContentIsVisible } = this.state;

        let btnTextValue = btnText;
        if (moreContent === '') {
            btnTextValue = this.props.btnTextValue;
        } else {
            btnTextValue = moreContentIsVisible ? 'Скрыть' : 'Подробнее';
        }

        return (
            <div
                className={styles.wrapper}
                data-id={id}
                data-animate={this.state.shake ? 'shake' : ''}
            >
                <div className={styles.main} data-color={color}>
                    <div className={styles.content}>
                        {text}
                    </div>
                    <div
                        className={styles['action-btn']}
                        onClick={moreContent ? this.toggleMore : handleBtnClick}
                    >
                        {btnTextValue}
                    </div>
                </div>
                {moreContent && (
                    <div className={styles.more} data-visible={moreContentIsVisible}>
                        <div className={styles['more-content']}>
                            {moreContent}
                        </div>
                    </div>
                )}
            </div>
        );
    }
}
