import React from 'react';
import styles from './stay-info.css';

const StayInfo = props => (
    <div className={styles.wrapper}>
        {props.stayRef && (
            <div className={styles.row}>
                <div>
                    Идентификатор приостановления:
                </div>
                <div>
                    {props.stayRef}
                </div>
            </div>
        )}
        {props.amount && (
            <div className={styles.row}>
                <div>
                    Сумма ограничений:
                </div>
                <div>
                    {props.amount}
                </div>
            </div>
        )}
        {props.availablePaymentUrgents && (
            <div className={styles.row}>
                <div>
                    Очередность допустимых платежей:
                </div>
                <div>
                    {props.availablePaymentUrgents}
                </div>
            </div>
        )}
        {props.beginDate && (
            <div className={styles.row}>
                <div>
                    Дата начала действия приостановления:
                </div>
                <div>
                    {props.beginDate}
                </div>
            </div>
        )}
        {props.ifns && (
            <div className={styles.row}>
                <div>
                    Код ИФНС:
                </div>
                <div>
                    {props.ifns}
                </div>
            </div>
        )}
        {props.type && (
            <div className={styles.row}>
                <div>
                    Тип приостановления (источник):
                </div>
                <div>
                    {props.type}
                </div>
            </div>
        )}
        {props.reason && (
            <div className={styles.row}>
                <div>
                    Причина приостановления:
                </div>
                <div>
                    {props.reason}
                </div>
            </div>
        )}
        {props.comment && (
            <div className={styles.row}>
                <div>
                    Дополнительная информация:
                </div>
                <div>
                    {props.comment}
                </div>
            </div>
        )}
    </div>
);

export default StayInfo;
