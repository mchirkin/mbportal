import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import setImportantModalVisibility from 'modules/important_notifications/actions/set_visibility';

import Modal from './components/Modal';
import Notification from './components/Notification';
import StayInfo from './components/StayInfo';

class ImportantNotifications extends Component {
    static contextTypes = {
        router: PropTypes.object.isRequired,
    };

    static propTypes = {
        setImportantModalVisibility: PropTypes.func.isRequired,
        stays: PropTypes.object,
        list: PropTypes.array,
    }

    static defaultProps = {
        stays: null,
        list: [],
    }

    navigateToCertificates = () => {
        this.context.router.history.push('/home/settings/certificates');
    }

    closeModal = () => {
        this.props.setImportantModalVisibility(false);
    }

    openInfoModeNotification = () => {
        if (this.infoModeNotification) {
            this.infoModeNotification.showMoreAndShake();
        }
    }

    render() {
        const { list, stays } = this.props;

        const highPriorityList = (
            list &&
            list.length > 0 &&
            list.filter(el => el.priority && el.priority === 'high')
        );

        const standartPriorityList = (
            list &&
            list.length > 0 &&
            list.filter(el => !el.priority || el.priority !== 'high')
        );

        const accounts = stays && Object.keys(stays);

        return (
            <Modal
                {...this.props}
                closeModal={this.closeModal}
            >
                {highPriorityList && highPriorityList.length > 0
                    ? highPriorityList.map(el => (
                        <Notification
                            key={el.id}
                            {...el}
                            ref={(c) => {
                                if (el.id === 'info-mode') {
                                    this.infoModeNotification = c;
                                }
                            }}
                        />
                    ))
                    : null}
                {accounts && accounts.length > 0
                    ? accounts.map((account) => {
                        const accountStays = stays[account];
                        if (accountStays && !accountStays.errorCode) {
                            const stayList = Array.isArray(accountStays.stay) ? accountStays.stay : [accountStays.stay];

                            const moreContent = stayList.map(stay => (
                                <StayInfo {...stay} />
                            ));

                            return (
                                <Notification
                                    key={account}
                                    text={`По счету ${account} имеются ограничения!`}
                                    moreContent={(
                                        <div style={{ width: '100%' }}>
                                            {moreContent}
                                        </div>
                                    )}
                                    color="red"
                                />
                            );
                        }
                        return null;
                    })
                    : null}
                {standartPriorityList && standartPriorityList.length > 0
                    ? standartPriorityList.map(el => (
                        <Notification
                            key={el.id}
                            {...el}
                        />
                    ))
                    : null}
            </Modal>
        );
    }
}

function mapStateToProps(state) {
    return {
        stays: state.accounts.stays.data,
        list: state.importantNotifications.list,
        isVisible: state.importantNotifications.isVisible,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        setImportantModalVisibility: bindActionCreators(setImportantModalVisibility, dispatch),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
    null,
    {
        withRef: true,
    }
)(ImportantNotifications);
