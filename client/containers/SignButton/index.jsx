import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { fetch } from 'utils';
import getCertificates from 'utils/rutoken/get_certificates';
import { signDocument, signDocumentArray } from 'utils/rutoken/sign_document';

import fetchCertificates from 'modules/crypto/actions/certificates/fetch';
import fetchNewPayments from 'modules/payments/new_payments/actions/fetch';
import fetchSendPayments from 'modules/payments/send_payments/actions/fetch';

import Btn from 'components/ui/Btn';

import CertificateSelectModal from './components/CertificateSelectModal';

class SignButton extends Component {
    static propTypes = {
        /** список сертификатов пользователя */
        certificates: PropTypes.array,
        /** загружается ли в данный момент список сертификатов */
        certificatesIsFetching: PropTypes.bool,
        /** загрузка сертификатов */
        fetchCertificates: PropTypes.func.isRequired,
        /** обработчик нажатия на кнопку */
        onClick: PropTypes.func,
        /** загрузка платежей в статусе "новый" */
        fetchNewPayments: PropTypes.func.isRequired,
        /** загрузка платежей в статусе "в обработке" */
        fetchSendPayments: PropTypes.func.isRequired,
        /** идентификатор документа для подписи */
        docId: PropTypes.string,
        /** список идентификаторов документов для подписи */
        docIds: PropTypes.array,
        /** модуль документа в iSimple */
        docModule: PropTypes.string.isRequired,
        /** тип документа в iSimple */
        docType: PropTypes.string.isRequired,
        /** функция для вызова после успешной подписи */
        callback: PropTypes.func,
        /** функция для вызова после ошибки подписи */
        errorCallback: PropTypes.func,
        /** функция для вызова после отмены подписи(при отображении окна выбора сертификата) */
        cancelSignCallback: PropTypes.func,
        /** стили кнопки */
        btnStyle: PropTypes.object,
        /** цвет кнопки */
        btnColor: PropTypes.string,
        /** цвет loader'а на кнопке */
        loaderColor: PropTypes.string,
        /** текст кнопки */
        btnCaption: PropTypes.string,
        /** ширина формы для ввода пин-кода */
        width: PropTypes.number,
        /** высота формы для ввода пин-кода */
        height: PropTypes.number,
        /** класс формы для ввода пин-кода */
        formClassName: PropTypes.string,
        /** стили формы для ввода пин-кода */
        formStyle: PropTypes.object,
    };

    static defaultProps = {
        certificates: [],
        certificatesIsFetching: false,
        onClick: undefined,
        docId: undefined,
        docIds: undefined,
        callback: undefined,
        errorCallback: undefined,
        cancelSignCallback: undefined,
        btnStyle: {},
        btnColor: undefined,
        loaderColor: undefined,
        btnCaption: 'Подписать и отправить',
        width: undefined,
        height: undefined,
        formClassName: '',
        formStyle: {},
    };

    constructor(props) {
        super(props);

        this.state = {
            showPin: false,
            pinSymbolNumber: 0,
            pin: '',
            pinEditable: true,
            error: '',
            isFetching: false,
            selectCertificateModalIsVisible: false,
        };
    }

    componentWillMount() {
        if (!this.props.certificates && !this.props.certificatesIsFetching) {
            this.props.fetchCertificates();
        }
    }

    /**
     * Обработчик нажатия на кнопку
     * Если передан обработчик onClick, то вызываем его
     * По умолчанию вызываем подпись документа
     * @param {Event} e
     */
    handleBtnClick = (e) => {
        e.stopPropagation();

        if (this.props.onClick) {
            return this.props.onClick();
        }

        return this.signDocument();
    }

    signDocument = (selectedCertificate = null) => {
        console.log(selectedCertificate);
        if (!selectedCertificate && this.props.certificates.length > 1) {
            this.setState({
                selectCertificateModalIsVisible: true,
            });
            return;
        }

        const certificate = selectedCertificate || this.props.certificates[0];
        this.selectedCertificate = certificate;

        global.console.log('Certificate for sign: ', certificate);

        let pinSymbolNumber = 0;

        if (certificate.cryptoTypeCode.toLowerCase() === 'input_code') {
            pinSymbolNumber = 6;

            this.setState({
                isFetching: true,
            });
            // request sms sign
            this.requestSmsSign({
                certId: certificate.id,
                docId: this.props.docId,
                docIds: this.props.docIds,
            })
                .then((result) => {
                    if (result.errorCode) {
                        return Promise.reject(result);
                    }
                    this.setState({
                        showPin: true,
                        pinSymbolNumber,
                        isFetching: false,
                    }, () => {
                        setTimeout(() => {
                            this.pinInput.focus();
                        }, 0);
                    });
                });
        } else if (certificate.cryptoTypeCode.toLowerCase() === 'paycontrol') {
            // go paycontrol
            this.setState({
                isFetching: true,
                showPin: false,
            });

            this.requestSmsSign({
                certId: certificate.id,
                docId: this.props.docId,
                docIds: this.props.docIds,
            })
                .then((result) => {
                    console.log(result);
                    this.setState({
                        isFetching: false,
                    });

                    if (this.props.callback) {
                        this.props.callback({ paycontrol: true });
                    }

                    this.props.fetchNewPayments();
                    this.props.fetchSendPayments();

                    if (this.props.docType === 'doc_platpor') {
                        _paq.push(['trackGoal', 6]);
                    }
                    if (this.props.docType === 'mail2bank') {
                        _paq.push(['trackGoal', 7]);
                    }
                });
        } else {
            this.setState({
                showPin: true,
            }, () => {
                setTimeout(() => {
                    this.pinInput.focus();
                }, 0);
            });
        }
    }

    handlePinChange = (e) => {
        const value = e.target.value;

        this.setState({
            pin: value,
        }, () => {
            if (this.state.pinSymbolNumber !== 0 && value.length === this.state.pinSymbolNumber) {
                this.setState({
                    pinEditable: false,
                });
                this.pinSubmit(e);
            }
        });
    }

    pinSubmit = (e) => {
        e.preventDefault();

        const certificate = this.selectedCertificate || this.props.certificates[0];

        this.setState({
            isFetching: true,
            showPin: false,
        });

        if (certificate.cryptoTypeCode.toLowerCase() === 'input_code') {
            // request sms sign with pin
            this.requestSmsSign({
                certId: certificate.id,
                docId: this.props.docId,
                docIds: this.props.docIds,
                inputCode: this.state.pin,
            })
                .then((result) => {
                    if (result.errorCode) {
                        return Promise.reject(result);
                    }
                    this.setState({
                        showPin: false,
                        pin: '',
                    });
                    if (this.props.docIds) {
                        return this.sendDocumentArrayToBank();
                    }
                    return this.sendDocumentToBank();
                })
                .then(() => {
                    if (this.props.callback) {
                        this.props.callback({});
                    }
                    this.setState({
                        isFetching: false,
                    });
                    this.props.fetchNewPayments();
                    this.props.fetchSendPayments();
                    if (this.props.docType === 'doc_platpor') {
                        _paq.push(['trackGoal', 6]);
                    }
                    if (this.props.docType === 'mail2bank') {
                        _paq.push(['trackGoal', 7]);
                    }
                })
                .catch((err) => {
                    global.console.log(err);
                    this.setState({
                        isFetching: false,
                        showPin: true,
                        pin: '',
                        error: 'Неверный код',
                    });
                    setTimeout(() => {
                        this.setState({
                            isFetching: false,
                            showPin: true,
                            pin: '',
                            pinEditable: true,
                            error: '',
                        });
                        if (this.props.errorCallback) {
                            this.props.errorCallback();
                        }
                    }, 1500);
                });
        } else {
            getCertificates()
                .then((certificates) => {
                    let certFoundOnToken = false;
                    let certId = null;
                    certificates.forEach((tokenCert) => {
                        const tokenSerial = tokenCert.serialNumber.replace(/:/g, '').toLowerCase();
                        const isimpleSerial = certificate.hexSerial.toLowerCase();
                        if (tokenSerial === isimpleSerial) {
                            global.console.log('certificate found', tokenCert.serialNumber);
                            certId = tokenCert.certId;
                            certFoundOnToken = true;
                        }
                    });
                    if (certFoundOnToken) {
                        return certId;
                    }
                    return Promise.reject('Сертификат не найден на ключевом носителе');
                })
                .then((cert) => {
                    this.setState({
                        certId: cert,
                    });
                    if (this.props.docIds) {
                        const promiseArray = this.props.docIds.map(id => this.getDocumentData(id, certificate));
                        return Promise.all(promiseArray);
                    }
                    return this.getDocumentData(this.props.docId, certificate);
                })
                .then((data) => {
                    global.console.log(data);
                    this.setState({
                        // showPin: true,
                        docData: data,
                    });
                    return this.getSignedData();
                })
                .then((signedData) => {
                    if (Array.isArray(signedData)) {
                        return this.sendSignedDataArray(signedData, certificate);
                    }
                    return this.sendSignedData(signedData, certificate);
                })
                .then((res) => {
                    if (Array.isArray(res)) {
                        return this.sendDocumentArrayToBank();
                    }
                    return this.sendDocumentToBank();
                })
                .then((res) => {
                    global.console.log(res);
                    if (this.props.callback) {
                        this.props.callback({});
                    }
                    this.setState({
                        isFetching: false,
                        pin: '',
                    });
                    this.props.fetchNewPayments();
                    this.props.fetchSendPayments();
                })
                .catch((err) => {
                    global.console.log(err);

                    /*
                    let errorText = 'Произошла ошибка при подписи документа';
                    if (err.message === '3') {
                        errorText = 'Не подключено устройство';
                    }
                    */

                    this.setState({
                        isFetching: false,
                        showPin: true,
                        error: 'Произошла ошибка',
                    });

                    setTimeout(() => {
                        this.setState({
                            isFetching: false,
                            showPin: true,
                            pin: '',
                            pinEditable: true,
                            error: '',
                        });
                        if (this.props.errorCallback) {
                            this.props.errorCallback();
                        }
                    }, 1500);
                });
        }
    }

    requestSmsSign = ({ certId, docId, docIds, inputCode }) => fetch('/api/v1/crypto/sign/sms', {
        credentials: 'include',
        method: 'POST',
        body: JSON.stringify({
            certId,
            docId,
            docIds,
            inputCode,
        }),
    })
        .then(response => response.json())
        .then(json => json)
        .catch(err => err)

    /**
     * Вызов сервиса отправки документа в банк
     * @return {Promise}
     */
    sendDocumentToBank = () => new Promise((resolve, reject) => {
        fetch('/api/v1/documents/send2bank', {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify({
                docModule: this.props.docModule,
                docType: this.props.docType,
                docId: this.props.docId,
            }),
        })
            .then(response => response.text())
            .then((data) => {
                if (data.length > 0) {
                    const json = JSON.parse(data);
                    if (json.errorCode === '3004') {
                        return resolve({
                            msg: 'Для отправки документ должен быть подписан второй подписью',
                        });
                    }
                    if (json.errorText) {
                        return reject(json);
                    }
                }
                return resolve(data);
            })
            .catch(err => reject(err));
    })

    /**
     * Вызов сервиса отправки документов в банк для массива документов
     * @return {Promise}
     */
    sendDocumentArrayToBank = () => {
        const promiseArray = this.props.docIds.map(doc => new Promise((resolve, reject) => {
            fetch('/api/v1/documents/send2bank', {
                method: 'POST',
                credentials: 'include',
                body: JSON.stringify({
                    docModule: this.props.docModule,
                    docType: this.props.docType,
                    docId: doc,
                }),
            })
                .then(response => response.text())
                .then((data) => {
                    if (data.length > 0) {
                        const json = JSON.parse(data);
                        if (json.errorCode === '3004') {
                            return resolve({
                                msg: 'Для отправки документ должен быть подписан второй подписью',
                            });
                        }
                        if (json.errorText) {
                            return reject(json);
                        }
                    }
                    resolve(data);
                })
                .catch((err) => {
                    reject(err);
                });
        }));
        return Promise.all(promiseArray);
    }

    /**
     * Получить данные документа для подписи
     * @param {Object} data данные запрашиваемого документа
     * @return {Promise}
     */
    getDocumentData = (id, certificate) => new Promise((resolve, reject) => {
        fetch('/api/v1/documents/data', {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify({
                docModule: this.props.docModule,
                docType: this.props.docType,
                docId: id,
                libId: certificate.libId,
            }),
        }).then((response) => {
            if (response.status === 200) {
                return response.text();
            }
            reject('Произошла ошибка при запросе данных');
        }).then((data) => {
            resolve(data);
        }).catch((err) => {
            reject(err);
        });
    })

    /**
     * Отправка подписанных данных на проверку в iSimple
     * @param {String} signedData подписанные данные в base64
     * @return {Promise}
     */
    sendSignedData = (signedData, certificate) => new Promise((resolve, reject) => {
        fetch('/api/v1/crypto/verify', {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify({
                docModule: this.props.docModule,
                docType: this.props.docType,
                id: this.props.docId,
                libId: certificate.libId,
                certSerial: certificate.serial,
                data: this.state.docData,
                signedData,
            }),
        })
            .then(response => response.text()).then((data) => {
                const json = data.length > 0 ? JSON.parse(data) : null;
                if (json && json.errorText) {
                    reject(json.errorText);
                }
                resolve(data);
            })
            .catch((err) => {
                reject(err);
            });
    })

    /**
     * Отправка массива подписанных данных в iSimple
     * @param {Array} signedData массив подписанных данных документов в base64
     * @return {Promise}
     */
    sendSignedDataArray = (signedData, certificate) => {
        const promiseArray = signedData.map((data, index) => new Promise((resolve, reject) => {
            fetch('/api/v1/crypto/verify', {
                method: 'POST',
                credentials: 'include',
                body: JSON.stringify({
                    docModule: this.props.docModule,
                    docType: this.props.docType,
                    id: this.props.docIds[index],
                    libId: certificate.libId,
                    certSerial: certificate.serial,
                    data: this.state.docData[index],
                    signedData: data,
                }),
            })
                .then(response => response.text()).then((data) => {
                    global.console.log(data);
                    const json = data.length > 0 ? JSON.parse(data) : null;
                    if (json && json.errorText) {
                        reject(json.errorText);
                    }
                    resolve(data);
                })
                .catch((err) => {
                    reject(err);
                });
        }));
        return Promise.all(promiseArray);
    }

    getSignedData = () => {
        if (this.props.docIds) {
            return signDocumentArray(this.state.pin, this.state.certId, this.state.docData);
        }
        return signDocument(this.state.pin, this.state.certId, this.state.docData);
    }

    submitSelectCertificate = (certificate) => {
        this.signDocument(certificate);
        this.setState({
            selectCertificateModalIsVisible: false,
        });
    }

    closeSelectCertificateModal = () => {
        this.setState({
            selectCertificateModalIsVisible: false,
        });
        if (this.props.cancelSignCallback) {
            this.props.cancelSignCallback();
        }
    }

    render() {
        const { width, height, btnCaption, certificates } = this.props;

        return (
            <div>
                {!this.state.showPin
                    ? (
                        <Btn
                            caption={btnCaption || 'Подписать и отправить'}
                            bgColor={this.props.btnColor}
                            onClick={this.handleBtnClick}
                            style={this.props.btnStyle}
                            showLoader={this.state.isFetching}
                            loaderColor={this.props.loaderColor}
                        />
                    )
                    : (
                        <form
                            onClick={(e) => { e.stopPropagation(); }}
                            onFocus={(e) => { e.stopPropagation(); }}
                            onSubmit={this.pinSubmit}
                            className={this.props.formClassName}
                            style={Object.assign(
                                {},
                                {
                                    width,
                                    height,
                                    overflow: 'hidden',
                                    border: this.state.error
                                        ? '1px solid #ff3b30'
                                        : '1px solid #d8d8d8',
                                    borderRadius: 8,
                                },
                                this.props.formStyle,
                            )}
                        >
                            <input
                                ref={(el) => { this.pinInput = el; }}
                                placeholder="Введите код"
                                type="password"
                                value={this.state.error ? this.state.error : this.state.pin}
                                onChange={this.handlePinChange}
                                style={{
                                    width: '100%',
                                    height: '100%',
                                    textAlign: 'center',
                                    color: this.state.error
                                        ? '#ff3b30'
                                        : '#000',
                                    fontFamily: 'OpenSans-Regular',
                                    fontSize: 18,
                                    border: 0,
                                    outline: 'none',
                                }}
                                readOnly={!this.state.pinEditable}
                            />
                        </form>
                    )}
                {
                    certificates &&
                    certificates.length > 0 &&
                    this.state.selectCertificateModalIsVisible &&
                    (
                        <CertificateSelectModal
                            certificateList={certificates}
                            closeModal={this.closeSelectCertificateModal}
                            submitSelectCertificate={this.submitSelectCertificate}
                        />
                    )
                }
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        certificates: state.crypto.certificates.data,
        certificatesIsFetching: state.crypto.certificates.isFetching,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        fetchCertificates: bindActionCreators(fetchCertificates, dispatch),
        fetchNewPayments: bindActionCreators(fetchNewPayments, dispatch),
        fetchSendPayments: bindActionCreators(fetchSendPayments, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(SignButton);
