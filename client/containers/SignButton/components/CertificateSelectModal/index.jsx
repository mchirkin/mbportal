import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Scrollbars } from 'react-custom-scrollbars';

import Btn from 'components/ui/Btn';

import styles from './certificate-select-modal.css';

export default class CertificateSelectModal extends Component {
    static propTypes = {
        certificateList: PropTypes.array,
        submitSelectCertificate: PropTypes.func.isRequired,
        closeModal: PropTypes.func.isRequired,
    };

    static defaultProps = {
        certificateList: [],
    };

    constructor(props) {
        super(props);

        this.state = {
            selectedCertificate: 0,
        };
    }

    componentDidMount() {
        this.modal.addEventListener('click', (e) => {
            // e.preventDefault();
        });
    }

    selectCertificate = (index) => {
        this.setState({
            selectedCertificate: index,
        });
    }

    submitCertificate = () => {
        const { certificateList, submitSelectCertificate } = this.props;
        submitSelectCertificate(certificateList[this.state.selectedCertificate]);
    }

    render() {
        const {
            certificateList,
            closeModal,
        } = this.props;

        return (
            <div className={styles.modal} ref={(modal) => { this.modal = modal; }}>
                <div className={styles.modal__content}>
                    <div className={styles.header}>
                        Выберите сертификат
                    </div>
                    <Scrollbars
                        autoHeight
                        autoHeightMin={0}
                        autoHeightMax={250}
                    >
                        <div className={styles['certificate-list']}>
                            {certificateList.map((cert, index) => (
                                <div
                                    key={cert.id}
                                    className={styles['certificate-list__item']}
                                    onClick={() => { this.selectCertificate(index); }}
                                >
                                    <div className={styles['certificate-info']}>
                                        <div>
                                            {cert.subjectName}
                                        </div>
                                        <div>
                                            {cert.hexSerial}
                                        </div>
                                    </div>
                                    <div
                                        className={styles['item-checkbox']}
                                        data-active={this.state.selectedCertificate === index}
                                    >
                                        <div className={styles['item-checkbox-inner']} />
                                    </div>
                                </div>
                            ))}
                        </div>
                    </Scrollbars>
                    <div className={styles['btn-row']} ref={(el) => { this.btnRow = el; }}>
                        <Btn
                            bgColor="grey"
                            caption="&times;"
                            onClick={closeModal}
                            style={{
                                width: 46,
                                marginRight: 20,
                                fontSize: 34,
                            }}
                        />
                        <Btn
                            color="gradient"
                            caption="Выбрать"
                            onClick={this.submitCertificate}
                        />
                    </div>
                </div>
            </div>
        );
    }
}
