import React, { Component } from 'react';
import PropTypes from 'prop-types';

import TotalInfo from './components/TotalInfo';
import Forms from './components/Forms';

export default class Acquiring extends Component {
    static propTypes = {
        isVisible: PropTypes.bool,
        newTarifCaption: PropTypes.string,
        newProducts: PropTypes.array,
        handleBackwardBtn: PropTypes.func,
        handleForwardBtn: PropTypes.func,
        invalidInputs: PropTypes.array,
        deleteInvalidInput: PropTypes.func,
    }

    static defaultProps = {
        isVisible: false,
        newTarifCaption: '',
        newProducts: [],
        handleBackwardBtn: undefined,
        handleForwardBtn: undefined,
        deleteInvalidInput: undefined,
        invalidInputs: [],
    }

    constructor(props) {
        super(props);

        this.state = {
            /** Список полей */
            formFields: {
                terminalConnection: '',
                communicationType: '',
                needPinPad: '',
                ipAdress: '',
                networkMask: '',
                networkGateway: '',
                organizationName: '',
                setupAddress: '',
                checkSetupAddress: '',
                posSurname: '',
                posName: '',
                posMiddleName: '',
                posContactPhone: '',
                posTwoDigits: '',
            },
            fileList: [],
        };
    }

    /**
     * Обработчик инпутов
     */
    handleInputChange = (e) => {
        const id = e.target.getAttribute('id');
        const value = e.target.value;

        const newFormFields = Object.assign({}, this.state.formFields);
        newFormFields[id] = value;

        // Удаляем данные дадаты из стейта,
        // если пользователь решил не использовать сервис
        if (this.state.dadata !== undefined) {
            if (this.state.dadata[id] !== undefined) {
                const newDadata = Object.assign({}, this.state.dadata);

                delete newDadata[id];

                this.setState({
                    dadata: newDadata,
                });
            }
        }

        this.setState({
            formFields: newFormFields,
        });

        if (this.props.invalidInputs.includes(id)) {
            this.props.deleteInvalidInput(id);
        }
    }

    /**
     * Обработчик дадатовских инпутов
     */
    handleDadataSelect = (suggestion, id) => {
        const newFormFields = Object.assign({}, this.state.formFields);
        const newDadata = Object.assign({}, this.state.dadata);

        const suggestionData = suggestion.data;

        // удалям неиспользуемые поля дадаты
        // Object.keys(suggestionData).forEach(key => (suggestionData[key] == null) && delete suggestionData[key]);

        newDadata[id] = suggestionData;
        newFormFields[id] = suggestion.value;

        this.setState({
            dadata: newDadata,
            formFields: newFormFields,
        });
    }

    /**
     * Обработчик загрузчика файлов
     */
    handleFileChange = (acceptedFiles, id) => {
        if (acceptedFiles && acceptedFiles.length > 0) {
            this.setState({
                fileList: [...this.state.fileList, acceptedFiles[0]],
            });
        }

        if (this.props.invalidInputs.includes(id)) {
            this.props.deleteInvalidInput(id);
        }
    }

    handleOptionChange = (e) => {
        const id = e.target.dataset.name;
        const value = e.target.getAttribute('for');

        const newFormFields = Object.assign({}, this.state.formFields);
        newFormFields[id] = value;

        if (id === 'terminalConnection') {
            if (value === 'mobileGsm' || value === 'mobileWifi' || value === 'staticGsm') {
                newFormFields.communicationType = '';
                newFormFields.ipAdress = '';
                newFormFields.networkMask = '';
                newFormFields.networkGateway = '';
            }

            if (value === 'mobileGsm' || value === 'mobileWifi') {
                newFormFields.needPinPad = '';
            }
        }

        if (id === 'communicationType') {
            if (value === 'dhcp') {
                newFormFields.ipAdress = '';
                newFormFields.networkMask = '';
                newFormFields.networkGateway = '';
            }
        }

        this.setState({
            formFields: newFormFields,
        });

        if (this.props.invalidInputs.includes(id)) {
            this.props.deleteInvalidInput(id);
        }
    }

    deleteFile = (file) => {
        this.setState({
            fileList: this.state.fileList.filter(f => f !== file),
        });
    }

    getData = () => {
        const data = Object.assign({}, this.state.formFields);
        const dadata = Object.assign({}, this.state.dadata);
        const fileList = this.state.fileList;

        data.dadata = dadata;
        data.fileList = fileList;

        return data;
    }

    render() {
        return (
            <div style={this.props.isVisible ? {} : { display: 'none' }}>
                <Forms
                    handleInputChange={this.handleInputChange}
                    handleDadataSelect={this.handleDadataSelect}
                    handleFileChange={this.handleFileChange}
                    deleteFile={this.deleteFile}
                    handleOptionChange={this.handleOptionChange}
                    invalidInputs={this.props.invalidInputs}
                    {...this.state}
                />
                <TotalInfo />
            </div>
        );
    }
}
