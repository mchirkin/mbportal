import React from 'react';

import styles from './total-info.css';

const TotalInfo = () => {
    return (
        <div className={styles.container}>
            <p className={styles.title}>
                Перечень платежных систем, карты которых будут прениматься к оплате
            </p>
            <div className={styles['card-list']}>
                <img
                    src={require('./img/ico-visa.svg')}
                    alt="Логотип виза"
                    className={styles['card-logo']}
                />
                <img
                    src={require('./img/ico-master-card.svg')}
                    alt="Логотип мастеркард"
                    className={styles['card-logo']}
                />
                <img
                    src={require('./img/ico-mir.svg')}
                    alt="Логотип мир"
                    className={styles['card-logo']}
                />
            </div>
            <div className={styles.info}>
                <p className={styles['info-title']}>КОЛИЧЕСТВО ТЕРМИНАЛОВ:</p>
                <p className={styles['info-value']}>1</p>
            </div>
        </div>
    );
};

export default TotalInfo;
