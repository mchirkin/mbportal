import React, { Component } from 'react';
import PropTypes from 'prop-types';

import InputDadata from 'components/ui/InputDadata';
import InputGroup from 'components/ui/InputGroup';
import InputRadio from 'components/ui/InputRadio';
import InputFile from 'components/ui/InputFile';
import TextareaDadata from 'components/ui/TextareaDadata';

import styles from 'css/forms/forms.css';

export default class Forms extends Component {
    static propTypes = {
        /** Список полей */
        formFields: PropTypes.object,
        /** Список полей */
        fileList: PropTypes.array,
        /** Обработчик инпутов */
        handleInputChange: PropTypes.func.isRequired,
        /** Обработчик дадатовских инпутов */
        handleDadataSelect: PropTypes.func.isRequired,
        /** Обработчик для радио инпутов */
        handleOptionChange: PropTypes.func.isRequired,
        handleFileChange: PropTypes.func.isRequired,
        deleteFile: PropTypes.func.isRequired,
        invalidInputs: PropTypes.array,
    };

    static defaultProps = {
        formFields: {
            terminalConnection: '',
            communicationType: '',
            needPinPad: '',
            ipAdress: '',
            networkMask: '',
            networkGateway: '',
            organizationName: '',
            setupAddress: '',
            checkSetupAddress: '',
            posSurname: '',
            posName: '',
            posMiddleName: '',
            posContactPhone: '',
            posTwoDigits: '',
        },
        fileList: [],
        invalidInputs: [],
    };

    render() {
        const terminalConnection = [
            {
                id: 'mobileGsm',
                label: 'Мобильный (GSM/GPRS)',
            },
            {
                id: 'mobileWifi',
                label: 'Мобильный (WI-FI - соединение)',
            },
            {
                id: 'staticGsm',
                label: 'Стационарный (GSM/GPRS)',
            },
            {
                id: 'staticIp',
                label: 'Стационарный (IP-соединение)',
            },
        ];

        const needPinPad = [
            {
                id: 'pinPadNeeded',
                label: 'Да',
            },
            {
                id: 'pinPadNotNeeded',
                label: 'Нет',
            },
        ];

        const communicationType = [
            {
                id: 'dhcp',
                label: 'Динамический IP (DHCP)',
            },
            {
                id: 'static',
                label: 'Статистический IP (Static)',
            },
        ];

        return (
            <div className={styles.container}>
                <h3 className={styles.title}>
                    Оформление торгового эквайринга
                </h3>
                <div className={styles.wrap}>
                    <form className={styles.form}>
                        <div className={styles.group}>
                            <p className={styles['group-title']}>
                                Загрузите скан паспорта Индивидуального Предпринимателя или Генерального директора организации
                            </p>
                            <div className={styles['group-content']}>
                                <InputFile
                                    description={'Максимальный размер файлов - 25 Мб.\nДопустимые форматы: .PDF, .DOC, .DOCX, .JPG, .PNG'}
                                    fileList={this.props.fileList}
                                    handleFileChange={this.props.handleFileChange}
                                    deleteFile={this.props.deleteFile}
                                    hintContainer={this.hintContainer}
                                    hintDisabled={false}
                                    invalid={this.props.invalidInputs.includes('fileList')
                                        ? 'Загрузите файлы'
                                        : null}
                                />
                            </div>
                        </div>
                        <div className={styles.group}>
                            <p className={styles['group-title']}>
                                Выберите тип подключения терминала
                            </p>
                            <div className={styles['group-content']}>
                                <InputRadio
                                    handleOptionChange={this.props.handleOptionChange}
                                    radioList={terminalConnection}
                                    radioGroupName="terminalConnection"
                                    selectOption={this.props.formFields.terminalConnection}
                                    hintContainer={this.hintContainer}
                                    hintDisabled={false}
                                    invalid={this.props.invalidInputs.includes('terminalConnection')
                                        ? 'Выберете пункт'
                                        : null}
                                />
                            </div>
                        </div>
                        {this.props.formFields.terminalConnection === 'staticIp'
                            && (
                                <div className={styles.group}>
                                    <p className={styles['group-title']}>
                                        Тип связи
                                    </p>
                                    <div className={styles['group-content']}>
                                        <InputRadio
                                            handleOptionChange={this.props.handleOptionChange}
                                            radioList={communicationType}
                                            radioGroupName="communicationType"
                                            selectOption={this.props.formFields.communicationType}
                                            hintContainer={this.hintContainer}
                                            hintDisabled={false}
                                            invalid={this.props.invalidInputs.includes('communicationType')
                                                ? 'Выберете пункт'
                                                : null}
                                        />
                                    </div>
                                </div>
                            )
                        }
                        {this.props.formFields.communicationType === 'static'
                            && (
                                <div className={styles.group}>
                                    <p className={styles['group-title']}>
                                        Параметры статического IP-адреса:
                                    </p>
                                    <div className={styles['group-content']}>
                                        <InputGroup
                                            key="ipAdress"
                                            caption="IP адрес"
                                            id="ipAdress"
                                            value={this.props.formFields.ipAdress}
                                            onChange={this.props.handleInputChange}
                                            mask="999.999.999.999"
                                            maskChar="_"
                                            hintContainer={this.hintContainer}
                                            hintDisabled={false}
                                            invalid={this.props.invalidInputs.includes('ipAdress')
                                                ? 'Проверьте корректность данных'
                                                : null}
                                        />
                                        <InputGroup
                                            key="networkMask"
                                            caption="Маска сети"
                                            id="networkMask"
                                            value={this.props.formFields.networkMask}
                                            onChange={this.props.handleInputChange}
                                            mask="999.999.999.999"
                                            maskChar="_"
                                            hintContainer={this.hintContainer}
                                            hintDisabled={false}
                                            invalid={this.props.invalidInputs.includes('networkMask')
                                                ? 'Проверьте корректность данных'
                                                : null}
                                        />
                                        <InputGroup
                                            key="networkGateway"
                                            caption="Шлюз сети"
                                            id="networkGateway"
                                            value={this.props.formFields.networkGateway}
                                            onChange={this.props.handleInputChange}
                                            mask="999.999.999.999"
                                            maskChar="_"
                                            hintContainer={this.hintContainer}
                                            hintDisabled={false}
                                            invalid={this.props.invalidInputs.includes('networkGateway')
                                                ? 'Проверьте корректность данных'
                                                : null}
                                        />
                                    </div>
                                </div>
                            )
                        }
                        {/static/.test(this.props.formFields.terminalConnection)
                            && (
                                <div className={styles.group}>
                                    <p className={styles['group-title']}>
                                        Необходимость пин-пада
                                    </p>
                                    <div className={styles['group-content']}>
                                        <InputRadio
                                            handleOptionChange={this.props.handleOptionChange}
                                            radioList={needPinPad}
                                            radioGroupName="needPinPad"
                                            selectOption={this.props.formFields.needPinPad}
                                            hintContainer={this.hintContainer}
                                            hintDisabled={false}
                                            invalid={this.props.invalidInputs.includes('needPinPad')
                                                ? 'Выберете пункт'
                                                : null}
                                        />
                                    </div>
                                </div>
                            )
                        }
                        <div className={styles.group}>
                            <p className={styles['group-title']}>Данные для установки терминала</p>
                            <div className={styles['group-content']}>
                                <InputGroup
                                    key="organizationName"
                                    caption="Наименование организации на чеке"
                                    id="organizationName"
                                    value={this.props.formFields.organizationName}
                                    onChange={this.props.handleInputChange}
                                    hintContainer={this.hintContainer}
                                    hintDisabled={false}
                                    invalid={this.props.invalidInputs.includes('organizationName')
                                        ? 'Проверьте корректность данных'
                                        : null}
                                />
                                <TextareaDadata
                                    key="setupAddress"
                                    caption="Адрес установки терминала"
                                    id="setupAddress"
                                    value={this.props.formFields.setupAddress}
                                    onChange={this.props.handleInputChange}
                                    saveDadata={this.props.handleDadataSelect}
                                    service="address"
                                    autosize
                                    hintContainer={this.hintContainer}
                                    hintDisabled={false}
                                    invalid={this.props.invalidInputs.includes('setupAddress')
                                        ? 'Проверьте корректность данных'
                                        : null}
                                />
                                <TextareaDadata
                                    key="checkSetupAddress"
                                    caption="Адрес установки терминала на чеке"
                                    id="checkSetupAddress"
                                    value={this.props.formFields.checkSetupAddress}
                                    onChange={this.props.handleInputChange}
                                    saveDadata={this.props.handleDadataSelect}
                                    service="address"
                                    autosize
                                    hintContainer={this.hintContainer}
                                    hintDisabled={false}
                                    invalid={this.props.invalidInputs.includes('checkSetupAddress')
                                        ? 'Проверьте корректность данных'
                                        : null}
                                />
                                <InputDadata
                                    key="posSurname"
                                    caption="Фамилия"
                                    id="posSurname"
                                    value={this.props.formFields.posSurname}
                                    onChange={this.props.handleInputChange}
                                    saveDadata={this.props.handleDadataSelect}
                                    service="fio"
                                    parts="surname"
                                    hintContainer={this.hintContainer}
                                    hintDisabled={false}
                                    invalid={this.props.invalidInputs.includes('posSurname')
                                        ? 'Проверьте корректность данных'
                                        : null}
                                />
                                <InputDadata
                                    key="posName"
                                    caption="Имя"
                                    id="posName"
                                    value={this.props.formFields.posName}
                                    onChange={this.props.handleInputChange}
                                    saveDadata={this.props.handleDadataSelect}
                                    service="fio"
                                    parts="name"
                                    hintContainer={this.hintContainer}
                                    hintDisabled={false}
                                    invalid={this.props.invalidInputs.includes('posName')
                                        ? 'Проверьте корректность данных'
                                        : null}
                                />
                                <InputDadata
                                    key="posMiddleName"
                                    caption="Отчество"
                                    id="posMiddleName"
                                    value={this.props.formFields.posName}
                                    onChange={this.props.handleInputChange}
                                    saveDadata={this.props.handleDadataSelect}
                                    service="fio"
                                    parts="patronymic"
                                    hintContainer={this.hintContainer}
                                    hintDisabled={false}
                                    invalid={this.props.invalidInputs.includes('posMiddleName')
                                        ? 'Проверьте корректность данных'
                                        : null}
                                />
                                <InputGroup
                                    key="posContactPhone"
                                    caption="Контактный телефон"
                                    id="posContactPhone"
                                    value={this.props.formFields.posContactPhone}
                                    onChange={this.props.handleInputChange}
                                    mask="+7 (999) 999-99-99"
                                    maskChar="_"
                                    hintContainer={this.hintContainer}
                                    hintDisabled={false}
                                    invalid={this.props.invalidInputs.includes('posContactPhone')
                                        ? 'Проверьте корректность данных'
                                        : null}
                                />
                                <InputGroup
                                    key="posTwoDigits"
                                    caption="Первые две цифры ОКТМО или ОКАТО"
                                    id="posTwoDigits"
                                    value={this.props.formFields.posTwoDigits}
                                    onChange={this.props.handleInputChange}
                                    mask="99"
                                    maskChar="_"
                                    hintContainer={this.hintContainer}
                                    hintDisabled={false}
                                    invalid={this.props.invalidInputs.includes('posTwoDigits')
                                        ? 'Проверьте корректность данных'
                                        : null}
                                />
                            </div>
                        </div>
                    </form>
                    <div className={styles['information-content']} ref={(el) => { this.hintContainer = el; }} />
                </div>
            </div>
        );
    }
}
