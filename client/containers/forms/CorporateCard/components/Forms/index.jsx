import React, { Component } from 'react';
import PropTypes from 'prop-types';

import InputDadata from 'components/ui/InputDadata';
import InputGroup from 'components/ui/InputGroup';
import InputTextarea from 'components/ui/InputTextarea';
import TextareaDadata from 'components/ui/TextareaDadata';

import styles from 'css/forms/forms.css';

// import * as Hints from '../Hints';

export default class Forms extends Component {
    static propTypes = {
        /** Список полей */
        formFields: PropTypes.object,
        /** Обработчик инпутов */
        handleInputChange: PropTypes.func.isRequired,
        /** Обработчик дадатовских инпутов */
        handleDadataSelect: PropTypes.func.isRequired,
        /** Обработчик дат */
        getDateValue: PropTypes.func.isRequired,
        /** Обработчик для чекбоксов */
        handleCheckboxChange: PropTypes.func.isRequired,
        /** Одинаковые ли адреса регистрации и фактического проживания */
        sameAddress: PropTypes.bool,
        /** Одинаковые ли мобильный и домашний телефон */
        samePhones: PropTypes.bool,
        invalidInputs: PropTypes.array,
    };

    static defaultProps = {
        formFields: {},
        sameAddress: true,
        samePhones: true,
        invalidInputs: [],
    };

    render() {
        const {
            latinName,
            latinSurname,
            surname,
            middleName,
            passportSeries,
            passportNumber,
            passportDate,
            passportIssued,
            passportCode,
            placeOfBirth,
            dateOfBirth,
            registrationAddress,
            homeAddress,
            mobilePhone,
            homePhone,
            workName,
            workPosition,
            workAddress,
            workPhone,
        } = this.props.formFields;

        const {
            invalidInputs,
        } = this.props;

        return (
            <div className={styles.container}>
                <h3 className={styles.title}>
                    Оформление корпоративной карты VISA Business
                </h3>
                <div className={styles.card}>
                    <div className={styles['card-number']}>
                        1234 5678 0000 5678
                    </div>
                    <div className={styles['card-expire']}>
                        10/22
                    </div>
                    <div className={styles['card-holder']}>
                        {`
                            ${latinName === undefined ? '' : latinName} 
                            ${latinSurname === undefined ? '' : latinSurname}
                        `}
                    </div>
                </div>
                <div className={styles.wrap}>
                    <form className={styles.form}>
                        <div className={styles.group}>
                            <p className={styles['group-title']}>Данные держателя карты</p>
                            <div className={styles['group-content']}>
                                <div className={styles['group-row']}>
                                    <InputDadata
                                        key="surname"
                                        caption="Фамилия"
                                        id="surname"
                                        value={surname}
                                        onChange={this.props.handleInputChange}
                                        saveDadata={this.props.handleDadataSelect}
                                        service="fio"
                                        parts="surname"
                                        hintContainer={this.hintContainer}
                                        hintDisabled={false}
                                        invalid={invalidInputs.includes('surname')
                                            ? 'Проверьте корректность данных'
                                            : null}
                                    />
                                    <InputGroup
                                        key="latinSurname"
                                        caption="Фамилия"
                                        id="latinSurname"
                                        value={latinSurname}
                                        onChange={this.props.handleInputChange}
                                        hintContainer={this.hintContainer}
                                        hintDisabled={false}
                                        invalid={invalidInputs.includes('latinSurname')
                                            ? 'Проверьте корректность данных'
                                            : null}
                                    />
                                </div>
                                <div className={styles['group-row']}>
                                    <InputDadata
                                        key="name"
                                        caption="Имя"
                                        id="name"
                                        value={name}
                                        onChange={this.props.handleInputChange}
                                        saveDadata={this.props.handleDadataSelect}
                                        service="fio"
                                        parts="name"
                                        hintContainer={this.hintContainer}
                                        hintDisabled={false}
                                        invalid={invalidInputs.includes('name')
                                            ? 'Проверьте корректность данных'
                                            : null}
                                    />
                                    <InputGroup
                                        key="latinName"
                                        caption="Имя"
                                        id="latinName"
                                        value={latinName}
                                        onChange={this.props.handleInputChange}
                                        hintContainer={this.hintContainer}
                                        hintDisabled={false}
                                        invalid={invalidInputs.includes('latinName')
                                            ? 'Проверьте корректность данных'
                                            : null}
                                    />
                                </div>
                                <InputDadata
                                    key="middleName"
                                    caption="Отчество"
                                    id="middleName"
                                    value={middleName}
                                    onChange={this.props.handleInputChange}
                                    saveDadata={this.props.handleDadataSelect}
                                    service="fio"
                                    parts="patronymic"
                                    hintContainer={this.hintContainer}
                                    hintDisabled={false}
                                    invalid={invalidInputs.includes('middleName')
                                        ? 'Проверьте корректность данных'
                                        : null}
                                />
                            </div>
                        </div>
                        <div className={styles.group}>
                            <p className={styles['group-title']}>Паспортные данные</p>
                            <div className={styles['group-content']}>
                                <div className={styles['group-row']}>
                                    <InputGroup
                                        key="passportSeries"
                                        caption="Серия"
                                        id="passportSeries"
                                        value={passportSeries}
                                        onChange={this.props.handleInputChange}
                                        mask={'9999'}
                                        maskChar={'_'}
                                        hintContainer={this.hintContainer}
                                        hintDisabled={false}
                                        invalid={invalidInputs.includes('passportSeries')
                                            ? 'Проверьте корректность данных'
                                            : null}
                                    />
                                    <InputGroup
                                        key="passportNumber"
                                        caption="Номер"
                                        id="passportNumber"
                                        value={passportNumber}
                                        onChange={this.props.handleInputChange}
                                        mask={'999999'}
                                        maskChar={'_'}
                                        hintContainer={this.hintContainer}
                                        hintDisabled={false}
                                        invalid={invalidInputs.includes('passportNumber')
                                            ? 'Проверьте корректность данных'
                                            : null}
                                    />
                                    <InputGroup
                                        key="passportDate"
                                        caption="Дата выдачи"
                                        id="passportDate"
                                        value={passportDate}
                                        onChange={this.props.handleInputChange}
                                        mask={'99.99.9999'}
                                        hintContainer={this.hintContainer}
                                        hintDisabled={false}
                                        invalid={invalidInputs.includes('passportDate')
                                            ? 'Проверьте корректность данных'
                                            : null}
                                    />
                                </div>
                                <InputTextarea
                                    key="passportIssued"
                                    caption="Кем выдан"
                                    id="passportIssued"
                                    value={passportIssued}
                                    autosize
                                    height={'auto'}
                                    onChange={this.props.handleInputChange}
                                    hintContainer={this.hintContainer}
                                    hintDisabled={false}
                                    invalid={invalidInputs.includes('passportIssued')
                                        ? 'Проверьте корректность данных'
                                        : null}
                                />
                                <InputGroup
                                    key="passportCode"
                                    caption="Код подразделения"
                                    id="passportCode"
                                    value={passportCode}
                                    onChange={this.props.handleInputChange}
                                    mask={'999-999'}
                                    maskChar={'_'}
                                    hintContainer={this.hintContainer}
                                    hintDisabled={false}
                                    invalid={invalidInputs.includes('passportCode')
                                        ? 'Проверьте корректность данных'
                                        : null}
                                />
                                <InputDadata
                                    key="placeOfBirth"
                                    caption="Место рождения"
                                    id="placeOfBirth"
                                    service="address"
                                    value={placeOfBirth}
                                    onChange={this.props.handleInputChange}
                                    saveDadata={this.props.handleDadataSelect}
                                    hintContainer={this.hintContainer}
                                    hintDisabled={false}
                                    invalid={invalidInputs.includes('placeOfBirth')
                                        ? 'Проверьте корректность данных'
                                        : null}
                                />
                                <InputGroup
                                    key="dateOfBirth"
                                    caption="Дата рождения"
                                    id="dateOfBirth"
                                    value={dateOfBirth}
                                    onChange={this.props.handleInputChange}
                                    mask={'99.99.9999'}
                                    hintContainer={this.hintContainer}
                                    hintDisabled={false}
                                    invalid={invalidInputs.includes('dateOfBirth')
                                        ? 'Проверьте корректность данных'
                                        : null}
                                />
                                <div className={styles['group-union']}>
                                    <div className={styles.checkbox}>
                                        <input
                                            className={styles['checkbox-source']}
                                            type="checkbox"
                                            id="sameAddress"
                                            checked={this.props.sameAddress}
                                            onChange={this.props.handleCheckboxChange}
                                        />
                                        <label
                                            className={styles['checkbox-icon']}
                                            htmlFor="sameAddress"
                                            onClick={this.props.handleCheckboxChange}
                                        />
                                    </div>
                                    <TextareaDadata
                                        key="registrationAddress"
                                        caption="Адрес регистрации"
                                        id="registrationAddress"
                                        value={registrationAddress}
                                        autosize
                                        onChange={this.props.handleInputChange}
                                        saveDadata={this.props.handleDadataSelect}
                                        hintContainer={this.hintContainer}
                                        hintDisabled={false}
                                        invalid={invalidInputs.includes('registrationAddress')
                                            ? 'Проверьте корректность данных'
                                            : null}
                                    />
                                    {this.props.sameAddress
                                        ? (
                                            <InputTextarea
                                                key="homeAddress"
                                                caption="Адрес фактического проживания"
                                                id="homeAddress"
                                                value={registrationAddress}
                                                onChange={this.props.handleInputChange}
                                                autosize
                                                disabled
                                                height={'auto'}
                                            />
                                        )
                                        : (
                                            <TextareaDadata
                                                key="homeAddress"
                                                caption="Адрес фактического проживания"
                                                id="homeAddress"
                                                value={homeAddress}
                                                autosize
                                                onChange={this.props.handleInputChange}
                                                saveDadata={this.props.handleDadataSelect}
                                                hintContainer={this.hintContainer}
                                                hintDisabled={false}
                                                invalid={invalidInputs.includes('homeAddress')
                                                    ? 'Проверьте корректность данных'
                                                    : null}
                                            />
                                        )
                                    }
                                </div>
                                <div className={styles['group-union']}>
                                    <div className={styles.checkbox}>
                                        <input
                                            className={styles['checkbox-source']}
                                            type="checkbox"
                                            id="samePhones"
                                            checked={this.props.samePhones}
                                            onChange={this.props.handleCheckboxChange}
                                        />
                                        <label
                                            className={styles['checkbox-icon']}
                                            htmlFor="samePhones"
                                            onClick={this.props.handleCheckboxChange}
                                        />
                                    </div>
                                    <InputGroup
                                        key="mobilePhone"
                                        caption="Мобильный телефон"
                                        id="mobilePhone"
                                        value={mobilePhone}
                                        onChange={this.props.handleInputChange}
                                        mask="+7 (999) 999-99-99"
                                        maskChar="_"
                                        hintContainer={this.hintContainer}
                                        hintDisabled={false}
                                        invalid={invalidInputs.includes('mobilePhone')
                                            ? 'Проверьте корректность данных'
                                            : null}
                                    />
                                    {this.props.samePhones
                                        ? (
                                            <InputGroup
                                                key="homePhone"
                                                caption="Домашний телефон"
                                                id="homePhone"
                                                value={mobilePhone}
                                                onChange={this.props.handleInputChange}
                                                disabled
                                            />
                                        )
                                        : (
                                            <InputGroup
                                                key="homePhone"
                                                caption="Домашний телефон"
                                                id="homePhone"
                                                value={homePhone}
                                                onChange={this.props.handleInputChange}
                                                mask="+7 (999) 999-99-99"
                                                maskChar="_"
                                                hintContainer={this.hintContainer}
                                                hintDisabled={false}
                                                invalid={invalidInputs.includes('homePhone')
                                                    ? 'Проверьте корректность данных'
                                                    : null}
                                            />
                                        )
                                    }
                                </div>
                            </div>
                        </div>
                        <div className={styles.group}>
                            <p className={styles['group-title']}>Место работы</p>
                            <div className={styles['group-content']}>
                                <InputTextarea
                                    key="workName"
                                    caption="Название организации"
                                    id="workName"
                                    value={workName}
                                    autosize
                                    height={'auto'}
                                    onChange={this.props.handleInputChange}
                                    hintContainer={this.hintContainer}
                                    hintDisabled={false}
                                    invalid={invalidInputs.includes('workName')
                                        ? 'Проверьте корректность данных'
                                        : null}
                                />
                                <InputTextarea
                                    key="workPosition"
                                    caption="Занимаемая должность"
                                    id="workPosition"
                                    value={workPosition}
                                    autosize
                                    height={'auto'}
                                    onChange={this.props.handleInputChange}
                                    hintContainer={this.hintContainer}
                                    hintDisabled={false}
                                    invalid={invalidInputs.includes('workPosition')
                                        ? 'Проверьте корректность данных'
                                        : null}
                                />
                                <TextareaDadata
                                    key="workAddress"
                                    caption="Адрес организации"
                                    id="workAddress"
                                    value={workAddress}
                                    autosize
                                    onChange={this.props.handleInputChange}
                                    saveDadata={this.props.handleDadataSelect}
                                    hintContainer={this.hintContainer}
                                    hintDisabled={false}
                                    invalid={invalidInputs.includes('workAddress')
                                        ? 'Проверьте корректность данных'
                                        : null}
                                />
                                <InputGroup
                                    key="workPhone"
                                    caption="Контактный телефон организации"
                                    id="workPhone"
                                    value={workPhone}
                                    onChange={this.props.handleInputChange}
                                    mask="+7 (999) 999-99-99"
                                    maskChar="_"
                                    hintContainer={this.hintContainer}
                                    hintDisabled={false}
                                    invalid={invalidInputs.includes('workPhone')
                                        ? 'Проверьте корректность данных'
                                        : null}
                                />
                            </div>
                        </div>
                    </form>
                    <div className={styles['information-content']} ref={(el) => { this.hintContainer = el; }} />
                </div>
            </div>
        );
    }
}
