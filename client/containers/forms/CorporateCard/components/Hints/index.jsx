import React from 'react';
import { CSSTransition } from 'react-transition-group';

export const DocNumber = (
    <CSSTransition
        timeout={500}
        key="docNumber"
        classNames="fadeappear"
        appear
        in
    >
        <div>
            Если не указан, то номер документа будет присвоен автоматически
        </div>
    </CSSTransition>
);
