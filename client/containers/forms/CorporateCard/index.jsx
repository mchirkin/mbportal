import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { translit } from 'utils';

import Forms from './components/Forms';

export default class CorporateCard extends Component {
    static propTypes = {
        isVisible: PropTypes.bool,
        newTarifCaption: PropTypes.string,
        newProducts: PropTypes.array,
        handleBackwardBtn: PropTypes.func,
        handleForwardBtn: PropTypes.func,
        invalidInputs: PropTypes.array,
        deleteInvalidInput: PropTypes.func,
    }

    static defaultProps = {
        isVisible: false,
        newTarifCaption: '',
        newProducts: [],
        handleBackwardBtn: undefined,
        handleForwardBtn: undefined,
        deleteInvalidInput: undefined,
        invalidInputs: [],
    }

    constructor(props) {
        super(props);

        this.state = {
            /** Список полей */
            formFields: {
                passportSeries: '',
                passportNumber: '',
                passportDate: '',
                passportIssued: '',
                passportCode: '',
                surname: '',
                name: '',
                middleName: '',
                latinSurname: '',
                latinName: '',
                dateOfBirth: '',
                placeOfBirth: '',
                homePhone: '',
                workPhone: '',
                mobilePhone: '',
                workName: '',
                workPosition: '',
            },
            /** Одинаковые ли адреса регистрации и фактического проживания */
            sameAddress: true,
            /** Одинаковые ли мобильный и домашний телефон */
            samePhones: true,
        };
    }

    /**
     * Обработчик инпутов
     */
    handleInputChange = (e) => {
        const id = e.target.getAttribute('id');
        const value = e.target.value;

        const newFormFields = Object.assign({}, this.state.formFields);
        newFormFields[id] = value;

        // Удаляем данные дадаты из стейта,
        // если пользователь решил не использовать сервис
        if (this.state.dadata !== undefined) {
            if (this.state.dadata[id] !== undefined) {
                const newDadata = Object.assign({}, this.state.dadata);

                delete newDadata[id];

                this.setState({
                    dadata: newDadata,
                });
            }
        }

        if (id === 'surname' || id === 'latinSurname') {
            newFormFields.latinSurname = translit(value);
        }

        if (id === 'name' || id === 'latinName') {
            newFormFields.latinName = translit(value);
        }

        this.setState({
            formFields: newFormFields,
        });
        
        if (this.props.invalidInputs.includes(id)) {
            this.props.deleteInvalidInput(id);
        }
    }

    /**
     * Обработчик для чекбоксов
     */
    handleCheckboxChange = (e) => {
        const id = e.target.getAttribute('for');

        this.setState({
            [id]: !this.state[id],
        }, () => {
            if (id === 'sameAddress' && this.state[id]) {
                const newFormFields = Object.assign({}, this.state.formFields);
                const newDadata = Object.assign({}, this.state.dadata);

                delete newFormFields.homeAddress;
                delete newDadata.homeAddress;

                this.setState({
                    formFields: newFormFields,
                    dadata: newDadata,
                });
            }
            if (id === 'samePhones' && this.state[id]) {
                const newFormFields = Object.assign({}, this.state.formFields);

                delete newFormFields.homePhone;

                this.setState({
                    formFields: newFormFields,
                });
            }
        });
    }

    /**
     * Обработчик дадатовских инпутов
     */
    handleDadataSelect = (suggestion, id) => {
        const newFormFields = Object.assign({}, this.state.formFields);
        const newDadata = Object.assign({}, this.state.dadata);

        const suggestionData = suggestion.data;

        // удалям неиспользуемые поля дадаты
        // Object.keys(suggestionData).forEach(key => (suggestionData[key] == null) && delete suggestionData[key]);

        newDadata[id] = suggestionData;
        newFormFields[id] = suggestion.value;

        if (id === 'surname') {
            newFormFields.latinSurname = translit(suggestion.value);
        }

        if (id === 'name') {
            newFormFields.latinName = translit(suggestion.value);
        }

        this.setState({
            dadata: newDadata,
            formFields: newFormFields,
        });
    }

    /**
     * Обработчик дат
     */
    getDateValue = (date, id) => {
        const newFormFields = Object.assign({}, this.state.formFields);
        newFormFields[id] = date;

        this.setState({
            formFields: newFormFields,
        });
    }

    getData = () => {
        const data = Object.assign({}, this.state.formFields);
        const dadata = Object.assign({}, this.state.dadata);

        if (this.state.sameAddress && dadata.registrationAddress !== undefined) {
            dadata.homeAddress = dadata.registrationAddress;
        }
        if (this.state.samePhones) {
            data.homePhone = data.mobilePhone;
        }

        data.dadata = Object.assign({}, dadata);

        return data;
    }

    render() {
        return (
            <div style={this.props.isVisible ? {} : { display: 'none' }}>
                <Forms
                    handleInputChange={this.handleInputChange}
                    handleDadataSelect={this.handleDadataSelect}
                    handleCheckboxChange={this.handleCheckboxChange}
                    getDateValue={this.getDateValue}
                    invalidInputs={this.props.invalidInputs}
                    {...this.state}
                />
            </div>
        );
    }
}
