import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import addToast from 'modules/toast/actions/add_toast';
import deleteToast from 'modules/toast/actions/delete_toast';
import MountToBody from 'components/MountToBody';
import ToastList from 'components/ToastList';

class ToastContainer extends Component {
    static propTypes = {
        toastList: PropTypes.array.isRequired,
        addToast: PropTypes.func.isRequired,
        deleteToast: PropTypes.func.isRequired,
    };

    static defaultProps = {

    };

    render() {
        return (
            <MountToBody>
                <ToastList data={this.props.toastList} />
            </MountToBody>
        );
    }
}

function mapStateToProps(state) {
    return {
        toastList: state.toast.toastList,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        addToast: bindActionCreators(addToast, dispatch),
        deleteToast: bindActionCreators(deleteToast, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ToastContainer);
