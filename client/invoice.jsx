import React from 'react';
import { render } from 'react-dom';

import Invoice from 'scenes/Invoice';

const Component = () => (
    <div style={{ width: '100%', height: '100%' }}>
        <Invoice />
    </div>
);

render(
    <Component />,
    document.getElementById('app'),
);
