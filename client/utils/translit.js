export default function translit(clienteng) {
    const space = '';
    const text = clienteng === undefined ? '' : clienteng.toLowerCase();
    const transl = {
        а: 'a',
        б: 'b',
        в: 'v',
        г: 'g',
        д: 'd',
        е: 'e',
        ё: 'e',
        ж: 'zh',
        з: 'z',
        и: 'i',
        й: 'y',
        к: 'k',
        л: 'l',
        м: 'm',
        н: 'n',
        о: 'o',
        п: 'p',
        р: 'r',
        с: 's',
        т: 't',
        у: 'u',
        ф: 'f',
        х: 'kh',
        ц: 'ts',
        ч: 'ch',
        ш: 'sh',
        щ: 'shch',
        ъ: space,
        ы: 'y',
        ь: space,
        э: 'e',
        ю: 'yu',
        я: 'ya',
        ' ': space,
        _: space,
        '`': space,
        '~': space,
        '!': space,
        '@': space,
        '#': space,
        $: space,
        '%': space,
        '^': space,
        '&': space,
        '*': space,
        '(': space,
        ')': space,
        '-': space,
        '=': space,
        '+': space,
        '[': space,
        ']': space,
        '\\': space,
        '|': space,
        '/': space,
        '.': space,
        ',': space,
        '{': space,
        '}': space,
        '\'': space,
        '"': space,
        ';': space,
        ':': space,
        '?': space,
        '<': space,
        '>': space,
        '№': space,
    };
    let result = '';
    let curent = '';
    for (let i = 0; i < text.length; i += 1) {
        if (transl[text[i]] !== undefined) {
            if (curent !== transl[text[i]] || curent !== space) {
                result += transl[text[i]];
                curent = transl[text[i]];
            }
        }
        else {
            result += text[i];
            curent = text[i];
        }
    }

    result = result.toUpperCase();
    return result.trim();
}
