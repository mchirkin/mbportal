/**
 * Добавление 0 перед однозначным числом
 * @param {number} num
 * @return {string}
 */
function padZero(num) {
    const numStr = num.toString();
    return numStr.length > 1 ? numStr : `0${numStr}`;
}

/**
 * Установка времени 00:00:00:0000 для даты
 * @param {Date} date дата для сброса
 * @return {Date}
 */
function resetDate(date) {
    date.setMilliseconds(0);
    date.setSeconds(0);
    date.setMinutes(0);
    date.setHours(0);
    return date;
}

/**
 * Получение сторового представления даты по заданному формату
 * @param {Date} date дата
 * @param {string} format сторчный формат даты. d - день, m - месяц, Y - год
 * @return {string}
 */
function getDateString(date, format) {
    const day = padZero(date.getDate());
    const month = padZero(date.getMonth() + 1);
    const year = date.getFullYear();
    if (format) {
        return format.replace('d', day).replace('m', month).replace('Y', year);
    }
    return `${day}.${month}.${year}`;
}

export default {
    padZero,
    resetDate,
    getDateString,
};
