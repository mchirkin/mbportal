import rutoken from 'rutoken';

export default async function loadPlugin() {
    await rutoken.ready;

    let isExtensionInstalled = true;
    if (window.chrome && !/Edge\/\d/i.test(navigator.userAgent)) {
        console.log('[Rutoken] Check extension');
        isExtensionInstalled = await rutoken.isExtensionInstalled();
    }

    let isPluginInstalled;
    if (isExtensionInstalled) {
        isPluginInstalled = await rutoken.isPluginInstalled();
    } else {
        throw new Error('Rutoken Extension wasn\'t found');
    }

    let plugin;
    if (isPluginInstalled) {
        plugin = await rutoken.loadPlugin();
    } else {
        throw new Error('Rutoken Plugin isn\'t installed');
    }

    return plugin;
}
