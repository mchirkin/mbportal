import loadPlugin from './load_plugin';

/**
 * Генерация ключевой пары на борту токена
 * @param {string} pin - пин-код от токена
 * @return {Promise}
 */
export default function generateKeyPair(pin) {
    // маркер ключевой пары - base64 от текущего времени
    const key = window.btoa(encodeURIComponent(new Date().toLocaleString()));
    return new Promise((resolve, reject) => {
        loadPlugin().then((plugin) => {
            plugin.login(0, pin);
            return plugin.generateKeyPair(0, 'A', key, {
                keyType: plugin.KEY_TYPE_COMMON,
                needPin: false,
                needConfirm: false,
            });
        }).then((res) => {
            console.log(res);
            resolve(key);
        }).catch((reason) => {
            console.log(reason);
            reject(reason);
        });
    });
}
