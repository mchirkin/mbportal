import loadPlugin from './load_plugin';

export function signDocument(pin, certificate, base64data) {
    return new Promise((resolve, reject) => {
        loadPlugin().then((plugin) => {
            console.log(pin, certificate, base64data);
            plugin.login(0, pin);
            return plugin.sign(0, certificate, base64data, true, {
                detached: false,
                addUserCertificate: true,
                addSignTime: true,
                useHardwareHash: false,
            });
        }).then((res) => {
            console.log(res);
            resolve(res);
        }).catch((reason) => {
            console.log(reason);
            reject(reason);
        });
    });
}

export function signDocumentArray(pin, certificate, base64data) {
    return new Promise((resolve, reject) => {
        loadPlugin().then((plugin) => {
            console.log(pin, certificate, base64data);
            plugin.login(0, pin);
            const signPromises = base64data.map(data => plugin.sign(0, certificate, data, true, {
                detached: false,
                addUserCertificate: true,
                addSignTime: true,
                useHardwareHash: false,
            }));
            return Promise.all(signPromises);
        }).then((values) => {
            console.log(values);
            resolve(values);
        }).catch((reason) => {
            console.log(reason);
            reject(reason);
        });
    });
}
