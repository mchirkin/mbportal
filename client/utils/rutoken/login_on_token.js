import loadPlugin from './load_plugin';

export default function loginOnToken(pin) {
    return new Promise((resolve, reject) => {
        loadPlugin().then((plugin) => {
            console.log('pin', pin);
            plugin.login(0, pin);
            resolve(true);
        }).catch((err) => {
            reject(err);
        });
    });
}
