import loadPlugin from './load_plugin';

/**
 * Импортирование сертификата на токен
 * @param {string} pin - пин-код от токена
 * @param {string} certificate - сертификат в формате PEM
 * @return {Promise}
 */
export default function importCertificate(pin, certificate) {
    return new Promise((resolve, reject) => {
        loadPlugin().then((plugin) => {
            console.log(plugin);
            plugin.enumerateDevices();
            plugin.login(0, pin);
            return plugin.importCertificate(0, certificate, plugin.CERT_CATEGORY_USER);
        }).then((certId) => {
            console.log(certId);
            resolve(certId);
        }).catch((reason) => {
            console.log(reason);
            reject(reason);
        });
    });
}
