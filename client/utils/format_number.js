/**
 * Форматирование числа из вида 0000000(.|,)000 в вид 0 000 000(.|,)000
 * @param {string | number} number - исходное число
 * @param {boolean} withFrac - возвращать с дробной частью
 * @param {number | boolean} precision - количество символов после запятой
 * @return {string} отформатированное число
 */
export default function formatNumber(number, withFrac = true, precision = false) {
    if (!number) {
        return '-';
    }
    const numString = number.toString().replace(/ /g, '');
    let arr;
    if (/\./.test(numString)) {
        arr = numString.split('.');
    }
    if (/,/.test(numString)) {
        arr = numString.split(',');
    }
    const int = typeof arr !== 'undefined' ? arr[0] : numString;
    const frac = typeof arr !== 'undefined' && arr.length > 1 ? arr[1] : '00';
    const fracPart = precision ? frac.substr(0, precision) : frac;
    const integerPart = int.split('').reverse().join('').replace(/(\d{3})/g, '$1 ');
    const integerPartResult = integerPart.split('').reverse().join('').replace(/- ?/, '- ');
    return withFrac ? `${integerPartResult || '0'},${fracPart}`.trim() : integerPartResult.trim();
}
