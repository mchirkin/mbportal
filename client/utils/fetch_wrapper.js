import fetch from 'isomorphic-fetch';

export default function fetchWrapper(url, options) {
    let csrfToken;
    if (/\/login/.test(url) || /confirm_register_ul/.test(url) || /captcha/.test(url)) {
        csrfToken = document.querySelector('#csrf-token').value;
    } else {
        csrfToken = localStorage.getItem('csrfToken');
    }
    const headers = new Headers({
        'X-CSRF-TOKEN': csrfToken,
    });
    const newOptions = Object.assign({ credentials: 'include' }, options, { headers });
    return fetch(url, newOptions).then((response) => {
        if (
            response.status === 401 &&
            !/\/login/.test(url) &&
            !/\/change_password/.test(url) &&
            !/\/change_login/.test(url) &&
            !/confirm_register_ul/.test(url) &&
            !/captcha/.test(url) &&
            !/two_fa/.test(url)
        ) {
            fetch('/api/v1/logout').then(() => {
                document.location.href = '/login';
            });
        }
        return response;
    });
}
