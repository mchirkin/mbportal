export function findTargetInChildNode(parent, target) {
    if (!parent) {
        return false;
    }
    return parent.contains(target);
}
