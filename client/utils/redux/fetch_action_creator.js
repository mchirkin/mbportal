function request(type) {
    return function (isFetching) {
        return {
            type,
            isFetching,
        };
    };
}

function receive(type) {
    return function (data) {
        return {
            type,
            data,
        };
    };
}

function reject(type) {
    return function (error) {
        return {
            type,
            error,
        };
    };
}

export default {
    request,
    receive,
    reject,
};
