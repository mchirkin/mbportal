import { rubles } from 'rubles';

export default function numberToPhrase(number, currCode = 'RUB') {
    if (!number || (currCode.toUpperCase() !== 'RUB' && currCode.toUpperCase() !== 'RUR')) {
        return '';
    }

    const numberTrimed = number.toString().replace(/\s/g, '');

    if (/^\d{13}/.test(numberTrimed) || /e/.test(numberTrimed)) {
        return '';
    }

    return rubles(number);
}
