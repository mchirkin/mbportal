export default function debounce(func, delay, ...rest) {
    let inDebounce;
    return () => {
        const context = this;
        const args = [func, delay, ...rest];
        clearTimeout(inDebounce);
        inDebounce = setTimeout(() => func.apply(context, args), delay);
        return inDebounce;
    };
}
