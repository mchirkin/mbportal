class Emitter {
    constructor() {
        this.events = {};
    }

    addEvent = (eventName) => {
        console.log('[Emitter] addEvent', eventName);

        if (!this.events[eventName]) {
            this.events[eventName] = {
                listeners: [],
                lastPayload: null,
            };
        }
    }

    dispatchEvent = (eventName, payload) => {
        console.log(`[Emitter] dispatch ${eventName}`, payload);

        this.events[eventName].lastPayload = payload;

        this.events[eventName].listeners.forEach((listener) => {
            listener(payload);
        });
    }

    addListener = (eventName, listener) => {
        if (!this.events[eventName]) {
            this.addEvent(eventName);
        }

        this.events[eventName].listeners.push(listener);
    }

    removeListener = (eventName, listener) => {
        if (!this.events[eventName]) {
            return;
        }

        this.events[eventName].listeners = this.events[eventName].listeners.filter(l => l !== listener);
    }
}

const emitter = new Emitter();

export default emitter;
