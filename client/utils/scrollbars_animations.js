/**
 * Анимация скролла компонента scrollbars
 * @param {String} direction направление (горизонтакльное или вертикальное)
 * @param {Node} element дом элемент, который надо скроллить
 * @param {Number} to кол-во пикселей, на которое передвинуть
 * @param {Number} duration скорость анимации
 * @param {Object} context контекст
 */
export default function scrollbarsAnimations({
    direction,
    element,
    to,
    duration,
    context,
}) {
    let start;

    if (context.scrollTimeout) {
        clearTimeout(context.scrollTimeout);
    }

    switch (direction) {
        case 'x':
            start = element.getScrollLeft();
            break;
        case 'y':
            start = element.getScrollTop();
            break;
        default:
            start = element.getScrollTop();
    }

    const change = to - start;
    let currentTime = 0;
    const increment = 20;

    const animation = (t, b, c, d) => c * t / d + b;

    const animateScroll = () => {
        currentTime += increment;
        const val = animation(currentTime, start, change, duration);
        if (direction === 'x') {
            element.scrollLeft(val);
        } else {
            element.scrollTop(val);
        }
        if (currentTime < duration) {
            context.scrollTimeout = setTimeout(animateScroll, increment);
        }
    };
    animateScroll();
}
