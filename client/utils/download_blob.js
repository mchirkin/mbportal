export default function downloadBlob(blob, fileName) {
    if (window.navigator.msSaveBlob) {
        // Для IE
        window.navigator.msSaveBlob(blob, fileName);
    } else {
        const url = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.style.display = 'none';
        a.href = url;
        a.download = fileName;
        a.click();
        window.URL.revokeObjectURL(url);
        a.parentNode.removeChild(a);
    }
}
