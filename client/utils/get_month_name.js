export default function getMonthName(monthNumber, startIndex = 0) {
    monthNumber = typeof monthNumber === 'string' ? parseInt(monthNumber, 10) : monthNumber;
    const monthNameArray = [
        'Январь',
        'Февраль',
        'Март',
        'Апрель',
        'Май',
        'Июнь',
        'Июль',
        'Август',
        'Сентябрь',
        'Октябрь',
        'Ноябрь',
        'Декабрь',
    ];
    return monthNameArray[monthNumber - startIndex];
}
