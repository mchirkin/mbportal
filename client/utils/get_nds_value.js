/**
 * Расчет значения НДС от суммы платежа
 * @param {string|number} amount сумма платежа, от которой рассчитывается НДС
 * @param {Object} nds объект с информацией об НДС
 * @return {number} значение НДС
 */
export default function getNdsValue(amount, nds) {
    const rate = parseFloat(nds, 10);
    const ndsValue = parseFloat(amount, 10) * (rate / 100) / (1 + rate / 100);

    return ndsValue ? ndsValue.toFixed(2) : 0;
}
