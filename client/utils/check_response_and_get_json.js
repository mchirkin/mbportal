/**
 * Проверка запроса и получение json ответа
 * @param {Object} response - ответ от вызова fetch
 * @param {string} prop - свойство
 * @return {Object}
 */
export default async function checkResponseAndGetJson(response, prop = '', makeArray = false) {
    if (!response.ok && response.status !== 500) {
        throw Error('Network error');
    }
    const json = await response.json();
    if (json && json.errorText) {
        throw Error(json.errorText);
    }
    let data = json;
    if (!data) return data;
    if (prop.length > 0) {
        data = json[prop];
    }
    if (makeArray) {
        data = Array.isArray(data) ? data : [data];
    }
    return data;
}
