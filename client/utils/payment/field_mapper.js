const fieldMapper = (field) => {
    const fieldList = {
        kpp: 'kpp',
        receiverName: 'corrFullname',
        receiverInn: 'corrInn',
        receiverKpp: 'corrKpp',
        receiverAccount: 'corrAccNumber',
        bankOfReceiver: 'corrBankBik',
        stat: 'stat',
        uin: 'uin',
        paymentDesc: 'description',
        kbk: 'kbk',
        oktmo: 'okato',
        ground: 'ground',
        taxdocnum: 'taxdocnum',
        taxdocdate: 'taxdocdate',
        docDate: 'docDate',
        docNumber: 'docNumber',
        amount: 'amount',
        tax1: 'tax1',
        tax2: 'tax2',
        tax3: 'tax3',
        notifyEmail: 'notifyReceiverEmail',
        notifyMobile: 'notifyReceiverPhone',
    };

    return fieldList[field];
};

export default fieldMapper;
