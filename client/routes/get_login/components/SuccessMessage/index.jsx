import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router-dom';
import styles from './styles.css';

export default class UserBlocked extends Component {
    static propTypes = {
        error: PropTypes.object,
    };

    static defaultProps = {
        error: {},
    };

    render() {
        return (
            <div className={styles['modal-wrapper']}>
                <div className={styles.modal}>
                    <div className={styles['form-header']}>
                        <div className={styles['reb-logo']} />
                        <div className={styles['product-name']}>
                            Интернет-банк для бизнеса
                        </div>
                    </div>
                    <h3 className={styles['modal-header']}>
                        Логин и пароль направлены на номер мобильного телефона и адрес электронной почты,
                        который Вы указали в заявлении на подключение к интернет-банку.
                    </h3>
                    <Link className={styles['get-login']} to="login">
                        <div className={styles['get-login-btn']}>
                            Перейти на страницу входа
                        </div>
                    </Link>
                </div>
            </div>
        );
    }
}
