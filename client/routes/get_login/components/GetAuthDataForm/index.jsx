import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import IconedInput from 'components/ui/IconedInput';
import styles from './styles.css';

export default function GetAuthDataForm(props) {
    return (
        <div className={styles.wrapper}>
            <div className={styles['form-header']}>
                <div className={styles['reb-logo']} />
                <div className={styles['product-name']}>
                    Получение логина и пароля
                </div>
            </div>
            <form className={styles['login-form']} onSubmit={props.handleFormSubmit}>
                <IconedInput
                    className={styles['form-input']}
                    icon={require('./img/tid.png')}
                    iconBackgroundColor={!props.values.showCodeInput && props.values.error ? '#d75342' : ''}
                    inputProps={{
                        id: 'inn',
                        type: 'text',
                        placeholder: 'ИНН',
                        onChange: props.handleInputChange,
                        mask: '999999999999',
                        maskChar: null,
                    }}
                />
                <IconedInput
                    className={styles['form-input']}
                    icon={require('./img/plus7.png')}
                    iconBackgroundColor={!props.values.showCodeInput && props.values.error ? '#d75342' : ''}
                    inputProps={{
                        id: 'phone',
                        type: 'text',
                        placeholder: 'Номер телефона',
                        onChange: props.handleInputChange,
                        mask: '(999) 999-99-99',
                        maskChar: '_',
                    }}
                    errorMsg={!props.values.showCodeInput && props.values.error}
                />
                {props.values.showCodeInput
                    ? (
                        <IconedInput
                            className={styles['form-input']}
                            icon={require('./img/lock.svg')}
                            iconBackgroundColor={props.values.showCodeInput && props.values.error ? '#d75342' : ''}
                            iconBackgroundSize={28}
                            inputProps={{
                                id: 'confirmCode',
                                type: 'text',
                                placeholder: 'Код',
                                onChange: props.handleInputChange,
                            }}
                            errorMsg={props.values.showCodeInput && props.values.error}
                        />
                    )
                    : null}
                <button className={styles['form-submit']}>
                    Отправить
                </button>
            </form>
            <div className={styles['back-to-login']}>
                <Link to="/login">
                    <span>
                        Вернуться на страницу входа
                    </span>
                </Link>
            </div>
        </div>
    );
}

GetAuthDataForm.propTypes = {
    handleFormSubmit: PropTypes.func.isRequired,
    handleInputChange: PropTypes.func.isRequired,
    values: PropTypes.object,
};

GetAuthDataForm.defaultProps = {
    values: {},
};
