/** Third party */
import React from 'react';
/** Utils */
import fetch from 'utils/fetch_wrapper';
/** Global */
import Wrapper from 'components/login/Wrapper';
/** Relative */
import GetAuthDataForm from '../../components/GetAuthDataForm';
import SuccessMessage from '../../components/SuccessMessage';

/**
 * Страница Получения логина/пароля
 */
class GetLoginPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            inn: '',
            phone: '',
            showCodeInput: false,
            docId: '',
            confirmType: '',
            confirmCode: '',
            isFetching: false,
            error: '',
            showSuccessMessage: false,
        };
    }

    handleInputChange = (e) => {
        const id = e.target.getAttribute('id');
        this.setState({
            [id]: e.target.value,
            error: '',
        });
    }

    /**
     * Обработчик подтверждения формы
     * @param {Event} e
     */
    handleFormSubmit = (e) => {
        e.preventDefault();
        if (
            this.state.inn.length !== 5 &&
            this.state.inn.length !== 10 &&
            this.state.inn.length !== 12
        ) {
            this.setState({
                error: 'Укажите корректный ИНН',
            });
            return;
        }

        if (!/\(\d{3}\) \d{3}-\d{2}-\d{2}/.test(this.state.phone)) {
            this.setState({
                error: 'Укажите корректный номер телефона',
            });
            return;
        }

        this.setState({
            isFetching: true,
        });
        if (this.state.showCodeInput) {
            this.makeConfirmSubmitRegisterRequest().then(() => {
                this.setState({
                    isFetching: false,
                });
            });
        } else {
            this.makeSubmitRegisterRequest().then(() => {
                this.setState({
                    isFetching: false,
                });
            });
        }
    }

    makeSubmitRegisterRequest = () => {
        return fetch('/api/v1/confirm_register_ul', {
            method: 'POST',
            body: JSON.stringify({
                inn: this.state.inn,
                phone: `7${this.state.phone.replace(/[+()-\s]/g, '')}`,
            }),
        })
            .then(response => response.json())
            .then((json) => {
                console.log(json);
                if (json && !json.errorText) {
                    this.setState({
                        docId: json.docId,
                        showCodeInput: true,
                        confirmType: json.confirmData.type,
                    });
                } else {
                    console.log(json.errorText);
                    this.setState({
                        error: json.errorText || 'Произошла ошибка при отправке данных',
                    });
                }
            })
            .catch(() => {
                this.setState({
                    error: 'Произошла ошибка при отправке данных',
                });
            });
    }

    makeConfirmSubmitRegisterRequest = () => {
        return fetch('/api/v1/confirm_register_ul/confirm', {
            method: 'POST',
            body: JSON.stringify({
                docId: this.state.docId,
                confirmType: this.state.confirmType,
                confirmCode: this.state.confirmCode,
            }),
        })
            .then(response => response.json())
            .then((json) => {
                console.log(json);
                if (json && !json.errorText) {
                    if (json.changeResult === 'false') {
                        return Promise.reject({
                            errorText: json.confirmResult.message,
                        });
                    }
                    this.setState({
                        showSuccessMessage: true,
                        isFetching: false,
                    });
                } else {
                    console.log(json.errorText);
                    return Promise.reject({
                        errorText: json.errorText,
                    });
                }
            })
            .catch((err) => {
                this.setState({
                    error: err.errorText || 'Произошла ошибка при отправке данных',
                    isFetching: false,
                });
            });
    }

    render() {
        const content = this.state.showSuccessMessage
            ? (
                <SuccessMessage />
            )
            : (
                <GetAuthDataForm
                    handleInputChange={this.handleInputChange}
                    handleFormSubmit={this.handleFormSubmit}
                    values={this.state}
                />
            );

        return (
            <Wrapper
                isFetching={this.state.isFetching}
                style={{ minHeight: this.state.showCodeInput ? 800 : 700 }}
            >
                {content}
            </Wrapper>
        );
    }
}

GetLoginPage.contextTypes = {
    router: React.PropTypes.object.isRequired,
};

export default GetLoginPage;
