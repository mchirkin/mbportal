import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.css';

const SessionExpired = ({ closeMessage }) => (
    <div className={styles['modal-wrapper']}>
        <div className={styles.modal}>
            <div className={styles['form-header']}>
                <div className={styles['reb-logo']} />
                <div className={styles['product-name']}>
                    Интернет-банк для бизнеса
                </div>
            </div>
            <h3 className={styles['modal-header']}>
                Вас долго не было и мы завершили сеанс, чтобы интернет-банком
                 не воспользовались посторонние. Для продолжения работы введите логин и пароль еще раз.
            </h3>
            <div
                className={styles['get-login-btn']}
                onClick={closeMessage}
            >
                Авторизоваться
            </div>
        </div>
    </div>
);

SessionExpired.propTypes = {
    closeMessage: PropTypes.func.isRequired,
};

export default SessionExpired;
