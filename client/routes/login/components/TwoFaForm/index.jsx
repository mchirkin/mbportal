import React, { Component } from 'react';
import PropTypes from 'prop-types';
import IconedInput from 'components/ui/IconedInput';

import styles from './styles.css';

export default class TwoFaForm extends Component {
    static propTypes = {
        /** state контейнера с данными для формы */
        values: PropTypes.object.isRequired,
        /** обработчик подтверждения формы */
        handleFormSubmit: PropTypes.func.isRequired,
        /** обработчик изменения поля "Код" */
        handleCodeChange: PropTypes.func.isRequired,
        /** повторная отправка смс-кода */
        resendTwoFA: PropTypes.func.isRequired,
    };

    constructor(props) {
        super(props);

        this.state = {
            secondsLeft: 30,
        };
    }

    componentDidMount() {
        this.startCount();
    }

    startCount = () => {
        this.setState({
            secondsLeft: 30,
        }, () => {
            const interval = setInterval(() => {
                const secondsLeft = this.state.secondsLeft - 1;

                this.setState({
                    secondsLeft,
                });

                if (secondsLeft === 0) {
                    clearInterval(interval);
                }
            }, 1000);
        });
    }

    handleResendClick = () => {
        this.props.resendTwoFA();
        this.startCount();
    }

    render() {
        const props = this.props;

        return (
            <div className={styles['code-form']}>
                <div className={styles['form-wrapper']}>
                    <div className={styles['form-header']}>
                        <div className={styles['reb-logo']} />
                        <div className={styles['product-name']}>
                            Интернет-банк для бизнеса
                        </div>
                    </div>
                    <form onSubmit={props.handleFormSubmit} autoComplete="off">
                        <div
                            style={{
                                marginBottom: 10,
                                fontSize: 13,
                            }}
                        >
                            Вам отправлен код для подтверждения входа
                        </div>
                        <IconedInput
                            className={styles['form-input']}
                            icon={require('./img/lock.png')}
                            iconBackgroundColor={props.values.error ? '#d75342' : undefined}
                            inputProps={{
                                id: '2facode',
                                type: 'text',
                                onChange: props.handleCodeChange,
                                value: props.values.twoFaCode,
                                placeholder: 'Введите код из SMS',
                                tabIndex: 2,
                            }}
                            errorMsg={props.values.errorMsg}
                        />
                        {this.state.secondsLeft > 0
                            ? (
                                <div className={styles['resend-count']}>
                                    Повторная отправка кода будет доступна через {this.state.secondsLeft} сек.
                                </div>
                            )
                            : (
                                <div
                                    className={styles.resend}
                                    onClick={this.handleResendClick}
                                >
                                    Выслать повторно
                                </div>
                            )}
                        <button className={styles['form-submit']}>
                            Отправить
                        </button>
                    </form>
                </div>
            </div>
        );
    }
}
