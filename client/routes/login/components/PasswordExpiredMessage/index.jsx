import React from 'react';
import PropTypes from 'prop-types';
import styles from './login.css';

const PasswordExpiredMessage = ({ hideMessage }) => (
    <div className={styles.wrapper}>
        <div className={styles.message}>
            <div className={styles['message-header']}>
                    Внимание!
            </div>
            <div className={styles['message-body']}>
                    Действие временного пароля истекло. <br />
                    Необходимо сменить временный пароль на новый
            </div>
        </div>
        <div className={styles['change-password-btn']} onClick={hideMessage}>
                Сменить пароль
        </div>
    </div>
);

PasswordExpiredMessage.propTypes = {
    /** закрыть сообщение */
    hideMessage: PropTypes.func.isRequired,
};

export default PasswordExpiredMessage;
