import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import IconedInput from 'components/ui/IconedInput';
import Recaptcha from 'components/Recaptcha';
import styles from './styles.css';

const LoginForm = (props) => {
    const changePasswordConfirmed = (
        props.values.newPassword.length > 0 &&
        props.values.confirmPassword.length > 0 &&
        props.values.newPassword === props.values.confirmPassword
    );

    const passwordRestrictionsError = props.values.passwordRestrictionsError;

    return (
        <div className={styles['login-form']}>
            <div className={styles['form-wrapper']}>
                <div className={styles['form-header']}>
                    <div className={styles['reb-logo']} />
                    <div className={styles['product-name']}>
                        Интернет-банк для бизнеса
                    </div>
                </div>
                <form className={styles['login-form']} onSubmit={props.handleFormSubmit} autoComplete="off">
                    <div
                        style={{
                            width: 0,
                            height: 0,
                            overflow: 'hidden',
                        }}
                    >
                        <input
                            type="text"
                            style={{
                                width: 0,
                                height: 0,
                                overflow: 'hidden',
                                border: 0,
                            }}
                        />
                        {/*
                        <input
                            type="password"
                            style={{
                                width: 0,
                                height: 0,
                                overflow: 'hidden',
                                border: 0,
                            }}
                        />
                        */}
                    </div>
                    <IconedInput
                        className={styles['form-input']}
                        icon={require('./img/man.svg')}
                        iconBackgroundColor={props.values.error ? '#d75342' : undefined}
                        inputProps={{
                            id: 'username',
                            onChange: props.handleUsernameChange,
                            value: props.values.username,
                            placeholder: 'Логин',
                            tabIndex: 1,
                        }}
                    />
                    <IconedInput
                        className={styles['form-input']}
                        icon={require('./img/lock.png')}
                        iconBackgroundColor={props.values.error ? '#d75342' : undefined}
                        inputProps={{
                            id: 'password',
                            type: 'password',
                            onChange: props.handlePasswordChange,
                            value: props.values.password,
                            placeholder: 'Пароль',
                            tabIndex: 2,
                        }}
                        errorMsg={props.values.errorMsg}
                    />
                    {
                        props.values.passwordExpired
                            ? (
                                <div>
                                    <IconedInput
                                        className={styles['form-input']}
                                        iconBackgroundColor={(
                                            props.values.newPassword.length > 0 && !passwordRestrictionsError
                                                ? '#0dbe82'
                                                : '#d75342'
                                        )}
                                        inputProps={{
                                            id: 'newPassword',
                                            type: 'password',
                                            onChange: props.handleNewPasswordChange,
                                            value: props.values.newPassword,
                                            placeholder: 'Новый пароль',
                                        }}
                                    />
                                    <IconedInput
                                        className={styles['form-input']}
                                        iconBackgroundColor={(
                                            changePasswordConfirmed && !passwordRestrictionsError
                                                ? '#0dbe82'
                                                : '#d75342'
                                        )}
                                        inputProps={{
                                            id: 'confirmPassword',
                                            type: 'password',
                                            onChange: props.handleConfirmPasswordChange,
                                            value: props.values.confirmPassword,
                                            placeholder: 'Повторить пароль',
                                        }}
                                        errorMsg={passwordRestrictionsError}
                                    />
                                </div>
                            )
                            : null
                    }

                    {/*
                    <div className={styles['captcha-wrapper']} data-visible={props.values.showCaptcha}>
                        <Recaptcha
                            sitekey="6LcCeE0UAAAAAKb4i3c-eRK1pgAV0ytt42zEplmp"
                            verifyCallback={props.recaptchaVerifyCallback}
                        />
                    </div>
                    */}

                    {/* <div className={styles['captcha-wrapper']} data-visible={props.values.showCaptcha}>
                        <div id="captcha-img" className={styles['captcha-img']} />
                        <IconedInput
                            className={styles['form-input']}
                            icon={require('./img/lock.png')}
                            noIcon
                            inputProps={{
                                id: 'captchaCode',
                                type: 'text',
                                onChange: props.handleCaptchaCodeChange,
                                value: props.values.captchaText,
                                placeholder: 'Код с картинки',
                            }}
                            errorMsg={props.values.captchaError}
                        />
                    </div> */}

                    <button className={styles['form-submit']} onClick={props.handleFormSubmit}>
                        {props.values.passwordExpired ? 'Изменить пароль' : 'Войти'}
                    </button>
                </form>
                <div className={styles['link-list']}>
                    <Link className={styles['get-login-link']} to="get_login">Получить логин и пароль</Link>
                </div>
            </div>
        </div>
    );
};

LoginForm.propTypes = {
    /** state контейнера с данными для формы */
    values: PropTypes.object.isRequired,
    /** обработчик подтверждения формы */
    handleFormSubmit: PropTypes.func.isRequired,
    /** обработчик изменения поля "Логин" */
    handleUsernameChange: PropTypes.func.isRequired,
    /** обработчик изменения поля "Пароль" */
    handlePasswordChange: PropTypes.func.isRequired,
    /** обработчик изменения поля "Новый пароль" */
    handleNewPasswordChange: PropTypes.func.isRequired,
    /** обработчик изменения поля "Подтвердите пароль" */
    handleConfirmPasswordChange: PropTypes.func.isRequired,
    /** Получение данных для проверки каптчи */
    recaptchaVerifyCallback: PropTypes.func.isRequired,
};

export default LoginForm;
