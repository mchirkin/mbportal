import React from 'react';
import { Link } from 'react-router-dom';
import styles from './styles.css';

const PasswordChangedMessage = () => (
    <div className={styles['modal-wrapper']}>
        <div className={styles.modal}>
            <div className={styles['form-header']}>
                <div className={styles['reb-logo']} />
                <div className={styles['product-name']}>
                    Интернет-банк для бизнеса
                </div>
            </div>
            <h3 className={styles['modal-header']}>
                Ваш пароль успешно изменен!
            </h3>
            <Link className={styles['get-login']} to="home">
                <div className={styles['get-login-btn']}>
                    Перейти в Интернет-банк
                </div>
            </Link>
        </div>
    </div>
);

export default PasswordChangedMessage;
