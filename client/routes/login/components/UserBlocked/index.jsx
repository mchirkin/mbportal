import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import styles from './styles.css';

const UserBlocked = ({ error }) => {
    const getLoginEnable = error.blockLevel && parseInt(error.blockLevel, 10) === 1;

    return (
        <div className={styles['modal-wrapper']}>
            <div className={styles.modal}>
                <div className={styles['form-header']}>
                    <div className={styles['reb-logo']} />
                    <div className={styles['product-name']}>
                        Интернет-банк для бизнеса
                    </div>
                </div>
                <h3 className={styles['modal-header']}>
                    {error.blockLevel && parseInt(error.blockLevel, 10) === 1
                        ? (
                            <div>
                                Ваша учетная запись временно заблокирована.
                                 Повторная попытка входа возможна через 15 минут.
                                 Если Вы забыли пароль, воспользуйтесь сервисом восстановления пароля.
                            </div>
                        )
                        : (
                            <div>
                                Ваша учетная запись заблокирована.
                            </div>
                        )}
                </h3>
                {getLoginEnable
                    ? (
                        <Link className={styles['get-login']} to="get_login">
                            <div className={styles['get-login-btn']}>
                                Восстановить логин и пароль
                            </div>
                        </Link>
                    )
                    : (
                        <div className={styles.message}>
                            <div>
                                Для разблокировки обратитесь в Банк по телефону
                            </div>
                            <div>
                                <strong>8 (800) 500-77-78 или 8 (495) 777 1111</strong>
                            </div>
                        </div>
                    )}
            </div>
        </div>
    );
};

UserBlocked.propTypes = {
    /** информация об ошибке */
    error: PropTypes.object,
};

UserBlocked.defaultProps = {
    error: {},
};

export default UserBlocked;
