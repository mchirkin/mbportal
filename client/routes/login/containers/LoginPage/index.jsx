import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import fetch from 'utils/fetch_wrapper';

import showModal from 'modules/modal/actions/show_modal';
import fetchPasswordRestrictions from 'modules/settings/actions/password_restrictions/fetch';

import Wrapper from 'components/login/Wrapper';

import setPopupNotificationData from 'modules/popup_notification/actions/set_data';
import PopupNotification from 'components/PopupNotification';

import LoginForm from '../../components/LoginForm';
import PasswordExpiredMessage from '../../components/PasswordExpiredMessage';
import PasswordChangedMessage from '../../components/PasswordChangedMessage';
import UserBlocked from '../../components/UserBlocked';
import SessionExpired from '../../components/SessionExpired';
import TwoFaForm from '../../components/TwoFaForm';

/**
 * Страница Авторизации
 */
class LoginPage extends Component {
    static contextTypes = {
        router: PropTypes.object.isRequired,
    };

    static propTypes = {
        passwordRestrictions: PropTypes.object,
        fetchPasswordRestrictions: PropTypes.func,
        location: PropTypes.object,
        setPopupNotificationData: PropTypes.func.isRequired,
    };

    static defaultProps = {
        passwordRestrictions: {},
        fetchPasswordRestrictions: undefined,
        location: {},
    };

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            newPassword: '',
            confirmPassword: '',
            error: false,
            errorMsg: '',
            passwordExpired: false,
            showPasswordExpiredMessage: false,
            passwordRestrictionsError: '',
            showPasswordChangedMessage: false,
            attemptsNum: 0,
            isFetching: false,
            captchaText: '',
            captchaError: '',
            showSessionExpired: false,
            showCaptcha: false,
            responseCaptcha: '',
            twoFaCode: '',
        };
    }

    componentWillMount() {
        if (window.showSessionExpired) {
            this.setState({
                showSessionExpired: true,
            });
            window.showSessionExpired = false;
        }
    }

    componentDidMount() {
        localStorage.setItem('canChangeDefaultTarif', 'true');
        localStorage.setItem('canChangeTarif', 'true');
    }

    /**
     * Проверить новый пароль на ограничения
     * @return {boolean}
     */
    checkNewPassword = () => {
        if (!this.props.passwordRestrictions.data) {
            return true;
        }

        if (this.state.newPassword !== this.state.confirmPassword) {
            this.setState({
                passwordRestrictionsError: 'Введенные пароли не совпадают',
            });
            return false;
        }

        const minPassLength = parseInt(this.props.passwordRestrictions.data.passwordMinLength, 10);
        if (this.state.newPassword.length < minPassLength) {
            this.setState({
                passwordRestrictionsError: `Длина пароля менее ${minPassLength} символов.`,
            });
            return false;
        }
        if (this.props.passwordRestrictions.data.usePasswordDifferentRegister === 'true') {
            if (
                this.state.newPassword === this.state.newPassword.toLowerCase() ||
                this.state.newPassword === this.state.newPassword.toUpperCase() ||
                !/[А-Яа-яA-Za-z]/.test(this.state.newPassword)
            ) {
                this.setState({
                    passwordRestrictionsError: 'В пароле должны использоваться строчные и прописные буквы.',
                });
                return false;
            }
        }
        if (this.props.passwordRestrictions.data.usePasswordDigits === 'true') {
            if (!/\d/.test(this.state.newPassword)) {
                this.setState({
                    passwordRestrictionsError: 'В пароле должна присутствовать хотя бы одна цифра.',
                });
                return false;
            }
        }
        if (this.state.newPassword === this.state.password) {
            this.setState({
                passwordRestrictionsError: 'Новый пароль совпадает с текущим.',
            });
            return false;
        }
        if (/[А-Яа-я]/.test(this.state.newPassword)) {
            this.setState({
                passwordRestrictionsError: 'В пароле должны отсутствовать символы кириллицы.',
            });
            return false;
        }

        return true;
    };

    /**
     * Смена пароля пользователя
     */
    changeUserPassword = () => {
        this.setState({
            isFetching: true,
        });
        if (this.checkNewPassword()) {
            fetch('/api/v1/change_password', {
                method: 'POST',
                credentials: 'include',
                body: JSON.stringify({
                    currentPassword: this.state.password,
                    newPassword: this.state.newPassword,
                    newSalt: this.props.passwordRestrictions.data.saltPassword,
                }),
            })
                .then(response => {
                    if (response.status === 200) {
                        // при 200 статусе тело ответа пустое
                        return {};
                    }
                    return response.json();
                })
                .then(json => {
                    if (json.errorCode) {
                        this.setState({
                            error: json,
                            isFetching: false,
                        });
                    } else {
                        this.setState({
                            isFetching: false,
                            showPasswordChangedMessage: true,
                        });
                    }
                })
                .catch(error => {
                    this.setState({
                        isFetching: false,
                    });
                    console.log('error', error);
                });
        } else {
            this.setState({
                isFetching: false,
            });
        }
    };

    /**
     * Обработчик подтверждения формы
     * @param {Event} e
     */
    handleLoginFormSubmit = e => {
        e.preventDefault();
        this.setState({
            attemptsNum: this.state.attemptsNum + 1,
            isFetching: true,
        });

        if (this.state.passwordExpired) {
            this.changeUserPassword();
        } else {
            fetch('/api/v1/login', {
                method: 'POST',
                credentials: 'include',
                body: JSON.stringify({
                    username: this.state.username,
                    password: this.state.password,
                    responseCaptcha: this.state.responseCaptcha,
                }),
            })
                .then(response => {
                    /*
                if (!isServerInLAN) {
                    grecaptcha.reset();
                }
                */

                    /**
                     * При 200 статусе приходит либо пустой ответ при удачной авторизации
                     * либо JSON { passwordExpired: true }
                     */
                    switch (response.status) {
                        case 200:
                            return response.text();
                        case 401:
                            return response.json();
                        case 461:
                            return response.json();
                        default:
                            return response.json();
                    }
                })
                .then(res => {
                    if (/passwordExpired/.test(res)) {
                        const json = JSON.parse(res);
                        if (json && json.csrfToken) {
                            localStorage.setItem('login', this.state.username);
                            localStorage.setItem('csrfToken', json.csrfToken);
                        }
                        this.props.fetchPasswordRestrictions();
                        this.setState({
                            passwordExpired: true,
                            showPasswordExpiredMessage: true,
                            isFetching: false,
                            showCaptcha: false,
                            captchaResponse: '',
                        });
                        return;
                    }

                    console.log(res);

                    if (res.errorCode || res.sessionExpired) {
                        let leftAttempts = '';
                        if (res.leftAttempts && res.leftAttempts !== '0') {
                            const leftAttemptsValue = parseInt(res.leftAttempts, 10);
                            if (leftAttemptsValue === 1) {
                                leftAttempts = '\r\nОсталось 1 попытка.';
                            } else {
                                leftAttempts = `\r\nОсталось ${leftAttemptsValue} попытки.`;
                            }
                        }
                        this.setState({
                            error: res,
                            errorMsg: `Вы ввели неправильный логин/пароль.${leftAttempts}`,
                            isFetching: false,
                        });
                    } else if (res.twoFa) {
                        localStorage.setItem('login', this.state.username);
                        localStorage.setItem('csrfToken', res.csrfToken);

                        this.setState({
                            isFetching: false,
                            showCaptcha: false,
                            twoFa: true,
                            captchaResponse: '',
                        });
                    } else if (res.needCaptcha) {
                        this.setState({
                            isFetching: false,
                            showCaptcha: true,
                        });
                    } else {
                        const json = JSON.parse(res);
                        if (json && json.csrfToken) {
                            localStorage.setItem('login', this.state.username);
                            localStorage.setItem('csrfToken', json.csrfToken);
                        }
                        this.context.router.history.push('/home');
                    }
                })
                .catch(err => {
                    console.log('err', err);
                    this.setState({
                        isFetching: false,
                    });
                });
        }
    };

    handleTwoFaFormSubmit = e => {
        e.preventDefault();

        fetch('/api/v1/two_fa', {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify({
                key: this.state.twoFaCode,
            }),
        })
            .then(response => {
                switch (response.status) {
                    case 200:
                        return response.text();
                    case 401:
                        return response.json();
                    case 461:
                        return response.json();
                    default:
                        return response.json();
                }
            })
            .then(res => {
                if (typeof res === 'string' && res.length === 0) {
                    this.context.router.history.push('/home');
                    return;
                }

                let leftAttempts = '';
                if (res.leftAttempts && res.leftAttempts !== '0') {
                    const leftAttemptsValue = parseInt(res.leftAttempts, 10);
                    if (leftAttemptsValue === 1) {
                        leftAttempts = '\r\nОсталось 1 попытка.';
                    } else {
                        leftAttempts = `\r\nОсталось ${leftAttemptsValue} попытки.`;
                    }
                }

                // Для компонента UserBlock, для отображения кнопки восстановления пароля
                res.blockLevel = '1';

                this.setState({
                    error: res,
                    errorMsg: `Вы ввели неправильный смс код авторизации.${leftAttempts}`,
                    isFetching: false,
                });
            })
            .catch(err => {
                console.log('err', err);
                this.setState({
                    isFetching: false,
                });
            });
    };

    recaptchaVerifyCallback = responseCaptcha => {
        this.setState({
            responseCaptcha,
        });
    };

    /**
     * Обработчик изменения поля "Логин"
     * @param {Event} e
     */
    handleUsernameChange = e => {
        this.setState({
            username: e.target.value,
        });
        // сбрасываем ошибки при изменении поля
        this.resetErrors();
    };

    /**
     * Обработчик изменения поля "Пароль"
     * @param {Event} e
     */
    handlePasswordChange = e => {
        this.setState({
            password: e.target.value,
        });
        this.resetErrors();
    };

    /**
     * Обработчик изменения поля "Код"
     * @param {Event} e
     */
    handleTwoFaCodeChange = e => {
        this.setState({
            twoFaCode: e.target.value,
        });
        this.resetErrors();
    };

    /**
     * Обработчик изменения поля "Новый пароль"
     * @param {Event} e
     */
    handleNewPasswordChange = e => {
        this.setState({
            newPassword: e.target.value,
            passwordRestrictionsError: '',
        });
    };

    /**
     * Обработчик изменения поля "Подтвердите пароль"
     * @param {Event} e
     */
    handleConfirmPasswordChange = e => {
        this.setState({
            confirmPassword: e.target.value,
            passwordRestrictionsError: '',
        });
    };

    /**
     * Сброс ошибок
     */
    resetErrors = () => {
        if (this.state.error) {
            this.setState({
                error: null,
                errorMsg: '',
                captchaError: '',
            });
        }
    };

    hidePasswordExpiredMessage = () => {
        this.setState({
            showPasswordExpiredMessage: false,
        });
    };

    getCaptcha = () => {
        fetch('/api/v1/captcha/captcha')
            .then(response => {
                console.log(response.headers, response.headers.get('x-captcha-id'));
                this.captchaId = response.headers.get('x-captcha-id');
                return response.text();
            })
            .then(data => {
                document.querySelector('#captcha-img').innerHTML = data;
                this.setState({
                    showCaptcha: true,
                    isFetching: false,
                    captchaText: '',
                });
            });
    };

    handleCaptchaCodeChange = e => {
        this.setState({
            captchaText: e.target.value,
            error: null,
            errorMsg: '',
            captchaError: '',
        });
    };

    closeSessionExpiredMessage = () => {
        this.setState({
            showSessionExpired: false,
        });
    };

    resendTwoFA = () => {
        this.setState({
            isFetching: true,
        });
        fetch('/api/v1/two_fa/resend')
            .then(response => response.text())
            .then(() => {
                this.setState({
                    isFetching: false,
                });
            })
            .catch(err => {
                console.log(err);
                this.setState({
                    isFetching: false,
                });
            });
    };

    render() {
        const userBlocked =
            this.state.error !== null &&
            (parseInt(this.state.error.errorCode, 10) === 1005 || this.state.error.userBlocked === true);

        let formContent = this.state.showPasswordExpiredMessage ? (
            <PasswordExpiredMessage hideMessage={this.hidePasswordExpiredMessage} />
        ) : (
            <LoginForm
                handleUsernameChange={this.handleUsernameChange}
                handlePasswordChange={this.handlePasswordChange}
                handleNewPasswordChange={this.handleNewPasswordChange}
                handleConfirmPasswordChange={this.handleConfirmPasswordChange}
                handleCaptchaCodeChange={this.handleCaptchaCodeChange}
                handleFormSubmit={this.handleLoginFormSubmit}
                recaptchaVerifyCallback={this.recaptchaVerifyCallback}
                values={this.state}
            />
        );
        if (this.state.showPasswordChangedMessage) {
            formContent = <PasswordChangedMessage />;
        }

        if (this.state.twoFa) {
            formContent = (
                <TwoFaForm
                    handleCodeChange={this.handleTwoFaCodeChange}
                    handleFormSubmit={this.handleTwoFaFormSubmit}
                    resendTwoFA={this.resendTwoFA}
                    values={this.state}
                />
            );
        }

        if (this.state.showSessionExpired) {
            return (
                <Wrapper>
                    <SessionExpired closeMessage={this.closeSessionExpiredMessage} />
                </Wrapper>
            );
        }

        return (
            <Wrapper
                isFetching={this.state.isFetching}
                large={(this.state.passwordExpired && !this.state.showPasswordExpiredMessage) || this.state.showCaptcha}
            >
                {userBlocked ? <UserBlocked error={this.state.error} /> : formContent}
                <PopupNotification />
            </Wrapper>
        );
    }
}

function mapStateToProps(state) {
    return {
        passwordRestrictions: state.settings.passwordRestrictions,
    };
}

const mapDispatchToProps = dispatch => ({
    showModal: bindActionCreators(showModal, dispatch),
    fetchPasswordRestrictions: bindActionCreators(fetchPasswordRestrictions, dispatch),
    setPopupNotificationData: bindActionCreators(setPopupNotificationData, dispatch),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(LoginPage);
