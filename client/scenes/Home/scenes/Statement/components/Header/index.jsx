import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { DayPickerRangeController } from 'react-dates';
import moment from 'moment';
import SearchInput from 'components/ui/SearchInput';
import CalendarBottom from 'components/CalendarBottom';
import styles from './header.css';

moment.locale('ru');

export default class Header extends Component {
    static propTypes = {
        initialStartDate: PropTypes.object,
        initialEndDate: PropTypes.object,
        clearDateFilter: PropTypes.func.isRequired,
        acceptDateFilter: PropTypes.func.isRequired,
        handleSearchChange: PropTypes.func.isRequired,
        search: PropTypes.string,
        showPrint: PropTypes.string,
        saveXLSX: PropTypes.func.isRequired,
        savePDF: PropTypes.func.isRequired,
        saveDOCX: PropTypes.func.isRequired,
        save1C: PropTypes.func.isRequired,
    }

    static defaultProps = {
        initialStartDate: null,
        initialEndDate: null,
        search: '',
        showPrint: '',
    }

    constructor(props) {
        super(props);

        this.state = {
            startDate: null,
            endDate: null,
            focusedInput: 'startDate',
            showCalendar: false,
        };
    }

    componentWillReceiveProps(nextProps) {
        if (!this.state.startDate && nextProps.initialStartDate) {
            this.setState({
                startDate: nextProps.initialStartDate,
            });
        }
        if (!this.state.endDate && nextProps.initialEndDate) {
            this.setState({
                endDate: nextProps.initialEndDate,
            });
        }
    }

    onFocusChange = (focusedInput) => {
        this.setState({
            // Force the focusedInput to always be truthy so that dates are always selectable
            focusedInput: !focusedInput ? 'startDate' : focusedInput,
        });
    }

    showCalendar = (e) => {
        const id = e.target.getAttribute('id');
        this.setState({
            focusedInput: id,
            showCalendar: true,
        });
    }

    handleDatesChange = ({ startDate, endDate }) => {
        this.setState({
            startDate,
            endDate,
        });
    }

    handlePeriodSet = (firstDate, secondDate) => {
        this.setState({
            startDate: firstDate,
            endDate: secondDate,
            showCalendar: false,
        });

        this.props.acceptDateFilter({
            startDate: firstDate,
            endDate: secondDate,
        });
    }

    handlePeriodClear = () => {
        this.setState({
            startDate: this.props.initialStartDate,
            endDate: this.props.initialEndDate,
            showCalendar: false,
        });
        this.props.clearDateFilter();
    }

    handlePeriodSubmit = () => {
        this.setState({
            showCalendar: false,
        });
        this.props.acceptDateFilter({
            startDate: this.state.startDate,
            endDate: this.state.endDate,
        });
    }

    render() {
        const { startDate, endDate } = this.state;

        const startDateString = startDate && startDate.format('DD.MM.YYYY');
        const endDateString = endDate && endDate.format('DD.MM.YYYY');

        return (
            <div className={styles.header}>
                <div className={styles['header-name']}>
                    Выписка
                </div>
                <div className={styles.row}>
                    <div className={styles.filter}>
                        <div className={styles.search}>
                            <SearchInput
                                value={this.props.search || ''}
                                onChange={this.props.handleSearchChange}
                            />
                        </div>
                        <div className={styles.period}>
                            <span style={{ marginRight: 10 }}>
                                Период с
                            </span>
                            <input
                                id="startDate"
                                className={styles['period-input']}
                                type="text"
                                name="start date"
                                value={startDateString || ''}
                                readOnly
                                onClick={this.showCalendar}
                            />
                            <div
                                id="startDate"
                                className={styles.calendar}
                                onClick={this.showCalendar}
                            />
                            <span style={{ marginLeft: 10, marginRight: 10 }}>
                                по
                            </span>
                            <input
                                id="endDate"
                                className={styles['period-input']}
                                type="text"
                                name="end date"
                                value={endDateString || ''}
                                readOnly
                                onClick={this.showCalendar}
                            />
                            <div
                                id="endDate"
                                className={styles.calendar}
                                onClick={this.showCalendar}
                            />
                            {this.state.showCalendar
                                ? (
                                    <div className={styles.daypicker}>
                                        <DayPickerRangeController
                                            startDate={this.state.startDate}
                                            endDate={this.state.endDate}
                                            focusedInput={this.state.focusedInput}
                                            onFocusChange={this.onFocusChange}
                                            onDatesChange={this.handleDatesChange}
                                            firstDayOfWeek={1}
                                            isOutsideRange={() => false}
                                            numberOfMonths={2}
                                            minimumNights={0}
                                            daySize={35}
                                            onOutsideClick={() => this.setState({ showCalendar: false })}
                                            renderCalendarInfo={() => (
                                                <CalendarBottom
                                                    onSubmit={this.handlePeriodSubmit}
                                                    onCancel={this.handlePeriodClear}
                                                    onSetPeriod={this.handlePeriodSet}
                                                />
                                            )}
                                            hideKeyboardShortcutsPanel
                                        />
                                    </div>
                                )
                                : null}
                        </div>
                    </div>
                    {this.props.showPrint
                        ? (
                            <div className={styles.print}>
                                <div>
                                    Скачать:
                                </div>
                                <div className={styles['print-btn']} onClick={this.props.saveXLSX}>
                                    .XLSX
                                </div>
                                <div className={styles['print-btn']} onClick={this.props.savePDF}>
                                    .PDF
                                </div>
                                <div className={styles['print-btn']} onClick={this.props.saveDOCX}>
                                    .DOCX
                                </div>
                                <div className={styles['print-btn']} onClick={this.props.save1C}>
                                    1C
                                </div>
                            </div>
                        )
                        : null}
                </div>
            </div>
        );
    }
}
