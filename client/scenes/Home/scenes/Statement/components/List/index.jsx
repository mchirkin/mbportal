import React from 'react';
import PropTypes from 'prop-types';
import Item from '../Item';
import styles from './list.css';

const List = ({ list, search }) => {
    if (list === null) {
        return null;
    }

    const datesObject = {};

    list
        .filter((item) => {
            if (!search) return true;

            const searchLine = search.toLowerCase();

            if (item.bank_name.toLowerCase().includes(searchLine)) return true;

            const inn = item.line_is_debet === '0' ? item.inn : item.corr_inn;
            if (inn.includes(searchLine)) return true;

            const account = item.line_is_debet === '0' ? item.acc_number : item.corr_acc_number;
            if (account.includes(searchLine)) return true;

            const bik = item.line_is_debet === '0' ? item.bank_bik : item.corr_bank_bik;
            if (bik.includes(searchLine)) return true;

            const contragent = item.line_is_debet === '0' ? item.fullname : item.corr_fullname;
            if (contragent.toLowerCase().includes(searchLine)) return true;

            if (item.line_description.toLowerCase().includes(searchLine)) return true;

            return false;
        })
        .forEach((item) => {
            if (!datesObject[item.doc_date]) {
                datesObject[item.doc_date] = [item];
            } else {
                datesObject[item.doc_date].push(item);
            }
        });

    const dates = Object.keys(datesObject).sort((a, b) => (new Date(b)).getTime() - (new Date(a)).getTime());

    return (
        <div>
            {dates && dates.length > 0
                ? dates.map(date => (
                    <div key={date}>
                        <div className={styles['date-line']}>
                            {date.replace(/(\d+)-(\d+)-(\d+)/, '$3.$2.$1')}
                        </div>
                        {datesObject[date].map(item => (
                            <Item key={item.id} data={item} />
                        ))}
                    </div>
                ))
                : (
                    <h3 style={{ marginLeft: 28, marginTop: 20 }}>Нет операций за указанный период</h3>
                )}
        </div>
    );
};

List.propTypes = {
    list: PropTypes.array,
    search: PropTypes.string,
};

List.defaultProps = {
    list: [],
    search: '',
};

export default List;
