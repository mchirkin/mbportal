import React from 'react';
import PropTypes from 'prop-types';
import { formatNumber } from 'utils';
import styles from './item.css';

const Item = ({ data }) => (
    <div className={styles.wrapper}>
        <div className={styles['payment-info']} style={{ maxWidth: 160 }}>
            <div>
                № {data.doc_number}
            </div>
            <div>
                Тип операции {data.line_operation_kind}
            </div>
        </div>
        <div className={styles['payment-info']} style={{ flexGrow: 1 }}>
            <div className={styles.requisites}>
                <div>
                    БАНК: {data.bank_name}
                </div>
                <div>
                    ИНН: {data.line_is_debet === '0' ? data.inn : data.corr_inn}
                </div>
                <div>
                    Р/С: {data.line_is_debet === '0' ? data.acc_number : data.corr_acc_number}
                </div>
                <div>
                    БИК: {data.line_is_debet === '0' ? data.bank_bik : data.corr_bank_bik}
                </div>
            </div>
            <div className={styles['payment-data']}>
                <div className={styles['payment-contragent']}>
                    {data.line_is_debet === '0' ? data.fullname : data.corr_fullname}
                </div>
                <div className={styles['payment-desc']}>
                    {data.line_description}
                </div>
                <div className={styles['payment-amount']} data-plus={data.line_is_debet === '0'}>
                    {`${data.line_is_debet === '0' ? '+' : '-'} ${formatNumber(data.amount)}`}
                </div>
            </div>
        </div>
    </div>
);

Item.propTypes = {
    data: PropTypes.object,
};

Item.defaultProps = {
    data: {},
};

export default Item;
