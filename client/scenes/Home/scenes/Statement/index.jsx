import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment';

import { fetch, downloadBlob } from 'utils';

import showNotification from 'modules/notification/actions/show_notification';
import closeNotification from 'modules/notification/actions/close_notification';

import Loader from 'components/Loader';
import SceneWrapper from 'components/SceneWrapper';

import Header from './components/Header';
import List from './components/List';

const today = moment();
const yesterday = moment().subtract(1, 'days');
// const yesterday = moment('30.11.2017', 'DD.MM.YYYY');
// const today = moment('01.12.2017', 'DD.MM.YYYY');

class Statement extends Component {
    static propTypes = {
        selectedAccount: PropTypes.string,
        showNotification: PropTypes.func.isRequired,
        closeNotification: PropTypes.func.isRequired,
        notification: PropTypes.bool,
    }

    static defaultProps = {
        selectedAccount: '',
        notification: null,
    }

    constructor(props) {
        super(props);

        // const today = moment().format('DD.MM.YYYY');
        // const yesterday = moment().subtract(25, 'days').format('DD.MM.YYYY');

        this.state = {
            statement: null,
            isFetching: false,
            from: yesterday.format('DD.MM.YYYY'),
            to: today.format('DD.MM.YYYY'), // today,
            search: '',
        };
    }

    componentWillMount() {
        if (this.props.selectedAccount) {
            this.fetchStatement();
        }
    }

    async componentDidUpdate() {
        if (!this.props.selectedAccount || this.state.isFetching || this.state.statement !== null) {
            return;
        }

        console.log(1);

        this.fetchStatement();
    }

    fetchStatement = async () => {
        try {
            this.setState({
                isFetching: true,
            });

            const response = await fetch('/api/v1/statement/offline', {
                method: 'POST',
                credentials: 'include',
                body: JSON.stringify({
                    accountId: this.props.selectedAccount,
                    dateFrom: this.state.from,
                    dateTo: this.state.to,
                }),
            });

            const json = await response.json();

            if (json && json.statementAnswer) {
                this.setState({
                    statement: json.statementAnswer,
                    isFetching: false,
                });
            } else {
                this.setState({
                    statement: {},
                    isFetching: false,
                });
            }
        } catch (err) {
            global.console.log(err);
            this.setState({
                statement: {},
                isFetching: false,
            });
        }
    }

    acceptDateFilter = ({ startDate, endDate }) => {
        this.setState({
            from: startDate ? startDate.format('DD.MM.YYYY') : endDate.format('DD.MM.YYYY'),
            to: endDate ? endDate.format('DD.MM.YYYY') : startDate.format('DD.MM.YYYY'),
        }, () => {
            this.fetchStatement();
        });
    }

    clearDateFilter = () => {
        this.setState({
            from: yesterday.format('DD.MM.YYYY'),
            to: today.format('DD.MM.YYYY'),
        }, () => {
            this.fetchStatement();
        });
    }

    handleSearchChange = (e) => {
        this.setState({
            search: e.target.value,
        });
    }

    saveXLSX = () => {
        const notificationData = {
            text: 'Идет формирование документа...',
        };

        this.props.showNotification(notificationData);

        fetch(`/api/v1/statement/print/xls/${this.state.statement.id}`)
            .then(response => response.blob())
            .then((blob) => {
                if (this.props.notification === notificationData) {
                    this.props.closeNotification();
                }
                downloadBlob(blob, `${this.state.statement.id}.xlsx`);
            })
            .catch(() => {
                if (this.props.notification === notificationData) {
                    this.props.closeNotification();
                }
            });
    }

    savePDF = () => {
        const notificationData = {
            text: 'Идет формирование документа...',
        };

        this.props.showNotification(notificationData);

        fetch(`/api/v1/statement/print/${this.state.statement.id}`)
            .then(response => response.blob())
            .then((blob) => {
                if (this.props.notification === notificationData) {
                    this.props.closeNotification();
                }
                downloadBlob(blob, `${this.state.statement.id}.pdf`);
            })
            .catch(() => {
                if (this.props.notification === notificationData) {
                    this.props.closeNotification();
                }
            });
    }

    saveDOCX = () => {
        const notificationData = {
            text: 'Идет формирование документа...',
        };

        this.props.showNotification(notificationData);

        fetch(`/api/v1/statement/print/doc/${this.state.statement.id}`)
            .then(response => response.blob())
            .then((blob) => {
                if (this.props.notification === notificationData) {
                    this.props.closeNotification();
                }
                downloadBlob(blob, `${this.state.statement.id}.docx`);
            })
            .catch(() => {
                if (this.props.notification === notificationData) {
                    this.props.closeNotification();
                }
            });
    }

    save1C = () => {
        const notificationData = {
            text: 'Идет формирование документа...',
        };

        this.props.showNotification(notificationData);

        fetch('/api/v1/statement/export', {
            method: 'POST',
            body: JSON.stringify({
                accId: this.props.selectedAccount,
                from: this.state.from,
                to: this.state.to,
            }),
        })
            .then(response => response.blob())
            .then((blob) => {
                if (this.props.notification === notificationData) {
                    this.props.closeNotification();
                }
                downloadBlob(blob, `${this.state.statement.id}.txt`);
            })
            .catch((err) => {
                if (this.props.notification === notificationData) {
                    this.props.closeNotification();
                }
            });
    }

    render() {
        let list = this.state.statement ? this.state.statement.lines : null;

        if (list && !Array.isArray(list)) {
            list = [list];
        }

        if (!list) {
            list = [];
        }

        return (
            <SceneWrapper>
                <Loader visible={this.state.isFetching} style={{ backgroundColor: 'rgba(255, 255, 255, 0.8)' }} />
                <Header
                    initialStartDate={yesterday}
                    initialEndDate={today}
                    acceptDateFilter={this.acceptDateFilter}
                    clearDateFilter={this.clearDateFilter}
                    search={this.state.search}
                    handleSearchChange={this.handleSearchChange}
                    showPrint={this.state.statement && this.state.statement.id}
                    saveXLSX={this.saveXLSX}
                    savePDF={this.savePDF}
                    saveDOCX={this.saveDOCX}
                    save1C={this.save1C}
                />
                <List
                    list={list}
                    search={this.state.search}
                />
            </SceneWrapper>
        );
    }
}

function mapStateToProps(state) {
    return {
        selectedAccount: state.accounts.selectedAccount,
        notification: state.notification.data,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        showNotification: bindActionCreators(showNotification, dispatch),
        closeNotification: bindActionCreators(closeNotification, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Statement);
