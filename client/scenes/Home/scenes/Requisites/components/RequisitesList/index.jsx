import React from 'react';
import PropTypes from 'prop-types';
import styles from './requisites-list.css';

const OrgInfoItem = ({ caption, value }) => (
    <div className={styles['item-wrapper']}>
        <div className={styles['item-caption']}>
            {caption}
        </div>
        <div className={styles['item-value']}>
            {value || '-'}
        </div>
    </div>
);

OrgInfoItem.propTypes = {
    caption: PropTypes.string,
    value: PropTypes.string,
};

OrgInfoItem.defaultProps = {
    caption: '',
    value: '',
};

const RequisitesList = ({ orgInfo, accountsList, cardsList, accountID }) => {
    if (!orgInfo || !accountsList || !cardsList || !accountID) {
        return null;
    }

    let accountInfo = accountsList.find(acc => acc.id === accountID);

    if (!accountInfo) {
        const cardsInfo = cardsList.find(acc => acc.id === accountID);
        accountInfo = accountsList.find(acc => acc.number === cardsInfo.number);
    }

    return (
        <div>
            <div className={styles.header}>
                Реквизиты
            </div>
            <div className={styles.wrapper}>
                <OrgInfoItem
                    key="fullName"
                    caption="Полное наименование организации"
                    value={orgInfo.fullName}
                />
                <OrgInfoItem
                    key="orgLegalAddress"
                    caption="Юридический адрес организации"
                    value={orgInfo.lawAddress}
                />
                <OrgInfoItem
                    key="INN"
                    caption="ИНН"
                    value={orgInfo.INN}
                />
                <OrgInfoItem
                    key="OGRN"
                    caption="ОГРН"
                    value={orgInfo.OGRN}
                />
                <OrgInfoItem
                    key="KPP"
                    caption="КПП"
                    value={orgInfo.KPP}
                />
                <OrgInfoItem
                    key="checkingAccount"
                    caption="Расчетный счет"
                    value={accountInfo.number}
                />
                <OrgInfoItem
                    key="bankName"
                    caption="Банк"
                    value="АКБ «Росевробанк» (АО)"
                />
                <OrgInfoItem
                    key="bankLegalAddress"
                    caption="Юридический адрес банка"
                    value="119991, Москва, ул. Вавилова, д. 24"
                />
                <OrgInfoItem
                    key="correspondentAccountBank"
                    caption="Корр.счет банка"
                    value={accountInfo.bankingInformation.correspondentAccount}
                />
                <OrgInfoItem
                    key="innBank"
                    caption="ИНН банка"
                    value={accountInfo.bankingInformation.inn}
                />
                <OrgInfoItem
                    key="bicBank"
                    caption="БИК банка"
                    value={accountInfo.bankingInformation.bic}
                />
            </div>
        </div>
    );
};

RequisitesList.propTypes = {
    orgInfo: PropTypes.object,
    accountsList: PropTypes.array,
    accountID: PropTypes.string,
    cardsList: PropTypes.array,
};

RequisitesList.defaultProps = {
    orgInfo: {},
    accountsList: [],
    accountID: '',
    cardsList: [],
};

export default RequisitesList;
