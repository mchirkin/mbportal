import React from 'react';
import { connect } from 'react-redux';

import SceneWrapper from 'components/SceneWrapper';

import RequisitesList from './components/RequisitesList';

const Requisites = props => (
    <SceneWrapper>
        <RequisitesList
            {...props}
        />
    </SceneWrapper>
);

function mapStateToProps(state) {
    return {
        orgInfo: state.user.orgInfo.data,
        accountsList: state.accounts.data,
        cardsList: state.cards.data,
        accountID: state.accounts.selectedAccount,
    };
}

export default connect(mapStateToProps)(Requisites);
