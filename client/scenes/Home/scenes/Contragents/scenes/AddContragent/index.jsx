import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { fetch } from 'utils';

import setBackUrl from 'modules/settings/actions/set_back_url';
import setMessageData from 'modules/info_message/actions/set_data';
import fetchContragents from 'modules/settings/actions/contragents/fetch';

import Loader from 'components/Loader';

import Header from './components/Header';
import Form from './components/Form';
import BtnRow from './components/BtnRow';

class AddContragent extends Component {
    static propTypes = {
        contragentList: PropTypes.array,
        fetchContragents: PropTypes.func.isRequired,
        setBackUrl: PropTypes.func.isRequired,
        setMessageData: PropTypes.func.isRequired,
        orgInfo: PropTypes.object,
    };

    static defaultProps = {
        contragentList: [],
        orgInfo: {},
    };

    static contextTypes = {
        router: PropTypes.object.isRequired,
    };

    constructor(props) {
        super(props);

        this.state = {
            contragent: '',
            inn: '',
            kpp: '',
            account: '',
            bank: '',
            bik: '',
            'payment-id': '',
            'payment-description': '',
            'contragent-description': '',
            email: '',
            phone: '',
            errorText: '',
            isFetching: false,
            konturData: null,
        };
    }

    componentWillMount() {
        if (!this.props.contragentList) {
            this.props.fetchContragents();
        }
        this.props.setBackUrl('/home/contacts');
    }

    handleInputChange = (e) => {
        const id = e.target.getAttribute('id');
        this.setState({
            [id]: e.target.value,
            errorText: '',
        });
        if (id === 'bik') {
            if (e.target.value.length === 9) {
                this.getBankInfoByBik(e.target.value).then((result) => {
                    if (result.length > 0) {
                        this.setState({
                            bank: result[0].name,
                        });
                    }
                });
            }
        }
        if (id === 'bank' && e.target.value.length > 2) {
            this.getBankInfoByName(e.target.value).then((result) => {
                this.setState({
                    bankList: result,
                });
            });
        }
        if (id === 'inn') {
            const inn = e.target.value;
            this.setState({
                konturData: null,
            });
            if (inn.length === 5 || inn.length === 10 || inn.length === 12) {
                this.setState({
                    konturDataIsFetching: true,
                });
                this.getContragentDataFromKonturFocus({
                    inn,
                }).then((data) => {
                    const info = data ? (data.CompanyInfo || data.IpInfo) : null;
                    const fio = info && info.fio ? `ИП ${info.fio}` : null;
                    const newState = {
                        konturData: data === null ? false : data,
                        konturDataIsFetching: false,
                    };
                    if (info) {
                        newState.kpp = info.kpp || '0';
                        newState.contragent = this.state.contragent || info.longName || info.shortName || fio;
                    }
                    this.setState(newState);
                });
            }
        }
    }

    getBankInfoByBik = bik => fetch('/api/v1/products/getbikinfo', {
        method: 'POST',
        credentials: 'include',
        body: JSON.stringify({
            value: bik,
        }),
    })
        .then(response => response.json())
        .then(json => Promise.resolve(json))
        .catch(err => Promise.reject(err))

    getBankInfoByName = name => fetch('/api/v1/products/getbanknameinfo', {
        method: 'POST',
        credentials: 'include',
        body: JSON.stringify({
            value: name,
        }),
    })
        .then(response => response.json())
        .then(json => Promise.resolve(json))
        .catch(err => Promise.reject(err))

    setBankInfo = (info) => {
        this.setState({
            bik: info.bik,
            bank: info.name,
            bankList: [],
        });
    }

    /**
     * Проверить сохранен ли уже контрагент
     * @param {string} inn ИНН контрагента
     * @return {boolean}
     */
    checkIfContragentInList = inn =>
        this.props.contragentList &&
        this.props.contragentList.find(agent => agent.inn && agent.inn === inn)

    saveContragent = () => {
        if (this.checkIfContragentInList(this.state.inn)) {
            this.props.setMessageData({
                isVisible: true,
                header: 'Внимание!',
                body: 'Контрагент с таким ИНН уже добавлен в список контактов',
                btnText: 'Назад',
                callback: () => this.props.setMessageData({ isVisible: false }),
            });
            return;
        }

        this.setState({
            isFetching: true,
        });

        const data = Object.assign({}, this.state);
        data.account = data.account.replace(/\./g, '');
        data.phone = data.phone.replace(/[\s()+-]/g, '');

        fetch('/api/v1/contragent/create', {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify(data),
        }).then(response => response.json()).then((json) => {
            this.setState({
                isFetching: false,
            });
            if (!json.errorText) {
                this.props.setMessageData({
                    isVisible: true,
                    header: 'Успешно!',
                    body: 'Контакт добавлен',
                    btnText: 'Далее',
                    callback: () => this.props.setMessageData({ isVisible: false }),
                });
                this.resetState();
                this.props.fetchContragents();
            } else {
                this.props.setMessageData({
                    isVisible: true,
                    header: 'Внимание!',
                    body: json.errorText,
                    btnText: 'Назад',
                    callback: () => this.props.setMessageData({ isVisible: false }),
                });
            }
        }).catch((err) => {
            global.console.log(err);
        });
    }

    handleCancelBtnClick = () => {
        this.resetState();
    }

    handleKonturClick = () => {

    }

    resetState = () => {
        this.setState({
            contragent: '',
            inn: '',
            kpp: '',
            account: '',
            bank: '',
            bik: '',
            'payment-id': '',
            'payment-description': '',
            'contragent-description': '',
            email: '',
            phone: '',
            isFetching: false,
            konturData: null,
        });
    }

    getContragentDataFromKonturFocus = ({ inn }) => {
        if (this.props.orgInfo && this.props.orgInfo.konturFocusSubscr === 'paid') {
            return fetch('/api/v1/contragent/get_facts', {
                credentials: 'include',
                method: 'POST',
                body: JSON.stringify({
                    inn,
                }),
            })
                .then(response => response.json());
        }
        return Promise.resolve(null);
    }

    closeWindow = () => {
        this.resetState();
        this.props.setBackUrl('/home');
        this.context.router.history.push('/home/contacts');
    }

    render() {
        return (
            <div>
                <Loader visible={this.state.isFetching} />
                <Header />
                <Form
                    {...this.state}
                    handleInputChange={this.handleInputChange}
                    setBankInfo={this.setBankInfo}
                />
                <BtnRow
                    onCancel={this.closeWindow}
                    onSave={this.saveContragent}
                />
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        orgInfo: state.user.orgInfo.data,
        backUrl: state.settings.backUrl,
        contragentList: state.settings.contragents.data,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        setBackUrl: bindActionCreators(setBackUrl, dispatch),
        setMessageData: bindActionCreators(setMessageData, dispatch),
        fetchContragents: bindActionCreators(fetchContragents, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(AddContragent);
