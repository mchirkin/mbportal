import React, { Component } from 'react';
import PropTypes from 'prop-types';
import InputGroup from 'components/ui/InputGroup';
import InputTextarea from 'components/ui/InputTextarea';
import KonturFocusModalContainer from 'containers/KonturFocusModalContainer';
import styles from './form.css';

class Form extends Component {
    static propTypes = {
        konturData: PropTypes.bool,
        contragent: PropTypes.string,
        handleInputChange: PropTypes.func.isRequired,
        bank: PropTypes.string,
        bankList: PropTypes.array,
        setBankInfo: PropTypes.func,
        inn: PropTypes.string,
        account: PropTypes.string,
        kpp: PropTypes.string,
        bik: PropTypes.string,
        email: PropTypes.string,
        phone: PropTypes.string,
        'contragent-description': PropTypes.string,
    };

    static defaultProps = {
        account: '',
        bank: '',
        bik: '',
        contragent: '',
        kpp: '',
        phone: '',
        email: '',
        bankList: [],
        inn: '',
        setBankInfo: undefined,
        konturData: null,
        'contragent-description': '',

    };

    handleKonturClick = (href) => {
        if (href) {
            const win = window.open(href, '_blank');
            win.focus();
        }
    }

    render() {
        let factsColor = null;
        if (this.props.konturData !== null) {
            factsColor = 'grey';
            if (this.props.konturData && this.props.konturData.ActivityFacts.facts.length > 0) {
                factsColor = 'green';
            }
            if (this.props.konturData && this.props.konturData.PayAttentionFacts.facts.length > 0) {
                factsColor = 'yellow';
            }
            if (this.props.konturData && this.props.konturData.CriticalFacts.facts.length > 0) {
                factsColor = 'red';
            }
        }

        let konturHref;
        if (this.props.konturData && this.props.konturData.CompanyInfo) {
            konturHref = this.props.konturData.CompanyInfo.href;
        }
        if (this.props.konturData && this.props.konturData.IpInfo) {
            konturHref = this.props.konturData.IpInfo.href;
        }

        return (
            <div className={styles.wrapper}>
                <div className={styles.form}>
                    <div className={styles['input-row']}>
                        <InputGroup
                            id="contragent"
                            caption="Наименование контрагента"
                            value={this.props.contragent}
                            onChange={this.props.handleInputChange}
                            style={{
                                flexGrow: 1,
                                width: 394,
                            }}
                        />
                        {this.props.konturData !== null
                            ? (
                                <div
                                    className={styles['kontur-info']}
                                    onClick={() => { this.handleKonturClick(konturHref); }}
                                >
                                    <div
                                        className={styles['contragent-kontur']}
                                        data-facts={factsColor}
                                    />
                                </div>
                            )
                            : null}
                    </div>
                    <div className={styles['input-row']}>
                        <InputGroup
                            id="bank"
                            caption="Банк"
                            value={this.props.bank}
                            onChange={this.props.handleInputChange}
                            autocomplete={this.props.bankList}
                            handleAutocompleteSelect={this.props.setBankInfo}
                            style={{
                                flexGrow: 1,
                            }}
                        />
                    </div>
                    <div className={styles['input-row']}>
                        <InputGroup
                            id="inn"
                            caption="ИНН"
                            value={this.props.inn}
                            onChange={this.props.handleInputChange}
                            mask={'999999999999'}
                            maskChar={null}
                            style={{
                                flexGrow: 1,
                            }}
                        />
                    </div>
                    <div className={styles['input-row']}>
                        <InputGroup
                            id="account"
                            caption="Расчетный счет"
                            value={this.props.account}
                            onChange={this.props.handleInputChange}
                            mask={'99999.999.9.99999999999'}
                            maskChar={'_'}
                            style={{
                                flexGrow: 1,
                            }}
                        />
                    </div>
                    <div className={styles['input-row']}>
                        <InputGroup
                            id="kpp"
                            caption="КПП"
                            value={this.props.kpp}
                            onChange={this.props.handleInputChange}
                            mask={'999999999'}
                            maskChar={null}
                            style={{
                                flexGrow: 1,
                                marginRight: 20,
                            }}
                        />
                        <InputGroup
                            id="bik"
                            caption="БИК"
                            value={this.props.bik}
                            onChange={this.props.handleInputChange}
                            mask={'999999999'}
                            maskChar={null}
                            style={{
                                flexGrow: 1,
                            }}
                        />
                    </div>
                    <div className={styles['input-row']}>
                        <InputGroup
                            id="email"
                            caption="E-Mail"
                            onChange={this.props.handleInputChange}
                            value={this.props.email}
                            style={{
                                flexGrow: 1,
                                marginRight: 20,
                            }}
                        />
                        <InputGroup
                            id="phone"
                            caption="Мобильный"
                            mask="+7 (999) 999-99-99"
                            maskChar="_"
                            onChange={this.props.handleInputChange}
                            value={this.props.phone}
                            style={{
                                flexGrow: 1,
                            }}
                        />
                    </div>
                    <div className={styles['input-row']}>
                        <InputTextarea
                            id="contragent-description"
                            caption="Описание контрагента"
                            value={this.props['contragent-description']}
                            onChange={this.props.handleInputChange}
                            style={{
                                flexGrow: 1,
                            }}
                            autosize
                        />
                    </div>
                </div>
                <div className={styles['kontur-data']}>
                    {this.props.konturData !== null
                        ? (
                            <KonturFocusModalContainer modalData={this.props.konturData} />
                        )
                        : null}
                </div>
            </div>
        );
    }
}

export default Form;
