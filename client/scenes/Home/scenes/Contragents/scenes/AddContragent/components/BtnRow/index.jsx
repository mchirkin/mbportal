import React from 'react';
import PropTypes from 'prop-types';
import Btn from 'components/ui/Btn';
import styles from './btn-row.css';

const BtnRow = ({ onCancel, onSave }) => (
    <div className={styles.wrapper}>
        <Btn
            bgColor="grey"
            caption="&times;"
            onClick={onCancel}
            style={{
                width: 46,
                marginRight: 20,
                fontSize: 34,
            }}
        />
        <Btn
            caption="Сохранить контакт"
            onClick={onSave}
        />
    </div>
);

BtnRow.propTypes = {
    onCancel: PropTypes.func.isRequired,
    onSave: PropTypes.func.isRequired,
};

export default BtnRow;
