import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Scrollbars } from 'react-custom-scrollbars';
import { Link } from 'react-router-dom';

import Btn from 'components/ui/Btn';
import SearchInput from 'components/ui/SearchInput';

import Item from '../Item';
import styles from './list.css';

export default class List extends Component {
    static propTypes = {
        data: PropTypes.array,
        selectedContragent: PropTypes.number,
        selectContragent: PropTypes.func.isRequired,
        deleteContragent: PropTypes.func.isRequired,
        createPayment: PropTypes.func.isRequired,
        setContragentKonturColor: PropTypes.func.isRequired,
        setKonturInfoData: PropTypes.func.isRequired,
    };

    static defaultProps = {
        data: [],
        selectedContragent: 0,
    };

    constructor(props) {
        super(props);

        this.state = {
            search: '',
        };
    }

    handleSearchChange = (e) => {
        this.setState({
            search: e.target.value,
        });
    }

    render() {
        const props = this.props;
        const {
            data,
            selectedContragent,
            selectContragent,
            deleteContragent,
            createPayment,
            setContragentKonturColor,
            setKonturInfoData,
        } = this.props;

        return (
            <div className={styles.wrapper}>

                <div className={styles['contragent-list']}>
                    <Scrollbars>
                        <div className={styles.header}>
                            <div className={styles['header-top']}>
                                <div className={styles['header-name']}>
                                    Список контактов
                                </div>
                            </div>
                            <div className={styles['header-bottom']}>
                                <SearchInput
                                    value={this.state.search}
                                    onChange={this.handleSearchChange}
                                    style={{
                                        width: 446,
                                    }}
                                />
                                <Link to="/home/contacts/add" title="Добавить контакт">
                                    <div className={styles['add-btn']}>
                                        <div className={styles['add-btn-plus']} />
                                    </div>
                                </Link>
                            </div>
                        </div>
                        {data && data.length > 0
                            ? data
                                /*
                                .filter(item => this.state.search
                                    ? item.fullname.toLowerCase().includes(this.state.search.toLowerCase())
                                    : true
                                )
                                */
                                .map((item, index) => (
                                    <Item
                                        data={item}
                                        key={item.id}
                                        getContragentDataFromKonturFocus={props.getContragentDataFromKonturFocus}
                                        konturFocusSubscr={props.konturFocusSubscr}
                                        isSelected={selectedContragent === index}
                                        selectContragent={selectContragent}
                                        setContragentKonturColor={setContragentKonturColor}
                                        setKonturInfoData={setKonturInfoData}
                                        isVisible={this.state.search
                                            ? item.fullname.toLowerCase().includes(this.state.search.toLowerCase())
                                            : true}
                                    />
                                ))
                            : null}
                    </Scrollbars>
                </div>
                <div className={styles['btn-row']}>
                    <Btn
                        caption="Удалить контакт"
                        bgColor="grey"
                        onClick={deleteContragent}
                    />
                    <Btn
                        caption="Создать платеж"
                        onClick={createPayment}
                    />
                </div>
            </div>
        );
    }
}
