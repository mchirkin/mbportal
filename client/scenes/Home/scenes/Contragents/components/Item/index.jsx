import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './item.css';

export default class Item extends Component {
    static propTypes = {
        data: PropTypes.object,
        getContragentDataFromKonturFocus: PropTypes.func.isRequired,
        konturFocusSubscr: PropTypes.bool,
        isSelected: PropTypes.bool,
        setContragentKonturColor: PropTypes.func.isRequired,
        setKonturInfoData: PropTypes.func.isRequired,
    };

    static defaultProps = {
        data: {},
        konturFocusSubscr: false,
        isSelected: false,
    };

    constructor(props) {
        super(props);

        this.state = {
            konturData: null,
        };
    }

    componentWillMount() {
        if (this.props.data && this.props.getContragentDataFromKonturFocus && !this.konturDataFetched) {
            this.konturDataFetched = true;
            this.props.getContragentDataFromKonturFocus(this.props.data)
                .then((result) => {
                    this.setState({
                        konturData: result || false,
                    });
                });
        }
    }

    componentWillReceiveProps(nextProps) {
        if (
            nextProps.data &&
            nextProps.getContragentDataFromKonturFocus &&
            nextProps.konturFocusSubscr &&
            !this.konturDataFetched
        ) {
            this.konturDataFetched = true;
            nextProps.getContragentDataFromKonturFocus(nextProps.data)
                .then((result) => {
                    this.setState({
                        konturData: result || false,
                    });
                });
        }
    }

    componentDidUpdate(prevProps) {
        if (
            prevProps.data &&
            this.props.data &&
            this.props.konturFocusSubscr &&
            prevProps.data.inn !== this.props.data.inn
        ) {
            this.setState({
                konturData: null,
            });
            this.props.getContragentDataFromKonturFocus(this.props.data)
                .then((result) => {
                    this.setState({
                        konturData: result || false,
                    });
                });
        }

        let factsColor = null;
        if (this.state.konturData !== null && this.props.isSelected && !prevProps.isSelected) {
            factsColor = 'grey';
            if (this.state.konturData && this.state.konturData.ActivityFacts.facts.length > 0) {
                factsColor = 'green';
            }
            if (this.state.konturData && this.state.konturData.PayAttentionFacts.facts.length > 0) {
                factsColor = 'yellow';
            }
            if (this.state.konturData && this.state.konturData.CriticalFacts.facts.length > 0) {
                factsColor = 'red';
            }
            this.props.setContragentKonturColor(factsColor);
        }
    }

    /**
     * Обработчик нажатия на Контур.Светофор
     */
    handleKonturClick = (e) => {
        e.stopPropagation();
        if (this.state.konturData) {
            let href;

            if (this.state.konturData.CompanyInfo && this.state.konturData.CompanyInfo.href) {
                href = this.state.konturData.CompanyInfo.href;
            }

            if (this.state.konturData.IpInfo && this.state.konturData.IpInfo.href) {
                href = this.state.konturData.IpInfo.href;
            }

            if (href) {
                const win = window.open(href, '_blank');
                win.focus();
            }
        }
    }

    handleKonturMouseEnter = () => {
        this.props.setKonturInfoData(this.state.konturData);
    }

    handleKonturMouseLeave = () => {
        this.props.setKonturInfoData(null);
    }

    render() {
        const props = this.props;

        let factsColor = null;
        if (this.state.konturData !== null) {
            factsColor = 'grey';
            if (this.state.konturData && this.state.konturData.ActivityFacts.facts.length > 0) {
                factsColor = 'green';
            }
            if (this.state.konturData && this.state.konturData.PayAttentionFacts.facts.length > 0) {
                factsColor = 'yellow';
            }
            if (this.state.konturData && this.state.konturData.CriticalFacts.facts.length > 0) {
                factsColor = 'red';
            }
        }

        return (
            <div
                className={styles['contragent-wrapper']}
                onClick={() => { props.selectContragent(props.data); }}
                data-selected={props.isSelected}
                data-visible={props.isVisible}
            >
                <div className={styles['contragent-name']}>
                    {props.data.fullname}
                </div>
                {
                    factsColor && props.konturFocusSubscr
                        ? (
                            <div
                                className={styles['contragent-kontur-wrapper']}
                                onClick={this.handleKonturClick}
                                onMouseEnter={this.handleKonturMouseEnter}
                                onMouseLeave={this.handleKonturMouseLeave}
                            >
                                <div
                                    className={styles['contragent-kontur']}
                                    data-facts={factsColor}
                                />
                                <div className={styles['contragent-kontur-more']}>Подробнее</div>
                            </div>
                        )
                        : (
                            <div style={{ width: 32 }} />
                        )
                }
            </div>
        );
    }
}
