import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Scrollbars } from 'react-custom-scrollbars';
import { CSSTransition } from 'react-transition-group';

import Btn from 'components/ui/Btn';
import InputGroup from 'components/ui/InputGroup';
import InputTextarea from 'components/ui/InputTextarea';
import KonturFocusModalContainer from 'containers/KonturFocusModalContainer';

import styles from './info-panel.css';

export default class InfoPanel extends Component {
    static propTypes = {
        contragentDataFactsColor: PropTypes.string.isRequired,
        konturInfoData: PropTypes.oneOfType([
            PropTypes.bool,
            PropTypes.object,
        ]),
        fullname: PropTypes.string,
        handleInputChange: PropTypes.func.isRequired,
        bankName: PropTypes.string,
        bankList: PropTypes.array,
        setBankInfo: PropTypes.func.isRequired,
        bankBik: PropTypes.string,
        inn: PropTypes.string,
        accNumber: PropTypes.string,
        email: PropTypes.string,
        kpp: PropTypes.string,
        phone: PropTypes.string,
        description: PropTypes.string,
        contragentDataEdited: PropTypes.bool.isRequired,
        cancelEdit: PropTypes.func.isRequired,
        saveChanges: PropTypes.func.isRequired,
    };

    static defaultProps = {
        konturInfoData: null,
        fullname: '',
        bankName: '',
        bankList: [],
        bankBik: '',
        inn: '',
        accNumber: '',
        email: '',
        kpp: '',
        phone: '',
        description: '',
    };

    getContragentBorderColor = () => {
        let factsColor;
        if (this.props.konturInfoData && !this.props.konturInfoData.nodata) {
            factsColor = 'grey';
            if (this.props.konturInfoData.ActivityFacts.facts.length > 0) {
                factsColor = 'green';
            }
            if (this.props.konturInfoData.PayAttentionFacts.facts.length > 0) {
                factsColor = 'yellow';
            }
            if (this.props.konturInfoData.CriticalFacts.facts.length > 0) {
                factsColor = 'red';
            }
        }
        if (this.props.konturInfoData && this.props.konturInfoData.nodata) {
            factsColor = 'grey';
        }

        let contragentBorder = '1px solid #d8d8d8';
        if (factsColor === 'red') {
            contragentBorder = '2px solid #fa2742';
        }
        if (factsColor === 'yellow') {
            contragentBorder = '2px solid #f9cf3e';
        }
        if (factsColor === 'green') {
            contragentBorder = '2px solid #30d2b5';
        }

        return contragentBorder;
    }

    render() {
        let contragentBorder = '1px solid #d8d8d8';
        if (this.props.contragentDataFactsColor === 'red') {
            contragentBorder = '2px solid #fa2742';
        }
        if (this.props.contragentDataFactsColor === 'yellow') {
            contragentBorder = '2px solid #f9cf3e';
        }
        if (this.props.contragentDataFactsColor === 'green') {
            contragentBorder = '2px solid #30d2b5';
        }

        let konturInfoDataFullname = '';
        if (this.props.konturInfoData && this.props.konturInfoData.CompanyInfo) {
            konturInfoDataFullname = this.props.konturInfoData.CompanyInfo.longName;
        }
        if (this.props.konturInfoData && this.props.konturInfoData.IpInfo) {
            konturInfoDataFullname = `ИП ${this.props.konturInfoData.IpInfo.fio}`;
        }

        return (
            <div className={styles.wrapper}>
                {this.props.konturInfoData
                    ? (
                        <div className={styles['kontur-info']}>
                            <Scrollbars>
                                <div className={styles.header}>
                                    Информация
                                </div>
                                <div className={styles['kontur-info-data']}>
                                    <CSSTransition
                                        timeout={0}
                                        key="konturInfo"
                                        classNames="fadeappear"
                                        appear
                                        in
                                    >
                                        <InputTextarea
                                            key="fullname"
                                            caption="Контрагент"
                                            id="fullname"
                                            value={konturInfoDataFullname}
                                            autosize
                                            height={22}
                                            style={{
                                                border: this.getContragentBorderColor(),
                                                marginBottom: 40,
                                            }}
                                            textareaStyle={{
                                                fontSize: 16,
                                            }}
                                            readOnly
                                        />
                                    </CSSTransition>
                                    <KonturFocusModalContainer modalData={this.props.konturInfoData} />
                                </div>
                            </Scrollbars>
                        </div>
                    )
                    : null}
                <div className={styles.form}>
                    <Scrollbars>
                        <div className={styles.header}>
                            Информация
                        </div>
                        <div className={styles['input-row']}>
                            <InputTextarea
                                key="fullname"
                                caption="Контрагент"
                                id="fullname"
                                value={this.props.fullname}
                                autosize
                                height={22}
                                style={{
                                    border: contragentBorder,
                                }}
                                textareaStyle={{
                                    fontSize: 16,
                                }}
                                onChange={this.props.handleInputChange}
                            />
                        </div>
                        <div className={styles['input-row']}>
                            <InputGroup
                                key="bankName"
                                caption="Банк"
                                id="bankName"
                                value={this.props.bankName}
                                onChange={this.props.handleInputChange}
                                autocomplete={this.props.bankList}
                                handleAutocompleteSelect={this.props.setBankInfo}
                            />
                        </div>
                        <div className={styles['input-row']}>
                            <InputGroup
                                key="bankBik"
                                caption="БИК"
                                id="bankBik"
                                value={this.props.bankBik}
                                onChange={this.props.handleInputChange}
                                mask={'999999999'}
                                maskChar={null}
                            />
                        </div>
                        <div className={styles['input-row']}>
                            <InputGroup
                                key="inn"
                                caption="ИНН"
                                id="inn"
                                value={this.props.inn}
                                onChange={this.props.handleInputChange}
                                mask={'999999999999'}
                                maskChar={null}
                            />
                        </div>
                        <div className={styles['input-row']}>
                            <InputGroup
                                key="accNumber"
                                caption="Расчетный счет"
                                id="accNumber"
                                value={this.props.accNumber}
                                onChange={this.props.handleInputChange}
                                mask={'99999.999.9.99999999999'}
                                maskChar={'_'}
                            />
                        </div>
                        <div className={styles['input-row']}>
                            <InputGroup
                                key="kpp"
                                caption="КПП"
                                id="kpp"
                                value={this.props.kpp}
                                onChange={this.props.handleInputChange}
                                mask={'999999999'}
                                maskChar={null}
                            />
                        </div>
                        <div className={styles['input-row']}>
                            <InputGroup
                                key="email"
                                id="email"
                                caption="E-Mail"
                                onChange={this.props.handleInputChange}
                                value={this.props.email}
                            />
                        </div>
                        <div className={styles['input-row']}>
                            <InputGroup
                                key="phone"
                                id="phone"
                                caption="Мобильный"
                                mask="+7 (999) 999-99-99"
                                maskChar="_"
                                onChange={this.props.handleInputChange}
                                value={this.props.phone}
                            />
                        </div>
                        <div className={styles['input-row']}>
                            <InputTextarea
                                key="description"
                                caption="Описание контрагента"
                                id="description"
                                value={this.props.description}
                                autosize
                                height={70}
                                textareaStyle={{
                                    fontSize: 16,
                                }}
                                onChange={this.props.handleInputChange}
                            />
                        </div>
                    </Scrollbars>
                </div>
                <div className={styles['btn-row']}>
                    {this.props.contragentDataEdited
                        ? [
                            <Btn
                                key="cancel"
                                bgColor="grey"
                                caption="&times;"
                                onClick={this.props.cancelEdit}
                                style={{
                                    width: 46,
                                    fontSize: 34,
                                }}
                            />,
                            <Btn
                                key="saveChanges"
                                caption="Сохранить изменения"
                                onClick={this.props.saveChanges}
                            />,
                        ]
                        : null}
                </div>
            </div>
        );
    }
}
