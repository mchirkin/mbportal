import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Switch, Route } from 'react-router-dom';

import { fetch } from 'utils';

import AddContragent from 'scenes/Home/scenes/Contragents/scenes/AddContragent';

import fetchContragents from 'modules/settings/actions/contragents/fetch';
import setBackUrl from 'modules/settings/actions/set_back_url';
import setMessageData from 'modules/info_message/actions/set_data';
import setPaymentModalData from 'modules/payments/payment_modal/actions/set_data';

import SceneWrapper from 'components/SceneWrapper';
import Loader from 'components/Loader';

import List from './components/List';
import InfoPanel from './components/InfoPanel';

class Contragents extends Component {
    static propTypes = {
        fetchContragents: PropTypes.func.isRequired,
        contragentList: PropTypes.array,
        orgInfo: PropTypes.object,
        setPaymentModalData: PropTypes.func.isRequired,
        setMessageData: PropTypes.func.isRequired,
        contragentListFetching: PropTypes.bool,
    };

    static defaultProps = {
        contragentList: [],
        orgInfo: {},
        contragentListFetching: false,
    };

    static contextTypes = {
        router: PropTypes.object.isRequired,
    };

    constructor(props) {
        super(props);

        this.state = {
            isFetching: false,
            selectedContragent: 0,
            contragentData: {},
            contragentDataFactsColor: '',
            contragentDataEdited: false,
            bankList: [],
        };
    }

    componentWillMount() {
        this.props.fetchContragents();
    }

    componentDidUpdate(prevProps, prevState) {
        const dataChanged = (
            (this.props.contragentList && !prevProps.contragentList) ||
            (this.state.selectedContragent !== prevState.selectedContragent) ||
            (this.props.contragentList && Object.keys(this.state.contragentData).length === 0)
        );

        if (dataChanged && this.props.contragentList && this.props.contragentList.length > 0) {
            const contragentData = this.props.contragentList[this.state.selectedContragent];
            let accList = [];
            if (contragentData.accList) {
                accList = Array.isArray(contragentData.accList)
                    ? contragentData.accList
                    : [contragentData.accList];
            }
            const account = accList.length > 0 ? accList[0] : {};
            if (account.id) {
                account.paymentId = account.id;
            }
            this.setState({
                contragentData: Object.assign({}, account, contragentData),
                contragentDataEdited: false,
            });
        }
    }

    getContragentDataFromKonturFocus = ({ inn }) => {
        if (this.props.orgInfo && this.props.orgInfo.konturFocusSubscr === 'paid') {
            return fetch('/api/v1/contragent/get_facts', {
                credentials: 'include',
                method: 'POST',
                body: JSON.stringify({
                    inn,
                }),
            })
                .then(response => response.json());
        }
        return Promise.resolve(null);
    }

    selectContragent = (data) => {
        const index = this.props.contragentList.findIndex(agent => agent.id === data.id);
        this.setState({
            selectedContragent: index,
        });
    }

    setContragentKonturColor = (color) => {
        this.setState({
            contragentDataFactsColor: color,
        });
    }

    deleteContragent = () => {
        const id = this.props.contragentList[this.state.selectedContragent].id;

        return fetch('/api/v1/documents/delete', {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify({
                documentId: id,
            }),
        })
            .then(response => response.text())
            .then((text) => {
                if (text.length > 0) {
                    const json = JSON.parse(text);
                    Promise.reject(json.errorText);
                } else {
                    Promise.resolve(true);
                    this.props.fetchContragents()
                        .then(() => {
                            this.setState({
                                selectedContragent: 0,
                                contragentDataEdited: false,
                            });
                        });
                }
            })
            .catch((err) => {
                global.console.log(err);
                Promise.reject(err);
            });
    }

    createPayment = () => {
        const agent = this.props.contragentList[this.state.selectedContragent];

        const data = {
            status: 'new',
            signStatus: 0,
            corrFullname: agent.fullname,
            corrInn: agent.inn,
            corrKpp: agent.kpp,
        };

        if (agent.accList) {
            const accListData = Array.isArray(agent.accList) ? agent.accList[0] : agent.accList;
            data.corrAccNumber = accListData.accNumber;
            data.corrBankName = accListData.bankName;
            data.corrBankBik = accListData.bankBik;
            data.corrBankPlace = accListData.bankPlace;
            data.corrBankCorrAccount = accListData.bankCorrAccount;
        }

        this.props.setPaymentModalData(data);
        this.context.router.history.push('/home/payment');
    }

    handleInputChange = (e) => {
        const newData = Object.assign({}, this.state.contragentData);
        const id = e.target.getAttribute('id');
        newData[id] = e.target.value;
        this.setState({
            contragentData: newData,
            contragentDataEdited: true,
        });

        if (id === 'bankBik') {
            if (e.target.value.length === 9) {
                this.getBankInfoByBik(e.target.value).then((result) => {
                    if (result.length > 0) {
                        const newContragentData = Object.assign(
                            {},
                            this.state.contragentData,
                            {
                                bankName: result[0].name,
                            },
                        );

                        this.setState({
                            contragentData: newContragentData,
                        });
                    }
                });
            }
        }
        if (id === 'bankName' && e.target.value.length > 2) {
            this.getBankInfoByName(e.target.value).then((result) => {
                this.setState({
                    bankList: result,
                });
            });
        }
    }

    cancelEdit = () => {
        const contragentData = this.props.contragentList[this.state.selectedContragent];
        this.setState({
            contragentData,
            contragentDataEdited: false,
        });
    }

    saveChanges = () => {
        let accList = [];
        if (this.state.contragentData.accList) {
            accList = Array.isArray(this.state.contragentData.accList)
                ? this.state.contragentData.accList
                : [this.state.contragentData.accList];
        }
        const account = accList.length > 0 ? accList[0] : {};

        this.setState({
            isFetching: true,
        });

        fetch('/api/v1/contragent/create', {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify({
                id: this.state.contragentData.id,
                contragent: this.state.contragentData.fullname,
                inn: this.state.contragentData.inn,
                kpp: this.state.contragentData.kpp,
                account: this.state.contragentData.accNumber
                    ? this.state.contragentData.accNumber.replace(/\./g, '')
                    : '',
                bank: this.state.contragentData.bankName ? this.state.contragentData.bankName : '',
                bik: this.state.contragentData.bankBik ? this.state.contragentData.bankBik : '',
                'payment-id': this.state.contragentData.paymentId ? this.state.contragentData.paymentId : '',
                'payment-description': this.state.contragentData.accDescription
                    ? this.state.contragentData.accDescription
                    : '',
                email: this.state.contragentData.email,
                phone: this.state.contragentData.phone
                    ? this.state.contragentData.phone.replace(/[\s()+-]/g, '')
                    : '',
                'contragent-description': this.state.contragentData.description,
            }),
        })
            .then(response => response.json())
            .then((json) => {
                this.setState({
                    isFetching: false,
                });
                if (!json.errorText) {
                    this.setState({
                        contragentDataEdited: false,
                    });
                    this.props.fetchContragents()
                        .then(() => {
                            const contragentData = this.props.contragentList[this.state.selectedContragent];
                            this.setState({
                                contragentData,
                                contragentDataEdited: false,
                            });
                        });
                } else {
                    this.props.setMessageData({
                        isVisible: true,
                        header: 'Внимание!',
                        body: json.errorText,
                        btnText: 'Назад',
                        callback: () => this.props.setMessageData({ isVisible: false }),
                    });
                }
            })
            .catch((err) => {
                global.console.log(err);
            });
    }

    getBankInfoByBik = bik => fetch('/api/v1/products/getbikinfo', {
        method: 'POST',
        credentials: 'include',
        body: JSON.stringify({
            value: bik,
        }),
    })
        .then(response => response.json())
        .then(json => Promise.resolve(json))
        .catch(err => Promise.reject(err))

    getBankInfoByName = name => fetch('/api/v1/products/getbanknameinfo', {
        method: 'POST',
        credentials: 'include',
        body: JSON.stringify({
            value: name,
        }),
    })
        .then(response => response.json())
        .then(json => Promise.resolve(json))
        .catch(err => Promise.reject(err))

    setBankInfo = (info) => {
        const newContragentData = Object.assign(
            {},
            this.state.contragentData,
            {
                bankBik: info.bik,
                bankName: info.name,
            },
        );

        this.setState({
            contragentData: newContragentData,
            bankList: [],
        });
    }

    setKonturInfoData = (data) => {
        this.setState({
            konturInfoData: data,
        });
    }

    render() {
        return (
            <SceneWrapper>
                <Loader
                    visible={this.state.isFetching || this.props.contragentListFetching}
                    style={{ backgroundColor: 'rgba(255, 255, 255, 0.8)' }}
                />
                <Switch>
                    <Route exact path="/home/contacts/add" component={AddContragent} />
                    <Route
                        exact
                        path="/home/contacts"
                        render={() => (
                            <div style={{ display: 'flex', height: '100%' }}>
                                <List
                                    data={this.props.contragentList}
                                    getContragentDataFromKonturFocus={this.getContragentDataFromKonturFocus}
                                    konturFocusSubscr={
                                        this.props.orgInfo &&
                                        this.props.orgInfo.konturFocusSubscr === 'paid'
                                    }
                                    selectContragent={this.selectContragent}
                                    deleteContragent={this.deleteContragent}
                                    createPayment={this.createPayment}
                                    setContragentKonturColor={this.setContragentKonturColor}
                                    setKonturInfoData={this.setKonturInfoData}
                                    {...this.state}
                                />
                                <InfoPanel
                                    {...this.state.contragentData}
                                    contragentDataFactsColor={this.state.contragentDataFactsColor}
                                    contragentDataEdited={this.state.contragentDataEdited}
                                    handleInputChange={this.handleInputChange}
                                    cancelEdit={this.cancelEdit}
                                    saveChanges={this.saveChanges}
                                    setBankInfo={this.setBankInfo}
                                    bankList={this.state.bankList}
                                    konturInfoData={this.state.konturInfoData}
                                />
                            </div>
                        )}
                    />
                </Switch>
            </SceneWrapper>
        );
    }
}

function mapStateToProps(state) {
    return {
        orgInfo: state.user.orgInfo.data,
        contragentList: state.settings.contragents.data,
        contragentListFetching: state.settings.contragents.isFetching,
        backUrl: state.settings.backUrl,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        fetchContragents: bindActionCreators(fetchContragents, dispatch),
        setBackUrl: bindActionCreators(setBackUrl, dispatch),
        setMessageData: bindActionCreators(setMessageData, dispatch),
        setPaymentModalData: bindActionCreators(setPaymentModalData, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Contragents);
