import React from 'react';
import PropTypes from 'prop-types';
import randomColor from 'randomcolor';

import { formatNumber } from 'utils';

import AnalyticsItem from './components/AnalyticsItem';

import styles from './analytics.css';

const generateData = (data, operationType) => {
    const hasData = data && data.contragents && Object.keys(data.contragents[operationType]).length > 0;

    if (!hasData) return [{ name: 'nodata', value: 1 }];

    const result = [];
    const etc = {
        name: 'Прочее',
        value: 0,
        percent: 0,
    };

    Object.keys(data.contragents[operationType]).forEach((name) => {
        const orgData = data.contragents[operationType][name];

        const percent = (orgData.total / data[operationType]) * 100;

        const org = {
            name,
            value: orgData.total,
            percent,
        };

        if (percent < 2) {
            etc.value += orgData.total;
            etc.percent = (etc.value / data[operationType]) * 100;
        }

        result.push(org);
    });

    if (etc.value > 0) {
        result.push(etc);
    }

    return result;

    /*
    return Object.keys(data.contragents[operationType]).map((name) => {
        const orgData = data.contragents[operationType][name];

        const percent = (orgData.total / data[operationType]) * 100;

        const org = {
            name,
            value: orgData.total,
            percent,
        };

        return org;
    });
    */
};

const generateColors = (data, operationType) => {
    const hasData = data && data.contragents && Object.keys(data.contragents[operationType]).length > 0;

    if (!hasData) return ['#ccc'];

    return randomColor({
        count: Object.keys(data.contragents[operationType]).length,
        hue: 'blue',
    });
};


const Analytics = (props) => {
    const { data } = props;

    const value = Object.keys(data).length === 0 ? 0 : data.plus - data.minus;

    const valueColor = ['#3ac56c', '#e94d4d'];

    return (
        <div>
            {data.length !== 0 && (
                <div className={styles.container}>
                    <p
                        className={styles.total}
                        style={{ color: value >= 0 ? valueColor[0] : valueColor[1] }}
                    >
                        <span className={styles['financial-result']}>
                            Финансовый результат:
                        </span>
                        <br />
                        {`${formatNumber(value, true, 2)} ₽`}
                    </p>
                    <AnalyticsItem
                        title="Нам платили"
                        data={data}
                        chartData={generateData(data, 'plus')}
                        valueColor={valueColor[0]}
                        colors={generateColors(data, 'plus')}
                        link={{
                            to: '/home/invoices',
                            caption: 'Выставить счет',
                        }}
                        type="plus"
                        handlePieCellClick={props.handlePieCellClick}
                    />
                    <AnalyticsItem
                        title="Мы платили"
                        data={data}
                        chartData={generateData(data, 'minus')}
                        valueColor={valueColor[1]}
                        colors={generateColors(data, 'minus')}
                        link={{
                            to: '/home/payment',
                            caption: 'Заплатить',
                        }}
                        type="minus"
                        handlePieCellClick={props.handlePieCellClick}
                    />
                </div>
            )}
        </div>
    );
};

Analytics.propTypes = {
    data: PropTypes.object,
    handlePieCellClick: PropTypes.func,
};

Analytics.defaultProps = {
    data: {},
    handlePieCellClick: undefined,
};

export default Analytics;
