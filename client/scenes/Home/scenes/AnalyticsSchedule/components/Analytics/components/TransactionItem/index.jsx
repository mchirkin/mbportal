import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { formatNumber } from 'utils';

import Marquee from 'components/ui/Marquee';

import styles from './transaction-item.css';

export default class TransactionItem extends Component {
    static propTypes = {
        color: PropTypes.string,
        data: PropTypes.object,
        percent: PropTypes.number,
        name: PropTypes.string,
        type: PropTypes.object,
    };

    static defaultProps = {
        color: '#000',
        data: {},
        percent: 0,
        name: '',
        type: { type: 'plus', color: '#3ac56c' },
    };

    constructor(props) {
        super(props);

        this.state = {
            isOpen: false,
            height: 80,
        };
    }

    componentWillReceiveProps() {
        this.setState({
            height: this.head.offsetHeight,
            isOpen: false,
        });
    }

    handleHeadClick = () => {
        const height = !this.state.isOpen
            ? this.head.offsetHeight + this.content.offsetHeight
            : this.head.offsetHeight;

        this.setState({
            height,
            isOpen: !this.state.isOpen,
        });
    }

    render() {
        const {
            color,
            data,
            percent,
            name,
            type,
        } = this.props;

        return (
            <div
                key={name}
                className={styles.container}
                data-open={this.state.isOpen}
                data-transaction-name={name}
                data-transaction-type={type.type}
                style={{ height: this.state.height }}
            >
                <div
                    onClick={this.handleHeadClick}
                    className={styles.head}
                    ref={(node) => { this.head = node; }}
                >
                    <div
                        className={styles.wrap}
                        style={{
                            flexGrow: 1,
                            marginRight: 20,
                            overflow: 'hidden',
                        }}
                    >
                        <p className={styles.chart} style={{ borderColor: color }}>
                            {`${Math.round(percent)}%`}
                        </p>
                        <div className={styles.title}>
                            <Marquee text={name} />
                        </div>
                    </div>
                    <div className={styles.wrap}>
                        <p className={styles.total}>
                            {`${formatNumber(data.total, true, 2)} ₽`}
                        </p>
                        <div className={styles.toggle} />
                    </div>
                </div>
                <div className={styles.content} ref={(node) => { this.content = node; }}>
                    {data.payments.map((payment, i) => (
                        <div className={styles['content-item']} key={i}>
                            <div className={styles.description}>
                                <p className={styles.date}>
                                    {`${payment.docNumber} от ${payment.docDate}`}
                                </p>
                                <p className={styles.text}>
                                    {payment.description}
                                </p>
                            </div>
                            <p
                                className={styles.value}
                                style={{ color: type.color }}
                            >
                                {`${type.type === 'minus'
                                    ? formatNumber(-payment.amount, true, 2)
                                    : formatNumber(payment.amount, true, 2)} ₽
                                `}
                            </p>
                        </div>
                    ))}
                </div>
            </div>
        );
    }
}
