import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { NavLink as Link } from 'react-router-dom';
import {
    PieChart,
    Pie,
    Cell,
    ResponsiveContainer,
    Tooltip,
} from 'recharts';

import { formatNumber } from 'utils';

import TransactionItem from '../TransactionItem';

import styles from './analytics.css';

function renderPieTooltip({ payload }) {
    // console.log(payload);

    const data = payload[0] || {};

    if (data.name === 0 || data.name === 'nodata') {
        return null;
    }

    return (
        <div className={styles['pie-tooltip']}>
            <div className={styles['pie-tooltip-info']}>
                <div className={styles['pie-tooltip-contragent']}>
                    {data.name}
                </div>
                <div className={styles['pie-tooltip-sum']}>
                    {`${formatNumber(data.value, true, 2)} \u20bd`}
                </div>
            </div>
            <div className={styles['pie-tooltip-percent']}>
                {data.payload ? Math.round(data.payload.percent) : 0}%
            </div>
        </div>
    );
}

export default class AnalyticsItem extends Component {
    static propTypes = {
        title: PropTypes.string,
        data: PropTypes.object,
        chartData: PropTypes.array,
        colors: PropTypes.array,
        valueColor: PropTypes.string,
        link: PropTypes.object,
        type: PropTypes.string,
        handlePieCellClick: PropTypes.func,
        getTransactionItemTopPosition: PropTypes.func,
    };

    static defaultProps = {
        title: '',
        data: {},
        chartData: [],
        colors: [],
        valueColor: '',
        link: {
            to: '',
            caption: '',
        },
        type: '',
        handlePieCellClick: undefined,
        getTransactionItemTopPosition: undefined,
    };

    shouldComponentUpdate(nextProps) {
        if (nextProps.data !== this.props.data) return true;
        return false;
    }

    render() {
        const {
            title,
            data,
            chartData,
            colors,
            valueColor,
            link,
            type,
            handlePieCellClick,
            getTransactionItemTopPosition,
        } = this.props;

        return (
            <div className={styles.item}>
                <div className={styles.info}>
                    <div className={styles['info-content']}>
                        <p className={styles['info-title']}>
                            {title}
                        </p>
                        <p className={styles['info-value']}>
                            {`${formatNumber(data[type], true, 2)} ₽`}
                        </p>
                    </div>
                </div>
                <ResponsiveContainer width={'100%'} height={280}>
                    <PieChart>
                        <Pie
                            data={chartData.filter(entry => (
                                entry.name === 'Прочее' ||
                                entry.name === 'nodata' ||
                                entry.percent >= 2
                            ))}
                            innerRadius={115}
                            outerRadius={130}
                            fill="#8884d8"
                            paddingAngle={2}
                            dataKey="value"
                        >
                            {chartData
                                .map((entry, i) => (
                                    {
                                        entry,
                                        comp: (
                                            <Cell
                                                key={i}
                                                fill={colors[i % colors.length]}
                                                className={styles['pie-sector']}
                                                stroke={colors[i % colors.length]}
                                            />
                                        ),
                                    }
                                ))
                                .filter(entry => (
                                    entry.entry.name === 'Прочее' ||
                                    entry.entry.name === 'nodata' ||
                                    entry.entry.percent >= 2
                                ))
                                .map((entry, i) => (
                                    <Cell
                                        key={i}
                                        fill={colors[i % colors.length]}
                                        className={styles['pie-sector']}
                                        stroke={colors[i % colors.length]}
                                        style={{
                                            cursor: entry.name === 'Прочее' || entry.name === 'nodata'
                                                ? 'default'
                                                : 'pointer',
                                        }}
                                        onClick={() => handlePieCellClick(entry, type)}
                                    />
                                ))}
                        </Pie>
                        <Tooltip content={renderPieTooltip} />
                    </PieChart>
                </ResponsiveContainer>
                <div className={styles.list}>
                    {data && data.contragents && Object.keys(data.contragents[type]).length > 0
                        ? chartData.map((item, i) => {
                            if (item.name === 'Прочее') {
                                return null;
                            }

                            const contragentsData = data.contragents[type][item.name];
                            const percent = (contragentsData.total / data[type]) * 100;

                            return (
                                <TransactionItem
                                    key={i}
                                    color={colors[i]}
                                    data={contragentsData}
                                    percent={percent}
                                    name={item.name}
                                    type={{ type, color: valueColor }}
                                    getTransactionItemTopPosition={getTransactionItemTopPosition}
                                />
                            );
                        })
                        : (
                            <Link
                                to={link.to}
                                exact
                                className={styles.button}
                                style={{ color: valueColor }}
                            >
                                {link.caption}
                            </Link>
                        )
                    }
                </div>
            </div>
        );
    }
}
