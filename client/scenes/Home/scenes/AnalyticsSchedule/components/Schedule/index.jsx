import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { NavLink as Link } from 'react-router-dom';
import {
    AreaChart,
    Area,
    XAxis,
    YAxis,
    Tooltip,
    ResponsiveContainer,
} from 'recharts';
import moment from 'moment';

import getWord from 'utils/get_word';
import { formatNumber } from 'utils';

import SelectPeriod from 'components/ui/SelectPeriod';

import styles from './schedule.css';

const CustomTooltip = () => null;

moment.locale('ru');

class Schedule extends Component {
    render() {
        const props = this.props;

        const { data, activeLabel } = props;

        const hasData = Object.keys(data).length > 0;

        let dataMin = 0;
        let dataMax = 0;

        const areaChartData = hasData
            ? (
                Object.keys(data.dates).map((name) => {
                    const dayData = data.dates[name];

                    const day = {
                        name,
                        value: dayData.relativeBalance,
                        balance: dayData.balance,
                    };

                    if (dayData.relativeBalance < dataMin) {
                        dataMin = dayData.relativeBalance;
                    }

                    if (dayData.relativeBalance > dataMax) {
                        dataMax = dayData.relativeBalance;
                    }

                    return day;
                })
            )
            : [];

        if (hasData) {
            areaChartData.unshift({
                name: 'start',
                value: 0,
            });
        }

        const dataAbsMax = Math.max(Math.abs(dataMin), Math.abs(dataMax));

        const value = hasData && data.dates[activeLabel]
            ? data.dates[activeLabel].plus - data.dates[activeLabel].minus
            : 0;

        const valueColor = ['#3ac56c', '#e94d4d'];

        return (
            <div className={styles.container}>
                <div className={styles.head} data-visible={props.dataIsValid}>
                    {data.length !== 0 && activeLabel !== '' && (
                        <div className={styles.info}>
                            <p className={styles.date}>
                                {(() => {
                                    if (activeLabel === 'start') {
                                        return moment(this.selectPeriod.state.startDate).format('DD MMMM YYYY');
                                    }
                                    return data.length !== 0 && moment(activeLabel, 'DD MM YYYY').format('DD MMMM YYYY');
                                })()}
                            </p>
                            {activeLabel !== 'start' && (
                                <p
                                    className={styles.value}
                                    style={{ color: value >= 0 ? valueColor[0] : valueColor[1] }}
                                >
                                    {`${formatNumber(value, true, 2)} ₽`}
                                </p>
                            )}
                        </div>
                    )}
                    <div className={styles.wrap}>
                        <SelectPeriod
                            fetchData={props.fetchAnalyticsData}
                            ref={(el) => { this.selectPeriod = el; }}
                        />
                        <Link to="/home">
                            <div className={styles['analytics-toggle']} />
                        </Link>
                    </div>
                </div>
                <div className={styles.content} data-active={props.dataIsValid}>
                    <div style={{ height: 100 }}>
                        {data.length !== 0 && activeLabel !== '' && (
                            <div className={styles.operations}>
                                {activeLabel === 'start' && (
                                    <div className={styles['operations-wrap']}>
                                        <p
                                            className={styles['operations-value']}
                                            style={{
                                                color: data.openingBalance >= 0 ? valueColor[0] : valueColor[1],
                                            }}
                                        >
                                            {`${formatNumber(data.openingBalance, true, 2)} ₽`}
                                        </p>
                                        <p className={styles['operations-description']}>
                                            Остаток на счете
                                        </p>
                                    </div>
                                )}
                                {data.dates[activeLabel] && data.dates[activeLabel].plusCount !== 0 && (
                                    <div className={styles['operations-wrap']}>
                                        <p
                                            className={styles['operations-value']}
                                            style={{
                                                color: data.dates[activeLabel].plus >= 0 ? valueColor[0] : valueColor[1],
                                            }}
                                        >
                                            {`${formatNumber(data.dates[activeLabel].plus, true, 2)} ₽`}
                                        </p>
                                        <p className={styles['operations-description']}>
                                            {`
                                            ${data.dates[activeLabel].plusCount}
                                            ${getWord(data.dates[activeLabel].plusCount, ['операция', 'операции', 'операций'])}
                                        `}
                                        </p>
                                    </div>
                                )}
                                {data.dates[activeLabel] && data.dates[activeLabel].minusCount !== 0 && (
                                    <div className={styles['operations-wrap']}>
                                        <p
                                            className={styles['operations-value']}
                                            style={{
                                                color: -data.dates[activeLabel].minus >= 0 ? valueColor[0] : valueColor[1],
                                            }}
                                        >
                                            {`${formatNumber(-data.dates[activeLabel].minus, true, 2)} ₽`}
                                        </p>
                                        <p className={styles['operations-description']}>
                                            {`
                                            ${data.dates[activeLabel].minusCount}
                                            ${getWord(data.dates[activeLabel].minusCount, ['операция', 'операции', 'операций'])}
                                            `}
                                        </p>
                                    </div>
                                )}
                                {data.dates[activeLabel] && data.dates[activeLabel].balance !== 0 && (
                                    <div className={styles['operations-wrap']}>
                                        <p
                                            className={styles['operations-value']}
                                            style={{
                                                color: data.dates[activeLabel].balance >= 0 ? valueColor[0] : valueColor[1],
                                            }}
                                        >
                                            {`${formatNumber(data.dates[activeLabel].balance, true, 2)} ₽`}
                                        </p>
                                        <p className={styles['operations-description']}>
                                            Остаток на счете
                                        </p>
                                    </div>
                                )}
                            </div>
                        )}
                    </div>
                    {props.dataIsValid === false && (
                        <div className={styles.message}>
                            <p className={styles['message-text']}>
                                {props.messageText}
                            </p>
                            <div className={styles['message-wrap']}>
                                <Link to="/home/invoices" className={styles['message-button']}>
                                    Выставить счет
                                </Link>
                                <span>или</span>
                                <Link to="/home/payment" className={styles['message-button']}>
                                    Заплатить
                                </Link>
                            </div>
                        </div>
                    )}
                    <ResponsiveContainer width="100%" height={160}>
                        <AreaChart
                            data={areaChartData}
                            margin={{ left: 0, right: 0, top: 30 }}
                            onMouseMove={props.handleMouseMoveAreaChart}
                        >
                            <XAxis
                                dataKey="name"
                                tickLine={false}
                                axisLine={false}
                                stroke="#a8a8a8"
                                tickSize={20}
                                allowDataOverflow
                            />
                            <YAxis
                                hide
                                domain={[-dataAbsMax, dataAbsMax]}
                            />
                            <defs>
                                <linearGradient id="tooltipGradient">
                                    <stop offset={'0%'} stopColor="#0a357e" stopOpacity={1} />
                                    <stop offset={'100%'} stopColor="#e72e92" stopOpacity={1} />
                                </linearGradient>
                                <linearGradient id="areaGradient">
                                    <stop offset={'10%'} stopColor="#a8fae7" stopOpacity={1} />
                                    <stop offset={'20%'} stopColor="#a2f4fb" stopOpacity={1} />
                                    <stop offset={'30%'} stopColor="#9ae1fc" stopOpacity={1} />
                                    <stop offset={'40%'} stopColor="#92fdfc" stopOpacity={1} />
                                    <stop offset={'50%'} stopColor="#4efac8" stopOpacity={1} />
                                    <stop offset={'60%'} stopColor="#a8fae7" stopOpacity={1} />
                                    <stop offset={'70%'} stopColor="#a2f4fb" stopOpacity={1} />
                                    <stop offset={'80%'} stopColor="#9ae1fc" stopOpacity={1} />
                                    <stop offset={'90%'} stopColor="#92fdfc" stopOpacity={1} />
                                    <stop offset={'100%'} stopColor="#4efac8" stopOpacity={1} />
                                </linearGradient>
                            </defs>
                            <Area
                                type="monotone"
                                dataKey="value"
                                stroke="transparent"
                                fill="url(#areaGradient)"
                                fillOpacity={1}
                                activeDot={{ r: 4, fill: '#0a357e' }}
                            />
                            <Tooltip
                                cursor={{ stroke: '#0a357e' }}
                                content={<CustomTooltip />}
                            />
                        </AreaChart>
                    </ResponsiveContainer>
                </div>
            </div>
        );
    }
};

Schedule.propTypes = {
    fetchAnalyticsData: PropTypes.func,
    data: PropTypes.object,
    handleMouseMoveAreaChart: PropTypes.func,
    activeLabel: PropTypes.string,
    dataIsValid: PropTypes.bool,
    messageText: PropTypes.string,
};

Schedule.defaultProps = {
    fetchAnalyticsData: undefined,
    data: {},
    handleMouseMoveAreaChart: undefined,
    activeLabel: '',
    dataIsValid: true,
    messageText: '',
};

export default Schedule;
