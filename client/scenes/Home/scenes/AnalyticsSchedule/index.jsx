import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import moment from 'moment';
import { Scrollbars } from 'react-custom-scrollbars';
import { CSSTransition } from 'react-transition-group';

import { fetch, scrollbarsAnimations } from 'utils';
import Loader from 'components/Loader';

import Schedule from './components/Schedule';
import Analytics from './components/Analytics';

class AnalyticsSchedule extends Component {
    static propTypes = {
        selectedAccount: PropTypes.string,
    }

    static defaultProps = {
        selectedAccount: '',
    }

    constructor(props) {
        super(props);

        this.state = {
            isFetching: true,
            data: {},
            activeLabel: '',
            prevStatementData: {
                dates: [
                    { relativeBalance: 0, plus: 0, minus: 0 },
                    { relativeBalance: -50000, plus: 0, minus: 0 },
                    { relativeBalance: -1000, plus: 0, minus: 0 },
                    { relativeBalance: 100000, plus: 0, minus: 0 },
                    { relativeBalance: 0, plus: 0, minus: 0 },
                    { relativeBalance: 15000, plus: 0, minus: 0 },
                    { relativeBalance: 70000, plus: 0, minus: 0 },
                    { relativeBalance: -10000, plus: 0, minus: 0 },
                ],
                plus: 0,
                minus: 0,
            },
            dataIsValid: true,
            piePlusColors: [],
            pieMinusColors: [],
            messageText: 'Недостаточно данных для формирования графика',
        };
    }

    componentWillMount() {
        if (this.props.selectedAccount) {
            this.fetchAnalyticsData(moment().subtract(7, 'days'), moment());
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.selectedAccount !== this.props.selectedAccount) {
            this.fetchAnalyticsData(moment().subtract(7, 'days'), moment());
        }
    }

    fetchAnalyticsData = (startDate, endDate) => {
        this.setState({
            isFetching: true,
        });

        const options = {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify({
                dateFrom: startDate.format('DD.MM.YYYY'),
                dateTo: endDate.format('DD.MM.YYYY'),
                accountId: this.props.selectedAccount,
            }),
        };

        fetch('/api/v1/bfm/data', options)
            .then((response) => {
                if (response.status === 200) {
                    return response.json();
                }
                return Promise.reject();
            })
            .then((data) => {
                let statementData;

                if (data && data.dates.length > 0) {
                    statementData = Object.assign({}, data);
                    statementData.dates = {};

                    data.dates.forEach((item) => {
                        statementData.dates[item.date] = {
                            ...item,
                        };
                    });

                    this.setState({
                        dataIsValid: true,
                    });
                } else {
                    statementData = this.state.prevStatementData;

                    this.setState({
                        dataIsValid: false,
                    });
                }

                // const activeLabel = Object.keys(statementData.dates)[0];

                this.setState({
                    data: statementData,
                    activeLabel: 'start',
                    isFetching: false,
                });
            })
            .catch((error) => {
                console.log('error', error);
                this.setState({
                    messageText: 'При загрузке данных произошла ошибка',
                    dataIsValid: false,
                    isFetching: false,
                });
            });
    };

    handleMouseMoveAreaChart = (e) => {
        if (e.isTooltipActive && e.activeLabel !== this.state.activeLabel) {
            this.setState({
                activeLabel: e.activeLabel,
            });
        }
    }

    handlePieCellClick = (entry, type) => {
        if (entry.name === 'Прочее' || entry.name === 'nodata') return;

        const transactionItem = document.querySelector(`[data-transaction-name='${entry.name}'][data-transaction-type='${type}']`);

        const options = {
            direction: 'y',
            element: this.scrollbars,
            to: 415 + transactionItem.offsetTop,
            duration: 300,
            context: this,
        };

        if (transactionItem.dataset.open === 'false') {
            transactionItem.firstChild.click();
            setTimeout(() => {
                scrollbarsAnimations(options);
            }, 300);
        } else {
            scrollbarsAnimations(options);
        }
    }

    render() {
        return (
            <div
                style={{
                    position: 'relative',
                    width: '100%',
                    height: 'calc(100% - 10px)',
                }}
            >
                <Loader
                    visible={this.state.isFetching}
                    style={{
                        background: Object.keys(this.state.data).length !== 0
                            ? 'rgba(255, 255, 255, 0.7)'
                            : '#fff',
                    }}
                />
                <CSSTransition
                    timeout={500}
                    key="analytics"
                    classNames="fadeappear"
                    appear
                    in
                    style={{
                        position: 'relative',
                        width: '100%',
                        overflow: 'hidden',
                    }}
                >
                    <Scrollbars
                        style={{ width: '100%', overflow: 'hidden' }}
                        ref={(scrollbars) => { this.scrollbars = scrollbars; }}
                    >
                        <Schedule
                            fetchAnalyticsData={this.fetchAnalyticsData}
                            handleMouseMoveAreaChart={this.handleMouseMoveAreaChart}
                            {...this.state}
                        />
                        {this.state.dataIsValid === true && (
                            <Analytics
                                data={this.state.data}
                                handlePieCellClick={this.handlePieCellClick}
                            />
                        )}
                    </Scrollbars>
                </CSSTransition>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        selectedAccount: state.accounts.selectedAccount,
    };
}

export default connect(mapStateToProps)(AnalyticsSchedule);
