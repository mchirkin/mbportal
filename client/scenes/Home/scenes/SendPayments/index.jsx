import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { fetch } from 'utils';

import fetchNewPayments from 'modules/payments/new_payments/actions/fetch';
import setPaymentModalData from 'modules/payments/payment_modal/actions/set_data';
import setPaymentModalVisibility from 'modules/payments/payment_modal/actions/set_visibility';
import setBackUrl from 'modules/settings/actions/set_back_url';

import SceneWrapper from 'components/SceneWrapper';

import PaymentList from './components/PaymentList';

let cacheState = null;

class SendPayments extends Component {
    // static propTypes = {
    // }

    static contextTypes = {
        router: PropTypes.object.isRequired,
    };

    constructor(props) {
        super(props);

        this.state = {
            /** выбранные документы */
            selectedItems: [],
            /** список документов, по которым в процессе вызов сервиса отправки документа */
            isSendingList: [],
        };
    }

    componentWillMount() {
        if (cacheState) {
            this.setState(cacheState);
        }
    }

    selectItem = (item) => {
        const hasItem = this.state.selectedItems.find(el => el.platporDocument.id === item.platporDocument.id);

        if (hasItem) {
            this.setState({
                selectedItems: this.state.selectedItems.filter(el => el.platporDocument.id !== item.platporDocument.id),
            });
        } else {
            this.setState({
                selectedItems: [...this.state.selectedItems, item],
            });
        }
    }

    selectAll = () => {
        this.setState({
            selectedItems: [...this.props.sendPayments],
        });
    }

    clearAll = () => {
        this.setState({
            selectedItems: [],
        });
    }

    viewPayment = (data) => {
        let dataForView = data;
        if (data.platporDocument) {
            dataForView = data.platporDocument;
        }

        cacheState = this.state;

        this.props.setBackUrl('/home/send_payments');

        this.props.setPaymentModalData(Object.assign(dataForView, {
            income: false,
        }));

        this.props.setPaymentModalVisibility(true);

        // this.context.router.history.push('/home/payment');
    }

    /**
     * Вызов сервиса отправки документа в банк
     * @param {Object} data данные документа
     * @param {boolean} multiple массовая отправка
     * @return {Promise}
     */
    sendDocumentToBank = (data, multiple = false) => new Promise((resolve, reject) => {
        if (!multiple) {
            this.setState({
                isSendingList: [...this.state.isSendingList, data],
            });
        }

        return fetch('/api/v1/documents/send2bank', {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify({
                docModule: data.platporDocument.docModule,
                docType: data.platporDocument.docType,
                docId: data.platporDocument.id,
            }),
        })
            .then(response => response.text())
            .then((data) => {
                if (data.length > 0) {
                    const json = JSON.parse(data);
                    if (json.errorCode === '3004') {
                        return resolve({
                            msg: 'Для отправки документ должен быть подписан второй подписью',
                        });
                    }
                    if (json.errorText) {
                        return reject(json);
                    }
                }
                return resolve(data);
            })
            .then(() => {
                if (!multiple) {
                    this.setState({
                        isSendingList: this.state.isSendingList.filter(
                            el => el.platporDocument.id !== data.platporDocument.id
                        ),
                    });
                    this.props.fetchNewPayments();
                }
                return resolve(true);
            })
            .catch(err => reject(err));
    })

    sendDocuments = () => {
        const promises = this.state.selectedItems.map(el => this.sendDocumentToBank(el, true));
        this.setState({
            isDocumentsSending: true,
        });

        Promise.all(promises)
            .then((values) => {
                global.console.log(values);
                this.props.fetchNewPayments();
            })
            .catch(() => {
                this.props.fetchNewPayments();
            });
    }

    render() {
        return (
            <SceneWrapper>
                <PaymentList
                    list={this.props.sendPayments}
                    selectItem={this.selectItem}
                    selectAll={this.selectAll}
                    clearAll={this.clearAll}
                    viewPayment={this.viewPayment}
                    sendDocumentToBank={this.sendDocumentToBank}
                    sendDocuments={this.sendDocuments}
                    {...this.state}
                />
            </SceneWrapper>
        );
    }
}

function mapStateToProps(state) {
    return {
        sendPayments: state.payments.newPayments.signed,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        fetchNewPayments: bindActionCreators(fetchNewPayments, dispatch),
        setPaymentModalData: bindActionCreators(setPaymentModalData, dispatch),
        setPaymentModalVisibility: bindActionCreators(setPaymentModalVisibility, dispatch),
        setBackUrl: bindActionCreators(setBackUrl, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(SendPayments);
