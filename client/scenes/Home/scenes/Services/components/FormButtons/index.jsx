import React from 'react';
import Btn from 'components/ui/Btn';

import styles from './form-buttons.css';

const FormButtons = ({
    onCancel,
    onSubmit,
    style,
}) => (
    <div className={styles.wrapper} style={style}>
        <Btn
            bgColor="grey"
            caption="Назад"
            onClick={onCancel}
            style={{
                width: 470,
                marginRight: 270,
            }}
        />
        <Btn
            caption="Отправить"
            onClick={onSubmit}
            style={{
                width: 240,
            }}
        />
    </div>
);

export default FormButtons;
