import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Btn from 'components/ui/Btn';

import styles from './product-item.css';

const ProductItem = ({
    link,
    img,
    btnText,
    onBtnClick,
}) => {
    const button = (
        <Btn
            caption={btnText}
            bgColor="white"
            onClick={onBtnClick}
            style={{
                height: 46,
                marginTop: 26,
                fontSize: 14,
            }}
        />
    );
    return (
        <div className={styles.wrapper}>
            <div
                className={styles['product-image']}
                style={{
                    backgroundImage: `url(${img})`,
                }}
            />
            {link
                ? (
                    <Link to={link}>
                        {button}
                    </Link>
                )
                : button}
        </div>
    );
};

ProductItem.propTypes = {
    link: PropTypes.string,
    img: PropTypes.string,
    btnText: PropTypes.string,
    onBtnClick: PropTypes.func,
};

ProductItem.defaultProps = {
    link: '',
    img: '',
    btnText: '',
    onBtnClick: undefined,
};

export default ProductItem;
