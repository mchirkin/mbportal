import React from 'react';
import PropTypes from 'prop-types';
import ProductItem from './components/ProductItem';

import styles from './product-list.css';

const ProductList = ({
    showInternetAcquiringModal,
    showCorpCardModal,
    tariffCode,
}) => {
    const list = [
        {
            id: 'corp-card',
            link: tariffCode === 'TP_DEFAULT' ? '/home/services/corporate_card' : null,
            img: require('./img/serv_card@3x.png'),
            btnText: 'Бизнес-карта',
            onBtnClick: tariffCode !== 'TP_DEFAULT' ? showCorpCardModal : undefined,
        },
        {
            id: 'acquiring',
            link: '/home/services/acquiring',
            img: require('./img/serv_acq@3x.png'),
            btnText: 'Эквайринг',
        },
        /*
        {
            id: 'internet-acquiring',
            img: require('./img/serv_iacq@3x.png'),
            btnText: 'Интернет-эквайринг',
            onBtnClick: showInternetAcquiringModal,
        },
        */
    ];

    return (
        <div className={styles.wrapper}>
            <div className={styles.header}>
                Услуги банка
            </div>
            <div className={styles.list}>
                {list.map(el => (
                    <ProductItem
                        key={el.id}
                        {...el}
                    />
                ))}
            </div>
        </div>
    );
};

ProductList.propTypes = {
    showInternetAcquiringModal: PropTypes.func.isRequired,
    showCorpCardModal: PropTypes.func.isRequired,
    tariffCode: PropTypes.string,
};

ProductList.defaultProps = {
    tariffCode: '',
};

export default ProductList;
