import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import setMessageData from 'modules/info_message/actions/set_data';

import { fetch } from 'utils';

import CorporateCardForm from 'containers/forms/CorporateCard';
import Loader from 'components/Loader';

import FormButtons from '../../components/FormButtons';

class CorporateCard extends Component {
    static contextTypes = {
        router: PropTypes.object.isRequired,
    };

    static propTypes = {
        productList: PropTypes.array,
    };

    static defaultProps = {
        productList: [],
    };

    constructor(props) {
        super(props);

        this.state = {
            invalidFields: [],
        };
    }

    shouldComponentUpdate() {
        return true;
    }

    navigatoToServices = () => {
        this.context.router.history.push('/home/services');
    }

    submitRequest = () => {
        this.validateCorporateCard()
            .then((result) => {
                const defaultTarif = this.props.productList.data.find(p => p.code === 'TP_DEFAULT');
                const cardEntityInfo = defaultTarif.productList.find(p => p.code === 'CORP_CARD');
                const dbo = defaultTarif.productList.find(p => p.code === 'DBO');
                const acquiring = defaultTarif.productList.find(p => p.code === 'TRADE_EQU');

                if (result) {
                    const formattedData = this.getFormattedData();

                    const data = {
                        typeId: defaultTarif.id,
                        period: '0',
                        branchCode: this.props.orgInfo.branchCodeRef,
                        tariff_plan: 'DEFAULT',
                        operation: 'add',
                        products: [{
                            id: cardEntityInfo.id,
                            entityId: cardEntityInfo.entityId,
                            values: formattedData.values,
                        }],
                    };

                    if (dbo) {
                        const formattedDboData = {
                            id: dbo.id,
                            entityId: dbo.entityId,
                            values: {
                                surname_dbo: '',
                                first_name_dbo: '',
                                second_name_dbo: '',
                                phone_mob_dbo: '',
                                email: '',
                                rutoken_accept: '',
                                sms_accept: '',
                            },
                        };
                        data.products.push(formattedDboData);
                    }

                    if (acquiring) {
                        const formattedAcquiringData = {
                            id: acquiring.id,
                            entityId: acquiring.entityId,
                            values: {
                                surname_equ: '',
                                first_name_equ: '',
                                second_name_equ: '',
                                phone_num: '',
                                oktmo: '',
                                mobile_gsm: false,
                                mobile_wifi: false,
                                static_gsm: false,
                                static_ip: false,
                                need_pin_pad: false,
                                org_name_on_check: '',
                                dhcp: false,
                                static: false,
                                ip: '',
                                gateway: '',
                                network_mask: '',
                                address_check: '',
                                address_type_term: '',
                                code_cladr_term: '',
                                region_term: '',
                                district_term: '',
                                town_term: '',
                                settlement_term: '',
                                street_term: '',
                                street_type_term: '',
                                house_term: '',
                                building_term: '',
                                construction_term: '',
                                flat_term: '',
                                postcode_term: '',
                            },
                        };
                        data.products.push(formattedAcquiringData);
                    }

                    this.setState({
                        isFetching: true,
                    });

                    return fetch('/api/v1/tariff/create_request', {
                        method: 'POST',
                        credentials: 'include',
                        body: JSON.stringify(data),
                    })
                        .then(response => response.json())
                        .then((json) => {
                            if (!json.id) {
                                return Promise.reject('error');
                            }
                            this.sendToBank(json.id)
                                .then(() => {
                                    this.props.setMessageData({
                                        isVisible: true,
                                        header: 'Успешно!',
                                        body: 'Заявка отправлена в банк. Ожидайте уведомления банка по итогам рассмотрения заявки',
                                        btnText: 'Далее',
                                        callback: () => {
                                            this.props.setMessageData({ isVisible: false });
                                            this.context.router.history.push('/home');
                                        },
                                    });
                                })
                                .catch((err) => {
                                    // this.showErrorModal(err);
                                    this.setState({
                                        isFetching: false,
                                    });
                                    this.props.setMessageData({
                                        isVisible: true,
                                        header: 'Внимание!',
                                        body: 'Произошла ошибка при отправке заявки.',
                                        btnText: 'Назад',
                                        callback: () => this.props.setMessageData({ isVisible: false }),
                                    });
                                    console.log(err);
                                });
                        })
                        .catch((err) => {
                            // this.showErrorModal(err);
                            this.setState({
                                isFetching: false,
                            });
                            this.props.setMessageData({
                                isVisible: true,
                                header: 'Внимание!',
                                body: 'Произошла ошибка при отправке заявки.',
                                btnText: 'Назад',
                                callback: () => this.props.setMessageData({ isVisible: false }),
                            });
                            console.log(err);
                        });
                }
            });
    }

    sendToBank = (id) => {
        return fetch('/api/v1/tariff/send2bank', {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify({
                docId: id,
                docModule: 'ibankul_req',
                docType: 'client_request',
            }),
        })
            .then(response => response.text())
            .then((text) => {
                if (text.length > 0) {
                    const json = JSON.parse(text);
                    return Promise.reject(json);
                }
                // this.props.fetchTariffPlanRequests();
                return true;
            })
            .then((result) => {
                // this.showGoodModal(result);
                console.log('success', result);
            })
            .catch((err) => {
                return Promise.reject(err);
            });
    }

    validateCorporateCard = () => {
        const data = this.form.getData();
        const requiredInputs = [
            'registrationAddress',
            'homeAddress',
            'workAddress',
            'placeOfBirth',
        ];

        const arr = Object.keys(data);

        const invalidFields = arr.filter((key) => {
            if (key === 'dadata') {
                return false;
            }

            // Проверка на дозаплненность поля
            if (data[key].search('_') !== -1) {
                return true;
            }

            return !data[key];
        });

        requiredInputs.forEach((key) => {
            if (!invalidFields.includes(key)) {
                if (!Object.keys(data.dadata).includes(key)) {
                    invalidFields.push(key);
                }
            }
        });

        return new Promise((resolve) => {
            this.setState({
                invalidFields,
            }, () => {
                if (invalidFields && invalidFields.length > 0) {
                    resolve(false);
                    return;
                }

                resolve(true);
            });
        });
    }

    getFormattedData = () => {
        const data = this.form.getData();

        const {
            passportSeries,
            passportNumber,
            passportDate,
            passportIssued,
            passportCode,
            surname,
            name,
            middleName,
            latinSurname,
            latinName,
            dateOfBirth,
            placeOfBirth,
            homePhone,
            workPhone,
            mobilePhone,
            workName,
            workPosition,
            dadata,
        } = data;

        const formattedCorporateCardData = {
            // id: corpCard.id,
            // entityId: corpCard.entityId,
            values: {
                doc_type_id_hold: 'FS0001',
                series_hold: passportSeries,
                number_hold: passportNumber,
                date_delivery_hold: passportDate,
                who_issues_hold: passportIssued,
                division_code_hold: passportCode,
                card_type: 'VB',
                currency_code: 'RUB',
                surname_hold: surname,
                first_name_hold: name,
                second_name_hold: middleName,
                surname_lat_hold: latinSurname,
                first_name_lat_hold: latinName,
                person_date: dateOfBirth,
                birth_place: placeOfBirth,
                address_type_reg: 'reg',
                code_cladr_reg: dadata.registrationAddress.kladr_id,
                region_reg: dadata.registrationAddress.region,
                district_reg: dadata.registrationAddress.city_area,
                town_reg: dadata.registrationAddress.city,
                settlement_reg: dadata.registrationAddress.settlement,
                street_reg: dadata.registrationAddress.street,
                street_type_reg: dadata.registrationAddress.street_type,
                house_reg: dadata.registrationAddress.house,
                building_reg: dadata.registrationAddress.block,
                construction_reg: '',
                flat_reg: dadata.registrationAddress.flat,
                postcode_reg: dadata.registrationAddress.postal_code,
                address_type_fact: 'fact',
                code_cladr_fact: dadata.homeAddress.kladr_id,
                region_fact: dadata.homeAddress.region,
                district_fact: dadata.homeAddress.city_area,
                town_fact: dadata.homeAddress.city,
                settlement_fact: dadata.homeAddress.settlement,
                street_fact: dadata.homeAddress.street,
                street_type_fact: dadata.homeAddress.street_type,
                house_fact: dadata.homeAddress.house,
                building_fact: dadata.homeAddress.block,
                construction_fact: '',
                flat_fact: dadata.homeAddress.flat,
                postcode_fact: dadata.homeAddress.postal_code,
                address_type_job: 'job',
                code_cladr_job: dadata.workAddress.kladr_id,
                region_job: dadata.workAddress.region,
                district_job: dadata.workAddress.city_area,
                town_job: dadata.workAddress.city,
                settlement_job: dadata.workAddress.settlement,
                street_job: dadata.workAddress.street,
                street_type_job: dadata.workAddress.street_type,
                house_job: dadata.workAddress.house,
                building_job: dadata.workAddress.block,
                construction_job: '',
                flat_job: dadata.workAddress.flat,
                postcode_job: dadata.workAddress.postal_code,
                phone_home: homePhone,
                phone_job: workPhone,
                phone_mob: mobilePhone,
                org_title: workName,
                position: workPosition,
            },
        };

        return formattedCorporateCardData;
    }

    deleteInvalidInput = (id) => {
        if (this.state.invalidFields.includes(id)) {
            const newFields = this.state.invalidFields.filter(e => e !== id);

            this.setState({
                invalidFields: newFields,
            });
        }
    }

    render() {
        return (
            <div
                style={{
                    height: '100%',
                    overflow: this.props.productList.isFetching || this.state.isFetching ? 'hidden' : undefined,
                }}
            >
                <Loader visible={this.props.productList.isFetching || this.state.isFetching} />
                <CorporateCardForm
                    isVisible
                    invalidInputs={this.state.invalidFields}
                    deleteInvalidInput={this.deleteInvalidInput}
                    ref={(c) => { this.form = c; }}
                />
                <FormButtons
                    onCancel={this.navigatoToServices}
                    onSubmit={this.submitRequest}
                />
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        productList: state.settings.productList,
        orgInfo: state.user.orgInfo.data,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        setMessageData: bindActionCreators(setMessageData, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(CorporateCard);
