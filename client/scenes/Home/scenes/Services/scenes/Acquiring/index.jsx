import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import setMessageData from 'modules/info_message/actions/set_data';

import { fetch } from 'utils';

import AcquiringForm from 'containers/forms/Acquiring';
import Loader from 'components/Loader';

import FormButtons from '../../components/FormButtons';

class Acquiring extends Component {
    static contextTypes = {
        router: PropTypes.object.isRequired,
    };

    static propTypes = {
        productList: PropTypes.array,
    };

    static defaultProps = {
        productList: [],
    };

    constructor(props) {
        super(props);

        this.state = {
            invalidFields: [],
        };
    }

    navigatoToServices = () => {
        this.context.router.history.push('/home/services');
    }

    submitRequest = () => {
        this.validateAcquiring()
            .then((result) => {
                const defaultTarif = this.props.productList.data.find(p => p.code === 'TP_DEFAULT');
                const cardEntityInfo = defaultTarif.productList.find(p => p.code === 'CORP_CARD');
                const dbo = defaultTarif.productList.find(p => p.code === 'DBO');
                const acquiring = defaultTarif.productList.find(p => p.code === 'TRADE_EQU');

                if (result) {
                    const formattedData = this.getFormattedData();

                    const data = {
                        typeId: defaultTarif.id,
                        period: '0',
                        branchCode: this.props.orgInfo.branchCodeRef,
                        tariff_plan: 'DEFAULT',
                        operation: 'add',
                        products: [{
                            id: acquiring.id,
                            entityId: acquiring.entityId,
                            values: formattedData.values,
                        }],
                    };

                    if (dbo) {
                        const formattedDboData = {
                            id: dbo.id,
                            entityId: dbo.entityId,
                            values: {
                                surname_dbo: '',
                                first_name_dbo: '',
                                second_name_dbo: '',
                                phone_mob_dbo: '',
                                email: '',
                                rutoken_accept: '',
                                sms_accept: '',
                            },
                        };
                        data.products.push(formattedDboData);
                    }

                    if (cardEntityInfo) {
                        const formattedCardData = {
                            id: cardEntityInfo.id,
                            entityId: cardEntityInfo.entityId,
                            values: {
                                doc_type_id_hold: 'FS0001',
                                series_hold: '',
                                number_hold: '',
                                date_delivery_hold: '',
                                who_issues_hold: '',
                                division_code_hold: '',
                                card_type: 'VB',
                                currency_code: 'RUB',
                                surname_hold: '',
                                first_name_hold: '',
                                second_name_hold: '',
                                surname_lat_hold: '',
                                first_name_lat_hold: '',
                                person_date: '',
                                birth_place: '',
                                address_type_reg: 'reg',
                                code_cladr_reg: '',
                                region_reg: '',
                                district_reg: '',
                                town_reg: '',
                                settlement_reg: '',
                                street_reg: '',
                                street_type_reg: '',
                                house_reg: '',
                                building_reg: '',
                                construction_reg: '',
                                flat_reg: '',
                                postcode_reg: '',
                                address_type_fact: 'fact',
                                code_cladr_fact: '',
                                region_fact: '',
                                district_fact: '',
                                town_fact: '',
                                settlement_fact: '',
                                street_fact: '',
                                street_type_fact: '',
                                house_fact: '',
                                building_fact: '',
                                construction_fact: '',
                                flat_fact: '',
                                postcode_fact: '',
                                address_type_job: 'job',
                                code_cladr_job: '',
                                region_job: '',
                                district_job: '',
                                town_job: '',
                                settlement_job: '',
                                street_job: '',
                                street_type_job: '',
                                house_job: '',
                                building_job: '',
                                construction_job: '',
                                flat_job: '',
                                postcode_job: '',
                                phone_home: '',
                                phone_job: '',
                                phone_mob: '',
                                org_title: '',
                                position: '',
                            },
                        };
                        data.products.push(formattedCardData);
                    }

                    this.setState({
                        isFetching: true,
                    });

                    return fetch('/api/v1/tariff/create_request', {
                        method: 'POST',
                        credentials: 'include',
                        body: JSON.stringify(data),
                    })
                        .then(response => response.json())
                        .then((json) => {
                            if (!json.id) {
                                return Promise.reject('error');
                            }

                            const attachPromises = this.form.getData().fileList
                                .map(file => this.attachFile(json.id, file));

                            Promise.all(attachPromises)
                                .then((values) => {
                                    console.log(values);
                                    return this.sendToBank(json.id);
                                })
                                .then(() => {
                                    this.props.setMessageData({
                                        isVisible: true,
                                        header: 'Успешно!',
                                        body: 'Заявка отправлена в банк. Ожидайте уведомления банка по итогам рассмотрения заявки',
                                        btnText: 'Далее',
                                        callback: () => {
                                            this.props.setMessageData({ isVisible: false });
                                            this.context.router.history.push('/home');
                                        },
                                    });
                                })
                                .catch((err) => {
                                    this.setState({
                                        isFetching: false,
                                    });
                                    this.props.setMessageData({
                                        isVisible: true,
                                        header: 'Внимание!',
                                        body: 'Произошла ошибка при отправке заявки.',
                                        btnText: 'Назад',
                                        callback: () => this.props.setMessageData({ isVisible: false }),
                                    });
                                    // this.showErrorModal(err);
                                    console.log(err);
                                });
                        })
                        .catch((err) => {
                            // this.showErrorModal(err);
                            this.setState({
                                isFetching: false,
                            });
                            this.props.setMessageData({
                                isVisible: true,
                                header: 'Внимание!',
                                body: 'Произошла ошибка при отправке заявки.',
                                btnText: 'Назад',
                                callback: () => this.props.setMessageData({ isVisible: false }),
                            });
                            console.log(err);
                        });
                }
            });
    }

    sendToBank = (id) => {
        return fetch('/api/v1/tariff/send2bank', {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify({
                docId: id,
                docModule: 'ibankul_req',
                docType: 'client_request',
            }),
        })
            .then(response => response.text())
            .then((text) => {
                if (text.length > 0) {
                    const json = JSON.parse(text);
                    return Promise.reject(json);
                }
                // this.props.fetchTariffPlanRequests();
                return true;
            })
            .then((result) => {
                // this.showGoodModal(result);
                console.log('success', result);
            })
            .catch((err) => {
                return Promise.reject(err);
            });
    }

    validateAcquiring = () => {
        const data = this.form.getData();
        const requiredInputs = [
            'setupAddress',
            'checkSetupAddress',
        ];

        const arr = Object.keys(data);

        const acquiringInvalidFields = arr.filter((key) => {
            if (key === 'dadata') {
                return false;
            }

            if (key === 'fileList') {
                if (data[key].length === 0) {
                    return true;
                }
                return false;
            }

            if (key === 'needPinPad') {
                if (
                    data.terminalConnection === 'mobileWifi' ||
                    data.terminalConnection === 'mobileGsm'
                ) {
                    return false;
                }
            }

            if (key === 'communicationType') {
                if (
                    data.terminalConnection === 'mobileWifi' ||
                    data.terminalConnection === 'mobileGsm' ||
                    data.terminalConnection === 'staticGsm'
                ) {
                    return false;
                }
            }

            if (key === 'networkGateway' || key === 'ipAdress' || key === 'networkMask') {
                if (
                    data.terminalConnection === 'staticIp' &&
                    data.communicationType === 'static' &&
                    data[key] === ''
                ) {
                    return true;
                }
                if (
                    data.terminalConnection === '' &&
                    data[key] === ''
                ) {
                    return true;
                }
                return false;
            }

            // Проверка на дозаплненность поля
            if (data[key].search('_') !== -1) {
                return true;
            }

            return !data[key];
        });

        requiredInputs.forEach((key) => {
            if (!acquiringInvalidFields.includes(key)) {
                if (!Object.keys(data.dadata).includes(key)) {
                    acquiringInvalidFields.push(key);
                }
            }
        });

        return new Promise((resolve) => {
            this.setState({
                invalidFields: acquiringInvalidFields,
            }, () => resolve(true));
        });
    }

    getFormattedData = () => {
        const data = this.form.getData();

        const {
            posName,
            posSurname,
            posMiddleName,
            posContactPhone,
            posTwoDigits,
            terminalConnection,
            needPinPad,
            organizationName,
            communicationType,
            ipAdress,
            networkGateway,
            networkMask,
            checkSetupAddress,
            dadata,
        } = data;

        const formattedAcquiringData = {
            id: data.id,
            entityId: data.entityId,
            values: {
                surname_equ: posSurname,
                first_name_equ: posName,
                second_name_equ: posMiddleName,
                phone_num: posContactPhone,
                oktmo: posTwoDigits,
                mobile_gsm: terminalConnection === 'mobileGsm' && true,
                mobile_wifi: terminalConnection === 'mobileWifi' && true,
                static_gsm: terminalConnection === 'staticGsm' && true,
                static_ip: terminalConnection === 'staticIp' && true,
                need_pin_pad: needPinPad === 'pinPadNeeded' && true,
                org_name_on_check: organizationName,
                dhcp: communicationType === 'dhcp' && true,
                static: communicationType === 'static' && true,
                ip: ipAdress,
                gateway: networkGateway,
                network_mask: networkMask,
                address_check: checkSetupAddress,
                address_type_term: 'term',
                code_cladr_term: dadata.setupAddress.kladr_id,
                region_term: dadata.setupAddress.region,
                district_term: dadata.setupAddress.city_area,
                town_term: dadata.setupAddress.city,
                settlement_term: dadata.setupAddress.settlement,
                street_term: dadata.setupAddress.street,
                street_type_term: dadata.setupAddress.street_type,
                house_term: dadata.setupAddress.house,
                building_term: dadata.setupAddress.block,
                construction_term: '',
                flat_term: dadata.setupAddress.flat,
                postcode_term: dadata.setupAddress.postal_code,
            },
        };

        return formattedAcquiringData;
    }

    attachFile = (id, file) => {
        console.log(file);
        const formData = new FormData();
        formData.append('clientRequest', id);
        formData.append('typeId', '12357');
        formData.append('file', file, file.name);
        return fetch('/api/v1/documents/attachment/type', {
            method: 'POST',
            credentials: 'include',
            body: formData,
        }).then((response) => {
            if (response.ok) {
                if (response.status !== 200) {
                    return false;
                }
                return response.json();
            }
            return false;
        }).then((json) => {
            if (json === false || json.errorCode) {
                return Promise.reject(json);
            }
            return true;
        }).catch((err) => {
            return Promise.reject(err);
        });
    }

    deleteInvalidInput = (id) => {
        if (this.state.invalidFields.includes(id)) {
            const newFields = this.state.invalidFields.filter(e => e !== id);

            this.setState({
                invalidFields: newFields,
            });
        }
    }

    render() {
        return (
            <div
                style={{
                    height: '100%',
                    overflow: this.props.productList.isFetching || this.state.isFetching ? 'hidden' : undefined,
                }}
            >
                <Loader visible={this.props.productList.isFetching || this.state.isFetching} />
                <AcquiringForm
                    isVisible
                    invalidInputs={this.state.invalidFields}
                    deleteInvalidInput={this.deleteInvalidInput}
                    ref={(c) => { this.form = c; }}
                />
                <FormButtons
                    onCancel={this.navigatoToServices}
                    onSubmit={this.submitRequest}
                    style={{ marginTop: 28 }}
                />
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        productList: state.settings.productList,
        orgInfo: state.user.orgInfo.data,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        setMessageData: bindActionCreators(setMessageData, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Acquiring);
