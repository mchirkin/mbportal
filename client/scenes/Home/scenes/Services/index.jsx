import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Switch, Route } from 'react-router-dom';

import fetchProductList from 'modules/settings/actions/product_list/fetch';
import setMessageData from 'modules/info_message/actions/set_data';

import { fetch } from 'utils';

import SceneWrapper from 'components/SceneWrapper';
import Loader from 'components/Loader';

import ProductList from './components/ProductList';
import CorporateCard from './scenes/CorporateCard';
import Acquiring from './scenes/Acquiring';

class Services extends Component {
    static contextTypes = {
        router: PropTypes.object.isRequired,
    };

    static propTypes = {
        fetchProductList: PropTypes.func.isRequired,
        setMessageData: PropTypes.func.isRequired,
        user: PropTypes.object,
        org: PropTypes.object,
    };

    static defaultProps = {
        user: {},
        org: {},
    };

    constructor(props) {
        super(props);

        this.state = {
            isFetching: false,
        };
    }

    componentDidMount() {
        this.props.fetchProductList();
    }

    showInternetAcquiringModal = () => {
        this.props.setMessageData({
            isVisible: true,
            header: 'Заявка на интернет-эквайринг',
            body: 'Для отправки заявки нажмите "Далее"',
            btnText: 'Назад',
            callback: () => this.props.setMessageData({ isVisible: false }),
            showSubmitBtn: true,
            submitBtnText: 'Далее',
            submitCallback: () => {
                this.props.setMessageData({ isVisible: false });

                this.setState({
                    isFetching: true,
                });

                const { user, org } = this.props;

                fetch('/api/v1/msg/acquiring_request', {
                    method: 'POST',
                    credentials: 'include',
                    body: JSON.stringify({
                        user,
                        org,
                    }),
                })
                    .then(response => response.json())
                    .then((json) => {
                        this.setState({
                            isFetching: false,
                        });

                        let header = 'Ваша заявка успешно принята!';
                        let msg = 'Наш менеджер свяжется с вами в ближайшее время';

                        if (json.error) {
                            header = 'Что-то пошло не так...';
                            msg = 'Попробуйте отправить заявку еще раз';
                        }

                        this.showMessage({
                            header,
                            msg,
                            btnText: json.error ? 'Назад' : 'Далее',
                        });
                    })
                    .catch((err) => {
                        console.log(err);

                        this.setState({
                            isFetching: false,
                        });

                        const header = 'Что-то пошло не так...';
                        const msg = 'Попробуйте отправить заявку еще раз';

                        this.showMessage({
                            header,
                            msg,
                            btnText: 'Назад',
                        });
                    });
            },
        });
    }

    showCorpCardModal = () => {
        const { user, org } = this.props;

        fetch('/api/v1/msg/corp_card_request', {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify({
                user,
                org,
            }),
        })
            .then(response => response.json())
            .catch((err) => {
                console.log(err);
            });

        this.props.setMessageData({
            isVisible: true,
            header: 'Внимание!',
            body: 'На вашем тарифном плане не предусмотрен выпуск дополнительных корпоративных карт.',
            btnText: 'Назад',
            callback: () => this.props.setMessageData({ isVisible: false }),
        });
    }

    showMessage = ({
        header,
        msg,
        btnText,
    }) => {
        this.props.setMessageData({
            isVisible: true,
            header,
            body: msg,
            btnText,
            callback: () => this.props.setMessageData({ isVisible: false }),
        });
    }

    render() {
        let tariffCode = '';
        if (this.props.org) {
            tariffCode = this.props.org.tarifCode;
        }

        return (
            <SceneWrapper>
                <Loader visible={this.state.isFetching} style={{ backgroundColor: 'rgba(255, 255, 255, 0.8)' }} />
                <Switch>
                    <Route exact path="/home/services/corporate_card" component={CorporateCard} />
                    <Route exact path="/home/services/acquiring" component={Acquiring} />
                    <Route
                        exact
                        path="/home/services"
                        render={() => (
                            <ProductList
                                showInternetAcquiringModal={this.showInternetAcquiringModal}
                                showCorpCardModal={this.showCorpCardModal}
                                tariffCode={tariffCode}
                            />
                        )}
                    />
                </Switch>
            </SceneWrapper>
        );
    }
}

function mapStateToProps(state) {
    return {
        user: state.user.userInfo.data,
        org: state.user.orgInfo.data,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        fetchProductList: bindActionCreators(fetchProductList, dispatch),
        setMessageData: bindActionCreators(setMessageData, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Services);
