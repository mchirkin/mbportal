import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import moment from 'moment';

import setBackUrl from 'modules/settings/actions/set_back_url';
import { fetchInvoice } from 'modules/invoice/invoice';
import { fetchInvoiceTemplates } from 'modules/invoice/templates';
import { fetchNds } from 'modules/dictionary/nds';
import setMessageData from 'modules/info_message/actions/set_data';

import {
    getNdsValue,
    fetch,
    formatNumber,
    downloadBlob,
    emitter,
} from 'utils';

import InfoMessage from 'components/InfoMessage';
import Clipboard from 'components/ui/Clipboard';
import Loader from 'components/Loader';

import Buttons from '../../components/Buttons';
import RightPanel from '../../components/RightPanel';
import Form from '../../components/Form';
import TemplateNameModal from '../../components/TemplateNameModal';

import styles from './invoices.css';

let cacheState = {};

@withRouter
class FormContainer extends Component {
    static propTypes = {
        setBackUrl: PropTypes.func.isRequired,
        backUrl: PropTypes.string,
        invoiceList: PropTypes.object,
        templateList: PropTypes.object,
        orgInfo: PropTypes.object,
        accounts: PropTypes.array,
        nds: PropTypes.array,
        fetchNds: PropTypes.func.isRequired,
        fetchInvoice: PropTypes.func.isRequired,
        fetchInvoiceTemplates: PropTypes.func.isRequired,
        setMessageData: PropTypes.func.isRequired,
    }

    static defaultProps = {
        backUrl: '',
        invoiceList: {},
        templateList: {},
        orgInfo: {},
        accounts: [],
        nds: [],
    }

    static contextTypes = {
        router: PropTypes.object.isRequired,
    }

    constructor(props) {
        super(props);

        const initialState = {
            isFetching: false,
            /** подсказки на полях ввода */
            hintsDisabled: false,
            /** Список полянок */
            formFields: {
                /** Номер выставленного счета */
                invoiceNumber: '',
                /** Номер счета */
                accountNumber: '',
                /** Дата выставления счета */
                payFrom: moment().format('DD.MM.YYYY'),
                /** Оплатить до */
                payUpTo: null,
                /** Наименование плательщика или ИНН */
                payerName: '',
                /** ИНН */
                inn: '',
                /** КПП */
                kpp: '',
                /** Юр. адрес */
                legalAddress: '',
                /** Комментарий для плательщика */
                paymentDesc: '',
                /** Чекбокс отправить контрагенту уведомление... */
                invoicesCheck: false,
                /** Номер телефона */
                phoneNumber: '',
                /** Адрес электронной почты */
                emailAddress: '',
                /** Дата оплаты */
                paymentDate: '',
                /** Чекбокс выставления счета с НДС */
                invoiceWithNds: false,
                /** Чекбокс расчета товара с НДС */
                priceIncludesNds: false,
            },
            /** Список товаров или услуг */
            productList: [{
                /** Артикул */
                vendorCode: '',
                /** Наименование */
                name: '',
                /** Цена за единицу */
                price: '',
                /** Количество */
                quantity: '1.00',
                /** Единицы */
                unit: 'Штука',
                /** НДС */
                nds: {
                    rate: 0,
                    value: 0,
                    caption: 'Без НДС',
                },
                /** Общая стоимость */
                totalPrice: '',
            }],
            historyProductList: [],
            /** Итоговая сумма */
            totalSum: '0.00',
            /** Итоговый ндс */
            totalNds: '0.00',
            /** Итоговая сумма без ндс */
            totalSumWithoutNds: '0.00',
            /** Отображение таблица истории */
            showHistoryProductList: false,
            /** Отображать модальное окно для сохранения шаблона */
            templateModalVisibility: false,
            selectedTemplate: null,
            /** Список невалидных полей */
            invalidFields: [],
            requiredFields: [
                'invoiceNumber',
                'payFrom',
                'payerName',
                'inn',
                'kpp',
                'legalAddress',
                'payUpTo',
            ],
        };

        this.state = {
            ...initialState,
            ...cacheState,
        };
        this.initialState = initialState;
    }

    componentDidMount() {
        this.initialFetchInvoiceList();
        this.initialFetchInvoiceTemplatesList();
        this.props.fetchNds();
        this.getInitialInvoiceNumber();

        if (this.props.location.state && this.props.location.state.date) {
            const { date } = this.props.location.state;
            this.getDateValue(date, 'payFrom');
            if (this.form) {
                this.form.setDate('payFrom', date);
            }
        }
    }

    componentWillUnmount() {
        cacheState = Object.assign({}, this.state);
    }

    componentDidUpdate() {
        this.initialFetchInvoiceList();
        this.initialFetchInvoiceTemplatesList();
        this.getInitialInvoiceNumber();
    }

    orginazerDateChangeListener = (date) => {
        this.getDateValue(date, 'payFrom');
        if (this.form) {
            this.form.setDate('payFrom', date);
        }
    }

    /**
     * Переход на главный экран
     */
    navigateToHomePage = () => {
        if (this.props.backUrl !== '/home') {
            this.context.router.history.push(this.props.backUrl);
            this.props.setBackUrl('/home');
        } else {
            this.context.router.history.push('/home');
        }
    }

    /**
     * Обработчик для инпутов
     */
    handleInputChange = (e) => {
        const id = e.id || e.target.getAttribute('id');
        const value = e.target.value;
        const newFormFields = Object.assign({}, this.state.formFields);
        newFormFields[id] = e.target.value;

        this.setState({
            formFields: newFormFields,
        });

        if (this.state.invalidFields.includes(id)) {
            this.deleteInvalidInput(id);
        }

        if (id === 'payerName') {
            const payerName = e.target.value;
            if (payerName.length === 0) {
                this.setState({
                    contragentList: [],
                });
            } else {
                this.fetchContragentList(e.target.value).then((result) => {
                    if (this.state.formFields.payerName === payerName) {
                        if (result) {
                            const contragentList = Array.isArray(result.corrDicElement)
                                ? result.corrDicElement
                                : [result.corrDicElement];

                            contragentList.forEach((agent) => {
                                agent.name = agent.fullname;
                            });

                            console.log(contragentList);
                            this.setState({
                                contragentList,
                            });
                        } else {
                            this.setState({
                                contragentList: null,
                            });
                        }
                    }
                });

                if (/^\d+$/.test(value)) {
                    if (/^(\d{5}|\d{10}|\d{12})$/.test(value)) {
                        const innValue = value;
                        this.setState({
                            konturData: null,
                            konturDataIsFetching: true,
                        });
                        this.getContragentDataFromKonturFocus({ inn: value }).then((data) => {
                            if (innValue === this.state.formFields.payerName) {
                                const info = data ? (data.CompanyInfo || data.IpInfo) : null;
                                const fio = info && info.fio ? `ИП ${info.fio}` : null;
                                const newState = {};
                                if (info) {
                                    newState.inn = info.inn || '';
                                    newState.kpp = info.kpp || '0';
                                    newState.payerName = info.longName || info.shortName || fio;
                                }

                                this.setState({
                                    konturData: data === null ? false : data,
                                    konturDataIsFetching: false,
                                    formFields: {
                                        ...this.state.formFields,
                                        ...newState,
                                    },
                                });
                            }
                        });
                    } else {
                        this.setState({
                            konturData: null,
                            konturDataIsFetching: false,
                        });
                    }
                }
            }
        }

        if (id === 'inn') {
            this.fetchKonturFocus(value);
        }
    }

    handleInputAutocompleteSelect = (id, el) => {
        const newFormFields = Object.assign({}, this.state.formFields);
        newFormFields[id] = el.name;

        if (this.state.invalidFields.includes(id)) {
            this.deleteInvalidInput(id);
        }

        if (id === 'payerName') {
            newFormFields.inn = el.inn;
            newFormFields.kpp = el.kpp;
        }

        this.setState({
            formFields: newFormFields,
        });
    }

    /**
     * Обработчик для чекбоксов
     */
    handleCheckboxChange = (e) => {
        const id = e.id || e.target.getAttribute('for');
        const newFormFields = Object.assign({}, this.state.formFields);

        newFormFields[id] = !this.state.formFields[id];

        if (id === 'invoiceWithNds') {
            if (this.state.formFields.invoiceWithNds && this.state.formFields.priceIncludesNds) {
                newFormFields.priceIncludesNds = false;
            }

            if (this.state.formFields.invoiceWithNds) {
                const newProductList = [...this.state.productList];
                newProductList.forEach((product, i) => {
                    newProductList[i].nds = this.props.nds.find(el => el.rate === 0);
                });

                this.setState({
                    productList: newProductList,
                }, () => {
                    newProductList.forEach((product, i) => {
                        this.priceCalculation(i, product.price, product.quantity);
                    });
                });
            }
        } else if (id === 'priceIncludesNds') {
            this.setState({
                formFields: newFormFields,
            }, () => {
                this.state.productList.forEach((product, i) => {
                    this.priceCalculation(i, product.price, product.quantity);
                });
            });
            return;
        }

        this.setState({
            formFields: newFormFields,
        });
    }

    /**
     * Добавление строки в таблицу продуктов
     */
    addProduct = () => {
        const newProductList = [...this.state.productList];
        newProductList.push({
            vendorCode: '',
            name: '',
            price: '',
            quantity: '1.00',
            unit: 'Штука',
            nds: this.props.nds.find(el => el.rate === 0),
            totalPrice: '',
        });

        this.setState({
            productList: newProductList,
        });
    }

    /**
     * Восстановление строки в таблицу продуктов
     */
    restoreProduct = (i) => {
        const newProductList = [...this.state.productList];
        const newHistoryProductList = [...this.state.historyProductList];

        newProductList.push(newHistoryProductList[i]);
        newHistoryProductList.splice(i, 1);

        this.setState({
            productList: newProductList,
            historyProductList: newHistoryProductList,
        }, () => {
            this.totalCalculation();
        });
    }

    /**
     * Удаление строки в таблице продуктов (если последняя, очищает все поля)
     */
    removeProduct = (i) => {
        const newProductList = [...this.state.productList];
        const newHistoryProductList = [...this.state.historyProductList];

        if (newProductList[i].name !== '') {
            newHistoryProductList.push(newProductList[i]);
            this.setState({
                historyProductList: newHistoryProductList,
            });
        }

        if (newProductList.length <= 1) {
            this.setState({
                productList: [{
                    vendorCode: '',
                    name: '',
                    price: '',
                    quantity: '1.00',
                    unit: 'Штука',
                    nds: this.props.nds.find(el => el.rate === 0),
                    totalPrice: '',
                }],
            }, () => {
                this.totalCalculation();
            });
        } else {
            newProductList.splice(i, 1);
            this.setState({
                productList: newProductList,
            }, () => {
                this.totalCalculation();
            });
        }
    }

    /**
     * Переключение отображения подсказок при наведении на поля ввода
     */
    toggleHints = () => {
        this.setState({
            hintsDisabled: !this.state.hintsDisabled,
        });
    }

    /**
     * Переключение таблицы с историей товаров и услуг
     */
    toggleHistoryProductList = () => {
        this.setState({
            showHistoryProductList: !this.state.showHistoryProductList,
        });
    }

    /**
     * Получения даты платежа
     */
    getDateValue = (date, id) => {
        const newFormFields = Object.assign({}, this.state.formFields);
        newFormFields[id] = date;

        if (this.state.invalidFields.includes(id)) {
            this.deleteInvalidInput(id);
        }

        this.setState({
            formFields: newFormFields,
        });
    }

    /**
     * Обработчик для инпутов таблицы продуктов
     */
    handleProductChange = (id, val, i) => {
        const newProductList = [...this.state.productList];
        newProductList[i][id] = val;

        this.setState({
            productList: newProductList,
        }, () => {
            this.priceCalculation(i, newProductList[i].price, newProductList[i].quantity);
        });
    }

    /**
     * Обработчик изменения поля ввода суммы и количества
     */
    handleTablePriceChange = (id, val, i) => {
        const value = val === '-' ? '' : val.replace(/,/, '.');

        const newProductList = [...this.state.productList];
        newProductList[i][id] = value;

        this.setState({
            productList: newProductList,
        }, () => {
            this.priceCalculation(i, newProductList[i].price, newProductList[i].quantity);
        });
    }

    /**
     * Расчет суммы продукта
     */
    priceCalculation = (i, price, quantity) => {
        const toNumber = value => parseFloat(value.replace(/\s/g, ''), 10);
        const toFormat = value => formatNumber(value, true, 2).replace(/,/, '.');

        const unformattedPrice = price === '' ? 0 : toNumber(price);
        const unformattedQuantity = quantity === '' ? 1 : toNumber(quantity);

        const newProductList = [...this.state.productList];

        let totalPrice = 0;
        const result = unformattedPrice * unformattedQuantity;

        if (this.state.formFields.priceIncludesNds) {
            totalPrice = toFormat(result);
        } else {
            let nds = (result / 100) * newProductList[i].nds.rate;

            if (typeof nds === 'string') {
                nds = toNumber(nds);
            }

            const sumWithNds = result + nds;

            totalPrice = toFormat(sumWithNds);
        }

        newProductList[i].totalPrice = totalPrice;

        this.setState({
            productList: newProductList,
        }, () => {
            this.totalCalculation();
        });
    }

    /**
     * Расчет итоговой суммы и ндс
     */
    totalCalculation = () => {
        const toNumber = value => parseFloat(value.replace(/\s/g, ''), 10);
        const toFormat = value => formatNumber(value, true, 2).replace(/,/, '.');

        const newProductList = [...this.state.productList];

        const unformattedTotalSum = newProductList
            .map(el => (isNaN(toNumber(el.totalPrice)) ? 0 : toNumber(el.totalPrice)))
            .reduce((accumulator, currentValue) => accumulator + currentValue);

        const unformattedtotalNds = this.props.nds
            .map((nds) => {
                let result = 0;
                newProductList.forEach((product) => {
                    if (nds.rate === product.nds.rate) {
                        result += isNaN(toNumber(product.totalPrice)) ? 0 : toNumber(product.totalPrice);
                    }
                });

                return getNdsValue(result, nds.rate);
            })
            .reduce((accumulator, currentValue) => Number(accumulator) + Number(currentValue));

        const unformattedWithoutNds = unformattedTotalSum - unformattedtotalNds;

        const totalNds = unformattedtotalNds === 0
            ? '0.00'
            : toFormat(unformattedtotalNds);

        const totalSum = isNaN(unformattedTotalSum) || toFormat(unformattedTotalSum) === '-'
            ? '0.00'
            : toFormat(unformattedTotalSum);

        const totalSumWithoutNds = isNaN(unformattedWithoutNds) || toFormat(unformattedWithoutNds) === '-'
            ? '0.00'
            : toFormat(unformattedWithoutNds);

        this.setState({ totalSum, totalSumWithoutNds, totalNds });
    }

    showTemplateModal = () => {
        this.setState({
            templateModalVisibility: true,
            templateName: this.state.formFields.payerName,
        });
    }

    closeTemplateModal = () => {
        this.setState({
            templateModalVisibility: false,
            templateName: '',
        });
    }

    handleTemplateNameChange = (e) => {
        this.setState({
            templateName: e.target.value,
        });
    }

    changeTemplateName = (data) => {
        return fetch('/api/v1/invoice/template/update', {
            credentials: 'include',
            method: 'POST',
            body: JSON.stringify(data),
        })
            .then(response => response.json())
            .then((result) => {
                console.log(result);
            })
            .catch((err) => {
                console.log(err);
            });
    }

    setData = (data) => {
        const newState = {
            selectedHistoryItem: data.selectedHistoryItem,
            selectedTemplate: data.selectedHistoryItem ? null : data._id,
            templateName: data.templateName,
            formFields: {
                ...this.state.formFields,
                payerName: data.payerName,
                inn: data.inn,
                kpp: data.kpp,
                legalAddress: data.legalAddress,
                paymentDesc: data.paymentDesc,
                invoicesCheck: data.sendNotification,
                phoneNumber: data.phoneNumber,
                emailAddress: data.emailAddress,
                invoiceWithNds: data.withNds,
                priceIncludesNds: data.priceIncludesNds,
                payFrom: data.selectedHistoryItem
                    ? moment(data.invoiceDate).format('DD.MM.YYYY')
                    : this.state.formFields.payFrom,
                payUpTo: data.selectedHistoryItem
                    ? moment(data.paymentDate).format('DD.MM.YYYY')
                    : '',
            },
            productList: data.productList.map(product => ({
                vendorCode: product.vendorCode,
                name: product.name,
                price: formatNumber(product.price),
                quantity: formatNumber(product.quantity),
                totalPrice: formatNumber(product.price * product.quantity),
                unit: product.units,
                nds: this.props.nds.find(item => item.caption === product.nds),
            })),
        };

        this.setState(newState, () => {
            this.totalCalculation();
            if (data.selectedHistoryItem) {
                this.form.setDate('payFrom', moment(data.invoiceDate).format('DD.MM.YYYY'));
                this.form.setDate('payUpTo', moment(data.paymentDate).format('DD.MM.YYYY'));
            }
        });
    }

    /**
     * ====================
     * Вызовы сервисов
     * ====================
     */

    initialFetchInvoiceList = () => {
        if (this.props.invoiceList && this.props.orgInfo && !this.invoiceListLoaded) {
            this.invoiceListLoaded = true;
            this.props.fetchInvoice({ extCorporateRef: this.props.orgInfo.extCorporateRef });
        }
    }

    initialFetchInvoiceTemplatesList = () => {
        if (this.props.templateList && this.props.orgInfo && !this.templateListLoaded) {
            this.templateListLoaded = true;
            this.props.fetchInvoiceTemplates({ extCorporateRef: this.props.orgInfo.extCorporateRef });
        }
    }

    getInitialInvoiceNumber = () => {
        if (this.props.orgInfo && !this.invoiceNumberLoaded) {
            this.invoiceNumberLoaded = true;
            this.getInvoiceNumber();
        }
    }

    getInvoiceNumber = () => {
        fetch('/api/v1/invoice/number', {
            credentials: 'include',
            method: 'POST',
            body: JSON.stringify({
                extCorporateRef: this.props.orgInfo.extCorporateRef,
            }),
        })
            .then(response => response.json())
            .then((json) => {
                this.setState({
                    formFields: {
                        ...this.state.formFields,
                        invoiceNumber: json.number,
                    },
                });
            })
            .catch((err) => {
                console.log(err);
            });
    }

    saveTemplate = () => {
        const data = {
            templateName: this.state.templateName,
            extCorporateRef: this.props.orgInfo.extCorporateRef,
            payerName: this.state.formFields.payerName,
            inn: this.state.formFields.inn,
            kpp: this.state.formFields.kpp,
            legalAddress: this.state.formFields.legalAddress,
            paymentDesc: this.state.formFields.paymentDesc,
            sendNotification: this.state.formFields.invoicesCheck,
            phoneNumber: this.state.formFields.phoneNumber,
            emailAddress: this.state.formFields.emailAddress,
            productList: this.state.productList.map(product => ({
                vendorCode: product.vendorCode,
                name: product.name,
                price: parseFloat(product.price.replace(/\s/g, '').replace(',', '.'), 10),
                quantity: parseFloat(product.quantity.replace(/\s/g, '').replace(',', '.'), 10),
                units: product.unit,
                nds: product.nds.caption,
            })),
            withNds: this.state.formFields.invoiceWithNds,
            priceIncludesNds: this.state.formFields.priceIncludesNds,
        };

        let url = '/api/v1/invoice/template/create';

        if (this.state.selectedTemplate) {
            data._id = this.state.selectedTemplate;
            url = '/api/v1/invoice/template/update';
        }

        this.setState({
            isFetching: false,
        });

        fetch(url, {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify(data),
        })
            .then(response => response.json())
            .then((json) => {
                console.log(json);
                if (json.success) {
                    this.props.setMessageData({
                        isVisible: true,
                        header: 'Успешно!',
                        body: this.state.selectedTemplate ? 'Шаблон изменен' : 'Шаблон сохранен',
                        btnText: 'Далее',
                        callback: () => this.props.setMessageData({ isVisible: false }),
                    });
                    this.props.fetchInvoiceTemplates({ extCorporateRef: this.props.orgInfo.extCorporateRef });
                    this.setState({
                        templateModalVisibility: false,
                        templateName: '',
                        isFetching: false,
                    });
                } else {
                    this.props.setMessageData({
                        isVisible: true,
                        header: 'Внимание!',
                        body: 'Произошла ошибка при сохранении.',
                        btnText: 'Назад',
                        callback: () => this.props.setMessageData({ isVisible: false }),
                    });
                    this.setState({
                        isFetching: false,
                    });
                }
            })
            .catch((err) => {
                console.log(err);
                this.setState({
                    isFetching: false,
                });
            });
    }

    deleteTemplate = (id) => {
        fetch('/api/v1/invoice/template/delete', {
            credentials: 'include',
            method: 'POST',
            body: JSON.stringify({
                id,
            }),
        })
            .then(response => response.json())
            .then(() => {
                if (this.state.selectedTemplate === id) {
                    this.setState({
                        selectedTemplate: null,
                    });
                }
                this.props.fetchInvoiceTemplates({ extCorporateRef: this.props.orgInfo.extCorporateRef });
            })
            .catch((err) => {
                console.log(err);
            });
    }

    saveInvoiceWithValidate = () => {
        this.validateForms()
            .then((response) => {
                if (response === true) {
                    this.saveInvoice();
                } else {
                    this.props.setMessageData({
                        isVisible: true,
                        header: 'Внимание!',
                        body: 'Произошла ошибка при сохранении. Проверьте заполненность всех полей.',
                        btnText: 'Назад',
                        callback: () => this.props.setMessageData({ isVisible: false }),
                    });
                }
            });
    }

    saveInvoice = () => {
        const account = this.props.accounts.find(acc => acc.type !== '7' && acc.currencyCode === '810');

        this.setState({
            isFetching: true,
        });

        fetch('/api/v1/invoice/create', {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify({
                extCorporateRef: this.props.orgInfo.extCorporateRef,
                payerName: this.state.formFields.payerName,
                inn: this.state.formFields.inn,
                kpp: this.state.formFields.kpp,
                legalAddress: this.state.formFields.legalAddress,
                paymentDesc: this.state.formFields.paymentDesc,
                sendNotification: this.state.formFields.invoicesCheck,
                phoneNumber: this.state.formFields.phoneNumber,
                emailAddress: this.state.formFields.emailAddress,
                providerInn: this.props.orgInfo.INN,
                providerFullName: this.props.orgInfo.fullName,
                providerBankBik: account.bankingInformation.bic,
                providerBank: 'АКБ РосЕвроБанк (АО)',
                providerCorrespondentAccount: account.bankingInformation.correspondentAccount,
                providerAccount: account.number,
                productList: this.state.productList.map(product => ({
                    vendorCode: product.vendorCode,
                    name: product.name,
                    price: parseFloat(product.price.replace(/\s/g, '').replace(',', '.'), 10),
                    quantity: parseFloat(product.quantity.replace(/\s/g, '').replace(',', '.'), 10),
                    units: product.unit,
                    nds: product.nds.caption,
                })),
                withNds: this.state.formFields.invoiceWithNds,
                priceIncludesNds: this.state.formFields.priceIncludesNds,
                ndsTotal: parseFloat(this.state.totalNds.replace(/\s/g, '').replace(',', '.'), 10),
                sumTotal: parseFloat(this.state.totalSum.replace(/\s/g, '').replace(',', '.'), 10),
                invoiceDate: moment(this.state.formFields.payFrom, 'DD.MM.YYYY').toISOString(),
                paymentDate: moment(this.state.formFields.payUpTo, 'DD.MM.YYYY').toISOString(),
                invoiceNumber: this.state.formFields.invoiceNumber,
            }),
        })
            .then(response => response.json())
            .then((json) => {
                this.setState({
                    isFetching: false,
                });
                console.log(json);
                const linkHref = `${document.location.origin}/invoice/${json.id}`;

                if (json.success) {
                    this.props.setMessageData({
                        isVisible: true,
                        header: 'Успешно!',
                        body: (
                            <div>
                                {this.state.formFields.invoicesCheck
                                    ? 'Счет отправлен контрагенту и доступен по ссылке:'
                                    : 'Счет сформирован и доступен по ссылке:'}<br />
                                <a href={linkHref}>{linkHref}</a>
                                <Clipboard
                                    clipboardText={linkHref}
                                    style={{ display: 'inline-block', marginLeft: 5 }}
                                >
                                    <div
                                        title="Скопировать ссылку"
                                        style={{
                                            position: 'relative',
                                            top: 1,
                                            width: 18,
                                            height: 18,
                                            backgroundImage: `url(${require('./img/copypaste.svg')})`,
                                            backgroundPosition: 'center',
                                            backgroundRepeat: 'no-repeat',
                                            backgroundSize: 'contain',
                                            cursor: 'pointer',
                                        }}
                                    />
                                </Clipboard>
                            </div>
                        ),
                        btnText: 'Далее',
                        callback: () => this.props.setMessageData({ isVisible: false }),
                    });

                    this.setState(this.initialState, () => {
                        this.removeProduct(0);
                        this.getInvoiceNumber();
                    });
                    this.props.fetchInvoice({ extCorporateRef: this.props.orgInfo.extCorporateRef });
                } else {
                    this.props.setMessageData({
                        isVisible: true,
                        header: 'Внимание!',
                        body: 'Произошла ошибка при сохранении. Проверьте заполненность всех полей в списке товаров/услуг.',
                        btnText: 'Назад',
                        callback: () => this.props.setMessageData({ isVisible: false }),
                    });
                }
            })
            .catch((err) => {
                console.log(err);
                this.props.setMessageData({
                    isVisible: true,
                    header: 'Внимание!',
                    body: 'Произошла ошибка при сохранении',
                    btnText: 'Назад',
                    callback: () => this.props.setMessageData({ isVisible: false }),
                });
                this.setState({
                    isFetching: false,
                });
            });
    }

    printDOCX = () => {
        const account = this.props.accounts.find(acc => acc.type !== '7' && acc.currencyCode === '810');

        this.setState({
            isFetching: true,
        });

        fetch('/invoice/docx', {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify({
                payerName: this.state.formFields.payerName,
                inn: this.state.formFields.inn,
                kpp: this.state.formFields.kpp,
                legalAddress: this.state.formFields.legalAddress,
                providerInn: this.props.orgInfo.INN,
                providerFullName: this.props.orgInfo.fullName,
                providerBankBik: account.bankingInformation.bic,
                providerBank: 'АКБ РосЕвроБанк (АО)',
                providerCorrespondentAccount: account.bankingInformation.correspondentAccount,
                providerAccount: account.number,
                productList: this.state.productList.map(product => ({
                    vendorCode: product.vendorCode,
                    name: product.name,
                    price: parseFloat(product.price.replace(/\s/g, '').replace(',', '.'), 10),
                    quantity: parseFloat(product.quantity.replace(/\s/g, '').replace(',', '.'), 10),
                    units: product.unit,
                    nds: product.nds.caption,
                })),
                withNds: this.state.formFields.invoiceWithNds,
                ndsTotal: parseFloat(this.state.totalNds.replace(/\s/g, '').replace(',', '.'), 10),
                sumTotal: parseFloat(this.state.totalSum.replace(/\s/g, '').replace(',', '.'), 10),
                invoiceDate: moment(this.state.formFields.payFrom, 'DD.MM.YYYY').toISOString(),
                paymentDate: moment(this.state.formFields.payUpTo, 'DD.MM.YYYY').toISOString(),
                invoiceNumber: this.state.formFields.invoiceNumber,
            }),
        })
            .then(response => response.blob())
            .then((blob) => {
                this.setState({
                    isFetching: false,
                });

                downloadBlob(blob, `invoice-${this.state.formFields.invoiceNumber}.docx`);
            })
            .catch((err) => {
                console.log(err);
            });
    }

    handleHistoryItemClick = (data) => {
        this.setData({
            ...data,
            selectedHistoryItem: data._id,
        });
    }

    saveHistoryDOCX = (id) => {
        this.setState({
            isFetching: true,
        });

        fetch('/invoice/docx', {
            method: 'POST',
            body: JSON.stringify({
                id,
            }),
        })
            .then(response => response.blob())
            .then((blob) => {
                this.setState({
                    isFetching: false,
                });

                downloadBlob(blob, `${id}.docx`);
            })
            .catch((err) => {
                console.log(err);
            });
    }

    /**
     * Получить список контрганетов по имени из справочника
     * @param {string} name имя контрагента для поиска в справочнике
     * @return {Promise}
     */
    fetchContragentList = name => fetch(encodeURI(`/api/v1/contragent/list?name=${name}`), {
        credentials: 'include',
    })
        .then(response => response.json()).then(json => json).catch((err) => {
            console.log(err);
        })

    /**
     * Получение фактов о контрагенте из Контур.Фокус Светофор
     * @param {string} inn ИНН контрагента
     * @return {Promise}
     */
    getContragentDataFromKonturFocus = ({ inn }) => {
        // Вызываем сервис, если у клиента платная подписка на сервис Светофор
        if (this.props.orgInfo && this.props.orgInfo.konturFocusSubscr === 'paid') {
            return fetch('/api/v1/contragent/get_facts', {
                credentials: 'include',
                method: 'POST',
                body: JSON.stringify({
                    inn,
                }),
            })
                .then(response => response.json())
                .then((json) => {
                    if (json && json.errorCode) {
                        return Promise.resolve(null);
                    }
                    return json;
                });
        }
        return Promise.resolve(null);
    }

    fetchKonturFocus = (value) => {
        if (value.length === 5 || value.length === 10 || value.length === 12) {
            const innValue = value;
            this.setState({
                konturData: null,
                konturDataIsFetching: true,
            });
            this.getContragentDataFromKonturFocus({ inn: value }).then((data) => {
                if (innValue === this.state.formFields.inn) {
                    const info = data ? (data.CompanyInfo || data.IpInfo) : null;
                    const fio = info && info.fio ? `ИП ${info.fio}` : null;
                    const newState = {};

                    if (info) {
                        newState.kpp = info.kpp || '0';
                        newState.payerName = this.state.formFields.payerName || info.longName || info.shortName || fio;
                    }

                    this.setState({
                        konturData: data === null ? false : data,
                        konturDataIsFetching: false,
                        formFields: {
                            ...this.state.formFields,
                            ...newState,
                        },
                    });
                }
            });
        } else {
            this.setState({
                konturData: null,
                konturDataIsFetching: false,
            });
        }
    }

    validateForms = () => {
        const data = this.state.formFields;

        const invalidFields = this.state.requiredFields.filter((key) => {
            // Проверка на дозаплненность поля
            if (data[key] !== null && data[key].toString().search('_') !== -1) {
                return true;
            }

            return !data[key];
        });

        const validContactDetails =
            (data.phoneNumber !== '' && data.phoneNumber.toString().search('_') === -1) ||
            data.emailAddress !== '';

        if (data.invoicesCheck && !validContactDetails) {
            invalidFields.push('emailAddress');
            invalidFields.push('phoneNumber');
        }

        return new Promise((resolve) => {
            this.setState({
                invalidFields,
            }, () => {
                if (invalidFields && invalidFields.length > 0) {
                    resolve(false);
                    return;
                }

                resolve(true);
            });
        });
    }

    deleteInvalidInput = (id) => {
        if (this.state.invalidFields.includes(id)) {
            const newFields = this.state.invalidFields.filter(e => e !== id);

            this.setState({
                invalidFields: newFields,
            });
        }
    }

    render() {
        return (
            <div className={styles.wrap}>
                <Loader visible={this.state.isFetching} />
                <div className={styles.content}>
                    <div className={styles.title}>
                        Выставление счета
                    </div>
                    <Form
                        handleInputChange={this.handleInputChange}
                        handleTablePriceChange={this.handleTablePriceChange}
                        handleCheckboxChange={this.handleCheckboxChange}
                        handleProductChange={this.handleProductChange}
                        handleInputAutocompleteSelect={this.handleInputAutocompleteSelect}
                        totalCalculation={this.totalCalculation}
                        addProduct={this.addProduct}
                        restoreProduct={this.restoreProduct}
                        removeProduct={this.removeProduct}
                        toggleHints={this.toggleHints}
                        getDateValue={this.getDateValue}
                        toggleHistoryProductList={this.toggleHistoryProductList}
                        ndsList={this.props.nds || []}
                        invoiceList={this.props.invoiceList.data}
                        {...this.state}
                        ref={(c) => { this.form = c; }}
                    />
                    <Buttons
                        navigateToHomePage={this.navigateToHomePage}
                        printDOCX={this.printDOCX}
                        showTemplateModal={this.showTemplateModal}
                        saveInvoice={this.saveInvoiceWithValidate}
                        saveTemplate={this.saveTemplate}
                        selectedTemplate={this.state.selectedTemplate}
                        sendNotification={this.state.formFields.invoicesCheck}
                    />
                </div>
                <div className={styles.close} onClick={this.navigateToHomePage} />
                <RightPanel
                    invoiceList={this.props.invoiceList.data}
                    templateList={this.props.templateList.data}
                    setData={this.setData}
                    changeTemplateName={this.changeTemplateName}
                    deleteTemplate={this.deleteTemplate}
                    handleHistoryItemClick={this.handleHistoryItemClick}
                    saveHistoryDOCX={this.saveHistoryDOCX}
                />
                <TemplateNameModal
                    isVisible={this.state.templateModalVisibility}
                    cancelSaveTemplate={this.closeTemplateModal}
                    templateName={this.state.templateName}
                    handleTemplateNameChange={this.handleTemplateNameChange}
                    saveTemplate={this.saveTemplate}
                />
                <InfoMessage />
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        backUrl: state.settings.backUrl,
        orgInfo: state.user.orgInfo.data,
        invoiceList: state.invoice.invoice,
        templateList: state.invoice.templates,
        nds: state.dictionary.nds.data,
        accounts: state.accounts.data,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        setBackUrl: bindActionCreators(setBackUrl, dispatch),
        fetchInvoice: bindActionCreators(fetchInvoice, dispatch),
        fetchInvoiceTemplates: bindActionCreators(fetchInvoiceTemplates, dispatch),
        fetchNds: bindActionCreators(fetchNds, dispatch),
        setMessageData: bindActionCreators(setMessageData, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(FormContainer);
