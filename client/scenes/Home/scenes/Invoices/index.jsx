import React from 'react';

import FormContainer from './containers/FormContainer';

const Invoices = () => (
    <div
        style={{
            position: 'relative',
            width: '100%',
            height: '100%',
            paddingBottom: 14,
        }}
    >
        <FormContainer />
    </div>
);

export default Invoices;
