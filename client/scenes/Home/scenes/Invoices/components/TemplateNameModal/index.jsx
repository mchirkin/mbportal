/** Thir party */
import React from 'react';
import PropTypes from 'prop-types';

/** Global */
import InputGroup from 'components/ui/InputGroup';

import Btn from 'components/ui/Btn';
/** styles */
import styles from './payment.css';

export default function TemplateNameModal(props) {
    const {
        isVisible,
        templateName,
        handleTemplateNameChange,
        cancelSaveTemplate,
        saveTemplate,
    } = props;

    return (
        <div className={styles['template-name-modal-wrapper']} data-visible={isVisible}>
            <div className={styles['template-name-modal']}>
                <h4 className={styles['template-modal-header']}>
                    Название шаблона
                </h4>
                <InputGroup
                    id="draftsName"
                    value={templateName}
                    onChange={handleTemplateNameChange}
                    style={{
                        marginRight: 0,
                        border: '1px solid #41334b',
                    }}
                />
                <div className={styles['btn-row']}>
                    <Btn
                        bgColor="grey"
                        caption="&times;"
                        onClick={cancelSaveTemplate}
                        style={{
                            width: 46,
                            fontSize: 34,
                        }}
                    />
                    <Btn
                        caption="Сохранить"
                        onClick={saveTemplate}
                    />
                </div>
            </div>
        </div>
    );
}

TemplateNameModal.propTypes = {
    isVisible: PropTypes.bool,
    templateName: PropTypes.string,
    handleTemplateNameChange: PropTypes.func.isRequired,
    cancelSaveTemplate: PropTypes.func.isRequired,
};

TemplateNameModal.defaultProps = {
    isVisible: false,
    templateName: '',
};
