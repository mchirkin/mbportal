import React from 'react';
import PropTypes from 'prop-types';

import TableRow from './components/TableRow';

import styles from './invoices.css';

const HistoryTable = props => (
    <table className={styles.table}>
        <thead className={styles.thead}>
            <tr className={styles.tr}>
                <th className={styles.th}>Артикул</th>
                <th className={styles.th}>Наименование</th>
                <th className={styles.th}>Цена за 1 ед., ₽</th>
                <th className={styles.th}>Количество</th>
                <th className={styles.th}>Единицы</th>
                <th className={styles.th}>НДС</th>
                <th className={styles.th}>Цена, ₽</th>
                <th className={styles.th} />
            </tr>
        </thead>
        <tbody className={styles.tbody}>
            {props.historyProductList.map((tr, i) =>
                (
                    <TableRow
                        key={`productItem${i}`}
                        productItem={tr}
                        index={i}
                        restoreProduct={props.restoreProduct}
                    />
                ),
            )}
        </tbody>
    </table>
);

HistoryTable.propTypes = {
    historyProductList: PropTypes.array,
    restoreProduct: PropTypes.func.isRequired,
};

HistoryTable.defaultProps = {
    historyProductList: [
        {
            vendorCode: '',
            name: '',
            price: '',
            quantity: '',
            unit: {
                value: '',
                caption: 'Кг.',
            },
            nds: {
                value: 0,
                caption: '0%',
            },
            totalPrice: '',
        },
    ],
    unitList: [],
    ndsList: [],
};

export default HistoryTable;
