import React from 'react';
import PropTypes from 'prop-types';

import styles from '../../invoices.css';

const TableRow = (props) => {
    const {
        index,
        restoreProduct,
        productItem,
    } = props;

    const {
        vendorCode,
        name,
        price,
        quantity,
        unit,
        nds,
        totalPrice,
    } = productItem;

    return (
        <tr className={styles.tr}>
            <td className={styles.td}>
                <input
                    id="vendorCode"
                    type="text"
                    className={styles.input}
                    value={vendorCode}
                    readOnly
                />
            </td>
            <td className={styles.td}>
                <input
                    id="name"
                    type="text"
                    className={styles.input}
                    value={name}
                    readOnly
                />
            </td>
            <td className={styles.td}>
                <input
                    id="price"
                    type="text"
                    className={styles.input}
                    value={price}
                    readOnly
                />
            </td>
            <td className={styles.td}>
                <input
                    id="quantity"
                    type="text"
                    className={styles.input}
                    value={quantity}
                    readOnly
                />
            </td>
            <td className={styles.td}>
                <input
                    id="unit"
                    type="text"
                    className={styles.input}
                    value={unit.caption}
                    readOnly
                />
            </td>
            <td className={styles.td}>
                <input
                    id="nds"
                    type="text"
                    className={styles.input}
                    value={nds.caption}
                    readOnly
                />
            </td>
            <td className={styles.td}>
                <input
                    id="totalPrice"
                    type="text"
                    className={styles.input}
                    value={totalPrice}
                    readOnly
                />
            </td>
            <td className={styles.td}>
                <div
                    className={styles.restore}
                    onClick={() => restoreProduct(index)}
                />
            </td>
        </tr>
    );
};

TableRow.propTypes = {
    index: PropTypes.number,
    productItem: PropTypes.object,
    restoreProduct: PropTypes.func.isRequired,
};

TableRow.defaultProps = {
    index: 0,
    productItem: [],
};

export default TableRow;
