import React from 'react';
import { CSSTransition } from 'react-transition-group';

export const DocNumber = (
    <CSSTransition
        timeout={500}
        key="docNumber"
        classNames="fadeappear"
        appear
        in
    >
        <div>
            Номер выставленного счета
        </div>
    </CSSTransition>
);

export const PayFrom = (
    <CSSTransition
        timeout={500}
        key="payFrom"
        classNames="fadeappear"
        appear
        in
    >
        <div>
            Дата выставления счета
        </div>
    </CSSTransition>
);

export const PayerName = (
    <CSSTransition
        timeout={500}
        key="receiverName"
        classNames="fadeappear"
        appear
        in
    >
        <div>
            Вы можете ввести ИНН и мы выполним поиск получателя автоматически или ввести наименование плательщика
        </div>
    </CSSTransition>
);

export const Inn = (
    <CSSTransition
        timeout={500}
        key="inn"
        classNames="fadeappear"
        appear
        in
    >
        <div>
            Введите инн покупателя для поиска
        </div>
    </CSSTransition>
);

export const Kpp = (
    <CSSTransition
        timeout={500}
        key="kpp"
        classNames="fadeappear"
        appear
        in
        exit
    >
        <div>
            <ul style={{ marginLeft: 15 }}>
                <li>
                    КПП покупателя: 9 цифр, при этом первый и второй знаки (цифры) не могут одновременно
                    принимать значение "00",
                </li>
                <li>
                    При отсутствии КПП получателя: значение «0»,
                </li>
            </ul>
        </div>
    </CSSTransition>
);

export const LawAddress = (
    <CSSTransition
        timeout={500}
        key="lawAddress"
        classNames="fadeappear"
        appear
        in
        exit
    >
        <div>
            Юридический адрес покупателя
        </div>
    </CSSTransition>
);

export const PayUpTo = (
    <CSSTransition
        timeout={500}
        key="payUpTo"
        classNames="fadeappear"
        appear
        in
        exit
    >
        <div>
            Дата, до которой необходимо провести оплату счета
        </div>
    </CSSTransition>
);


export const Description = (
    <CSSTransition
        timeout={500}
        key="description"
        classNames="fadeappear"
        appear
        in
        exit
    >
        <div>
            Комментарий к выставленному счету
        </div>
    </CSSTransition>
);
