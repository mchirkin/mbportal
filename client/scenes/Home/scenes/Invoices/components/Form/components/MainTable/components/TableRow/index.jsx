import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { fetch } from 'utils';

import InputAmount from 'components/ui/InputAmount';

import Select from '../Select';

import styles from '../../invoices.css';

class TableRow extends Component {
    constructor(props) {
        super(props);

        this.state = {
            okeiList: [],
            unitInputValue: 'Штука',
        };
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.productItem.unit !== nextProps.productItem.unit) {
            this.setState({
                unitInputValue: nextProps.productItem.unit,
            });
        }
    }

    handleUnitChange = (e) => {
        const value = e.target.value;

        this.setState({
            unitInputValue: value,
        }, () => {
            this.getOkeiList(value);
        });
    }

    handleUnitSelect = (data) => {
        this.setState({
            unitInputValue: data.national,
            okeiList: [],
        });

        this.props.handleProductChange('unit', data.national, this.props.index);
    }

    handleUnitFocus = () => {
        this.getOkeiList(this.state.unitInputValue);
    }

    handleUnitBlur = (e) => {
        setTimeout(() => {
            if (this.state.okeiList.length > 0) {
                this.setState({
                    okeiList: [],
                });
            }
        }, 300);
    }

    getOkeiList = (value) => {
        fetch(`/api/v1/dictionary/okei/list?search=${this.state.unitInputValue}`)
            .then(response => response.json())
            .then((json) => {
                if (this.state.unitInputValue === value) {
                    this.setState({
                        okeiList: json,
                    });
                }
            })
            .catch((err) => {
                console.log(err);
            });
    }

    render() {
        const props = this.props;

        const {
            index,
            handleProductChange,
            handleTablePriceChange,
            totalCalculation,
            removeProduct,
            unitList,
            ndsList,
            productItem,
            invoiceWithNds,
        } = props;

        const {
            vendorCode,
            name,
            price,
            quantity,
            unit,
            nds,
            totalPrice,
        } = productItem;

        const isActiveClass = invoiceWithNds ? '' : ` ${styles['td_is-hidden']}`;

        return (
            <tr className={styles.tr}>
                <td className={styles.td}>
                    <input
                        id="vendorCode"
                        type="text"
                        className={styles.input}
                        onChange={e => handleProductChange(e.target.id, e.target.value, index)}
                        value={vendorCode}
                    />
                </td>
                <td className={styles.td}>
                    <input
                        id="name"
                        type="text"
                        className={styles.input}
                        onChange={e => handleProductChange(e.target.id, e.target.value, index)}
                        value={name}
                    />
                </td>
                <td className={styles.td}>
                    <InputAmount
                        id="price"
                        value={price}
                        style={{
                            height: '100%',
                            padding: 0,
                            border: 'none',
                            borderRadius: 0,
                            backgroundColor: 'transparent',
                        }}
                        inputStyle={{
                            padding: '16px 12px',
                            fontSize: '12px',
                        }}
                        onChange={e => handleTablePriceChange('price', e.target.value, index)}
                    />
                </td>
                <td className={styles.td}>
                    <InputAmount
                        id="quantity"
                        value={quantity}
                        style={{
                            height: '100%',
                            padding: 0,
                            border: 'none',
                            borderRadius: 0,
                            backgroundColor: 'transparent',
                        }}
                        inputStyle={{
                            padding: '16px 12px',
                            fontSize: '12px',
                        }}
                        onChange={e => handleTablePriceChange('quantity', e.target.value, index)}
                    />
                </td>
                <td className={styles.td}>
                    <input
                        id="unit"
                        type="text"
                        className={styles.input}
                        value={this.state.unitInputValue}
                        onChange={this.handleUnitChange}
                        onFocus={this.handleUnitFocus}
                        onBlur={this.handleUnitBlur}
                    />
                    {this.state.okeiList.length > 0 && (
                        <div className={styles['unit-select']}>
                            {this.state.okeiList.map(el => (
                                <div
                                    className={styles['unit-item']}
                                    onClick={() => { this.handleUnitSelect(el); }}
                                >
                                    {el.name}
                                </div>
                            ))}
                        </div>
                    )}
                    {false && unitList.length && (
                        <Select
                            key="unitSelect"
                            inputId="unit"
                            productIndex={index}
                            list={unitList}
                            handleProductChange={handleProductChange}
                            totalCalculation={totalCalculation}
                        />
                    )}
                </td>
                <td className={`${styles.td}${isActiveClass}`}>
                    <input
                        id="nds"
                        type="text"
                        className={styles.input}
                        value={nds.caption}
                        readOnly
                    />
                    {ndsList && ndsList.length > 0 && (
                        <Select
                            key="nds"
                            inputId="nds"
                            productIndex={index}
                            list={ndsList}
                            handleProductChange={handleProductChange}
                            totalCalculation={totalCalculation}
                        />
                    )}
                </td>
                <td className={styles.td}>
                    <input
                        id="totalPrice"
                        type="text"
                        className={styles.input}
                        value={totalPrice}
                        readOnly
                    />
                </td>
                <td className={styles.td}>
                    <div
                        className={styles.close}
                        onClick={() => removeProduct(index)}
                    />
                </td>
            </tr>
        );
    }
}

TableRow.propTypes = {
    index: PropTypes.number,
    productItem: PropTypes.object,
    handleProductChange: PropTypes.func.isRequired,
    handleTablePriceChange: PropTypes.func.isRequired,
    totalCalculation: PropTypes.func.isRequired,
    removeProduct: PropTypes.func.isRequired,
    unitList: PropTypes.array,
    ndsList: PropTypes.array,
    invoiceWithNds: PropTypes.bool,
};

TableRow.defaultProps = {
    index: 0,
    productItem: [],
    unitList: [],
    ndsList: [],
    invoiceWithNds: false,
};

export default TableRow;
