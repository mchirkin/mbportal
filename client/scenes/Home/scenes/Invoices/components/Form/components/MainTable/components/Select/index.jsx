import React, { Component } from 'react';
import { findTargetInChildNode } from 'utils/dom';
import PropTypes from 'prop-types';

import styles from './invoices.css';

export default class Select extends Component {
    static propTypes = {
        list: PropTypes.array,
        inputId: PropTypes.string.isRequired,
        productIndex: PropTypes.number.isRequired,
        handleProductChange: PropTypes.func.isRequired,
        totalCalculation: PropTypes.func.isRequired,
    };

    static defaultProps = {
        list: [],
    };

    constructor(props) {
        super(props);

        this.state = {
            isOpen: false,
        };
    }

    componentDidMount() {
        document.addEventListener('click', this.handleOutsideClick);
    }

    componentWillUnmount() {
        document.removeEventListener('click', this.handleOutsideClick);
    }

    handleOutsideClick = (e) => {
        if (!findTargetInChildNode(this.select, e.target)) {
            this.setState({
                isOpen: false,
            });
        }
    }

    toggleSelect = () => {
        this.setState({
            isOpen: !this.state.isOpen,
        });
    }

    render() {
        const isOpen = this.state.isOpen ? ` ${styles['select_is-open']}` : '';

        return (
            <div
                className={`${styles.select}${isOpen}`}
                onClick={this.toggleSelect}
                ref={(select) => { this.select = select; }}
            >
                <div className={styles['select-icon']} />
                <ul className={styles['select-list']} >
                    {
                        this.props.list.map((item, i) => (
                            <li
                                key={`selectItem${i}`}
                                className={styles['select-item']}
                                onClick={() => {
                                    this.props.handleProductChange(
                                        this.props.inputId,
                                        item,
                                        this.props.productIndex,
                                    );
                                }}
                            >
                                {item.caption}
                            </li>
                        ))
                    }
                </ul>
            </div>
        );
    }
}
