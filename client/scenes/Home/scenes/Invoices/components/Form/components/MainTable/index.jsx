import React from 'react';
import PropTypes from 'prop-types';

import TableRow from './components/TableRow';

import styles from './invoices.css';

const MainTable = props => (
    <table className={styles.table}>
        <thead className={styles.thead}>
            <tr className={styles.tr}>
                <th className={styles.th}>Артикул</th>
                <th className={styles.th}>Наименование</th>
                <th className={styles.th}>Цена за 1 ед., ₽</th>
                <th className={styles.th}>Количество</th>
                <th className={styles.th}>Единицы</th>
                {props.invoiceWithNds
                    && (
                        <th className={styles.th}>НДС</th>
                    )
                }
                <th className={styles.th}>Цена, ₽</th>
                <th className={styles.th} />
            </tr>
        </thead>
        <tbody className={styles.tbody}>
            {props.productList.map((tr, i) =>
                (
                    <TableRow
                        key={`productItem${i}`}
                        productItem={tr}
                        index={i}
                        handleProductChange={props.handleProductChange}
                        handleTablePriceChange={props.handleTablePriceChange}
                        totalCalculation={props.totalCalculation}
                        removeProduct={props.removeProduct}
                        unitList={props.unitList}
                        ndsList={props.ndsList}
                        invoiceWithNds={props.invoiceWithNds}
                    />
                ),
            )}
        </tbody>
        <tfoot className={styles.tfoot}>
            <tr className={styles.tr}>
                <td className={`${styles.td} ${styles['add-product-row']}`} colSpan="8">
                    <div
                        className={styles.add}
                        onClick={props.addProduct}
                    >
                        Добавить позицию
                    </div>
                </td>
            </tr>
        </tfoot>
    </table>
);

MainTable.propTypes = {
    addProduct: PropTypes.func.isRequired,
    removeProduct: PropTypes.func.isRequired,
    handleProductChange: PropTypes.func.isRequired,
    totalCalculation: PropTypes.func.isRequired,
    handleTablePriceChange: PropTypes.func.isRequired,
    productList: PropTypes.array,
    unitList: PropTypes.array,
    ndsList: PropTypes.array,
    invoiceWithNds: PropTypes.bool,
};

MainTable.defaultProps = {
    productList: [
        {
            vendorCode: '',
            name: '',
            price: '',
            quantity: '',
            unit: {
                value: '',
                caption: 'Кг.',
            },
            nds: {
                value: 0,
                caption: '0%',
            },
            totalPrice: '',
        },
    ],
    unitList: [],
    ndsList: [],
    invoiceWithNds: false,
};

export default MainTable;
