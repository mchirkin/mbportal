import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Scrollbars } from 'react-custom-scrollbars';

import InputGroup from 'components/ui/InputGroup';
import InputDayPicker from 'components/ui/InputDayPicker';
import InputTextarea from 'components/ui/InputTextarea';

import * as Hints from './components/Hints';
import MainTable from './components/MainTable';
import HistoryTable from './components/HistoryTable';

import styles from './invoices.css';

export default class Form extends Component {
    static propTypes = {
        /** Список полянок */
        formFields: PropTypes.object,
        /** Список товаров или услуг */
        productList: PropTypes.array,
        /** Список истории товаров или услуг */
        historyProductList: PropTypes.array,
        /** Список едениц для таблицы списка товаров */
        unitList: PropTypes.array,
        /** Список НДС для таблицы списка товаров */
        ndsList: PropTypes.array,
        /** Обработчик для инпутов */
        handleInputChange: PropTypes.func.isRequired,
        /** Обработчик инпута цены */
        handleTablePriceChange: PropTypes.func.isRequired,
        /** Обработчик для чекбоксов */
        handleCheckboxChange: PropTypes.func.isRequired,
        /** Обработчик для инпутов таблицы продуктов */
        handleProductChange: PropTypes.func.isRequired,
        /** Обработчик выбора из автодополнения */
        handleInputAutocompleteSelect: PropTypes.func.isRequired,
        /** Расчет финальной суммы и ндс */
        totalCalculation: PropTypes.func.isRequired,
        /** Добавление строки в таблицу продуктов */
        addProduct: PropTypes.func.isRequired,
        /** Удаление строки в таблице продуктов (если последняя, очищает все поля) */
        removeProduct: PropTypes.func.isRequired,
        /** Восставновление строки в таблицу продуктов */
        restoreProduct: PropTypes.func.isRequired,
        /** Отображения подсказок при наведении на поля ввода */
        toggleHints: PropTypes.func.isRequired,
        /** Подсказки на полях ввода */
        hintsDisabled: PropTypes.bool,
        /** Получения даты платежа */
        getDateValue: PropTypes.func.isRequired,
        /** Итоговая сумма с ндс */
        totalSum: PropTypes.string,
        /** Итоговая сумма ндс */
        totalNds: PropTypes.string,
        /** Итоговая сумма без ндс */
        totalSumWithoutNds: PropTypes.string,
        /** Отображение таблица истории */
        showHistoryProductList: PropTypes.bool,
        /** Переключение таблицы с историей товаров и услуг */
        toggleHistoryProductList: PropTypes.func.isRequired,
        /** Список невалидных полей */
        invalidFields: PropTypes.array,
        invoiceList: PropTypes.array,
        contragentList: PropTypes.array,
    }

    static defaultProps = {
        formFields: {
            /** Номер счета */
            accountNumber: '',
            /** Наименование или ИНН */
            payerName: '',
            /** ИНН */
            inn: '',
            /** КПП */
            kpp: '',
            /** Юр. адрес */
            legalAddress: '',
            /** Комментарий для покупателя */
            paymentDesc: '',
            /** Чекбокс отправить контрагенту уведомление... */
            invoicesCheck: false,
            /** Номер телефона */
            phoneNumber: '',
            /** Адрес электронной почты */
            emailAddress: '',
            /** Дата платежа */
            paymentDate: '',
            /** Чекбокс выставления счета с НДС */
            invoiceWithNds: false,
            /** Чекбокс расчета товара с НДС */
            priceIncludesNds: false,
        },
        productList: [
            {
                /** Артикул */
                vendorCode: '',
                /** Наименование */
                name: '',
                /** Цена за единицу */
                price: '',
                /** Количество */
                quantity: '',
                /** Единицы */
                unit: {
                    value: 0,
                    caption: 'Кг.',
                },
                /** НДС */
                nds: {
                    value: 0,
                    caption: '0%',
                },
                /** Общая стоимость */
                totalPrice: '',
            },
        ],
        historyProductList: [],
        /** Список единиц */
        unitList: [],
        /** Список ндс */
        ndsList: [],
        /** Подсказки на полях ввода */
        hintsDisabled: false,
        /** Итоговая сумма */
        totalSum: '0.00',
        /** Итоговый ндс */
        totalNds: '0.00',
        /** Итоговая сумма без ндс */
        totalSumWithoutNds: '0.00',
        /** Отображение таблица истории */
        showHistoryProductList: false,
        /** Список невалидных полей */
        invalidFields: [],
        invoiceList: [],
        contragentList: [],
    }

    componentDidMount() {
        // для корректного присваивания ref
        this.forceUpdate();
    }

    setDate = (id, date) => {
        this[id].setDate(date);
    }

    getAutocompleteList = (field) => {
        if (!this.props.invoiceList) {
            return [];
        }

        const suggestionList = new Set();

        this.props.invoiceList
            .forEach((invoice) => {
                if (invoice[field]) {
                    suggestionList.add(invoice[field]);
                }
            });

        const result = Array.from(suggestionList)
            .map(el => ({
                name: el,
            }));

        return result;
    }

    render() {
        return (
            <Scrollbars>
                <div className={styles.container}>
                    <form className={styles.form}>
                        <div
                            className={styles['input-hint']}
                            onClick={this.props.toggleHints}
                            data-disabled={this.props.hintsDisabled}
                            title={this.props.hintsDisabled ? 'Включить подсказки' : 'Выключить подсказки'}
                        >
                            <div className={styles['input-hint-btn']} />
                        </div>
                        <div className={styles['form-group']}>
                            <p className={styles['group-title']}>
                                Информация о счете
                            </p>
                            <div className={styles['group-content']}>
                                <div className={`${styles.row} ${styles['row_non-split']}`}>
                                    <InputGroup
                                        id="invoiceNumber"
                                        caption="Номер счета"
                                        onChange={this.props.handleInputChange}
                                        hint={Hints.DocNumber}
                                        hintContainer={this.hintContainer}
                                        hintDisabled={this.props.hintsDisabled}
                                        value={this.props.formFields.invoiceNumber}
                                        style={{ width: 150 }}
                                        invalid={(
                                            this.props.invalidFields.includes('invoiceNumber')
                                                ? 'Поле обязательно для заполнения'
                                                : null
                                        )}
                                    />
                                    <InputDayPicker
                                        id="payFrom"
                                        title="От"
                                        hint={Hints.PayFrom}
                                        hintContainer={this.hintContainer}
                                        hintDisabled={this.props.hintsDisabled}
                                        value={this.props.formFields.payFrom}
                                        getDateValue={this.props.getDateValue}
                                        ref={(c) => { this.payFrom = c; }}
                                        invalid={this.props.invalidFields.includes('payFrom')
                                            ? 'Поле обязательно для заполнения'
                                            : null}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className={styles['form-group']}>
                            <p className={styles['group-title']}>
                                Данные счета
                            </p>
                            <div className={styles['group-content']}>
                                <div className={styles.row}>
                                    <InputGroup
                                        id="payerName"
                                        caption="Наименование или ИНН"
                                        onChange={this.props.handleInputChange}
                                        hint={Hints.PayerName}
                                        hintContainer={this.hintContainer}
                                        hintDisabled={this.props.hintsDisabled}
                                        value={this.props.formFields.payerName}
                                        invalid={this.props.invalidFields.includes('payerName')
                                            ? 'Поле обязательно для заполнения'
                                            : null}
                                        noAutoSelectAfterBlur
                                        autocomplete={this.props.contragentList}
                                        handleAutocompleteSelect={(el) => {
                                            this.props.handleInputAutocompleteSelect('payerName', el);
                                        }}
                                    />
                                </div>
                                <div className={styles.row}>
                                    <InputGroup
                                        id="inn"
                                        caption="ИНН"
                                        onChange={this.props.handleInputChange}
                                        hint={Hints.Inn}
                                        hintContainer={this.hintContainer}
                                        hintDisabled={this.props.hintsDisabled}
                                        value={this.props.formFields.inn}
                                        mask="999999999999"
                                        maskChar=""
                                        maxLength={12}
                                        invalid={(
                                            this.props.invalidFields.includes('inn')
                                                ? 'Поле обязательно для заполнения'
                                                : null
                                        )}
                                        autocomplete={this.getAutocompleteList('inn')}
                                        handleAutocompleteSelect={(el) => {
                                            this.props.handleInputAutocompleteSelect('inn', el);
                                        }}
                                        filterAutocompleteList
                                    />
                                    <InputGroup
                                        id="kpp"
                                        caption="КПП"
                                        onChange={this.props.handleInputChange || null}
                                        hint={Hints.Kpp}
                                        hintContainer={this.hintContainer}
                                        hintDisabled={this.props.hintsDisabled}
                                        value={this.props.formFields.kpp}
                                        mask="999999999"
                                        maskChar=""
                                        invalid={(
                                            this.props.invalidFields.includes('kpp')
                                                ? 'Поле обязательно для заполнения'
                                                : null
                                        )}
                                        autocomplete={this.getAutocompleteList('kpp')}
                                        handleAutocompleteSelect={(el) => {
                                            this.props.handleInputAutocompleteSelect('kpp', el);
                                        }}
                                        filterAutocompleteList
                                    />
                                </div>
                                <div className={styles.row}>
                                    <InputGroup
                                        id="legalAddress"
                                        caption="Юр. адрес"
                                        onChange={this.props.handleInputChange}
                                        hint={Hints.LawAddress}
                                        hintContainer={this.hintContainer}
                                        hintDisabled={this.props.hintsDisabled}
                                        value={this.props.formFields.legalAddress}
                                        invalid={(
                                            this.props.invalidFields.includes('legalAddress')
                                                ? 'Поле обязательно для заполнения'
                                                : null
                                        )}
                                        autocomplete={this.getAutocompleteList('legalAddress')}
                                        handleAutocompleteSelect={(el) => {
                                            this.props.handleInputAutocompleteSelect('legalAddress', el);
                                        }}
                                        filterAutocompleteList
                                    />
                                </div>
                                <InputDayPicker
                                    id="payUpTo"
                                    title="Оплатить до"
                                    hint={Hints.PayUpTo}
                                    hintContainer={this.hintContainer}
                                    hintDisabled={this.props.hintsDisabled}
                                    value={this.props.formFields.payUpTo}
                                    getDateValue={this.props.getDateValue}
                                    ref={(c) => { this.payUpTo = c; }}
                                    invalid={this.props.invalidFields.includes('payUpTo')
                                        ? 'Поле обязательно для заполнения'
                                        : null}
                                />
                                <InputTextarea
                                    id="paymentDesc"
                                    caption="Комментарий"
                                    height={50}
                                    style={{
                                        width: '100%',
                                        marginBottom: 30,
                                    }}
                                    textareaStyle={{
                                        fontSize: 16,
                                    }}
                                    onChange={this.props.handleInputChange}
                                    showSymbolCounter
                                    hint={Hints.Description}
                                    hintContainer={this.hintContainer}
                                    hintDisabled={this.props.hintsDisabled}
                                    restoreHintContainer
                                    autosize
                                    value={this.props.formFields.paymentDesc}
                                    invalid={this.props.invalidFields.includes('paymentDesc')
                                        ? 'Поле обязательно для заполнения'
                                        : null}
                                />
                            </div>
                        </div>
                        <div className={styles.checkbox}>
                            <input
                                className={styles['checkbox-source']}
                                type="checkbox"
                                id="invoicesCheck"
                                checked={this.props.formFields.invoicesCheck}
                            />
                            <label
                                className={styles['checkbox-icon']}
                                htmlFor="invoicesCheck"
                                onClick={this.props.handleCheckboxChange}
                            />
                            <label
                                className={styles['checkbox-label']}
                                htmlFor="invoicesCheck"
                                onClick={this.props.handleCheckboxChange}
                            >
                                Отправить контрагенту уведомление о выставленном счете
                            </label>
                        </div>
                        <div className={styles['form-group']}>
                            <p className={styles['group-title']}>
                                Контактные данные контрагента
                            </p>
                            <div className={styles['group-content']}>
                                <div className={styles.row}>
                                    <InputGroup
                                        id="phoneNumber"
                                        caption="Номер телефона"
                                        onChange={this.props.handleInputChange}
                                        hint={null}
                                        hintContainer={this.hintContainer}
                                        hintDisabled={this.props.hintsDisabled}
                                        value={this.props.formFields.phoneNumber}
                                        mask="+7 (999) 999-99-99"
                                        invalid={(
                                            this.props.invalidFields.includes('phoneNumber')
                                                ? 'Поле обязательно для заполнения'
                                                : null
                                        )}
                                        autocomplete={this.getAutocompleteList('phoneNumber')}
                                        handleAutocompleteSelect={(el) => {
                                            this.props.handleInputAutocompleteSelect('phoneNumber', el);
                                        }}
                                        filterAutocompleteList
                                        autocompleteFilterFunction={(item, value) => {
                                            const itemValueReplaced = item.replace(/[\s()\-_]/g, '');
                                            const valueReplaced = value.replace(/[\s()\-_]/g, '');
                                            return itemValueReplaced.includes(valueReplaced);
                                        }}
                                    />
                                </div>
                                <div className={styles.row}>
                                    <InputGroup
                                        id="emailAddress"
                                        caption="Адрес электронной почты"
                                        onChange={this.props.handleInputChange}
                                        hint={null}
                                        hintContainer={this.hintContainer}
                                        hintDisabled={this.props.hintsDisabled}
                                        value={this.props.formFields.emailAddress}
                                        invalid={(
                                            this.props.invalidFields.includes('emailAddress')
                                                ? 'Поле обязательно для заполнения'
                                                : null
                                        )}
                                        autocomplete={this.getAutocompleteList('emailAddress')}
                                        handleAutocompleteSelect={(el) => {
                                            this.props.handleInputAutocompleteSelect('emailAddress', el);
                                        }}
                                        filterAutocompleteList
                                    />
                                </div>
                            </div>
                        </div>
                        <div className={styles['form-group']}>
                            <div className={styles.checkbox}>
                                <input
                                    className={styles['checkbox-source']}
                                    type="checkbox"
                                    id="invoiceWithNds"
                                    checked={this.props.formFields.invoiceWithNds}
                                />
                                <label
                                    className={styles['checkbox-icon']}
                                    htmlFor="invoiceWithNds"
                                    onClick={this.props.handleCheckboxChange}
                                />
                                <label
                                    className={styles['checkbox-label']}
                                    htmlFor="invoiceWithNds"
                                    onClick={this.props.handleCheckboxChange}
                                >
                                    Выставить счет с НДС
                                </label>
                            </div>
                            <div
                                className={styles.checkbox}
                                style={{ display: this.props.formFields.invoiceWithNds ? '' : 'none' }}
                            >
                                <input
                                    className={styles['checkbox-source']}
                                    type="checkbox"
                                    id="priceIncludesNds"
                                    checked={this.props.formFields.priceIncludesNds}
                                />
                                <label
                                    className={styles['checkbox-icon']}
                                    htmlFor="priceIncludesNds"
                                    onClick={this.props.handleCheckboxChange}
                                />
                                <label
                                    className={styles['checkbox-label']}
                                    htmlFor="priceIncludesNds"
                                    onClick={this.props.handleCheckboxChange}
                                >
                                    Цена включает в себя НДС
                                </label>
                            </div>
                        </div>
                    </form>
                    <div className={styles['information-content']} ref={(el) => { this.hintContainer = el; }} />
                    {/*
                    <div
                        className={styles['input-hint']}
                        onClick={this.props.toggleHints}
                        data-disabled={this.props.hintsDisabled}
                        title={this.props.hintsDisabled ? 'Включить подсказки' : 'Выключить подсказки'}
                    >
                        <div className={styles['input-hint-btn']} />
                    </div>
                    */}
                </div>
                {this.props.showHistoryProductList
                    ? (
                        <div>
                            <div className={`${styles['table-header']} ${styles['table-header_history']}`}>
                                <p className={styles['table-header__title']}>
                                    История добавления товаров и услуг
                                </p>
                                <div
                                    className={styles['table-header__icon']}
                                    onClick={this.props.toggleHistoryProductList}
                                />
                            </div>
                            {this.props.historyProductList.length > 0
                                ? (
                                    <HistoryTable
                                        restoreProduct={this.props.restoreProduct}
                                        historyProductList={this.props.historyProductList}
                                    />
                                )
                                : (
                                    <p className={styles.message}>
                                        История пуста
                                    </p>
                                )
                            }
                        </div>
                    )
                    : (
                        <div>
                            <div className={styles['table-header']}>
                                <p className={styles['table-header__title']}>
                                    Список товаров/услуг
                                </p>
                                <div
                                    className={styles['table-header__icon']}
                                    onClick={this.props.toggleHistoryProductList}
                                    title="История добавления товаров и услуг"
                                />
                            </div>
                            <MainTable
                                handleProductChange={this.props.handleProductChange}
                                handleTablePriceChange={this.props.handleTablePriceChange}
                                totalCalculation={this.props.totalCalculation}
                                addProduct={this.props.addProduct}
                                removeProduct={this.props.removeProduct}
                                productList={this.props.productList}
                                unitList={this.props.unitList}
                                ndsList={this.props.ndsList}
                                invoiceWithNds={this.props.formFields.invoiceWithNds}
                            />
                        </div>
                    )
                }
                <div className={styles.summary}>
                    <div className={styles.sum}>
                        <p className={styles['sum-caption']}>
                            Итого без НДС:
                        </p>
                        <p className={styles['sum-value']}>
                            {`${this.props.totalSumWithoutNds} ₽`}
                        </p>
                    </div>
                    {this.props.formFields.invoiceWithNds
                        && (
                            <div>
                                <div className={styles.sum}>
                                    <p className={styles['sum-caption']}>
                                        НДС:
                                    </p>
                                    <p className={styles['sum-value']}>
                                        {`${this.props.totalNds} ₽`}
                                    </p>
                                </div>
                                <div className={styles.sum}>
                                    <p className={styles['sum-caption']}>
                                        Итого с НДС:
                                    </p>
                                    <p className={styles['sum-value']}>
                                        {`${this.props.totalSum} ₽`}
                                    </p>
                                </div>
                            </div>
                        )
                    }
                </div>
            </Scrollbars>
        );
    }
}
