import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Scrollbars } from 'react-custom-scrollbars';
import moment from 'moment';

import { formatNumber } from 'utils';

import Marquee from 'components/ui/Marquee';

import RightPanelTabs from './components/RightPanelTabs';
import TemplateItem from './components/TemplateItem';

import styles from './right-panel.css';

export default class RightPanel extends Component {
    static propTypes = {
        changeTemplateName: PropTypes.func.isRequired,
        deleteTemplate: PropTypes.func.isRequired,
        handleTemplateClick: PropTypes.func.isRequired,
        submitSelectContragent: PropTypes.func.isRequired,
        invoiceList: PropTypes.array,
        drafts: PropTypes.array,
        modalData: PropTypes.object,
    }

    static defaultProps = {
        invoiceList: [],
        drafts: [],
        modalData: null,
    }

    constructor(props) {
        super(props);

        this.state = {
            activeTab: 'drafts',
            search: '',
            editableTemplateIndex: null,
            editableTemplateName: '',
            templateError: '',
        };
    }

    setActiveTab = (tab) => {
        this.setState({
            activeTab: tab,
            search: '',
        }, () => {
            this.forceUpdate();
        });
    }

    handleSearchChange = (e) => {
        this.setState({
            search: e.target.value,
        });
    }

    render() {
        let content;

        if (this.state.activeTab === 'drafts') {
            content = (
                <div className={styles['template-list']}>
                    {this.props.templateList
                        ? this.props.templateList
                            .filter(template => (this.state.search
                                ? template.templateName.toLowerCase().includes(this.state.search.toLowerCase())
                                : true),
                            )
                            .map((data, index) => (
                                <TemplateItem
                                    key={data._id || index}
                                    deleteTemplate={this.props.deleteTemplate}
                                    changeTemplateName={this.props.changeTemplateName}
                                    setData={this.props.setData}
                                    template={data}
                                    handleTemplateClick={this.props.handleTemplateClick}
                                    deleteTemplate={this.props.deleteTemplate}
                                />
                            ))
                        : null}
                </div>
            );
        }

        if (this.state.activeTab === 'history') {
            content = (
                <div className={styles['template-list']}>
                    {this.props.invoiceList
                        ? this.props.invoiceList
                            .filter(invoice => (this.state.search
                                ? invoice.payerName.toLowerCase().includes(this.state.search.toLowerCase())
                                : true),
                            )
                            .map(invoice => (
                                <div
                                    key={invoice._id}
                                    className={styles['history-item']}
                                    onClick={() => { this.props.handleHistoryItemClick(invoice); }}
                                >
                                    <div>
                                        <Marquee text={invoice.payerName} />
                                        <div className={styles['history-item-info']}>
                                            № {invoice.invoiceNumber} от {moment(invoice.paymentData).format('DD.MM.YYYY')}
                                        </div>
                                    </div>
                                    <div>
                                        <div>
                                            {`${formatNumber(invoice.sumTotal)} \u20bd`}
                                        </div>
                                        <div className={styles['item-info-print']}>
                                            <div
                                                onClick={(e) => {
                                                    e.preventDefault();
                                                    e.stopPropagation();
                                                    this.props.saveHistoryDOCX(invoice._id);
                                                }}
                                            >
                                                .DOCX
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            ))
                        : null}
                </div>
            );
        }

        return (
            <div className={styles.wrapper}>
                <RightPanelTabs
                    {...this.state}
                    setActiveTab={this.setActiveTab}
                />
                <div className={styles['content-wrapper']}>
                    <Scrollbars
                        style={{
                            width: '100%',
                            height: '100%',
                        }}
                    >
                        {content}
                    </Scrollbars>
                </div>
                <div className={styles.search}>
                    <div className={styles['input-wrapper']}>
                        <div className={styles['search-icon']} />
                        <input
                            className={styles['search-input']}
                            value={this.state.search}
                            onChange={this.handleSearchChange}
                            placeholder="Поиск"
                        />
                    </div>
                </div>
            </div>
        );
    }
}
