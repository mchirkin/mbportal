import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './right-panel-tabs.css';

export default class RightPanelTabs extends Component {
    static propTypes = {
        setActiveTab: PropTypes.func.isRequired,
        activeTab: PropTypes.string,
    }

    static defaultProps = {
        activeTab: 'drafts',
    }

    handleTabClick = (e) => {
        const tab = e.target.getAttribute('data-tab');
        this.props.setActiveTab(tab);
    }

    render() {
        const activeTab = this.props.activeTab;
        return (
            <div
                className={styles['top-tabs-wrapper']}
            >
                <div className={styles['top-tabs-list-wrapper']}>
                    <div className={styles['top-tabs-list']}>
                        <div
                            className={styles['top-tab']}
                            data-active={activeTab === 'drafts'}
                            data-tab="drafts"
                            onClick={this.handleTabClick}
                        >
                            Черновики
                        </div>
                        <div
                            className={styles['top-tab']}
                            data-active={activeTab === 'history'}
                            data-tab="history"
                            onClick={this.handleTabClick}
                        >
                            История
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
