import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import Btn from 'components/ui/Btn';
import Marquee from 'components/ui/Marquee';
import styles from './template-item.css';

const cx = classNames.bind(styles);

export default class TemplateItem extends Component {
    static propTypes = {
        template: PropTypes.object,
        changeTemplateName: PropTypes.func.isRequired,
        deleteTemplate: PropTypes.func.isRequired,
    }

    static defaultProps = {
        template: {},
    }

    constructor(props) {
        super(props);

        this.state = {
            name: this.props.template.templateName,
            edit: false,
            editButtonsIsActive: false,
            deleteButtonsIsActive: false,
            actionsIsActive: true,
        };
    }

    submitChangeTemplateName = (e) => {
        e.preventDefault();

        this.props.changeTemplateName({
            ...this.props.template,
            templateName: this.state.name,
        })
            .catch((err) => {
                global.console.log(err);
            });

        this.setState({
            edit: false,
            editButtonsIsActive: false,
            actionsIsActive: true,
        });
    }

    handleTemplateNameChange = (e) => {
        this.setState({
            name: e.target.value,
        });
    }

    handleEditing = (e) => {
        e.preventDefault();
        e.stopPropagation();

        this.setState({
            edit: true,
            editButtonsIsActive: true,
            actionsIsActive: false,
        }, () => {
            setTimeout(() => {
                if (this.input) {
                    this.input.focus();
                    this.input.setSelectionRange(
                        this.input.value.length,
                        this.input.value.length,
                    );
                }
            }, 0);
        });
    }

    handleDelete = (e) => {
        e.preventDefault();
        e.stopPropagation();

        this.setState({
            name: 'Вы действительно хотите удалить черновик?',
            deleteButtonsIsActive: true,
            actionsIsActive: false,
        });
    }

    cancelEdit = () => {
        if (this.state.name !== this.props.template.name) {
            this.setState({
                name: this.props.template.templateName,
            });
        }

        this.setState({
            edit: false,
            editButtonsIsActive: false,
            deleteButtonsIsActive: false,
            actionsIsActive: true,
        });
    }

    render() {
        const actionsClasses = cx({
            actions: true,
            'actions_is-active': this.state.actionsIsActive,
        });

        const editButtonsClasses = cx({
            buttons: true,
            buttons_edit: true,
            'buttons_is-active': this.state.editButtonsIsActive,
        });

        const deleteButtonsClasses = cx({
            buttons: true,
            buttons_delete: true,
            'buttons_is-active': this.state.deleteButtonsIsActive,
        });

        return (
            <div className={styles.content}>
                <div
                    className={styles.head}
                    onClick={() => { this.props.setData(this.props.template); }}
                >
                    <form
                        className={styles.form}
                        onSubmit={this.submitChangeTemplateName}
                    >
                        {this.state.edit
                            ? (
                                <input
                                    className={styles.input}
                                    value={this.state.name}
                                    onClick={(e) => { e.stopPropagation(); }}
                                    onChange={this.handleTemplateNameChange}
                                    readOnly={!this.state.edit}
                                    ref={(input) => { this.input = input; }}
                                />
                            )
                            : (
                                <Marquee
                                    text={this.state.name}
                                />
                            )}
                        <div className={styles.error}>
                            {this.state.name === '' ? 'Не введено название шаблона' : null}
                        </div>
                    </form>
                    <div className={actionsClasses}>
                        <div
                            className={`${styles['actions-item']} ${styles['actions-item_edit']}`}
                            onClick={this.handleEditing}
                            title="Редактировать название шаблона"
                        />
                        <div
                            className={`${styles['actions-item']} ${styles['actions-item_delete']}`}
                            onClick={this.handleDelete}
                            title="Удалить шаблон"
                        />
                    </div>
                </div>
                <div
                    className={editButtonsClasses}
                    ref={(div) => { this.editButtons = div; }}
                >
                    <Btn
                        caption={
                            <div
                                style={{
                                    position: 'relative',
                                    top: 0,
                                }}
                            >
                                &times;
                            </div>
                        }
                        bgColor="white"
                        onClick={this.cancelEdit}
                        style={{
                            height: 30,
                            width: 30,
                            padding: 0,
                            fontSize: 26,
                            lineHeight: 1,
                            borderRadius: 4,
                        }}
                    />
                    <Btn
                        caption="Сохранить"
                        onClick={this.submitChangeTemplateName}
                        style={{
                            height: 30,
                            fontSize: 12,
                            borderRadius: 4,
                        }}
                    />
                </div>
                <div
                    className={deleteButtonsClasses}
                    ref={(div) => { this.deleteButtons = div; }}
                >
                    <Btn
                        caption={
                            <div
                                style={{
                                    position: 'relative',
                                    top: 0,
                                }}
                            >
                                &times;
                            </div>
                        }
                        bgColor="white"
                        onClick={this.cancelEdit}
                        style={{
                            height: 30,
                            width: 30,
                            padding: 0,
                            fontSize: 26,
                            lineHeight: 1,
                            borderRadius: 4,
                        }}
                    />
                    <Btn
                        caption="Удалить"
                        onClick={() => this.props.deleteTemplate(this.props.template._id)}
                        style={{
                            height: 30,
                            fontSize: 12,
                            borderRadius: 4,
                        }}
                    />
                </div>
            </div>
        );
    }
}
