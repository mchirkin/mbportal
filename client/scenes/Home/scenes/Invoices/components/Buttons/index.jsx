import React from 'react';
import PropTypes from 'prop-types';
import Btn from 'components/ui/Btn';
import styles from './invoices.css';

const Buttons = (props) => {
    return (
        <div className={styles.buttons}>
            <div className={styles['buttons-group']}>
                <Btn
                    bgColor="grey"
                    caption="&times;"
                    onClick={props.navigateToHomePage}
                    style={{
                        width: 46,
                        marginRight: 20,
                        fontSize: 34,
                    }}
                />
                <Btn
                    bgColor="grey"
                    caption={props.selectedTemplate ? 'Изменить шаблон' : 'Сохранить черновик'}
                    onClick={() => {
                        if (props.selectedTemplate) {
                            props.saveTemplate();
                        } else {
                            props.showTemplateModal(true);
                        }
                    }}
                    style={{
                        // width: editTemplate ? 120 : 220,
                        marginRight: 20,
                    }}
                />
                <Btn
                    bgColor="grey"
                    caption="Сохранить в DOCX"
                    onClick={props.printDOCX}
                    style={{
                        // width: editTemplate ? 120 : 220,
                        marginRight: 20,
                    }}
                />
            </div>
            <Btn
                caption={props.sendNotification ? 'Сохранить и отправить' : 'Сохранить'}
                onClick={props.saveInvoice}
                styles={{
                    width: 180,
                    marginRight: 10,
                }}
            />
        </div>
    );
};

Buttons.propTypes = {
    navigateToHomePage: PropTypes.func.isRequired,
};

export default Buttons;
