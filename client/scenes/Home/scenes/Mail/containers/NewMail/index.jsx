import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { fetch } from 'utils';
import NewMailForm from '../../components/NewMailForm';

export default class NewMail extends Component {
    static propTypes = {
        toggleLoader: PropTypes.func.isRequired,
        setMessageData: PropTypes.func.isRequired,
        fetchOutcomeList: PropTypes.func.isRequired,
    }

    constructor(props) {
        super(props);

        const initialState = {
            caption: '',
            text: '',
            fileList: [],
            id: null,
            requestSign: false,
            status: '',
        };

        this.state = initialState;
        this.initialState = initialState;
    }

    handleInputChange = (e) => {
        const id = e.target.getAttribute('id');

        this.setState({
            [id]: e.target.value,
        });
    }


    saveNewMail = (resetForm = true) => new Promise((resolve, reject) => {
        if (this.state.caption.length === 0) {
            this.setState({
                errorText: 'Не введена тема письма',
            });
            reject('Не введена тема письма');
            return false;
        }

        if (this.state.text.length === 0) {
            this.setState({
                errorText: 'Не введен текст письма',
            });
            reject('Не введен текст письма');
            return false;
        }

        this.props.toggleLoader(true);

        fetch('/api/v1/msg/mail/create', {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify({
                id: this.state.id || null,
                caption: this.state.caption,
                description: this.state.text,
            }),
        }).then((response) => {
            if (response.ok) {
                return response.json();
            }
        }).then((json) => {
            if (json && json.errorText) {
                return reject(json.errorText);
            }

            if (this.state.fileList.length > 0) {
                const promises = this.state.fileList.map(file => this.attachFile(json.id, file));
                Promise.all(promises).then((values) => {
                    this.setState({
                        isFetching: false,
                        // fileList: [],
                    });
                    this.props.toggleLoader(false);
                    resolve(json.id);
                    values.forEach((value) => {
                        if (!value) {
                            this.props.setMessageData({
                                isVisible: true,
                                header: 'Внимание!',
                                body: 'Письмо успешно сохранено, но при сохранении одного или ' +
                                        'нескольких файлов произошла ошибка',
                                btnText: 'Далее',
                                callback: () => this.props.setMessageData({ isVisible: false }),
                            });
                        }
                    });
                }).catch((err) => {
                    global.console.log(err);
                    reject(err);
                });
            } else {
                this.props.toggleLoader(false);
                //  this.props.fetchOutcomeList();
                resolve(json.id);
            }
            if (resetForm) {
                this.resetForm();
            }
        }).catch((err) => {
            global.console.error('Ошибка сохранения нового письма', err);
            this.setState({
                errorText: err.errorText || 'Произошла ошибка при сохранении письма',
            });
            this.props.setMessageData({
                isVisible: true,
                header: 'Внимание!',
                body: err.errorText || 'Произошла ошибка при сохранении письма',
                btnText: 'Назад',
                callback: () => this.props.setMessageData({ isVisible: false }),
            });
            reject(err);
        });
    })

    attachFile = (id, file) => new Promise((resolve, reject) => {
        if (file.id) {
            resolve(true);
            return true;
        }
        const formData = new FormData();
        formData.append('id', id);
        formData.append('file', file, file.name);
        fetch('/api/v1/documents/attachment/mail2bank', {
            method: 'POST',
            credentials: 'include',
            body: formData,
        }).then((response) => {
            if (response.ok) {
                if (response.status !== 200) {
                    return false;
                }
                return response.json();
            }
            return false;
        }).then((json) => {
            if (json === false || json.errorCode) {
                resolve(false);
            }
            resolve(true);
        }).catch((err) => {
            reject(err);
        });
    })

    handleFileChange = (acceptedFiles) => {
        if (acceptedFiles && acceptedFiles.length > 0) {
            this.setState({
                fileList: [...this.state.fileList, acceptedFiles[0]],
            });
        }
    }

    deleteFile = (file) => {
        this.setState({
            fileList: this.state.fileList.filter(f => f !== file),
        });
    }

    deleteFileFromISimple = (mailId, fileId) => {
        fetch('/api/v1/msg/mail/attachment/delete', {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify({
                id: mailId,
                attachmentId: fileId,
            }),
        }).then((response) => {
            if (response.ok) {
                return {};
            }
            return response.json();
        }).then(() => {
            this.props.fetchOutcomeList();
            this.setState({
                fileList: this.state.fileList.filter(f => f.id !== fileId || !f.id),
            });
        }).catch((err) => {
            console.log(err);
        });
    }

    resetForm = () => {
        this.setState(this.initialState);
    }

    handleSignBtnClick = () => this.saveNewMail(false)
        .then((id) => {
            this.setState({
                id,
                requestSign: true,
            });
        })
        .catch((err) => {
            global.console.log(err);
            this.setState({
                errorText: err || 'Произошла ошибка при сохранении письма',
            });
            this.props.setMessageData({
                isVisible: true,
                header: 'Внимание!',
                body: err || 'Произошла ошибка при сохранении письма',
                btnText: 'Назад',
                callback: () => this.props.setMessageData({ isVisible: false }),
            });
            this.props.toggleLoader(false);
        })

    signCallback = (data) => {
        this.props.fetchOutcomeList();
        this.resetForm();

        if (data.paycontrol) {
            this.props.setMessageData({
                isVisible: true,
                header: 'Успешно!',
                body: 'Подтвердите письмо в мобильном приложении',
                btnText: 'Далее',
                callback: () => this.props.setMessageData({ isVisible: false }),
            });
        }
        // _paq.push(['trackGoal', 7]);
    }

    errorSignCallback = () => {
        this.props.fetchOutcomeList();
    }

    cancelSignCallback = () => {
        this.setState({
            requestSign: false,
        });
    }

    setDataFromSavedMail = (data) => {
        this.setState({
            caption: data.caption,
            text: data.description,
            status: data.status,
            showCancelBtn: false,
        }, () => {
            if (data.status === 'new') {
                const attachments = Array.isArray(data.attachments) ? data.attachments : [data.attachments];
                this.setState({
                    id: data.id,
                    fileList: data.attachments ? attachments : [],
                    showCancelBtn: true,
                });
            }
        });
    }

    render() {
        return (
            <div style={{ height: '100%', borderLeft: '1px solid #d8d8d8' }}>
                <NewMailForm
                    {...this.state}
                    handleInputChange={this.handleInputChange}
                    saveNewMail={this.saveNewMail}
                    handleFileChange={this.handleFileChange}
                    deleteFile={this.deleteFile}
                    handleSignBtnClick={this.handleSignBtnClick}
                    signCallback={this.signCallback}
                    errorSignCallback={this.errorSignCallback}
                    cancelSignCallback={this.cancelSignCallback}
                    deleteFileFromISimple={this.deleteFileFromISimple}
                    resetForm={this.resetForm}
                />
            </div>
        );
    }
}
