import React from 'react';
import PropTypes from 'prop-types';
import styles from './calendar-bottom.css';

const CalendarBottom = ({ onCancel, onSubmit }) => (
    <div className={styles.wrapper}>
        <div className={styles['cancel-btn']} onClick={onCancel}>
            Сбросить
        </div>
        <div className={styles['accept-btn']} onClick={onSubmit}>
            Применить
        </div>
    </div>
);

CalendarBottom.propTypes = {
    onCancel: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
};

export default CalendarBottom;
