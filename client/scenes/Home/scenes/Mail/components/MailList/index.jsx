import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Scrollbars from 'react-custom-scrollbars';

import MailListHeader from '../MailListHeader';
import MailItem from '../MailItem';
import styles from './mail-list.css';

class MailList extends Component {
    static propTypes = {
        initialStartDate: PropTypes.object,
        initialEndDate: PropTypes.object,
        mailList: PropTypes.array,
        downloadFile: PropTypes.func.isRequired,
        deleteMail: PropTypes.func.isRequired,
        fetchOutcomeList: PropTypes.func.isRequired,
        acceptDateFilter: PropTypes.func.isRequired,
        clearDateFilter: PropTypes.func.isRequired,
        getSavedMailData: PropTypes.func.isRequired,
        getOperationNotify: PropTypes.func.isRequired,
        operationNotifyList: PropTypes.array,
    };

    static defaultProps = {
        initialStartDate: undefined,
        initialEndDate: undefined,
        mailList: [],
        operationNotifyList: [],
        operationNotifyIsActive: false,
    };

    constructor(props) {
        super(props);

        this.state = {

        };
    }

    handleSearchChange = (e) => {
        this.setState({
            search: e.target.value,
        });
    }

    handleOperationNotify = () => {
        if (this.state.operationNotifyIsActive) {
            this.setState({
                operationNotifyIsActive: !this.state.operationNotifyIsActive,
            });
        } else {
            this.props.getOperationNotify()
                .then(() => {
                    this.setState({
                        operationNotifyIsActive: !this.state.operationNotifyIsActive,
                    });
                });
        }
    }

    render() {
        const {
            initialStartDate,
            initialEndDate,
            mailList,
            downloadFile,
            deleteMail,
            fetchOutcomeList,
            acceptDateFilter,
            clearDateFilter,
            getSavedMailData,
            operationNotifyList,
        } = this.props;

        let filteredList = [];

        if (operationNotifyList.length && this.state.operationNotifyIsActive) {
            filteredList = operationNotifyList.map((el) => {
                const updateEl = el;
                updateEl.status = 'end';
                updateEl.docType = 'mail2client';

                return updateEl;
            });
        }

        if (mailList.length && !this.state.operationNotifyIsActive) {
            filteredList = mailList.filter((mail) => {
                if (!this.state.search) {
                    return true;
                }
                return (
                    mail.caption.toLowerCase().includes(this.state.search.toLowerCase()) ||
                    mail.description.toLowerCase().includes(this.state.search.toLowerCase())
                );
            });
        }

        return (
            <div className={styles.wrapper}>
                <div className={styles['mail-list-wrapper']}>
                    <Scrollbars>
                        <MailListHeader
                            search={this.state.search}
                            handleSearchChange={this.handleSearchChange}
                            initialStartDate={initialStartDate}
                            initialEndDate={initialEndDate}
                            acceptDateFilter={acceptDateFilter}
                            clearDateFilter={clearDateFilter}
                        />

                        <div
                            className={styles.toggle}
                            onClick={this.handleOperationNotify}
                            data-active={this.state.operationNotifyIsActive}
                            title={this.state.operationNotifyIsActive
                                ? 'Показать письма'
                                : 'Показать уведомления'
                            }
                        >
                            Уведомления 161-ФЗ
                        </div>

                        <div className={styles['mail-list']}>
                            {filteredList.length !== 0
                                ? (
                                    filteredList.map((mail, index) => {
                                        const marginBottom = (
                                            index + 1 !== mailList.length &&
                                            mailList[index + 1].docType !== mail.docType
                                        )
                                            ? 60
                                            : 20;

                                        return (
                                            <div
                                                key={mail.id}
                                                style={{
                                                    display: 'flex',
                                                    width: '100%',
                                                    justifyContent: mail.docType === 'mail2client'
                                                        ? 'flex-start'
                                                        : 'flex-end',
                                                    marginBottom,
                                                }}
                                            >
                                                <MailItem
                                                    data={mail}
                                                    downloadFile={downloadFile}
                                                    deleteMail={deleteMail}
                                                    fetchOutcomeList={fetchOutcomeList}
                                                    getSavedMailData={getSavedMailData}
                                                />
                                            </div>
                                        );
                                    })
                                )
                                : (
                                    <div className={styles.subtitle}>
                                        {this.state.operationNotifyIsActive ? 'Уведомлений нет' : 'Писем нет'}
                                    </div>
                                )
                            }
                        </div>
                    </Scrollbars>
                </div>
            </div>
        );
    }
}

export default MailList;
