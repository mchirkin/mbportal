import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { dates } from 'utils';
import Btn from 'components/ui/Btn';
import SignButton from 'containers/SignButton';
import styles from './mail-item.css';

const today = dates.getDateString(new Date());

export default class MailItem extends Component {
    static propTypes = {
        data: PropTypes.object,
        downloadFile: PropTypes.func.isRequired,
        deleteMail: PropTypes.func.isRequired,
        fetchOutcomeList: PropTypes.func.isRequired,
        getSavedMailData: PropTypes.func.isRequired,
    };

    static defaultProps = {
        data: {},
    };

    render() {
        const {
            id,
            caption,
            description,
            docType,
            createstamp,
            attachments,
            status,
            protestUrl,
        } = this.props.data;

        const mailStamp = new Date(createstamp);
        const mailDay = dates.getDateString(mailStamp);
        const mailDateHours = dates.padZero(mailStamp.getHours());
        const mailDateMinutes = dates.padZero(mailStamp.getMinutes());

        const mailDate = `${today === mailDay ? 'Сегодня' : mailDay} ${mailDateHours}:${mailDateMinutes}`;

        let fileList = [];
        if (attachments) {
            fileList = Array.isArray(attachments) ? attachments : [attachments];
        }

        return (
            <div className={styles.wrapper}>
                <div className={styles['mail-wrapper']}>
                    <div className={styles['mail-sender']} data-type={docType} />
                    <div
                        className={styles['mail-item']}
                        data-type={docType}
                        onClick={() => docType === 'mail2bank' && this.props.getSavedMailData(this.props.data)}
                    >
                        <div className={styles['mail-top']}>
                            <div className={styles['mail-top-info']}>
                                <div>
                                    {mailDate}
                                </div>
                                <div>
                                    {(() => {
                                        if (docType.toLowerCase() !== 'mail2bank') {
                                            return null;
                                        }
                                        switch (status) {
                                            case 'new':
                                                return 'Черновик';
                                            case 'send':
                                            case 'for_send':
                                                return 'Отправлено';
                                            case 'decline':
                                                return 'Отклонено';
                                            case 'end':
                                                return 'Принято';
                                            default:
                                                return null;
                                        }
                                        return 'Черновик';
                                    })()}
                                </div>
                            </div>
                            {caption
                                ? (
                                    <div className={styles.caption}>
                                        {caption}
                                    </div>
                                )
                                : null}
                        </div>
                        <div className={styles.description}>
                            {description}
                        </div>
                        {protestUrl === undefined
                            ? null
                            : (
                                <a
                                    href={protestUrl}
                                    className={styles.url}
                                    target="blank"
                                >
                                    {protestUrl}
                                </a>
                            )
                        }
                        {fileList && fileList.length > 0
                            ? (
                                <div className={styles['file-list']}>
                                    {fileList.map((file, i) => (
                                        <div
                                            key={i}
                                            className={styles['file-item']}
                                            onClick={(e) => {
                                                e.stopPropagation();
                                                this.props.downloadFile(id, file);
                                            }}
                                        >
                                            <div className={styles['file-icon']} />
                                            <div>
                                                {file.fileName}
                                            </div>
                                        </div>
                                    ))}
                                </div>
                            )
                            : null}
                    </div>
                </div>
                {status === 'new'
                    ? (
                        <div className={styles['btn-row']}>
                            <Btn
                                caption="Удалить"
                                bgColor="grey"
                                onClick={() => { this.props.deleteMail(id); }}
                                style={{
                                    height: 30,
                                    fontSize: 12,
                                    borderRadius: 4,
                                }}
                            />
                            <SignButton
                                docId={id}
                                docModule="ibankul"
                                docType="mail2bank"
                                callback={this.props.fetchOutcomeList}
                                btnStyle={{
                                    height: 30,
                                    fontSize: 12,
                                    borderRadius: 4,
                                }}
                                height={30}
                                formClassName={styles['sign-form']}
                            />
                        </div>
                    )
                    : null}
            </div>
        );
    }
}
