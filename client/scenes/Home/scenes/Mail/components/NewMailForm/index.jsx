import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Dropzone from 'react-dropzone';
import { Scrollbars } from 'react-custom-scrollbars';

import InputGroup from 'components/ui/InputGroup';
import InputTextarea from 'components/ui/InputTextarea';
import Btn from 'components/ui/Btn';
import SignButton from 'containers/SignButton';

import styles from './new-mail-form.css';

export default class NewMailForm extends Component {
    static propTypes = {
        id: PropTypes.string,
        requestSign: PropTypes.bool,
        /** Тема письма */
        caption: PropTypes.string,
        /** Текст письма  */
        text: PropTypes.string,
        handleInputChange: PropTypes.func.isRequired,
        handleFileChange: PropTypes.func.isRequired,
        fileList: PropTypes.array,
        deleteFile: PropTypes.func.isRequired,
        saveNewMail: PropTypes.func.isRequired,
        handleSignBtnClick: PropTypes.func.isRequired,
        signCallback: PropTypes.func.isRequired,
        errorSignCallback: PropTypes.func.isRequired,
        cancelSignCallback: PropTypes.func.isRequired,
        deleteFileFromISimple: PropTypes.func.isRequired,
        resetForm: PropTypes.func.isRequired,
        status: PropTypes.string,
    }

    static defaultProps = {
        id: null,
        requestSign: false,
        caption: '',
        text: '',
        fileList: [],
        status: '',
    }

    componentDidUpdate(prevProps) {
        if (this.props.id && !prevProps.requestSign && this.props.requestSign) {
            global.console.log('show sign');
            const signBtn = this.signBtn.getWrappedInstance();
            signBtn.forceUpdate(() => {
                signBtn.signDocument();
            });
        }
    }

    render() {
        const dropzoneStyle = {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            width: '100%',
            height: 90,
            border: '1px dashed #d8d8d8',
            borderRadius: 8,
            cursor: 'pointer',
        };

        return (
            <div className={styles.wrapper}>
                <div className={styles.form}>
                    <Scrollbars>
                        <div className={styles.header}>
                            Новое письмо
                        </div>
                        <div className={styles['form-wrapper']}>
                            <div className={styles['input-row']}>
                                <InputGroup
                                    id="caption"
                                    caption="Тема письма"
                                    value={this.props.caption}
                                    onChange={this.props.handleInputChange}
                                />
                            </div>
                            <div className={styles['input-row']}>
                                <InputTextarea
                                    id="text"
                                    caption="Текст письма"
                                    value={this.props.text}
                                    onChange={this.props.handleInputChange}
                                    autosize
                                    textareaStyle={{
                                        fontSize: 16,
                                    }}
                                />
                            </div>
                            <div className={styles['input-row']}>
                                <Dropzone onDrop={this.props.handleFileChange} style={dropzoneStyle}>
                                    <div className={styles['dropzone-text']}>
                                        <div>
                                            <span>Выберите файл</span>
                                        </div>
                                        <div>или перетащите в эту область</div>
                                    </div>
                                </Dropzone>
                                <div className={styles['file-info']}>
                                    Размер файла не более 15Мб, форматы .doc, .jpeg, .pdf, .png
                                </div>
                                <div className={styles['file-list']}>
                                    {this.props.fileList.map((file, i) => (
                                        <div
                                            key={i}
                                            className={styles['file-item']}
                                        >
                                            <div className={styles['file-icon']} />

                                            {file.id
                                                ? (
                                                    <div className={styles['file-wrap']}>
                                                        <div className={styles['file-name']}>
                                                            {file.fileName}
                                                        </div>
                                                        <div
                                                            className={styles['file-delete']}
                                                            onClick={() => {
                                                                this.props.deleteFileFromISimple(
                                                                    this.props.id,
                                                                    file.id,
                                                                );
                                                            }}
                                                        >
                                                            &times;
                                                        </div>
                                                    </div>
                                                )
                                                : (
                                                    <div className={styles['file-wrap']}>
                                                        <div className={styles['file-name']}>
                                                            {file.name}
                                                        </div>
                                                        <div
                                                            className={styles['file-delete']}
                                                            onClick={() => { this.props.deleteFile(file); }}
                                                        >
                                                            &times;
                                                        </div>
                                                    </div>
                                                )
                                            }
                                        </div>
                                    ))}
                                </div>
                            </div>
                        </div>
                    </Scrollbars>
                </div>
                <div className={styles['btn-row']}>
                    <Btn
                        caption="Сохранить"
                        bgColor="grey"
                        onClick={this.props.saveNewMail}
                        style={{
                            display: 'none',
                        }}
                    />
                    {this.props.status === 'new'
                        ? (
                            <Btn
                                caption="Отмена"
                                bgColor="grey"
                                onClick={this.props.resetForm}
                                style={{ marginRight: 20 }}
                            />
                        )
                        : null
                    }
                    <SignButton
                        ref={(el) => { this.signBtn = el; }}
                        docId={this.props.id}
                        docModule="ibankul"
                        docType="mail2bank"
                        onClick={this.props.handleSignBtnClick}
                        callback={this.props.signCallback}
                        errorCallback={this.props.errorSignCallback}
                        cancelSignCallback={this.props.cancelSignCallback}
                        btnStyle={{
                            width: 240,
                        }}
                        width={240}
                        height={45}
                    />
                </div>
            </div>
        );
    }
}
