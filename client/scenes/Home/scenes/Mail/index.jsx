import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment';

import { fetch, dates, downloadBlob } from 'utils';

import fetchIncome from 'modules/mail/actions/income/fetch';
import fetchOutcome from 'modules/mail/actions/outcome/fetch';
import fetchUnreadMsg from 'modules/mail/actions/unread/fetch';
import setMessageData from 'modules/info_message/actions/set_data';

import SceneWrapper from 'components/SceneWrapper';
import Loader from 'components/Loader';

import MailList from './components/MailList';
import NewMail from './containers/NewMail';

class Mail extends Component {
    static propTypes = {
        fetchIncome: PropTypes.func.isRequired,
        fetchOutcome: PropTypes.func.isRequired,
        income: PropTypes.object,
        outcome: PropTypes.object,
        unreadMsg: PropTypes.object,
        fetchUnreadMsg: PropTypes.func.isRequired,
        setMessageData: PropTypes.func.isRequired,
    }

    static defaultProps = {
        income: {},
        outcome: {},
        unreadMsg: {},
    }

    constructor(props) {
        super(props);

        const today = dates.resetDate(new Date());

        let initialFromDate;
        if (today.getDate() === 31) {
            initialFromDate = new Date(today.getFullYear(), today.getMonth(), 1);
        } else {
            initialFromDate = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 30);
        }

        this.state = {
            newMailTheme: '',
            newMailText: '',
            fileList: [],
            theme: '',
            from: '',
            to: '',
            initialFromDate,
            initialToDate: today,
            operationNotifyList: [],
        };
    }

    componentWillMount() {
        const today = this.state.initialToDate;
        const firstDate = this.state.initialFromDate;

        this.props.fetchIncome({
            from: dates.getDateString(firstDate),
            to: dates.getDateString(today),
        });

        this.props.fetchOutcome({
            from: dates.getDateString(firstDate),
            to: dates.getDateString(today),
        });

        this.setState({
            theme: '',
            from: dates.getDateString(firstDate),
            to: dates.getDateString(today),
            statusList: ['all'],
        });
    }

    fetchIncomeList = () => {
        this.props.fetchIncome({
            from: this.state.from,
            to: this.state.to,
        });
    }

    fetchOutcomeList = () => {
        this.props.fetchOutcome({
            from: this.state.from,
            to: this.state.to,
        });
    }

    readMessage = id => fetch(`/api/v1/msg/mail/unread/${id}`, {
        method: 'PUT',
    })
        .then(response => response.text())
        .then((text) => {
            if (text.length === 0) {
                return true;
            }
            return false;
        })
        .catch(() => false)

    getMailList = () => {
        const income = this.props.income.data ? this.props.income.data : [];
        const outcome = this.props.outcome.data ? this.props.outcome.data : [];

        const list = [...income, ...outcome];

        const unreadId = this.props.unreadMsg && this.props.unreadMsg.ids ? this.props.unreadMsg.ids : [];
        const readId = income.map(mail => mail.id).filter(id => unreadId.indexOf(id) >= 0);

        if (readId.length > 0) {
            Promise.all(readId.map(id => this.readMessage(id)))
                .then(() => {
                    this.props.fetchUnreadMsg();
                });
        }

        return list.sort((a, b) => {
            const aDate = new Date(a.createstamp);
            const bDate = new Date(b.createstamp);

            return bDate.getTime() - aDate.getTime();
        });
    }

    getOperationNotify = () => {
        return fetch('/api/v1/msg/mail/operation_notify', {
            credentials: 'include',
            method: 'POST',
            body: JSON.stringify({
                from: this.state.from,
                to: this.state.to,
            }),
        })
            .then(response => response.json())
            .then((json) => {
                let notify = [];

                if (json !== null) {
                    notify = Array.isArray(json.rosevroSimpleDocument)
                        ? json.rosevroSimpleDocument.map(el => el.operationNotifyInfo)
                        : [json.rosevroSimpleDocument].map(el => el.operationNotifyInfo);
                }

                this.setState({
                    operationNotifyList: notify,
                });
            })
            .catch((err) => {
                global.console.log(err);
            });
    }

    downloadFile = (mailId, file) => {
        fetch(`/api/v1/msg/mail/attachment/${mailId}/${file.id}`, {
            credentials: 'include',
            method: 'POST',
            body: JSON.stringify({

            }),
        })
            .then((response) => {
                if (response.status === 200) {
                    return response.blob();
                }
            })
            .then((blob) => {
                downloadBlob(blob, file.name || file.fileName);
            })
            .catch((err) => {
                global.console.log(err);
            });
    }

    deleteMail = (id) => {
        this.toggleLoader(true);

        fetch('/api/v1/documents/delete', {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify({
                documentId: id,
            }),
        })
            .then(response => response.text())
            .then((text) => {
                if (/errorText/.test(text)) {
                    const json = JSON.parse(text);
                    const modalText = json.errorText;
                    this.props.setMessageData({
                        isVisible: true,
                        header: 'Внимание!',
                        body: modalText,
                        btnText: 'Назад',
                        callback: () => this.props.setMessageData({ isVisible: false }),
                    });
                }

                const filter = {
                    from: this.state.from,
                    to: this.state.to,
                };
                this.props.fetchOutcome(filter);
                this.toggleLoader(false);
            })
            .catch((err) => {
                global.console.log(err);
                this.toggleLoader(false);
            });
    }

    toggleLoader = (isVisible) => {
        this.setState({
            isFetching: isVisible,
        });
    }

    clearDateFilter = () => {
        this.setState({
            from: moment(this.state.initialFromDate).format('DD.MM.YYYY'),
            to: moment(this.state.initialToDate).format('DD.MM.YYYY'),
        }, () => {
            this.fetchIncomeList();
            this.fetchOutcomeList();
        });
    }

    acceptDateFilter = ({ startDate, endDate }) => {
        this.setState({
            from: startDate ? startDate.format('DD.MM.YYYY') : endDate.format('DD.MM.YYYY'),
            to: endDate ? endDate.format('DD.MM.YYYY') : startDate.format('DD.MM.YYYY'),
        }, () => {
            this.fetchIncomeList();
            this.fetchOutcomeList();
            this.getOperationNotify();
        });
    }

    getSavedMailData = (data) => {
        this.NewMail.setDataFromSavedMail(data);
    }

    render() {
        const mailList = this.getMailList();

        const isFetching = this.props.income.isFetching || this.props.outcome.isFetching || this.state.isFetching;

        return (
            <SceneWrapper>
                <Loader visible={isFetching} style={{ backgroundColor: 'rgba(255, 255, 255, 0.8)' }} />
                <div style={{ display: 'flex', height: '100%' }}>
                    <MailList
                        initialStartDate={moment(this.state.initialFromDate)}
                        initialEndDate={moment(this.state.initialToDate)}
                        mailList={mailList}
                        getOperationNotify={this.getOperationNotify}
                        operationNotifyList={this.state.operationNotifyList}
                        downloadFile={this.downloadFile}
                        deleteMail={this.deleteMail}
                        fetchOutcomeList={this.fetchOutcomeList}
                        acceptDateFilter={this.acceptDateFilter}
                        clearDateFilter={this.clearDateFilter}
                        getSavedMailData={this.getSavedMailData}
                    />
                    <NewMail
                        fetchOutcomeList={this.fetchOutcomeList}
                        toggleLoader={this.toggleLoader}
                        setMessageData={this.props.setMessageData}
                        setDataFromSavedMail={this.setDataFromSavedMail}
                        ref={(component) => { this.NewMail = component; }}
                    />
                </div>
            </SceneWrapper>
        );
    }
}

function mapStateToProps(state) {
    return {
        income: state.mail.income,
        outcome: state.mail.outcome,
        unreadMsg: state.mail.unread.data,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        fetchIncome: bindActionCreators(fetchIncome, dispatch),
        fetchOutcome: bindActionCreators(fetchOutcome, dispatch),
        fetchUnreadMsg: bindActionCreators(fetchUnreadMsg, dispatch),
        setMessageData: bindActionCreators(setMessageData, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Mail);
