import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { fetch } from 'utils';

import fetchNewPayments from 'modules/payments/new_payments/actions/fetch';
import setPaymentModalData from 'modules/payments/payment_modal/actions/set_data';
import setPaymentModalVisibility from 'modules/payments/payment_modal/actions/set_visibility';
import setBackUrl from 'modules/settings/actions/set_back_url';

import SceneWrapper from 'components/SceneWrapper';

import PaymentList from './components/PaymentList';

let cacheState = null;

class SignPayments extends Component {
    // static propTypes = {
    // };

    static contextTypes = {
        router: PropTypes.object.isRequired,
    };

    constructor(props) {
        super(props);

        this.state = {
            selectedItems: [],
        };
    }

    componentWillMount() {
        if (cacheState) {
            this.setState(cacheState);
        }
    }

    selectItem = (item) => {
        const hasItem = this.state.selectedItems.find(el => el.platporDocument.id === item.platporDocument.id);

        if (hasItem) {
            this.setState({
                selectedItems: this.state.selectedItems.filter(el => el.platporDocument.id !== item.platporDocument.id),
            });
        } else {
            this.setState({
                selectedItems: [...this.state.selectedItems, item],
            });
        }
    }

    selectAll = () => {
        this.setState({
            selectedItems: [...this.props.newPayments],
        });
    }

    clearAll = () => {
        this.setState({
            selectedItems: [],
        });
    }

    viewPayment = (data) => {
        let dataForView = data;
        if (data.platporDocument) {
            dataForView = data.platporDocument;
        }

        cacheState = this.state;

        this.props.setBackUrl('/home/sign_payments');

        this.props.setPaymentModalData(Object.assign(dataForView, {
            income: !data.platporDocument, // || check line_is_debet
        }));

        this.props.setPaymentModalVisibility(true);
        // this.context.router.history.push('/home/payment');
    }

    /**
     * Отправка запроса на удаление документа в iSimple
     * @param {string|number} id - идентификатор документа
     * @return {Promise}
     */
    sendDeleteDocument = id => new Promise((resolve, reject) => {
        fetch('/api/v1/documents/delete', {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify({
                documentId: id,
            }),
        }).then((response) => {
            global.console.log(response);
            return response.text();
        }).then((data) => {
            if (data.length > 0) {
                const json = JSON.parse(data);
                if (json.errorText) {
                    return reject({
                        errorText: json.errorText,
                    });
                }
            }
            return resolve(true);
        }).catch((err) => {
            global.console.warn('Error in deleting document', err);
            return reject(err);
        });
    })

    deleteDocuments = () => {
        const promises = this.state.selectedItems.map(item => this.sendDeleteDocument(item.platporDocument.id));
        this.setState({
            isFetching: true,
        });
        return Promise.all(promises)
            .then((values) => {
                global.console.log(values);
                this.setState({
                    selectedItems: [],
                });
                this.props.fetchNewPayments();
            })
            .catch((err) => {
                global.console.log(err);
            });
    }

    multipleSignCallback = () => {
        this.setState({
            selectedItems: [],
        });
    }

    render() {
        return (
            <SceneWrapper>
                <PaymentList
                    list={this.props.newPayments}
                    selectItem={this.selectItem}
                    selectAll={this.selectAll}
                    clearAll={this.clearAll}
                    deleteDocuments={this.deleteDocuments}
                    viewPayment={this.viewPayment}
                    multipleSignCallback={this.multipleSignCallback}
                    {...this.state}
                />
            </SceneWrapper>
        );
    }
}

function mapStateToProps(state) {
    return {
        newPayments: state.payments.newPayments.data,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        fetchNewPayments: bindActionCreators(fetchNewPayments, dispatch),
        setPaymentModalData: bindActionCreators(setPaymentModalData, dispatch),
        setBackUrl: bindActionCreators(setBackUrl, dispatch),
        setPaymentModalVisibility: bindActionCreators(setPaymentModalVisibility, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(SignPayments);
