import React from 'react';
import PropTypes from 'prop-types';
import { Scrollbars } from 'react-custom-scrollbars';

import Btn from 'components/ui/Btn';

import styles from './calendar.css';

const Wrapper = props => (
    <div className={styles.container}>
        <div className={styles.header}>
            Заметки
        </div>
        <Scrollbars style={{ width: 'calc(100% - 500px)' }}>
            <div className={styles.content}>
                {props.children}
            </div>
        </Scrollbars>
        {props.userNoteActiveId !== ''
            ? (
                <div className={styles.footer}>
                    <Btn
                        caption="Удалить"
                        bgColor="grey"
                        onClick={props.handleDeleteBtn}
                    />
                    <Btn
                        caption="Редактировать"
                        onClick={props.handleUpdateBtn}
                    />
                </div>
            )
            : (
                <div className={styles.footer}>
                    <Btn
                        caption="Создать"
                        onClick={props.handleSaveBtn}
                    />
                </div>
            )
        }
    </div>
);

Wrapper.propTypes = {
    children: PropTypes.node,
    handleSaveBtn: PropTypes.func,
    handleDeleteBtn: PropTypes.func,
    handleUpdateBtn: PropTypes.func,
    userNoteActiveId: PropTypes.string,
};

Wrapper.defaultProps = {
    children: null,
    handleSaveBtn: undefined,
    handleDeleteBtn: undefined,
    handleUpdateBtn: undefined,
    userNoteActiveId: '',
};


export default Wrapper;
