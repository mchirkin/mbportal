import React from 'react';
import PropTypes from 'prop-types';

import InputDayPicker from 'components/ui/InputDayPicker';
import InputGroup from 'components/ui/InputGroup';
import InputTextarea from 'components/ui/InputTextarea';

import styles from './calendar.css';

const Form = props => (
    <div className={styles.forms}>
        <InputDayPicker
            id="noteDate"
            title="Дата"
            value={props.noteDate}
            getDateValue={props.handleDayPicker}
            invalid={(
                props.invalidFields.includes('noteDate')
                    ? 'Проверьте корректность данных'
                    : null
            )}
        />
        <div>
            <InputGroup
                id="noteName"
                caption="Название"
                onChange={props.handleInputChange}
                hintDisabled
                value={props.noteName}
                invalid={(
                    props.invalidFields.includes('noteName')
                        ? 'Проверьте корректность данных'
                        : null
                )}
            />
        </div>
        <InputTextarea
            id="noteComment"
            caption="Описание"
            height={80}
            style={{ width: '100%' }}
            textareaStyle={{ fontSize: 16 }}
            onChange={props.handleInputChange}
            autosize
            value={props.noteComment}
            invalid={(
                props.invalidFields.includes('noteComment')
                    ? 'Проверьте корректность данных'
                    : null
            )}
        />
        <InputGroup
            id="noteUrl"
            caption="Ссылка (необязательно)"
            onChange={props.handleInputChange}
            hintDisabled
            value={props.noteUrl}
        />
    </div>
);

Form.propTypes = {
    handleInputChange: PropTypes.func,
    handleDayPicker: PropTypes.func,
    noteName: PropTypes.string,
    noteComment: PropTypes.string,
    noteDate: PropTypes.string,
    noteUrl: PropTypes.string,
    invalidFields: PropTypes.array,
};

Form.defaultProps = {
    handleInputChange: undefined,
    handleDayPicker: undefined,
    noteName: '',
    noteComment: '',
    noteDate: '',
    noteUrl: '',
    invalidFields: [],
};

export default Form;
