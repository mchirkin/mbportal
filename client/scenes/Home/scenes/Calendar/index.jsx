import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import moment from 'moment';

import { fetch } from 'utils';
import { fetchNote } from 'modules/user_note/note';
import { setUserNoteActiveId } from 'modules/user_note/active_id';
import { toggleCalendarWidget } from 'modules/calendar_widget/toggle_calendar_widget';
import setMessageData from 'modules/info_message/actions/set_data';

import SceneWrapper from 'components/SceneWrapper';

import Form from './components/Form';
import Wrapper from './components/Wrapper';

function mapStateToProps(state) {
    return {
        userInfo: state.user.userInfo.data,
        userNote: state.userNote.note.data,
        userNoteActiveId: state.userNote.activeId,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        fetchNote: bindActionCreators(fetchNote, dispatch),
        toggleCalendarWidget: bindActionCreators(toggleCalendarWidget, dispatch),
        setMessageData: bindActionCreators(setMessageData, dispatch),
        setUserNoteActiveId: bindActionCreators(setUserNoteActiveId, dispatch),
    };
}

const initialState = {
    noteName: '',
    noteComment: '',
    noteDate: moment().format('DD.MM.YYYY'),
    noteUrl: '',
    invalidFields: [],
};

@withRouter
@connect(mapStateToProps, mapDispatchToProps)
class Calendar extends Component {
    static propTypes = {
        fetchNote: PropTypes.func,
        toggleCalendarWidget: PropTypes.func,
        setMessageData: PropTypes.func,
        userNote: PropTypes.array,
        userInfo: PropTypes.object,
        history: PropTypes.object.isRequired,
        userNoteActiveId: PropTypes.string,
        setUserNoteActiveId: PropTypes.func.isRequired,
    }

    static defaultProps = {
        fetchNote: undefined,
        toggleCalendarWidget: undefined,
        setMessageData: undefined,
        userInfo: {},
        userNote: [],
        userNoteActiveId: '',
    }

    constructor(props) {
        super(props);

        this.state = initialState;
    }

    componentWillMount() {
        this.props.toggleCalendarWidget(true);

        this.props.history.listen((location) => {
            if (!/calendar/.test(location.pathname)) {
                this.props.toggleCalendarWidget(false);
                this.props.setUserNoteActiveId('');
            }
        });

        if (this.props.userNoteActiveId !== '' && this.props.userNote) {
            const selectedNote = this.props.userNote.find(note => this.props.userNoteActiveId === note._id);

            this.setState({
                noteName: selectedNote.name,
                noteComment: selectedNote.comment,
                noteDate: moment(selectedNote.date).format('DD.MM.YYYY'),
                noteUrl: selectedNote.url,
            });
        }

        if (this.props.userNoteActiveId === '' && this.props.location.state && this.props.location.state.date) {
            const { date } = this.props.location.state;

            this.setState({
                noteDate: date,
            });
        }
    }

    handleInputChange = (e) => {
        const id = e.id || e.target.getAttribute('id');
        const value = e.target.value;

        this.setState({
            [id]: value,
        });

        if (this.state.invalidFields.includes(id)) {
            this.deleteInvalidInput(id);
        }
    }

    handleDayPicker = (date, id) => {
        this.setState({
            [id]: date,
        });

        if (this.state.invalidFields.includes(id)) {
            this.deleteInvalidInput(id);
        }
    }

    fetchNote = (extRef) => {
        this.props.fetchNote({ extRef });
    }

    deleteInvalidInput = (id) => {
        if (this.state.invalidFields.includes(id)) {
            const newFields = this.state.invalidFields.filter(e => e !== id);

            this.setState({
                invalidFields: newFields,
            });
        }
    }

    handleUpdateBtn = () => {
        const data = {
            _id: this.props.userNoteActiveId,
            extRef: this.props.userInfo.extRef,
            name: this.state.noteName,
            comment: this.state.noteComment,
            date: moment(this.state.noteDate, 'DD.MM.YYYY').toISOString(),
            url: this.state.noteUrl,
        };

        const url = '/api/v1/financial_calendar/user_note/update';
        const options = {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify(data),
        };

        const invalidFields = [];

        if (this.state.noteName === '') invalidFields.push('noteName');
        // if (this.state.noteComment === '') invalidFields.push('noteComment');
        if (this.state.noteDate === '') invalidFields.push('noteDate');

        if (invalidFields.length > 0) {
            this.setState({
                invalidFields,
            });

            return;
        }

        fetch(url, options)
            .then(response => response.json())
            .then((json) => {
                let header = 'Заметка обновлена!';
                let msg = '';

                if (json.errorCode) {
                    header = 'Что-то пошло не так...';
                    msg = json.errorMessage;

                    this.setState({
                        invalidFields,
                    });
                } else {
                    this.fetchNote(this.props.userInfo.extRef);
                }

                this.props.setMessageData({
                    isVisible: true,
                    header,
                    body: msg,
                    btnText: json.error ? 'Назад' : 'Далее',
                    callback: () => this.props.setMessageData({ isVisible: false }),
                });
            })
            .catch((error) => {
                console.log('error', error);
            });
    }

    handleDeleteBtn = () => {
        const data = {
            id: this.props.userNoteActiveId,
        };

        const url = '/api/v1/financial_calendar/user_note/delete';
        const options = {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify(data),
        };

        fetch(url, options)
            .then(response => response.json())
            .then((json) => {
                let header = 'Заметка удалена!';
                let msg = '';

                if (json.errorCode) {
                    header = 'Что-то пошло не так...';
                    msg = json.errorMessage;
                } else {
                    this.fetchNote(this.props.userInfo.extRef);
                }

                this.props.setUserNoteActiveId('');

                this.setState(initialState);

                this.props.setMessageData({
                    isVisible: true,
                    header,
                    body: msg,
                    btnText: json.error ? 'Назад' : 'Далее',
                    callback: () => this.props.setMessageData({ isVisible: false }),
                });
            })
            .catch((error) => {
                console.log('error', error);
            });
    }


    handleSaveBtn = () => {
        const data = {
            extRef: this.props.userInfo.extRef,
            name: this.state.noteName,
            comment: this.state.noteComment,
            date: moment(this.state.noteDate, 'DD.MM.YYYY').toISOString(),
            url: this.state.noteUrl,
        };

        const url = '/api/v1/financial_calendar/user_note/create';
        const options = {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify(data),
        };

        const invalidFields = [];

        if (this.state.noteName === '') invalidFields.push('noteName');
        if (this.state.noteComment === '') invalidFields.push('noteComment');
        if (this.state.noteDate === '') invalidFields.push('noteDate');

        fetch(url, options)
            .then(response => response.json())
            .then((json) => {
                let header = 'Заметка создана!';
                let msg = '';

                if (json.errorCode) {
                    header = 'Что-то пошло не так...';
                    msg = json.errorMessage;

                    this.setState({
                        invalidFields,
                    });
                } else {
                    this.fetchNote(this.props.userInfo.extRef);
                }

                this.props.setMessageData({
                    isVisible: true,
                    header,
                    body: msg,
                    btnText: json.error ? 'Назад' : 'Далее',
                    callback: () => this.props.setMessageData({ isVisible: false }),
                });
            })
            .catch((error) => {
                console.log('error', error);
            });
    }

    render() {
        return (
            <SceneWrapper>
                <Wrapper
                    handleSaveBtn={this.handleSaveBtn}
                    handleUpdateBtn={this.handleUpdateBtn}
                    handleDeleteBtn={this.handleDeleteBtn}
                    userNoteActiveId={this.props.userNoteActiveId}
                >
                    <Form
                        handleInputChange={this.handleInputChange}
                        handleDayPicker={this.handleDayPicker}
                        {...this.state}
                    />
                </Wrapper>
            </SceneWrapper>
        );
    }
}

export default Calendar;
