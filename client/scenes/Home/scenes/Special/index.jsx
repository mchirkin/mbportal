import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { fetch } from 'utils';

import setMessageData from 'modules/info_message/actions/set_data';

import SceneWrapper from 'components/SceneWrapper';
import Loader from 'components/Loader';

import PartnersList from './components/PartnersList';

class Special extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isFetching: false,
        };
    }

    sendMail = (partnerName) => {
        this.setState({
            isFetching: true,
        });

        fetch('/api/v1/msg/mail/send', {
            credentials: 'include',
            method: 'POST',
            body: JSON.stringify({
                user: this.props.user,
                org: this.props.org,
                managers: this.props.managers,
                partner: partnerName,
            }),
        })
            .then(response => response.json())
            .then((json) => {
                global.console.log(json);
                this.setState({
                    isFetching: false,
                });

                let header = 'Ваша заявка успешно принята!';
                let msg = 'Наш менеджер свяжется с вами в ближайшее время';

                if (json.error) {
                    header = 'Что-то пошло не так...';
                    msg = 'Попробуйте отправить заявку еще раз';
                }

                this.props.setMessageData({
                    isVisible: true,
                    header,
                    body: msg,
                    btnText: json.error ? 'Назад' : 'Далее',
                    callback: () => this.props.setMessageData({ isVisible: false }),
                });
            });
    }

    render() {
        return (
            <SceneWrapper>
                <Loader visible={this.state.isFetching} style={{ backgroundColor: 'rgba(255, 255, 255, 0.8)' }} />
                <PartnersList
                    isFetching={this.state.isFetching}
                    sendMail={this.sendMail}
                />
            </SceneWrapper>
        );
    }
}

function mapStateToProps(state) {
    return {
        user: state.user.userInfo.data,
        org: state.user.orgInfo.data,
        managers: state.user.managers.data,
    };
}


function mapDispatchToProps(dispatch) {
    return {
        setMessageData: bindActionCreators(setMessageData, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Special);
