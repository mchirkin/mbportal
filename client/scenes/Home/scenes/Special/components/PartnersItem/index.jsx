import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import styles from './partners.css';

export default class PartnersItem extends PureComponent {
    static propTypes = {
        /** путь к логотипу партнера */
        logoSrc: PropTypes.string.isRequired,
        /** описание предложения */
        description: PropTypes.string,
        /** название партнера */
        name: PropTypes.string.isRequired,
        /** отправка предложения на почту менеджерам */
        sendMail: PropTypes.func.isRequired,
    };

    static defaultProps = {
        description: '',
    };

    render() {
        const {
            logoSrc,
            description,
            name,
            sendMail,
        } = this.props;

        return (
            <div className={styles.wrapper}>
                <div className={styles['company-logo']}>
                    <div style={{ backgroundImage: `url(${logoSrc})` }} />
                </div>
                <div className={styles['company-description']}>
                    {description}
                </div>
                <div className={styles['get-btn']} onClick={() => { sendMail(name); }}>
                    Воспользоваться предложением
                </div>
            </div>
        );
    }
}
