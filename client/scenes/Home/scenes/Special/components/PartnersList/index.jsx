/** Third-party */
import React from 'react';
import PropTypes from 'prop-types';
/** Global */
import BlockLoader from 'components/Loader';
/** Relative */
import PartnersItem from '../PartnersItem';
/** Styles */
import styles from './partners.css';
/** Images */
import kontur from './img/kontur.jpg';
import knopka from './img/knopka.jpg';
import tilda from './img/tilda.jpg';
import smartReading from './img/smartreading.svg';
import amplifr from './img/amplifr.jpg';
import gettaxi from './img/gettaxi.svg';
import myTarget from './img/mytarget.jpg';

const PartnersList = ({ sendMail, isFetching }) => {
    const partnersList = [
        {
            logoSrc: kontur,
            description: '90 дней бесплатного доступа',
            name: 'Контур Бухгалтерия',
        },
        {
            logoSrc: myTarget,
            description: 'до 50 000 рублей на рекламу через систему my target',
            name: 'My Target',
        },
        {
            logoSrc: knopka,
            description: '10 часов работ юриста или бизнес-ассистента в подарок',
            name: 'Кнопка',
        },
        {
            logoSrc: tilda,
            description: '2 месяца бесплатного абонентского обслуживания',
            name: 'Tilda Publishing',
        },
        {
            logoSrc: amplifr,
            description: '3 месяца пользования Amplifr со скидкой 30%',
            name: 'Amplifr',
        },
        {
            logoSrc: gettaxi,
            description: '600 рублей на первую поездку с Gett и спец-тарифы для малого бизнеса',
            name: 'Gett Taxi и Gett Курьер',
        },
        {
            logoSrc: smartReading,
            description: '12 бесплатных summary от Smart Reading',
            name: 'Smart Reading',
        },
    ];

    return (
        <div className={styles['partners-wrapper']}>
            <BlockLoader isFetching={isFetching} />
            <div className={styles['partners-list']}>
                {
                    partnersList.map((partner, i) => (
                        <PartnersItem
                            key={i}
                            logoSrc={partner.logoSrc}
                            description={partner.description}
                            name={partner.name}
                            sendMail={sendMail}
                        />
                    ))
                }
            </div>
        </div>
    );
};

PartnersList.propTypes = {
    /** переключение видимости списка спецпредложений */
    toggleModalVisibility: PropTypes.func,
    /** показывать лоадер или нет */
    isFetching: PropTypes.bool,
    /** отправка уведомления на почту менеджеру */
    sendMail: PropTypes.func.isRequired,
};

PartnersList.defaultProps = {
    toggleModalVisibility: undefined,
    isFetching: false,
};

export default PartnersList;
