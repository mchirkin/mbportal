import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Btn from 'components/ui/Btn';
import InputTextarea from 'components/ui/InputTextarea';
import Loader from 'components/Loader';
import SignButton from 'containers/SignButton';

import styles from './cancel.css';

/**
 * Модальное окно создания отзыва документа
 */
export default class CancelDocumentModal extends Component {
    static propTypes = {
        /** нужно ли подписывать документ на отзыв */
        needSign: PropTypes.bool,
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.cancelId && nextProps.needSign && !this.props.needSign) {
            const signBtn = this.signBtn.getWrappedInstance();
            signBtn.forceUpdate(() => {
                signBtn.signDocument();
            });
        }
    }

    render() {
        /**
         * Тип документа
         * type {string}
         */
        let docType;
        if (this.props.docData) {
            switch (this.props.docData.docType) {
                case 'doc_platpor':
                    docType = 'Платежное поручение';
                    break;
                default:
                    docType = 'Не определено';
            }
        }

        const showDeclineInfo = (
            this.props.cancelData &&
            this.props.cancelData.status === 'decline'
        );

        return (
            <div className={styles['cancel-modal']}>
                <Loader visible={this.props.isFetching} style={{ backgroundColor: 'rgba(255, 255, 255, 0.7)' }} />
                <div className={styles.reason}>
                    <InputTextarea
                        caption="Причина отзыва"
                        onChange={this.props.handleReasonChange}
                        value={this.props.reason}
                        disabled={!this.props.editable}
                        autosize
                    />
                </div>
                <div className={styles['btn-row']}>
                    <Btn
                        bgColor="grey"
                        caption="&times;"
                        onClick={this.props.handleCancelBtnClick}
                        style={{
                            width: 46,
                            fontSize: 34,
                        }}
                    />
                    {this.props.editable
                        ? (
                            <SignButton
                                docId={this.props.cancelId}
                                docModule="ibankul"
                                docType="request_cancel"
                                btnCaption="Отозвать"
                                onClick={this.props.handleSubmitCancelBtnClick}
                                callback={this.props.signCallback}
                                height={45}
                                ref={(el) => { this.signBtn = el; }}
                            />
                        )
                        : null}
                    {false && this.props.editable
                        ? (
                            <Btn
                                width={120}
                                bgColor="white"
                                caption="Сохранить"
                                handleClick={this.props.handleSaveBtnClick}
                            />
                        )
                        : null}
                </div>
            </div>
        );
    }
}
