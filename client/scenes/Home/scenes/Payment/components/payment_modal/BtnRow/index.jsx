import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Btn from 'components/ui/Btn';
import SignButton from 'containers/SignButton';
import styles from './payment.css';

export default class BtnRow extends Component {
    static propTypes = {
        modalData: PropTypes.object,
    };

    static defaultProps = {
        modalData: null,
    };

    componentDidUpdate(prevProps) {
        if (
            this.props.modalData &&
            this.props.modalData.id &&
            this.props.modalData.requestSign &&
            (!prevProps.modalData || !prevProps.modalData.requestSign || this.props.modalData.justCreated)
        ) {
            const signBtn = this.signBtn.getWrappedInstance();
            signBtn.forceUpdate(() => {
                signBtn.signDocument();
            });
        }
    }

    render() {
        const props = this.props;

        const showSignAndSaveBtn = (
            !props.modalData ||
            props.modalData.status === 'new' ||
            props.modalData.status === 'imported'
        );

        const showSaveTemplateBtn = (
            !props.modalData ||
            props.modalData.status !== 'decline' && !props.modalData.income
        );

        const showCopyBtn = (
            props.modalData &&
            props.modalData.status === 'decline' &&
            !props.modalData.income
        );

        const editTemplate = props.modalData && props.modalData.editTemplate;

        const showPDFBtn = (
            props.modalData &&
            props.modalData.status !== 'new' &&
            props.modalData.id
        );

        const showCancelDocumentBtn = (
            props.modalData &&
            /send/.test(props.modalData.status)
        );

        return (
            <div className={styles['action-btn-row']}>
                <div className={styles['action-btn-wrapper']} data-smallform={props.smallForm}>
                    <div className={styles['btn-group']}>
                        <Btn
                            bgColor="grey"
                            caption="&times;"
                            onClick={props.closeModal}
                            style={{
                                width: 46,
                                marginRight: 20,
                                fontSize: 34,
                            }}
                        />

                        {
                            showSignAndSaveBtn && !editTemplate && props.activeTab !== '1c'
                                ? (
                                    <Btn
                                        bgColor="grey"
                                        caption="Сохранить"
                                        onClick={() => { props.handleSaveBtnClick('save'); }}
                                        style={{
                                            // width: 120,
                                            marginRight: 20,
                                        }}
                                    />
                                )
                                : null
                        }
                        {
                            showSaveTemplateBtn && props.activeTab !== '1c'
                                ? (
                                    <Btn
                                        bgColor="grey"
                                        caption={props.selectedTemplateId ? 'Обновить шаблон' : 'Сохранить как шаблон'}
                                        onClick={props.handleSaveTemplateClick}
                                        style={{
                                            // width: editTemplate ? 120 : 220,
                                            marginRight: 20,
                                        }}
                                    />
                                )
                                : null
                        }
                        {
                            showCopyBtn && props.activeTab !== '1c'
                                ? (
                                    <Btn
                                        bgColor="grey"
                                        caption="Повторить"
                                        onClick={props.handleCopyClick}
                                        style={{
                                            // width: 120,
                                            marginRight: 20,
                                        }}
                                    />
                                )
                                : null
                        }
                        {
                            showPDFBtn && props.activeTab !== '1c'
                                ? (
                                    <Btn
                                        caption="Сохранить в PDF"
                                        onClick={props.handlePDFPrint}
                                        styles={{
                                            width: 180,
                                            marginRight: 10,
                                        }}
                                    />
                                )
                                : null
                        }
                    </div>
                    <div className={styles['btn-group']}>
                        {
                            showCancelDocumentBtn && props.activeTab !== '1c'
                                ? (
                                    <Btn
                                        color="gradient"
                                        caption="Отозвать"
                                        onClick={props.handleCancelDocument}
                                    />
                                )
                                : null
                        }
                        {
                            showSignAndSaveBtn && !editTemplate && props.activeTab !== '1c'
                                ? (
                                    <SignButton
                                        ref={(el) => { this.signBtn = el; }}
                                        docId={props.modalData ? props.modalData.id : null}
                                        docModule="ibankul"
                                        docType="doc_platpor"
                                        onClick={() => { props.handleSaveBtnClick('sign'); }}
                                        callback={props.signPaymentCallback}
                                        formStyle={{
                                            height: 42,
                                        }}
                                    />
                                )
                                : null
                        }
                    </div>
                </div>
            </div>
        );
    }
}
