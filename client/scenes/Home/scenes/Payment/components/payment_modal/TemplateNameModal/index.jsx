/** Thir party */
import React, { Component } from 'react';
/** Global */
import InputGroup from 'components/ui/InputGroup';
import ModalBtnRow from 'components/ModalBtnRow';
import ModalBtn from 'components/ModalBtn';
import Btn from 'components/ui/Btn';
/** styles */
import styles from './payment.css';

export default function TemplateNameModal(props) {
    return (
        <div className={styles['template-name-modal-wrapper']} data-visible={props.isVisible}>
            <div className={styles['template-name-modal']}>
                <h4 className={styles['template-modal-header']}>
                    Название шаблона
                </h4>
                <InputGroup
                    id="template-name"
                    value={props.templateName}
                    onChange={props.handleTemplateNameChange}
                    style={{
                        marginRight: 0,
                        border: '1px solid #41334b',
                    }}
                />
                <div className={styles['btn-row']}>
                    <Btn
                        bgColor="grey"
                        caption="&times;"
                        onClick={props.cancelSaveTemplate}
                        style={{
                            width: 46,
                            fontSize: 34,
                        }}
                    />
                    <Btn
                        caption="Сохранить"
                        onClick={props.savePaymentAsTemplate}
                    />
                </div>
            </div>
        </div>
    );
}
