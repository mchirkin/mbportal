/** Third-party */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import InputElement from 'react-input-mask';
/** Global */
import InputSelect from 'components/ui/InputSelect';
import colors from 'constants/colors';
/** Relative */
import * as Hints from '../Hints';
/** Styles */
import styles from './payment.css';

export default class NalogPeriod extends Component {
    static propTypes = {
        showHint: PropTypes.func.isRequired,
        invalid: PropTypes.array,
    }

    static defaultProps = {
        invalid: [],
    }

    showHint = () => {
        const invalidText = this.getInvalid();
        if (invalidText.length === 0) {
            this.props.showHint(this.group, Hints.NalogPeriod);
        } else {
            this.props.showHint(
                this.group,
                (
                    <div style={{ color: colors.ERROR_RED }}>
                        {invalidText}
                    </div>
                )
            );
        }
    }

    getInvalid = () => {
        if (this.props.invalid && this.props.invalid.length > 0) {
            return this.props.invalid.map((el) => {
                const text = el ? `${el}\n` : '';
                return text;
            }).join('');
        }
        return '';
    }

    render() {
        const selectStyle = {
            width: 70,
            height: 42,
            position: 'relative',
            border: 0,
            background: 'transparent',
            top: -10,
            marginRight: 0,
            paddingLeft: 0,
        };

        const taxPeriods = this.props.paymentsTaxperiod.map(period => ({
            period,
            value: period.code,
            valueCaption: `${period.code} - ${period.caption}`,
        }));

        const invalidText = this.getInvalid();

        return (
            <div
                className={styles['nalog-period']}
                onMouseEnter={this.showHint}
                onMouseLeave={this.props.deleteHint}
                data-disabled={this.props.disabled}
                data-invalid={invalidText && invalidText.length > 0}
                ref={(group) => { this.group = group; }}
            >
                <div className={styles['input-top']}>
                    <label>(107) - Налоговый период</label>
                    {/* <Hint text={Hints.NalogPeriod} hintPosition="top" style={{ top: -1 }} /> */}
                </div>
                <InputSelect
                    id="tax1"
                    style={selectStyle}
                    options={taxPeriods}
                    optionsListStyle={{
                        top: -153,
                        left: -6,
                        width: 220,
                        borderRadius: '8px 8px 0 0',
                    }}
                    chevronStyle={{
                        top: -10,
                        borderLeft: 0,
                        width: 25,
                    }}
                    onChange={this.props.handleSelectChange}
                    value={this.props.values.tax1}
                    disabled={this.props.disabled}
                />
                <InputElement
                    id="taxPeriod"
                    className={styles['period-input']}
                    mask="99.9999"
                    maskChar="_"
                    alwaysShowMask
                    value={this.props.values.taxPeriod || '__.____'}
                    onChange={this.props.handleInputChange}
                    disabled={this.props.disabled}
                />
            </div>
        );
    }
}
