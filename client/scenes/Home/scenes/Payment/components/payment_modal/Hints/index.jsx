import React from 'react';
import { TransitionGroup, CSSTransition } from 'react-transition-group';

export const DocNumber = (
    <CSSTransition
        timeout={500}
        key="docNumber"
        classNames="fadeappear"
        appear
        in
    >
        <div>
            Если не указан, то номер документа будет присвоен автоматически
        </div>
    </CSSTransition>
);

export const Urgen = (
    <CSSTransition
        timeout={500}
        key="urgen"
        classNames="fadeappear"
        appear
        in
    >
        <div>
            Очередность платежа нужна банку, чтобы понять, в какой последовательности обработать платежи.
            Когда средств недостаточно, выберите очередность (приоритет) платежа по его назначению.
        </div>
    </CSSTransition>
);

export const ReceiverName = (
    <CSSTransition
        timeout={500}
        key="receiverName"
        classNames="fadeappear"
        appear
        in
    >
        <div>
            Вы можете ввести ИНН и мы выполним поиск получателя автоматически или ввести наименование получателя:<br />
            <ul style={{ marginLeft: 15 }}>
                <li>
                    Если получатель юридическое лицо, укажите полное или сокращенное название, например: «ОАО Ромашка»,
                </li>
                <li>
                    если ИП — ФИО и статус, например: «Иванов Иван Иванович, ИП»,
                </li>
                <li>
                    если физическое лицо — ФИО.
                </li>
                <li>
                    в случае уплаты налогов укажите: УФК по _____
                    (укажите наименование субъекта РФ, в котором оплачиваете налог), <br />
                    а затем, в скобках — наименование вашей ИФНС.
                    Например, « УФК по г. Челябинску (ИФНС N 1 по г. Челябинску)
                </li>
            </ul>
            <br />
            Не более 160 символов
        </div>
    </CSSTransition>
);

export const Inn = (
    <CSSTransition
        timeout={500}
        key="inn"
        classNames="fadeappear"
        appear
        in
    >
        <div>
            <ul style={{ marginLeft: 15 }}>
                <li>
                    ИНН юридического лица: 10 цифр, при этом первый и второй знаки (цифры) не могут
                    одновременно принимать значение "00",
                </li>
                <li>
                    ИНН физического лица: 12 цифр, при этом первый и второй знаки (цифры) не могут одновременно
                    принимать значение "00",
                </li>
                <li>
                    Код иностранной организации: 5 цифр, при этом все знаки (цифры) не могут одновременно
                    принимать значение "0",
                </li>
                <li>
                    При отсутствии ИНН получателя: значение «0»,
                </li>
                <li>
                    В случае уплаты налогов укажите ИНН ИФНС, в которую вы подаете отчетность по налогу.
                </li>
            </ul>
            <br />
            Обязательно для заполнения при переводе в пользу ИП и юридического лица.
        </div>
    </CSSTransition>
);

export const Kpp = (
    <CSSTransition
        timeout={500}
        key="kpp"
        classNames="fadeappear"
        appear
        in
        exit
    >
        <div>
            <ul style={{ marginLeft: 15 }}>
                <li>
                    КПП получателя средств: 9 цифр, при этом первый и второй знаки (цифры) не могут одновременно
                    принимать значение "00",
                </li>
                <li>
                    При отсутствии КПП получателя: значение «0»,
                </li>
                <li>
                    В случае уплаты налогов укажите КПП ИФНС, в которую вы подаете отчетность по налогу.
                </li>
            </ul>
        </div>
    </CSSTransition>
);

export const AccountNumber = (
    <div />
);

export const Bik = (
    <CSSTransition
        timeout={500}
        key="bik"
        classNames="fadeappear"
        appear
        in
        exit
    >
        <div>
            БИК банка получателя. Введите первые 3 цифры для автоматического поиска.
        </div>
    </CSSTransition>
);

export const Description = (
    <CSSTransition
        timeout={500}
        key="description"
        classNames="fadeappear"
        appear
        in
        exit
    >
        <div>
            В назначении платежа необходимо указать: наименование товаров, работ, услуг, номера и даты договоров,
            товарных документов. Также может указываться другая необходимая информация, в том числе в соответствии с
            законодательством, включая налог на добавленную стоимость. Обязательно упомянуть НДС.
            Максимальная длина: 210 символов.
        </div>
    </CSSTransition>
);

export const ClientKpp = (
    <CSSTransition
        timeout={500}
        key="kppPayeer"
        classNames="fadeappear"
        appear
        in
        exit
    >
        <div>
            Указывается КПП плательщика (при наличии).
        </div>
    </CSSTransition>
);

export const Uin = (
    <CSSTransition
        timeout={500}
        key="uin"
        classNames="fadeappear"
        appear
        in
        exit
    >
        <div>
            Указывается уникальный идентификатор начисления (УИН) или
            "Индекс документа" (20 или 25 символов). Указывается в требовании на уплату недоимки, пени,
            штрафов(в том числе по требованию ИФНС). Для текущих платежей укажите "0".
        </div>
    </CSSTransition>
);

export const Status = (
    <CSSTransition
        timeout={500}
        key="status"
        classNames="fadeappear"
        appear
        in
        exit
    >
        <div>
            Для оплаты налогов выберите статус из списка
        </div>
    </CSSTransition>
);

export const Kbk = (
    <CSSTransition
        timeout={500}
        key="kbk"
        classNames="fadeappear"
        appear
        in
        exit
    >
        <div>
            20 цифр — указывается в соответствии с классификацией доходов бюджетов РФ.
            Если КБК отсутствует, проставляется "0".
        </div>
    </CSSTransition>
);

export const Payground = (
    <CSSTransition
        timeout={500}
        key="payground"
        classNames="fadeappear"
        appear
        in
        exit
    >
        <div>
            При оплате: - текущих налоговых платежей — «ТП», - недоимки, самостоятельно — «ЗД», - недоимки по
            требованию ИФНС — «ТР», - недоимки на основании акта проверки до выставления ИФНС требования — «АП».
            В случае указания значения ноль ("0") налоговые органы при невозможности однозначно идентифицировать платеж
            самостоятельно относят поступившие денежные средства к одному из оснований платежа, руководствуясь
            законодательством о налогах и сборах.
        </div>
    </CSSTransition>
);

export const NalogPeriod = (
    <CSSTransition
        timeout={500}
        key="nalogPeriod"
        classNames="fadeappear"
        appear
        in
        exit
    >
        <div style={{ width: 370 }}>
            Указывается периодичность. Если в поле «Основание платежа» (106) указано:
            <ul style={{ marginLeft: 15 }}>
                <li>
                    «ТР», укажите тут дату уплаты налога по требованию,
                </li>
                <li>
                    «АП» — «0»,
                </li>
                <li>
                    «ТП» или «ЗД» — налоговый период, за который уплачивается налог.
                </li>
            </ul>
            1-ый и 2-ый знаки: период уплаты налога: "МС"-месяц, "КВ"-квартал, "ПЛ"-полугодие, "ГД"-год. <br />
            3-ий знак: точка. <br />
            4-ый и 5-ый знаки: значение периода: 01-12 для "МС", 01-04 для "КВ", 01 или 02 для "ПЛ", 00 - для "ГД". <br />
            6-ой знак: точка. <br />
            С 7-го по 10-й знак: указывается год, за который производится уплата налога. <br />
            Например: "МС.02.2014", "КВ.01.2014", "ПЛ.02.2014", "ГД.00.2014".<br />
        </div>
    </CSSTransition>
);

export const TamozhOrgan = (
    <CSSTransition
        timeout={500}
        key="tamozhOrgan"
        classNames="fadeappear"
        appear
        in
        exit
    >
        <div>
            Восьмизначный код таможенного органа.
        </div>
    </CSSTransition>
);

export const NalogDocNumber = (
    <CSSTransition
        timeout={500}
        key="nalogDocNumber"
        classNames="fadeappear"
        appear
        in
        exit
    >
        <div>
            Заполняется информацией БЕЗ ЗНАКА N. Указывается "0", если поле "Основание налогового платежа" имеет значение:
            ТП, ЗД. Указывается показатель номера документа, если поле "Основание налогового платежа"
            имеет значение: ТР, РС, ОТ, РТ, ПР, ВУ, АП, АР.
        </div>
    </CSSTransition>
);

export const DocDate = (
    <CSSTransition
        timeout={500}
        key="docDate"
        classNames="fadeappear"
        appear
        in
        exit
    >
        <div>
            Указывается дата документа, на основании которого осуществляется платеж
        </div>
    </CSSTransition>
);

export const OKTMO = (
    <CSSTransition
        timeout={500}
        key="oktmo"
        classNames="fadeappear"
        appear
        in
        exit
    >
        <div>
            Указывайте код вашего муниципального образования(территории) Код ОКТМО можно узнать:
            <ul style={{ marginLeft: 15 }}>
                <li>
                    в отделении ПФР (ФСС) по адресу регистрации,
                </li>
                <li>
                    в свидетельстве о присвоении кодов статистики,
                </li>
                <li>
                    на сайте ФНС России, в сервисе «Узнай ОКТМО».
                </li>
            </ul>
            Длина может составлять 8. При оплате налога по месту нахождения:
            <ul style={{ marginLeft: 15 }}>
                <li>
                    организации — ОКТМО по месту нахождения организации,
                </li>
                <li>
                    обособленного подразделения — ОКТМО по месту нахождения этого подразделения
                    (Письмо ФНС от 12.03.2014 N БС¬4¬11/4431@),
                </li>
                <li>
                    недвижимого имущества — ОКТМО по месту нахождения этого имущества.
                </li>
            </ul>
            Если ОКТМО отсутствует, проставляется "0".
        </div>
    </CSSTransition>
);
