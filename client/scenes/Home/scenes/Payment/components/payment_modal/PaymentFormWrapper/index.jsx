/** Third party */
import React from 'react';
import PropTypes from 'prop-types';
/** Global */
import BlockLoader from 'components/Loader';
/** Relative */
import TopTabs from '../TopTabs';
import BtnRow from '../BtnRow';
import RightPanel from '../RightPanel';

import styles from './payment.css';

const PaymentFormWrapper = ({
    isVisible,
    activeTab,
    changeTemplateName,
    closeModal,
    deleteTemplate,
    children,
    contragentList,
    editable,
    handleCancelDocument,
    handleCopyClick,
    handlePDFPrint,
    handleSaveBtnClick,
    handleSaveTemplateClick,
    handleTemplateClick,
    saveAndSignPayment,
    savePayment,
    setActiveTab,
    signPaymentCallback,
    submitSelectContragent,
    isFetching,
    showFilledLoader,
    modalData,
    templates,
    smallForm,
    selectedTemplateId,
}) => (
    <div className={styles['payment-modal']} data-visible={isVisible}>
        <div className={styles.close} onClick={closeModal} />
        <BlockLoader
            visible={isFetching || showFilledLoader}
            style={{
                zIndex: 10000,
                backgroundColor: showFilledLoader
                    ? 'rgba(255, 255, 255, 1)'
                    : 'rgba(255, 255, 255, 0.7)',
            }}
        />
        <div
            style={{
                display: 'flex',
                flexDirection: 'column',
                flexGrow: 1,
            }}
        >
            <TopTabs
                activeTab={activeTab}
                setActiveTab={setActiveTab}
                editable={editable}
                smallForm={smallForm}
                closeModal={closeModal}
            />
            {children}
            <BtnRow
                savePayment={savePayment}
                saveAndSignPayment={saveAndSignPayment}
                signPaymentCallback={signPaymentCallback}
                handleSaveBtnClick={handleSaveBtnClick}
                handleSaveTemplateClick={handleSaveTemplateClick}
                handleCopyClick={handleCopyClick}
                handlePDFPrint={handlePDFPrint}
                handleCancelDocument={handleCancelDocument}
                closeModal={closeModal}
                modalData={modalData}
                smallForm={smallForm}
                activeTab={activeTab}
                selectedTemplateId={selectedTemplateId}
            />
        </div>
        <RightPanel
            templates={templates}
            contragents={contragentList}
            handleTemplateClick={handleTemplateClick}
            submitSelectContragent={submitSelectContragent}
            deleteTemplate={deleteTemplate}
            changeTemplateName={changeTemplateName}
            modalData={modalData}
        />
    </div>
);

PaymentFormWrapper.propTypes = {
    activeTab: PropTypes.string,
    changeTemplateName: PropTypes.func.isRequired,
    closeModal: PropTypes.func.isRequired,
    deleteTemplate: PropTypes.func.isRequired,
    children: PropTypes.array,
    contragentList: PropTypes.array,
    editable: PropTypes.bool,
    handleCancelDocument: PropTypes.func.isRequired,
    handleCopyClick: PropTypes.func.isRequired,
    handlePDFPrint: PropTypes.func.isRequired,
    handleSaveBtnClick: PropTypes.func.isRequired,
    handleSaveTemplateClick: PropTypes.func.isRequired,
    handleTemplateClick: PropTypes.func.isRequired,
    saveAndSignPayment: PropTypes.func.isRequired,
    savePayment: PropTypes.func.isRequired,
    setActiveTab: PropTypes.func.isRequired,
    signPaymentCallback: PropTypes.func.isRequired,
    submitSelectContragent: PropTypes.func.isRequired,
    isFetching: PropTypes.bool,
    showFilledLoader: PropTypes.bool,
    modalData: PropTypes.object,
    templates: PropTypes.array,
    selectedTemplateId: PropTypes.string,
};

PaymentFormWrapper.defaultProps = {
    activeTab: '',
    children: [],
    contragentList: [],
    editable: false,
    isFetching: false,
    showFilledLoader: false,
    modalData: null,
    templates: [],
    selectedTemplateId: null,
};

export default PaymentFormWrapper;
