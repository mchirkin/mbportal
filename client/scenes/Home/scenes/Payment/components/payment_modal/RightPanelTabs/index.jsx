import React, { Component } from 'react';
import styles from './right-panel-tabs.css';

export default class RightPanelTabs extends Component {
    handleTabClick = (e) => {
        const tab = e.target.getAttribute('data-tab');
        this.props.setActiveTab(tab);
    }

    render() {
        const activeTab = this.props.activeTab;
        return (
            <div
                className={styles['top-tabs-wrapper']}
            >
                <div className={styles['top-tabs-list-wrapper']}>
                    <div className={styles['top-tabs-list']}>
                        <div
                            className={styles['top-tab']}
                            data-active={activeTab === 'templates'}
                            data-tab="templates"
                            onClick={this.handleTabClick}
                        >
                            Шаблоны
                        </div>
                        <div
                            className={styles['top-tab']}
                            data-active={activeTab === 'contragents'}
                            data-tab="contragents"
                            onClick={this.handleTabClick}
                        >
                            Контакты
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
