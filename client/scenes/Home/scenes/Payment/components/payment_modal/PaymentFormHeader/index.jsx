import React, { Component } from 'react';
import SelectSendType from '../SelectSendType';
import styles from './payment-form-header.css';

export default class PaymentFormHeader extends Component {
    handleTabClick = (value) => {
        this.props.handlePaymentTypeChange(value);
    }

    render() {
        const toggleValues = this.props.values.activeTab === 'simple'
            ? [
                {
                    id: 'UL',
                    name: 'Внешний',
                },
                {
                    id: 'ACC2ACC',
                    name: 'Между своими счетами',
                },
            ]
            : [
                {
                    id: 'BUDZHET',
                    name: 'Налоговый',
                },
                {
                    id: 'TAMOZH',
                    name: 'Таможенный',
                },
            ];

        const urgenValues = [1, 2, 3, 4, 5];

        return (
            <div className={styles.wrapper}>
                <div className={styles['tab-list']}>
                    {toggleValues.map(value => (
                        <div
                            className={styles.tab}
                            onClick={() => { this.handleTabClick(value); }}
                            data-active={this.props.values.paymentType === value.id}
                        >
                            {value.name}
                        </div>
                    ))}
                </div>
                <div className={styles['params-list']}>
                    {this.props.values.paymentType !== 'ACC2ACC'
                        ? (
                            <SelectSendType
                                sendType={this.props.sendType}
                                selectSendType={this.props.selectSendType}
                                selectedSendType={this.props.values.selectedSendType}
                                disabled={!this.props.editable}
                                hintContainer={this.props.hintContainer}
                            />
                        )
                        : null}
                    {this.props.values.paymentType !== 'ACC2ACC'
                        ? (
                            <div
                                className={styles['urgen-wrapper']}
                                data-hidden={this.props.values.paymentType === 'ACC2ACC'}
                                data-disabled={!this.props.editable}
                            >
                                <div style={{ display: 'flex', alignItems: 'center' }}>
                                    {/*
                                    <Hint text={Hints.Urgen} style={{ marginRight: 5 }} />
                                    */}
                                    <span
                                        className={styles['urgen-btn']}
                                        onClick={this.props.toggleUrgenSelectVisibility}
                                    >
                                        Очередность платежа: <span>{this.props.values.urgen}</span>
                                    </span>
                                </div>
                                <div className={styles['urgen-list']} data-visible={this.props.values.showSelectUrgen}>
                                    {urgenValues.map(value => (
                                        <div
                                            className={styles['urgen-item']}
                                            onClick={() => { this.props.handleUrgenChange(value); }}
                                            data-active={parseInt(this.props.values.urgen, 10) === value}
                                        >
                                            <span>
                                                {value}
                                            </span>
                                        </div>
                                    ))}
                                </div>
                            </div>
                        )
                        : null}
                    <div
                        className={styles['input-hint']}
                        onClick={this.props.toggleHints}
                        data-disabled={this.props.values.hintsDisabled}
                        title={this.props.values.hintsDisabled ? 'Включить подсказки' : 'Выключить подсказки'}
                    >
                        <div className={styles['input-hint-btn']} />
                    </div>
                </div>
            </div>
        );
    }
}
