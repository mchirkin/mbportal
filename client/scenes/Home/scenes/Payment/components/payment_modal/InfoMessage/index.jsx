import React from 'react';
import Btn from 'components/ui/Btn';
import styles from './info-message.css';

const InfoMessage = ({
    isVisible = false,
    header,
    body,
    callback,
}) => (
    <div className={styles.wrapper} data-visible={isVisible}>
        <div className={styles.header}>
            {header}
        </div>
        <div className={styles.body}>
            {body}
        </div>
        <Btn
            caption="Далее"
            bgColor="grey"
            onClick={callback}
            style={{
                width: 200,
                marginTop: 50,
            }}
        />
    </div>
);

export default InfoMessage;
