import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Scrollbars } from 'react-custom-scrollbars';

import Btn from 'components/ui/Btn';
import Marquee from 'components/ui/Marquee';

import RightPanelTabs from '../RightPanelTabs';
import TemplateItem from './components/TemplateItem';

import styles from './right-panel.css';

export default class RightPanel extends Component {
    static propTypes = {
        changeTemplateName: PropTypes.func.isRequired,
        deleteTemplate: PropTypes.func.isRequired,
        handleTemplateClick: PropTypes.func.isRequired,
        submitSelectContragent: PropTypes.func.isRequired,
        contragents: PropTypes.array,
        templates: PropTypes.array,
        modalData: PropTypes.object,
    }

    static defaultProps = {
        contragents: [],
        templates: [],
        modalData: null,
    }

    constructor(props) {
        super(props);

        this.state = {
            activeTab: 'templates',
            search: '',
            editableTemplateIndex: null,
            editableTemplateName: '',
            templateError: '',
        };
    }

    setActiveTab = (tab) => {
        this.setState({
            activeTab: tab,
            search: '',
        }, () => {
            this.forceUpdate();
        });
    }

    handleSearchChange = (e) => {
        this.setState({
            search: e.target.value,
        });
    }

    render() {
        let content;

        if (this.state.activeTab === 'templates') {
            content = (
                <div className={styles['template-list']}>
                    {this.props.templates
                        ? this.props.templates
                            .filter(template => (this.state.search
                                ? template.template.name.toLowerCase().includes(this.state.search.toLowerCase())
                                : true),
                            )
                            .map((data, index) => (
                                <TemplateItem
                                    key={data.template.id || index}
                                    deleteTemplate={this.props.deleteTemplate}
                                    changeTemplateName={this.props.changeTemplateName}
                                    template={data.template}
                                    handleTemplateClick={this.props.handleTemplateClick}
                                />
                            ))
                        : null}
                </div>
            );
        }

        if (this.state.activeTab === 'contragents') {
            content = (
                <div className={styles['template-list']}>
                    {this.props.contragents
                        ? this.props.contragents
                            .filter(agent => (this.state.search
                                ? agent.fullname.toLowerCase().includes(this.state.search.toLowerCase())
                                : true),
                            )
                            .map(agent => (
                                <div
                                    className={styles['template-item']}
                                    onClick={() => { this.props.submitSelectContragent(agent); }}
                                >
                                    <Marquee text={agent.fullname} />
                                </div>
                            ))
                        : null}
                </div>
            );
        }

        return (
            <div className={styles.wrapper}>
                {(
                    this.props.modalData &&
                    this.props.modalData.declineInfo &&
                    this.props.modalData.status === 'decline'
                )
                    ? (
                        <div className={styles['decline-info']}>
                            <div className={styles['decline-info-header']}>
                                Причина отказа
                            </div>
                            <div className={styles['decline-info-text']}>
                                {this.props.modalData && this.props.modalData.declineInfo}
                            </div>
                        </div>
                    )
                    : null}
                <RightPanelTabs
                    {...this.state}
                    setActiveTab={this.setActiveTab}
                />
                <div className={styles['content-wrapper']}>
                    <Scrollbars
                        style={{
                            width: '100%',
                            height: '100%',
                        }}
                    >
                        {content}
                    </Scrollbars>
                </div>
                <div className={styles.search}>
                    <div className={styles['input-wrapper']}>
                        <div className={styles['search-icon']} />
                        <input
                            className={styles['search-input']}
                            value={this.state.search}
                            onChange={this.handleSearchChange}
                            placeholder="Поиск"
                        />
                    </div>
                </div>
            </div>
        );
    }
}
