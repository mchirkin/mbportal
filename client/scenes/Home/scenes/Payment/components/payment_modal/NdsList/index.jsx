import React, { Component } from 'react';
import styles from './payment.css';

export default function NdsList(props) {
    if (!props.selectedNds && props.disabled) {
        return null;
    }

    return (
        <div className={styles['nds-list']}>
            {
                props.ndsList
                    ? props.ndsList
                        .sort((a, b) => {
                            if (a.ndsNo && !b.ndsNo) {
                                return -1;
                            }
                            return parseInt(b.id) - parseInt(a.id);
                        })
                        .filter(nds => !nds.ndsNo)
                        .map((nds) => {
                            let caption = nds.caption;
                            switch (nds.ndsRate) {
                                case '0':
                                    caption = 'Без НДС';
                                    break;
                                case '10':
                                    caption = 'НДС 10%';
                                    break;
                                case '18':
                                    caption = 'НДС 18%';
                                    break;
                            }
                            return (
                                <div
                                    key={nds.id}
                                    className={styles.nds}
                                    onClick={() => { props.handleNdsChange(nds); }}
                                    data-active={props.selectedNds && props.selectedNds.id === nds.id}
                                >
                                    {caption}
                                </div>
                            );
                        })
                    : null
            }
        </div>
    );
}
