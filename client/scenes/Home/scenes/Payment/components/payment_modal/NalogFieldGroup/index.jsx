/** Third-party */
import React, { Component } from 'react';
import { render, unmountComponentAtNode } from 'react-dom';
import { DayPickerSingleDateController } from 'react-dates';
import moment from 'moment';
/** utils */
import colors from 'constants/colors';
/** Global */
import InputGroup from 'components/ui/InputGroup';
import InputSelect from 'components/ui/InputSelect';
//import Datepicker from 'components/ui/Datepicker';
import Hint from 'components/ui/Hint';
/** Relative */
import NalogPeriod from '../NalogPeriod';
import * as Hints from '../Hints';
/** Styles */
import styles from './payment.css';

export default class NalogFieldGroup extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showCalendar: false,
            date: null
        };
    }

    setNalogDate = (date) => {
        this.setState({
            date: date
        })
    }

    showHint = (el, hint) => {
        if (hint && this.props.hintContainer && !this.props.hintDisabled) {
            const offsetTop = el.offsetTop;

            this.previousHintContainerHTML = this.props.hintContainer.innerHTML;
            this.previousHintContainerPadding = this.props.hintContainer.style.paddingTop;

            this.props.hintContainer.innerHTML = '';
            const hintContainerHeight = this.props.hintContainer.clientHeight;
            this.props.hintContainer.style.paddingTop = `${offsetTop - 45}px`;

            render(hint, this.props.hintContainer);

            if (this.props.hintContainer.clientHeight > hintContainerHeight) {
                const heightDiff = this.props.hintContainer.clientHeight - hintContainerHeight;
                this.props.hintContainer.style.paddingTop = `${offsetTop - 45 - heightDiff}px`;
            }
        }
    }

    deleteHint = () => {
        unmountComponentAtNode(this.props.hintContainer);
        this.props.hintContainer.innerHTML = this.previousHintContainerHTML;
        this.props.hintContainer.style.paddingTop = this.previousHintContainerPadding;
    }

    showCalendar = (e) => {
        this.setState({
            showCalendar: true,
        });
    }

    handleNalogDateChange = (date) => {
        this.props.handleNalogDateChange(date);
        this.setState({
            showCalendar: false,
        });
    }

    render() {
        const pStatus = this.props.pStatus.map(status => ({
            status,
            value: `${status.code} - ${status.caption}`,
        }));

        const paymentsPayground = this.props.paymentsPayground.map(payground => ({
            payground,
            value: `${payground.code} - ${payground.caption}`,
        }));

        return (
            <div style={{ display: 'flex', flexWrap: 'wrap', marginTop: 20 }}>
                <div className={styles['input-row']}>
                    <InputSelect
                        id="stat"
                        caption="(101) - Статус налогоплательщика"
                        options={pStatus}
                        style={{
                            flexGrow: 1,
                        }}
                        onChange={this.props.handleSelectChange}
                        value={this.props.values.stat}
                        disabled={this.props.disabled}
                        hint={Hints.Status}
                        hintContainer={this.props.hintContainer}
                        hintDisabled={this.props.values.hintsDisabled}
                        restoreHintContainer
                        invalid={this.props.checkIfInvalid('stat')}
                    />
                </div>
                <div className={styles['input-row']}>
                    <InputGroup
                        id="uin"
                        caption="(22) - Код (УИН)"
                        mask="rrrrrrrrrrrrrrrrrrrrrrrrr"
                        maskChar={null}
                        style={{
                            flexGrow: 1,
                        }}
                        onChange={this.props.handleInputChange}
                        value={this.props.values.uin}
                        disabled={this.props.disabled}
                        hint={Hints.Uin}
                        hintContainer={this.props.hintContainer}
                        hintDisabled={this.props.values.hintsDisabled}
                        restoreHintContainer
                        invalid={this.props.checkIfInvalid('uin')}
                    />
                </div>
                <div className={styles['input-row']}>
                    <InputGroup
                        id="kbk"
                        caption="(104) - КБК"
                        mask="99999999999999999999"
                        maskChar={null}
                        style={{
                            flexGrow: 1,
                            width: 200,
                        }}
                        onChange={this.props.handleInputChange}
                        value={this.props.values.kbk}
                        disabled={this.props.disabled}
                        hint={Hints.Kbk}
                        hintContainer={this.props.hintContainer}
                        hintDisabled={this.props.values.hintsDisabled}
                        restoreHintContainer
                        invalid={this.props.checkIfInvalid('kbk')}
                    />
                    <InputGroup
                        id="okato"
                        caption="(105) - ОКТМО"
                        mask="99999999999"
                        maskChar={null}
                        style={{
                            flexGrow: 1,
                            width: 145,
                        }}
                        onChange={this.props.handleInputChange}
                        value={this.props.values.okato}
                        disabled={this.props.disabled}
                        hint={Hints.OKTMO}
                        hintContainer={this.props.hintContainer}
                        hintDisabled={this.props.values.hintsDisabled}
                        restoreHintContainer
                        invalid={this.props.checkIfInvalid('oktmo')}
                    />
                </div>
                <div className={styles['input-row']}>
                    <InputSelect
                        id="ground"
                        caption="(106) - Осн. платежа"
                        options={paymentsPayground}
                        style={{
                            flexGrow: 1,
                            width: 255,
                        }}
                        onChange={this.props.handleSelectChange}
                        value={this.props.values.ground}
                        disabled={this.props.disabled}
                        hint={Hints.Payground}
                        hintContainer={this.props.hintContainer}
                        hintDisabled={this.props.values.hintsDisabled}
                        restoreHintContainer
                        invalid={this.props.checkIfInvalid('ground')}
                    />
                </div>
                <div className={styles['input-row']}>
                    {
                        this.props.paymentType === 'BUDZHET'
                            ? (
                                <NalogPeriod
                                    paymentsTaxperiod={this.props.paymentsTaxperiod}
                                    handleInputChange={this.props.handleInputChange}
                                    handleSelectChange={this.props.handleSelectChange}
                                    values={this.props.values}
                                    disabled={this.props.disabled}
                                    showHint={this.showHint}
                                    deleteHint={this.deleteHint}
                                    invalid={[
                                        this.props.checkIfInvalid('tax1'),
                                        this.props.checkIfInvalid('tax2'),
                                        this.props.checkIfInvalid('tax3'),
                                    ]}
                                />
                            )
                            : null
                    }
                    {
                        this.props.paymentType === 'TAMOZH'
                            ? (
                                <InputGroup
                                    id="tax1"
                                    caption="(107) - Таможенный орган"
                                    style={{
                                        // flexGrow: 1,
                                        width: 280,
                                    }}
                                    onChange={this.props.handleInputChange}
                                    value={this.props.tax1}
                                    disabled={this.props.disabled}
                                    hint={Hints.TamozhOrgan}
                                    hintContainer={this.props.hintContainer}
                                    hintDisabled={this.props.values.hintsDisabled}
                                    restoreHintContainer
                                    invalid={this.props.checkIfInvalid('tax1')}
                                />
                            )
                            : null
                    }
                    <InputGroup
                        id="taxdocnum"
                        caption="(108) - Номер налог. док-та"
                        style={{
                            flexGrow: 1,
                            width: 167,
                        }}
                        onChange={this.props.handleInputChange}
                        value={this.props.values.taxdocnum}
                        disabled={this.props.disabled}
                        hint={Hints.NalogDocNumber}
                        hintContainer={this.props.hintContainer}
                        hintDisabled={this.props.values.hintsDisabled}
                        restoreHintContainer
                        invalid={this.props.checkIfInvalid('taxdocnum')}
                    />
                </div>
                <div className={styles['input-row']}>
                    <InputGroup
                        id="taxdocdate"
                        caption="(109) - Дата налог. док-та"
                        style={{
                            // flexGrow: 1,
                            width: 280,
                        }}
                        onChange={this.props.handleInputChange}
                        value={this.props.values.taxdocdate}
                        disabled={this.props.disabled}
                        mask="99.99.9999"
                        hint={<div />}
                        hintContainer={this.props.hintContainer}
                        restoreHintContainer
                        invalid={this.props.checkIfInvalid('taxdocdate')}
                    />
                </div>
                {/*
                    <div
                        className={styles['datepicker-group']}
                        style={{
                            // flexGrow: 1,
                            width: 280,
                            backgroundColor: this.props.disabled ? 'rgba(232, 232, 232, .5)' : undefined,
                            cursor: this.props.disabled ? 'default' : 'pointer',
                            border: this.props.checkIfInvalid('taxdocdate')
                                ? `1px solid ${colors.ERROR_RED}`
                                : undefined,
                        }}
                        onClick={!this.props.modalDataCheckStatus ? this.showCalendar : undefined}
                    >
                        <div className={styles['datepicker-top']}>
                            <label
                                style={{
                                    color: this.props.checkIfInvalid('taxdocdate')
                                        ? colors.ERROR_RED
                                        : undefined,
                                }}
                            >
                                (109) - Дата налог. док-та
                            </label>
                        </div>
                        <div style={{fontSize: 15}}>
                            {
                                this.state.date &&
                                (this.props.modalDataCheckStatus || this.props.modalDataCheckStatus === null)
                                    ? this.state.date
                                    : this.props.taxDocDate
                            }
                        </div>
                        {this.state.showCalendar && (
                            <div className={styles.period__daypicker}>
                                <DayPickerSingleDateController
                                    onDateChange={this.handleNalogDateChange}
                                    onOutsideClick={() => this.setState({ showCalendar: false })}
                                    firstDayOfWeek={1}
                                    numberOfMonths={1}
                                    daySize={35}
                                    hideKeyboardShortcutsPanel
                                />
                            </div>
                        )}
                    </div>
                </div>
                */}

                {/*
                <div className={styles['input-row']}>
                    <div
                        className={styles['datepicker-group']}
                        style={{
                            // flexGrow: 1,
                            width: 280,
                            backgroundColor: this.props.disabled ? 'rgba(232, 232, 232, .5)' : undefined,
                        }}
                    >
                        <div className={styles['datepicker-top']}>
                            <label>(109) - Дата налог. док-та</label>
                        </div>

                        <Datepicker
                            styles={{
                                width: '100%',
                            }}
                            inputStyles={{
                                width: '100%',
                                border: 0,
                            }}
                            calendarStyles={{
                                top: -260,
                                left: 0,
                            }}
                            onChange={this.props.handleNalogDateChange}
                            zeroDate
                            ref={(nalogDate) => { this.nalogDate = nalogDate; }}
                            disabled={this.props.disabled}
                        />
                    </div>
                </div>
                */}
            </div>
        );
    }
}
