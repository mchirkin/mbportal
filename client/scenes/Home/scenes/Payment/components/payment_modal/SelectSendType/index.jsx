import React, { Component } from 'react';
import { findTargetInChildNode } from 'utils/dom';
import styles from './payment.css';

export default class SelectSendType extends Component {
    constructor(props) {
        super(props);

        this.state = {
            dropdownVisible: false,
        };
    }

    componentWillMount() {
        document.addEventListener('click', this.handleDocumentClick);
    }

    componentWillUnmount() {
        document.removeEventListener('click', this.handleDocumentClick);
    }

    toggleDropdownVisibility = () => {
        if (this.props.disabled) {
            return;
        }
        this.setState({
            dropdownVisible: !this.state.dropdownVisible,
        });
    }

    handleSendTypeClick = (type) => {
        this.props.selectSendType(type, this.props.hintContainer);
        this.toggleDropdownVisibility();
    }

    handleDocumentClick = (e) => {
        if (!findTargetInChildNode(this.wrapper, e.target)) {
            this.setState({
                dropdownVisible: false,
            });
        }
    }

    render() {
        const sendTypeList = this.props.sendType && this.props.sendType.length > 0
            ? this.props.sendType.filter((type) => {
                const caption = type.caption.toLowerCase();
                return caption === 'электронно' || caption === 'срочно';
            }).map((type) => {
                let caption = type.caption;

                switch (type.caption) {
                    case 'электронно':
                        caption = 'Обычный платеж';
                        break;
                    case 'срочно':
                        caption = 'Срочный платеж';
                        break;
                }

                return (
                    <li
                        key={type.code}
                        onClick={() => { this.handleSendTypeClick(type); }}
                        data-active={this.props.selectedSendType && this.props.selectedSendType.code === type.code}
                    >
                        {caption}
                    </li>
                );
            })
            : null;

        let sendtypeCaption = this.props.selectedSendType ? this.props.selectedSendType.caption : '';

        if (sendtypeCaption) {
            switch (sendtypeCaption) {
                case 'электронно':
                    sendtypeCaption = 'Обычный платеж';
                    break;
                case 'срочно':
                    sendtypeCaption = 'Срочный платеж';
                    break;
            }
        }

        if (
            this.props.disabled && sendtypeCaption.length === 0 ||
            sendtypeCaption === ' (электронно)'
        ) {
            sendtypeCaption = ' (обычная)';
        }


        return (
            <div
                className={styles['select-send-type']}
                ref={(el) => { this.wrapper = el; }}
                data-disabled={this.props.disabled}
            >
                <span className={styles['select-send-type-btn']} onClick={this.toggleDropdownVisibility}>
                    {sendtypeCaption
                        ? (
                            <span className={styles['selected-send-type']}>
                                {sendtypeCaption}
                                <div className={styles.triangle} />
                            </span>
                        )
                        : 'Срочность платежа'}
                </span>
                <ul className={styles['send-type-list']} data-visible={this.state.dropdownVisible}>
                    {sendTypeList}
                </ul>
            </div>
        );
    }
}
