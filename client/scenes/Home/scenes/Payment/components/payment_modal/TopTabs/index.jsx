import React, { Component } from 'react';
import styles from './payment.css';

export default class TopTabs extends Component {
    handleTabClick = (e) => {
        const tab = e.target.getAttribute('data-tab');
        this.props.setActiveTab(tab);
    }

    render() {
        const activeTab = this.props.activeTab;
        return (
            <div
                className={styles['top-tabs-wrapper']}
                data-editable={this.props.editable}
            >
                <div className={styles['top-tabs-list-wrapper']}>
                    <div className={styles['top-tabs-header']}>
                        Новый платеж
                    </div>
                    <div className={styles['top-tabs-list']}>
                        <div
                            className={styles['top-tab']}
                            data-active={activeTab === 'simple'}
                            data-tab="simple"
                            onClick={this.handleTabClick}
                        >
                            Простой платеж
                        </div>
                        <div
                            className={styles['top-tab']}
                            data-active={activeTab === 'nalog'}
                            data-tab="nalog"
                            onClick={this.handleTabClick}
                        >
                            Налоговый платеж
                        </div>
                        <div
                            className={styles['top-tab']}
                            data-active={activeTab === '1c'}
                            data-tab="1c"
                            onClick={this.handleTabClick}
                        >
                            Импорт из 1С
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
