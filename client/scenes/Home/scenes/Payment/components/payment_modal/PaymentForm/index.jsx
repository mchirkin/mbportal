/** Third-party */
import React, { Component } from 'react';
import { Scrollbars } from 'react-custom-scrollbars';
import Dropzone from 'react-dropzone';
import { DayPickerSingleDateController } from 'react-dates';
import moment from 'moment';
/** Utils */
import numberToPhrase from 'utils/number_to_phrase';
import fieldMapper from 'utils/payment/field_mapper';
/** Global */
import InputGroup from 'components/ui/InputGroup';
import InputSelect from 'components/ui/InputSelect';
import InputTextarea from 'components/ui/InputTextarea';
import InputAmount from 'components/ui/InputAmount';
//import Datepicker from 'components/ui/Datepicker';

import ModalBtnRow from 'components/ModalBtnRow';
import ModalBtn from 'components/ModalBtn';
import Btn from 'components/ui/Button';
import Toggle from 'components/ui/Toggle';
import Hint from 'components/ui/Hint';
/** Relative */
import NdsList from '../NdsList';
import NalogFieldGroup from '../NalogFieldGroup';
import SelectSendType from '../SelectSendType';
import * as Hints from '../Hints';
import PaymentFormHeader from '../PaymentFormHeader';
/** Styles */
import styles from './payment.css';

export default class PaymentForm extends Component {

    constructor(props) {
        super(props);

        this.state = {
            showCalendar: false,
            date: null
        };
    }

    setNalogDate = (date) => {
        setTimeout(() => {
            if (this.nalogFieldGroup) {
                this.nalogFieldGroup.setNalogDate(date);
            }
        }, 0);
    }

    setDocumentDate = (date) => {
        this.setState({
            date,
        });
    }

    setDocNumberWidth = (value) => {
        return;
        if (value.length > 2) {
            this.docNumber.style.width = `${(value.length) * 10 + 30}px`;
        } else {
            this.docNumber.style.width = '42px';
        }
    }

    handleKonturMouseEnter = () => {
        this.props.handleKonturMouseEnter(this.hintContainer);
    }

    handleKonturMouseLeave = () => {
        this.props.handleKonturMouseLeave(this.hintContainer);
    }

    showCalendar = (e) => {
        this.setState({
            showCalendar: true,
        });
    }

    setRangeDate = (date) => {
        return (
            date > moment().add(30, 'days') ||
            date < moment().subtract(7, 'days')
        );
    }

    handleDocDateChange = (date) => {
        this.props.handleDocDateChange(date);
        this.setState({
            showCalendar: false,
        });
    }

    checkIfInvalid = (field) => {
        const errors = this.props.values.invalidFields.filter(f => f.field.includes(fieldMapper(field)));
        return errors && errors.length > 0 ? errors.map(err => err.errorText).join('\n') : null;
    }

    render() {
        // Код валютной оперции
        let showValuteCode = false;

        if (this.props.values.receiverAccount) {
            const valuteAccList = ['30111', '30231', '40807', '40820'];
            valuteAccList.forEach((el) => {
                if (this.props.values.receiverAccount.substr(0, 5) === el) {
                    showValuteCode = true;
                }
            });
        }

        let codevoPaymentsValues = [];
        if (this.props.codevoPayments && showValuteCode) {
            codevoPaymentsValues = this.props.codevoPayments.map(el => ({
                el,
                value: `${el.code} - ${el.description}`,
            }));
        }

        const toggleValues = this.props.values.activeTab === 'simple'
            ? [
                {
                    id: 'UL',
                    name: 'Третьему лицу',
                },
                {
                    id: 'ACC2ACC',
                    name: 'Себе',
                },
            ]
            : [
                {
                    id: 'BUDZHET',
                    name: 'Налоговый',
                },
                {
                    id: 'TAMOZH',
                    name: 'Таможенный',
                },
            ];

        // счета с возможностью платежей
        const accountsForPayments = this.props.values.accountList.filter(account => account.acc.type !== '7');

        // счета с возможность перевода как источник
        const accountsFromList = this.props.canTransferFromSKS
            ? this.props.values.accountList
            : this.props.values.accountList.filter(account => account.acc.type !== '7');
        // счета с возможность перевода как получатель
        const accountsToList = this.props.values.accountList.filter(account => (
            this.props.values.account && this.props.values.account.acc
                ? account.acc.id !== this.props.values.account.acc.id
                : true
        ));

        const acc2accDisabled = accountsFromList.length === 0 || accountsToList.length === 0;

        const urgenValues = [1, 2, 3, 4, 5];

        const isDebet = (
            this.props.modalData &&
            (this.props.modalData.lineIsDebet === '0' || this.props.modalData.lineIsDebet === 'true')
        );

        const modalDataCheckStatus = (
            this.props.modalData && this.props.modalData.status !== 'new' && this.props.modalData.status !== 'imported'
        );

        let factsColor = null;
        if (this.props.values.konturData !== null) {
            factsColor = 'grey';
            if (this.props.values.konturData && this.props.values.konturData.ActivityFacts.facts.length > 0) {
                factsColor = 'green';
            }
            if (this.props.values.konturData && this.props.values.konturData.PayAttentionFacts.facts.length > 0) {
                factsColor = 'yellow';
            }
            if (this.props.values.konturData && this.props.values.konturData.CriticalFacts.facts.length > 0) {
                factsColor = 'red';
            }
            if (!factsColor) {
                factsColor = 'grey';
            }
        }

        if (this.props.values.activeTab === '1c') {
            const dropZoneStyle = {
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                width: '100%',
                height: 250,
                fontSize: 14,
                textAlign: 'center',
                border: '2px dashed #ccc',
                borderRadius: 10,
                cursor: 'pointer',
            };

            return (
                <div className={styles['form-wrapper']}>
                    <Scrollbars>
                        <div className={styles.form} style={{ paddingRight: 25 }}>
                            <Dropzone
                                style={dropZoneStyle}
                                onDrop={this.props.handle1CFileSelect}
                            >
                                <span>
                                    <strong className={styles['select-file']}>Выберите файл</strong>
                                    или перетащите в эту область
                                </span>
                            </Dropzone>
                        </div>
                    </Scrollbars>
                </div>
            );
        }

        return (
            <div className={styles['form-wrapper']}>
                <Scrollbars
                    ref={(el) => { this.formScrollbars = el; }}
                >

                    <div className={styles.form}>

                        <PaymentFormHeader
                            {...this.props}
                            onChange={this.props.handlePaymentTypeChange}
                            disabled={(
                                this.props.values.activeTab === 'simple' &&
                            this.props.values.accountList &&
                            acc2accDisabled
                            )}
                            hidden={!this.props.editable}
                            hintContainer={this.hintContainer}
                        />

                        <div className={styles['main-form']}>
                            <div>
                                <div className={styles['flex-wrap']}>
                                    <div className={styles['form-block-name']}>
                                    Информация о платеже
                                    </div>
                                    <div className={styles['input-row']}>
                                        <div style={{ display: 'flex' }}>
                                            <InputGroup
                                                id="docNumber"
                                                caption="№ платежа"
                                                style={{
                                                    width: 150,
                                                }}
                                                onChange={this.props.handleInputChange}
                                                value={this.props.values.docNumber}
                                                maxLength={6}
                                                disabled={!this.props.editable}
                                                hint={Hints.DocNumber}
                                                hintContainer={this.hintContainer}
                                                hintDisabled={this.props.values.hintsDisabled}
                                                restoreHintContainer
                                                invalid={this.checkIfInvalid('docNumber')}
                                            />
                                            <div
                                                className={modalDataCheckStatus
                                                    ? `${styles['doc-date-input']} ${styles['doc-date-input-disabled']}`
                                                    : styles['doc-date-input']
                                                }
                                                onClick={!modalDataCheckStatus ? this.showCalendar : undefined}
                                            >
                                                <div className={styles['doc-date-label']}>
                                                    Дата
                                                </div>
                                                <div className={styles['doc-date-text']}>
                                                    <div>
                                                        {this.state.date && modalDataCheckStatus
                                                            ? this.state.date
                                                            : this.props.docDate}
                                                    </div>
                                                </div>
                                                {this.state.showCalendar &&
                                                    <div className={styles.period__daypicker}>
                                                        <DayPickerSingleDateController
                                                            onDateChange={this.handleDocDateChange}
                                                            onOutsideClick={() => this.setState({ showCalendar: false })}
                                                            isOutsideRange={this.setRangeDate}
                                                            firstDayOfWeek={1}
                                                            numberOfMonths={1}
                                                            daySize={35}
                                                            hideKeyboardShortcutsPanel
                                                        />
                                                    </div>}
                                            </div>
                                        </div>
                                        <div
                                            style={{ display: 'flex' }}
                                            data-hidden={this.props.values.paymentType === 'ACC2ACC'}
                                        >
                                            {
                                                this.props.values.paymentType !== 'ACC2ACC' &&
                                                this.props.values.konturData !== null &&
                                                this.props.konturFocusSubscr
                                                    ? (
                                                        <div className={styles['kontur-data']}>
                                                            <div
                                                                className={styles['contragent-kontur-wrapper']}
                                                                onClick={this.props.handleKonturClick}
                                                                onMouseEnter={this.handleKonturMouseEnter}
                                                                onMouseLeave={this.handleKonturMouseLeave}
                                                            >
                                                                <div
                                                                    className={styles['contragent-kontur']}
                                                                    data-facts={factsColor}
                                                                />
                                                            </div>
                                                        </div>
                                                    )
                                                    : null
                                            }
                                            <div
                                                className={styles['add-contragent-btn-wrapper']}
                                                onClick={this.props.saveContragent}
                                                title="Добавить контакт в справочник"
                                            >
                                                <div className={styles['add-contragent-btn-plus']} />
                                            </div>
                                        </div>
                                    </div>

                                    <div className={styles['input-row']} data-hidden={this.props.values.paymentType === 'ACC2ACC'}>
                                        <InputGroup
                                            id="receiverName"
                                            caption={
                                                isDebet
                                                    ? 'Контрагент'
                                                    : 'Наименование получателя или ИНН'
                                            }
                                            style={{
                                                width: '100%',
                                            }}
                                            onChange={this.props.handleInputChange}
                                            value={this.props.values.receiverName}
                                            disabled={!this.props.editable}
                                            maxLength={160}
                                            autocomplete={this.props.values.contragentList}
                                            handleAutocompleteSelect={this.props.handleContragentSelect}
                                            showSymbolCounter
                                            hint={Hints.ReceiverName}
                                            hintContainer={this.hintContainer}
                                            hintDisabled={this.props.values.hintsDisabled}
                                            restoreHintContainer
                                            noAutoSelectAfterBlur
                                            invalid={this.checkIfInvalid('receiverName')}
                                        />
                                    </div>
                                    <div className={styles['input-row']} data-hidden={this.props.values.paymentType === 'ACC2ACC'}>
                                        <InputGroup
                                            id="receiverInn"
                                            caption="ИНН"
                                            style={{
                                                width: 225,
                                                flexGrow: 1,
                                            }}
                                            mask={'999999999999'}
                                            maskChar={null}
                                            onChange={this.props.handleInputChange}
                                            value={this.props.values.receiverInn}
                                            disabled={!this.props.editable}
                                            hint={Hints.Inn}
                                            hintContainer={this.hintContainer}
                                            hintDisabled={this.props.values.hintsDisabled}
                                            restoreHintContainer
                                            invalid={this.checkIfInvalid('receiverInn')}
                                        />
                                        <InputGroup
                                            id="receiverKpp"
                                            caption="КПП"
                                            style={{
                                                width: 200,
                                                flexGrow: 1,
                                            }}
                                            mask={'999999999'}
                                            maskChar={null}
                                            onChange={this.props.handleInputChange}
                                            value={this.props.values.receiverKpp}
                                            disabled={!this.props.editable}
                                            hint={Hints.Kpp}
                                            hintContainer={this.hintContainer}
                                            hintDisabled={this.props.values.hintsDisabled}
                                            restoreHintContainer
                                            invalid={this.checkIfInvalid('receiverKpp')}
                                        />
                                        <InputGroup
                                            id="receiverAccount"
                                            caption="Счет"
                                            style={{
                                                width: 380,
                                                flexGrow: 1,
                                            }}
                                            mask={'99999.999.9.99999999999'}
                                            maskChar={'_'}
                                            onChange={this.props.handleInputChange}
                                            value={this.props.values.receiverAccount}
                                            disabled={!this.props.editable}
                                            hint={Hints.AccountNumber}
                                            hintContainer={this.hintContainer}
                                            hintDisabled={this.props.values.hintsDisabled}
                                            restoreHintContainer
                                            invalid={this.checkIfInvalid('receiverAccount')}
                                        />
                                    </div>
                                    <div className={styles['input-row']} style={{ marginBottom: 5 }} data-hidden={this.props.values.paymentType === 'ACC2ACC'}>
                                        <InputGroup
                                            id="bankOfReceiver"
                                            caption="БИК или название Банка"
                                            style={{
                                                width: '100%',
                                            }}
                                            value={this.props.values.bankOfReceiver}
                                            onChange={this.props.handleInputChange}
                                            autocomplete={this.props.values.bankList}
                                            handleAutocompleteSelect={this.props.handleBankSelect}
                                            disabled={!this.props.editable}
                                            hint={Hints.Bik}
                                            hintContainer={this.hintContainer}
                                            hintDisabled={this.props.values.hintsDisabled}
                                            restoreHintContainer
                                            invalid={this.checkIfInvalid('bankOfReceiver')}
                                        />
                                    </div>
                                    <div
                                        style={{
                                            display: 'flex',
                                            width: '100%',
                                            alignItems: 'flex-start',
                                            justifyContent: 'space-between',
                                        }}
                                        data-hidden={this.props.values.paymentType === 'ACC2ACC'}
                                    >
                                        <div
                                            style={{
                                                fontSize: 14,
                                            }}
                                        >
                                            {this.props.values.selectedBank
                                                ? (
                                                    <span>
                                                        {`БИК ${this.props.values.selectedBank.bik}`}
                                                        {
                                                            this.props.values.selectedBank.place
                                                                ? `, ${this.props.values.selectedBank.place}`
                                                                : ''
                                                        }
                                                        {
                                                            this.props.values.selectedBank.corrAccount
                                                                ? `, Кор.счет ${this.props.values.selectedBank.corrAccount}`
                                                                : ''
                                                        }
                                                    </span>
                                                )
                                                : null}
                                        </div>
                                    </div>
                                    {this.props.showAddressField
                                        ? (
                                            <div className={styles['input-row']} style={{ marginTop: 15 }} data-hidden={this.props.values.paymentType === 'ACC2ACC'}>
                                                <InputGroup
                                                    id="address"
                                                    caption="Адрес плательщика"
                                                    style={{
                                                        flexGrow: 1,
                                                        width: '100%',
                                                    }}
                                                    onChange={this.props.handleInputChange}
                                                    value={this.props.values.address}
                                                    disabled={this.props.disabled}
                                                />
                                            </div>
                                        )
                                        : null}
                                </div>
                            </div>

                            {
                                this.props.values.paymentType === 'ACC2ACC'
                                    ? (
                                        <div>
                                            <div className={styles['flex-wrap']}>
                                                <div className={styles['input-row']} style={{ marginBottom: 0 }}>
                                                    <InputSelect
                                                        id="destinationAccount"
                                                        caption="На счет"
                                                        style={{
                                                            width: '100%',
                                                            flexGrow: 1,
                                                        }}
                                                        options={accountsToList}
                                                        value={this.props.values.destinationAccount}
                                                        disabled={!this.props.editable}
                                                        onChange={this.props.handleSelectChange}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    )
                                    : null
                            }

                            <div className={styles['block-amount']}>
                                <div className={styles['form-block-name']}>
                                Сколько
                                </div>
                                <div
                                    className={styles['input-row']}
                                    style={{
                                        display: 'flex',
                                        alignItems: 'center',
                                        flexWrap: 'wrap',
                                        marginBottom: 0,
                                    }}
                                >
                                    <InputAmount
                                        id="amount"
                                        caption="Сумма платежа"
                                        style={{
                                            width: 200,
                                            flexGrow: 1,
                                            marginRight: this.props.values.paymentType === 'ACC2ACC' ? 0 : 20,
                                        }}
                                        value={this.props.values.amount}
                                        disabled={!this.props.editable}
                                        onChange={this.props.handleAmountChange}
                                        invalid={this.props.values.invalidFields.find(f => f.field === 'amount')}
                                    />
                                    {this.props.values.paymentType !== 'ACC2ACC'
                                        ? (
                                            <NdsList
                                                ndsList={this.props.ndsList}
                                                selectedNds={this.props.values.selectedNds}
                                                handleNdsChange={this.props.handleNdsChange}
                                                disabled={!this.props.editable}
                                            />
                                        )
                                        : null}

                                    <div
                                        style={{
                                            width: '100%',
                                            height: 20,
                                            marginBottom: 5,
                                            fontSize: 14,
                                            textOverflow: 'ellipsis',
                                            whiteSpace: 'nowrap',
                                            overflow: 'hidden',
                                        }}
                                    >
                                        {numberToPhrase(
                                            parseFloat(
                                                this.props.values.amount.replace(/\s/g, '').replace(/,/, '.'),
                                                10,
                                            ),
                                            this.props.values.currCodeIso,
                                        )}
                                    </div>
                                </div>

                                <div className={styles['input-row']} data-hidden={this.props.values.paymentType === 'ACC2ACC'}>
                                    <InputSelect
                                        id="kpp"
                                        caption="КПП"
                                        style={{
                                            width: 200,
                                        // flexGrow: 1,
                                        }}
                                        options={this.props.values.kppList}
                                        value={this.props.values.kpp}
                                        disabled={!this.props.editable}
                                        onChange={this.props.handleSelectChange}
                                        editable
                                        mask={'999999999'}
                                        hint={Hints.ClientKpp}
                                        hintContainer={this.hintContainer}
                                        hintDisabled={this.props.values.hintsDisabled}
                                        restoreHintContainer
                                        invalid={this.checkIfInvalid('kpp')}
                                    />
                                    {
                                        this.props.values.activeTab === 'simple'
                                            ? (
                                                <div
                                                    style={{ width: 280 }}
                                                    data-hidden={this.props.values.paymentType === 'ACC2ACC'}
                                                >
                                                    {
                                                        this.props.editable
                                                            ? (
                                                                <div data-hidden={this.props.values.showUinField}>
                                                                    <span
                                                                        style={{
                                                                            color: '#41334b',
                                                                            fontSize: 12,
                                                                            cursor: 'pointer',
                                                                        }}
                                                                        onClick={this.props.toggleUinFieldVisibility}
                                                                    >
                                                                        {this.props.values.showUinField ? 'Скрыть УИН' : 'Указать УИН'}
                                                                    </span>
                                                                </div>
                                                            )
                                                            : null
                                                    }
                                                    <InputGroup
                                                        id="uin"
                                                        caption="(22) - Код (УИН)"
                                                        mask="rrrrrrrrrrrrrrrrrrrrrrrrr"
                                                        maskChar={null}
                                                        style={{
                                                            display: this.props.values.showUinField || (!this.props.editable && this.props.values.uin)
                                                                ? 'flex'
                                                                : 'none',
                                                            flexGrow: 1,
                                                            width: 280,
                                                        }}
                                                        value={this.props.values.uin}
                                                        disabled={!this.props.editable}
                                                        onChange={this.props.handleInputChange}
                                                        hint={Hints.Uin}
                                                        hintContainer={this.hintContainer}
                                                        hintDisabled={this.props.values.hintsDisabled}
                                                        restoreHintContainer
                                                        invalid={this.checkIfInvalid('uin')}
                                                    />
                                                </div>
                                            )
                                            : null
                                    }
                                </div>

                                <div className={styles['input-row']}>
                                    <InputSelect
                                        id="account"
                                        caption="Источник"
                                        style={{
                                            width: '100%',
                                            flexGrow: 1,
                                        }}
                                        options={accountsForPayments}
                                        value={this.props.values.account}
                                        disabled
                                        onChange={this.props.handleSelectChange}
                                    />
                                </div>

                                {
                                    this.props.codevoPayments && showValuteCode
                                        ? (
                                            <div className={styles['input-row']} style={{ marginBottom: 20 }}>
                                                <InputSelect
                                                    id="codevo"
                                                    caption="Код валютной операции"
                                                    style={{
                                                        width: '100%',
                                                        flexGrow: 1,
                                                    }}
                                                    options={codevoPaymentsValues}
                                                    value={this.props.values.codevo}
                                                    disabled={!this.props.editable}
                                                    onChange={this.props.handleSelectChange}
                                                />
                                            </div>
                                        )
                                        : null
                                }

                                <div className={styles['input-row']} style={{ marginBottom: 5 }}>
                                    <InputTextarea
                                        id="paymentDesc"
                                        caption="Назначение платежа"
                                        maxLength={210}
                                        height={50}
                                        style={{
                                            width: '100%',
                                        }}
                                        textareaStyle={{
                                            fontSize: 16,
                                        }}
                                        value={this.props.values.paymentDesc}
                                        disabled={!this.props.editable}
                                        onChange={this.props.handleInputChange}
                                        onPaste={this.props.handleDescriptionOnPaste}
                                        showSymbolCounter
                                        hint={Hints.Description}
                                        hintContainer={this.hintContainer}
                                        hintDisabled={this.props.values.hintsDisabled}
                                        restoreHintContainer
                                        autosize
                                        invalid={this.checkIfInvalid('paymentDesc')}
                                    />
                                </div>
                            </div>

                            {
                                this.props.values.activeTab === 'simple' && this.props.editable
                                    ? (
                                        <div
                                            style={{
                                                display: 'flex',
                                                flexWrap: 'wrap',
                                                marginTop: 25,
                                            }}
                                            data-hidden={this.props.values.paymentType === 'ACC2ACC'}
                                        >
                                            <div className={styles['form-block-name']}>
                                            Уведомить получателя
                                            </div>
                                            <div style={{ display: 'flex' }}>
                                                <InputGroup
                                                    id="notifyMobile"
                                                    caption="Мобильный телефон"
                                                    mask="+7 (999) 999-99-99"
                                                    maskChar="_"
                                                    style={{
                                                        width: 200,
                                                    }}
                                                    onChange={this.props.handleInputChange}
                                                    value={this.props.values.notifyMobile}
                                                    disabled={!this.props.editable}
                                                />
                                                <InputGroup
                                                    id="notifyEmail"
                                                    caption="E-Mail"
                                                    style={{
                                                        width: 280,
                                                    }}
                                                    onChange={this.props.handleInputChange}
                                                    value={this.props.values.notifyEmail}
                                                    disabled={!this.props.editable}
                                                />
                                            </div>
                                        </div>
                                    )
                                    : null
                            }

                            {
                                this.props.values.activeTab === 'nalog'
                                    ? (
                                        <NalogFieldGroup
                                            disabled={!this.props.editable}
                                            activeTab={this.props.values.activeTab}
                                            paymentType={this.props.values.paymentType}
                                            pStatus={this.props.pStatus}
                                            paymentsPayground={this.props.paymentsPayground}
                                            paymentsTaxperiod={this.props.paymentsTaxperiod}
                                            handleInputChange={this.props.handleInputChange}
                                            handleSelectChange={this.props.handleSelectChange}
                                            handleNalogDateChange={this.props.handleNalogDateChange}
                                            showAddressField={this.props.showAddressField}
                                            values={this.props.values}
                                            ref={(el) => { this.nalogFieldGroup = el; }}
                                            hintContainer={this.hintContainer}
                                            hintDisabled={this.props.values.hintsDisabled}
                                            modalDataCheckStatus={modalDataCheckStatus}
                                            taxDocDate={this.props.taxDocDate}
                                            checkIfInvalid={this.checkIfInvalid}
                                        />
                                    )
                                    : null
                            }
                        </div>

                        <div className={styles['information-content']} ref={(el) => { this.hintContainer = el; }} />
                    </div>

                </Scrollbars>
            </div>
        );
    }
}
