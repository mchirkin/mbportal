/** Third party */
import React, { Component } from 'react';
import { Scrollbars } from 'react-custom-scrollbars';
import classNames from 'classnames/bind';
/** Global */
import ModalWrapper from 'components/ModalWrapper';
import ModalBtnRow from 'components/ModalBtnRow';
import ModalBtn from 'components/ModalBtn';
import BlockLoader from 'components/BlockLoader';
/** Styles */
import styles from './payment.css';

const cx = classNames.bind(styles);

/**
 * Модальная окно выбора контрагента
 */
export default class SelectContragentModal extends Component {
    render() {
        const contragentList = this.props.contragentList ? this.props.contragentList : [];
        return (
            <ModalWrapper
                id="payment-modal-contragent-select"
                header="Список контрагентов"
                width={515}
                isVisible={this.props.isVisible}
                closeModal={this.props.handleCancel}
            >
                {
                    this.props.isFetching
                        ? (
                            <BlockLoader isFetching />
                        )
                        : null
                }
                <div style={{ marginBottom: 20 }}>
                    {/* <h4>Выберите контрагента</h4> */}
                    <Scrollbars style={{ height: 400 }}>
                        <ul className={styles['certificates-list']}>
                            {contragentList.map((contragent, index) => {
                                let itemStyles = cx({
                                    'certificates-list-item': true,
                                });

                                if (index === this.props.selectedContragent) {
                                    itemStyles = cx({
                                        'certificates-list-item': true,
                                        'certificates-list-item--selected': true,
                                    });
                                }

                                let accNumber = '';
                                if (contragent.accList && contragent.accList.accNumber) {
                                    accNumber = contragent.accList.accNumber;
                                }

                                return (
                                    <li
                                        className={itemStyles}
                                        key={contragent.id}
                                        onClick={() => this.props.selectContragent(index)}
                                    >
                                        <div>
                                            <div style={{ fontSize: 16 }}>
                                                {contragent.fullname}
                                            </div>
                                            <div>
                                            ИНН {contragent.inn}
                                            </div>
                                            {
                                                accNumber
                                                    ? (
                                                        <div>Расчетный счет {accNumber}</div>
                                                    )
                                                    : null
                                            }
                                        </div>
                                        <div>
                                            <div className={styles.circle} />
                                        </div>
                                    </li>
                                );
                            })}
                        </ul>
                    </Scrollbars>
                </div>
                {!this.props.isFetching
                    ? (
                        <ModalBtnRow>
                            <ModalBtn
                                width={120}
                                btnTheme="gradient"
                                btnText="Выбрать"
                                handleClick={this.props.handleSubmit}
                            />
                            <ModalBtn
                                width={120}
                                btnTheme="white"
                                btnText="Отмена"
                                handleClick={() => { this.props.setModalVisibility(false); }}
                            />
                        </ModalBtnRow>
                    )
                    : null}
            </ModalWrapper>
        );
    }
}
