import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { dates, fetch } from 'utils';

import setCancelDocumentModalVisibility from 'modules/accounts/actions/set_cancel_document_modal_visibility';
import setCancelId from 'modules/accounts/actions/set_cancel_id';
import setDocumentForSign from 'modules/crypto/actions/set_document_for_sign';
import setSignModalVisibility from 'modules/crypto/actions/set_sign_modal_visibility';
import setSignModalCallback from 'modules/crypto/actions/set_sign_modal_callback';
import setSignModalSuccessMessage from 'modules/crypto/actions/set_sign_modal_success_message';
import setPaymentModalVisibility from 'modules/payments/payment_modal/actions/set_visibility';
import setPaymentModalData from 'modules/payments/payment_modal/actions/set_data';

import CancelDocumentModal from '../../components/CancelDocumentModal';

/**
 * Контейнер модального окна для отзыва документа
 */
class CancelDocumentModalContainer extends Component {
    static contextTypes = {
        router: PropTypes.object.isRequired,
    };

    static propTypes = {
        isVisible: PropTypes.bool,
        docData: PropTypes.object,
        cancelId: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.array,
        ]),
        accounts: PropTypes.array,
        selectedAccount: PropTypes.string,
        setCancelDocumentModalVisibility: PropTypes.func,
        setCancelId: PropTypes.func,
        setPaymentModalVisibility: PropTypes.func,
        setPaymentModalData: PropTypes.func,
        setMessageData: PropTypes.func,
        closePaymentModal: PropTypes.func,
    };

    static defaultProps = {
        isVisible: false,
        docData: null,
        cancelId: '',
        accounts: [],
        selectedAccount: '',
        setCancelDocumentModalVisibility: undefined,
        setCancelId: undefined,
        setPaymentModalVisibility: undefined,
        setPaymentModalData: undefined,
        setMessageData: undefined,
        closePaymentModal: undefined,
    };

    constructor(props) {
        super(props);
        this.state = {
            reason: '',
            data: {},
            error: '',
            editable: true,
            isFetching: false,
        };
    }

    componentWillReceiveProps(nextProps) {
        // смотрим есть ли отзыв
        if (nextProps.cancelId && this.props.cancelId !== nextProps.cancelId) {
            this.setState({
                isFetching: true,
            });

            const cancelIds = Array.isArray(nextProps.cancelId) ? nextProps.cancelId : [nextProps.cancelId];
            if (!Array.isArray(nextProps.cancelId)) {
                this.getCancelDataById(nextProps.cancelId).then((data) => {
                    if (data) {
                        const cancelData = data.rosevroSimpleDocument.requestCancelUlDocument;
                        if (cancelData.status !== 'decline' || nextProps.showDecline) {
                            this.setState({
                                data: cancelData,
                                reason: cancelData.cause,
                                editable: cancelData.status === 'new',
                            });
                        } else {
                            this.setState({
                                data: {},
                                reason: '',
                                editable: true,
                            });
                        }
                    }
                    this.setState({
                        isFetching: false,
                    });
                });
            } else {
                this.getAllCancelData(cancelIds).then((result) => {
                    const newStatus = result.filter((el) => {
                        if (el) {
                            return el.rosevroSimpleDocument.requestCancelUlDocument.status === 'new';
                        }
                        return false;
                    });
                    if (newStatus && newStatus.length > 0) {
                        return newStatus;
                    }
                    const sendStatus = result.filter((el) => {
                        if (el) {
                            const status = el.rosevroSimpleDocument.requestCancelUlDocument.status;
                            return status !== 'new' && status !== 'decline';
                        }
                        return false;
                    });
                    return sendStatus;
                }).then((newRequests) => {
                    console.log(newRequests);
                    if (newRequests && newRequests.length > 0) {
                        const data = newRequests[0].rosevroSimpleDocument.requestCancelUlDocument;
                        this.setState({
                            data,
                            reason: data.cause,
                            editable: data.status === 'new',
                        });
                    }
                    this.setState({
                        isFetching: false,
                    });
                });
            }
        }
    }

    /**
     * Получить данные отзыва по его id
     * @param {string|number} id - идентификатор документа
     * @return {Promise}
     */
    getCancelDataById = id => fetch('/api/v1/documents/getbyid', {
        method: 'POST',
        credentials: 'include',
        body: JSON.stringify({
            id,
            docModule: 'ibankul',
            docType: 'request_cancel',
        }),
    }).then(response => response.json()).then(json => json).catch((err) => {
        console.log(err);
        return err;
    })

    /**
     * Получить данные отзывов по массиву идентификаторов
     * @param {array} ids - массив идентификаторов отзывов
     * @return {Promise}
     */
    getAllCancelData = (ids) => {
        ids = Array.isArray(ids) ? ids : [ids];
        const promises = ids.map(id => this.getCancelDataById(id));
        return Promise.all(promises);
    }

    /**
     * Сохранить отзыв в iSimple
     * @param {Object} data - данные для отзыва
     * @return {Promise}
     */
    saveCancel = data => fetch('/api/v1/documents/cancel', {
        method: 'POST',
        credentials: 'include',
        body: JSON.stringify({
            id: data.id,
            docDate: data.docDate,
            cancelDocId: data.cancelDocId,
            cause: data.cause,
        }),
    })
        .then(response => response.json())
        .then((json) => {
            if (json && json.errorText) {
                return Promise.reject(json.errorText);
            }
            return json;
        })
        .catch((err) => {
            console.log(err);
            return Promise.reject(err);
        });

    /**
     * Обработчик изменения причины отзыва
     * @param {Event} e
     */
    handleReasonChange = (e) => {
        this.setState({
            reason: e.target.value,
            error: '',
        });
    }

    /**
     * Обработчик нажатия кнопки "Отозвать"
     * Выполняется сохранение отзыва и установка признака, что нужно подписать документ
     */
    handleSubmitCancelBtnClick = () => {
        // данные для сохранения отзыва
        let objectToSave = {};
        // смотрим редактируем отзыв или создаем новый
        if (Object.keys(this.state.data).length > 0) {
            objectToSave = {
                id: this.state.data.id,
                docDate: this.state.data.docDate,
                cancelDocId: this.state.data.cancelDocId,
                cause: this.state.reason,
            };
        } else {
            objectToSave = {
                docDate: dates.getDateString(new Date(), 'Y-m-d'),
                cancelDocId: this.props.docData.id,
                cause: this.state.reason,
            };
        }
        this.setState({
            isFetching: true,
        });
        this.saveCancel(objectToSave)
            .then((result) => {
                this.setState({
                    isFetching: false,
                    cancelId: result.id,
                    needSign: true,
                });
                // this.handleCancelBtnClick();
            })
            .catch((err) => {
                console.log(err);
                this.setState({
                    isFetching: false,
                    error: err,
                });
                this.props.setMessageData({
                    isVisible: true,
                    header: 'Внимание!',
                    body: err,
                    btnText: 'Назад',
                    callback: () => this.props.setMessageData({ isVisible: false }),
                });
            });
    }

    /**
     * Обработчик нажатия кнопки "Сохранить"
     */
    handleSaveBtnClick = () => {
        let objectToSave = {};
        if (Object.keys(this.state.data).length > 0) {
            objectToSave = {
                id: this.state.data.id,
                cancelDocId: this.state.data.cancelDocId,
                cause: this.state.reason,
            };
        } else {
            objectToSave = {
                docDate: (new Date()).toLocaleDateString().replace(/^(\d+).(\d+).(\d+)$/, '$3-$2-$1'),
                cancelDocId: this.props.docData.id,
                cause: this.state.reason,
            };
        }
        this.setState({
            isFetching: true,
        });
        this.saveCancel(objectToSave).then((result) => {
            console.log(result);
            this.setState({
                isFetching: false,
            });
            this.handleCancelBtnClick();
        }).catch((err) => {
            console.log('Error in request cancel', err);
            this.setState({
                isFetching: false,
                error: err,
            });
        });
    }

    /**
     * Обработчик нажатия кнопки "Отмена"
     * @param {Event} e
     */
    handleCancelBtnClick = () => {
        this.props.setCancelDocumentModalVisibility(false);
        this.props.setCancelId(null);
        this.setState({
            reason: '',
            data: {},
            error: '',
            isFetching: false,
            editable: true,
        });
    }

    signCallback = () => {
        this.handleCancelBtnClick();
        this.props.setMessageData({
            isVisible: true,
            header: 'Успешно!',
            body: 'Обработка запроса на отзыв платежного поручения займет не более двух минут',
            btnText: 'Далее',
            callback: () => {
                this.props.setMessageData({ isVisible: false });
                this.props.closePaymentModal();
                this.context.router.history.push('/home');
            },
        });
    }

    render() {
        return (
            <div
                style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    display: this.props.isVisible ? 'flex' : 'none',
                    zIndex: 10000,
                    width: '100%',
                    height: '100%',
                    backgroundColor: '#fff',
                }}
            >
                <CancelDocumentModal
                    docData={this.props.docData}
                    reason={this.state.reason}
                    error={this.state.error}
                    cancelData={this.state.data}
                    handleReasonChange={this.handleReasonChange}
                    handleSubmitCancelBtnClick={this.handleSubmitCancelBtnClick}
                    handleSaveBtnClick={this.handleSaveBtnClick}
                    handleCancelBtnClick={this.handleCancelBtnClick}
                    isFetching={this.state.isFetching}
                    editable={this.state.editable}
                    signCallback={this.signCallback}
                    {...this.state}
                />
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        isVisible: state.accounts.cancelDocumentModalVisibility,
        docData: state.accounts.cancelDocumentModalData,
        cancelId: state.accounts.cancelId,
        accounts: state.accounts.data,
        selectedAccount: state.accounts.selectedAccount,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        setCancelDocumentModalVisibility: bindActionCreators(setCancelDocumentModalVisibility, dispatch),
        setCancelId: bindActionCreators(setCancelId, dispatch),
        setSignModalVisibility: bindActionCreators(setSignModalVisibility, dispatch),
        setDocumentForSign: bindActionCreators(setDocumentForSign, dispatch),
        setSignModalCallback: bindActionCreators(setSignModalCallback, dispatch),
        setSignModalSuccessMessage: bindActionCreators(setSignModalSuccessMessage, dispatch),
        setPaymentModalVisibility: bindActionCreators(setPaymentModalVisibility, dispatch),
        setPaymentModalData: bindActionCreators(setPaymentModalData, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(CancelDocumentModalContainer);
