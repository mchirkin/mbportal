/** Third Party */
import React, { Component } from 'react';
import { render, unmountComponentAtNode } from 'react-dom';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { CSSTransition } from 'react-transition-group';
import _ from 'lodash';
import moment from 'moment';
/** Redux actions */
import fetchTemplates from 'modules/templates/actions/fetch';
import fetchContragents from 'modules/settings/actions/contragents/fetch';
import fetchSendType from 'modules/settings/actions/send_type/fetch';
import fetchPStatus from 'modules/payments/actions/pstatus/fetch';
import fetchSorts from 'modules/payments/actions/sorts/fetch';
import fetchTaxperiod from 'modules/payments/actions/taxperiod/fetch';
import fetchPayground from 'modules/payments/actions/payground/fetch';
import fetchPaytype from 'modules/payments/actions/paytype/fetch';
import fetchNds from 'modules/payments/actions/nds/fetch';
import fetchNewStatus from 'modules/main_page/actions/ministatement/outcome/sign/fetch';
import showModal from 'modules/modal/actions/show_modal';
import setPaymentModalVisibility from 'modules/payments/payment_modal/actions/set_visibility';
import setPaymentModalData from 'modules/payments/payment_modal/actions/set_data';
import setSignModalVisibility from 'modules/crypto/actions/set_sign_modal_visibility';
import setSignModalCallback from 'modules/crypto/actions/set_sign_modal_callback';
import setDocumentForSign from 'modules/crypto/actions/set_document_for_sign';
import setKonturFocusModalData from 'modules/settings/actions/contragents/set_kontur_focus_modal_data';
import setKonturFocusModalVisibility from 'modules/settings/actions/contragents/set_kontur_focus_modal_visibility';
import showToast from 'modules/toast/actions/add_toast';
import closeToast from 'modules/toast/actions/delete_toast';
import setCancelDocumentModalVisibility from 'modules/accounts/actions/set_cancel_document_modal_visibility';
import setCancelDocumentModalData from 'modules/accounts/actions/set_cancel_document_modal_data';
import setCancelId from 'modules/accounts/actions/set_cancel_id';
import selectAccount from 'modules/accounts/actions/select_account';

import fetchNewPayments from 'modules/payments/new_payments/actions/fetch';
import fetchErrorPayments from 'modules/payments/error_payments/actions/fetch';
import setMessageData from 'modules/info_message/actions/set_data';
import setBackUrl from 'modules/settings/actions/set_back_url';
import showNotification from 'modules/notification/actions/show_notification';
import closeNotification from 'modules/notification/actions/close_notification';
import { toggleCalendarWidget } from 'modules/calendar_widget/toggle_calendar_widget';
/** Utils */
import { fetch, dates, formatNumber, downloadBlob, emitter } from 'utils';
import fieldMapper from 'utils/payment/field_mapper';
/** Global */
import KonturFocusModalContainer from 'containers/KonturFocusModalContainer';
import InfoMessage from 'components/InfoMessage';
/** Relative */
import PaymentFormWrapper from '../../components/payment_modal/PaymentFormWrapper';
import PaymentForm from '../../components/payment_modal/PaymentForm';
import TemplateNameModal from '../../components/payment_modal/TemplateNameModal';
import CancelDocumentModalContainer from '../CancelDocumentModalContainer';

class PaymentFormContainer extends Component {
    static contextTypes = {
        router: PropTypes.object.isRequired,
    };

    static propTypes = {
        modalData: PropTypes.object,
        accounts: PropTypes.array,
        selectedAccount: PropTypes.string,
        orgInfo: PropTypes.object,
        ndsList: PropTypes.array,
        templates: PropTypes.array,
        contragentList: PropTypes.array,
        sendType: PropTypes.array,
        pStatus: PropTypes.array,
        paymentsPayground: PropTypes.array,
        paymentsTaxperiod: PropTypes.array,
        codevoPayments: PropTypes.array,
        backUrl: PropTypes.string,
        notification: PropTypes.object,
        fetchTemplates: PropTypes.func.isRequired,
        fetchContragents: PropTypes.func.isRequired,
        fetchSendType: PropTypes.func.isRequired,
        fetchPStatus: PropTypes.func.isRequired,
        fetchSorts: PropTypes.func.isRequired,
        fetchTaxperiod: PropTypes.func.isRequired,
        fetchPaytype: PropTypes.func.isRequired,
        fetchPayground: PropTypes.func.isRequired,
        fetchNds: PropTypes.func.isRequired,
        showModal: PropTypes.func.isRequired,
        setPaymentModalVisibility: PropTypes.func.isRequired,
        setPaymentModalData: PropTypes.func.isRequired,
        setSignModalVisibility: PropTypes.func.isRequired,
        setSignModalCallback: PropTypes.func.isRequired,
        setDocumentForSign: PropTypes.func.isRequired,
        fetchNewStatus: PropTypes.func.isRequired,
        setKonturFocusModalData: PropTypes.func.isRequired,
        setKonturFocusModalVisibility: PropTypes.func.isRequired,
        showToast: PropTypes.func.isRequired,
        closeToast: PropTypes.func.isRequired,
        setCancelDocumentModalVisibility: PropTypes.func.isRequired,
        setCancelDocumentModalData: PropTypes.func.isRequired,
        setCancelId: PropTypes.func.isRequired,
        selectAccount: PropTypes.func.isRequired,
        fetchNewPayments: PropTypes.func.isRequired,
        fetchErrorPayments: PropTypes.func.isRequired,
        setMessageData: PropTypes.func.isRequired,
        setBackUrl: PropTypes.func.isRequired,
        showNotification: PropTypes.func.isRequired,
        closeNotification: PropTypes.func.isRequired,
    };

    static defaultProps = {
        modalData: null,
        accounts: null,
        selectedAccount: '',
        orgInfo: null,
        ndsList: null,
        templates: null,
        contragentList: null,
        sendType: null,
        pStatus: null,
        paymentsPayground: null,
        paymentsTaxperiod: null,
        codevoPayments: null,
        backUrl: '',
        notification: null,
    };

    constructor(props) {
        super(props);

        const today = dates.resetDate(new Date());

        this.state = {
            /**
             * активная вкладка
             * (simple - простой платеж, nalog - налоговый)
             */
            activeTab: 'simple',
            /**
             * Тип платежа
             * (UL - третьему лицу, ACC2ACC - себе, BUDZHET - бюджетный, TAMOZH - таможенный)
             */
            paymentType: 'UL',
            today,
            /** отображать лоадер или нет */
            isFetching: false,
            /** платежное поручение доступно для редактирования */
            editable: true,
            /** дата платежа */
            docDate: dates.getDateString(today),
            /** номер документа */
            docNumber: '',
            /** наименование получателя */
            receiverName: '',
            /** КПП получателя */
            receiverKpp: '',
            /** ИНН получателя */
            receiverInn: '',
            /** счет получателя */
            receiverAccount: '',
            /** счет, на который идет перевод(при переводе себе) */
            destinationAccount: null,
            /** сумма платежа */
            amount: '',
            /** выбранный счет */
            account: null,
            /** список счетов */
            accountList: [],
            /** выбранный КПП */
            kpp: '',
            /** список кпп */
            kppList: [],
            /** назначение платежа */
            paymentDesc: '',
            /** email для уведомления контрагента */
            notifyEmail: '',
            /** номер телефона для уведомления контрагента */
            notifyMobile: '',
            /** название банка получателя */
            bankOfReceiver: '',
            /** список банков, найденных по имени или БИК, для автодополнения */
            bankList: [],
            /** объект с информацией о выбранном банке */
            selectedBank: null,
            /** список контрагентов, найденных по имени, для автодополнения */
            contragentList: [],
            /** индекс выбранного контрагента в массиве контрагентов */
            selectedContragent: 0,
            /** объект с информацией о выбранном НДС */
            selectedNds: null,
            /** текущая строка с НДС в назначении платежа */
            currentNdsStr: '',
            /** показывать выбор очередности платежа */
            showSelectUrgen: false,
            /** очередность платежа */
            urgen: 5,
            /** срочность платежа */
            selectedSendType: null,
            /** статус налогоплательщика */
            stat: null,
            /** код УИН */
            uin: '',
            /** показать поле ввода УИН в Простом платеже */
            showUinField: false,
            /** КБК */
            kbk: '',
            /** ОКТМО */
            okato: '',
            /** Основание платежа */
            ground: null,
            /** Номер налогового документа */
            taxdocnum: '',
            /** тип налогового периода */
            tax1: null,
            /** номер месяца и года налогового периода */
            taxPeriod: '',
            /** видимость модального окна ввода названия шаблона */
            isTemplateModalVisible: false,
            /** название шаблона */
            templateName: '',
            /** видимость модального окна выбора контрагента из справочника */
            isSelectContragentModalVisible: false,
            /** было ли показано сообщние при выборе срочности платежа "срочно" */
            infoPaymentTypeMsgWasShown: false,
            /** данные из Контур.Фокус Светофор */
            konturData: null,
            /** идет ли запрос в Контур */
            konturDataIsFetching: false,
            /** видимость информационного сообщения */
            showInfoMessage: false,
            /** заголовок информационного сообщения */
            infoMessageHeader: null,
            /** контент информационного сообщения */
            infoMessageBody: null,
            /** вызываемая функция по кнопке "Далее" */
            infoMessageCallback: null,
            /** текст кнопки в информационном сообщении */
            infoMessageBtnText: '',
            /** поля, содержащие ошибки */
            invalidFields: [],
            /** выключены/включены подсказки на полях ввода */
            hintsDisabled: false,
            /** показывать непрозрачный лоадер(нужно для первоначальной загрузки, если подставляются данные) */
            showFilledLoader: window.paymentFormWillReceiveData,
            /** идентификатор выбранного шаблона из меню справа */
            selectedTemplateId: null,
        };

        // первоначальное состояние компонента
        this.initialState = Object.assign({}, this.state);
    }

    componentWillMount() {
        if (!this.props.templates) {
            this.props.fetchTemplates();
        }
        if (!this.props.contragentList) {
            this.props.fetchContragents();
        }
        if (!this.props.sendType) {
            this.props.fetchSendType();
        }
        this.props.fetchPStatus();
        this.props.fetchSorts();
        this.props.fetchTaxperiod();
        this.props.fetchPaytype();
        this.props.fetchNds();
        this.props.fetchPayground();
    }

    componentWillUnmount() {
        this.closeModal();
    }

    componentDidMount() {
        /*
        emitter.addListener('organizerDateChange', (date) => {
            this.setState({
                docDate: date,
            });
        });
        */
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.modalData && nextProps.modalData.justCreated) {
            return;
        }

        if (
            this.state.accountList.length === 0 &&
            Array.isArray(nextProps.accounts) &&
            nextProps.accounts.length > 0
        ) {
            // фильтруем рублевые счета
            const accountList = nextProps.accounts
                .filter(acc => {
                    const currency = acc.currency.toLowerCase();
                    return currency === 'rur' || currency === 'rub';
                })
                .map(acc => ({
                    acc,
                    value: acc.alias || acc.info || acc.number,
                }));
            this.setState({
                accountList,
            });
            this.initialState.accountList = accountList;
        }

        if (
            nextProps.selectedAccount &&
            Array.isArray(nextProps.accounts) &&
            !nextProps.modalData &&
            !this.state.account
        ) {
            const selectedAccount = nextProps.accounts.filter(
                acc => acc.id === nextProps.selectedAccount
            );
            if (selectedAccount && selectedAccount.length > 0) {
                const newState = {
                    account: {
                        acc: selectedAccount[0],
                        value:
                            selectedAccount[0].alias ||
                            selectedAccount[0].info ||
                            selectedAccount[0].number,
                    },
                };
                if (selectedAccount[0].type === '7') {
                    newState.activeTab = 'simple';
                    newState.paymentType = 'ACC2ACC';
                    newState.paymentDesc =
                        'Перевод собственных средств.\nНДС не  облагается';
                } else {
                    newState.activeTab = 'simple';
                    newState.paymentType = 'UL';
                }
                this.setState(newState);
            }
        }

        if (
            nextProps.accounts &&
            nextProps.selectedAccount !== this.props.selectedAccount
        ) {
            const selectedAccount = nextProps.accounts.filter(
                acc => acc.id === nextProps.selectedAccount
            );
            if (selectedAccount && selectedAccount.length > 0) {
                this.setState({
                    account: {
                        acc: selectedAccount[0],
                        value:
                            selectedAccount[0].alias ||
                            selectedAccount[0].info ||
                            selectedAccount[0].number,
                    },
                });
            } else {
                this.closeModal();
            }
        }

        /*
        if (!this.props.paymentData && nextProps.paymentData) {
            this.getNdsValueFromPaymentData(nextProps.paymentData);
        }
        */

        if (this.state.kppList.length === 0 && nextProps.orgInfo) {
            const kppArr = [];
            if (nextProps.orgInfo.KPP) {
                kppArr.push({
                    value: nextProps.orgInfo.KPP,
                });
            }
            if (nextProps.orgInfo.additionalKpp) {
                if (!Array.isArray(nextProps.orgInfo.additionalKpp)) {
                    kppArr.push({
                        value: nextProps.orgInfo.additionalKpp,
                    });
                } else {
                    const additionalKpp = nextProps.orgInfo.additionalKpp.map(
                        kpp => ({
                            value: kpp,
                        })
                    );
                    kppArr.push(...additionalKpp);
                }
            }
            if (kppArr.length === 0) {
                kppArr.push({
                    value: '',
                });
            }
            this.setState({
                kppList: kppArr,
                kpp: kppArr[0],
            });

            this.initialState.kpp = kppArr[0];
            this.initialState.kppList = kppArr;
        }

        if (
            nextProps.modalData &&
            !_.isEqual(this.props.modalData, nextProps.modalData) &&
            !nextProps.modalData.justCreated
        ) {
            // установка статуса "Прочитано"
            if (nextProps.modalData.sendViewedRequest && !this.viewedFetched) {
                this.setViewed(nextProps.modalData.id);
            }

            if (
                !_.isEqual(this.props.modalData, nextProps.modalData) ||
                !this.firstTimeDataWasSet
            ) {
                this.setModalData(nextProps.modalData);

                if (nextProps.modalData.showCancelModal) {
                    this.handleCancelDocument(nextProps.modalData);
                }
            }

            const inn =
                (nextProps.modalData.status === 'new' &&
                    nextProps.modalData.corrInn) ||
                '';
            if (
                (inn.length === 5 || inn.length === 10 || inn.length === 12) &&
                !this.konturFirstTimeFetched
            ) {
                this.konturFirstTimeFetched = true;
                const innValue = inn;
                this.setState({
                    konturData: null,
                    konturDataIsFetching: true,
                });
                this.getContragentDataFromKonturFocus({ inn }).then(data => {
                    if (innValue === this.state.receiverInn) {
                        const info = data
                            ? data.CompanyInfo || data.IpInfo
                            : null;
                        const fio = info && info.fio ? `ИП ${info.fio}` : null;
                        const newState = {
                            konturData: data === null ? false : data,
                            konturDataIsFetching: false,
                        };
                        if (info) {
                            newState.receiverKpp = info.kpp || '0';
                            newState.receiverName =
                                this.state.receiverName ||
                                info.longName ||
                                info.shortName ||
                                fio;
                        }
                        this.setState(newState);
                    }
                });
            }
        }

        if (nextProps.orgInfo) {
            this.setState({
                address: nextProps.orgInfo.lawAddress || '',
            });
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (
            !_.isEqual(this.props, nextProps) ||
            !_.isEqual(this.state, nextState)
        ) {
            return true;
        }
        return false;
    }

    componentDidUpdate() {
        if (this.state.paymentType !== 'ACC2ACC') {
            if (this.state.account && this.state.account.acc.type === '7') {
                this.resetAccount();
            }
        }
    }

    resetAccount = () => {
        this.setState({
            account: null,
        });
    };

    handleWrapperRef = c => {
        this.wrapperRef = c;
    };

    /**
     * Обработчик нажатия кнопки "Просмотрено" у поручения в статусе "Ошибка"
     * @param {string} id идентификатор документа
     */
    setViewed = (/* id */) => {
        /*
        this.viewedFetched = true;
        fetch('/api/v1/documents/set_viewed', {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify({
                docId: id,
            }),
        }).then((response) => {
            console.log(response);
            if (response.ok) {
                return {};
            }
        }).then((data) => {
            this.props.fetchErrorPayments();
        }).catch((err) => {
            console.log(err);
        });
        */
    };

    // ----------------------------------------
    // Обработка изменения полей формы
    // ----------------------------------------

    /**
     * Обработчик измеения даты платежа
     * @param {string} id идентификатор Datepicker'а
     * @param {string} date новая дата платежа
     */

    handleDocDateChange = date => {
        this.setState({
            docDate: date.format('DD.MM.YYYY') || '',
        });
    };

    /**
     * Обработчик изменения полей формы
     * @param {Event} e
     */
    handleInputChange = e => {
        const id = e.id || e.target.getAttribute('id');
        const value =
            id === 'docNumber'
                ? e.target.value.replace(/[^\d]/g, '')
                : e.target.value;
        const currentValue = this.state[id];

        const fieldInISIMPLE = fieldMapper(id);

        const newInvalidFields = this.state.invalidFields.filter(field => {
            if (id === 'taxPeriod') {
                return (
                    field.field !== id &&
                    !field.field.includes('tax2') &&
                    !field.field.includes('tax3')
                );
            }

            return field.field !== id && !field.field.includes(fieldInISIMPLE);
        });

        this.setState({
            [id]: value,
            invalidFields: newInvalidFields,
        });

        if (id === 'docNumber') {
            if (e.target.value.length > 2) {
                e.target.style.width = `${e.target.value.length * 10 + 30}px`;
            } else {
                e.target.style.width = '42px';
            }
        }

        if (id === 'bankOfReceiver') {
            const bankOfReceiver = e.target.value;
            if (bankOfReceiver.length === 0) {
                this.setState({
                    bankList: [],
                });
            } else {
                this.fetchBankList(bankOfReceiver).then(result => {
                    if (this.state.bankOfReceiver === bankOfReceiver) {
                        this.setState({
                            bankList: result,
                        });
                    }
                });
            }
            this.setState({
                selectedBank: null,
            });
        }

        if (id === 'receiverName') {
            const receiverName = e.target.value;
            if (receiverName.length === 0) {
                this.setState({
                    contragentList: [],
                });
            } else {
                this.fetchContragentList(e.target.value).then(result => {
                    if (this.state.receiverName === receiverName) {
                        if (result) {
                            const contragentList = Array.isArray(
                                result.corrDicElement
                            )
                                ? result.corrDicElement
                                : [result.corrDicElement];

                            contragentList.forEach(agent => {
                                agent.name = agent.fullname;
                            });

                            this.setState({
                                contragentList,
                            });
                        } else {
                            this.setState({
                                contragentList: null,
                            });
                        }
                    }
                });

                if (/^\d+$/.test(value)) {
                    if (/^(\d{5}|\d{10}|\d{12})$/.test(value)) {
                        const innValue = value;
                        this.setState({
                            konturData: null,
                            konturDataIsFetching: true,
                        });
                        this.getContragentDataFromKonturFocus({
                            inn: value,
                        }).then(data => {
                            if (innValue === this.state.receiverName) {
                                const info = data
                                    ? data.CompanyInfo || data.IpInfo
                                    : null;
                                const fio =
                                    info && info.fio ? `ИП ${info.fio}` : null;
                                const newState = {
                                    konturData: data === null ? false : data,
                                    konturDataIsFetching: false,
                                };
                                if (info) {
                                    newState.receiverInn = info.inn || '';
                                    newState.receiverKpp = info.kpp || '0';
                                    newState.receiverName =
                                        info.longName || info.shortName || fio;
                                }
                                this.setState(newState);
                            }
                        });
                    } else {
                        this.setState({
                            konturData: null,
                            konturDataIsFetching: false,
                        });
                    }
                }
            }
        }

        if (id === 'receiverAccount') {
            const nalogAccList = [
                '40101',
                '40302',
                '40501',
                '40601',
                '40701',
                '40503',
                '40603',
                '40703',
            ];
            nalogAccList.forEach(el => {
                if (value.substr(0, 5) === el) {
                    this.setActiveTab('nalog');
                }
            });

            const valuteAccList = ['30111', '30231', '40807', '40820'];
            valuteAccList.forEach(el => {
                if (
                    currentValue &&
                    currentValue.substr(0, 5) === el &&
                    value.substr(0, 5) !== el
                ) {
                    const paymentDesc = this.state.paymentDesc.replace(
                        /\{VO(\d*)\} */,
                        ''
                    );
                    this.setState({
                        paymentDesc,
                        codevo: null,
                    });
                }
            });
        }

        if (id === 'receiverInn') {
            this.fetchKonturFocus(value);
        }
    };

    /**
     * Обработчик изменения значений выпадающих списков
     * @param {Object} e объект с новым значением
     */
    handleSelectChange = e => {
        // идентификатор выпадающего списка
        const id = e.id;

        const fieldInISIMPLE = fieldMapper(id);

        this.setState({
            [id]: e.el,
            invalidFields: this.state.invalidFields.filter(
                field =>
                    field.field !== id && !field.field.includes(fieldInISIMPLE)
            ),
        });

        if (id === 'tax1') {
            if (e.el.period.code === '0') {
                this.setState({
                    taxPeriod: '',
                    invalidFields: this.state.invalidFields.filter(
                        field =>
                            !field.field.includes('tax1') &&
                            !field.field.includes('tax2') &&
                            !field.field.includes('tax3')
                    ),
                });
            }
        }
        if (id === 'destinationAccount') {
            if (
                this.state.account &&
                this.state.account.acc.id === e.el.acc.id
            ) {
                this.setState({
                    account: null,
                });
            }
        }
        if (id === 'account') {
            if (
                this.state.destinationAccount &&
                this.state.destinationAccount.acc.id === e.el.acc.id
            ) {
                this.setState({
                    destinationAccount: null,
                });
            }
        }
        if (id === 'codevo') {
            const paymentDesc = this.state.paymentDesc.replace(
                /\{VO(\d*)\} */,
                ''
            );
            this.setState({
                paymentDesc: `{VO${e.el.el.code}} ${paymentDesc}`,
            });
        }
    };

    /**
     * Обработчик выбора банка из автодополнения
     * @param {Object} bank
     */
    handleBankSelect = bank => {
        this.setState({
            bankOfReceiver: bank.name,
            selectedBank: bank,
            bankList: [],
        });
        if (/000|001|002$/.test(bank.bik)) {
            this.setActiveTab('nalog');
        }
    };

    /**
     * Обработчик выбора контрагента из автодополнения
     * @param {Object} agent
     */
    handleContragentSelect = agent => {
        const data = {
            receiverName: agent.fullname,
            receiverInn: agent.inn,
            receiverKpp: agent.kpp,
            contragentList: [],
        };
        if (agent.accList) {
            const accListData = Array.isArray(agent.accList)
                ? agent.accList[0]
                : agent.accList;
            // data.paymentDesc = accListData.accDescription;
            data.receiverAccount = accListData.accNumber;
            data.bankOfReceiver = accListData.bankName;
            data.selectedBank = {
                bik: accListData.bankBik,
                place: accListData.bankPlace,
                corrAccount: accListData.bankCorrAccount,
            };
        }
        data.bankList = [];
        this.setState(data, () => {
            this.fetchKonturFocus(agent.inn);
        });
    };

    /**
     * Обработчик изменения даты налогового документа
     * @param {Moment} date
     */
    handleNalogDateChange = date => {
        this.setState({
            taxdocdate: date.format('DD.MM.YYYY') || '',
            invalidFields: this.state.invalidFields.filter(
                f => !f.field.includes(fieldMapper('taxdocdate'))
            ),
        });
    };

    /**
     * Обработчик изменения типа платежа(Третьему лицу, Себе, Налоговый, Таможенный)
     * @param {Object} value
     */
    handlePaymentTypeChange = value => {
        const newState = {
            paymentType: value.id,
        };
        if (value.id === 'ACC2ACC') {
            if (
                !this.state.account &&
                this.props.selectedAccount &&
                this.state.accountList
            ) {
                const selectedAccount = this.state.accountList.filter(
                    acc => acc.acc.id === this.props.selectedAccount
                );
                if (selectedAccount && selectedAccount.length > 0) {
                    newState.account = selectedAccount[0];
                }
            }
            newState.paymentDesc =
                'Перевод собственных средств.\nНДС не  облагается';
        }
        if (this.state.paymentType === 'ACC2ACC' && value.id !== 'ACC2ACC') {
            newState.paymentDesc = '';
        }
        this.setState(newState);
    };

    /**
     * Загрузка списка банков по БИКу или наименованию
     * @param {string} context БИК или наименование банка
     * @return {Promise}
     */
    fetchBankList = context =>
        fetch('/api/v1/products/banklist', {
            credentials: 'include',
            method: 'POST',
            body: JSON.stringify({
                context,
            }),
        })
            .then(response => response.json())
            .then(json => json)
            .catch(err => {
                console.log(err);
            });

    // ----------------------------------------
    // Работа с Контрагентами
    // ----------------------------------------

    /**
     * Получить список контрганетов по имени из справочника
     * @param {string} name имя контрагента для поиска в справочнике
     * @return {Promise}
     */
    fetchContragentList = name =>
        fetch(encodeURI(`/api/v1/contragent/list?name=${name}`), {
            credentials: 'include',
        })
            .then(response => response.json())
            .then(json => json)
            .catch(err => {
                console.log(err);
            });

    /**
     * Получить данные для сохранения контрагента
     * @return {Object}
     */
    getContragentData = () => ({
        bank: this.state.bankOfReceiver,
        corrBankBik: this.state.selectedBank ? this.state.selectedBank.bik : '',
        corrFullname: this.state.receiverName,
        corrInn: this.state.receiverInn,
        corrKpp: this.state.receiverKpp,
        corrAccNumber: this.state.receiverAccount,
        paymentDescription: this.state.description,
    });

    /**
     * Сохранить контрагента в справочник
     * на основе введенных данных о получателе в платежке
     * @param {boolean} silent не выводить никаких уведомлений и не возвращать ошибок для сохранения в фоне
     */
    saveContragent = (silent = false) => {
        // если в переменной silent находится объект Event
        if (silent.target) {
            silent = false;
        }

        const data = this.getContragentData();
        if (
            data.corrInn.length !== 5 &&
            data.corrInn.length !== 10 &&
            data.corrInn.length !== 12
        ) {
            if (!silent) {
                this.props.setMessageData({
                    isVisible: true,
                    header: 'Внимание!',
                    body: 'Неверно введено значение ИНН',
                    btnText: 'Назад',
                    callback: () =>
                        this.props.setMessageData({ isVisible: false }),
                });
            }
            return;
        }
        if (!silent) {
            if (this.checkIfContragentInList(data.corrInn)) {
                this.props.setMessageData({
                    isVisible: true,
                    header: 'Внимание!',
                    body:
                        'Контрагент с таким ИНН уже добавлен в список контактов',
                    btnText: 'Назад',
                    callback: () =>
                        this.props.setMessageData({ isVisible: false }),
                });
                return;
            }

            this.setState({
                isFetching: true,
            });
        }
        fetch('/api/v1/contragent/create', {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify({
                contragent: data.corrFullname,
                inn: data.corrInn,
                kpp: data.corrKpp || '0',
                account: data.corrAccNumber.replace(/[^\d]/g, ''),
                bank: data.bank,
                bik: data.corrBankBik,
                'payment-id': '',
                'payment-description': data.paymentDescription,
            }),
        })
            .then(response => response.json())
            .then(json => {
                if (!silent) {
                    this.setState({
                        isFetching: false,
                    });
                }

                let modalText = '';
                if (!json.errorText) {
                    modalText = 'Контрагент успешно добавлен';
                    this.props.fetchContragents();
                } else {
                    modalText = json.errorText;
                }

                if (!silent) {
                    this.showInfoModal(modalText);
                    this.props.setMessageData({
                        isVisible: true,
                        header: !json.errorText ? 'Успешно' : 'Внимание!',
                        body: modalText,
                        btnText: !json.errorText ? 'Далее' : 'Назад',
                        callback: () =>
                            this.props.setMessageData({ isVisible: false }),
                    });
                }
            })
            .catch(err => {
                console.log(err);
                if (!silent) {
                    this.props.setMessageData({
                        isVisible: true,
                        header: 'Внимание!',
                        body: 'Произошла ошибка при добавление контрагента',
                        btnText: 'Назад',
                        callback: () =>
                            this.props.setMessageData({ isVisible: false }),
                    });
                }
            });
    };

    selectContragent = index => {
        this.setState({
            selectedContragent: index,
        });
    };

    setSelectContragentModalVisibility = value => {
        this.setState({
            isSelectContragentModalVisible: value,
        });
    };

    submitSelectContragent = data => {
        this.handleContragentSelect(data);
    };

    fetchKonturFocus = value => {
        if (value.length === 5 || value.length === 10 || value.length === 12) {
            const innValue = value;
            this.setState({
                konturData: null,
                konturDataIsFetching: true,
            });
            this.getContragentDataFromKonturFocus({ inn: value }).then(data => {
                if (innValue === this.state.receiverInn) {
                    const info = data ? data.CompanyInfo || data.IpInfo : null;
                    const fio = info && info.fio ? `ИП ${info.fio}` : null;
                    const newState = {
                        konturData: data === null ? false : data,
                        konturDataIsFetching: false,
                    };
                    if (info) {
                        newState.receiverKpp = info.kpp || '0';
                        newState.receiverName =
                            this.state.receiverName ||
                            info.longName ||
                            info.shortName ||
                            fio;
                    }
                    this.setState(newState);
                }
            });
        } else {
            this.setState({
                konturData: null,
                konturDataIsFetching: false,
            });
        }
    };

    /**
     * Проверить сохранен ли уже контрагент
     * @param {string} inn ИНН контрагента
     * @return {boolean}
     */
    checkIfContragentInList = inn =>
        this.props.contragentList &&
        this.props.contragentList.find(agent => agent.inn && agent.inn === inn);

    /**
     * Обработка изменения поля ввода суммы
     * @param {Event} e
     */
    handleAmountChange = e => {
        this.setState({
            amount: e.target.value,
            invalidFields: this.state.invalidFields.filter(
                field => field.field !== 'amount'
            ),
        });
        const amountUnformatted = e.target.value
            .replace(/,/, '.')
            .replace(/\s/g, '');
        const amountValue = parseFloat(amountUnformatted, 10);
        const paymentDesc = this.state.paymentDesc
            .replace(this.state.currentNdsStr, '')
            .trim();
        if (this.state.selectedNds && this.state.selectedNds.ndsTemplate) {
            // const ndsRate = parseFloat(this.state.selectedNds.ndsRate, 10);
            const ndsValue = this.getNdsValue(
                amountValue,
                this.state.selectedNds
            );
            const ndsStr = this.state.selectedNds.ndsTemplate.replace(
                /{NDS}/,
                ndsValue.toString().replace('.', ',')
            );
            this.setState({
                paymentDesc: `${paymentDesc}\n${ndsStr}`,
                currentNdsStr: ndsStr,
            });
        } else {
            this.setState({
                paymentDesc,
                currentNdsStr: '',
            });
        }
    };

    /**
     * Сохранение платежа
     * @param {Object|boolean} template объект для сохранения шаблона
     * @param {boolean} showSuccessMessage показывать сообщение об успешности сохранения платежа
     * @param {boolean} updateTemplate обновить шаблон
     * @param {string} successMessage текст сообщения при успешном сохранении платежа/шаблона
     * @return {Promise}
     */
    savePayment = (
        template = false,
        showSuccessMessage = true,
        updateTemplate = false,
        successMessage = ''
    ) => {
        this.showError(null);

        const ignoreWarnings = this.ignoreWarnings || false;

        if (!this.state.account) {
            // this.showInfoModal('Выберите источник платежа');
            return;
        }

        if (
            this.state.account &&
            this.state.account.acc &&
            this.state.account.acc.status === 'info'
        ) {
            this.props.setMessageData({
                isVisible: true,
                header: 'Внимание!',
                body: 'Счет заблокирован',
                btnText: 'Далее',
                callback: () => {
                    this.props.setMessageData({ isVisible: false });
                    this.closeModal();
                    this.context.router.history.push('/home');
                },
            });

            return;
        }

        if (!template && !this.state.amount) {
            this.showInfoModal('Укажите сумму платежа');
            this.setState({
                invalidFields: [
                    ...this.state.invalidFields,
                    {
                        field: 'amount',
                        error: 'Укажите сумму платежа',
                    },
                ],
            });
            return;
        }

        if (this.props.modalData && !template) {
            const signStatus = parseInt(this.props.modalData.signStatus, 10);
            if (signStatus > 0) {
                return Promise.resolve(this.props.modalData);
            }
        }

        const paymentData = {
            id: this.props.modalData ? this.props.modalData.id : null,
            accNumber: this.state.account.acc.number,
            accId: this.state.account.acc.id,
            kpp:
                this.state.kpp && this.state.kpp.value !== '0'
                    ? this.state.kpp.value
                    : '',
            corrBankBik: this.state.selectedBank
                ? this.state.selectedBank.bik
                : this.state.bankOfReceiver,
            corrFullname: this.state.receiverName,
            corrInn: this.state.receiverInn,
            corrKpp: this.state.receiverKpp,
            corrAccNumber: this.state.receiverAccount.replace(/[^\d]/g, ''),
            stat: null,
            uin: this.state.uin || null,
            sendtype: this.state.selectedSendType
                ? this.state.selectedSendType.code
                : null,
            urgenttype: this.state.urgen,
            description: this.state.paymentDesc,
            kbk: null,
            okato: null,
            taxdocnum: null,
            taxdocdate: null,
            docDate:
                this.state.docDate.replace(
                    /^(\d{2})\.(\d{2})\.(\d{4})$/,
                    '$3-$2-$1'
                ) || null,
            docNumber: this.state.docNumber || null,
            amount: this.state.amount.replace(/\s/g, '') || null,
            ndsTypeId: null,
            corrType: null,
            ground: null,
            tax1: this.state.tax1 || null,
            tax2: this.state.tax2 || null,
            tax3: this.state.tax3 || null,
            notifyReceiverEnabled:
                this.state.notifyEmail.length > 0 ||
                this.state.notifyMobile.length > 0,
            notifyReceiverEmail: this.state.notifyEmail,
            notifyReceiverPhone: this.state.notifyMobile.replace(/[^\d]/g, ''),
            ignoreWarnings,
        };

        if (updateTemplate && this.state.selectedTemplateId) {
            paymentData.id = this.state.selectedTemplateId;
        }

        const noNdsElement = this.props.ndsList.filter(nds => nds.ndsNo);
        if (noNdsElement && noNdsElement.length > 0) {
            paymentData.ndsTypeId = noNdsElement[0].id;
        }

        if (
            this.props.modalData &&
            this.props.modalData.fromTemplate &&
            !this.props.modalData.editTemplate
        ) {
            paymentData.id = null;
        }

        if (this.state.paymentType === 'UL') {
            paymentData.uin = this.state.showUinField
                ? this.state.uin || null
                : null;
        }

        if (template) {
            Object.assign(paymentData, template, { id: null });
            if (!this.state.templateName) {
                this.props.setMessageData({
                    isVisible: true,
                    header: 'Внимание!',
                    body: (
                        <div style={{ whiteSpace: 'pre-wrap' }}>
                            Необходимо указать название шаблона
                        </div>
                    ),
                    btnText: 'Назад',
                    callback: () => {
                        this.props.setMessageData({ isVisible: false });
                        this.setState({
                            isTemplateModalVisible: true,
                        });
                    },
                });
                return;
            }
        }

        if (this.state.paymentType === 'ACC2ACC') {
            const destAcc = this.state.destinationAccount.acc;
            paymentData.corrBankBik = destAcc.bankingInformation.bic;
            paymentData.corrFullname = this.props.orgInfo.fullName;
            paymentData.corrInn = this.props.orgInfo.INN;
            paymentData.corrAccNumber = destAcc.number;
            paymentData.sendtype = null;
        }

        const orgName =
            this.props.orgInfo.shortName || this.props.orgInfo.fullName;

        if (/^(30111|30231)/.test(this.state.receiverAccount)) {
            paymentData.fullname = `${orgName} //${this.state.address}//`;
        }

        if (this.state.activeTab === 'nalog') {
            const needAddressField =
                this.props.orgInfo && this.props.orgInfo.INN.length === 12;

            if (needAddressField) {
                paymentData.fullname = `${orgName} //${this.state.address}//`;
            }

            let taxdocdate = this.state.taxdocdate || null;
            if (taxdocdate && taxdocdate.replace(/[_.]/g, '') === '0') {
                taxdocdate = '0';
            }

            const nalogData = {
                stat: this.state.stat ? this.state.stat.status.code : null,
                uin: this.state.uin || null,
                kbk: this.state.kbk || null,
                okato: this.state.okato || null,
                taxdocnum: this.state.taxdocnum || null,
                taxdocdate,
                ground: this.state.ground
                    ? this.state.ground.payground.code
                    : null,
            };

            if (this.state.paymentType === 'BUDZHET') {
                nalogData.corrType = 'NALOG';
                nalogData.tax1 = this.state.tax1
                    ? this.state.tax1.period.code
                    : null;
                const [tax2, tax3] = this.state.taxPeriod.split('.');
                nalogData.tax2 = tax2 || null;
                nalogData.tax3 = tax3 || null;
            }

            if (this.state.paymentType === 'TAMOZH') {
                nalogData.corrType = 'TAMOZH';
                nalogData.tax1 = this.state.tax1 || null;
                nalogData.tax2 = null;
                nalogData.tax3 = null;
            }

            Object.assign(paymentData, nalogData);
        }

        if (paymentData.fullname && paymentData.fullname.length > 160) {
            this.showError(
                <div>
                    Длина поля &quot;Наименование плательщика&quot; более 160
                    символов.<br />
                    <span style={{ wordBreak: 'break-word' }}>
                        Текущее значение: {paymentData.fullname}.
                    </span>
                    <br />
                    Пожалуйста, измените значение поля &quot;Адрес
                    плательщика&quot;.
                </div>
            );

            return Promise.reject();
        }

        this.setState({
            isFetching: true,
        });

        return fetch('/api/v1/products/create', {
            credentials: 'include',
            method: 'POST',
            body: JSON.stringify(paymentData),
        })
            .then(response => response.json())
            .then(json => {
                this.setState({
                    isFetching: false,
                });

                this.ignoreWarnings = false;

                if (json.errorCode) {
                    if (json.errorText) {
                        this.setState({
                            invalidFields: [],
                        });

                        this.showError(
                            <div style={{ whiteSpace: 'pre-wrap' }}>
                                {json.errorText}
                            </div>
                        );
                    }
                    if (!json.errorText && json.warningText) {
                        this.showError(
                            <div style={{ whiteSpace: 'pre-wrap' }}>
                                {json.warningText}
                            </div>,
                            true
                        );

                        this.setState({
                            invalidFields: [],
                        });

                        this.ignoreWarnings = true;
                    }
                    if (json.errors) {
                        const errorList = Array.isArray(json.errors)
                            ? json.errors
                            : [json.errors];

                        const commonErrors = errorList.filter(err => {
                            const commonFields = [
                                'amountAll',
                                'currCode',
                                'nds',
                            ];

                            if (!err.field) {
                                return true;
                            }

                            if (commonFields.find(c => err.field.includes(c))) {
                                return true;
                            }

                            return false;
                        });

                        this.setState({
                            invalidFields: errorList,
                        });

                        if (commonErrors && commonErrors.length > 0) {
                            this.showError(
                                <div style={{ whiteSpace: 'pre-wrap' }}>
                                    Проверьте правильность заполнения полей.<br />
                                    {commonErrors.map(
                                        err => `${err.errorText}\n`
                                    )}
                                </div>
                            );
                        } else {
                            this.showError(
                                <div style={{ whiteSpace: 'pre-wrap' }}>
                                    Проверьте правильность заполнения полей.
                                </div>
                            );
                        }
                    }
                    if (!json.errors && json.warnings) {
                        const warnings = Array.isArray(json.warnings)
                            ? json.warnings
                            : [json.warnings];

                        this.showError(
                            <div style={{ whiteSpace: 'pre-wrap' }}>
                                {warnings.map(w => `${w.errorText}\n`).join('')}
                            </div>,
                            true
                        );
                    }
                } else {
                    if (showSuccessMessage) {
                        let successText =
                            successMessage || 'Ваш платеж сформирован';
                        if (template) {
                            successText = 'Ваш шаблон сформирован';
                            this.props.fetchTemplates();
                        }

                        if (updateTemplate) {
                            this.props.fetchTemplates();
                        }

                        /*
                        if (!template && this.state.selectedTemplateId) {
                            this.savePayment(false, false, true);
                        }
                        */

                        this.props.setMessageData({
                            isVisible: true,
                            header: 'Успешно!',
                            body: successText,
                            btnText: 'Далее',
                            callback: () => {
                                this.props.setMessageData({ isVisible: false });
                                if (!template && !updateTemplate) {
                                    this.closeModal();
                                    this.context.router.history.push('/home');
                                }
                            },
                        });
                    }
                    if (!template) {
                        this.props.fetchNewPayments();
                        if (
                            !this.checkIfContragentInList(
                                this.state.receiverInn
                            )
                        ) {
                            this.saveContragent(true);
                        }
                        if (showSuccessMessage) {
                            // this.closeModal();
                        }
                    }
                }

                return json;
            })
            .catch(err => {
                console.error('[Payment save error]', err);
                this.setState({
                    isFetching: false,
                });
                this.showError(
                    err.errorText || 'Произошла ошибка при сохранении платежа'
                );
                this.ignoreWarnings = false;
                return err;
            });
    };

    /**
     * Сохранение платежа и открытие окна подписи
     * @param {boolean} ignoreWarnings игнорировать предупреждения
     */
    saveAndSignPayment = (ignoreWarnings = false) => {
        this.savePayment(false, false, ignoreWarnings)
            .then(result => {
                if (result && result.id) {
                    this.props.setPaymentModalData(
                        Object.assign(result, {
                            status: 'new',
                            justCreated: true,
                            requestSign: true,
                        })
                    );

                    /*
                const selectedAccount = this.props.accounts.filter(acc => acc.id === this.props.selectedAccount);

                if (selectedAccount && selectedAccount.length > 0) {
                    this.props.setSignModalCallback(() => {
                        this.props.fetchPaymentsList(
                            this.props.countFilter,
                            false,
                            selectedAccount[0].number,
                            this.props.paymentFilter
                        );
                        this.closeModal();
                    });
                } else {
                    this.props.setSignModalCallback(() => {
                        this.closeModal();
                    });
                }
                this.props.setSignModalVisibility(true);
                */
                }
            })
            .catch(err => {
                console.log(err);
            });
    };

    signPaymentCallback = data => {
        let msg = 'Ваш платеж подписан и отправлен в банк';

        const docDate = moment(this.state.docDate, 'DD.MM.YYYY');
        const today = moment().startOf('day');

        if (today <= docDate) {
            msg = 'Платеж отправлен в банк и добавлен в календарь';
        }

        if (data.paycontrol) {
            msg = 'Подтвердите платеж в мобильном приложении';
        }

        this.props.setMessageData({
            isVisible: true,
            header: 'Успешно!',
            body: msg,
            btnText: 'Далее',
            callback: () => {
                this.closeModal();
                this.props.setMessageData({ isVisible: false });
                this.context.router.history.push('/home');
            },
        });
        // paq.push(['trackGoal', 6]);
    };

    /**
     * Обработчик нажатия на кнопки "Сохранить" и "Подписать и отправить"
     * @param {string} type тип кнопки ("Сохранить" - "save", "Подписать и отправить" - "sign")
     */
    handleSaveBtnClick = type => {
        const gibddInReceiverName =
            /гибдд|мвд/i.test(this.state.receiverName) ||
            /министерства|о внутренних дел/i.test(this.state.receiverName);

        if (
            /^40101/.test(this.state.receiverAccount) &&
            gibddInReceiverName &&
            this.state.uin === '0' &&
            !this.ignoreGibddWarning
        ) {
            const msg =
                'Обращаем ваше внимание на то, что при оплате штрафов в ГИБДД в поле Код должен быть' +
                ' указан уникальный идентификатор начисления. В противном случае оплата штрафа не будет' +
                ' учтена в ГИБДД.';
            this.showError(msg, true);

            this.ignoreGibddWarning = true;
        } else {
            switch (type) {
                case 'save':
                    this.savePayment();
                    break;
                case 'sign':
                    this.saveAndSignPayment();
                    break;
            }
        }
    };

    setModalData = data => {
        if (!this.state.accountList || this.state.accountList.length === 0) {
            return;
        }
        this.firstTimeDataWasSet = true;

        const accNumber =
            /* data.account ? data.account.acc.number : */ data.accNumber;
        let acc = this.state.accountList.find(
            acc => acc.acc.number === accNumber
        );

        if (acc && acc.acc.id !== this.props.selectedAccount) {
            this.props.selectAccount(acc.acc.id);
        }

        if (!acc && !this.state.account) {
            acc = this.state.accountList.find(
                acc => acc.acc.id === this.props.selectedAccount
            );
        }

        const isDebet = data.lineIsDebet === '0' || data.lineIsDebet === 'true';

        const kpp = isDebet ? data.corrKpp : data.kpp;

        console.log('[Acc]', acc);

        const newState = {
            showFilledLoader: false,
            editable: !!(
                data.status === 'new' ||
                data.status === 'imported' ||
                data.fromTemplate
            ),
            docNumber: !data.fromTemplate
                ? data.docNumber
                : this.state.docNumber,
            receiverName: isDebet
                ? data.fullname || ''
                : data.corrFullname || '',
            receiverKpp: isDebet ? data.kpp || '' : data.corrKpp || '',
            receiverInn: isDebet ? data.inn || '' : data.corrInn || '',
            receiverAccount: isDebet
                ? data.accNumber || ''
                : data.corrAccNumber || '',
            amount: data.amount ? formatNumber(data.amount) : '',
            // account: this.state.account ? this.state.account : acc,
            account: acc,
            kpp: kpp ? { value: kpp } : this.state.kpp,
            paymentDesc: data.description || data.lineDescription || '',
            notifyEmail: '',
            notifyMobile: '',
            bankOfReceiver: isDebet
                ? data.bankName || ''
                : data.corrBankName || '',
            selectedBank: {
                bik: isDebet ? data.bankBik || '' : data.corrBankBik || '',
                place: isDebet
                    ? data.bankPlace || ''
                    : data.corrBankPlace || '',
                corrAccount: isDebet
                    ? data.bankCorrAccount || ''
                    : data.corrBankCorrAccount || '',
            },
            bankList: [],
            contragentList: [],
            selectedNds: null,
            isFetching: false,
            uin: data.uin || '',
            kbk: data.kbk || '',
            okato: data.okato || '',
            taxdocnum: data.taxdocnum || '',
            templateName: data.templateName || '',
            urgen: data.urgenttype || 5,
            selectedSendType: data.sendtypeCaption
                ? {
                      code: data.sendtype,
                      caption: data.sendtypeCaption,
                  }
                : null,
            currCodeIso: data.currCodeIso,
            currCode: data.currCode,
        };

        this.paymentForm.setDocNumberWidth(newState.docNumber);

        if (!newState.editable) {
            newState.hintsDisabled = true;
        }

        /*
        if (data.accNumber && Array.isArray(this.props.accounts)) {
            const selectedAccount = this.props.accounts.filter(acc => acc.number === data.accNumber);
            if (selectedAccount && selectedAccount.length > 0) {
                this.setState({
                    account: {
                        acc: selectedAccount[0],
                        value: selectedAccount[0].number,
                    },
                });
            }
        }
        */

        if (newState.receiverInn && !data.income) {
            this.fetchKonturFocus(newState.receiverInn);
        }

        if (data.accNumber && data.income) {
            newState.account = {
                acc: {},
                value: data.corrAccNumber,
            };
        }

        if (data.ground) {
            const ground = this.props.paymentsPayground.find(
                ground => data.ground === ground.code
            );
            if (ground) {
                newState.ground = {
                    payground: ground,
                    value: `${ground.code} - ${ground.caption}`,
                };
            }
        }

        if (data.stat) {
            const stat = this.props.pStatus.find(
                stat => data.stat === stat.code
            );
            if (stat) {
                newState.stat = {
                    status: stat,
                    value: `${stat.code} - ${stat.caption}`,
                };
            }
        }

        if (data.corrType && data.corrType === 'NALOG') {
            newState.activeTab = 'nalog';
            newState.paymentType = 'BUDZHET';

            const tax1 = this.props.paymentsTaxperiod.find(
                tax => data.tax1 === tax.code
            );
            if (tax1) {
                newState.tax1 = {
                    period: tax1,
                    value: tax1.code,
                };
            }

            newState.taxPeriod = [data.tax2, data.tax3].join('.');
        }

        if (data.corrType && data.corrType === 'TAMOZH') {
            newState.activeTab = 'nalog';
            newState.paymentType = 'TAMOZH';
            newState.tax1 = data.tax1 || '';
        }

        if (
            !data.corrType ||
            data.corrType === 'UL' ||
            data.corrType === 'OWN'
        ) {
            newState.activeTab = 'simple';
            const destAcc = this.state.accountList.findIndex(
                acc => acc.acc.number === data.corrAccNumber
            );

            if (destAcc !== -1 && !data.income) {
                newState.paymentType = 'ACC2ACC';
                newState.destinationAccount = this.state.accountList[destAcc];
            } else {
                newState.paymentType = 'UL';
            }
        }

        if (data.taxdocdate) {
            newState.taxdocdate = data.taxdocdate;
            // this.paymentForm.setNalogDate(data.taxdocdate);
        }

        if (data.docDate && !data.fromTemplate) {
            const docDate = data.docDate.replace(
                /(\d+)-(\d+)-(\d+)/,
                '$3.$2.$1'
            );
            newState.docDate = docDate;
            this.paymentForm.setDocumentDate(
                data.docDate.replace(/(\d+)-(\d+)-(\d+)/, '$3.$2.$1')
            );
        }

        this.setState(newState);

        this.getNdsValueFromPaymentData(data);
    };

    // ----------------------------------------
    // Работа с шаблоном
    // ----------------------------------------

    /**
     * Сохранение шаблона
     */
    savePaymentAsTemplate = () => {
        this.savePayment(
            {
                template: {
                    name: this.state.templateName,
                },
                createDoc: false,
            },
            true
        );
        this.setState({
            isTemplateModalVisible: false,
        });
    };

    /**
     * Обработчик нажатия кнопки "Сохранить как шаблон"
     */
    handleSaveTemplateClick = () => {
        if (this.props.modalData && this.props.modalData.editTemplate) {
            const templateName = this.state.templateName;
            this.savePayment({}, false)
                .then(result => {
                    if (result && result.id) {
                        this.changeTemplateName(result.id, templateName)
                            .then(() => {
                                this.showInfoModal('Шаблон успешно изменен');
                            })
                            .catch(err => {
                                this.showInfoModal(
                                    err.errorText ||
                                        'Произошла ошибка при изменении шаблона'
                                );
                            });
                    }
                })
                .catch(err => {
                    console.log(err);
                });
            return;
        }

        if (this.state.selectedTemplateId) {
            this.savePayment(false, true, true, 'Шаблон изменен');
            return;
        }

        this.setState({
            isTemplateModalVisible: true,
            templateName: this.state.receiverName,
        });
    };

    /**
     * Обработчик отмены сохранения шаблона
     */
    cancelSaveTemplate = () => {
        this.setState({
            isTemplateModalVisible: false,
            templateName: '',
        });
    };

    /**
     * Обработчик изменения значения имени сохраняемого шаблона
     * @param {Event} e
     */
    handleTemplateNameChange = e => {
        this.setState({
            templateName: e.target.value,
        });
    };

    /**
     * Изменение имени шаблона
     * @param  {string} id
     * @param  {string} name
     * @return {Promise}
     */
    changeTemplateName = (id, name) =>
        new Promise((resolve, reject) => {
            fetch('/api/v1/templates/modify', {
                method: 'POST',
                credentials: 'include',
                body: JSON.stringify({
                    id,
                    name,
                }),
            })
                .then(response => response.text())
                .then(text => {
                    if (text.length > 0) {
                        const json = JSON.parse(text);
                        reject(json.errorText);
                    }
                    this.props.fetchTemplates();
                    resolve(true);
                })
                .catch(err => reject(err));
        });

    // ----------------------------------------
    // Работа с боковым списком шаблонов
    // ----------------------------------------

    /**
     * Обработчик нажатия на один из шаблонов в списке
     * @param {Object} template данные шаблона
     */
    handleTemplateClick = template => {
        this.setState({
            isFetching: true,
            selectedTemplateId: template.template.id,
            invalidFields: [],
        });
        this.showError('');
        this.fetchTemplateData(template.template.id);
    };

    /**
     * Загрузка модели документа по идентификатору шаблона
     * @param {string} id идентификатор шаблона
     */
    fetchTemplateData = id =>
        fetch(`/api/v1/templates/get_by_id/${id}`, {
            credentials: 'include',
        })
            .then(response => response.json())
            .then(data => {
                this.setModalData(
                    Object.assign({}, data, {
                        fromTemplate: true,
                        editTemplate: false,
                        account: this.state.account,
                        // accNumber: this.state.account ? this.state.account.value : data.accNumber,
                        accNumber: data.accNumber,
                    })
                );
            })
            .catch(err => {
                console.log(err);
            });

    deleteTemplate = template => {
        this.setState({
            isFetching: true,
        });

        return fetch('/api/v1/templates/delete', {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify({
                templateId: template.id,
            }),
        })
            .then(response => {
                if (response.ok) {
                    return '{}';
                }
                return response.text();
            })
            .then(result => {
                const json = JSON.parse(result);
                if (json.errorText) {
                    Promise.reject(json.errorText);
                } else {
                    this.props.fetchTemplates();
                }
                this.setState({
                    isFetching: false,
                });
            })
            .catch(err => {
                console.log(err);
            });
    };

    // ----------------------------------------
    // Работа с очередностью платежа
    // ----------------------------------------

    /**
     * Изменить видимость блока выбора очередности платежа
     */
    toggleUrgenSelectVisibility = () => {
        if (
            this.props.modalData &&
            this.props.modalData.status !== 'new' &&
            this.props.modalData.status !== 'imported'
        ) {
            return;
        }
        this.setState({
            showSelectUrgen: !this.state.showSelectUrgen,
        });
    };

    /**
     * Обработчик выбора значения очередности платежа
     * @param {number} urgen значение очередности платежа
     */
    handleUrgenChange = urgen => {
        if (
            this.props.modalData &&
            this.props.modalData.status !== 'new' &&
            this.props.modalData.status !== 'imported'
        ) {
            return;
        }
        this.setState({
            urgen,
            showSelectUrgen: false,
        });
    };

    // ----------------------------------------
    // Работа с НДС
    // ----------------------------------------

    /**
     * Получить значение НДС на основе данных платежа
     * @param {Object} data информация о платеже
     */
    getNdsValueFromPaymentData = data => {
        const nds = this.props.ndsList.filter(element => {
            if (element.ndsTemplate) {
                // const ndsRate = parseFloat(element.ndsRate, 10);
                const ndsValue = this.getNdsValue(data.amount, element);
                const ndsStr = element.ndsTemplate.replace(
                    /{NDS}/,
                    ndsValue.toString().replace('.', ',')
                );
                if (data.description && data.description.includes(ndsStr)) {
                    this.setState({
                        currentNdsStr: ndsStr,
                    });
                    return true;
                }
            }
            return !element.ndsNo && element.id === data.ndsTypeId;
        });
        if (nds && nds.length > 0) {
            this.setState({
                selectedNds: nds[0],
            });
        }
    };

    /**
     * Обработчик изменения НДС
     * @param {Object} selectedNds объект с информацией о выбранном НДС
     */
    handleNdsChange = selectedNds => {
        if (
            this.props.modalData &&
            this.props.modalData.status !== 'new' &&
            this.props.modalData.status !== 'imported'
        ) {
            return;
        }
        this.setState({
            selectedNds,
        });
        const amountUnformatted = this.state.amount
            .replace(/,/, '.')
            .replace(/\s/g, '');
        const amountValue = parseFloat(amountUnformatted, 10);
        const paymentDesc = this.state.paymentDesc
            .replace(this.state.currentNdsStr, '')
            .trim();
        if (selectedNds && selectedNds.ndsTemplate) {
            // const ndsRate = parseFloat(selectedNds.ndsRate, 10);
            const ndsValue = this.getNdsValue(amountValue, selectedNds);
            const ndsStr = selectedNds.ndsTemplate.replace(
                /{NDS}/,
                ndsValue.toString().replace('.', ',')
            );
            this.setState({
                paymentDesc: `${paymentDesc}\n${ndsStr}`,
                currentNdsStr: ndsStr,
            });
        } else {
            this.setState({
                paymentDesc,
                currentNdsStr: '',
            });
        }
    };

    /**
     * Расчет значения НДС от суммы платежа
     * @param {string|number} amount сумма платежа, от которой рассчитывается НДС
     * @param {Object} nds объект с информацией об НДС
     * @return {number} значение НДС
     */
    getNdsValue = (amount, nds) => {
        const rate = parseFloat(nds.ndsRate, 10);
        const ndsValue =
            (parseFloat(amount, 10) * (rate / 100)) / (1 + rate / 100);
        return ndsValue ? ndsValue.toFixed(2) : 0;
    };

    closeModal = e => {
        this.setState(this.initialState);
        this.props.toggleCalendarWidget(false);
        // если определен Event, то нажали на кнопку закрыть
        // и нужно вернуться назад
        if (e) {
            // this.context.router.history.push(this.props.backUrl);
        }
        const selectedAccount = this.props.accounts.find(
            acc => acc.id === this.props.selectedAccount
        );

        const mainAccount = this.props.accounts.find(
            acc =>
                (acc.currency.toUpperCase() === 'RUB' ||
                    acc.currency.toUpperCase() === 'RUR') &&
                acc.type !== '7'
        );

        if (selectedAccount && selectedAccount.type === '7') {
            this.props.selectAccount(mainAccount.id);
        }

        this.props.setBackUrl('/home');
        this.props.setPaymentModalData(null);
        this.props.setMessageData({ isVisible: false });
        this.props.setPaymentModalVisibility(false);
    };

    showInfoModal = () => {};

    showInfoMessage = ({ header, body, btnText, callback }) => {
        this.setState({
            showInfoMessage: true,
            infoMessageHeader: header,
            infoMessageBody: body,
            infoMessageBtnText: btnText,
            infoMessageCallback: callback,
        });
    };

    setActiveTab = tab => {
        if (tab !== this.state.activeTab) {
            const paymentType =
                this.state.activeTab === 'simple' ? 'BUDZHET' : 'UL';
            this.setState({
                activeTab: tab,
                tax1: null,
                taxPeriod: '',
                paymentType,
            });
        }
    };

    selectSendType = (type, infoContainer) => {
        if (
            !this.state.infoPaymentTypeMsgWasShown &&
            type.caption.toLowerCase() === 'срочно'
        ) {
            const msg = (
                <div>
                    За срочные платежи взимается повышенная комиссия в
                    соответствии с
                    <a
                        href="https://www.rosevrobank.ru/msb/otkrytie-schyeta/#tarifs"
                        target="_blank"
                        rel="noopener noreferrer"
                        style={{
                            marginLeft: 5,
                        }}
                    >
                        тарифами банка
                    </a>
                </div>
            );

            infoContainer.innerHTML = '';
            infoContainer.paddingTop = '30px';

            render(
                <CSSTransition
                    timeout={0}
                    key="sendType"
                    classNames="fadeappear"
                    appear
                    in
                >
                    <div>{msg}</div>
                </CSSTransition>,
                infoContainer
            );

            infoContainer.paddingTop = '30px';

            this.setState({
                infoPaymentTypeMsgWasShown: true,
            });
        } else {
            infoContainer.innerHTML = '';
        }

        this.setState({
            selectedSendType: type,
        });
    };

    toggleUinFieldVisibility = () => {
        const showUinField = !this.state.showUinField;
        this.setState({
            showUinField,
            uin: !showUinField ? '' : this.state.uin,
        });
    };

    handleDescriptionOnPaste = e => {
        this.setState({
            description: e.target.value.substr(0, 210),
        });
    };

    handleCopyClick = () => {
        this.setState({
            isFetching: true,
        });
        fetch('/api/v1/documents/copy', {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify({
                documentId: this.props.modalData.id,
            }),
        })
            .then(response => response.json())
            .then(data => {
                if (data.errorText) {
                    this.showInfoModal(
                        data.errorText ||
                            'Произошла ошибка при копировании документа'
                    );
                    this.setState({
                        isFetching: false,
                    });
                    return;
                }
                this.props.fetchNewStatus();
                this.props.setPaymentModalData(
                    Object.assign({}, this.props.modalData, {
                        id: data.id,
                        docDate: dates.getDateString(this.state.today),
                        docNumber: '',
                        status: 'new',
                        signStatus: '0',
                    })
                );
                this.setState({
                    editable: true,
                    isFetching: false,
                });
            })
            .catch(err => {
                console.log(err);
                this.showInfoModal(
                    err.errorText ||
                        'Произошла ошибка при копировании документа'
                );
                this.setState({
                    isFetching: false,
                });
            });
    };

    // ----------------------------------------
    // Контур.Фокус Светофор
    // ----------------------------------------

    /**
     * Получение фактов о контрагенте из Контур.Фокус Светофор
     * @param {string} inn ИНН контрагента
     * @return {Promise}
     */
    getContragentDataFromKonturFocus = ({ inn }) => {
        // Вызываем сервис, если у клиента платная подписка на сервис Светофор
        if (
            this.props.orgInfo &&
            this.props.orgInfo.konturFocusSubscr === 'paid'
        ) {
            return fetch('/api/v1/contragent/get_facts', {
                credentials: 'include',
                method: 'POST',
                body: JSON.stringify({
                    inn,
                }),
            })
                .then(response => response.json())
                .then(json => {
                    if (json && json.errorCode) {
                        return Promise.resolve(null);
                    }
                    return json;
                });
        }
        return Promise.resolve(null);
    };

    /**
     * Обработчик нажатия на Контур.Светофор
     */
    handleKonturClick = () => {
        if (this.state.konturData) {
            let href;

            if (
                this.state.konturData.CompanyInfo &&
                this.state.konturData.CompanyInfo.href
            ) {
                href = this.state.konturData.CompanyInfo.href;
            }

            if (
                this.state.konturData.IpInfo &&
                this.state.konturData.IpInfo.href
            ) {
                href = this.state.konturData.IpInfo.href;
            }

            if (href) {
                const win = window.open(href, '_blank');
                win.focus();
            }
        }
    };

    /**
     * Обработчик наведения на Светофор
     * @param {HTMLElement} infoContainer контейнер для отображения информации о контрагенте
     */
    handleKonturMouseEnter = infoContainer => {
        infoContainer.innerHTML = '';
        infoContainer.style.paddingTop = '30px';
        render(
            <KonturFocusModalContainer modalData={this.state.konturData} />,
            infoContainer
        );
    };

    /**
     * Обработчик покидания курсора мыши Светофора
     * @param {HTMLElement} infoContainer контейнер для отображения информации о контрагенте
     */
    handleKonturMouseLeave = infoContainer => {
        infoContainer.innerHTML = '';
    };

    /**
     * Обработчик нажатия на кнопку "Сохранить в PDF"
     */
    handlePDFPrint = () => {
        const { id } = this.props.modalData;

        const notificationData = {
            text: 'Идет формирование документа...',
        };

        this.props.showNotification(notificationData);

        fetch(`/api/v1/documents/get_pdf/${id}`, {
            credentials: 'include',
        })
            .then(response => {
                if (response.ok) {
                    return response.blob();
                }
            })
            .then(blob => {
                if (this.props.notification === notificationData) {
                    this.props.closeNotification();
                }
                downloadBlob(blob, `${id}.pdf`);
            })
            .catch(err => {
                console.log(err);
            });
    };

    /**
     * Обработчик нажатия на кнопку "Отозвать"
     */
    handleCancelDocument = (data = null) => {
        const modalData = data || this.props.modalData;
        this.props.setCancelDocumentModalData(modalData);
        // проверяем есть ли уже отзывы у документа
        if (modalData.cancel) {
            this.props.setCancelId(modalData.cancel);
        }
        this.props.setCancelDocumentModalVisibility(true);
    };

    /**
     * Возможен ли перевод со счета СКС
     * Возможность определяется по тарифу
     */
    canTransferFromSKS = () => {
        if (!this.props.orgInfo) return false;
        if (!this.props.orgInfo.tarifCode) return false;

        const orgTariff = this.props.orgInfo.tarifCode.toUpperCase();

        return [
            'TP_DEFAULT',
            'TP_SUCCESS',
            'TP_ACCOUNT',
            'TP_STORE',
            'TP_START',
            'TP_CASH',
        ].find(tariff => tariff === orgTariff);
    };

    /**
     * Переключение отображения подсказок при наведении на поля ввода
     */
    toggleHints = () => {
        this.setState({
            hintsDisabled: !this.state.hintsDisabled,
        });
    };

    /**
     * Обработчик выбора файла для импорта из 1С
     * @param {Array} acceptedFiles список выбранных файлов из react-dropzone
     */
    handle1CFileSelect = acceptedFiles => {
        const file = acceptedFiles[0];

        const formData = new FormData();
        formData.append('file', file);

        this.setState({
            isFetching: true,
        });

        fetch('/api/v1/products/import', {
            credentials: 'include',
            method: 'POST',
            body: formData,
        })
            .then(response => response.json())
            .then(result => {
                console.log(result);
                this.setState({
                    isFetching: false,
                });

                this.props.fetchNewPayments();

                this.props.setMessageData({
                    isVisible: true,
                    header: '',
                    body: (
                        <div style={{ whiteSpace: 'pre-wrap' }}>
                            {result.items.map(item => (
                                <div>{item.message}</div>
                            ))}
                        </div>
                    ),
                    btnText: 'Назад',
                    callback: () => {
                        this.props.setMessageData({ isVisible: false });
                    },
                });
            });
    };

    /**
     * Показать сообщение об ошибке
     * @param {string|ReactElement} text сообщение об ошибке
     * @param {boolean} warning ошибка является неблокирующей
     */
    showError = (text, warning = false) => {
        const infoContainer = this.paymentForm.hintContainer;
        infoContainer.innerHTML = '';

        if (!text) {
            infoContainer.style.paddingTop = '0px';
            unmountComponentAtNode(infoContainer);
            return;
        }

        const infoContainerHeight = infoContainer.clientHeight;

        const color = warning ? '#e6b91b' : '#ff3b30';

        render(
            <CSSTransition
                timeout={0}
                key="sendType"
                classNames="fadeappear"
                appear
                in
                style={{ color }}
            >
                <div>
                    <div>Внимание!</div>
                    <div>{text}</div>
                </div>
            </CSSTransition>,
            infoContainer
        );

        const errorTextHeight = infoContainer.firstChild.clientHeight;
        const heightDiff = infoContainerHeight - errorTextHeight;
        const paddingTop = heightDiff < 0 ? 0 : heightDiff;

        infoContainer.style.paddingTop = `${paddingTop}px`;
        this.paymentForm.formScrollbars.scrollTop(paddingTop);

        // infoContainer.clientHeight
    };

    render() {
        /**
         * Форма для просмотра
         * Убрана возможность переключения вкладок и список шаблонов
         */
        const smallForm =
            this.props.modalData &&
            this.props.modalData.status !== 'decline' &&
            this.props.modalData.status !== 'new' &&
            this.props.modalData.status !== 'imported' &&
            !this.props.modalData.editTemplate;

        // Показывать ли поле адрес в налоговом платеже
        const showAddressField =
            this.props.orgInfo &&
            ((this.props.orgInfo.INN.length === 12 &&
                this.state.activeTab === 'nalog') ||
                /^(30111|30231)/.test(this.state.receiverAccount)) &&
            this.state.editable;

        // возможен ли перевод со счета СКС
        const canTransferFromSKS = this.canTransferFromSKS();

        const isVisible = this.props.match || this.props.isVisible;

        return (
            <PaymentFormWrapper
                isVisible={!!isVisible}
                isFetching={this.state.isFetching}
                closeModal={this.closeModal}
                savePayment={this.savePayment}
                saveAndSignPayment={this.saveAndSignPayment}
                handleSaveBtnClick={this.handleSaveBtnClick}
                handleSaveTemplateClick={this.handleSaveTemplateClick}
                handleCopyClick={this.handleCopyClick}
                handlePDFPrint={this.handlePDFPrint}
                handleCancelDocument={this.handleCancelDocument}
                handleTemplateClick={this.handleTemplateClick}
                activeTab={this.state.activeTab}
                setActiveTab={this.setActiveTab}
                ref={this.handleWrapperRef}
                modalData={this.props.modalData}
                editable={this.state.editable}
                templates={this.props.templates}
                contragentList={this.props.contragentList}
                submitSelectContragent={this.submitSelectContragent}
                deleteTemplate={this.deleteTemplate}
                changeTemplateName={this.changeTemplateName}
                signPaymentCallback={this.signPaymentCallback}
                showFilledLoader={this.state.showFilledLoader}
                selectedTemplateId={this.state.selectedTemplateId}
            >
                <InfoMessage />
                <CancelDocumentModalContainer
                    setMessageData={this.props.setMessageData}
                    closePaymentModal={this.closeModal}
                />
                <PaymentForm
                    ref={el => {
                        this.paymentForm = el;
                    }}
                    editable={this.state.editable}
                    values={this.state}
                    modalData={this.props.modalData}
                    ndsList={this.props.ndsList}
                    templates={this.props.templates}
                    handleInputChange={this.handleInputChange}
                    handleSelectChange={this.handleSelectChange}
                    handleBankSelect={this.handleBankSelect}
                    handleContragentSelect={this.handleContragentSelect}
                    handleNdsChange={this.handleNdsChange}
                    handleAmountChange={this.handleAmountChange}
                    handleDocDateChange={this.handleDocDateChange}
                    handleTemplateClick={this.handleTemplateClick}
                    handleUrgenChange={this.handleUrgenChange}
                    handleNalogDateChange={this.handleNalogDateChange}
                    handlePaymentTypeChange={this.handlePaymentTypeChange}
                    handleDescriptionOnPaste={this.handleDescriptionOnPaste}
                    handle1CFileSelect={this.handle1CFileSelect}
                    savePayment={this.savePayment}
                    toggleUrgenSelectVisibility={
                        this.toggleUrgenSelectVisibility
                    }
                    toggleHints={this.toggleHints}
                    saveContragent={this.saveContragent}
                    setSelectContragentModalVisibility={
                        this.setSelectContragentModalVisibility
                    }
                    contragentList={this.props.contragentList}
                    pStatus={this.props.pStatus}
                    paymentsPayground={this.props.paymentsPayground}
                    paymentsTaxperiod={this.props.paymentsTaxperiod}
                    codevoPayments={this.props.codevoPayments}
                    sendType={this.props.sendType}
                    selectSendType={this.selectSendType}
                    toggleUinFieldVisibility={this.toggleUinFieldVisibility}
                    smallForm={smallForm}
                    getContragentDataFromKonturFocus={
                        this.getContragentDataFromKonturFocus
                    }
                    handleKonturClick={this.handleKonturClick}
                    handleKonturMouseEnter={this.handleKonturMouseEnter}
                    handleKonturMouseLeave={this.handleKonturMouseLeave}
                    konturFocusSubscr={
                        this.props.orgInfo &&
                        this.props.orgInfo.konturFocusSubscr === 'paid'
                    }
                    showAddressField={showAddressField}
                    canTransferFromSKS={canTransferFromSKS}
                    docDate={this.state.docDate}
                    taxDocDate={this.state.taxdocdate}
                />
                <TemplateNameModal
                    isVisible={this.state.isTemplateModalVisible}
                    savePaymentAsTemplate={this.savePaymentAsTemplate}
                    cancelSaveTemplate={this.cancelSaveTemplate}
                    templateName={this.state.templateName}
                    handleTemplateNameChange={this.handleTemplateNameChange}
                />
            </PaymentFormWrapper>
        );
    }
}

function mapStateToProps(state) {
    return {
        modalData: state.payments.paymentModal.data,
        accounts: state.accounts.data,
        selectedAccount: state.accounts.selectedAccount,
        orgInfo: state.user.orgInfo.data,
        ndsList: state.payments.paymentsNds.data,
        templates: state.templates.data,
        contragentList: state.settings.contragents.data,
        sendType: state.settings.sendType.data,
        pStatus: state.payments.paymentsPstatus.data,
        paymentsPayground: state.payments.paymentsPayground.data,
        paymentsTaxperiod: state.payments.paymentsTaxperiod.data,
        codevoPayments: state.payments.codevoPayments.data,
        backUrl: state.settings.backUrl,
        notification: state.notification.data,
        isVisible: state.payments.paymentModal.isVisible,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        fetchTemplates: bindActionCreators(fetchTemplates, dispatch),
        fetchContragents: bindActionCreators(fetchContragents, dispatch),
        fetchSendType: bindActionCreators(fetchSendType, dispatch),
        fetchPStatus: bindActionCreators(fetchPStatus, dispatch),
        fetchSorts: bindActionCreators(fetchSorts, dispatch),
        fetchTaxperiod: bindActionCreators(fetchTaxperiod, dispatch),
        fetchPaytype: bindActionCreators(fetchPaytype, dispatch),
        fetchPayground: bindActionCreators(fetchPayground, dispatch),
        fetchNds: bindActionCreators(fetchNds, dispatch),
        showModal: bindActionCreators(showModal, dispatch),
        setPaymentModalVisibility: bindActionCreators(
            setPaymentModalVisibility,
            dispatch
        ),
        setPaymentModalData: bindActionCreators(setPaymentModalData, dispatch),
        setSignModalVisibility: bindActionCreators(
            setSignModalVisibility,
            dispatch
        ),
        setSignModalCallback: bindActionCreators(
            setSignModalCallback,
            dispatch
        ),
        setDocumentForSign: bindActionCreators(setDocumentForSign, dispatch),
        fetchNewStatus: bindActionCreators(fetchNewStatus, dispatch),
        setKonturFocusModalData: bindActionCreators(
            setKonturFocusModalData,
            dispatch
        ),
        setKonturFocusModalVisibility: bindActionCreators(
            setKonturFocusModalVisibility,
            dispatch
        ),
        showToast: bindActionCreators(showToast, dispatch),
        closeToast: bindActionCreators(closeToast, dispatch),
        setCancelDocumentModalVisibility: bindActionCreators(
            setCancelDocumentModalVisibility,
            dispatch
        ),
        setCancelDocumentModalData: bindActionCreators(
            setCancelDocumentModalData,
            dispatch
        ),
        setCancelId: bindActionCreators(setCancelId, dispatch),
        selectAccount: bindActionCreators(selectAccount, dispatch),

        fetchNewPayments: bindActionCreators(fetchNewPayments, dispatch),
        fetchErrorPayments: bindActionCreators(fetchErrorPayments, dispatch),
        setMessageData: bindActionCreators(setMessageData, dispatch),
        setBackUrl: bindActionCreators(setBackUrl, dispatch),
        showNotification: bindActionCreators(showNotification, dispatch),
        closeNotification: bindActionCreators(closeNotification, dispatch),
        toggleCalendarWidget: bindActionCreators(
            toggleCalendarWidget,
            dispatch
        ),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PaymentFormContainer);
