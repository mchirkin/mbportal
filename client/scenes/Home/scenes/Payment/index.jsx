import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';

import setPaymentModalVisibility from 'modules/payments/payment_modal/actions/set_visibility';

import PaymentFormContainer from './containers/PaymentFormContainer';


function mapStateToProps(state) {
    return {
        isVisible: state.payments.paymentModal.isVisible,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        setPaymentModalVisibility: bindActionCreators(setPaymentModalVisibility, dispatch),
    };
}

let historyListenerWasAdded = false;

@withRouter
@connect(mapStateToProps, mapDispatchToProps)
export default class PaymentScene extends Component {
    static propTypes = {
        location: PropTypes.object.isRequired,
        history: PropTypes.object.isRequired,
        accounts: PropTypes.array,
        isVisible: PropTypes.bool,
        setPaymentModalVisibility: PropTypes.func.isRequired,
    };

    static defaultProps = {
        accounts: null,
        isVisible: false,
    };

    componentDidMount() {
        console.log('[PaymentScene] didMount', this.props);

        const { history } = this.props;
        if (!historyListenerWasAdded) {
            historyListenerWasAdded = true;

            history.listen((location) => {
                console.log(location);
                if (location.pathname === '/home/payment') {
                    this.props.setPaymentModalVisibility(true);
                    history.replace('/home');
                }
                if (location.pathname === '/home/invoices') {
                    setTimeout(() => {
                        this.props.setPaymentModalVisibility(false);
                    }, 500);
                }
            });
        }
    }

    render() {
        const {
            location,
            accounts,
            isVisible,
        } = this.props;

        const match = location.pathname === '/home/payment';

        const overScene = [
            '/home/sign_payments',
            '/home/error_payments',
            '/home/send_payments'
        ].find(path => location.pathname === path);

        return (
            accounts
                ? (
                    <div
                        style={{
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            zIndex: overScene ? 2500 : 2100,
                            width: '100%',
                            height: '100%',
                            padding: 15,
                            paddingTop: 0,
                            backgroundColor: '#f1f5f8',
                            opacity: match || isVisible ? 1 : 0,
                            pointerEvents: match || isVisible ? 'all' : 'none',
                            transition: 'opacity 0.5s ease',
                        }}
                    >
                        <PaymentFormContainer
                            match={match}
                        />
                    </div>
                )
                : null
        );
    }
}
