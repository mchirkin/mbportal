import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import fetchErrorPayments from 'modules/payments/error_payments/actions/fetch';
import setPaymentModalData from 'modules/payments/payment_modal/actions/set_data';
import setPaymentModalVisibility from 'modules/payments/payment_modal/actions/set_visibility';
import setBackUrl from 'modules/settings/actions/set_back_url';

import { fetch } from 'utils';

import SceneWrapper from 'components/SceneWrapper';

import PaymentList from './components/PaymentList';

class ErrorPayments extends Component {
    static propTypes = {
        setBackUrl: PropTypes.func.isRequired,
        setPaymentModalData: PropTypes.func.isRequired,
        errorPayments: PropTypes.array,
    };

    static defaultProps = {
        errorPayments: [],
    };

    static contextTypes = {
        router: PropTypes.object.isRequired,
    };

    constructor(props) {
        super(props);

        this.state = {
            selectedItems: [],
        };
    }

    selectItem = (item) => {
        const hasItem = this.state.selectedItems.find(el => el.platporDocument.id === item.platporDocument.id);

        if (hasItem) {
            this.setState({
                selectedItems: this.state.selectedItems.filter(el => el.platporDocument.id !== item.platporDocument.id),
            });
        } else {
            this.setState({
                selectedItems: [...this.state.selectedItems, item],
            });
        }
    }

    selectAll = () => {
        this.setState({
            selectedItems: this.props.errorPayments.filter(el => el.viewed === 'false'),
        });
    }

    clearAll = () => {
        this.setState({
            selectedItems: [],
        });
    }

    viewPayment = (data) => {
        let dataForView = data;
        if (data.platporDocument) {
            dataForView = data.platporDocument;
        }

        this.props.setBackUrl('/home/error_payments');

        this.props.setPaymentModalData(Object.assign(dataForView, {
            income: !data.platporDocument, // || check line_is_debet
            sendViewedRequest: true,
        }));

        this.props.setPaymentModalVisibility(true);

        // this.context.router.history.push('/home/payment');
    }

    /**
     * Обработчик нажатия кнопки "Просмотрено" у поручения в статусе "Ошибка"
     * @param {string} id идентификатор документа
     */
    setViewed = (id) => {
        fetch('/api/v1/documents/set_viewed', {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify({
                docId: id,
            }),
        }).then((response) => {
            console.log(response);
            if (response.ok) {
                return {};
            }
        }).then((data) => {
            this.props.fetchErrorPayments();
        }).catch((err) => {
            console.log(err);
        });
    }

    render() {
        return (
            <SceneWrapper>
                <PaymentList
                    list={this.props.errorPayments}
                    selectItem={this.selectItem}
                    viewPayment={this.viewPayment}
                    selectAll={this.selectAll}
                    clearAll={this.clearAll}
                    setViewed={this.setViewed}
                    {...this.state}
                />
            </SceneWrapper>
        );
    }
}

function mapStateToProps(state) {
    return {
        errorPayments: state.payments.errorPayments.data,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        fetchErrorPayments: bindActionCreators(fetchErrorPayments, dispatch),
        setPaymentModalData: bindActionCreators(setPaymentModalData, dispatch),
        setPaymentModalVisibility: bindActionCreators(setPaymentModalVisibility, dispatch),
        setBackUrl: bindActionCreators(setBackUrl, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ErrorPayments);
