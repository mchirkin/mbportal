import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { formatNumber } from 'utils';
import Btn from 'components/ui/Btn';
import styles from './payment-list.css';

function getDocumentWord(number) {
    let word = 'документов';

    if (number % 10 === 1) {
        word = 'документ';
    }

    if (number % 10 > 1 && number % 10 < 5) {
        word = 'документа';
    }

    if (number > 9 && number < 20) {
        word = 'документов';
    }

    return word;
}

export default class PaymentList extends Component {
    static propTypes = {
        list: PropTypes.array,
        viewPayment: PropTypes.func.isRequired,
        selectAll: PropTypes.func.isRequired,
        clearAll: PropTypes.func.isRequired,
        setViewed: PropTypes.func.isRequired,
        selectItem: PropTypes.func.isRequired,
        selectedItems: PropTypes.array,
    }

    static defaultProps = {
        list: [],
        selectedItems: [],
    }

    render() {
        const paymentList = this.props.list ? this.props.list.filter(p => p.viewed === 'false') : [];

        return (
            <div>
                <div className={styles.header}>
                    Документы с ошибкой <span className={styles['doc-amount']}>{paymentList.length}</span>
                </div>
                {paymentList.length > 0
                    ? (
                        <div className={styles['select-actions']}>
                            <div onClick={this.props.selectAll}>
                                Выбрать все
                            </div>
                            <div onClick={this.props.clearAll}>
                                Снять выбор
                            </div>
                        </div>
                    )
                    : null}
                <div>
                    {paymentList
                        .sort((a, b) => {
                            const aDate = new Date(a.platporDocument.createStamp);
                            const bDate = new Date(b.platporDocument.createStamp);
                            return bDate.getTime() - aDate.getTime();
                        })
                        .map((el, i) => (
                            <div
                                className={styles['list-item']}
                                onClick={() => { this.props.viewPayment(el); }}
                                key={i}
                            >
                                <div className={styles['info-item']} style={{ width: 100, minWidth: 100 }}>
                                    <div>
                                        № {el.platporDocument.docNumber}
                                    </div>
                                    <div style={{ fontSize: 12 }}>
                                        {/* /(\d+)\-(\d+)\-(\d+)/ */}
                                        {el.platporDocument.docDate.replace(/(\d+)-(\d+)-(\d+)/, '$3.$2.$1')}
                                    </div>
                                </div>
                                <div
                                    className={`${styles['info-item']} ${styles['info-item-desc']}`}
                                    style={{ flexGrow: 1 }}
                                >
                                    <div>
                                        {el.platporDocument.corrFullname}
                                    </div>
                                    <div className={styles['payment-description']}>
                                        {el.platporDocument.description}
                                    </div>
                                </div>
                                <div className={`${styles['info-item']} ${styles['info-item-amount']}`}>
                                    <div>
                                        <span className={styles.minus}>
                                            {`- ${formatNumber(el.platporDocument.amount)} \u20bd`}
                                        </span>
                                    </div>
                                    <div>
                                        Основной счет
                                    </div>
                                </div>
                                <div className={styles.actions}>
                                    <Btn
                                        caption="Просмотрено"
                                        onClick={(e) => {
                                            e.stopPropagation();
                                            this.props.setViewed(el.platporDocument.id);
                                        }}
                                        bgColor="white"
                                        style={{
                                            width: 190,
                                            height: 30,
                                            color: '#979797',
                                            fontSize: 12,
                                            border: 'solid 1px #979797',
                                            borderRadius: 4,
                                        }}
                                        loaderColor="linear-gradient(121deg, #09357e, #e72e92)"
                                    />
                                    <div
                                        className={styles['item-checkbox']}
                                        onClick={(e) => {
                                            e.stopPropagation();
                                            this.props.selectItem(el);
                                        }}
                                        data-active={!!this.props.selectedItems.find(item =>
                                            el.platporDocument.id === item.platporDocument.id,
                                        )}
                                    >
                                        <div className={styles['item-checkbox-inner']} />
                                    </div>
                                </div>
                            </div>
                        ))}
                </div>
                {paymentList.length > 0 && this.props.selectedItems.length > 0
                    ? (
                        <Btn
                            caption={`
                                Пометить просмотренным
                                ${this.props.selectedItems.length}
                                ${getDocumentWord(this.props.selectedItems.length)}
                            `}
                            onClick={(e) => {
                                e.stopPropagation();
                                this.props.selectedItems.forEach(el => this.props.setViewed(el.platporDocument.id));
                            }}
                            style={{
                                width: 380,
                                'margin-left': 'auto',
                                'margin-right': 35,
                                'margin-top': 50,
                                'margin-bottom': 80,
                            }}
                        />
                    )
                    : null}
            </div>
        );
    }
}
