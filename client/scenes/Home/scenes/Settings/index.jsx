import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Switch, Route } from 'react-router-dom';
import SceneWrapper from 'components/SceneWrapper';
import Profile from 'scenes/Home/scenes/Settings/scenes/Profile';
import Tarif from 'scenes/Home/scenes/Settings/scenes/Tarif';
import Certificates from 'scenes/Home/scenes/Settings/scenes/Certificates';


class Settings extends Component {
    static contextTypes = {
        router: PropTypes.object.isRequired,
    };

    componentDidMount() {
        // if (document.location.href === '/')
        // this.context.router.history.push('/home/settings/profile');
    }

    render() {
        return (
            <SceneWrapper>
                <Switch>
                    <Route exact path="/home/settings/profile" component={Profile} />
                    <Route exact path="/home/settings/tarif" component={Tarif} />
                    <Route exact path="/home/settings/certificates" component={Certificates} />
                </Switch>
            </SceneWrapper>
        );
    }
}

export default Settings;
