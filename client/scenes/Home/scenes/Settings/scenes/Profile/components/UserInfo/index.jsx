import React from 'react';
import PropTypes from 'prop-types';
import styles from './user-info.css';

const UserInfo = ({ data }) => {
    if (!data) {
        return null;
    }

    return (
        <div className={styles.wrapper}>
            <div className={styles.photo}>
                <div className={styles.avatar} />
            </div>
            <div className={styles.data}>
                <div className={styles.fio}>
                    {`${data.surname} ${data.name} ${data.patronymic}`}
                </div>
                <div className={styles.position}>
                    {data.employeePosition}
                </div>
                <div className={styles.phone}>
                    {data.phoneNumber.replace(/\+7(\d{3})(\d{3})(\d{2})(\d{2})/, '+7 ($1) $2-$3-$4')}
                </div>
            </div>
        </div>
    );
};

UserInfo.propTypes = {
    data: PropTypes.object,
};

UserInfo.defaultProps = {
    data: {},
};

export default UserInfo;
