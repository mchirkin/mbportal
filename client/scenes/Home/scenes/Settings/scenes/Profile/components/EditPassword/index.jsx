import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Btn from 'components/ui/Btn';
import styles from './edit-password.css';

export default class EditPassword extends Component {
    static propTypes = {
        newPassword: PropTypes.string,
        confirmNewPassword: PropTypes.string,
        changePassword: PropTypes.func.isRequired,
        passwordError: PropTypes.string,
        clearPassword: PropTypes.func.isRequired,
        currentPassword: PropTypes.string,
        handlePasswordChange: PropTypes.func.isRequired,
    }

    static defaultProps = {
        newPassword: '',
        confirmNewPassword: '',
        changePassword: '',
        passwordError: '',
        currentPassword: '',
    }


    constructor(props) {
        super(props);

        this.state = {
            editable: false,
        };
    }

    toggleEditable = () => {
        this.setState({
            editable: !this.state.editable,
        });
    }

    cancelEdit = () => {
        this.setState({
            editable: false,
        });
        this.props.clearPassword();
    }

    render() {
        const expandedChangePassword = (
            <div className={styles['expanded-wrapper']}>
                <div className={styles['password-input']}>
                    <div className={styles.caption}>
                        Текущий пароль
                    </div>
                    <div className={styles['input-wrapper']}>
                        <input
                            id="currentPassword"
                            className={styles['login-input']}
                            type="password"
                            value={this.props.currentPassword}
                            onChange={this.props.handlePasswordChange}
                            data-active={this.state.editable}
                        />
                    </div>
                </div>

                <div className={styles['password-input']}>
                    <div className={styles.caption}>
                        Новый пароль
                    </div>
                    <div className={styles['input-wrapper']}>
                        <input
                            id="newPassword"
                            className={styles['login-input']}
                            type="password"
                            value={this.props.newPassword}
                            onChange={this.props.handlePasswordChange}
                            data-active={this.state.editable}
                        />
                    </div>
                </div>

                <div>
                    <div className={styles.caption}>
                        Подтвердите пароль
                    </div>
                    <div className={styles['input-wrapper']}>
                        <input
                            id="confirmNewPassword"
                            className={styles['login-input']}
                            type="password"
                            value={this.props.confirmNewPassword}
                            onChange={this.props.handlePasswordChange}
                            data-active={this.state.editable}
                        />
                        <div className={styles['btn-group']}>
                            <Btn
                                caption={
                                    <div
                                        style={{
                                            position: 'relative',
                                            top: 0,
                                        }}
                                    >
                                        &times;
                                    </div>
                                }
                                bgColor="white"
                                onClick={this.cancelEdit}
                                style={{
                                    height: 30,
                                    width: 30,
                                    marginRight: 20,
                                    padding: 0,
                                    fontSize: 26,
                                    lineHeight: 1,
                                    borderRadius: 4,
                                }}
                            />
                            <Btn
                                caption="Подтвердить"
                                onClick={this.props.changePassword}
                                style={{
                                    height: 30,
                                    fontSize: 12,
                                    borderRadius: 4,
                                }}
                            />
                        </div>
                    </div>
                </div>
            </div>
        );

        return (
            <div className={styles.wrapper}>
                {!this.state.editable
                    ? (
                        <div>
                            <div className={styles.caption}>
                                Пароль
                            </div>
                            <div className={styles['input-wrapper']}>
                                <input
                                    className={styles['login-input']}
                                    value={'*************'}
                                    disabled={!this.state.editable}
                                />
                                <div>
                                    <div
                                        className={styles['edit-btn']}
                                        onClick={this.toggleEditable}
                                    />
                                </div>
                            </div>
                        </div>
                    )
                    : expandedChangePassword}
                <div className={styles['password-error']}>
                    {this.props.passwordError}
                </div>
            </div>
        );
    }
}
