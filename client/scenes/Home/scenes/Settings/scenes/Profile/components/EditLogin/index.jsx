import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Btn from 'components/ui/Btn';
import styles from './edit-login.css';

export default class EditLogin extends Component {
    static propTypes = {
        login: PropTypes.string,
        handleLoginChange: PropTypes.func.isRequired,
        changeLogin: PropTypes.func.isRequired,
        loginError: PropTypes.string,
    }

    static defaultProps = {
        login: '',
        loginError: '',
    }

    constructor(props) {
        super(props);

        this.state = {
            editable: false,
        };
    }

    toggleEditable = () => {
        this.setState({
            editable: !this.state.editable,
        });
    }

    cancelEdit = () => {
        this.setState({
            editable: false,
        });
        this.props.handleLoginChange({
            target: {
                value: localStorage.getItem('login'),
            },
        });
    }

    render() {
        return (
            <div className={styles.wrapper}>
                <div className={styles.caption}>
                    Логин
                </div>
                <div className={styles['input-wrapper']}>
                    <input
                        className={styles['login-input']}
                        value={this.props.login}
                        onChange={this.props.handleLoginChange}
                        disabled={!this.state.editable}
                        data-active={this.state.editable}
                    />
                    {this.state.editable
                        ? (
                            <div className={styles['btn-group']}>
                                <Btn
                                    caption={
                                        <div
                                            style={{
                                                position: 'relative',
                                                top: 0,
                                            }}
                                        >
                                            &times;
                                        </div>
                                    }
                                    bgColor="white"
                                    onClick={this.cancelEdit}
                                    style={{
                                        height: 30,
                                        width: 30,
                                        marginRight: 20,
                                        padding: 0,
                                        fontSize: 26,
                                        lineHeight: 1,
                                        borderRadius: 4,
                                    }}
                                />
                                <Btn
                                    caption="Подтвердить"
                                    onClick={this.props.changeLogin}
                                    style={{
                                        height: 30,
                                        fontSize: 12,
                                        borderRadius: 4,
                                    }}
                                />
                            </div>
                        )
                        : (
                            <div>
                                <div
                                    className={styles['edit-btn']}
                                    onClick={this.toggleEditable}
                                />
                            </div>
                        )}
                </div>
                <div className={styles['login-error']}>
                    {this.props.loginError}
                </div>
            </div>
        );
    }
}
