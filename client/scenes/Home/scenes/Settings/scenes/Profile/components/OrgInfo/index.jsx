import React from 'react';
import PropTypes from 'prop-types';
import styles from './org-info.css';

const OrgInfoItem = ({ caption, value }) => (
    <div className={styles['item-wrapper']}>
        <div className={styles['item-caption']}>
            {caption}
        </div>
        <div className={styles['item-value']}>
            {value}
        </div>
    </div>
);

OrgInfoItem.propTypes = {
    caption: PropTypes.string,
    value: PropTypes.string,
};

OrgInfoItem.defaultProps = {
    caption: '',
    value: '',
};

const AccountsInfo = ({ accounts }) => {
    if (!accounts) {
        return null;
    }

    return (
        <div className={styles['item-wrapper']}>
            <div className={styles['item-caption']}>
                Расчетные счета
            </div>
            <div className={styles['item-value']}>
                {accounts
                    .filter(acc => acc.currency.toUpperCase() === 'RUR' || acc.currency.toUpperCase() === 'RUB')
                    .map(acc => (
                        <div
                            key={acc.id}
                            className={styles['item-account']}
                        >
                            {acc.formattedNumber}
                        </div>
                    ))}
            </div>
        </div>
    );
};

AccountsInfo.propTypes = {
    accounts: PropTypes.array,
};

AccountsInfo.defaultProps = {
    accounts: [],
};

const OrgInfo = ({ data, accounts }) => {
    if (!data) {
        return null;
    }

    return (
        <div className={styles.wrapper}>
            <OrgInfoItem
                key="fullName"
                caption="Наименование организации"
                value={data.fullName}
            />
            <OrgInfoItem
                key="INN"
                caption="ИНН"
                value={data.INN}
            />
            {data.KPP
                ? (
                    <OrgInfoItem
                        key="KPP"
                        caption="КПП"
                        value={data.KPP}
                    />
                )
                : null}
            <AccountsInfo
                accounts={accounts}
            />
        </div>
    );
};

OrgInfo.propTypes = {
    accounts: PropTypes.array,
    data: PropTypes.object,
};

OrgInfo.defaultProps = {
    accounts: [],
    data: {},
};

export default OrgInfo;
