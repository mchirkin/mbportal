import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { fetch } from 'utils';

import setMessageData from 'modules/info_message/actions/set_data';
import fetchPasswordRestrictions from 'modules/settings/actions/password_restrictions/fetch';

import BlockLoader from 'components/BlockLoader';

import EditLogin from '../../components/EditLogin';
import EditPassword from '../../components/EditPassword';

class ChangeAuthData extends Component {
    static propTypes = {
        passwordRestrictions: PropTypes.object,
        fetchPasswordRestrictions: PropTypes.func.isRequired,
        setMessageData: PropTypes.func.isRequired,
    }

    static defaultProps = {
        passwordRestrictions: {},
    }

    constructor(props) {
        super(props);

        this.state = {
            login: localStorage.getItem('login'),
            currentPassword: '',
            newPassword: '',
            confirmNewPassword: '',
            isFetching: false,
            loginError: '',
            passwordError: '',
        };
    }

    componentWillMount() {
        if (this.props.passwordRestrictions === null) {
            this.props.fetchPasswordRestrictions();
        }
    }

    handleLoginChange = (e) => {
        this.setState({
            login: e.target.value,
            loginError: '',
        });
    }

    handlePasswordChange = (e) => {
        const id = e.target.getAttribute('id');
        this.setState({
            [id]: e.target.value,
            passwordError: '',
        });
    }

    clearPassword = () => {
        this.setState({
            currentPassword: '',
            newPassword: '',
            confirmNewPassword: '',
            passwordError: '',
        });
    }

    changeLogin = () => {
        if (!this.state.login) {
            this.setState({
                loginError: 'Не указан логин',
            });
            return false;
        }

        this.setState({
            isFetching: true,
        });

        fetch('/api/v1/change_login', {
            credentials: 'include',
            method: 'POST',
            body: JSON.stringify({
                newLogin: this.state.login,
            }),
        })
            .then(response => response.text())
            .then((text) => {
                if (text.length === 0) {
                    this.props.setMessageData({
                        isVisible: true,
                        header: 'Успешно!',
                        body: 'Логин изменен',
                        btnText: 'Далее',
                        callback: () => this.props.setMessageData({ isVisible: false }),
                    });
                    localStorage.setItem('login', this.state.login);
                    this.editLogin.toggleEditable();
                } else {
                    const json = JSON.parse(text);
                    return Promise.reject(json.errorText || 'Произошла ошибка при смене логина');
                }
                this.setState({
                    isFetching: false,
                });
            })
            .catch((err) => {
                this.setState({
                    isFetching: false,
                    loginError: err || 'Произошла ошибка при смене логина',
                });
            });
    }

    changePassword = () => {
        if (this.state.isFetching) {
            return;
        }

        if (
            !this.state.currentPassword ||
            !this.state.newPassword ||
            !this.state.confirmNewPassword
        ) {
            this.setState({
                passwordError: 'Заполните все поля',
            });
            return false;
        }

        if (
            this.state.newPassword !== this.state.confirmNewPassword
        ) {
            this.setState({
                passwordError: 'Введенные пароли не совпадают',
            });
            return false;
        }

        const checkPswd = this.checkPasswordForRestrictions(this.state.newPassword);
        if (checkPswd.errorText.length > 0) {
            this.setState({
                passwordError: checkPswd.errorText,
            });
            return false;
        }

        this.setState({
            isFetching: true,
        });

        fetch('/api/v1/change_password', {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify({
                currentPassword: this.state.currentPassword,
                newPassword: this.state.newPassword,
            }),
        }).then((response) => {
            if (response.status === 200) {
                return {};
            }
            return response.json();
        }).then((json) => {
            if (!json.errorText) {
                this.props.setMessageData({
                    isVisible: true,
                    header: 'Успешно!',
                    body: 'Пароль изменен',
                    btnText: 'Далее',
                    callback: () => this.props.setMessageData({ isVisible: false }),
                });

                this.setState({
                    currentPassword: '',
                    newPassword: '',
                    confirmNewPassword: '',
                    passwordError: '',
                });

                // this.profileChangePassword.setPasswordEditable(false);
                this.editPassword.cancelEdit();
            } else {
                let errorText = json.errorText || 'Произошла ошибка при смене пароля';
                if (json.errorCode === 1010) {
                    errorText = 'Неверный текущий пароль';
                }
                this.setState({
                    passwordError: errorText,
                });
            }
            this.setState({
                isFetching: false,
            });
        }).catch((err) => {
            console.log(err);
            this.setState({
                isFetching: false,
            });
        });
    }

    checkPasswordForRestrictions = (pswd) => {
        if (!this.props.passwordRestrictions) {
            return {
                errorText: '',
            };
        }

        const requiredPswdLength = this.props.passwordRestrictions.passwordMinLength
            ? parseInt(this.props.passwordRestrictions.passwordMinLength, 10)
            : 1;
        if (pswd.length < requiredPswdLength) {
            return {
                errorText: `Пароль должен быть не менее ${requiredPswdLength} знаков`,
            };
        }

        if (this.props.passwordRestrictions.usePasswordDifferentRegister === 'true') {
            if (
                pswd === pswd.toLowerCase() ||
                pswd === pswd.toUpperCase() ||
                !/[А-Яа-яA-Za-z]/.test(pswd)
            ) {
                return {
                    errorText: 'В пароле должны использоваться строчные и прописные буквы.',
                };
            }
        }

        if (this.props.passwordRestrictions.usePasswordDigits === 'true') {
            if (!/\d/.test(pswd)) {
                return {
                    errorText: 'В пароле должна присутствовать хотя бы одна цифра.',
                };
            }
        }
        if (/[А-Яа-я]/.test(pswd)) {
            return {
                errorText: 'В пароле должны отсутствовать символы кириллицы.',
            };
        }

        return {
            errorText: '',
        };
    }

    render() {
        return (
            <div>
                <BlockLoader isFetching={this.state.isFetching} />
                <EditLogin
                    {...this.state}
                    handleLoginChange={this.handleLoginChange}
                    changeLogin={this.changeLogin}
                    ref={(el) => { this.editLogin = el; }}
                />
                <EditPassword
                    {...this.state}
                    handlePasswordChange={this.handlePasswordChange}
                    changePassword={this.changePassword}
                    clearPassword={this.clearPassword}
                    ref={(el) => { this.editPassword = el; }}
                />
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        passwordRestrictions: state.settings.passwordRestrictions.data,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        setMessageData: bindActionCreators(setMessageData, dispatch),
        fetchPasswordRestrictions: bindActionCreators(fetchPasswordRestrictions, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ChangeAuthData);
