import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ChangeAuthData from './containers/ChangeAuthData';
import UserInfo from './components/UserInfo';
import OrgInfo from './components/OrgInfo';

const Profile = ({ userInfo, orgInfo, accounts }) => (
    <div style={{ padding: '23.5px 25px' }}>
        <UserInfo data={userInfo} />
        <ChangeAuthData />
        <OrgInfo
            data={orgInfo}
            accounts={accounts}
        />
    </div>
);

Profile.propTypes = {
    userInfo: PropTypes.object,
    orgInfo: PropTypes.object,
    accounts: PropTypes.array,
};

Profile.defaultProps = {
    userInfo: {},
    orgInfo: {},
    accounts: [],
};

function mapStateToProps(state) {
    return {
        userInfo: state.user.userInfo.data,
        orgInfo: state.user.orgInfo.data,
        accounts: state.accounts.data,
    };
}

export default connect(mapStateToProps)(Profile);
