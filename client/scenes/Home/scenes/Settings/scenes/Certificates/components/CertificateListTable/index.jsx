import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Scrollbars } from 'react-custom-scrollbars';
import { dates } from 'utils';
import RegenerationButton from '../../containers/RegenerationButton';
import styles from './certificates.css';

/**
 * Таблица со списком сертификатов
 */
export default class CertificateListTable extends Component {
    static propTypes = {
        /** Список сертификатов пользователя */
        certificates: PropTypes.array,
        importOnToken: PropTypes.func.isRequired,
    };

    static defaultProps = {
        certificates: [],
    };

    render() {
        // собираем все сертификаты для подстановки в таблицу
        let rows;
        if (this.props.certificates && this.props.certificates.length > 0) {
            const sortCertificates = this.props.certificates.sort((a, b) => a.isTemporary < b.isTemporary);

            rows = sortCertificates.map(cert => (
                <tr className={styles['table-row']} key={cert.id}>
                    <td data-field="id" data-name="ID">
                        {cert.id}
                    </td>
                    <td data-field="subject" data-name="Владелец сертификата">
                        {cert.subjectName}
                    </td>
                    <td data-field="date" data-name="Дата начала">
                        {cert.validFrom && dates.getDateString(new Date(cert.validFrom)) || '-'}
                    </td>
                    <td data-field="end-date" data-name="Дата и время окончания">
                        {cert.validTo && (new Date(cert.validTo)).toLocaleString().replace(/,/g, '\n') || '-'}
                    </td>
                    <td data-field="status" data-name="Статус">
                        {(() => {
                            switch (cert.status) {
                                case 'new':
                                    return 'Новый';
                                case 'imported':
                                    return 'Импортирован';
                                case 'activated':
                                    return 'Активирован';
                                default:
                                    return cert.status;
                            }
                        })()}
                    </td>
                    <td
                        data-field="type"
                        data-type-value="true"
                        data-name="Тип сертификата"
                    >
                        {cert.isTemporary === 'false' ? 'Рабочий' : 'Транспортный'}
                    </td>
                    <td>
                        <div className={styles['action-btn-group']}>
                            {cert.cryptoTypeCode.toLowerCase() !== 'input_code'
                                ? [
                                    <div
                                        className={styles['import-on-token']}
                                        title="Импортировать на токен"
                                        onClick={() => { this.props.importOnToken(cert); }}
                                        data-disabled={cert.isTemporary === 'true'}
                                        data-import-btn="true"
                                    />,
                                    <RegenerationButton
                                        certificate={cert}
                                        isTemporary={cert.isTemporary === 'true'}
                                        welcomeTourClose={this.props.welcomeTourClose}
                                    />,
                                ]
                                : null
                            }
                        </div>
                    </td>
                </tr>
            ));
        }

        return (
            <div className={styles['certificates-list-table']}>
                <h3 className={styles.header}>
                    Мои сертификаты
                </h3>
                {this.props.certificates && this.props.certificates.length > 0
                    ? (
                        <div style={{ paddingBottom: 20 }}>
                            <Scrollbars
                                style={{ width: '100%' }}
                                autoHeight
                                autoHeightMax={'100%'}
                            >
                                <table className={styles['certificates-list']}>
                                    <thead className={styles['table-header']}>
                                        <tr>
                                            <td data-field="id">
                                                ID
                                            </td>
                                            <td data-field="subject">
                                                Владелец сертификата
                                            </td>
                                            <td data-field="date">
                                                Дата начала
                                            </td>
                                            <td data-field="end-date">
                                                Дата и время окончания
                                            </td>
                                            <td data-field="status">
                                                Статус
                                            </td>
                                            <td data-field="type">
                                                Тип сертификата
                                            </td>
                                            <td />
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {rows}
                                    </tbody>
                                </table>
                            </Scrollbars>
                        </div>
                    )
                    : (
                        <h3>Нет доступных сертификатов</h3>
                    )}
            </div>
        );
    }
}
