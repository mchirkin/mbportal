import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Btn from 'components/ui/Btn';
import InputGroup from 'components/ui/InputGroup';
import Loader from 'components/Loader';

import styles from './import-certificate-pin-modal.css';

export default class ImportCertificatePinModal extends Component {
    static propTypes = {
        isVisible: PropTypes.bool,
        closeImportPinModal: PropTypes.func.isRequired,
        pinValue: PropTypes.string,
        handlePinChange: PropTypes.func.isRequired,
        submitPin: PropTypes.func.isRequired,
        importInProgress: PropTypes.bool,
    }

    static defaultProps = {
        isVisible: false,
        pinValue: '',
        importInProgress: false,
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.isVisible && !this.props.isVisible) {
            setTimeout(() => {
                this.input.setFocusOnInput();
                this.wrapper.setAttribute('data-fade', true);
            }, 0);
        }
    }

    closeModal = () => {
        this.wrapper.setAttribute('data-fade', false);
        setTimeout(() => {
            this.props.closeImportPinModal();
        }, 500);
    }

    render() {
        const {
            isVisible,
            pinValue,
            handlePinChange,
            submitPin,
            importInProgress,
        } = this.props;

        return (
            <div
                className={styles.wrapper}
                data-visible={isVisible}
                ref={(wrapper) => { this.wrapper = wrapper; }}
            >
                <Loader visible={importInProgress} />
                <form onSubmit={submitPin}>
                    <div className={styles.header}>
                        Введите пин-код от токена
                    </div>
                    <div>
                        <InputGroup
                            type="password"
                            value={pinValue}
                            onChange={handlePinChange}
                            ref={(el) => { this.input = el; }}
                        />
                    </div>
                    <div className={styles['btn-row']}>
                        <Btn
                            bgColor="grey"
                            caption="&times;"
                            onClick={this.closeModal}
                            style={{
                                width: 46,
                                marginRight: 20,
                                fontSize: 34,
                            }}
                        />
                        <Btn
                            caption="Подтвердить"
                            onClick={submitPin}
                        />
                    </div>
                </form>
            </div>
        );
    }
}
