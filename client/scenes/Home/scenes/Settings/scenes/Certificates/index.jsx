import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { fetch } from 'utils';
import importCertificate from 'utils/rutoken/import_certificate';

import fetchCertificates from 'modules/crypto/actions/certificates/fetch';
import fetchRegen from 'modules/crypto/actions/regen/fetch';
import setMessageData from 'modules/info_message/actions/set_data';
import setCertificateTourVisible from 'modules/settings/actions/welcome_tour/set_visibility';

import WelcomeTour from 'components/WelcomeTour';

import CertificateListTable from './components/CertificateListTable';
import ImportCertificatePinModal from './components/ImportCertificatePinModal';

class Certificates extends Component {
    static propTypes = {
        fetchCertificates: PropTypes.func.isRequired,
        fetchRegen: PropTypes.func.isRequired,
        setMessageData: PropTypes.func.isRequired,
        isTourVisible: PropTypes.bool,
        setCertificateTourVisible: PropTypes.func.isRequired,
    }

    static defaultProps = {
        isTourVisible: false,
    }

    constructor(props) {
        super(props);

        this.state = {
            isImportPinModalVisible: false,
            certificateToImport: null,
            pinValue: '',
            importInProgress: false,
        };
    }

    componentWillMount() {
        this.props.fetchCertificates();
        this.props.fetchRegen();
    }

    showImportPinModal = () => {
        this.setState({
            isImportPinModalVisible: true,
        });
    }

    closeImportPinModal = () => {
        this.setState({
            isImportPinModalVisible: false,
        });
    }

    importOnToken = (cert) => {
        this.setState({
            certificateToImport: cert,
        }, () => {
            console.log('Certificate to import: ', this.state.certificateToImport);
            this.certificateToImport = cert;
            this.showImportPinModal();
        });
    }

    handlePinChange = (e) => {
        const value = e.target.value;

        this.setState({
            pinValue: value,
        }, () => {
            /*
            if (value.length === 9) {
                this.submitPin();
            }
            */
        });
    }

    /**
     * Обработчик подтверждения ввода пин-кода
     * выполняется следующие действия:
     * получение данных сертификата -> импорт сертификата на токен -> вызов сервиса активации сертификата
     * @param {Event} e
     */
    submitPin = (e) => {
        if (e) {
            e.preventDefault();
        }

        this.setState({
            importInProgress: true,
        });

        const certificateToImport = this.state.certificateToImport || this.certificateToImport;
        console.log('Certificate to import after pin submit: ', certificateToImport);

        this.getCertificateData(certificateToImport.id)
            .then(data => importCertificate(this.state.pinValue, data))
            .then(() => this.activateCertificate(this.state.certificateToImport.id))
            .then((result) => {
                if (result) {
                    this.props.setMessageData({
                        isVisible: true,
                        header: 'Успешно!',
                        body: 'Сертификат успешно импортирован на устройство',
                        btnText: 'Далее',
                        callback: () => this.props.setMessageData({ isVisible: false }),
                    });

                    this.setState({
                        isImportPinModalVisible: false,
                        pinValue: '',
                        certificateToImport: null,
                        importInProgress: false,
                    });

                    this.props.fetchCertificates();
                } else {
                    throw Error('Произошла ошибка при импорте сертификата');
                }
            })
            .catch((err) => {
                global.console.log(err);
                let errorMessage;
                if (err.message || typeof err === 'number') {
                    switch (err.message || err.toString()) {
                        case '3':
                            global.console.log('Устройство не найдено');
                            errorMessage = 'Устройство не найдено';
                            break;
                        case '6':
                            global.console.log('Сертификат уже записан на устройство');
                            errorMessage = 'Сертификат уже записан на устройство';
                            break;
                        case '19':
                            global.console.log('Неверный пин-код');
                            errorMessage = 'Неверный пин-код';
                            break;
                        case '20':
                            global.console.log('Ключевая пара не найдена');
                            errorMessage = 'Ключевая пара не найдена';
                            break;
                        case 'activate':
                            global.console.log('Произошла ошибка при активации сертификата');
                            errorMessage = 'Произошла ошибка при активации сертификата';
                            break;
                        default:
                            global.console.log('default err');
                            errorMessage = 'Произошла ошибка при импорте сертификата';
                            break;
                    }
                }
                if (err.errorText) {
                    errorMessage = err.errorText;
                }

                this.props.setMessageData({
                    isVisible: true,
                    header: 'Внимание!',
                    body: errorMessage,
                    btnText: 'Назад',
                    callback: () => this.props.setMessageData({ isVisible: false }),
                });

                this.setState({
                    isImportPinModalVisible: false,
                    pinValue: '',
                    certificateToImport: null,
                    importInProgress: false,
                });
            });
    }

    /**
     * Получение бинарных данных сертификата из iSimple
     * @param {string|number} id - идентификатор сертификата в iSimple
     * @return {Promise}
     */
    getCertificateData = id => fetch(`/api/v1/crypto/certificate/${id}`, {
        credentials: 'include',
    })
        .then(response => response.text())
        .then((text) => {
            if (/errorText/.test(text)) {
                const json = JSON.parse(text);
                return Promise.reject(json);
            }
            return text;
        })
        .catch((err) => {
            global.console.log(err);
            return err;
        })

    /**
     * Вызов сервиса установки статуса активирован сертификата
     * @param {string|number} id - id сертификата
     * return {Promise}
     */
    activateCertificate = id => fetch(`/api/v1/crypto/certificate/activate/${id}`, {
        credentials: 'include',
    })
        // даже если произошла ошибка, то возвращаем ок, т.к. импорт прошел успешно
        .then(() => true)

    welcomeTourClose = () => {
        this.props.setCertificateTourVisible(false);
    }

    render() {
        const { isImportPinModalVisible } = this.state;

        const steps = [
            {
                title: `
                    Для перевыпуска сертификата нажмите на эту кнопку и введите PIN от Рутокена.
                    В 95 % случаев перевыпуск происходит в течение 15 минут.
                `,
                selector: '[data-temporary="true"]',
                position: 'left',
                isFixed: true,
                showProgress: true,
                allowClicksThruHole: true,
            },
            {
                title: `
                    После  перевыпуска  статус  сертификата изменится на "Рабочий".
                `,
                selector: '[data-type-value="true"]',
                position: 'left',
                isFixed: true,
                allowClicksThruHole: true,
                style: {
                    width: 500,
                    main: {
                        top: 40,
                    },
                },
            },
            {
                title: `
                    После того, как статус сертификата изменится на «Рабочий» нужно импортировать его на Рутокен.
                     Для этого нажмите на кнопку "Импортировать".
                     Загрузка сертификата на Рутокен произойдет  моментально.
                `,
                selector: '[data-import-btn="true"]',
                position: 'left',
                isFixed: true,
                allowClicksThruHole: true,
                style: {
                    width: 500,
                },
            },
        ];

        return (
            <div
                style={{
                    position: 'relative',
                    width: '100%',
                    height: '100%',
                    padding: '23.5px 25px',
                }}
            >
                <CertificateListTable
                    {...this.props}
                    importOnToken={this.importOnToken}
                    welcomeTourClose={this.welcomeTourClose}
                />
                <ImportCertificatePinModal
                    isVisible={isImportPinModalVisible}
                    closeImportPinModal={this.closeImportPinModal}
                    pinValue={this.state.pinValue}
                    handlePinChange={this.handlePinChange}
                    submitPin={this.submitPin}
                    importInProgress={this.state.importInProgress}
                />
                <WelcomeTour
                    steps={steps}
                    isTourOpen={this.props.isTourVisible}
                    handleClose={this.welcomeTourClose}
                    tooltipOffset={65}
                />
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        certificates: state.crypto.certificates.data,
        regen: state.crypto.regen.data,
        isTourVisible: state.settings.welcomeTour.isVisible,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        fetchCertificates: bindActionCreators(fetchCertificates, dispatch),
        fetchRegen: bindActionCreators(fetchRegen, dispatch),
        setMessageData: bindActionCreators(setMessageData, dispatch),
        setCertificateTourVisible: bindActionCreators(setCertificateTourVisible, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Certificates);
