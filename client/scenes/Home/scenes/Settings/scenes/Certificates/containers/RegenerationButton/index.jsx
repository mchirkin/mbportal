import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { fetch } from 'utils';
import loadPlugin from 'utils/rutoken/load_plugin';
import getCertificates from 'utils/rutoken/get_certificates';
import generateKeyPair from 'utils/rutoken/generate_key_pair';
import { signDocument } from 'utils/rutoken/sign_document';
import importCertificate from 'utils/rutoken/import_certificate';

import fetchRegen from 'modules/crypto/actions/regen/fetch';
import setMessageData from 'modules/info_message/actions/set_data';

import Btn from 'components/ui/Btn';

/**
 * Контейнер кнопки "Запросить перегенерацию"
 */
class RegenerationButton extends Component {
    static propTypes = {
        /** информация о сертификате для перегенерации */
        certificate: PropTypes.object.isRequired,
        isTemporary: PropTypes.bool,
        welcomeTourClose: PropTypes.func.isRequired,
    }

    static defaultProps = {
        isTemporary: false,
    }

    constructor(props) {
        super(props);

        this.state = {
            /** идентификатор документа для перегенерации сертификата */
            docId: null,
            /** идет ли загрузка данных */
            isFetching: false,
            /** показывать поле для ввода пин-кода от токена */
            showPin: false,
            /** значение поля ввода пин-кода */
            pin: '',
            /** данные сертификата на токене */
            tokenCert: null,
        };
    }

    componentWillMount() {
        loadPlugin().then((plugin) => {
            window.rutokenPlugin = plugin;
        });

        if (this.props.regen.data === null) {
            this.props.fetchRegen();
        }
    }

    /**
     * Обработчик нажатия на кнопку "Запросить перегенерацию"
     * Вызываем сервис для создания запроса на перегенерацию в iSimple
     * и записываем полученный идентификатор запроса в state
     */
    handleBtnClick = () => {
        this.setState({
            showPin: true,
        }, () => {
            this.input.focus();
        });
    }

    /**
     * Обработчик изменения значение в поле ввода пин-кода
     * @param {Event} e
     */
    handlePinChange = (e) => {
        const value = e.target.value;

        this.setState({
            pin: value,
        }, () => {
            if (value.length === 9) {
                this.handlePinSubmit();
            }
        });
        this.props.welcomeTourClose();
    }

    /**
     * Обработчик подтверждения ввода пин-кода
     * @param {Event} e
     */
    handlePinSubmit = (e) => {
        if (e) {
            e.preventDefault();
        }

        // выбранный пользователем сертификат
        const selectedCert = this.props.certificate;
        this.setState({
            isFetching: true,
            showPin: false,
        });

        let Pkcs10Request = '';
        let docData = '';
        let docId = '';

        // получаем список сертификатов на токене
        getCertificates()
            .then((certList) => {
                // найден ли выбранный сертификат на токене
                let certFound = false;
                // идентификатор выбранного сертификата на токене
                const certId = null;
                // проходимся по всем сертификатам на токене в поисках выбранного сертификата из iSimple
                certList.forEach((tokenCert) => {
                    if (tokenCert.serialNumber.replace(/\:/g, '').toLowerCase() === selectedCert.hexSerial.toLowerCase()) {
                        console.log('certificate found', tokenCert.serialNumber);
                        this.setState({
                            tokenCert,
                        });
                        certFound = true;
                    }
                });
                return certFound;
            })
            .then((found) => {
                if (!found) {
                    console.log('Сертификат не найден на ключевом носителе');
                    return Promise.reject('Сертификат не найден на ключевом носителе');
                }
                return true;
            })
            .then(result => generateKeyPair(this.state.pin))
            .then(key => this.createPkcs10(key))
            .then((request) => {
                console.log('Pkcs10:', request);
                Pkcs10Request = request;
                // получаем серийный номер токена
                return window.rutokenPlugin.getDeviceInfo(0, window.rutokenPlugin.TOKEN_INFO_SERIAL);
            })
            .then(deviceSerial =>
                // отправляем на создание запрос на перегенерацию в iSimple
                this.createRegenRequest(deviceSerial, Pkcs10Request),
            )
            .then((json) => {
                docId = json.id;
                // получаем бинарные данные созданного запроса для подписи
                return this.getDocumentData(json.id);
            })
            .then((data) => {
                docData = data;
                // подписываем запрос
                return signDocument(this.state.pin, this.state.tokenCert.certId, data);
            })
            .then(signedData =>
                // отправляем на проверку подписанный запрос в iSimple
                this.sendSignedData(docData, signedData, docId),
            )
            .then(result =>
                // отправляем запрос в банк
                this.sendRequestToBank(docId),
            )
            .then((res) => {
                this.setState({
                    isFetching: false,
                    pin: '',
                    showPin: false,
                });
                this.props.fetchRegen();
                /*
                this.props.showModal({
                    isVisible: true,
                    data: {
                        bodyHeader: '',
                        bodyText: 'Запрос успешно создан',
                        submitText: 'Хорошо',
                        showCancelBtn: false,
                        handleSubmit: () => this.props.showModal({isVisible: false}),
                        handleCancel: () => this.props.showModal({isVisible: false}),
                    },
                });
                */
                this.props.setMessageData({
                    isVisible: true,
                    header: 'Успешно!',
                    body: 'Запрос на перегенерацию сертификата отправлен в Банк',
                    btnText: 'Далее',
                    callback: () => this.props.setMessageData({ isVisible: false }),
                });
            })
            .catch((err) => {
                console.log('Error:', err);
                this.setState({
                    isFetching: false,
                });

                let errorMessage = 'Произошла ошибка при создании запроса на перегенерацию сертификата';
                if (err.message == '3') {
                    errorMessage = 'Устройство не найдено';
                }
                if (err.message == '19') {
                    errorMessage = 'Неверный пин-код';
                }

                this.props.setMessageData({
                    isVisible: true,
                    header: 'Внимание!',
                    body: errorMessage,
                    btnText: 'Далее',
                    callback: () => this.props.setMessageData({ isVisible: false }),
                });

                this.setState({
                    pin: '',
                    showPin: false,
                });
            });
    }

    /**
     * Получение бинарных данных сертификата из iSimple
     * @param {string|number} id идентификатор сертификата в iSimple
     * @return {Promise}
     */
    getCertificateData = id => fetch(`/api/v1/crypto/certificate/${id}`, {
        credentials: 'include',
    }).then(response => response.text()).catch((err) => {
        console.log(err);
        return err;
    })

    /**
     * Создание запроса на перегенерацию сертификата Pkcs10
     * @param {string} key - идентификатор ключевой пары
     * @return {Promise}
     */
    createPkcs10 = (key) => {
        window.rutokenPlugin.login(0, this.state.pin);
        // находим ключевую пару с заданным идентификатором
        return window.rutokenPlugin.enumerateKeys(0, key).then((keys) => {
            const keyUsageVal = [
                'digitalSignature',
                'nonRepudiation',
                'keyEncipherment',
                'dataEncipherment',
                'keyAgreement',
            ];
            const extKeyUsageVal = [
                'emailProtection',
                'clientAuth',
                'serverAuth',
                'codeSigning',
                'timeStamping',
                'msCodeInd',
                'msCodeCom',
                'msCTLSign',
                '1.3.6.1.5.5.7.3.9',
                '1.2.643.2.2.34.6',
            ];
            const extensions = {
                keyUsage: keyUsageVal,
                extKeyUsage: extKeyUsageVal,
            };
            // генерируем Pkcs10 запрос
            return window.rutokenPlugin.createPkcs10(0, keys[0], this.state.tokenCert.subject, extensions, false);
        });
    }

    /**
     * Вызов сервиса создания запроса на перегенерацию сертификата в iSimple
     * @param {string} deviceSerial - идентификатор токена
     * @param {string} request - запрос на перегенерацию сертификата(base64)
     * @return {Promise}
     */
    createRegenRequest = (deviceSerial, request) => fetch('/api/v1/crypto/certificate_regen_request/create', {
        method: 'POST',
        credentials: 'include',
        body: JSON.stringify({
            certificate: this.props.certificate,
            request,
            deviceSerial,
        }),
    })
        .then(response => response.json())
        .then((json) => {
            console.log(json);
            if (json.errorText) {
                return Promise.reject(json);
            }
            return json;
        })

    /**
     * Получение бинарный данных в формате base64 для подписи
     * @param {string|number} id - идентификатор документа
     * @return {Promise}
     */
    getDocumentData = id => fetch('/api/v1/documents/data', {
        method: 'POST',
        credentials: 'include',
        body: JSON.stringify({
            docModule: 'doc',
            docType: 'cert_regen_req',
            docId: id,
            libId: this.props.certificate.libId,
        }),
    })
        .then(response => response.text())
        .catch((err) => {
            console.log(err);
        })

    /**
     * Передача подписанного документа в iSimple
     * @param {string} docData - данные в base64 исходного документа
     * @param {string} signedData - данные в base64 подписанного документа
     * @param {string|number} id - идентификатор документа в iSimple
     * @return {Promise}
     */
    sendSignedData = (docData, signedData, id) => fetch('/api/v1/crypto/verify', {
        method: 'POST',
        credentials: 'include',
        body: JSON.stringify({
            docModule: 'doc',
            docType: 'cert_regen_req',
            id,
            libId: this.props.certificate.libId,
            certSerial: this.props.certificate.serial,
            data: docData,
            signedData,
        }),
    })
        .then(response => response.text())
        .then((data) => {
            console.log(data);
            const json = data.length > 0 ? JSON.parse(data) : null;
            if (json && json.errorText) {
                Promise.reject(errorText);
            }
            return data;
        })
        .catch((err) => {
            Promise.reject(err);
        })

    /**
     * Отправка документа в банк iSimple
     * @param {string|number} id - идентификатор документа в iSimple
     * @return {Promise}
     */
    sendRequestToBank = id => fetch('/api/v1/documents/send2bank', {
        method: 'POST',
        credentials: 'include',
        body: JSON.stringify({
            docModule: 'doc',
            docType: 'cert_regen_req',
            docId: id,
        }),
    }).then(response => response.text()).then((text) => {
        if (text.length > 0) {
            const json = JSON.parse(text);
            if (json.errorText) {
                Promise.reject(json.errorText);
            }
        }
    }).catch((err) => {
        console.log(err);
    })

    render() {
        const { certificate } = this.props;

        return (
            <div style={{ height: 30 }} data-temporary={this.props.isTemporary === true}>
                {this.state.showPin
                    ? (
                        <form style={{ height: 30, display: 'block' }} onSubmit={this.handlePinSubmit}>
                            <input
                                type="password"
                                onChange={this.handlePinChange}
                                value={this.state.pin}
                                placeholder="Введите пин"
                                style={{
                                    width: 190,
                                    height: 30,
                                    paddingLeft: 10,
                                    paddingRight: 10,
                                    color: '#979797',
                                    fontFamily: 'OpenSans-Regular',
                                    fontSize: 12,
                                    border: 'solid 1px #979797',
                                    borderRadius: 4,
                                    outline: 0,
                                    margin: 0,
                                    display: 'inline-block',
                                    verticalAlign: 'middle',
                                    whiteSpace: 'normal',
                                    background: 'none',
                                    lineHeight: 1,
                                }}
                                ref={(input) => { this.input = input; }}
                            />
                        </form>
                    )
                    : (
                        <Btn
                            caption="Перевыпустить сертификат"
                            bgColor="white"
                            style={{
                                width: 190,
                                height: 31,
                                color: '#979797',
                                fontSize: 12,
                                border: 'solid 1px #979797',
                                borderRadius: 4,
                            }}
                            onClick={this.handleBtnClick}
                            showLoader={this.state.isFetching}
                            loaderColor="linear-gradient(121deg, #09357e, #e72e92)"
                        />
                    )}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        regen: state.crypto.regen,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        fetchRegen: bindActionCreators(fetchRegen, dispatch),
        setMessageData: bindActionCreators(setMessageData, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(RegenerationButton);
