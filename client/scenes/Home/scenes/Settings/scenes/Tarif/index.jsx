import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import fetchTariffs from 'modules/settings/actions/tariffs/fetch';
import fetchProductList from 'modules/settings/actions/product_list/fetch';
/** Загрузка списка тарифов из CMS */
import { fetchTariffList } from 'modules/tariffs/list';
import setMessageData from 'modules/info_message/actions/set_data';

import Loader from 'components/Loader';

import TarifContainer from './containers/TarifContainer';

class Tarif extends Component {
    static propTypes = {
        fetchTariffs: PropTypes.func.isRequired,
        fetchProductList: PropTypes.func.isRequired,
        fetchTariffList: PropTypes.func.isRequired,
        setMessageData: PropTypes.func.isRequired,
        productList: PropTypes.object,
        orgInfo: PropTypes.object,
        tariffs: PropTypes.object,
        tariffListCMS: PropTypes.object,
    };

    static defaultProps = {
        productList: {},
        orgInfo: {},
        tariffs: {},
        tariffListCMS: {},
    };

    constructor(props) {
        super(props);

        this.state = {
            isFetching: false,
        };
    }

    componentWillMount() {
        this.props.fetchTariffs();
        this.props.fetchProductList();
        this.props.fetchTariffList();
    }

    toggleLoader = isVisible => {
        this.setState({
            isFetching: isVisible,
        });
    };

    render() {
        const isFetching =
            this.props.tariffs.isFetching ||
            this.props.productList.isFetching ||
            this.props.tariffListCMS.isFetching ||
            this.state.isFetching;

        return (
            <div style={{ position: 'relative', height: '100%' }}>
                <Loader visible={isFetching} />
                <TarifContainer
                    productList={this.props.productList.data}
                    orgInfo={this.props.orgInfo}
                    tariffs={this.props.tariffs.data}
                    tariffListCMS={this.props.tariffListCMS.data}
                    toggleLoader={this.toggleLoader}
                    setMessageData={this.props.setMessageData}
                />
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        tariffs: state.settings.tariffs,
        orgInfo: state.user.orgInfo.data,
        productList: state.settings.productList,
        tariffListCMS: state.tariffs.list,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        fetchTariffs: bindActionCreators(fetchTariffs, dispatch),
        fetchProductList: bindActionCreators(fetchProductList, dispatch),
        fetchTariffList: bindActionCreators(fetchTariffList, dispatch),
        setMessageData: bindActionCreators(setMessageData, dispatch),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Tarif);
