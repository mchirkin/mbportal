import React, { Component } from 'react';
import PropTypes from 'prop-types';

import CorporateCard from 'containers/forms/CorporateCard';
import Acquiring from 'containers/forms/Acquiring';

import FormHeader from '../../components/FormHeader';
import FormFooter from '../../components/FormFooter';

export default class RefillData extends Component {
    static propTypes = {
        handleBackwardBtn: PropTypes.func,
        handleForwardBtn: PropTypes.func,
        getFormData: PropTypes.func,
        newProducts: PropTypes.array,
        newTarif: PropTypes.object,
    };

    static defaultProps = {
        handleBackwardBtn: undefined,
        handleForwardBtn: undefined,
        getFormData: undefined,
        newProducts: [],
        newTarif: {},
    };

    constructor(props) {
        super(props);

        this.state = {
            activeStep: '',
            steps: [],
            disabledSteps: [],
            corporateCardInvalidFields: [],
            acquiringInvalidFields: [],
        };
    }

    componentWillReceiveProps = (nextProps) => {
        if (nextProps.newProducts && !this.newProductsReceived) {
            let activeStep = '';
            let steps = [];
            let disabledSteps = [];

            nextProps.newProducts.forEach((product) => {
                if (/CORP_CARD/.test(product.code)) {
                    steps = [...steps, 'corporateCard'];
                    // activeStep = 'corporateCard';
                    return;
                }

                if (/TRADE_EQU/.test(product.code)) {
                    steps = [...steps, 'acquiring'];
                    // activeStep = 'acquiring';
                }
            });

            activeStep = steps[0] || '';

            if (steps.length > 1) {
                disabledSteps = steps.slice(0, 1);
            }

            this.setState({
                activeStep,
                steps,
                disabledSteps,
            });

            this.newProductsReceived = true;
        }
    }

    handleForwardBtn = () => {
        console.log(this.state.steps);
        if (this.state.steps && this.state.steps.length === 0) {
            this.changeStep();
        }
        // Валидация корпоративной карты
        if (this.state.activeStep === 'corporateCard') {
            this.validateCorporateCard()
                .then(this.changeStep);
        }

        // Валидация эквайринга
        if (this.state.activeStep === 'acquiring') {
            this.validateAcquiring()
                .then(this.changeStep);
        }
    }

    handleHeaderButton = (e) => {
        const stepId = e.target.name;
        if (stepId === 'acquiring' && this.state.activeStep === 'corporateCard') {
            this.validateCorporateCard()
                .then(() => {
                    if (this.state.corporateCardInvalidFields.length === 0) {
                        this.setState({
                            activeStep: stepId,
                        });
                    } else {
                        this.setState({
                            disabledSteps: [...this.state.disabledSteps, stepId],
                        });
                    }
                });
            return;
        }

        if (!this.state.disabledSteps.includes(stepId)) {
            this.setState({
                activeStep: stepId,
            });
        }
    }

    changeStep = () => {
        const currentStep = this.state.activeStep;

        // Делаем кнопку эквайринга активной, если все поля корп карты валидны
        if (
            currentStep === 'corporateCard' &&
            this.state.corporateCardInvalidFields.length === 0 &&
            this.state.steps.includes('acquiring') &&
            this.state.disabledSteps.includes('acquiring')
        ) {
            this.setState({
                disabledSteps: this.state.disabledSteps.filter(item => item !== 'acquiring'),
            });
        }

        // Переключаем на эквайринг, если все поля корп карты валидны
        if (
            currentStep === 'corporateCard' &&
            this.state.corporateCardInvalidFields.length === 0 &&
            this.state.steps.includes('acquiring')
        ) {
            this.setState({
                activeStep: 'acquiring',
            });
        }

        // Показываем экран итога, если все поля валидны
        if (
            currentStep === 'acquiring' &&
            this.state.acquiringInvalidFields.length === 0 &&
            this.state.corporateCardInvalidFields.length === 0
        ) {
            if (this.state.steps.includes('corporateCard')) {
                this.props.getFormData(this.corporateCardComp.getData(), 'corporateCardData');
            }

            if (this.state.steps.includes('acquiring')) {
                this.props.getFormData(this.acquiringComp.getData(), 'acquiringData');
            }

            this.props.handleForwardBtn();
        }

        if (this.state.steps && this.state.steps.length === 0) {
            this.props.handleForwardBtn();
        }
    }

    validateCorporateCard = () => {
        const data = this.corporateCardComp.getData();
        const requiredInputs = [
            'registrationAddress',
            'homeAddress',
            'workAddress',
            'placeOfBirth',
        ];

        const arr = Object.keys(data);

        const corporateCardInvalidFields = arr.filter((key) => {
            if (key === 'dadata') {
                return false;
            }

            // Проверка на дозаплненность поля
            if (data[key].search('_') !== -1) {
                return true;
            }

            return !data[key];
        });

        requiredInputs.forEach((key) => {
            if (!corporateCardInvalidFields.includes(key)) {
                if (!Object.keys(data.dadata).includes(key)) {
                    corporateCardInvalidFields.push(key);
                }
            }
        });

        return new Promise((resolve) => {
            this.setState({
                corporateCardInvalidFields,
            }, () => resolve(true));
        });
    }

    validateAcquiring = () => {
        const data = this.acquiringComp.getData();
        const requiredInputs = [
            'setupAddress',
            'checkSetupAddress',
        ];

        const arr = Object.keys(data);

        const acquiringInvalidFields = arr.filter((key) => {
            if (key === 'dadata') {
                return false;
            }

            if (key === 'fileList') {
                if (data[key].length === 0) {
                    return true;
                }
                return false;
            }

            if (key === 'needPinPad') {
                if (
                    data.terminalConnection === 'mobileWifi' ||
                    data.terminalConnection === 'mobileGsm'
                ) {
                    return false;
                }
            }

            if (key === 'communicationType') {
                if (
                    data.terminalConnection === 'mobileWifi' ||
                    data.terminalConnection === 'mobileGsm' ||
                    data.terminalConnection === 'staticGsm'
                ) {
                    return false;
                }
            }

            if (key === 'networkGateway' || key === 'ipAdress' || key === 'networkMask') {
                if (
                    data.terminalConnection === 'staticIp' &&
                    data.communicationType === 'static' &&
                    data[key] === ''
                ) {
                    return true;
                }
                if (
                    data.terminalConnection === '' &&
                    data[key] === ''
                ) {
                    return true;
                }
                return false;
            }

            // Проверка на дозаплненность поля
            if (data[key].search('_') !== -1) {
                return true;
            }

            return !data[key];
        });

        requiredInputs.forEach((key) => {
            if (!acquiringInvalidFields.includes(key)) {
                if (!Object.keys(data.dadata).includes(key)) {
                    acquiringInvalidFields.push(key);
                }
            }
        });

        return new Promise((resolve) => {
            this.setState({
                acquiringInvalidFields,
            }, () => resolve(true));
        });
    }

    deleteInvalidInput = (id) => {
        // для корп карты
        if (this.state.corporateCardInvalidFields.includes(id)) {
            const newFields = this.state.corporateCardInvalidFields.filter(e => e !== id);

            this.setState({
                corporateCardInvalidFields: newFields,
            });
        }

        // для эквайринга
        if (this.state.acquiringInvalidFields.includes(id)) {
            const newFields = this.state.acquiringInvalidFields.filter(e => e !== id);

            this.setState({
                acquiringInvalidFields: newFields,
            });
        }
    }

    render() {
        return (
            <div>
                <FormHeader
                    tarifCaption={this.props.newTarif ? this.props.newTarif.caption : ''}
                    products={this.props.newProducts}
                    oldProducts={this.props.oldProducts}
                    steps={this.state.steps}
                    disabledSteps={this.state.disabledSteps}
                    handleHeaderButton={this.handleHeaderButton}
                />
                <div>
                    <CorporateCard
                        isVisible={this.state.activeStep === 'corporateCard'}
                        ref={(comp) => { this.corporateCardComp = comp; }}
                        invalidInputs={this.state.corporateCardInvalidFields}
                        deleteInvalidInput={this.deleteInvalidInput}
                    />
                    <Acquiring
                        isVisible={this.state.activeStep === 'acquiring'}
                        ref={(comp) => { this.acquiringComp = comp; }}
                        invalidInputs={this.state.acquiringInvalidFields}
                        deleteInvalidInput={this.deleteInvalidInput}
                    />
                </div>
                <FormFooter
                    handleBackwardBtn={this.props.handleBackwardBtn}
                    handleForwardBtn={this.handleForwardBtn}
                />
            </div>
        );
    }
}
