import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { fetch, scrollbarsAnimations } from 'utils';

import RefillData from '../../containers/RefillData';
import TarifInfo from '../../components/TarifInfo';
import SummaryModal from '../../components/SummaryModal';

export default class TarifContainer extends Component {
    static propTypes = {
        setMessageData: PropTypes.func.isRequired,
        toggleLoader: PropTypes.func.isRequired,
        tariffs: PropTypes.array,
        tariffListCMS: PropTypes.array,
        orgInfo: PropTypes.object,
        productList: PropTypes.array,
    };

    static defaultProps = {
        tariffs: [],
        tariffListCMS: [],
        orgInfo: {},
        productList: [],
    };

    constructor(props) {
        super(props);

        this.state = {
            newTarif: [],
            showTarifInfo: true,
            showChangeTariffs: false,
            showRefillData: false,
            showSummaryModal: false,
            paymentPeriodOption: '1',
        };
    }

    handleBtnChangeTariffClick = () => {
        this.setState({
            showChangeTariffs: true,
        });
    };

    handleBtnSelectTariffClick = newTarif => {
        if (this.props.orgInfo.tarifCode === 'TP_DEFAULT') {
            this.setState({
                showSummaryModal: true,
                newTarif,
            });

            return;
        }

        this.setState(
            {
                showRefillData: true,
                showChangeTariffs: false,
                showTarifInfo: false,
                newTarif,
            },
            () => {
                this.comparisonOfTariffs();
            }
        );
    };

    handleOptionChange = e => {
        this.setState({
            paymentPeriodOption: e.target.value,
        });
    };

    handleBackwardBtn = () => {
        this.setState({
            showTarifInfo: true,
            showRefillData: false,
            showChangeTariffs: false,
        });
    };

    handleForwardBtn = () => {
        this.setState({
            showSummaryModal: true,
        });
    };

    handleSubmitSummary = () => {
        if (this.props.orgInfo.tarifCode === 'TP_DEFAULT') {
            this.submitFromDefaultTarif();
            return;
        }

        this.createRequest();
    };

    handleResetSummary = () => {
        this.setState({
            showSummaryModal: false,
        });
    };

    submitFromDefaultTarif = () => {
        this.props.toggleLoader(true);

        fetch('/api/v1/tariff/change_from_default_tarif', {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify({
                org: this.props.orgInfo,
            }),
        })
            .then(response => response.json())
            .then(json => {
                this.props.toggleLoader(false);

                let header = 'Ваша заявка успешно принята!';
                let msg = 'Наш менеджер свяжется с вами в ближайшее время';

                if (json.error) {
                    header = 'Что-то пошло не так...';
                    msg = 'Попробуйте отправить заявку еще раз';
                }

                this.showMessage({
                    header,
                    msg,
                    btnText: json.error ? 'Назад' : 'Далее',
                });
                localStorage.setItem('canChangeDefaultTarif', 'false');
                if (!json.error) {
                    localStorage.setItem('canChangeDefaultTarif', 'false');

                    this.setState({
                        showTarifInfo: true,
                        showRefillData: false,
                        showChangeTariffs: false,
                        showSummaryModal: false,
                    });
                }
            })
            .catch(err => {
                console.log(err);

                this.setState({
                    isFetching: false,
                });

                const header = 'Что-то пошло не так...';
                const msg = 'Попробуйте отправить заявку еще раз';

                this.showMessage({
                    header,
                    msg,
                    btnText: 'Назад',
                });
            });
    };

    handleScrollItemMouseEnter = (e, scrollbars) => {
        if (e.target.dataset.direction === 'forward') {
            const options = {
                direction: 'x',
                element: scrollbars,
                to: scrollbars.getScrollWidth(),
                duration: 4000,
                context: this,
            };

            scrollbarsAnimations(options);
        } else {
            const options = {
                direction: 'x',
                element: scrollbars,
                to: -scrollbars.getScrollWidth(),
                duration: 4000,
                context: this,
            };

            scrollbarsAnimations(options);
        }
    };

    handleScrollItemMouseleave = (e, scrollbars) => {
        if (e.target.dataset.direction === 'forward') {
            const options = {
                direction: 'x',
                element: scrollbars,
                to: scrollbars.getScrollLeft() + 40,
                duration: 300,
                context: this,
            };

            scrollbarsAnimations(options);
        } else {
            const options = {
                direction: 'x',
                element: scrollbars,
                to: scrollbars.getScrollLeft() - 40,
                duration: 300,
                context: this,
            };

            scrollbarsAnimations(options);
        }
    };

    comparisonOfTariffs = () => {
        const currentTarif = this.props.productList.filter(tarif => tarif.code === this.props.orgInfo.tarifCode)[0];
        const newTarif = this.props.productList.filter(tarif => tarif.code === this.state.newTarif.tarifPlanCode)[0];

        // Если тариф стандартный - считаем, что у него нет продуктов
        if (currentTarif.code === 'TP_DEFAULT') currentTarif.productList = [];
        if (newTarif.code === 'TP_DEFAULT') newTarif.productList = [];

        const newProducts = newTarif.productList.filter(
            product => currentTarif.productList.map(currentProduct => currentProduct.code).indexOf(product.code) < 0
        );

        const oldProducts = currentTarif.productList.filter(
            product => newTarif.productList.map(currentProduct => currentProduct.code).indexOf(product.code) < 0
        );

        this.setState({
            newProducts,
            oldProducts,
        });
    };

    attachFile = (id, file) => {
        const formData = new FormData();
        formData.append('clientRequest', id);
        formData.append('typeId', '12357');
        formData.append('file', file, file.name);

        return fetch('/api/v1/documents/attachment/type', {
            method: 'POST',
            credentials: 'include',
            body: formData,
        })
            .then(response => {
                if (response.ok) {
                    if (response.status !== 200) {
                        return false;
                    }
                    return response.json();
                }
                return false;
            })
            .then(json => {
                if (json === false || json.errorCode) {
                    return Promise.reject(json);
                }
                return true;
            })
            .catch(err => {
                return Promise.reject(err);
            });
    };

    sendToBank = id => {
        return fetch('/api/v1/tariff/send2bank', {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify({
                docId: id,
                docModule: 'ibankul_req',
                docType: 'client_request',
            }),
        })
            .then(response => response.text())
            .then(text => {
                if (text.length > 0) {
                    const json = JSON.parse(text);
                    return Promise.reject(json);
                }
                // this.props.fetchTariffPlanRequests();

                localStorage.setItem('canChangeTarif', 'false');

                this.props.setMessageData({
                    isVisible: true,
                    header: 'Ваша заявка успешно принята!',
                    // eslint-disable-next-line
                    body:
                        'Обслуживание по выбранному тарифу начнется с 1-го числа месяца, следующего за месяцем подписания заявлений на новые продукты при условии, что дата подписания не приходится на последние 5 (пять) рабочих дней месяца заключения. В противном случае перевод осуществляется с первого числа второго месяца, следующего за месяцем подписания заявлений',
                    btnText: 'Далее',
                    callback: () => this.props.setMessageData({ isVisible: false }),
                });

                this.setState({
                    showTarifInfo: true,
                    showRefillData: false,
                    showChangeTariffs: false,
                    showSummaryModal: false,
                });

                this.props.toggleLoader(false);

                return true;
            })
            .then(result => {
                // this.showGoodModal(result);
                console.log('success', result);
            })
            .catch(err => {
                return Promise.reject(err);
            });
    };

    createRequest = () => {
        const data = {
            typeId: this.props.productList.find(pr => pr.code === this.state.newTarif.tarifPlanCode).id,
            period: this.state.newTarif.tarifPlanCode === 'TP_DEFAULT' ? '0' : this.state.paymentPeriodOption,
            branchCode: this.props.orgInfo.branchCodeRef,
            tariff_plan: this.state.newTarif.tarifPlanCode.replace('TP_', '').toUpperCase(),
            operation: 'change',
            products: [],
        };

        const dbo = this.state.newProducts.find(pr => pr.code === 'DBO');
        const corpCard = this.state.newProducts.find(pr => pr.code === 'CORP_CARD');
        const acquiring = this.state.newProducts.find(pr => pr.code === 'TRADE_EQU');

        if (dbo) {
            const formattedDboData = {
                id: dbo.id,
                entityId: dbo.entityId,
                values: {
                    surname_dbo: '',
                    first_name_dbo: '',
                    second_name_dbo: '',
                    phone_mob_dbo: '',
                    email: '',
                    rutoken_accept: '',
                    sms_accept: '',
                },
            };
            data.products.push(formattedDboData);
        }

        if (corpCard && this.state.corporateCardData) {
            const {
                passportSeries,
                passportNumber,
                passportDate,
                passportIssued,
                passportCode,
                surname,
                name,
                middleName,
                latinSurname,
                latinName,
                dateOfBirth,
                placeOfBirth,
                homePhone,
                workPhone,
                mobilePhone,
                workName,
                workPosition,
                dadata,
            } = this.state.corporateCardData;

            const formattedcorporateCardData = {
                id: corpCard.id,
                entityId: corpCard.entityId,
                values: {
                    doc_type_id_hold: 'FS0001',
                    series_hold: passportSeries,
                    number_hold: passportNumber,
                    date_delivery_hold: passportDate,
                    who_issues_hold: passportIssued,
                    division_code_hold: passportCode,
                    card_type: 'VB',
                    currency_code: 'RUB',
                    surname_hold: surname,
                    first_name_hold: name,
                    second_name_hold: middleName,
                    surname_lat_hold: latinSurname,
                    first_name_lat_hold: latinName,
                    person_date: dateOfBirth,
                    birth_place: placeOfBirth,
                    address_type_reg: 'reg',
                    code_cladr_reg: dadata.registrationAddress.kladr_id,
                    region_reg: dadata.registrationAddress.region,
                    district_reg: dadata.registrationAddress.city_area,
                    town_reg: dadata.registrationAddress.city,
                    settlement_reg: dadata.registrationAddress.settlement,
                    street_reg: dadata.registrationAddress.street,
                    street_type_reg: dadata.registrationAddress.street_type,
                    house_reg: dadata.registrationAddress.house,
                    building_reg: dadata.registrationAddress.block,
                    construction_reg: '',
                    flat_reg: dadata.registrationAddress.flat,
                    postcode_reg: dadata.registrationAddress.postal_code,
                    address_type_fact: 'fact',
                    code_cladr_fact: dadata.homeAddress.kladr_id,
                    region_fact: dadata.homeAddress.region,
                    district_fact: dadata.homeAddress.city_area,
                    town_fact: dadata.homeAddress.city,
                    settlement_fact: dadata.homeAddress.settlement,
                    street_fact: dadata.homeAddress.street,
                    street_type_fact: dadata.homeAddress.street_type,
                    house_fact: dadata.homeAddress.house,
                    building_fact: dadata.homeAddress.block,
                    construction_fact: '',
                    flat_fact: dadata.homeAddress.flat,
                    postcode_fact: dadata.homeAddress.postal_code,
                    address_type_job: 'job',
                    code_cladr_job: dadata.workAddress.kladr_id,
                    region_job: dadata.workAddress.region,
                    district_job: dadata.workAddress.city_area,
                    town_job: dadata.workAddress.city,
                    settlement_job: dadata.workAddress.settlement,
                    street_job: dadata.workAddress.street,
                    street_type_job: dadata.workAddress.street_type,
                    house_job: dadata.workAddress.house,
                    building_job: dadata.workAddress.block,
                    construction_job: '',
                    flat_job: dadata.workAddress.flat,
                    postcode_job: dadata.workAddress.postal_code,
                    phone_home: homePhone,
                    phone_job: workPhone,
                    phone_mob: mobilePhone,
                    org_title: workName,
                    position: workPosition,
                },
            };
            data.products.push(formattedcorporateCardData);
        }

        if (acquiring && this.state.acquiringData) {
            const {
                posName,
                posSurname,
                posMiddleName,
                posContactPhone,
                posTwoDigits,
                terminalConnection,
                needPinPad,
                organizationName,
                communicationType,
                ipAdress,
                networkGateway,
                networkMask,
                checkSetupAddress,
                dadata,
            } = this.state.acquiringData;

            const formattedAcquiringData = {
                id: acquiring.id,
                entityId: acquiring.entityId,
                values: {
                    surname_equ: posSurname,
                    first_name_equ: posName,
                    second_name_equ: posMiddleName,
                    phone_num: posContactPhone,
                    oktmo: posTwoDigits,
                    mobile_gsm: terminalConnection === 'mobileGsm' && true,
                    mobile_wifi: terminalConnection === 'mobileWifi' && true,
                    static_gsm: terminalConnection === 'staticGsm' && true,
                    static_ip: terminalConnection === 'staticIp' && true,
                    need_pin_pad: needPinPad === 'pinPadNeeded' && true,
                    org_name_on_check: organizationName,
                    dhcp: communicationType === 'dhcp' && true,
                    static: communicationType === 'static' && true,
                    ip: ipAdress,
                    gateway: networkGateway,
                    network_mask: networkMask,
                    address_check: checkSetupAddress,
                    address_type_term: 'term',
                    code_cladr_term: dadata.setupAddress.kladr_id,
                    region_term: dadata.setupAddress.region,
                    district_term: dadata.setupAddress.city_area,
                    town_term: dadata.setupAddress.city,
                    settlement_term: dadata.setupAddress.settlement,
                    street_term: dadata.setupAddress.street,
                    street_type_term: dadata.setupAddress.street_type,
                    house_term: dadata.setupAddress.house,
                    building_term: dadata.setupAddress.block,
                    construction_term: '',
                    flat_term: dadata.setupAddress.flat,
                    postcode_term: dadata.setupAddress.postal_code,
                },
            };
            data.products.push(formattedAcquiringData);
        }

        console.log('[data]', data);

        this.props.toggleLoader(true);

        return fetch('/api/v1/tariff/create_request', {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify(data),
        })
            .then(response => response.json())
            .then(json => {
                if (json.sessionExpired === true) {
                    window.location.href = '/login';
                }
                if (!json.id) {
                    return Promise.reject('error');
                }
                if (acquiring && this.state.acquiringData && this.state.acquiringData.fileList.length > 0) {
                    const attachPromises = this.state.acquiringData.fileList.map(file =>
                        this.attachFile(json.id, file)
                    );

                    Promise.all(attachPromises)
                        .then(values => {
                            console.log(values);
                            return this.sendToBank(json.id);
                        })
                        .catch(err => {
                            // this.showErrorModal(err);
                            this.props.setMessageData({
                                isVisible: true,
                                header: 'Внимание!',
                                body: 'Произошла ошибка. Попробуйте позже',
                                btnText: 'Назад',
                                callback: () => this.props.setMessageData({ isVisible: false }),
                            });
                            console.log(err);
                            this.props.toggleLoader(false);
                        });
                } else {
                    this.sendToBank(json.id)
                        .then(() => true)
                        .catch(err => {
                            // this.showErrorModal(err);
                            console.log(err);
                        });
                }
            })
            .catch(err => {
                // this.showErrorModal(err);
                console.log(err);
                this.props.setMessageData({
                    isVisible: true,
                    header: 'Внимание!',
                    body: 'Произошла ошибка. Попробуйте позже',
                    btnText: 'Назад',
                    callback: () => this.props.setMessageData({ isVisible: false }),
                });
            });
    };

    getFormData = (data, formName) => {
        this.setState({
            [formName]: data,
        });
    };

    showMessage = ({ header, msg, btnText }) => {
        this.props.setMessageData({
            isVisible: true,
            header,
            body: msg,
            btnText,
            callback: () => this.props.setMessageData({ isVisible: false }),
        });
    };

    render() {
        return (
            <div style={{ position: 'relative', height: '100%' }}>
                {this.state.showSummaryModal && (
                    <SummaryModal
                        newTarif={this.state.newTarif}
                        tariffListCMS={this.props.tariffListCMS}
                        handleOptionChange={this.handleOptionChange}
                        paymentPeriodOption={this.state.paymentPeriodOption}
                        handleSubmitSummary={this.handleSubmitSummary}
                        handleResetSummary={this.handleResetSummary}
                    />
                )}
                <div style={this.state.showSummaryModal ? { display: 'none' } : { height: '100%' }}>
                    {this.state.showRefillData && (
                        <RefillData
                            handleBackwardBtn={this.handleBackwardBtn}
                            handleForwardBtn={this.handleForwardBtn}
                            newProducts={this.state.newProducts}
                            oldProducts={this.state.oldProducts}
                            newTarif={this.state.newTarif}
                            getFormData={this.getFormData}
                        />
                    )}
                    {this.state.showTarifInfo && (
                        <TarifInfo
                            handleBtnChangeTariffClick={this.handleBtnChangeTariffClick}
                            handleBtnSelectTariffClick={this.handleBtnSelectTariffClick}
                            handleOptionChange={this.handleOptionChange}
                            handleScrollItemMouseEnter={this.handleScrollItemMouseEnter}
                            handleScrollItemMouseleave={this.handleScrollItemMouseleave}
                            tariffs={this.props.tariffs}
                            tariffListCMS={this.props.tariffListCMS}
                            orgInfo={this.props.orgInfo}
                            showChangeTariffs={this.state.showChangeTariffs}
                            paymentPeriodOption={this.state.paymentPeriodOption}
                        />
                    )}
                </div>
            </div>
        );
    }
}
