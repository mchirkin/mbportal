import React from 'react';
import PropTypes from 'prop-types';

import Btn from 'components/ui/Btn';

import styles from './footer.css';

const FormFooter = (props) => {
    return (
        <div className={styles.container}>
            <Btn
                caption="Остаться на действующем тарифе"
                bgColor="grey"
                onClick={props.handleBackwardBtn}
                style={{
                    width: '100%',
                    maxWidth: 470,
                }}
            />
            <Btn
                caption="Далее"
                bgColor="gradient"
                onClick={props.handleForwardBtn}
                style={{
                    width: '100%',
                    maxWidth: 240,
                }}
            />
        </div>
    );
};

FormFooter.propTypes = {
    handleBackwardBtn: PropTypes.func.isRequired,
    handleForwardBtn: PropTypes.func.isRequired,
};

export default FormFooter;
