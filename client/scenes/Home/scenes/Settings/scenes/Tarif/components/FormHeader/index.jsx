
import React from 'react';
import PropTypes from 'prop-types';

import styles from './header.css';

const FormHeader = (props) => {
    const oldProductsStr = props.oldProducts && props.oldProducts
        .filter(p => p.code !== 'DBO')
        .map(p => `«${p.caption}»`)
        .join(', ');

    return (
        <div className={styles.container}>
            <h2 className={styles.title}>
                Вы выбрали тариф {props.tarifCaption && `«${props.tarifCaption}»`}
            </h2>
            {props.oldProducts && props.oldProducts.length
                ? (
                    <p className={styles.description}>
                        <span>
                            В выбранный вами тариф не
                            {props.oldProducts && props.oldProducts.length > 1
                                ? ' входят услуги: '
                                : ' входит услуга '
                            }
                        </span>
                        <strong>
                            {props.oldProducts && oldProductsStr}.
                        </strong>
                    </p>
                )
                : null
            }
            {
                props.oldProducts &&
                props.oldProducts.length
                    ? [
                        <p className={styles.description}>
                            После перехода на тарифный план&nbsp;
                            <strong>
                                {props.tarifCaption && `«${props.tarifCaption}»`}
                            </strong>
                            {props.oldProducts && props.oldProducts.length > 1
                                ? ' услуги будут '
                                : ' услуга будет '
                            }
                            предоставляться по стандартному тарифу.
                        </p>,
                        <p className={styles.description}>
                            Подробнее с тарифами Вы можете ознакомится&nbsp;
                            <a
                                className={styles.link}
                                target="blank"
                                href="https://www.rosevrobank.ru/download/1298"
                            >
                                на сайте
                            </a>
                        </p>,
                    ]
                    : null
            }
            {props.steps.length > 0
                ? (
                    <p className={styles.subtitle}>
                        Для завершения оформления тарифа заполните, пожалуйста, данные для оформления новых продуктов
                    </p>
                )
                : null
            }
            <div className={styles.buttons}>
                {props.steps.includes('corporateCard')
                    && (
                        <button
                            className={`
                                ${styles.button}
                                ${styles.button_theme_blue}
                            `}
                            onClick={props.handleHeaderButton}
                            name="corporateCard"
                        >
                            Корпоративная карта
                        </button>
                    )
                }
                {props.steps.includes('acquiring')
                    && (
                        <button
                            className={`
                                ${styles.button}
                                ${styles.button_theme_yellow}
                                ${props.disabledSteps.includes('acquiring') && styles.button_disabled}
                            `}
                            onClick={props.handleHeaderButton}
                            name="acquiring"
                        >
                            Торговый эквайринг
                        </button>
                    )
                }
            </div>
        </div>
    );
};

FormHeader.propTypes = {
    tarifCaption: PropTypes.string,
    products: PropTypes.array,
    steps: PropTypes.array,
    disabledSteps: PropTypes.array,
    handleHeaderButton: PropTypes.func,
};

FormHeader.defaultProps = {
    tarifCaption: '',
    products: [],
    steps: [],
    disabledSteps: [],
    handleHeaderButton: undefined,
};

export default FormHeader;
