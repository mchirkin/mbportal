import React from 'react';
import PropTypes from 'prop-types';

import { formatNumber } from 'utils';

import Btn from 'components/ui/Btn';

import styles from './tarif.css';

const TarifItem = ({ tarif, tariffListCMS, btnCaption, btnHandleClick, btnBgColor, noPrice, paymentPeriodOption }) => {
    const canChangeDefaultTarif = localStorage.getItem('canChangeDefaultTarif');
    const canChangeTarif = localStorage.getItem('canChangeTarif');

    const showChangeBtnOnDefaultTarif = !canChangeDefaultTarif || canChangeDefaultTarif === 'true';
    const showChangeBtn = !canChangeTarif || canChangeTarif === 'true';

    let showBtn = true;

    if (tarif.tarifPlanCode === 'TP_DEFAULT' && !showChangeBtnOnDefaultTarif) {
        showBtn = false;
    }

    if (tarif.tarifPlanCode !== 'TP_DEFAULT' && !showChangeBtn) {
        showBtn = false;
    }

    const showPrice = !noPrice || tarif.tarifPlanCode === 'TP_DEFAULT';

    const tariffCMSInfo = tariffListCMS.find(t => t.code === tarif.tarifPlanCode);

    const getPrice = period => {
        if (tariffCMSInfo && tariffCMSInfo.noPayment) {
            return {};
        }
        return tariffCMSInfo && tariffCMSInfo.price.find(p => p.period === period);
    };

    return (
        <div className={styles.item}>
            <div className={styles.content}>
                <p className={styles.name}>
                    {tariffCMSInfo && tariffCMSInfo.name}
                    {!tariffCMSInfo && tarif.caption}
                </p>
                {/*
                <p className={styles.description}>
                    {tarif.description}
                </p>
                */}
                <div className={styles.options}>
                    {tariffCMSInfo &&
                        tariffCMSInfo.description &&
                        [...tariffCMSInfo.description].sort((a, b) => a.priority - b.priority).map((line, i) => {
                            if (line.price === '0') {
                                return null;
                            }
                            return (
                                <div key={i} className={styles.option}>
                                    <p className={styles.caption}>{line.caption}</p>
                                    <p className={styles.value}>{line.price}</p>
                                </div>
                            );
                        })}
                    {!tariffCMSInfo &&
                        tarif.lines.slice(3).map((line, i) => (
                            <div key={i} className={styles.option}>
                                <p className={styles.caption}>{line.caption}</p>
                                <p className={styles.value}>
                                    {line.priceCaption === undefined ? line.orderCaption : line.priceCaption}
                                </p>
                            </div>
                        ))}
                </div>
                {showPrice && (
                    <div className={styles.price}>
                        {(() => {
                            if (tariffCMSInfo) {
                                const priceInfo = getPrice(paymentPeriodOption);

                                return (
                                    <div>
                                        {tariffCMSInfo.noPayment
                                            ? 'без абонентской платы'
                                            : `${formatNumber(priceInfo.price, false)} \u20bd`}
                                        {priceInfo.priceSave && (
                                            <div className={styles['price-save']}>
                                                экономия {`${formatNumber(priceInfo.priceSave, false)} \u20bd`}
                                            </div>
                                        )}
                                    </div>
                                );
                            }
                            if (paymentPeriodOption === '1' || tarif.tarifPlanCode === 'TP_DEFAULT') {
                                return tarif.lines[0].priceCaption;
                            }
                            if (paymentPeriodOption === '6') {
                                return tarif.lines[1].priceCaption;
                            }
                            if (paymentPeriodOption === '12') {
                                return tarif.lines[2].priceCaption;
                            }
                        })()}
                    </div>
                )}
            </div>
            {showBtn && <Btn caption={btnCaption} onClick={() => btnHandleClick(tarif)} bgColor={btnBgColor} />}
        </div>
    );
};

TarifItem.propTypes = {
    tarif: PropTypes.object,
    tariffListCMS: PropTypes.array,
    btnHandleClick: PropTypes.func.isRequired,
    btnCaption: PropTypes.string,
    btnBgColor: PropTypes.string,
    noPrice: PropTypes.bool,
    paymentPeriodOption: PropTypes.string,
};

TarifItem.defaultProps = {
    tarif: {},
    tariffListCMS: [],
    btnBgColor: '',
    btnCaption: '',
    noPrice: false,
    paymentPeriodOption: '1',
};

export default TarifItem;
