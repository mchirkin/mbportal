import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Scrollbars } from 'react-custom-scrollbars';

import TarifItem from '../TarifItem';
import PaymentPeriod from '../PaymentPeriod';

import styles from './tarif-info.css';

export default class TarifInfo extends Component {
    static propTypes = {
        tariffs: PropTypes.array,
        tariffListCMS: PropTypes.array,
        orgInfo: PropTypes.object,
        showChangeTariffs: PropTypes.bool,
        paymentPeriodOption: PropTypes.string,
        handleBtnChangeTariffClick: PropTypes.func.isRequired,
        handleBtnSelectTariffClick: PropTypes.func.isRequired,
        handleOptionChange: PropTypes.func.isRequired,
        handleScrollItemMouseEnter: PropTypes.func.isRequired,
        handleScrollItemMouseleave: PropTypes.func.isRequired,
    };

    static defaultProps = {
        tariffs: [],
        tariffListCMS: [],
        orgInfo: {},
        showChangeTariffs: false,
        paymentPeriodOption: 'yearOption',
    };

    render() {
        const { tariffs, orgInfo } = this.props;

        if (!orgInfo || !tariffs) {
            return null;
        }

        const currentTariffCode = orgInfo.tarifCode;
        const currentTariff = tariffs.find(t => {
            const endDate =
                t.endDate && t.endDate.replace(/(\d{2})\.(\d{2})\.(\d{4})/, '$3-$2-$1');
            if (endDate) {
                const endDateObject = new Date(endDate);
                if (endDateObject < new Date()) {
                    return false;
                }
            }

            const startDate =
                t.startDate &&
                t.startDate.replace(/(\d{2})\.(\d{2})\.(\d{4})/, '$3-$2-$1');
            if (startDate) {
                const startDateObject = new Date(startDate);
                if (startDateObject > new Date()) {
                    return false;
                }
            }

            return t.tarifPlanCode === currentTariffCode;
        });

        const msbTariffs = [
            'TP_DEFAULT',
            'TP_SUCCESS',
            'TP_STORE',
            'TP_CASH',
            'TP_ACCOUNT',
            'TP_START',
        ];

        const tariffList =
            tariffs && tariffs.length > 0
                ? tariffs.filter(t => {
                      const endDate =
                          t.endDate &&
                          t.endDate.replace(/(\d{2})\.(\d{2})\.(\d{4})/, '$3-$2-$1');
                      if (endDate) {
                          const endDateObject = new Date(endDate);
                          if (endDateObject < new Date()) {
                              return false;
                          }
                      }

                      const startDate =
                          t.startDate &&
                          t.startDate.replace(/(\d{2})\.(\d{2})\.(\d{4})/, '$3-$2-$1');
                      if (startDate) {
                          const startDateObject = new Date(startDate);
                          if (startDateObject > new Date()) {
                              return false;
                          }
                      }

                      return msbTariffs.includes(t.tarifPlanCode);
                  })
                : [];

        return (
            <div className={styles.container}>
                {this.props.showChangeTariffs ? (
                    <div className={`${styles.header} ${styles.header_border}`}>
                        <h3 className={styles.title}>Период оплаты</h3>
                        <PaymentPeriod
                            paymentPeriodOption={this.props.paymentPeriodOption}
                            handleOptionChange={this.props.handleOptionChange}
                        />
                        <div className={styles['close-giglet']} />
                    </div>
                ) : (
                    <div className={styles.header}>
                        <h3 className={styles.title}>Текущий тарифный план</h3>
                    </div>
                )}
                <div className={styles['scroll-container']}>
                    <div
                        className={styles['scroll-item']}
                        data-direction="backward"
                        onMouseEnter={e =>
                            this.props.handleScrollItemMouseEnter(e, this.scrollbars)
                        }
                        onMouseLeave={e =>
                            this.props.handleScrollItemMouseleave(e, this.scrollbars)
                        }
                    />
                    <div
                        className={styles['scroll-item']}
                        data-direction="forward"
                        onMouseEnter={e =>
                            this.props.handleScrollItemMouseEnter(e, this.scrollbars)
                        }
                        onMouseLeave={e =>
                            this.props.handleScrollItemMouseleave(e, this.scrollbars)
                        }
                    />
                </div>
                <Scrollbars
                    ref={scrollbars => {
                        this.scrollbars = scrollbars;
                    }}
                >
                    <div className={styles.list}>
                        {currentTariff && (
                            <TarifItem
                                key={currentTariff.id}
                                tarif={currentTariff}
                                tariffListCMS={this.props.tariffListCMS}
                                btnCaption={
                                    this.props.showChangeTariffs
                                        ? 'Текущий тариф'
                                        : 'Сменить тариф'
                                }
                                btnHandleClick={
                                    this.props.showChangeTariffs
                                        ? () => {}
                                        : this.props.handleBtnChangeTariffClick
                                }
                                btnBgColor="gradient"
                                noPrice={!this.props.showChangeTariffs}
                                paymentPeriodOption={this.props.paymentPeriodOption}
                            />
                        )}
                        {this.props.showChangeTariffs &&
                            [...tariffList]
                                .sort((a, b) => {
                                    const { tariffListCMS } = this.props;

                                    if (tariffListCMS && tariffListCMS.length > 0) {
                                        const aTariffCMSInfo = tariffListCMS.find(
                                            t => t.code === a.tarifPlanCode
                                        );
                                        const bTariffCMSInfo = tariffListCMS.find(
                                            t => t.code === b.tarifPlanCode
                                        );

                                        return (
                                            aTariffCMSInfo.order - bTariffCMSInfo.order
                                        );
                                    }

                                    if (a.tarifPlanCode === 'TP_DEFAULT') {
                                        return 1;
                                    }

                                    return 0;
                                })
                                .map(tarif => {
                                    if (
                                        tarif.tarifPlanCode ===
                                        currentTariff.tarifPlanCode
                                    )
                                        return false;
                                    return (
                                        <TarifItem
                                            key={tarif.id}
                                            tarif={tarif}
                                            tariffListCMS={this.props.tariffListCMS}
                                            btnCaption="Выбрать тариф"
                                            btnHandleClick={
                                                this.props.handleBtnSelectTariffClick
                                            }
                                            btnBgColor="white"
                                            paymentPeriodOption={
                                                this.props.paymentPeriodOption
                                            }
                                        />
                                    );
                                })}
                        {this.props.showChangeTariffs && (
                            <div
                                style={{
                                    flexShrink: 0,
                                    minWidth: 10,
                                    width: 10,
                                    height: 10,
                                }}
                            />
                        )}
                    </div>
                </Scrollbars>
                {this.props.showChangeTariffs && (
                    <p className={styles.footnote}>
                        Во все тарифы кроме тарифа «Стандартный»&#160;
                        <strong className={styles.footnote_strong}>
                            бесплатно&#160;
                        </strong>
                        включены следующие услуги:&#160;
                        <strong className={styles.footnote_strong}>
                            {/* eslint-disable-next-line */}
                            открытие и обслуживание счета, корпоративная карта,
                            интернет-банк, сервис проверки контрагентов&#160;
                        </strong>
                    </p>
                )}
            </div>
        );
    }
}
