import React from 'react';
import PropTypes from 'prop-types';

import styles from './radio-list.css';

const PaymentPeriod = (props) => {
    return (
        <div className={styles.list}>
            <div className={styles.item}>
                <input
                    className={styles.input}
                    type="radio"
                    value="12"
                    id="yearOption"
                    checked={props.paymentPeriodOption === '12'}
                    onChange={props.handleOptionChange}
                />
                <label
                    className={styles.label}
                    htmlFor="yearOption"
                >
                    12 мес.
                </label>
            </div>
            <div className={styles.item}>
                <input
                    className={styles.input}
                    type="radio"
                    value="6"
                    id="halfyearOption"
                    checked={props.paymentPeriodOption === '6'}
                    onChange={props.handleOptionChange}
                />
                <label
                    className={styles.label}
                    htmlFor="halfyearOption"
                >
                    6 мес.
                </label>
            </div>
            <div className={styles.item}>
                <input
                    className={styles.input}
                    type="radio"
                    value="1"
                    id="monthOption"
                    checked={props.paymentPeriodOption === '1'}
                    onChange={props.handleOptionChange}
                />
                <label
                    className={styles.label}
                    htmlFor="monthOption"
                >
                    1 мес.
                </label>
            </div>
        </div>
    );
};

PaymentPeriod.propTypes = {
    paymentPeriodOption: PropTypes.string.isRequired,
    handleOptionChange: PropTypes.func.isRequired,
};

export default PaymentPeriod;
