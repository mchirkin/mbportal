import React from 'react';
import PropTypes from 'prop-types';

import { formatNumber } from 'utils';

import Btn from 'components/ui/Btn';
import PaymentPeriod from '../PaymentPeriod';

import styles from './modal.css';

const SummaryModal = props => {
    const tariffCMSInfo = props.tariffListCMS.find(t => t.code === props.newTarif.tarifPlanCode);

    const getPrice = period => {
        if (tariffCMSInfo && tariffCMSInfo.noPayment) {
            return {};
        }
        return tariffCMSInfo && tariffCMSInfo.price.find(p => p.period === period);
    };

    return (
        <div className={styles.container}>
            <div className={styles.content}>
                <div className={styles.row}>
                    <p className={styles.info}>Тарифный план</p>
                    <p className={styles.value}>«{props.newTarif.caption}»</p>
                </div>
                {props.newTarif.tarifPlanCode !== 'TP_DEFAULT' && (
                    <div className={styles.row}>
                        <p className={styles.info}>Период оплаты</p>
                        <PaymentPeriod
                            handleOptionChange={props.handleOptionChange}
                            paymentPeriodOption={props.paymentPeriodOption}
                        />
                    </div>
                )}
                <div className={styles.row}>
                    <p className={styles.info}>Стоимость</p>
                    <p className={styles.value}>
                        {(() => {
                            if (tariffCMSInfo) {
                                const priceInfo = getPrice(props.paymentPeriodOption);

                                return (
                                    <div>
                                        {tariffCMSInfo.noPayment
                                            ? 'без абонентской платы'
                                            : `${formatNumber(priceInfo.price, false)} \u20bd`}
                                        {priceInfo.priceSave && (
                                            <div className={styles['price-save']}>
                                                экономия {`${formatNumber(priceInfo.priceSave, false)} \u20bd`}
                                            </div>
                                        )}
                                    </div>
                                );
                            }

                            if (props.paymentPeriodOption === '1' || props.newTarif.tarifPlanCode === 'TP_DEFAULT') {
                                return props.newTarif.lines[0].priceCaption;
                            }
                            if (props.paymentPeriodOption === '6') {
                                return props.newTarif.lines[1].priceCaption;
                            }
                            if (props.paymentPeriodOption === '12') {
                                return props.newTarif.lines[2].priceCaption;
                            }
                        })()}
                    </p>
                </div>
            </div>
            <Btn caption="Да, все верно" bgColor="grey" onClick={props.handleSubmitSummary} style={{ minWidth: 208 }} />
            <button className={styles['change-button']} onClick={props.handleResetSummary}>
                Изменить
            </button>
        </div>
    );
};

SummaryModal.propTypes = {
    newTarif: PropTypes.object.isRequired,
    paymentPeriodOption: PropTypes.string.isRequired,
    handleOptionChange: PropTypes.func.isRequired,
    handleSubmitSummary: PropTypes.func.isRequired,
    handleResetSummary: PropTypes.func.isRequired,
    tariffListCMS: PropTypes.array,
};

SummaryModal.defaultProps = {
    tariffListCMS: [],
};

export default SummaryModal;
