import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import moment from 'moment';

import { fetch, debounce } from 'utils';
import loadPlugin from 'utils/rutoken/load_plugin';

import fetchOrgInfo from 'modules/user/actions/org_info/fetch';
import fetchUserInfo from 'modules/user/actions/user_info/fetch';
import fetchAccounts from 'modules/accounts/actions/fetch';
import fetchCards from 'modules/cards/actions/fetch';
import selectAccount from 'modules/accounts/actions/select_account';
import fetchErrorPayments from 'modules/payments/error_payments/actions/fetch';
import fetchNewPayments from 'modules/payments/new_payments/actions/fetch';
import fetchTemplates from 'modules/templates/actions/fetch';
import fetchStatement from 'modules/statement/actions/fetch';
import setPaymentModalData from 'modules/payments/payment_modal/actions/set_data';
import fetchSendType from 'modules/settings/actions/send_type/fetch';
import fetchPStatus from 'modules/payments/actions/pstatus/fetch';
import fetchSorts from 'modules/payments/actions/sorts/fetch';
import fetchTaxperiod from 'modules/payments/actions/taxperiod/fetch';
import fetchPayground from 'modules/payments/actions/payground/fetch';
import fetchPaytype from 'modules/payments/actions/paytype/fetch';
import fetchNds from 'modules/payments/actions/nds/fetch';
import fetchUnreadMsg from 'modules/mail/actions/unread/fetch';
import fetchCertificates from 'modules/crypto/actions/certificates/fetch';
import fetchCodevo from 'modules/payments/codevo/actions/fetch';
import fetchAccountsStays from 'modules/accounts/stays/actions/fetch';
import { fetchNote } from 'modules/user_note/note';

import setPopupNotificationData from 'modules/popup_notification/actions/set_data';
import setImportantModalVisibility from 'modules/important_notifications/actions/set_visibility';
import addImportantNotification from 'modules/important_notifications/actions/add_notification';
import setCertificateTourVisible from 'modules/settings/actions/welcome_tour/set_visibility';

import Loader from 'components/Loader';

import AppLayout from './components/AppLayout';

class Home extends Component {
    static contextTypes = {
        router: PropTypes.object.isRequired,
    };

    static propTypes = {
        /** список счетов клиента */
        accounts: PropTypes.array,
        /** список карт клиента */
        cards: PropTypes.array,
        /** данные об организации клиента */
        orgInfo: PropTypes.object,
        /** данные о сотруднике клиента */
        userInfo: PropTypes.object,
        /** id выбранного счета */
        selectedAccount: PropTypes.string,
        /** ограничения по счетам */
        stays: PropTypes.object,
        /** выбор счета */
        selectAccount: PropTypes.func.isRequired,
        /** загрузка сертификатов */
        fetchCertificates: PropTypes.func.isRequired,
        /** загрузка счетов */
        fetchAccounts: PropTypes.func.isRequired,
        /** загрузка карт */
        fetchCards: PropTypes.func.isRequired,
        /** загрузка организационной информации */
        fetchOrgInfo: PropTypes.func.isRequired,
        /** загрузка данных о клиенте */
        fetchUserInfo: PropTypes.func.isRequired,
        fetchTemplates: PropTypes.func.isRequired,
        /** загрузка платежей */
        fetchNewPayments: PropTypes.func.isRequired,
        fetchErrorPayments: PropTypes.func.isRequired,
        fetchSendType: PropTypes.func.isRequired,
        fetchPStatus: PropTypes.func.isRequired,
        fetchSorts: PropTypes.func.isRequired,
        fetchTaxperiod: PropTypes.func.isRequired,
        fetchPaytype: PropTypes.func.isRequired,
        fetchNds: PropTypes.func.isRequired,
        fetchPayground: PropTypes.func.isRequired,
        fetchUnreadMsg: PropTypes.func.isRequired,
        setPopupNotificationData: PropTypes.func.isRequired,
        accountsIsFetching: PropTypes.bool,
        cardIsFetching: PropTypes.bool,
        certificates: PropTypes.array,
        templates: PropTypes.array,
        /** загрузка справочника кодов валютной операции */
        fetchCodevo: PropTypes.func.isRequired,
        /** загрука списка ограничений/блокировок по счетам */
        fetchAccountsStays: PropTypes.func.isRequired,
        /** установка видимости модального окна с важными сообщениями */
        setImportantModalVisibility: PropTypes.func.isRequired,
        /** добавление сообщения в список важных */
        addImportantNotification: PropTypes.func.isRequired,
        /** установка видимости welcome тура */
        setCertificateTourVisible: PropTypes.func.isRequired,
        userNote: PropTypes.array,
        fetchNote: PropTypes.func.isRequired,
    };

    static defaultProps = {
        accounts: [],
        cards: [],
        orgInfo: [],
        userInfo: [],
        selectedAccount: '',
        stays: {},
        accountsIsFetching: false,
        cardIsFetching: false,
        certificates: [],
        templates: [],
        userNote: [],
    };

    constructor(props) {
        super(props);

        this.state = {
            openingBalance: '', // остаток на начало месяца
            avaliableBalance: '', // доступный остаток
            creditSum: '', // + нам платили
            debetSum: '', // - мы платили
            sessionLeft: 120,
        };
    }

    componentWillMount() {
        window.addEventListener('click', debounce(this.sessionPing, 10000));
        window.addEventListener('keypress', debounce(this.sessionPing, 10000));

        this.startIntervalSession();

        const viewportMeta = document.querySelector('meta[name="viewport"]');
        // document.head.removeChild(viewportMeta);

        if (screen.width < 1280) {
            viewportMeta.setAttribute('content', 'width=1280');
        }

        if (!this.props.accounts) {
            this.props.fetchAccounts();
        }
        if (!this.props.cards) {
            this.props.fetchCards();
        }
        if (!this.props.orgInfo) {
            this.props.fetchOrgInfo();
        }
        if (!this.props.userInfo) {
            this.props.fetchUserInfo()
                .then(() => {
                    this.props.fetchNote({ extRef: this.props.userInfo.extRef });
                });
        }
        if (!this.props.templates) {
            this.props.fetchTemplates();
        }
        this.props.fetchNewPayments();
        this.props.fetchErrorPayments();

        this.props.fetchSendType();
        this.props.fetchPStatus();
        this.props.fetchSorts();
        this.props.fetchTaxperiod();
        this.props.fetchPaytype();
        this.props.fetchNds();
        this.props.fetchPayground();
        this.props.fetchUnreadMsg();
        this.props.fetchCodevo();

        if (!this.props.certificates) {
            this.props.fetchCertificates();
        }
    }

    async componentWillReceiveProps(nextProps) {
        if (!this.props.selectedAccount && nextProps.accounts && !this.initialAccountSelected) {
            const mainAccount = nextProps.accounts.find(acc => acc.type !== '7');
            this.props.selectAccount(mainAccount.id);

            this.initialAccountSelected = true;

            this.props.fetchAccountsStays(nextProps.accounts)
                .then(() => {
                    let hasStay = false;

                    Object.keys(this.props.stays).forEach((account) => {
                        const accountStays = this.props.stays[account];
                        if (accountStays && !accountStays.errorCode) {
                            hasStay = true;
                        }
                    });

                    if (hasStay) {
                        this.props.setImportantModalVisibility(true);
                    }
                });
        }

        if (!this.props.accounts && nextProps.accounts) {
            if (this.checkAccountInInfoModeExist(nextProps.accounts)) {
                /* eslint-disable max-len */
                this.props.addImportantNotification({
                    id: 'info-mode',
                    text: 'Возможность совершения расходных операций в интернет-банке приостановлена.',
                    moreContent: (
                        <div>
                            Возможные причины:<br /><br />
                            - Недостаточно средств для списания абонентской платы по вашему тарифному плану<br />
                            - Необходимо предоставление дополнительных документов для подтверждения операций.
                            <br /><br />
                            Для уточнения причины вы можете&nbsp;
                            <Link
                                to="/home/mail"
                                onClick={() => {
                                    this.props.setImportantModalVisibility(false);
                                }}
                            >
                                написать в банк
                            </Link> или позвонить по телефону 8-800-500-45-21
                        </div>
                    ),
                    color: 'red',
                    priority: 'high',
                });
                /* eslint-enable max-len */
                this.props.setImportantModalVisibility(true);
            }
        }

        if (!this.props.certificates && nextProps.certificates) {
            const soonExpiredCertificate = this.findSoonExpiredCertificate(nextProps.certificates);

            if (soonExpiredCertificate) {
                const expireDate = moment(soonExpiredCertificate.validTo).format('DD.MM.YYYY HH:mm:ss');
                this.props.addImportantNotification({
                    text: (
                        <div>
                            Срок действия сертификата вашей подписи истекает {expireDate}.
                        </div>
                    ),
                    moreContent: (
                        <div>
                            Во избежание блокировки работы в интернет-банке рекомендуем вам выполнить перевыпуск.
                             Для этого нужно перейти в раздел&nbsp;
                            <Link
                                to="/home/settings/certificates"
                                onClick={() => {
                                    this.props.setImportantModalVisibility(false);
                                }}
                            >
                                <span>
                                    настройки
                                </span>
                            </Link>
                        </div>
                    ),
                });
            }

            if (this.checkTemporaryCertificateExist(nextProps.certificates)) {
                this.props.addImportantNotification({
                    text: 'Необходимо выполнить перевыпуск транспортного сертификата.',
                    btnTextValue: 'Выполнить',
                    handleBtnClick: this.TemporaryCertificateHandleClick,
                });
                this.props.setImportantModalVisibility(true);
            }

            if (this.checkRutokenCertificateExist(nextProps.certificates)) {
                loadPlugin()
                    .then(() => true)
                    .catch((err) => {
                        const login = localStorage.getItem('login');
                        const [
                            notificationLogin,
                            showRutokenNotification,
                        ] = (localStorage.getItem('show_rutoken_notification') || '').split('||');

                        if (notificationLogin === login && showRutokenNotification === 'false') {
                            return;
                        }

                        /* eslint-disable */
                        if (/extension/i.test(err.message)) {
                            this.props.addImportantNotification({
                                text: 'Мы заметили, что у Вас не установлено расширение для корректной работы с Рутокен ЭЦП.',
                                moreContent: (
                                    <div>
                                        Найти расширение можно по этой <a href="https://chrome.google.com/webstore/detail/%D0%B0%D0%B4%D0%B0%D0%BF%D1%82%D0%B5%D1%80-%D1%80%D1%83%D1%82%D0%BE%D0%BA%D0%B5%D0%BD-%D0%BF%D0%BB%D0%B0%D0%B3%D0%B8%D0%BD/ohedcglhbbfdgaogjhcclacoccbagkjg?utm_source=chrome-app-launcher-info-dialog">ссылке</a>.
                                    </div>
                                ),
                            });
                        }
                        if (/plugin/i.test(err.message)) {
                            this.props.addImportantNotification({
                                text: 'Мы заметили, что у Вас не установлен плагин для корректной работы с Рутокен ЭЦП.',
                                moreContent: (
                                    <div>
                                        Найти плагин можно по этой&nbsp;
                                        <a href="https://www.rutoken.ru/support/download/rutoken-plugin/">
                                            ссылке
                                        </a>.
                                    </div>
                                ),
                            });
                        }
                        /* eslint-enable */
                        this.props.setImportantModalVisibility(true);
                    });
            }
        }

        const isCard = this.props.cards && this.props.cards.find(card => nextProps.selectedAccount === card.id);

        if (this.props.selectedAccount !== nextProps.selectedAccount && !isCard) {
            this.setState({
                openingBalance: '',
                avaliableBalance: '',
                creditSum: '',
                debetSum: '',
            });

            const today = moment().format('DD.MM.YYYY');

            try {
                await fetch('/api/v1/statement', {
                    method: 'POST',
                    credentials: 'include',
                    body: JSON.stringify({
                        accountId: nextProps.selectedAccount,
                        dateFrom: today,
                        dateTo: today,
                    }),
                });

                const response = await fetch('/api/v1/statement/offline', {
                    method: 'POST',
                    credentials: 'include',
                    body: JSON.stringify({
                        accountId: nextProps.selectedAccount,
                        dateFrom: moment().format('01.MM.YYYY'),
                        dateTo: today,
                    }),
                });

                const json = await response.json();

                if (json && json.statementAnswer) {
                    this.setState({
                        openingBalance: json.statementAnswer.head_opening_balance,
                        avaliableBalance: json.statementAnswer.head_closing_balance_avaliable, // доступный остаток
                        creditSum: json.statementAnswer.head_credit_turnover, // + нам платили
                        debetSum: json.statementAnswer.head_debet_turnover, // - мы платили
                    });
                }
            } catch (err) {
                global.console.log(err);
            }
        }
    }

    goToHomePage = (isCard) => {
        if (!/\/payment/.test(document.location.href) || isCard) {
            this.context.router.history.push('/home');
        }
    }

    TemporaryCertificateHandleClick = () => {
        setTimeout(() => {
            this.props.setCertificateTourVisible(true);
            this.props.setImportantModalVisibility(false);
        }, 600);
        this.context.router.history.push('/home/settings/certificates');
    }

    /**
     * Проверка наличия у клиента сертификата для подписи Рукокеном
     * @param {Array} certList список сертификатов пользователя
     * @return {boolean}
     */
    checkRutokenCertificateExist = certList => !!certList.find(cert => cert.cryptoTypeCode === 'AKTIV_RUTOKEN')

    /**
     * Проверка наличия у клиента транспортного сертификата
     * @param {Array} certList список сертификатов пользователя
     * @return {boolean}
     */
    checkTemporaryCertificateExist = certList => !!certList.find(cert => cert.isTemporary === 'true')

    /**
     * Проверка наличия сертификата, чей срок действия скоро заканчивается
     * @param {Array} certList список сертификатов пользователя
     * @return {Object|null}
     */
    findSoonExpiredCertificate = (certList) => {
        const now = moment();
        /**
         * За сколько дней считать,
         * что срок действия сертификата скоро истекает
         */
        const days = /mb/.test(document.location.origin) ? 30 : 120;

        return certList.find((cert) => {
            const endDate = moment(cert.validTo);
            console.log(cert.id, cert.validTo, endDate);
            const diff = moment.duration(endDate.diff(now)).asDays();
            console.log(diff);
            return cert.cryptoTypeCode.toUpperCase() === 'AKTIV_RUTOKEN' && diff <= days;
        }) || null;
    }

    /**
     *  Проверка наличия счета в информационном режиме(заблокированный счет)
     * @param {Array} accounts список счетов клиента
     * @return {boolean}
     */
    checkAccountInInfoModeExist = accounts => !!accounts.find(acc => acc.status === 'info');

    sessionPing = () => {
        if (!this.sessionInterval) return;

        this.restartIntervalSession();

        fetch('/api/v1/check_session', {
            method: 'GET',
            credentials: 'include',
        });
    }

    startIntervalSession = () => {
        const sessionLimit = 30;
        const warningTime = sessionLimit - 2;

        this.props.setPopupNotificationData({ isVisible: false });
        let minutesPassed = 0;
        this.sessionInterval = setInterval(() => {
            minutesPassed += 1;
            if (minutesPassed === warningTime) {
                this.startSessionLeftTimer();
                this.props.setPopupNotificationData({
                    isVisible: true,
                    headerText: 'Уважаемый клиент!',
                    message: (
                        <div>
                            Вы долгое время не совершали никаких действий.
                             Скоро мы разлогиним вас, чтобы интернет-банком не воспользовались посторонние<br />
                            <strong>
                                Осталось: {this.secondsToString(120)}
                            </strong>
                        </div>
                    ),
                    submitText: 'Продолжить',
                    showSubmit: true,
                    showClose: false,
                    handleSubmit: () => {
                        this.props.setPopupNotificationData({ isVisible: false });
                        this.sessionPing();
                    },
                });
            }

            if (minutesPassed === sessionLimit) {
                minutesPassed = null;
                this.stopIntervalSession();
                this.sessionInterval = null;

                window.showSessionExpired = true;
                this.props.setPopupNotificationData({
                    isVisible: false,
                });
                this.context.router.history.push('/login');
            }
        }, 60000);
    }

    stopIntervalSession = () => {
        clearInterval(this.sessionInterval);
    }

    restartIntervalSession = () => {
        this.stopIntervalSession();
        this.startIntervalSession();
    }

    startSessionLeftTimer = () => {
        let sessionLeft = 120;
        const sessionLeftInterval = setInterval(() => {
            sessionLeft -= 1;
            this.props.setPopupNotificationData({
                message: (
                    <div>
                        Вы долгое время не совершали никаких действий.
                         Скоро мы разлогиним вас, чтобы интернет-банком не воспользовались посторонние<br />
                        <strong>
                            Осталось: {this.secondsToString(sessionLeft)}
                        </strong>
                    </div>
                ),
            });
            if (sessionLeft === 0) {
                clearInterval(sessionLeftInterval);
            }
        }, 1000);
    }

    secondsToString = (seconds) => {
        const min = Math.floor(seconds / 60);
        const sec = parseInt(seconds % 60, 10);
        return `${min > 9 ? min : `0${min}`}:${sec > 9 ? sec : `0${sec}`}`;
    }

    render() {
        return (
            <div style={{ width: '100%', height: '100%' }}>
                {this.props.accountsIsFetching || this.props.cardIsFetching
                    ? (
                        <Loader
                            visible
                            text="Загружаем информацию о Ваших счетах..."
                        />
                    )
                    : (
                        <AppLayout
                            goToHomePage={this.goToHomePage}
                            {...this.state}
                            {...this.props}
                        />
                    )}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        orgInfo: state.user.orgInfo.data,
        userInfo: state.user.userInfo.data,
        accounts: state.accounts.data,
        accountsIsFetching: state.accounts.isFetching,
        cards: state.cards.data,
        cardIsFetching: state.cards.isFetching,
        selectedAccount: state.accounts.selectedAccount,
        errorPayments: state.payments.errorPayments.data,
        newPayments: state.payments.newPayments.data,
        toSendPayments: state.payments.newPayments.signed,
        templates: state.templates.data,
        notification: state.notification.data,
        unreadMsg: state.mail.unread.data,
        certificates: state.crypto.certificates.data,
        stays: state.accounts.stays.data,
        importantNotifications: state.importantNotifications.list,
        calendarWidgetIsVisible: state.calendarWidget.isVisible,
        userNote: state.userNote.note.data,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        fetchOrgInfo: bindActionCreators(fetchOrgInfo, dispatch),
        fetchUserInfo: bindActionCreators(fetchUserInfo, dispatch),
        fetchAccounts: bindActionCreators(fetchAccounts, dispatch),
        fetchCards: bindActionCreators(fetchCards, dispatch),
        selectAccount: bindActionCreators(selectAccount, dispatch),
        fetchErrorPayments: bindActionCreators(fetchErrorPayments, dispatch),
        fetchNewPayments: bindActionCreators(fetchNewPayments, dispatch),
        fetchTemplates: bindActionCreators(fetchTemplates, dispatch),
        fetchStatement: bindActionCreators(fetchStatement, dispatch),
        setPaymentModalData: bindActionCreators(setPaymentModalData, dispatch),
        fetchSendType: bindActionCreators(fetchSendType, dispatch),
        fetchPStatus: bindActionCreators(fetchPStatus, dispatch),
        fetchSorts: bindActionCreators(fetchSorts, dispatch),
        fetchTaxperiod: bindActionCreators(fetchTaxperiod, dispatch),
        fetchPaytype: bindActionCreators(fetchPaytype, dispatch),
        fetchPayground: bindActionCreators(fetchPayground, dispatch),
        fetchNds: bindActionCreators(fetchNds, dispatch),
        fetchUnreadMsg: bindActionCreators(fetchUnreadMsg, dispatch),
        fetchCertificates: bindActionCreators(fetchCertificates, dispatch),
        setPopupNotificationData: bindActionCreators(setPopupNotificationData, dispatch),
        fetchCodevo: bindActionCreators(fetchCodevo, dispatch),
        fetchAccountsStays: bindActionCreators(fetchAccountsStays, dispatch),
        setImportantModalVisibility: bindActionCreators(setImportantModalVisibility, dispatch),
        addImportantNotification: bindActionCreators(addImportantNotification, dispatch),
        setCertificateTourVisible: bindActionCreators(setCertificateTourVisible, dispatch),
        fetchNote: bindActionCreators(fetchNote, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
