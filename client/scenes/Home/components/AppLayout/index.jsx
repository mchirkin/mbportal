import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Scrollbars } from 'react-custom-scrollbars';
import { Switch, Route } from 'react-router-dom';
import { TransitionGroup, CSSTransition } from 'react-transition-group';

import PaymentScene from 'scenes/Home/scenes/Payment';
import Statement from 'scenes/Home/scenes/Statement';
import SignPayments from 'scenes/Home/scenes/SignPayments';
import SendPayments from 'scenes/Home/scenes/SendPayments';
import ErrorPayments from 'scenes/Home/scenes/ErrorPayments';
import Contragents from 'scenes/Home/scenes/Contragents';
import Mail from 'scenes/Home/scenes/Mail';
import Special from 'scenes/Home/scenes/Special';
import Services from 'scenes/Home/scenes/Services';
import Settings from 'scenes/Home/scenes/Settings';
import Requisites from 'scenes/Home/scenes/Requisites';
import Invoices from 'scenes/Home/scenes/Invoices';
import AnalyticsSchedule from 'scenes/Home/scenes/AnalyticsSchedule';
import Calendar from 'scenes/Home/scenes/Calendar';

import Organizer from 'scenes/Home/containers/Organizer';

import TopBar from 'components/layout/TopBar';
import BottomBar from 'components/layout/BottomBar';
import PopupNotification from 'components/PopupNotification';
import ImportantNotifications from 'containers/ImportantNotifications';

import CompanyProducts from '../CompanyProducts';
import SettingsNav from '../SettingsNav';
import AccountInfo from '../AccountInfo';
import CardInfo from '../CardInfo';
import PaymentsContainer from '../../containers/PaymentsContainer';

import styles from './app-layout.css';

export default class AppLayout extends Component {
    static propTypes = {
        orgInfo: PropTypes.object,
        userInfo: PropTypes.object,
        accounts: PropTypes.array,
        cards: PropTypes.array,
        fetchCards: PropTypes.func.isRequired,
        selectedAccount: PropTypes.string,
        selectAccount: PropTypes.func.isRequired,
        errorPayments: PropTypes.array,
        newPayments: PropTypes.array,
        toSendPayments: PropTypes.array,
        setPaymentModalData: PropTypes.func.isRequired,
        openingBalance: PropTypes.string.isRequired,
        avaliableBalance: PropTypes.string.isRequired,
        creditSum: PropTypes.string.isRequired,
        debetSum: PropTypes.string.isRequired,
        goToHomePage: PropTypes.func.isRequired,
        notification: PropTypes.bool,
        location: PropTypes.object.isRequired,
        unreadMsg: PropTypes.object,
        setImportantModalVisibility: PropTypes.func.isRequired,
        stays: PropTypes.object,
        importantNotifications: PropTypes.array,
        calendarWidgetIsVisible: PropTypes.bool,
    };

    static defaultProps = {
        orgInfo: {},
        userInfo: {},
        accounts: [],
        cards: [],
        selectedAccount: '',
        errorPayments: [],
        newPayments: [],
        toSendPayments: [],
        notification: null,
        unreadMsg: {},
        stays: null,
        importantNotifications: [],
        calendarWidgetIsVisible: false,
    };

    static contextTypes = {
        router: PropTypes.object.isRequired,
    };

    constructor(props) {
        super(props);

        this.homePageContentScrollTop = 0;
    }

    handleHomePageContentScroll = (e) => {
        // check if at page bottom
        const { scrollTop, clientHeight, scrollHeight } = e.target;

        const pixelsToBottom = 50;

        if (
            scrollTop + clientHeight >= scrollHeight - pixelsToBottom &&
            scrollTop > this.homePageContentScrollTop &&
            clientHeight < scrollHeight
        ) {
            this.paymentsContainer.getWrappedInstance().loadMore();
        }

        this.homePageContentScrollTop = e.target.scrollTop;
    }

    showInfoModeNotification = () => {
        if (this.importantNotifications) {
            this.props.setImportantModalVisibility(true);
            this.importantNotifications.getWrappedInstance().openInfoModeNotification();
        }
    }

    render() {
        const {
            orgInfo,
            userInfo,
            accounts,
            cards,
            fetchCards,
            selectedAccount,
            selectAccount,
            errorPayments,
            newPayments,
            toSendPayments,
            setPaymentModalData,
            openingBalance,
            avaliableBalance,
            creditSum,
            debetSum,
            goToHomePage,
            notification,
            location,
            unreadMsg,
            setImportantModalVisibility,
            stays,
            importantNotifications,
        } = this.props;

        const isAccount = accounts && accounts.find(acc => acc.id === selectedAccount);

        const hasStays = stays && Object.keys(stays).find(key => stays[key] !== null && !stays[key].errorText);
        const hasImportantNotifications = importantNotifications && importantNotifications.length > 0;

        return (
            <Scrollbars
                renderTrackHorizontal={props => (
                    <div
                        {...props}
                        style={{
                            /*
                            position: 'absolute',
                            right: 2,
                            bottom: 2,
                            left: 2,
                            zIndex: 9999,
                            height: 6,
                            borderRadius: 3,
                            */
                        }}
                    />
                )}
            >
                <div className={styles.wrapper}>
                    <TopBar />
                    <div className={styles.content}>
                        {!/settings/.test(location.pathname)
                            ? (
                                <CompanyProducts
                                    orgInfo={orgInfo}
                                    accounts={accounts}
                                    cards={cards}
                                    selectedAccount={selectedAccount}
                                    selectAccount={selectAccount}
                                    goToHomePage={goToHomePage}
                                />
                            )
                            : (
                                <SettingsNav />
                            )}
                        <div className={styles['main-content']}>
                            <PaymentScene
                                accounts={accounts}
                            />
                            <Scrollbars
                                onScroll={this.handleHomePageContentScroll}
                                renderThumbVertical={thumbProps =>
                                    (<div
                                        {...thumbProps}
                                        style={{
                                            position: 'relative',
                                            zIndex: 990,
                                            display: 'block',
                                            width: '100%',
                                            cursor: 'pointer',
                                            borderRadius: 'inherit',
                                            backgroundColor: 'rgba(0, 0, 0, 0.2)',
                                            height: '42px',
                                            transform: 'translateY(0px)',
                                        }}
                                    />)
                                }
                            >
                                <div
                                    style={{
                                        opacity: location.pathname === '/home' ? 1 : 0,
                                        transition: 'opacity 0.75s ease',
                                    }}
                                >
                                    {isAccount
                                        ? (
                                            <AccountInfo
                                                accounts={accounts}
                                                selectedAccount={selectedAccount}
                                                openingBalance={openingBalance}
                                                avaliableBalance={avaliableBalance}
                                                creditSum={creditSum}
                                                debetSum={debetSum}
                                                showInfoModeNotification={this.showInfoModeNotification}
                                            />
                                        )
                                        : (
                                            <CardInfo
                                                accounts={accounts}
                                                cards={cards}
                                                selectedAccount={selectedAccount}
                                                selectAccount={selectAccount}
                                                setPaymentModalData={setPaymentModalData}
                                                fetchCards={fetchCards}
                                            />
                                        )}
                                    <div style={{ marginTop: 15 }}>
                                        <PaymentsContainer
                                            ref={(c) => { this.paymentsContainer = c; }}
                                        />
                                    </div>
                                </div>
                            </Scrollbars>
                            <TransitionGroup
                                style={{
                                    position: 'absolute',
                                    top: 0,
                                    left: 0,
                                    width: 'calc(100% - 15px * 2)',
                                    height: '100%',
                                    margin: '0 15px',
                                    zIndex: location.pathname !== '/home' ? 2200 : -1,
                                }}
                            >
                                <CSSTransition
                                    key={location.key}
                                    classNames="fade"
                                    timeout={500}
                                >
                                    <div style={{ width: '100%', height: '100%' }}>
                                        <Switch location={location}>
                                            <Route exact path="/home/statement" component={Statement} />
                                            <Route exact path="/home/sign_payments" component={SignPayments} />
                                            <Route exact path="/home/send_payments" component={SendPayments} />
                                            <Route exact path="/home/error_payments" component={ErrorPayments} />
                                            <Route path="/home/contacts" component={Contragents} />
                                            <Route exact path="/home/mail" component={Mail} />
                                            <Route exact path="/home/special" component={Special} />
                                            <Route path="/home/services" component={Services} />
                                            <Route path="/home/settings" component={Settings} />
                                            <Route path="/home/requisites" component={Requisites} />
                                            <Route path="/home/invoices" component={Invoices} />
                                            <Route path="/home/analytics" component={AnalyticsSchedule} />
                                            <Route path="/home/calendar" component={Calendar} />
                                        </Switch>
                                    </div>
                                </CSSTransition>
                            </TransitionGroup>
                        </div>
                    </div>
                    <BottomBar
                        userInfo={userInfo || {}}
                        errorPayments={errorPayments}
                        newPayments={newPayments}
                        toSendPayments={toSendPayments}
                        notification={notification}
                        unreadMsg={unreadMsg}
                        setImportantModalVisibility={setImportantModalVisibility}
                        showImportantBtn={hasStays || hasImportantNotifications}
                    />
                    <PopupNotification />
                    <ImportantNotifications ref={(c) => { this.importantNotifications = c; }} />
                    <Organizer isVisible={this.props.calendarWidgetIsVisible} />
                </div>
            </Scrollbars>
        );
    }
}
