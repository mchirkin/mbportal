import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { NavLink as Link } from 'react-router-dom';

import moment from 'moment';

import { formatNumber } from 'utils';

import Btn from 'components/ui/Btn';

import styles from './account-info.css';

class AccountInfo extends Component {
    static propTypes = {
        accounts: PropTypes.array,
        selectedAccount: PropTypes.string,
        openingBalance: PropTypes.string,
        avaliableBalance: PropTypes.string,
        creditSum: PropTypes.string,
        debetSum: PropTypes.string,
        showInfoModeNotification: PropTypes.func.isRequired,
    };

    static defaultProps = {
        accounts: [],
        selectedAccount: '',
        openingBalance: '',
        avaliableBalance: '',
        creditSum: '',
        debetSum: '',
    };

    static contextTypes = {
        router: PropTypes.object.isRequired,
    };

    handleStatementBtnClick = () => {
        this.context.router.history.push('/home/statement');
    }

    render() {
        const {
            accounts,
            selectedAccount,
            openingBalance,
            avaliableBalance,
            creditSum,
            debetSum,
            showInfoModeNotification,
        } = this.props;
        let accountInfo = accounts && accounts.find(acc => acc.id === selectedAccount);

        if (!accountInfo) {
            accountInfo = {};
        }

        return (
            <div className={styles.wrapper}>
                <div className={styles.head}>
                    <div className={styles['money-info']}>
                        <div className={styles['money-info-item']}>
                            <div className={styles['money-info-item-caption']}>
                                Месяц
                            </div>
                            <div className={styles['money-info-item-value']}>
                                {moment().format('MMMM')} {moment().format('YYYY')}
                            </div>
                        </div>
                        <div className={styles['money-info-item']}>
                            <div className={styles['money-info-item-caption']}>
                                Остаток на начало месяца
                            </div>
                            <div className={styles['money-info-item-value']}>
                                {openingBalance ? formatNumber(openingBalance) : '-'} ₽
                            </div>
                        </div>
                        <div className={styles['money-info-item']}>
                            <div className={styles['money-info-item-caption']}>
                                Нам платили
                            </div>
                            <div className={styles['money-info-item-value']} style={{ color: '#3ac56c' }}>
                                {creditSum ? `+ ${formatNumber(creditSum)}` : '-'} ₽
                            </div>
                        </div>
                        <div className={styles['money-info-item']}>
                            <div className={styles['money-info-item-caption']}>
                                Мы платили
                            </div>
                            <div className={styles['money-info-item-value']} style={{ color: '#e94d4d' }}>
                                {debetSum ? `- ${formatNumber(debetSum)}` : '-'} ₽
                            </div>
                        </div>
                        <div className={styles['money-info-item']}>
                            <div className={styles['money-info-item-caption']}>
                                Доступные средства
                            </div>
                            <div className={styles['money-info-item-value']}>
                                {avaliableBalance ? formatNumber(avaliableBalance) : '-'} ₽
                            </div>
                        </div>
                    </div>
                    <Link to="/home/analytics">
                        <div className={styles['analytics-toggle']} />
                    </Link>
                </div>
                <div className={styles.balance}>
                    <span>{formatNumber(accountInfo.balance)} ₽</span>
                </div>
                <div className={styles['btn-row']}>
                    <div>
                        <Btn
                            caption="Заплатить"
                            bgColor="gradient"
                            style={{ width: 390, marginRight: 80 }}
                            onClick={() => {
                                /*
                                if (accountInfo.status === 'info') {
                                    showInfoModeNotification();
                                    return;
                                }
                                */
                                this.context.router.history.push('/home/payment');
                            }}
                            disabled={false && accountInfo.status === 'info'}
                        />
                        <Btn
                            caption="Выписка"
                            bgColor="white"
                            onClick={this.handleStatementBtnClick}
                            style={{ width: 140, marginRight: 30 }}
                        />
                        <Btn
                            caption="Реквизиты"
                            bgColor="white"
                            style={{ width: 140 }}
                            onClick={() => { this.context.router.history.push('/home/requisites'); }}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

export default AccountInfo;
