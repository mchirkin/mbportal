import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Btn from 'components/ui/Btn';
import { formatNumber } from 'utils';
import EditCard from '../EditCard';
import styles from './card-info.css';

class CardInfo extends Component {
    static propTypes = {
        /** список счетов организации */
        accounts: PropTypes.array,
        /** список карт организации */
        cards: PropTypes.array,
        selectedAccount: PropTypes.string,
        selectAccount: PropTypes.func.isRequired,
        setPaymentModalData: PropTypes.func.isRequired,
        fetchCards: PropTypes.func.isRequired,
    };

    static defaultProps = {
        accounts: [],
        cards: [],
        selectedAccount: '',
    };

    static contextTypes = {
        router: PropTypes.object.isRequired,
    };

    handleStatementBtnClick = () => {
        this.context.router.history.push('/home/statement');
    };

    transferToCard = () => {
        const { accounts, cards, selectedAccount } = this.props;
        const cardInfo =
            cards && cards.find(card => card.id === selectedAccount);

        let accountInfo = {};
        if (cardInfo) {
            accountInfo = accounts.find(acc => acc.number === cardInfo.number);
            // const number = cardInfo.cardNumber;
            // cardNumber = `
            //     ${number.substr(0,4)}
            //     \u2022\u2022\u2022\u2022 \u2022\u2022\u2022\u2022
            //     ${number.substr(number.length - 4)}
            // `;
            // cardName = cardInfo.alias || `\u2022\u2022\u2022\u2022 ${number.substr(number.length - 4)}`;
        }

        const sourceAccount = accounts.find(
            acc =>
                (acc.currency.toUpperCase() === 'RUB' ||
                    acc.currency.toUpperCase() === 'RUR') &&
                acc.type !== '7' &&
                acc.number !== accountInfo.number
        );

        if (sourceAccount) {
            this.props.selectAccount(sourceAccount.id);
        }

        this.props.setPaymentModalData({
            status: 'new',
            corrType: 'OWN',
            docNumber: '',
            corrAccNumber: accountInfo.number,
            accNumber: sourceAccount ? sourceAccount.number : null,
            // eslint-disable-next-line max-len
            description: `Перечисление денежных средств на карту ${
                cardInfo.cardNumber
            } ${cardInfo.cardSign}\nНДС не облагается`,
        });
        this.context.router.history.push('/home/payment');
    };

    transferFromSKS = () => {
        /*
        this.props.setPaymentModalData({
            status: 'new',
            docNumber: '',
            corrAccNumber: accountInfo.number,
            accNumber: sourceAccount ? sourceAccount.number : null,
            description:
                `Перечисление денежных средств на карту ${cardInfo.cardNumber}
                ${cardInfo.cardSign}\nНДС не облагается`,
        });
        */
        const { accounts, cards, selectedAccount } = this.props;
        const cardInfo =
            cards && cards.find(card => card.id === selectedAccount);

        let accountInfo = {};
        if (cardInfo) {
            accountInfo = accounts.find(acc => acc.number === cardInfo.number);
        }

        if (accountInfo.id) {
            this.props.selectAccount(accountInfo.id);
        }
        const destinationAccount = accounts.find(
            acc =>
                (acc.currency.toUpperCase() === 'RUB' ||
                    acc.currency.toUpperCase() === 'RUR') &&
                acc.type !== '7' &&
                acc.number !== accountInfo.number
        );

        if (destinationAccount) {
            this.props.setPaymentModalData({
                status: 'new',
                docNumber: '',
                corrAccNumber: destinationAccount.number,
                accNumber: accountInfo.number,
                description: 'Перевод собственных средств.\nНДС не  облагается',
            });
        }

        this.context.router.history.push('/home/payment');
    };

    render() {
        const { accounts, cards, selectedAccount, fetchCards } = this.props;
        let cardInfo = cards && cards.find(card => card.id === selectedAccount);

        let accountInfo = {};
        let cardNumber = '';
        let cardName = '';
        if (cardInfo) {
            accountInfo = accounts.find(acc => acc.number === cardInfo.number);
            const number = cardInfo.cardNumber;
            cardNumber = `
                ${number.substr(0, 4)}
                \u2022\u2022\u2022\u2022 \u2022\u2022\u2022\u2022
                ${number.substr(number.length - 4)}
            `;
            cardName =
                cardInfo.alias ||
                `\u2022\u2022\u2022\u2022 ${number.substr(number.length - 4)}`;
        }

        if (!cardInfo) {
            cardInfo = {};
        }

        return (
            <div className={styles.container}>
                <div className={styles.content}>
                    <div className={styles.head}>
                        <div className={styles.card}>
                            <div className={styles['card-number']}>
                                {cardNumber}
                            </div>
                            <div className={styles['card-expire']}>
                                {cardInfo.expire}
                            </div>
                            <div className={styles['card-holder']}>
                                {cardInfo.cardSign}
                            </div>
                        </div>
                        <div className={styles.description}>
                            <EditCard
                                cardName={cardName}
                                selectedAccount={selectedAccount}
                                fetchCards={fetchCards}
                            />
                            <div className={styles.balance}>
                                <div className={styles['balance-title']}>
                                    Баланс карты
                                </div>
                                <div className={styles['balance-value']}>
                                    {`${formatNumber(cardInfo.balance)} \u20bd`}
                                </div>
                            </div>
                            <div className={styles.account}>
                                <div className={styles['account-item']}>
                                    <div className={styles['account-title']}>
                                        Номер счета СКС
                                    </div>
                                    <div className={styles['account-value']}>
                                        {accountInfo.formattedNumber}
                                    </div>
                                </div>
                                <div className={styles['account-item']}>
                                    <div className={styles['account-title']}>
                                        Остаток по счету
                                    </div>
                                    <div className={styles['account-value']}>
                                        {`${formatNumber(
                                            accountInfo.balance
                                        )} \u20bd`}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={styles.buttons}>
                        <Btn
                            key="rechargeCard"
                            caption="Пополнить карту"
                            bgColor="gradient"
                            onClick={this.transferToCard}
                            style={{ marginRight: 20 }}
                        />
                        <Btn
                            key="transferToAccount"
                            caption="Перевести на счет"
                            onClick={this.transferFromSKS}
                            style={{ marginRight: 20 }}
                        />
                        <Btn
                            key="extract"
                            caption="Выписка"
                            bgColor="white"
                            onClick={this.handleStatementBtnClick}
                            style={{ width: 140, marginRight: 20 }}
                        />
                        <Btn
                            key="requisites"
                            caption="Реквизиты"
                            bgColor="white"
                            style={{ width: 140, marginRight: 20 }}
                            onClick={() => {
                                this.context.router.history.push(
                                    '/home/requisites'
                                );
                            }}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

export default CardInfo;
