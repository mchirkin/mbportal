import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { fetch } from 'utils';
import Btn from 'components/ui/Btn';
import styles from './edit-card.css';

export default class EditCard extends Component {
    static propTypes = {
        selectedAccount: PropTypes.string,
        cardName: PropTypes.string,
        fetchCards: PropTypes.func.isRequired,
    }

    static defaultProps = {
        selectedAccount: '',
        cardName: null,
    };

    constructor(props) {
        super(props);

        this.state = {
            cardName: null,
            editable: false,
            error: '',
        };
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.selectedAccount !== nextProps.selectedAccount) {
            this.setState({
                cardName: nextProps.cardName,
                editable: false,
            });
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.state.cardName !== prevState.cardName || prevProps.cardName !== this.props.cardName) {
            // this.setInputWidth();
        }
        // this.setInputWidth();
    }

    componentDidMount() {
        this.setInputWidth();
    }

    setNewCardName = cardName => fetch('/api/v1/products/change_alias', {
        method: 'POST',
        credentials: 'include',
        body: JSON.stringify({
            id: parseInt(this.props.selectedAccount, 10),
            alias: cardName,
        }),
    }).then((response) => {
        if (response.status === 200) {
            return {};
        }
        return response.json();
    }).then((result) => {
        if (result.errorText) {
            global.console.log(result.errorText);
            return Promise.reject(result.errorText);
        }
        return true;
    }).catch((err) => {
        global.console.log(err);
        this.setState({
            error: err.errorText || err,
        });
    })

    handleEditBtnClick = () => {
        this.setState({
            editable: true,
        }, () => {
            this.input.focus();
            this.input.setSelectionRange(this.input.value.length, this.input.value.length);
        });
    }

    handleCardNameChange = (e) => {
        this.setState({
            cardName: e.target.value,
        });
    }

    cancelEdit = () => {
        this.setState({
            editable: false,
            cardName: this.props.cardName,
        });
    }

    changeCardName = () => {
        this.setNewCardName(this.state.cardName)
            .then(() => {
                this.setState({
                    editable: false,
                });
                // reread card list
                this.props.fetchCards();
            })
            .catch((err) => {
                this.setState({
                    error: err,
                });
            });
    }

    setInputWidth = () => {
        const charNumber = this.state.cardName ? this.state.cardName.length : this.props.cardName.length;
        const width = 17 * charNumber;
        this.input.style.width = `${width}px`;
    }

    render() {
        const buttons = [
            <div
                key="editBtn"
                className={styles['edit-btn']}
                onClick={this.handleEditBtnClick}
            />,
            <div
                key="btnGroup"
                className={styles['btn-group']}
            >
                <Btn
                    key="cancel"
                    caption={
                        <div
                            style={{
                                position: 'relative',
                                top: 0,
                            }}
                        >
                            &times;
                        </div>
                    }
                    bgColor="white"
                    onClick={this.cancelEdit}
                    style={{
                        height: 30,
                        width: 30,
                        marginRight: 20,
                        padding: 0,
                        fontSize: 26,
                        lineHeight: 1,
                        borderRadius: 4,
                    }}
                />
                <Btn
                    key="submit"
                    caption="Подтвердить"
                    onClick={this.changeCardName}
                    style={{
                        height: 30,
                        fontSize: 12,
                        borderRadius: 4,
                    }}
                />
            </div>,
        ];

        const isActiveClass = this.state.editable ? ` ${styles['buttons_is-active']}` : '';

        return (
            <div>
                <div className={styles['edit-wrapper']}>
                    <input
                        className={styles.cardname}
                        value={this.state.cardName === null ? this.props.cardName : this.state.cardName}
                        onChange={this.handleCardNameChange}
                        readOnly={!this.state.editable}
                        data-active={this.state.editable}
                        ref={(el) => { this.input = el; }}
                    />
                    <div className={`${styles.buttons}${isActiveClass}`}>
                        {buttons}
                    </div>
                </div>
                <div className={styles.error}>
                    {this.state.error}
                </div>
            </div>
        );
    }
}
