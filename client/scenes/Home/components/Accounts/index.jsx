import React from 'react';
import PropTypes from 'prop-types';
import { Scrollbars } from 'react-custom-scrollbars';

import AccountContainer from './containers/AccountContainer';

import styles from './accounts.css';

/**
 * Список счетов и карт клиента
 */
const Accounts = ({ accounts, cards, selectedAccount, selectAccount, goToHomePage, orgInfo }) => (
    <div className={styles.wrapper}>
        <Scrollbars>
            <div className={styles['accounts-list']}>
                {accounts && cards
                    ? [...accounts, ...cards].map(acc => (
                        <AccountContainer
                            acc={acc}
                            accounts={accounts}
                            orgInfo={orgInfo}
                            selectedAccount={selectedAccount}
                            selectAccount={selectAccount}
                            goToHomePage={goToHomePage}
                            key={acc.id}
                        />
                    ))
                    : null}
            </div>
        </Scrollbars>
    </div>
);

Accounts.propTypes = {
    /** список счетов клиента */
    accounts: PropTypes.array,
    /** список карт клиента */
    cards: PropTypes.array,
    selectedAccount: PropTypes.string,
    selectAccount: PropTypes.func.isRequired,
    goToHomePage: PropTypes.func.isRequired,
    orgInfo: PropTypes.object,
};

Accounts.defaultProps = {
    accounts: [],
    cards: [],
    selectedAccount: '',
    orgInfo: {},
};

export default Accounts;
