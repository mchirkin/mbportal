import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { DragSource, DropTarget } from 'react-dnd';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { fetch } from 'utils';

import setPaymentModalData from 'modules/payments/payment_modal/actions/set_data';

import AccountView from '../../components/AccountView';

const dragSource = {
    beginDrag(props, monitor, component) {
        // Return the data describing the dragged item
        const item = { id: props.id };
        return item;
    },

    endDrag(props, monitor, component) {
        if (!monitor.didDrop()) {
            // You can check whether the drop was successful
            // or if the drag ended but nobody handled the drop
            return;
        }

        // When dropped on a compatible target, do something.
        // Read the original dragged item from getItem():
        const item = monitor.getItem();

        // You may also read the drop result from the drop target
        // that handled the drop, if it returned an object from
        // its drop() method.
        const dropResult = monitor.getDropResult();
        console.log(dropResult, component);

        const dropTarget = dropResult.component;
        console.log(dropTarget);
        if (dropTarget.props.acc.cardNumber && !component.props.acc.cardNumber) {
            console.log('transfer to card!', item);
            dropTarget.getWrappedInstance().transferToCard(component.props.acc, dropTarget.props.acc);
        }
        if (!dropTarget.props.acc.cardNumber && component.props.acc.cardNumber) {
            dropTarget.getWrappedInstance().transferFromSKS(dropTarget.props.acc, component.props.acc);
        }
    }
};

const dropTarget = {
    drop(props, monitor, component) {
        if (monitor.didDrop()) {
            // If you want, you can check whether some nested
            // target already handled drop
            return;
        }

        // Obtain the dragged item
        const item = monitor.getItem();

        // You can also do nothing and return a drop result,
        // which will be available as monitor.getDropResult()
        // in the drag source's endDrag() method
        return { component };
    }
};

/**
 * Контейнер account
 */
@DragSource('ACCOUNT', dragSource, (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    connectDragPreview: connect.dragPreview(),
    isDragging: monitor.isDragging()
}))
@DropTarget('ACCOUNT', dropTarget, (connect, monitor) => ({
    // Call this function inside render()
    // to let React DnD handle the drag events:
    connectDropTarget: connect.dropTarget(),
    // You can ask the monitor about the current drag state:
    isOver: monitor.isOver(),
    isOverCurrent: monitor.isOver({ shallow: true }),
    canDrop: monitor.canDrop(),
    itemType: monitor.getItemType(),
}))
@connect(
    null,
    (dispatch) => ({
        setPaymentModalData: bindActionCreators(setPaymentModalData, dispatch),
    }),
    null,
    {
        withRef: true,
    }
)
export default class AccountContainer extends PureComponent {
    static contextTypes = {
        router: PropTypes.object.isRequired,
    };

    static propTypes = {
        orgInfo: PropTypes.object.isRequired,
        selectedAccount: PropTypes.string,
        accounts: PropTypes.array,
        acc: PropTypes.object,
    };

    static defaultProps = {
        selectedAccount: '',
        accounts: [],
        acc: {},
    };

    constructor(props) {
        super(props);

        this.state = {
            accountsLimits: null,
        };
    }

    componentWillMount() {
        // if (!/mb\.rosevrobank\.ru/.test(document.location.origin)) {
        this.checkAndFetchLimits();
        // }
    }

    componentDidUpdate() {
        // if (!/mb\.rosevrobank\.ru/.test(document.location.origin)) {
        this.checkAndFetchLimits();
        // }
    }

    checkAndFetchLimits = () => {
        if (this.props.orgInfo && this.props.selectedAccount && !this.state.accountsLimits) {
            const accountInfo = this.props.accounts.find(acc => acc.id === this.props.acc.id);
            if (accountInfo && !this.limitsIsFetching && !accountInfo.cardNumber) {
                this.accountsLimitsFetch(accountInfo.number, this.props.orgInfo.branchCodeRef);
            }
        }
    }

    accountsLimitsFetch = (account, branch) => {
        if (this.limitsIsFetching) {
            return;
        }

        const url = `/api/v1/products/account_limits?account=${account}&branch=${branch}`;
        const options = {
            credentials: 'include',
        };

        this.limitsIsFetching = true;

        return fetch(url, options)
            .then(response => response.json())
            .then((json) => {
                if (json) {
                    this.setState({
                        accountsLimits: json,
                    }, () => {
                        this.limitsIsFetching = false;
                    });
                }
            });
    }

    transferToCard = (acc, card) => {
        this.props.setPaymentModalData({
            status: 'new',
            corrType: 'OWN',
            docNumber: '',
            corrAccNumber: card.number,
            accNumber: acc.number,
            // eslint-disable-next-line max-len
            description: `Перечисление денежных средств на карту ${card.cardNumber} ${card.cardSign}\nНДС не облагается`,
        });
        this.context.router.history.push('/home/payment');
    }

    transferFromSKS = (acc, sks) => {
        this.props.setPaymentModalData({
            status: 'new',
            docNumber: '',
            corrAccNumber: acc.number,
            accNumber: sks.number,
            description: 'Перевод собственных средств.\nНДС не  облагается',
        });

        this.context.router.history.push('/home/payment');
    }

    render() {
        const {
            connectDragSource,
            connectDropTarget,
            connectDragPreview,
            isDragging,
        } = this.props;

        return connectDragPreview(
            connectDragSource(
                connectDropTarget(
                    <div>
                        <AccountView
                            accountsLimits={this.state.accountsLimits}
                            {...this.props}
                        />
                    </div>
                )
            )
        );
    }
}
