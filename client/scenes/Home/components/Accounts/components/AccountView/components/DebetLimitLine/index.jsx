import React from 'react';
import PropTypes from 'prop-types';

import { formatNumber } from 'utils';

import styles from './debet_limit_line.css';

const sumToNumber = number => number && parseFloat(number.replace(',', '.'));

/**
 * Шкала дебетового лимита
 */

const DebetLimitLine = ({ accountsLimits }) => {
    if (accountsLimits && accountsLimits.error_code) return null;

    let availableSum = null;
    let progressWidth = 0;

    if (accountsLimits) {
        availableSum = sumToNumber(accountsLimits.total_limit) - sumToNumber(accountsLimits.fact_debit_sum);
        progressWidth = (
            sumToNumber(accountsLimits.fact_debit_sum) /
            (sumToNumber(accountsLimits.total_limit) / 100)
        ).toFixed(1);
    }

    progressWidth = (progressWidth <= 2 && progressWidth > 0) ? 2 : progressWidth;

    return (
        <div
            className={styles.progress}
            style={{ marginTop: 6 }}
        >
            <div className={styles.progress__content}>
                {
                    availableSum === 0
                        ? (
                            <div className={styles.progress__error} />
                        )
                        : (
                            <div
                                className={styles.progress__line}
                                style={{ width: `${progressWidth}%` }}
                            />
                        )
                }
            </div>
            {accountsLimits && (
                <div className={styles.progress__tooltip}>
                    {availableSum === 0
                        ? (
                            <div
                                className={`
                                    ${styles['progress__tooltip-text']}
                                    ${styles['progress__tooltip-text_error']}
                                `}
                            >
                                Превышен лимит на совершение расходных операций по вашему тарифному плану
                            </div>
                        )
                        : (
                            <div className={styles['progress__tooltip-text']}>
                                Дебетовый оборот: {accountsLimits ? formatNumber(availableSum.toFixed(2)) : '-'} ₽
                            </div>
                        )
                    }
                    <div className={styles['progress__tooltip-text']}>
                        Лимит с учетом исполненных платежей:<br />
                        {accountsLimits ? formatNumber(accountsLimits.fact_debit_sum) : '-'} ₽ /&nbsp;
                        {accountsLimits ? formatNumber(accountsLimits.total_limit) : '-'} ₽
                    </div>
                    <div className={styles['progress__tooltip-text']}>
                        Лимит с учетом проведенных и находящихся в обработке платежей:<br />
                        {accountsLimits ? formatNumber(accountsLimits.planned_debit_sum) : '-'} ₽ /&nbsp;
                        {accountsLimits ? formatNumber(accountsLimits.total_limit) : '-'} ₽
                    </div>
                </div>
            )}
        </div>
    );
};

DebetLimitLine.propTypes = {
    /** данные по лимиту и остатку */
    accountsLimits: PropTypes.object,
};

DebetLimitLine.defaultProps = {
    accountsLimits: {},
};


export default DebetLimitLine;
