import React from 'react';
import PropTypes from 'prop-types';

import { formatNumber } from 'utils';

import DebetLimitLine from './components/DebetLimitLine';

import styles from './account.css';

/**
 */
const AccountView = ({
    acc,
    selectAccount,
    goToHomePage,
    selectedAccount,
    accountsLimits,
    isDragging,
    isOver,
}) => {
    let icon = (<div className={`${styles.icon} ${styles['rub-account']}`} />);

    if (acc.cardNumber) {
        switch (acc.cardNumber[0]) {
            case '4':
                icon = (<div className={`${styles.icon} ${styles['card-visa']}`} />);
                break;
            case '5':
                icon = (<div className={`${styles.icon} ${styles['card-mc']}`} />);
                break;
        }
    }

    return (
        <div
            className={styles.account}
            onClick={() => {
                selectAccount(acc.id);
                goToHomePage(!!acc.cardNumber);
            }}
            style={{
                backgroundColor: isOver ? '#e8e8e8' : undefined,
                opacity: isDragging ? 0.5 : 1,
            }}
            data-selected={selectedAccount === acc.id}
        >
            <div className={styles['account-name']}>
                {icon}
                {acc.cardNumber
                    ? (
                        <span>
                            {
                                acc.alias ||
                                (
                                    <div>
                                        &bull;&bull;&bull;&bull;
                                        {acc.cardNumber.substr(acc.cardNumber.length - 4)}
                                    </div>
                                )
                            }
                        </span>
                    )
                    : (
                        <span>{acc.alias || acc.typeCaption}</span>
                    )}
            </div>
            <div className={styles['account-balance']}>
                {`${formatNumber(acc.balance)} \u20bd`}
            </div>
            {!acc.cardNumber
                ? (
                    <DebetLimitLine
                        accountsLimits={accountsLimits}
                    />
                )
                : null
            }
        </div>
    );
};

AccountView.propTypes = {
    orgInfo: PropTypes.object.isRequired,
    selectAccount: PropTypes.func.isRequired,
    selectedAccount: PropTypes.string,
    accounts: PropTypes.array,
    acc: PropTypes.object,
    goToHomePage: PropTypes.func.isRequired,
    accountsLimits: PropTypes.object,
};

AccountView.defaultProps = {
    selectedAccount: '',
    accounts: [],
    acc: {},
    accountsLimits: {},
};

export default AccountView;
