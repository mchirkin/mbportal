import React from 'react';
import PropTypes from 'prop-types';
import { formatNumber } from 'utils';
import Btn from 'components/ui/Btn';
import styles from './favorite-template.css';

const FavoriteTemplate = ({ data }) => (
    <div className={styles.wrapper}>
        <div className={styles['template-wrapper']}>
            <div className={styles['template-name']}>
                {data.template.name}
            </div>
            <div className={styles['template-sum']}>
                {data.platporDocument && data.platporDocument.amount
                    ? `${formatNumber(data.platporDocument.amount)} \u20bd`
                    : null}
            </div>
            <div className={styles.actions}>
                <div>
                    <div className={styles['drag-n-drop']} />
                    <div className={styles.delete} />
                </div>
            </div>
        </div>
        <div className={styles['sign-btn']}>
            <Btn
                caption="Подписать и отправить"
                color="gradient"
                style={{
                    width: 200,
                    height: 30,
                    fontSize: 12,
                    borderRadius: 4,
                }}
            />
        </div>
    </div>
);

FavoriteTemplate.propTypes = {
    data: PropTypes.object,
};

FavoriteTemplate.defaultProps = {
    data: {},
};

export default FavoriteTemplate;
