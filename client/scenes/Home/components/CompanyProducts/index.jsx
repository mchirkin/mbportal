import React from 'react';
import PropTypes from 'prop-types';
import Company from '../Company';
import Accounts from '../Accounts';
import styles from './company-products.css';

const CompanyProducts = ({
    orgInfo,
    accounts,
    cards,
    selectedAccount,
    selectAccount,
    goToHomePage,
}) => {
    const filteredAccounts = accounts
        ? accounts.filter((acc) => {
            const isRubleAcc = acc.currency.toLowerCase() === 'rur' || acc.currency.toLowerCase() === 'rub';
            const isSKS = acc.type === '7';

            return isRubleAcc && !isSKS;
        })
        : accounts;

    return (
        <div className={styles.wrapper}>
            <Company data={orgInfo} />
            <Accounts
                accounts={filteredAccounts}
                cards={cards}
                selectedAccount={selectedAccount}
                selectAccount={selectAccount}
                goToHomePage={goToHomePage}
                orgInfo={orgInfo}
            />
        </div>
    );
};

CompanyProducts.propTypes = {
    /** данные об организации */
    orgInfo: PropTypes.object,
    /** список счетов организации */
    accounts: PropTypes.array,
    /** список карт организации */
    cards: PropTypes.array,
    selectedAccount: PropTypes.string,
    selectAccount: PropTypes.func.isRequired,
    goToHomePage: PropTypes.func.isRequired,
};

CompanyProducts.defaultProps = {
    orgInfo: {},
    accounts: [],
    cards: [],
    selectedAccount: '',
};

export default CompanyProducts;
