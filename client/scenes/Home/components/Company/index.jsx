import React from 'react';
import PropTypes from 'prop-types';
import Marquee from 'components/ui/Marquee';
import styles from './company.css';

const Company = ({ data }) => (
    <div className={styles.company}>
        <Marquee text={data ? data.shortName || data.fullName : ''} />
    </div>
);

Company.propTypes = {
    /** данные об организации */
    data: PropTypes.object,
};

Company.defaultProps = {
    data: {},
};

export default Company;
