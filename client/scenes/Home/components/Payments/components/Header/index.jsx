import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { setTimeout } from 'timers';
import { DayPickerRangeController } from 'react-dates';
import CalendarBottom from 'components/CalendarBottom';
import styles from './header.css';

class Header extends Component {
    static propTypes = {
        startDate: PropTypes.object,
        endDate: PropTypes.object,
        clearDateFilter: PropTypes.func.isRequired,
        acceptDateFilter: PropTypes.func.isRequired,
        changeType: PropTypes.func.isRequired,
        changeStatus: PropTypes.func.isRequired,
        filterPaymentType: PropTypes.string,
        filterPaymentStatus: PropTypes.string,
        handleSearchChange: PropTypes.func.isRequired,
        search: PropTypes.string,
    }

    static defaultProps = {
        startDate: null,
        endDate: null,
        filterPaymentType: '',
        filterPaymentStatus: '',
        search: '',
    }

    constructor(props) {
        super(props);

        this.state = {
            startDate: null,
            endDate: null,
            focusedInput: 'startDate',
            showCalendar: false,
        };
    }

    componentWillMount() {
        if (this.props.startDate && this.props.endDate) {
            this.setState({
                startDate: this.props.startDate,
                endDate: this.props.endDate,
            });
        }
    }

    showCalendar = (e) => {
        const id = e.target.getAttribute('id');
        this.setState({
            focusedInput: id,
            showCalendar: true,
        });
    }

    handleDatesChange = ({ startDate, endDate }) => {
        this.setState({
            startDate,
            endDate,
        });
    }

    handlePeriodClear = () => {
        this.setState({
            startDate: null,
            endDate: null,
            showCalendar: false,
        });
        this.props.clearDateFilter();
    }

    handlePeriodSubmit = () => {
        this.setState({
            showCalendar: false,
        });

        this.props.acceptDateFilter({
            startDate: this.state.startDate,
            endDate: this.state.endDate,
        });
    }

    handlePeriodSet = (firstDate, secondDate) => {
        this.setState({
            startDate: firstDate,
            endDate: secondDate,
            showCalendar: false,
        });

        this.props.acceptDateFilter({
            startDate: firstDate,
            endDate: secondDate,
        });
    }

    onFocusChange = (focusedInput) => {
        this.setState({
            focusedInput: !focusedInput ? 'startDate' : focusedInput,
        });
    }

    render() {
        const { startDate, endDate } = this.state;

        const startDateString = startDate && startDate.format('DD.MM.YYYY') || '';
        const endDateString = endDate && endDate.format('DD.MM.YYYY') || '';

        const {
            changeType,
            changeStatus,
            filterPaymentType,
            filterPaymentStatus,
            handleSearchChange,
            search,
        } = this.props;

        return (
            <div className={styles.filter}>
                <div className={styles.filter__section}>
                    <div className={styles.filter__item}>
                        <div className={styles.filter__toggles}>
                            <div
                                className={styles.filter__toggle}
                                onClick={() => { changeType('all'); }}
                                data-active={filterPaymentType === 'all'}
                            >
                                Все платежи
                            </div>
                            <div
                                className={styles.filter__toggle}
                                onClick={() => { changeType('outcome'); }}
                                data-active={filterPaymentType === 'outcome'}
                            >
                                Мы платили
                            </div>
                            <div
                                className={styles.filter__toggle}
                                onClick={() => { changeType('income'); }}
                                data-active={filterPaymentType === 'income'}
                            >
                                Нам платили
                            </div>
                        </div>
                    </div>
                    <div className={styles.filter__item}>
                        <div className={`${styles.filter__toggles} ${styles.filter__toggles_hover}`}>
                            <div className={styles.filter__caption}>
                                Статус:
                            </div>
                            <div
                                className={styles.filter__toggle}
                                onClick={() => { changeStatus('all'); }}
                                data-active={filterPaymentStatus === 'all'}
                            >
                                Все платежи
                            </div>
                            <div
                                className={styles.filter__toggle}
                                onClick={() => { changeStatus('send'); }}
                                data-active={filterPaymentStatus === 'send'}
                            >
                                В обработке
                            </div>
                            <div
                                className={styles.filter__toggle}
                                onClick={() => { changeStatus('end'); }}
                                data-active={filterPaymentStatus === 'end'}
                            >
                                Исполненные
                            </div>
                            <div
                                className={styles.filter__toggle}
                                onClick={() => { changeStatus('decline'); }}
                                data-active={filterPaymentStatus === 'decline'}
                            >
                                Отказ
                            </div>
                        </div>
                    </div>
                </div>
                <div className={styles.filter__section}>
                    <div className={`${styles.filter__item}`}>
                        <div className={`${styles.filter__period} ${styles.period}`}>
                            <div className={styles.filter__caption}>
                                Период:
                            </div>
                            <span style={{ marginRight: 5 }}>
                                с
                            </span>
                            <div className={styles.period__date}>
                                <input
                                    id="startDate"
                                    className={styles.period__input}
                                    type="text"
                                    name="start date"
                                    value={startDateString}
                                    readOnly
                                    onClick={this.showCalendar}
                                />
                                <div
                                    id="startDate"
                                    className={styles.calendar}
                                    onClick={this.showCalendar}
                                />
                            </div>
                            <span style={{ marginLeft: 0, marginRight: 5 }}>
                                по
                            </span>
                            <div className={styles.period__date}>
                                <input
                                    id="endDate"
                                    className={styles.period__input}
                                    type="text"
                                    name="end date"
                                    value={endDateString}
                                    readOnly
                                    onClick={this.showCalendar}
                                />
                                <div
                                    id="endDate"
                                    className={styles.calendar}
                                    onClick={this.showCalendar}
                                />
                            </div>

                            {this.state.showCalendar
                                ? (
                                    <div className={styles.period__daypicker}>
                                        <DayPickerRangeController
                                            startDate={this.state.startDate}
                                            endDate={this.state.endDate}
                                            focusedInput={this.state.focusedInput}
                                            onFocusChange={this.onFocusChange}
                                            onDatesChange={this.handleDatesChange}
                                            firstDayOfWeek={1}
                                            isOutsideRange={() => false}
                                            numberOfMonths={2}
                                            minimumNights={0}
                                            daySize={35}
                                            onOutsideClick={() => this.setState({ showCalendar: false })}
                                            renderCalendarInfo={() => (
                                                <CalendarBottom
                                                    onSubmit={this.handlePeriodSubmit}
                                                    onCancel={this.handlePeriodClear}
                                                    onSetPeriod={this.handlePeriodSet}
                                                />
                                            )}
                                            hideKeyboardShortcutsPanel
                                        />
                                    </div>
                                )
                                : null}
                        </div>
                    </div>
                    <div
                        className={`${styles.filter__item} ${styles.search}`}
                        onFocus={(e) => {
                            if (window.innerWidth < 1500) {
                                const searchNode = e.target.parentNode;

                                searchNode.style.width = `${searchNode.offsetWidth}px`;
                                searchNode.dataset.blurWidth = `${searchNode.offsetWidth}px`;
                                searchNode.classList.add(styles['search_is-absolute']);
                                searchNode.classList.add(styles['search_is-focus']);

                                setTimeout(() => {
                                    searchNode.style.transition = 'width 0.2s ease-in-out';
                                    searchNode.style.width = '40%';
                                }, 0);
                            }
                        }}
                        onBlur={(e) => {
                            const searchNode = e.target.parentNode;

                            searchNode.style.width = searchNode.dataset.blurWidth;
                            searchNode.classList.remove(styles['search_is-focus']);

                            setTimeout(() => {
                                searchNode.style.transition = '';
                                searchNode.style.width = '100%';
                                searchNode.removeAttribute('data-blur-width');
                                searchNode.classList.remove(styles['search_is-absolute']);
                            }, 200);
                        }}
                    >
                        <div className={styles.search__icon} />
                        <input
                            className={styles.search__source}
                            placeholder="Поиск"
                            value={search}
                            onChange={handleSearchChange}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

export default Header;
