import React from 'react';
import PropTypes from 'prop-types';
import { formatNumber } from 'utils';
import styles from './payment-item.css';

const PaymentItem = ({
    data,
    viewPayment,
    copyDocument,
    savePDF,
    cancelDocument,
}) => {
    let docData = data;
    if (data.platporDocument) {
        docData = data.platporDocument;
    }

    return (
        <div className={styles.wrapper} onClick={() => { viewPayment(data); }}>
            <div className={styles['info-item']} style={{ width: 100, minWidth: 100 }}>
                <div>
                    № {docData.docNumber}
                </div>
                <div className={styles['payment-status']} data-status={docData.status}>
                    {docData.statusCaption || ''}
                </div>
            </div>
            <div
                className={`${styles['info-item']} ${styles['info-item-desc']}`}
                style={{ flexGrow: 1 }}
            >
                <div>
                    {docData.lineIsDebet === '0' || docData.lineIsDebet === 'true'
                        ? docData.fullname
                        : docData.corrFullname}
                </div>
                <div className={styles['payment-description']}>
                    {docData.lineDescription || docData.description}
                </div>
            </div>
            <div className={`${styles['info-item']} ${styles['info-item-amount']}`}>
                <div>
                    {docData.lineIsDebet !== '0' && docData.lineIsDebet !== 'true'
                        ? (
                            <span className={styles.minus}>
                                {`- ${formatNumber(docData.amount)} \u20bd`}
                            </span>
                        )
                        : (
                            <span className={styles.plus}>
                                {`+ ${formatNumber(docData.amount)} \u20bd`}
                            </span>
                        )}
                </div>
            </div>
            <div className={styles.actions}>
                {data.platporDocument
                    ? (
                        <div
                            className={styles.copy}
                            title="Повторить"
                            onClick={(e) => {
                                e.stopPropagation();
                                copyDocument(docData);
                            }}
                        />
                    )
                    : null}
                {docData.id
                    ? (
                        <div
                            className={styles.print}
                            title="Сохранить в pdf"
                            onClick={(e) => {
                                e.stopPropagation();
                                savePDF(docData);
                            }}
                        />
                    )
                    : null}
                {docData.id && /send/.test(docData.status)
                    ? (
                        <div
                            className={styles.cancel}
                            title="Отозвать"
                            onClick={(e) => {
                                e.stopPropagation();
                                cancelDocument(docData);
                            }}
                        />
                    )
                    : null}
            </div>
        </div>
    );
};

PaymentItem.propTypes = {
    data: PropTypes.object,
    viewPayment: PropTypes.func.isRequired,
    copyDocument: PropTypes.func.isRequired,
    savePDF: PropTypes.func.isRequired,
    cancelDocument: PropTypes.func.isRequired,
};

PaymentItem.defaultProps = {
    data: {},
};

export default PaymentItem;
