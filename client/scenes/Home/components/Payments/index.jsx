import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import Loader from 'components/Loader';
import Btn from 'components/ui/Btn';

import Header from './components/Header';
import PaymentItem from './components/PaymentItem';
import styles from './payments.css';

function getPaymentList(list, props) {
    const paymentListSorted = list.sort((a, b) => {
        const aDate = a.platporDocument ? new Date(a.platporDocument.docDate) : new Date(a.docDate);
        const bDate = b.platporDocument ? new Date(b.platporDocument.docDate) : new Date(b.docDate);

        if (bDate.getTime() - aDate.getTime() === 0) {
            const aId = a.platporDocument ? a.platporDocument.id : a.id;
            const bId = b.platporDocument ? b.platporDocument.id : b.id;

            if (!aId || !bId) {
                return 0;
            }
            return parseInt(bId, 10) - parseInt(aId, 10);
        }
        return bDate.getTime() - aDate.getTime();
    });

    return paymentListSorted && paymentListSorted.length > 0
        ? paymentListSorted.map((item, i) => (
            <PaymentItem
                key={item.id || i}
                data={item}
                {...props}
            />
        ))
        : null;
}

const Payments = ({
    accNumber,
    list,
    endPayments,
    sendPayments,
    errorPayments,
    viewPayment,
    copyDocument,
    savePDF,
    cancelDocument,
    changeType,
    changeStatus,
    acceptDateFilter,
    clearDateFilter,
    filterPaymentType,
    filterPaymentStatus,
    withPeriod,
    startDate,
    endDate,
    handleSearchChange,
    search,
    isFetching,
    count,
    handleLoadMoreBtnClick,
    saveRenderedPaymentsCount,
}) => {
    let paymentList = [];

    if (list) {
        if (list.lines) {
            const lines = Array.isArray(list.lines) ? list.lines : [list.lines];
            paymentList.push(...lines);
        } else {
            paymentList.push(...list);
        }
    }

    if (endPayments && !withPeriod) {
        paymentList.push(...endPayments);
    }

    if (sendPayments) {
        paymentList.push(...sendPayments);
    }

    if (errorPayments) {
        paymentList.push(...errorPayments);
    }

    if (filterPaymentStatus === 'end' && !withPeriod) {
        paymentList = endPayments;
    }

    if (filterPaymentStatus === 'end' && withPeriod) {
        paymentList = list.lines || [];
    }

    if (filterPaymentStatus === 'send') {
        paymentList = sendPayments;
    }

    if (filterPaymentStatus === 'decline') {
        paymentList = errorPayments;
    }

    const paymentsObject = {};

    let paymentCount = isFetching ? count - 10 : count;
    if (filterPaymentStatus === 'send' || filterPaymentStatus === 'decline' || withPeriod) {
        paymentCount = paymentList ? paymentList.length : 0;
    }

    const paymentListSorted = paymentList
        .filter((payment) => {
            if (payment.platporDocument && payment.platporDocument.accNumber !== accNumber) {
                return false;
            }

            const isOutcome = !!payment.platporDocument || payment.line_is_debet === '1';

            if (filterPaymentType === 'income') {
                return !isOutcome;
            }

            if (payment.platporDocument && withPeriod) {
                const docDate = moment(payment.platporDocument.docDate, 'YYYY-MM-DD');
                const startPeriod = moment(startDate).subtract(1, 'days');
                const endPeriod = moment(endDate).add(1, 'days');
                return docDate.isBetween(startPeriod, endPeriod);
            }

            if (filterPaymentType === 'outcome') {
                return isOutcome;
            }

            return true;
        })
        .filter((payment) => {
            if (!search) return true;

            const searchValue = search.toLowerCase();

            if (payment.platporDocument) {
                if (payment.platporDocument.corrFullname.toLowerCase().includes(searchValue)) {
                    return true;
                }
                if (payment.platporDocument.description.toLowerCase().includes(searchValue)) {
                    return true;
                }
            }

            if (payment.doc_date) {
                const corrName = payment.line_is_debet === '0' ? payment.fullname : payment.corr_fullname;
                if (corrName.toLowerCase().includes(searchValue)) {
                    return true;
                }

                const description = payment.line_description || payment.description;
                if (description.toLowerCase().includes(searchValue)) {
                    return true;
                }
            }

            if (!payment.platporDocument && !payment.doc_date) {
                const corrName = payment.lineIsDebet === '0' || payment.lineIsDebet === 'true'
                    ? payment.fullname
                    : payment.corrFullname;
                if (corrName.toLowerCase().includes(searchValue)) {
                    return true;
                }

                const description = payment.lineDescription || payment.description;
                if (description.toLowerCase().includes(searchValue)) {
                    return true;
                }
            }
            return false;
        })
        .sort((a, b) => {
            let aDate;
            let bDate;

            if (a.docDate) {
                aDate = a.docDate;
            }
            if (a.platporDocument) {
                aDate = a.platporDocument.docDate;
            }
            if (a.doc_date) {
                aDate = a.doc_date;
            }

            if (b.docDate) {
                bDate = b.docDate;
            }
            if (b.platporDocument) {
                bDate = b.platporDocument.docDate;
            }
            if (b.doc_date) {
                bDate = b.doc_date;
            }

            return (new Date(bDate)).getTime() - (new Date(aDate)).getTime();
        });

    saveRenderedPaymentsCount(paymentListSorted.length);

    paymentListSorted
        .slice(0, paymentCount)
        .forEach((payment) => {
            const fromStatement = payment.doc_date;

            let paymentData = {};
            if (fromStatement) {
                Object.keys(payment).forEach((key) => {
                    const parts = key.split('_').map((part, index) => {
                        if (index > 0) {
                            return part[0].toUpperCase() + part.slice(1);
                        }
                        return part;
                    });
                    let propName = parts.join('');
                    propName = propName === 'linePaymentUrgent' ? 'urgenttype' : propName;
                    propName = propName === 'lineTax' ? 'tax1' : propName;
                    propName = propName === 'lineTaxDocNum' ? 'taxdocnum' : propName;
                    propName = propName === 'lineTaxDocDate' ? 'taxdocdate' : propName;
                    propName = propName === 'lineTaxType' ? 'taxtype' : propName;
                    propName = propName === 'lineUid' ? 'uin' : propName;
                    paymentData[propName] = payment[key];
                });
            } else {
                paymentData = payment;
            }

            const paymentDate = paymentData.platporDocument ? paymentData.platporDocument.docDate : paymentData.docDate;
            if (!paymentsObject[paymentDate]) {
                paymentsObject[paymentDate] = [paymentData];
            } else {
                paymentsObject[paymentDate].push(paymentData);
            }
        });

    const dates = Object.keys(paymentsObject).sort((a, b) => {
        const aDate = new Date(a);
        const bDate = new Date(b);

        return bDate.getTime() - aDate.getTime();
    });

    return (
        <div className={styles.wrapper}>
            <div className={styles['section-name']}>
                Движение денежных средств
            </div>
            <div className={styles['list-wrapper']}>
                <Header
                    changeType={changeType}
                    changeStatus={changeStatus}
                    filterPaymentType={filterPaymentType}
                    filterPaymentStatus={filterPaymentStatus}
                    acceptDateFilter={acceptDateFilter}
                    clearDateFilter={clearDateFilter}
                    handleSearchChange={handleSearchChange}
                    search={search}
                    startDate={startDate}
                    endDate={endDate}
                />
                {dates && dates.length > 0
                    ? dates.map(date => (
                        <div key={date}>
                            <div className={styles['statement-date']}>
                                {moment(date).format('DD.MM.YYYY')}
                            </div>
                            {getPaymentList(
                                paymentsObject[date],
                                {
                                    viewPayment,
                                    copyDocument,
                                    savePDF,
                                    cancelDocument,
                                })}
                        </div>
                    ))
                    : (
                        <div className={styles['no-data']}>
                            Нет доступных операций
                        </div>
                    )}
                {isFetching && count > 10 && (
                    <div style={{ position: 'relative', width: '100%', height: 200, marginBottom: 14 }}>
                        <Loader visible={isFetching} />
                    </div>
                )}
                {!isFetching && paymentListSorted && paymentListSorted.length >= count && (
                    <div
                        style={{
                            display: 'flex',
                            justifyContent: 'center',
                            marginTop: 15,
                        }}
                    >
                        <Btn
                            caption="Загрузить еще"
                            bgColor="grey"
                            onClick={handleLoadMoreBtnClick}
                            style={{
                                width: 190,
                                height: 36,
                                fontSize: 14,
                                border: '1px solid #d8d8d8',
                            }}
                        />
                    </div>
                )}
            </div>
        </div>
    );
};

Payments.propTypes = {
    accNumber: PropTypes.string,
    list: PropTypes.oneOfType([
        PropTypes.array,
        PropTypes.object,
    ]),
    endPayments: PropTypes.array,
    sendPayments: PropTypes.array,
    errorPayments: PropTypes.array,
    viewPayment: PropTypes.func.isRequired,
    copyDocument: PropTypes.func.isRequired,
    savePDF: PropTypes.func.isRequired,
    changeType: PropTypes.func.isRequired,
    changeStatus: PropTypes.func.isRequired,
    acceptDateFilter: PropTypes.func.isRequired,
    clearDateFilter: PropTypes.func.isRequired,
    filterPaymentType: PropTypes.string,
    filterPaymentStatus: PropTypes.string,
    withPeriod: PropTypes.object,
    startDate: PropTypes.object,
    endDate: PropTypes.object,
    handleSearchChange: PropTypes.func.isRequired,
    search: PropTypes.string,
    cancelDocument: PropTypes.func.isRequired,
    isFetching: PropTypes.bool,
    count: PropTypes.number,
    handleLoadMoreBtnClick: PropTypes.func.isRequired,
    saveRenderedPaymentsCount: PropTypes.func.isRequired,
};

Payments.defaultProps = {
    accNumber: '',
    list: [],
    endPayments: [],
    sendPayments: [],
    errorPayments: [],
    filterPaymentType: '',
    filterPaymentStatus: '',
    withPeriod: null,
    startDate: null,
    endDate: null,
    search: '',
    isFetching: false,
    count: 10,
};

export default Payments;
