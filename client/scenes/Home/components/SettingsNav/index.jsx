import React from 'react';
import { NavLink as Link } from 'react-router-dom';
import styles from './settings-nav.css';

const SettingsNav = () => (
    <div className={styles.wrapper}>
        <div className={styles['nav-header']}>
            Настройки
        </div>
        <Link
            to="/home/settings/profile"
            className={styles['menu-link']}
            activeClassName={styles['active-link']}
            exact
        >
            Мой профиль
        </Link>
        <Link
            to="/home/settings/tarif"
            className={styles['menu-link']}
            activeClassName={styles['active-link']}
            exact
        >
            Тарифы
        </Link>
        <Link
            to="/home/settings/certificates"
            className={styles['menu-link']}
            activeClassName={styles['active-link']}
            exact
        >
            Сертификаты
        </Link>
    </div>
);

export default SettingsNav;
