import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import Day from './components/Day';

import styles from './organizer-diary.css';

const momentDate = date => moment(date, 'DD.MM.YYYY').format('DD.MM.YYYY');

const Diary = (props) => {
    const data = props.selectedDay === ''
        ? props.data
        : props.data.filter(day => momentDate(day.date) === momentDate(props.selectedDay));

    return (
        <div className={styles.content}>
            {data.length === 0 && props.selectedDay === ''
                ? (
                    <p className={styles.info}>Нет записей</p>
                )
                : (
                    <div className={styles.list}>
                        {data.length > 0
                            ? data.map(day => (
                                <Day
                                    key={day.date}
                                    entriesData={day.entriesData}
                                    date={day.date}
                                    selectedDay={props.selectedDay}
                                    handleBackwardBtnClick={props.handleBackwardBtnClick}
                                    handleEntryClick={props.handleEntryClick}
                                />
                            ))
                            : <Day
                                key={props.selectedDay}
                                date={props.selectedDay}
                                selectedDay={props.selectedDay}
                                handleBackwardBtnClick={props.handleBackwardBtnClick}
                                handleEntryClick={props.handleEntryClick}
                            />
                        }
                    </div>
                )
            }
        </div>
    );
};

Diary.propTypes = {
    data: PropTypes.array,
    selectedDay: PropTypes.string,
    handleBackwardBtnClick: PropTypes.func,
    handleEntryClick: PropTypes.func,
};

Diary.defaultProps = {
    data: [],
    selectedDay: '',
    handleBackwardBtnClick: undefined,
    handleEntryClick: undefined,
};

export default Diary;
