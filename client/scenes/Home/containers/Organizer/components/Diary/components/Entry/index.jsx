import React from 'react';
import PropTypes from 'prop-types';

import styles from '../../organizer-diary.css';

const Entry = (props) => {
    let typeTitle;

    switch (props.type) {
        case 'note':
            typeTitle = 'Заметка';
            break;
        case 'pay':
            typeTitle = 'Платеж';
            break;
        case 'invoice':
            typeTitle = 'Выставленный счет';
            break;
        default:
            typeTitle = '';
    }

    return (
        <div
            className={styles.entry}
            style={{ cursor: props.type === 'note' ? 'pointer' : null }}
            onClick={props.type === 'note' ? () => props.handleEntryClick(props.noteId) : undefined}
        >
            <div className={styles['entry-content']}>
                <div
                    className={styles['entry-type']}
                    data-type={props.type}
                    title={typeTitle}
                />
                <p className={styles['entry-title']}>
                    {props.title}
                </p>
            </div>
            {props.value && (
                <p className={styles['entry-value']}>
                    {`${props.value} ₽`}
                </p>
            )}
        </div>
    )
};

Entry.propTypes = {
    /** тип записи (pay, invoice, note) */
    type: PropTypes.string,
    /** заголовок */
    title: PropTypes.string,
    /** сумма  */
    value: PropTypes.string,
    noteId: PropTypes.string,
    handleEntryClick: PropTypes.func,
};

Entry.defaultProps = {
    type: '',
    title: '',
    value: '',
    noteId: '',
    handleEntryClick: undefined,
};

export default Entry;
