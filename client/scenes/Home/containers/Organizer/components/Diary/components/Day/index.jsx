import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import Entry from '../Entry';

import styles from '../../organizer-diary.css';

/**
 * Компонент дня
 * @param {array} entriesData - данные записей
 * @param {string} date - дата
 * @return {ReactElement}
 */
const Day = (props) => {
    const date = props.entriesData.length === 0 ? props.selectedDay : props.date;

    return (
        <div className={styles.day}>
            <div className={styles['day-title']}>
                <p className={styles['day-date']}>
                    {`
                        ${props.selectedDay !== '' ? 'События' : ''} 
                        ${moment(date, 'DD.MM.YYYY').format('DD MMMM YYYY')} г.
                    `}
                </p>
                {props.selectedDay !== '' && (
                    <div
                        className={styles['day-btn']}
                        onClick={props.handleBackwardBtnClick}
                    />
                )}
            </div>
            {props.entriesData.length === 0 && props.selectedDay !== ''
                ? (
                    <p className={styles.info}>Нет записей</p>
                )
                : props.entriesData.map((entry, i) => (
                    <Entry
                        key={i}
                        type={entry.type}
                        title={entry.title}
                        value={entry.value && entry.value}
                        noteId={entry.type === 'note' ? entry.id : ''}
                        handleEntryClick={props.handleEntryClick}
                    />
                ))
            }
        </div>
    );
};

Day.propTypes = {
    entriesData: PropTypes.array,
    date: PropTypes.string,
    selectedDay: PropTypes.string,
    handleBackwardBtnClick: PropTypes.func,
    handleEntryClick: PropTypes.func,
};

Day.defaultProps = {
    entriesData: [],
    date: '',
    selectedDay: '',
    handleBackwardBtnClick: undefined,
    handleEntryClick: undefined,
};

export default Day;
