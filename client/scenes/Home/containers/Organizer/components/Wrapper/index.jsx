import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Scrollbars } from 'react-custom-scrollbars';

import Head from './components/Head';
import Foot from './components/Foot';

import styles from './organizer.css';

export default class Wrapper extends Component {
    static propTypes = {
        activeTab: PropTypes.string,
        handleOptionChange: PropTypes.func,
        handleCloseBtnClick: PropTypes.func,
        handlePaymentClick: PropTypes.func,
        handleInvoiceClick: PropTypes.func,
        handleCreateNoteClick: PropTypes.func,
        children: PropTypes.node,
        isVisible: PropTypes.bool,
        index: PropTypes.number,
    };

    static defaultProps = {
        activeTab: 'diary',
        handleOptionChange: undefined,
        handleCloseBtnClick: undefined,
        handlePaymentClick: undefined,
        handleInvoiceClick: undefined,
        handleCreateNoteClick: undefined,
        children: null,
        isVisible: false,
        index: 7777,
    };

    render() {
        return (
            <div
                className={styles.container}
                data-visible={this.props.isVisible}
                style={{ zIndex: this.props.index }}
            >
                <Head
                    activeTab={this.props.activeTab}
                    handleOptionChange={this.props.handleOptionChange}
                    handleCloseBtnClick={this.props.handleCloseBtnClick}
                />
                <div className={styles.content}>
                    <Scrollbars
                        renderThumbVertical={thumbProps =>
                            (<div
                                {...thumbProps}
                                style={{
                                    position: 'relative',
                                    zIndex: 10,
                                    display: 'block',
                                    width: '100%',
                                    cursor: 'pointer',
                                    borderRadius: 'inherit',
                                    backgroundColor: 'rgba(0, 0, 0, 0.2)',
                                    height: '42px',
                                    transform: 'translateY(0px)',
                                }}
                            />)
                        }
                        ref={(el) => { this.scrollbars = el; }}
                    >
                        {this.props.children}
                    </Scrollbars>
                </div>
                <Foot
                    handleCreateNoteClick={this.props.handleCreateNoteClick}
                    handlePaymentClick={this.props.handlePaymentClick}
                    handleInvoiceClick={this.props.handleInvoiceClick}
                />
            </div>
        );
    }
}
