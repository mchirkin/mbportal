import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link, withRouter } from 'react-router-dom';

import styles from '../../organizer.css';

const initialState = {
    servicesIsActive: false,
};

@withRouter
class Foot extends Component {
    static propTypes = {
        handleCreateNoteClick: PropTypes.func,
        handlePaymentClick: PropTypes.func,
        handleInvoiceClick: PropTypes.func,
        history: PropTypes.object.isRequired,
    }

    static defaultProps = {
        handleCreateNoteClick: undefined,
        handlePaymentClick: undefined,
        handleInvoiceClick: undefined,
    }

    constructor(props) {
        super(props);

        this.state = initialState;
    }

    componentWillMount() {
        this.props.history.listen((location) => {
            if (!/calendar/.test(location.pathname)) {
                setTimeout(() => this.setState({ servicesIsActive: false }), 300);
            }
        });
    }

    handleAddButtonClick = () => {
        this.setState({
            servicesIsActive: !this.state.servicesIsActive,
        });
    }

    render() {
        return (
            <div
                className={styles.foot}
                data-active={this.state.servicesIsActive}
            >
                <div className={styles.services}>
                    <div
                        className={styles.type}
                        data-type="pay"
                        onClick={this.props.handlePaymentClick}
                    >
                        Заплатить
                    </div>
                    <div
                        className={styles.type}
                        data-type="invoice"
                        onClick={this.props.handleInvoiceClick}
                    >
                        Выставить счет
                    </div>
                    <div
                        className={styles.type}
                        data-type="note"
                        onClick={() => {
                            setTimeout(() => this.setState({ servicesIsActive: false }), 300);
                            this.props.handleCreateNoteClick();
                        }}
                    >
                        Создать заметку
                    </div>
                </div>
                <button
                    className={styles['add-button']}
                    onClick={this.handleAddButtonClick}
                />
            </div>
        );
    }
}

export default Foot;
