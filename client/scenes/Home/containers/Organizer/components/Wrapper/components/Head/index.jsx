import React from 'react';
import PropTypes from 'prop-types';

import styles from '../../organizer.css';

const Head = (props) => {
    return (
        <div className={styles.head}>
            <div className={styles['radio-buttons']}>
                <div className={styles.radio}>
                    <input
                        className={styles['radio-input']}
                        type="radio"
                        value="diary"
                        id="diaryOption"
                        checked={props.activeTab === 'diary'}
                        onChange={props.handleOptionChange}
                    />
                    <label
                        className={styles['radio-label']}
                        htmlFor="diaryOption"
                    >
                        Ежедневник
                    </label>
                </div>
                <div className={styles.radio}>
                    <input
                        className={styles['radio-input']}
                        type="radio"
                        value="calendar"
                        id="calendarOption"
                        checked={props.activeTab === 'calendar'}
                        onChange={props.handleOptionChange}
                    />
                    <label
                        className={styles['radio-label']}
                        htmlFor="calendarOption"
                    >
                        Календарь
                    </label>
                </div>
            </div>
            <div className={styles['close-btn']} onClick={props.handleCloseBtnClick} />
        </div>
    );
};

Head.propTypes = {
    activeTab: PropTypes.string,
    handleOptionChange: PropTypes.func,
    handleCloseBtnClick: PropTypes.func,
};

Head.defaultProps = {
    activeTab: 'diary',
    handleOptionChange: undefined,
    handleCloseBtnClick: undefined,
};

export default Head;
