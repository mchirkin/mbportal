import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import styles from '../../organizer-calendar.css';

const Month = ({ date, type, eventIcons, handleMonthClick }) => (
    <div
        className={styles.month}
        data-type={type !== '' ? type : null}
        onClick={() => handleMonthClick(date)}
    >
        <p className={styles['month-name']}>
            {moment(date, 'MM.YYYY').format('MMMM')}
        </p>
        {eventIcons.length > 0 && (
            <div className={styles['month-events']}>
                {eventIcons.map((iconType) => {
                    let iconTitle;

                    switch (iconType) {
                        case 'note':
                            iconTitle = 'Заметка';
                            break;
                        case 'pay':
                            iconTitle = 'Платеж';
                            break;
                        case 'invoice':
                            iconTitle = 'Выставленный счет';
                            break;
                        default:
                            iconTitle = '';
                    }

                    return (
                        <div
                            className={styles['event-type']}
                            data-type={iconType}
                            key={iconType}
                            title={iconTitle}
                        />
                    );
                })}
            </div>
        )}
    </div>
);

Month.propTypes = {
    date: PropTypes.string,
    type: PropTypes.string,
    eventIcons: PropTypes.array,
    handleMonthClick: PropTypes.func,
};

Month.defaultProps = {
    date: '',
    type: '',
    eventIcons: [],
    handleMonthClick: undefined,
};

export default Month;
