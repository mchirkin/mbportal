import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import styles from '../../organizer-calendar.css';

const Day = ({ date, type, eventIcons, handleDayClick }) => (
    <div
        className={styles.day}
        data-type={type !== '' ? type : null}
        onClick={() => handleDayClick(date)}
    >
        <p className={styles['day-number']}>
            {moment(date, 'DD.MM.YYYY').format('D')}
        </p>
        {eventIcons.length > 0 && (
            <div className={styles['day-events']}>
                {eventIcons.map((iconType) => {
                    let iconTitle;

                    switch (iconType) {
                        case 'note':
                            iconTitle = 'Заметка';
                            break;
                        case 'pay':
                            iconTitle = 'Платеж';
                            break;
                        case 'invoice':
                            iconTitle = 'Выставленный счет';
                            break;
                        default:
                            iconTitle = '';
                    }

                    return (
                        <div
                            className={styles['event-type']}
                            data-type={iconType}
                            key={iconType}
                            title={iconTitle}
                        />
                    );
                })}
            </div>
        )}
    </div>
);

Day.propTypes = {
    date: PropTypes.string,
    type: PropTypes.string,
    eventIcons: PropTypes.array,
    handleDayClick: PropTypes.func,
};

Day.defaultProps = {
    date: '',
    type: '',
    eventIcons: [],
    handleDayClick: undefined,
};

export default Day;
