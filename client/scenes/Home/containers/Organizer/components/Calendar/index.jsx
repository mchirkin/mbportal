import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import Granim from 'granim';

import { debounce } from 'utils';

import Day from './components/Day';
import Month from './components/Month';

import styles from './organizer-calendar.css';

const format = 'DD.MM.YYYY';

const initialState = {
    activeTab: 'month',
    rowPosition: -1,
    rowAnimtaionDuration: 0,
};

export default class Calendar extends Component {
    constructor(props) {
        super(props);

        this.state = initialState;
    }

    static propTypes = {
        data: PropTypes.array,
        handleDayClick: PropTypes.func,
        handleMonthClick: PropTypes.func,
        changeSelectedMonth: PropTypes.func,
        selectedDay: PropTypes.string,
        selectedMonth: PropTypes.string,
    };

    static defaultProps = {
        data: [],
        handleDayClick: undefined,
        handleMonthClick: undefined,
        changeSelectedMonth: undefined,
        selectedDay: '',
        selectedMonth: '',
    };

    componentDidMount = () => {
        const month = Number(moment(this.props.selectedMonth, format).format('M'));

        const colors = [
            ['#275fab', '#0089cc'],
            ['#0089cc', '#7facfc'],
            ['#7facfc', '#3ec795'],
            ['#3ec795', '#e9e60c'],
            ['#e9e60c', '#ffdb00'],
            ['#ffdb00', '#fbc330'],
            ['#fbc330', '#f18231'],
            ['#f18231', '#ea5330'],
            ['#ea5330', '#ed3b63'],
            ['#ed3b63', '#c0478e'],
            ['#c0478e', '#754c97'],
            ['#754c97', '#275fab'],
        ];

        this.granimInstance = new Granim({
            element: `#${this.granim.id}`,
            name: 'granim',
            opacity: [1, 1],
            stateTransitionSpeed: 200,
            states: {
                'default-state': { gradients: [colors[month - 1]] },
                1: { gradients: [colors[0]] },
                2: { gradients: [colors[1]] },
                3: { gradients: [colors[2]] },
                4: { gradients: [colors[3]] },
                5: { gradients: [colors[4]] },
                6: { gradients: [colors[5]] },
                7: { gradients: [colors[6]] },
                8: { gradients: [colors[7]] },
                9: { gradients: [colors[8]] },
                10: { gradients: [colors[9]] },
                11: { gradients: [colors[10]] },
                12: { gradients: [colors[11]] },
            },
        });

        this.granimInstance.changeState(month);
    }

    handleOptionChange = (e) => {
        const value = e.target.value;

        this.setState({
            activeTab: value,
        });
    }

    handleNavButtonClick = (direction) => {
        let selectedMonth;
        let rowPosition;
        const rowAnimtaionDuration = 200;

        if (direction === 'forward') {
            selectedMonth = moment(this.props.selectedMonth, format).add(1, `${this.state.activeTab}s`).format(format);
            rowPosition = this.state.rowPosition - 1;
        } else {
            selectedMonth = moment(this.props.selectedMonth, format).subtract(1, `${this.state.activeTab}s`).format(format);
            rowPosition = this.state.rowPosition + 1;
        }

        this.granimInstance.changeState(Number(moment(selectedMonth, format).format('M')));

        this.setState({
            rowAnimtaionDuration,
            rowPosition,
        });

        setTimeout(() => {
            this.setState({
                rowAnimtaionDuration: initialState.rowAnimtaionDuration,
                rowPosition: direction === 'forward' ? this.state.rowPosition + 1 : this.state.rowPosition - 1,
            });

            this.props.handleMonthClick(selectedMonth);
        }, rowAnimtaionDuration + 100); // закладываю 100мс на всякий случай
    }

    debouncedNavBtn = (direction) => {
        if (!this[direction]) {
            this[direction] = debounce(() => this.handleNavButtonClick(direction), 200);
        }

        return this[direction];
    }

    getListOfDaysInMonth = (month, year) => {
        const array = [];

        const start = moment(`${month}.${year}`, 'MM.YYYY');

        const end = moment(start).add(1, 'month');

        while (start.isBefore(end)) {
            array.push(start.format(format));
            start.add(1, 'day');
        }

        return array;
    }

    getEventIcons = (date, type) => {
        const eventArray = new Set();

        if (type === 'day') {
            this.props.data.forEach((eventDay) => {
                if (moment(eventDay.date, format).format(format) === date) {
                    eventDay.entriesData.forEach((event) => {
                        eventArray.add(event.type);
                    });
                }
            });
        }

        if (type === 'month') {
            const month = moment(date, 'MM.YYYY').format('MM');
            const year = moment(date, 'MM.YYYY').format('YYYY');

            this.getListOfDaysInMonth(month, year).forEach((day) => { // дни месяца
                this.props.data.forEach((eventDay) => { // дни, где есть события
                    if (moment(eventDay.date, format).format(format) === day) {
                        eventDay.entriesData.forEach((event) => { // события
                            eventArray.add(event.type);
                        });
                    }
                });
            });
        }

        return [...eventArray];
    };

    createMonth = (date) => {
        const dateNodes = [];

        const numberOfDays = 7;
        const numberOfWeeks = 6;

        const daysInThisMonth = moment(date, format).daysInMonth();
        const daysInPrevMonth = moment(date, format).subtract(1, 'months').daysInMonth();

        const prevMonthDaysCount = moment(date, format).startOf('month').subtract(1, 'days').day();
        const nextMonthDaysCount = (numberOfDays * numberOfWeeks) - daysInThisMonth - prevMonthDaysCount;

        for (let dayCounter = 0; dayCounter < prevMonthDaysCount; dayCounter += 1) {
            const day = (daysInPrevMonth - prevMonthDaysCount) + dayCounter + 1;

            const monthDay = moment(date, format).subtract(1, 'months').set('date', day).format(format);

            dateNodes.push(
                <Day
                    date={monthDay}
                    key={`disabled ${monthDay}`}
                    type={'disabled'}
                    eventIcons={this.getEventIcons(monthDay, 'day')}
                />
            );
        }

        for (let dayCounter = 1; dayCounter <= daysInThisMonth; dayCounter += 1) {
            const day = dayCounter;
            const monthDay = moment(date, format).set('date', day).format(format);

            let type;

            switch (monthDay.toString()) {
                case this.props.selectedDay:
                    type = 'active';
                    break;
                case moment().format(format).toString():
                    type = 'current';
                    break;
                default:
                    type = '';
            }

            dateNodes.push(
                <Day
                    date={monthDay}
                    key={monthDay}
                    type={type}
                    eventIcons={this.getEventIcons(monthDay, 'day')}
                    handleDayClick={() => this.props.handleDayClick(monthDay)}
                />
            );
        }

        for (let dayCounter = 0; dayCounter < nextMonthDaysCount; dayCounter += 1) {
            const day = dayCounter + 1;
            const monthDay = moment(date, format).add(1, 'months').set('date', day).format(format);

            dateNodes.push(
                <Day
                    date={monthDay}
                    key={`disabled ${monthDay}`}
                    type={'disabled'}
                    eventIcons={this.getEventIcons(monthDay, 'day')}
                />
            );
        }

        return dateNodes;
    }

    createYear = (date) => {
        const monthNodes = [];

        const year = moment(date, format).format('YYYY');

        for (let i = 0; i < 12; i += 1) {
            const momentMonth = moment(`${i + 1}.${year}`, 'MM.YYYY').format('MM.YYYY');

            const type = momentMonth.toString() === moment().format('MM.YYYY').toString() ? 'current' : '';

            monthNodes.push(
                <Month
                    date={momentMonth}
                    key={momentMonth}
                    type={type}
                    eventIcons={this.getEventIcons(momentMonth, 'month')}
                    handleMonthClick={() => {
                        this.setState({
                            activeTab: 'month',
                        });

                        this.granimInstance.changeState(Number(moment(`01.${momentMonth}`, format).format('M')));

                        this.props.handleMonthClick(moment(`01.${momentMonth}`, format).format(format));
                    }}
                />
            );
        }

        return monthNodes;
    }

    render() {
        const selectedMonthFormatted = moment(this.props.selectedMonth, format);
        const prevMonth = moment(selectedMonthFormatted).subtract(1, 'months').format(format);
        const nextMonth = moment(selectedMonthFormatted).add(1, 'months').format(format);

        const prevYear = moment(selectedMonthFormatted).subtract(1, 'years').format(format);
        const nextYear = moment(selectedMonthFormatted).add(1, 'years').format(format);

        return (
            <div className={styles.container}>
                <div className={styles['radio-buttons']}>
                    <div className={styles.radio}>
                        <input
                            className={styles['radio-input']}
                            type="radio"
                            value="month"
                            id="monthOption"
                            checked={this.state.activeTab === 'month'}
                            onChange={this.handleOptionChange}
                        />
                        <label
                            className={styles['radio-label']}
                            htmlFor="monthOption"
                        >
                            Месяц
                        </label>
                    </div>
                    <div className={styles.radio}>
                        <input
                            className={styles['radio-input']}
                            type="radio"
                            value="year"
                            id="yearOption"
                            checked={this.state.activeTab === 'year'}
                            onChange={this.handleOptionChange}
                        />
                        <label
                            className={styles['radio-label']}
                            htmlFor="yearOption"
                        >
                            Год
                        </label>
                    </div>
                </div>
                <div
                    className={styles.navigation}
                    data-theme={this.state.activeTab}
                >
                    <canvas
                        style={{ display: this.state.activeTab === 'year' && 'none' }}
                        id="granim-bg"
                        className={styles['navigation-bg']}
                        ref={(bg) => { this.granim = bg; }}
                    />
                    <div className={styles['navigation-titles']}>
                        {this.state.activeTab === 'month' && (
                            <div
                                className={styles.row}
                                style={{
                                    transform: `translateX(${this.state.rowPosition}00%)`,
                                    transition: `transform ${this.state.rowAnimtaionDuration}ms ease-in-out`,
                                }}
                            >
                                <p className={styles['navigation-title']}>
                                    {`${moment(selectedMonthFormatted).subtract(1, 'months').format('MMMM YYYY')} г.`}
                                </p>
                                <p className={styles['navigation-title']}>
                                    {`${moment(selectedMonthFormatted).format('MMMM YYYY')} г.`}
                                </p>
                                <p className={styles['navigation-title']}>
                                    {`${moment(selectedMonthFormatted).add(1, 'months').format('MMMM YYYY')} г.`}
                                </p>
                            </div>
                        )}
                        {this.state.activeTab === 'year' && (
                            <div
                                className={styles.row}
                                style={{
                                    transform: `translateX(${this.state.rowPosition}00%)`,
                                    transition: `transform ${this.state.rowAnimtaionDuration}ms ease-in-out`,
                                }}
                            >
                                <p className={styles['navigation-title']}>
                                    {`${moment(selectedMonthFormatted).subtract(1, 'years').format('YYYY')} г.`}
                                </p>
                                <p className={styles['navigation-title']}>
                                    {`${moment(selectedMonthFormatted).format('YYYY')} г.`}
                                </p>
                                <p className={styles['navigation-title']}>
                                    {`${moment(selectedMonthFormatted).add(1, 'years').format('YYYY')} г.`}
                                </p>
                            </div>
                        )}
                    </div>
                    <div className={styles['navigation-buttons']}>
                        <div
                            className={styles['navigation-button']}
                            data-direction="backward"
                            onClick={this.debouncedNavBtn('backward')}
                        />
                        <div
                            className={styles['navigation-button']}
                            data-direction="forward"
                            onClick={this.debouncedNavBtn('forward')}
                        />
                    </div>
                </div>
                {this.state.activeTab === 'month' && (
                    <div className={styles.week}>
                        <p className={styles['week-item']}>Пн</p>
                        <p className={styles['week-item']}>Вт</p>
                        <p className={styles['week-item']}>Ср</p>
                        <p className={styles['week-item']}>Чт</p>
                        <p className={styles['week-item']}>Пт</p>
                        <p className={styles['week-item']}>Сб</p>
                        <p className={styles['week-item']}>Вс</p>
                    </div>
                )}
                <div className={styles.content}>
                    {this.state.activeTab === 'month' && (
                        <div
                            className={styles.row}
                            style={{
                                transform: `translateX(${this.state.rowPosition}00%)`,
                                transition: `transform ${this.state.rowAnimtaionDuration}ms ease-in-out`,
                            }}
                        >
                            <div className={styles['row-item']}>
                                {this.createMonth(prevMonth)}
                            </div>
                            <div className={styles['row-item']}>
                                {this.createMonth(this.props.selectedMonth)}
                            </div>
                            <div className={styles['row-item']}>
                                {this.createMonth(nextMonth)}
                            </div>
                        </div>
                    )}
                    {this.state.activeTab === 'year' && (
                        <div
                            className={styles.row}
                            style={{
                                transform: `translateX(${this.state.rowPosition}00%)`,
                                transition: `transform ${this.state.rowAnimtaionDuration}ms ease-in-out`,
                            }}
                        >
                            <div className={styles['row-item']}>
                                {this.createYear(prevYear)}
                            </div>
                            <div className={styles['row-item']}>
                                {this.createYear(this.props.selectedMonth)}
                            </div>
                            <div className={styles['row-item']}>
                                {this.createYear(nextYear)}
                            </div>
                        </div>
                    )}
                </div>
            </div>
        );
    }
}
