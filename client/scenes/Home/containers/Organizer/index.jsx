import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment';

import { scrollbarsAnimations, emitter } from 'utils';

import setBackUrl from 'modules/settings/actions/set_back_url';
import fetchSend from 'modules/payments/send_payments/actions/fetch';
import { fetchInvoice } from 'modules/invoice/invoice';
import { toggleCalendarWidget } from 'modules/calendar_widget/toggle_calendar_widget';
import { setUserNoteActiveId } from 'modules/user_note/active_id';
import { fetchNote } from 'modules/user_note/note';
import setPaymentModalData from 'modules/payments/payment_modal/actions/set_data';

import Wrapper from './components/Wrapper';
import Diary from './components/Diary';
import Calendar from './components/Calendar';

const format = 'DD.MM.YYYY';

const initialState = {
    activeTab: 'diary',
    selectedDay: '',
    selectedMonth: moment().format(format),
};

class Organizer extends Component {
    static propTypes = {
        fetchInvoice: PropTypes.func.isRequired,
        fetchSend: PropTypes.func.isRequired,
        fetchNote: PropTypes.func.isRequired,
        setBackUrl: PropTypes.func.isRequired,
        setUserNoteActiveId: PropTypes.func.isRequired,
        backUrl: PropTypes.string,
        invoiceList: PropTypes.object,
        orgInfo: PropTypes.object,
        sendPayments: PropTypes.array,
        isVisible: PropTypes.bool,
        toggleCalendarWidget: PropTypes.func,
        userNote: PropTypes.array,
        infoMessageIsVisible: PropTypes.bool,
    }

    static defaultProps = {
        invoiceList: {},
        orgInfo: {},
        sendPayments: [],
        isVisible: false,
        toggleCalendarWidget: undefined,
        backUrl: '',
        userNote: [],
        infoMessageIsVisible: false,
    }

    static contextTypes = {
        router: PropTypes.object.isRequired,
    }

    constructor(props) {
        super(props);

        this.state = initialState;
    }

    componentDidMount() {
        this.initialFetchInvoiceList();

        if (!this.props.sendPayments && !this.sendPaymentsLoaded) {
            this.sendPaymentsLoaded = true;
            this.props.fetchSend();
        }

        emitter.addEvent('organizerDateChange');
    }

    initialFetchNote = (extRef) => {
        if (!this.props.userNote && !this.userNoteLoaded) {
            this.userNoteLoaded = true;
            this.props.fetchNote({ extRef });
        }
    }

    initialFetchInvoiceList = () => {
        if (this.props.invoiceList && this.props.orgInfo && !this.invoiceListLoaded) {
            this.invoiceListLoaded = true;
            this.props.fetchInvoice({ extCorporateRef: this.props.orgInfo.extCorporateRef });
        }
    }

    navigateToHomePage = () => {
        if (this.props.backUrl !== '/home') {
            this.context.router.history.push(this.props.backUrl);
            this.props.setBackUrl('/home');
        } else {
            this.context.router.history.push('/home');
        }
    }

    handleOptionChange = (e) => {
        const value = e.target.value;

        this.setState({
            activeTab: value,
            selectedDay: '',
        });
    }

    handleCloseBtnClick = () => {
        this.props.toggleCalendarWidget(false);

        if (/calendar/.test(window.location.pathname)) {
            this.navigateToHomePage();
        }
    }

    handleDayClick = (date) => {
        this.setState({
            selectedDay: date,
        });

        const options = {
            direction: 'y',
            element: this.wrapper.scrollbars,
            to: this.wrapper.scrollbars.getScrollHeight(),
            duration: 700,
            context: this,
        };

        scrollbarsAnimations(options);

        emitter.dispatchEvent('organizerDateChange', date);
    }

    handleMonthClick = (date) => {
        this.setState({
            selectedMonth: moment(date, format).format(format),
        });
    }

    handleBackwardBtnClick = () => {
        this.setState({
            selectedDay: '',
        });
    }

    handleEntryClick = (id) => {
        this.props.setUserNoteActiveId(id);

        this.context.router.history.push('/home/calendar');
    }

    handleCreateNoteClick = () => {
        this.props.setUserNoteActiveId('');

        this.context.router.history.push({
            pathname: '/home/calendar',
            state: {
                date: this.state.selectedDay,
            },
        });
    }

    handlePaymentClick = () => {
        this.props.setPaymentModalData({
            id: null,
            docDate: this.state.selectedDay,
            status: 'new',
        });

        this.context.router.history.push('/home/payment');
    }

    handleInvoiceClick = () => {
        this.context.router.history.push({
            pathname: '/home/invoices',
            state: {
                date: this.state.selectedDay,
            },
        });
    }

    render() {
        const data = {};

        if (this.props.invoiceList.data !== null) {
            this.props.invoiceList.data.forEach((invoice) => {
                const date = moment(invoice.paymentDate).format(format);

                const template = {
                    type: 'invoice',
                    title: invoice.payerName,
                    value: invoice.sumTotal.toString(),
                };

                if (moment(date, format).diff(moment(), 'days') >= 0) {
                    if (!data[date]) {
                        data[date] = {};
                        data[date].entriesData = [template];
                    } else {
                        data[date].entriesData.push(template);
                    }
                }
            });
        }

        if (this.props.sendPayments !== null) {
            this.props.sendPayments.forEach((pay) => {
                const date = moment(pay.platporDocument.docDate, 'YYYY-MM-DD').format(format);

                const template = {
                    type: 'pay',
                    title: pay.platporDocument.corrFullname,
                    value: pay.platporDocument.amountAll.toString(),
                };

                if (moment(date, format).diff(moment(), 'days') >= 0) {
                    if (!data[date]) {
                        data[date] = {};
                        data[date].entriesData = [template];
                    } else {
                        data[date].entriesData.push(template);
                    }
                }
            });
        }

        if (this.props.userNote !== null) {
            this.props.userNote.forEach((note) => {
                const date = moment(note.date).format(format);

                const template = {
                    type: 'note',
                    title: note.name,
                    id: note._id,
                };

                if (!data[date]) {
                    data[date] = {};
                    data[date].entriesData = [template];
                } else {
                    data[date].entriesData.push(template);
                }
            });
        }

        const formattedData = Object.keys(data)
            .sort((a, b) => moment(a, 'DD.MM.YYYY') - moment(b, 'DD.MM.YYYY'))
            .map(key => ({ date: key, ...data[key] }));
            // .filter(item => moment(item.date, format).diff(moment(), 'days') >= 0);

        return (
            <Wrapper
                activeTab={this.state.activeTab}
                handleOptionChange={this.handleOptionChange}
                handleCloseBtnClick={this.handleCloseBtnClick}
                handlePaymentClick={this.handlePaymentClick}
                handleInvoiceClick={this.handleInvoiceClick}
                isVisible={this.props.isVisible}
                index={this.props.infoMessageIsVisible ? 0 : 7777}
                handleCreateNoteClick={this.handleCreateNoteClick}
                ref={(wrapper) => { this.wrapper = wrapper; }}
            >
                {this.state.activeTab === 'calendar' && (
                    <Calendar
                        data={formattedData}
                        handleDayClick={this.handleDayClick}
                        handleMonthClick={this.handleMonthClick}
                        selectedDay={this.state.selectedDay}
                        selectedMonth={this.state.selectedMonth}
                    />
                )}
                <Diary
                    data={formattedData}
                    selectedDay={this.state.selectedDay}
                    handleBackwardBtnClick={this.handleBackwardBtnClick}
                    handleEntryClick={this.handleEntryClick}
                />
            </Wrapper>
        );
    }
}

function mapStateToProps(state) {
    return {
        orgInfo: state.user.orgInfo.data,
        sendPayments: state.payments.sendPayments.data,
        invoiceList: state.invoice.invoice,
        backUrl: state.settings.backUrl,
        userNote: state.userNote.note.data,
        userInfo: state.user.userInfo.data,
        infoMessageIsVisible: state.infoMessage.data.isVisible,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        fetchSend: bindActionCreators(fetchSend, dispatch),
        fetchInvoice: bindActionCreators(fetchInvoice, dispatch),
        toggleCalendarWidget: bindActionCreators(toggleCalendarWidget, dispatch),
        setUserNoteActiveId: bindActionCreators(setUserNoteActiveId, dispatch),
        setBackUrl: bindActionCreators(setBackUrl, dispatch),
        fetchNote: bindActionCreators(fetchNote, dispatch),
        setPaymentModalData: bindActionCreators(setPaymentModalData, dispatch),
    };
}


export default connect(mapStateToProps, mapDispatchToProps)(Organizer);
