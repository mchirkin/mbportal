import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { fetch, downloadBlob } from 'utils';

import fetchIncome from 'modules/main_page/actions/ministatement/income/fetch';
import fetchEnd from 'modules/main_page/actions/ministatement/outcome/end/fetch';
import fetchSend from 'modules/payments/send_payments/actions/fetch';
import fetchError from 'modules/payments/error_payments/actions/fetch';
import fetchNewStatus from 'modules/payments/new_payments/actions/fetch';
import setPaymentModalData from 'modules/payments/payment_modal/actions/set_data';
import showNotification from 'modules/notification/actions/show_notification';
import closeNotification from 'modules/notification/actions/close_notification';
import setPopupNotificationData from 'modules/popup_notification/actions/set_data';

import Loader from 'components/Loader';

import Payments from '../../components/Payments';

/**
 * Контейнер для отображения списка платежей на главной странице
 */
class PaymentsContainer extends Component {
    static propTypes = {
        /** идет ли загрузка платежей в статусе "Исполнен" */
        isEndFetching: PropTypes.bool,
        /** идет ли загрузка платежей в статусе "Отказан" */
        isErrorFetching: PropTypes.bool,
        /** идет ли загрузка последних входящих платежей */
        isIncomeFetching: PropTypes.bool,
        /** идет ли загрузка платежей в статусе "В обработке" */
        isSendFetching: PropTypes.bool,
        /** идентификтор выбранного счета */
        selectedAccount: PropTypes.string,
        /** Загрузка входящих платежей */
        fetchIncome: PropTypes.func.isRequired,
        /** список входящих платежей */
        income: PropTypes.array,
        /** список счетов клиента */
        accounts: PropTypes.array,
        /** список карт клиента */
        cards: PropTypes.array,
        /** список платежей в статусе "В обработке" */
        send: PropTypes.array,
        /** загрузка списка платежей в статусе "В обработке" */
        fetchSend: PropTypes.func.isRequired,
        /** список платежей в статусе "Отказан" */
        error: PropTypes.array,
        /** загрузка списка платежей в статусе "Отказан" */
        fetchError: PropTypes.func.isRequired,
        /** Список платежей в статусе "Исполнен" */
        end: PropTypes.array,
        /** загрузка списка платежей в статусе "Исполнен" */
        fetchEnd: PropTypes.func.isRequired,
        /** Установить данные для подстановки в форму платежного поручения */
        setPaymentModalData: PropTypes.func.isRequired,
        /** Отображение уведомления на нижней панели  */
        showNotification: PropTypes.func.isRequired,
        /** Текущее уведомления на нижней панели */
        notification: PropTypes.bool,
        /** Скрыть уведомление на нижней панели */
        closeNotification: PropTypes.func.isRequired,
        /** Загрузка платежей в статусе "Новый" */
        fetchNewStatus: PropTypes.func.isRequired,
        /** Установка данных для отображения popup-уведомления */
        setPopupNotificationData: PropTypes.func.isRequired,
    }

    static defaultProps = {
        isEndFetching: false,
        isErrorFetching: false,
        isIncomeFetching: false,
        isSendFetching: false,
        selectedAccount: '',
        income: [],
        accounts: [],
        cards: [],
        send: [],
        error: [],
        notification: null,
        end: [],
    }

    static contextTypes = {
        router: PropTypes.object.isRequired,
    };

    constructor(props) {
        super(props);

        this.state = {
            /** Дата начала периода(выбор из календаря) */
            startDate: null,
            /** Дата окончания периода(выбор из календаря) */
            endDate: null,
            /**
             * Установленный фильтр по типу платежей
             * Возможные значения:
             * all - все платежи
             * income - нам платили(входящие платежи)
             * outcome - мы платили(исходящие платежи)
             */
            filterPaymentType: 'all',
            /**
             * Установленный фильтр по статусу платежей
             * Возможные значения:
             * all - все платежи
             * send - в обработке
             * end - исполненные
             * decline - отказ
             */
            filterPaymentStatus: 'all',
            /** отображать лоадер */
            isFetching: false,
            /** данные по выписке(отображаем выписку, если установлен период) */
            statement: null,
            /** значение строки поиска */
            search: '',
            /** сколько загружать последних входящих платежей и исполенных */
            count: 10,
        };
    }

    componentWillMount() {
        if (window.paymentsState) {
            this.stateWasRestored = true;
            this.setState(window.paymentsState);
        }
    }

    componentWillUnmount() {
        if (this.needCacheState) {
            window.paymentsState = this.state;
        } else {
            window.paymentsState = null;
        }
    }

    componentWillReceiveProps(nextProps) {
        if (
            !this.props.isIncomeFetching &&
            (this.props.selectedAccount !== nextProps.selectedAccount || !this.props.income)
        ) {
            this.props.fetchIncome([nextProps.selectedAccount], 10);
        }

        if (
            nextProps.selectedAccount &&
            nextProps.accounts &&
            this.props.selectedAccount !== nextProps.selectedAccount
        ) {
            const account = nextProps.accounts.find(acc => acc.id === nextProps.selectedAccount);
            if (account) {
                this.getInitialPayments(account.number, account.id);
            }
        }
        if (nextProps.selectedAccount && nextProps.cards && this.props.selectedAccount !== nextProps.selectedAccount) {
            const card = nextProps.cards.find(c => c.id === nextProps.selectedAccount);
            if (card) {
                const account = nextProps.accounts.find(acc => acc.number === card.number);
                const accountId = account ? account.id : null;
                this.getInitialPayments(card.number, accountId);
            }
        }
    }

    componentDidMount() {
        if (this.stateWasRestored) {
            return;
        }

        if (
            !this.props.isIncomeFetching &&
            this.props.selectedAccount
        ) {
            this.props.fetchIncome([this.props.selectedAccount], 10);
        }

        if (this.props.accounts && this.props.selectedAccount) {
            const account = this.props.accounts.find(acc => acc.id === this.props.selectedAccount);
            if (account) {
                this.getInitialPayments(account.number, account.id);
            }
        }

        if (this.props.cards && this.props.selectedAccount) {
            const card = this.props.cards.find(c => c.id === this.props.selectedAccount);
            if (card) {
                const account = this.props.accounts.find(acc => acc.number === card.number);
                const accountId = account ? account.id : null;
                this.getInitialPayments(card.number, accountId);
            }
        }
    }

    getAccountInfo = () => {
        if (this.props.accounts && this.props.selectedAccount) {
            const account = this.props.accounts.find(acc => acc.id === this.props.selectedAccount);
            if (account) {
                return account;
            }
        }

        if (this.props.cards && this.props.selectedAccount) {
            const card = this.props.cards.find(c => c.id === this.props.selectedAccount);
            if (card) {
                const account = this.props.accounts.find(acc => acc.number === card.number);
                return account;
            }
        }

        return null;
    }

    getInitialPayments = (accNumber, accountId) => {
        /*
        if (!this.props.send) {
            this.props.fetchSend();
        }
        if (!this.props.error) {
            this.props.fetchError();
        }
        */
        this.props.fetchSend();
        this.props.fetchError();
        this.props.fetchEnd(accNumber, 10);

        if (this.state.startDate || this.state.endDate) {
            this.fetchStatement({
                startDate: this.state.startDate.format('DD.MM.YYYY'),
                endDate: this.state.endDate.format('DD.MM.YYYY'),
                accountId,
            });
        }
    }

    changeFilterPaymentType = (type) => {
        this.setState({
            filterPaymentType: type,
            filterPaymentStatus: type === 'income' ? 'all' : this.state.filterPaymentStatus,
        });
    }

    changeStatus = (status) => {
        this.setState({
            filterPaymentStatus: status,
            filterPaymentType: status !== 'all' ? 'outcome' : this.state.filterPaymentType,
        });
        /*
        if (status === 'send') {
            this.props.fetchSend();
        }
        */
    }

    clearDateFilter = () => {
        this.setState({
            startDate: null,
            endDate: null,
            statement: null,
        }, () => {
            const account = this.getAccountInfo();
            if (account) {
                this.getInitialPayments(account.number, account.id);
            }
        });
    }

    acceptDateFilter = ({ startDate, endDate }) => {
        this.setState({
            startDate: startDate || endDate,
            endDate: endDate || startDate,
        });
        this.fetchStatement({
            startDate: startDate ? startDate.format('DD.MM.YYYY') : endDate.format('DD.MM.YYYY'),
            endDate: endDate ? endDate.format('DD.MM.YYYY') : startDate.format('DD.MM.YYYY'),
        });
    }

    viewPayment = (data) => {
        let dataForView = data;
        if (data.platporDocument) {
            dataForView = data.platporDocument;
        }

        this.props.setPaymentModalData(Object.assign(dataForView, {
            income: !data.platporDocument, // || check line_is_debet
        }));

        this.needCacheState = true;
        this.context.router.history.push('/home/payment');
    }

    /**
     * Копирование документа
     * @param {Object} doc - документ для копирования
     */
    copyDocument = (doc) => {
        // const toast = this.props.showToast(`Подождите, идет копирование документа №${doc.docNumber}...`);
        const notificationData = {
            text: 'Идет копирование документа...',
        };

        this.props.showNotification(notificationData);

        fetch('/api/v1/documents/copy', {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify({
                documentId: doc.id,
            }),
        }).then(response => response.json()).then((data) => {
            if (data.errorText) {
                this.props.setPopupNotificationData({
                    isVisible: true,
                    headerText: 'Ошибка при копировании документа!',
                    message: data.errorText,
                    showSubmit: false,
                    showClose: true,
                    handleClose: () => this.props.setPopupNotificationData({ isVisible: false }),
                });

                if (this.props.notification === notificationData) {
                    this.props.closeNotification();
                }

                return;
            }
            global.console.log(doc.id, 'document copied');
            global.console.log('new document id', data.id);
            const documentData = Object.assign(
                {},
                doc,
                {
                    id: data.id,
                    docDate: '',
                    docNumber: '',
                    status: 'new',
                    signStatus: '0',
                },
            );

            if (this.props.notification === notificationData) {
                this.props.closeNotification();
            }

            this.props.setPaymentModalData(documentData);
            this.context.router.history.push('/home/payment');
            this.props.fetchNewStatus();
        }).catch((err) => {
            global.console.log(err);
        });
    }

    /**
     * Обработчик нажатия на кнопку "Сохранить в PDF"
     */
    savePDF = (doc) => {
        const { id } = doc;

        const notificationData = {
            text: 'Идет формирование документа...',
        };

        this.props.showNotification(notificationData);

        fetch(`/api/v1/documents/get_pdf/${id}`, {
            credentials: 'include',
        }).then((response) => {
            if (response.ok) {
                return response.blob();
            }
        }).then((blob) => {
            if (this.props.notification === notificationData) {
                this.props.closeNotification();
            }
            downloadBlob(blob, `${id}.pdf`);
        }).catch((err) => {
            global.console.log(err);
        });
    }

    fetchStatement = async ({ startDate, endDate, accountId }) => {
        try {
            this.setState({
                isFetching: true,
            });

            const response = await fetch('/api/v1/statement/offline', {
                method: 'POST',
                credentials: 'include',
                body: JSON.stringify({
                    accountId: accountId || this.props.selectedAccount,
                    dateFrom: startDate,
                    dateTo: endDate,
                }),
            });

            const json = await response.json();

            if (json && json.statementAnswer) {
                this.setState({
                    statement: json.statementAnswer,
                    isFetching: false,
                });
            } else {
                this.setState({
                    statement: {},
                    isFetching: false,
                });
            }
        } catch (err) {
            global.console.log(err);
            this.setState({
                statement: {},
                isFetching: false,
            });
        }
    }

    handleSearchChange = (e) => {
        this.setState({
            search: e.target.value,
        });
    }

    /**
     * Отзыв платежа
     * @param {Object} doc данные документа для отзыва
     */
    cancelDocument = (doc) => {
        console.log('[CANCEL]', doc);
        this.props.setPaymentModalData(Object.assign(doc, {
            income: false,
            showCancelModal: true,
        }));

        this.needCacheState = true;
        this.context.router.history.push('/home/payment');
    }

    /**
     * Загрузка входящих и исполненных платежей
     * при скролле страницы
     */
    loadMore = () => {
        if (this.props.isEndFetching || this.props.isIncomeFetching) {
            return;
        }

        if (this.state.startDate && this.state.endDate) {
            return;
        }

        if (this.renderedPaymentsCount < this.state.count) {
            return;
        }

        const newCount = this.state.count + 10;
        this.setState({
            count: newCount,
        }, () => {
            this.cacheIncome = this.props.income;
            this.cacheEnd = this.props.end;

            this.props.fetchIncome([this.props.selectedAccount], this.state.count);

            if (this.props.accounts && this.props.selectedAccount) {
                const account = this.props.accounts.find(acc => acc.id === this.props.selectedAccount);
                if (account) {
                    this.props.fetchEnd(account.number, this.state.count);
                }
            }

            if (this.props.cards && this.props.selectedAccount) {
                const card = this.props.cards.find(c => c.id === this.props.selectedAccount);
                if (card) {
                    const account = this.props.accounts.find(acc => acc.number === card.number);
                    this.props.fetchEnd(account.number, this.state.count);
                }
            }
        });
    }

    handleLoadMoreBtnClick = () => {
        this.loadMore();
    }

    saveRenderedPaymentsCount = (count) => {
        this.renderedPaymentsCount = count;
    }

    render() {
        let list = this.props.income;
        const accountName = '';

        if (this.state.statement) {
            list = this.state.statement || [];
        }

        const isFetching = (
            this.state.isFetching ||
            this.props.isIncomeFetching ||
            this.props.isEndFetching ||
            this.props.isSendFetching ||
            this.props.isErrorFetching
        );

        if (isFetching && !this.state.statement && this.cacheIncome) {
            list = this.cacheIncome;
        }

        let accNumber;

        if (this.props.selectedAccount && this.props.accounts) {
            const account = this.props.accounts.find(acc => acc.id === this.props.selectedAccount);
            if (account) {
                accNumber = account.number;
            }
        }
        if (this.props.selectedAccount && this.props.cards) {
            const card = this.props.cards.find(c => c.id === this.props.selectedAccount);
            if (card) {
                accNumber = card.number;
            }
        }

        return (
            <div style={{ position: 'relative', marginTop: 14 }}>
                {isFetching && this.state.count === 10
                    ? (
                        <div style={{ width: '100%', height: 200 }}>
                            <Loader visible={isFetching} />
                        </div>
                    )
                    : (
                        <Payments
                            ref={(c) => { this.paymentsComponent = c; }}
                            accNumber={accNumber}
                            list={list}
                            endPayments={isFetching ? this.cacheEnd : this.props.end}
                            sendPayments={this.props.send}
                            errorPayments={this.props.error}
                            accountName={accountName}
                            viewPayment={this.viewPayment}
                            changeType={this.changeFilterPaymentType}
                            changeStatus={this.changeStatus}
                            acceptDateFilter={this.acceptDateFilter}
                            clearDateFilter={this.clearDateFilter}
                            copyDocument={this.copyDocument}
                            savePDF={this.savePDF}
                            cancelDocument={this.cancelDocument}
                            withPeriod={this.state.startDate || this.state.endDate}
                            handleSearchChange={this.handleSearchChange}
                            {...this.state}
                            isFetching={isFetching}
                            handleLoadMoreBtnClick={this.handleLoadMoreBtnClick}
                            saveRenderedPaymentsCount={this.saveRenderedPaymentsCount}
                        />
                    )}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        accounts: state.accounts.data,
        cards: state.cards.data,
        selectedAccount: state.accounts.selectedAccount,
        income: state.mainPage.ministatement.income.data,
        end: state.mainPage.ministatement.outcome.end.data,
        send: state.payments.sendPayments.data,
        error: state.payments.errorPayments.data,
        isIncomeFetching: state.mainPage.ministatement.income.isFetching,
        isEndFetching: state.mainPage.ministatement.outcome.end.isFetching,
        isSendFetching: state.payments.sendPayments.isFetching,
        isErrorFetching: state.payments.errorPayments.isFetching,
        notification: state.notification.data,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        fetchIncome: bindActionCreators(fetchIncome, dispatch),
        fetchEnd: bindActionCreators(fetchEnd, dispatch),
        setPaymentModalData: bindActionCreators(setPaymentModalData, dispatch),
        fetchSend: bindActionCreators(fetchSend, dispatch),
        fetchError: bindActionCreators(fetchError, dispatch),
        fetchNewStatus: bindActionCreators(fetchNewStatus, dispatch),
        showNotification: bindActionCreators(showNotification, dispatch),
        closeNotification: bindActionCreators(closeNotification, dispatch),
        setPopupNotificationData: bindActionCreators(setPopupNotificationData, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(PaymentsContainer);
