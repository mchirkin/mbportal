import React, { Component } from 'react';
import Footer from 'components/login/Footer';
import fetch from 'isomorphic-fetch';

import { downloadBlob } from 'utils';

import Loader from 'components/Loader';

import Wrapper from './components/Wrapper';
import Info from './components/Info';

import styles from 'css/fonts.css'; /* eslint no-unused-vars: "off" */

export default class Invoice extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isFetching: false,
        };
    }

    downloadDOCX = () => {
        this.setState({
            isFetching: true,
        });

        const id = document.location.pathname.split('/').pop();
        fetch('/invoice/docx', {
            method: 'POST',
            body: JSON.stringify({
                id,
            }),
        })
            .then(response => response.blob())
            .then((blob) => {
                this.setState({
                    isFetching: false,
                });

                downloadBlob(blob, `${id}.docx`);
            })
            .catch((err) => {
                console.log(err);
            });
    }

    render() {
        const invoiceDataInput = document.getElementById('invoice-data');
        const data = JSON.parse(invoiceDataInput.value);

        let companyData = {};
        if (data[0]) {
            companyData = {
                fullName: data[0].providerFullName,
                inn: data[0].providerInn,
            };
        }

        return (
            <Wrapper>
                <Loader visible={this.state.isFetching} />
                <Info
                    companyData={companyData}
                    noData={!data[0]}
                    downloadDOCX={this.downloadDOCX}
                />
                <Footer />
            </Wrapper>
        );
    }
}
