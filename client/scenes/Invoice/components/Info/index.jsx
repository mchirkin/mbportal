import React from 'react';
import PropTypes from 'prop-types';

import styles from './info.css';

const Info = ({ companyData, noData, downloadDOCX }) => (
    <div className={styles.wrapper}>
        <div className={styles.content}>
            <div className={styles.header}>
                <div className={styles['reb-logo']} />
                <div className={styles['product-name']}>
                    Интернет-банк для бизнеса
                </div>
            </div>
            {noData
                ? (
                    <div className={styles['company-info']}>
                        Выставленный счет не найден
                    </div>
                )
                : (
                    <div className={styles['info-wrapper']}>
                        <div className={styles['company-info']}>
                            <div>
                                Вам выстален счет на оплату от компании
                            </div>
                            <div>
                                <div>
                                    <strong>{companyData.fullName}</strong>
                                </div>
                            </div>
                            <div>
                                ИНН <strong>{companyData.inn}</strong>
                            </div>
                        </div>
                        <div className={styles['download-btn']} onClick={downloadDOCX}>
                            <span>
                                Скачать счет <span className={styles.bold}>.DOCX</span>
                            </span>
                        </div>
                    </div>
                )}
        </div>
    </div>
);

Info.propTypes = {
    companyData: PropTypes.object,
    noData: PropTypes.bool,
    downloadDOCX: PropTypes.func.isRequired,
};

Info.defaultProps = {
    companyData: {},
    noData: false,
};

export default Info;
