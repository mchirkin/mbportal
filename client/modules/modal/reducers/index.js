import { SHOW_MODAL } from '../constants';

const initialState = {
    isVisible: false,
    data: {},
    closeModalCallback: null,
};

function modal(state = initialState, action) {
    switch (action.type) {
        case SHOW_MODAL:
            return {
                isVisible: action.isVisible,
                data: action.data,
                closeModalCallback: action.closeModalCallback,
            };
        default:
            return state;
    }
}

export default modal;
