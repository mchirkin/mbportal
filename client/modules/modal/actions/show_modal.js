import { SHOW_MODAL } from '../constants';

export default function showModal(payload) {
    return {
        type: SHOW_MODAL,
        isVisible: payload.isVisible,
        data: payload.data,
        closeModalCallback: payload.closeModalCallback,
    };
}
