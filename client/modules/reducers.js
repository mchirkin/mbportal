import { combineReducers } from 'redux';
import user from './user/reducers';
import modal from './modal/reducers';
import accounts from './accounts/reducers';
import cards from './cards/reducers';
import mainPage from './main_page/reducers';
import templates from './templates/reducers';
import payments from './payments/reducers';
import loader from './loader/reducers';
import crypto from './crypto/reducers';
import settings from './settings/reducers';
import statement from './statement/reducers';
import mail from './mail/reducers';
import msg from './msg/reducers';
import cancelDocuments from './cancel_documents/reducers';
import toast from './toast/reducers';
import partners from './partners/reducers';
import infoMessage from './info_message/reducers';
import notification from './notification/reducers';
import accountsLimits from './accounts_limits/reducers';
import popupNotification from './popup_notification/reducers';
import importantNotifications from './important_notifications/reducers';
import invoice from './invoice/reducers';
import dictionary from './dictionary/reducers';
import calendarWidget from './calendar_widget/reducers';
import userNote from './user_note/reducers';
import tariffs from './tariffs/reducers';

const reducers = combineReducers({
    user,
    modal,
    accounts,
    cards,
    mainPage,
    templates,
    payments,
    loader,
    crypto,
    statement,
    settings,
    mail,
    msg,
    cancelDocuments,
    toast,
    partners,
    infoMessage,
    notification,
    accountsLimits,
    popupNotification,
    importantNotifications,
    invoice,
    dictionary,
    calendarWidget,
    userNote,
    tariffs,
});

const rootReducer = (state, action) => {
    if (action.type === 'USER_LOGOUT') {
        state = undefined;
    }

    return reducers(state, action);
};

export default rootReducer;
