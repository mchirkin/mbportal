import fetch from 'utils/fetch_wrapper';
import fetchActionCreator from 'utils/redux/fetch_action_creator';
import checkResponseAndGetJson from 'utils/check_response_and_get_json';
import {
    FETCH_PRODUCT_LIST_REQUEST,
    FETCH_PRODUCT_LIST_SUCCESS,
    FETCH_PRODUCT_LIST_FAILURE,
} from '../../constants';

const request = fetchActionCreator.request(FETCH_PRODUCT_LIST_REQUEST);
const receive = fetchActionCreator.receive(FETCH_PRODUCT_LIST_SUCCESS);
const reject = fetchActionCreator.reject(FETCH_PRODUCT_LIST_FAILURE);

export default function fetchTariffs() {
    return async (dispatch) => {
        dispatch(request(true));
        try {
            const response = await fetch('/api/v1/tariff/get_product_list', {
                credentials: 'include',
            });
            const json = await checkResponseAndGetJson(response);
            dispatch(request(false));
            if (json) {
                dispatch(receive(json));
                dispatch(reject(null));
            }
        } catch (err) {
            dispatch(request(false));
            dispatch(reject(err));
        }
    };
}
