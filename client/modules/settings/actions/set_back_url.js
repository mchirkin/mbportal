import { SET_BACK_URL } from '../constants';

export default function setBackUrl(url) {
    return {
        type: SET_BACK_URL,
        url,
    };
}
