import fetch from 'utils/fetch_wrapper';
import fetchActionCreator from 'utils/redux/fetch_action_creator';
import checkResponseAndGetJson from 'utils/check_response_and_get_json';
import {
    FETCH_PASSWORD_RESTRICTIONS_REQUEST,
    FETCH_PASSWORD_RESTRICTIONS_SUCCESS,
    FETCH_PASSWORD_RESTRICTIONS_FAILURE,
} from '../../constants';

const request = fetchActionCreator.request(FETCH_PASSWORD_RESTRICTIONS_REQUEST);
const receive = fetchActionCreator.receive(FETCH_PASSWORD_RESTRICTIONS_SUCCESS);
const reject = fetchActionCreator.reject(FETCH_PASSWORD_RESTRICTIONS_FAILURE);

export default function fetchRestrictions() {
    return async (dispatch) => {
        dispatch(request(true));
        try {
            const response = await fetch('/api/v1/change_password', {
                method: 'POST',
                credentials: 'include',
            });
            const json = await checkResponseAndGetJson(response);
            dispatch(request(false));
            dispatch(receive(json || {}));
            dispatch(reject(null));
        } catch (err) {
            dispatch(request(false));
            dispatch(reject(err));
        }
    };
}
