import { SET_CERTIFICATE_TOUR_VISIBILITY } from '../../constants';

export default function setVisibility(isVisible) {
    return {
        type: SET_CERTIFICATE_TOUR_VISIBILITY,
        isVisible,
    };
}
