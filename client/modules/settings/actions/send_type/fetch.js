import fetch from 'utils/fetch_wrapper';
import fetchActionCreator from 'utils/redux/fetch_action_creator';
import checkResponseAndGetJson from 'utils/check_response_and_get_json';
import {
    FETCH_SEND_TYPE_SUCCESS,
    FETCH_SEND_TYPE_REQUEST,
    FETCH_SEND_TYPE_FAILURE,
} from '../../constants';

const request = fetchActionCreator.request(FETCH_SEND_TYPE_REQUEST);
const receive = fetchActionCreator.receive(FETCH_SEND_TYPE_SUCCESS);
const reject = fetchActionCreator.reject(FETCH_SEND_TYPE_FAILURE);


export default function fetchAllowedCancelList() {
    return async (dispatch) => {
        dispatch(request(true));
        try {
            const response = await fetch('/api/v1/products/getsendtype', {
                credentials: 'include',
            });
            const json = await checkResponseAndGetJson(response);
            dispatch(request(false));
            if (json) {
                dispatch(receive(json));
            } else {
                dispatch(receive([]));
            }
            dispatch(reject(null));
        } catch (err) {
            dispatch(request(false));
            dispatch(reject(err));
            dispatch(receive([]));
        }
    };
}
