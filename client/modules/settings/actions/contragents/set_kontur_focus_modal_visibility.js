import { SET_KONTUR_FOCUS_MODAL_VISIBILITY } from '../../constants';

export default function setVisibility(isVisible) {
    return {
        type: SET_KONTUR_FOCUS_MODAL_VISIBILITY,
        isVisible,
    };
}
