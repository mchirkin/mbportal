import { SET_CONTRAGENT_MODAL_DATA } from '../../constants';

export default function setModalData(data) {
    return {
        type: SET_CONTRAGENT_MODAL_DATA,
        data,
    };
}
