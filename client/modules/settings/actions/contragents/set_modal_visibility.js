import { SET_CONTRAGENTS_MODAL_VISIBILITY } from '../../constants';

export default function setStatus(isVisible) {
    return {
        type: SET_CONTRAGENTS_MODAL_VISIBILITY,
        isVisible,
    };
}
