import { SET_KONTUR_FOCUS_MODAL_DATA } from '../../constants';

export default function setData(data) {
    return {
        type: SET_KONTUR_FOCUS_MODAL_DATA,
        data,
    };
}
