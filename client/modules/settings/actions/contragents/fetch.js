import fetch from 'utils/fetch_wrapper';
import fetchActionCreator from 'utils/redux/fetch_action_creator';
import checkResponseAndGetJson from 'utils/check_response_and_get_json';
import {
    FETCH_CONTRAGENTS_SUCCESS,
    FETCH_CONTRAGENTS_REQUEST,
    FETCH_CONTRAGENTS_FAILURE,
} from '../../constants';

const request = fetchActionCreator.request(FETCH_CONTRAGENTS_REQUEST);
const receive = fetchActionCreator.receive(FETCH_CONTRAGENTS_SUCCESS);
const reject = fetchActionCreator.reject(FETCH_CONTRAGENTS_FAILURE);

export default function fetchContragent() {
    return async (dispatch) => {
        dispatch(request(true));
        try {
            const response = await fetch('/api/v1/contragent/list', {
                credentials: 'include',
            });
            const json = await checkResponseAndGetJson(response, 'corrDicElement', true);
            dispatch(request(false));
            if (json) {
                dispatch(receive(json));
                dispatch(reject(null));
            } else {
                dispatch(receive([]));
            }
        } catch (err) {
            dispatch(request(false));
            dispatch(receive([]));
            dispatch(reject(err));
        }
    };
}
