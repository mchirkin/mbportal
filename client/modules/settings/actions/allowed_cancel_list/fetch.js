import fetch from 'utils/fetch_wrapper';
import fetchActionCreator from 'utils/redux/fetch_action_creator';
import checkResponseAndGetJson from 'utils/check_response_and_get_json';
import {
    FETCH_ALLOWED_CANCEL_LIST_SUCCESS,
    FETCH_ALLOWED_CANCEL_LIST_REQUEST,
    FETCH_ALLOWED_CANCEL_LIST_FAILURE,
} from '../../constants';

const request = fetchActionCreator.request(FETCH_ALLOWED_CANCEL_LIST_REQUEST);
const receive = fetchActionCreator.receive(FETCH_ALLOWED_CANCEL_LIST_SUCCESS);
const reject = fetchActionCreator.reject(FETCH_ALLOWED_CANCEL_LIST_FAILURE);


export default function fetchAllowedCancelList() {
    return async (dispatch) => {
        dispatch(request(true));
        try {
            const response = await fetch('/api/v1/documents/list_for_cancel', {
                credentials: 'include',
            });
            const json = await checkResponseAndGetJson(response);
            dispatch(request(false));
            if (json) {
                dispatch(receive(json));
            } else {
                dispatch(receive([]));
            }
            dispatch(reject(null));
        } catch (err) {
            dispatch(request(false));
            dispatch(reject(err));
            dispatch(receive([]));
        }
    };
}
