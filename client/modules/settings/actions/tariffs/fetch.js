import fetch from 'utils/fetch_wrapper';
import fetchActionCreator from 'utils/redux/fetch_action_creator';
import checkResponseAndGetJson from 'utils/check_response_and_get_json';
import {
    FETCH_TARIFFS_REQUEST,
    FETCH_TARIFFS_SUCCESS,
    FETCH_TARIFFS_FAILURE,
} from '../../constants';

const request = fetchActionCreator.request(FETCH_TARIFFS_REQUEST);
const receive = fetchActionCreator.receive(FETCH_TARIFFS_SUCCESS);
const reject = fetchActionCreator.reject(FETCH_TARIFFS_FAILURE);

export default function fetchTariffs() {
    return async (dispatch) => {
        dispatch(request(true));
        try {
            const response = await fetch('/api/v1/products/gettariffs', {
                credentials: 'include',
            });
            const json = await checkResponseAndGetJson(response);
            dispatch(request(false));
            if (json) {
                const result = json.map((tariff) => {
                    const data = {
                        ...tariff,
                    };

                    if (tariff.tarifPlanCode === 'TP_DEFAULT') {
                        data.caption = 'Стандартный';
                    }

                    return data;
                });
                dispatch(receive(result));
                dispatch(reject(null));
            }
        } catch (err) {
            dispatch(request(false));
            dispatch(reject(err));
        }
    };
}
