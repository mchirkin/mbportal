import { FETCH_PASSWORD_RESTRICTIONS_REQUEST } from '../../constants';

const initialState = false;

export default function isFetching(state = initialState, action) {
    switch (action.type) {
        case FETCH_PASSWORD_RESTRICTIONS_REQUEST:
            return action.isFetching;
        default:
            return state;
    }
}
