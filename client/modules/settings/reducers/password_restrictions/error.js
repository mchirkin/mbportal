import { FETCH_PASSWORD_RESTRICTIONS_FAILURE } from '../../constants';

const initialState = null;

export default function error(state = initialState, action) {
    switch (action.type) {
        case FETCH_PASSWORD_RESTRICTIONS_FAILURE:
            return action.error;
        default:
            return state;
    }
}
