import { FETCH_PASSWORD_RESTRICTIONS_SUCCESS } from '../../constants';

const initialState = null;

export default function data(state = initialState, action) {
    switch (action.type) {
        case FETCH_PASSWORD_RESTRICTIONS_SUCCESS:
            return action.data;
        default:
            return state;
    }
}
