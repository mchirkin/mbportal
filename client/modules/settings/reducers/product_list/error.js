import { FETCH_PRODUCT_LIST_FAILURE } from '../../constants';

const initialState = null;

export default function error(state = initialState, action) {
    switch (action.type) {
        case FETCH_PRODUCT_LIST_FAILURE:
            return action.error;
        default:
            return state;
    }
}
