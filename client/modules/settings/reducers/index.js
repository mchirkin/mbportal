import { combineReducers } from 'redux';
import contragents from './contragents';
import tariffs from './tariffs';
import allowedCancelList from './allowed_cancel_list';
import passwordRestrictions from './password_restrictions';
import sendType from './send_type';
import backUrl from './back_url';
import welcomeTour from './welcome_tour';
import productList from './product_list';

const settings = combineReducers({
    contragents,
    tariffs,
    allowedCancelList,
    passwordRestrictions,
    sendType,
    backUrl,
    welcomeTour,
    productList,
});

export default settings;
