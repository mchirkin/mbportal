import { SET_CONTRAGENTS_MODAL_VISIBILITY } from '../../constants';

const initialState = false;

export default function modalVisible(state = initialState, action) {
    switch (action.type) {
        case SET_CONTRAGENTS_MODAL_VISIBILITY:
            return action.isVisible;
        default:
            return state;
    }
}
