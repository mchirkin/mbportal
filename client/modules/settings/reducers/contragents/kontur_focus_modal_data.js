import { SET_KONTUR_FOCUS_MODAL_DATA } from '../../constants';

const initialState = null;

export default function konturFocusModalData(state = initialState, action) {
    switch (action.type) {
        case SET_KONTUR_FOCUS_MODAL_DATA:
            return action.data;
        default:
            return state;
    }
}
