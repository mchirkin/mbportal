import { combineReducers } from 'redux';
import isFetching from './is_fetching';
import data from './data';
import error from './error';
import modalVisible from './modal_visible';
import modalData from './modal_data';
import konturFocusModalVisible from './kontur_focus_modal_visible';
import konturFocusModalData from './kontur_focus_modal_data';

const reducers = combineReducers({
    isFetching,
    data,
    error,
    modalVisible,
    modalData,
    konturFocusModalVisible,
    konturFocusModalData,
});

export default reducers;
