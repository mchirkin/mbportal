import { SET_KONTUR_FOCUS_MODAL_VISIBILITY } from '../../constants';

const initialState = false;

export default function konturFocusModalVisible(state = initialState, action) {
    switch (action.type) {
        case SET_KONTUR_FOCUS_MODAL_VISIBILITY:
            return action.isVisible;
        default:
            return state;
    }
}
