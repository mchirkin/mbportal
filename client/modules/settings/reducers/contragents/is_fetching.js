import { FETCH_CONTRAGENTS_REQUEST } from '../../constants';

const initialState = false;

export default function isFetching(state = initialState, action) {
    switch (action.type) {
        case FETCH_CONTRAGENTS_REQUEST:
            return action.isFetching;
        default:
            return state;
    }
}
