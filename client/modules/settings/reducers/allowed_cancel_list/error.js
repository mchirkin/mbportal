import { FETCH_ALLOWED_CANCEL_LIST_FAILURE } from '../../constants';

const initialState = null;

export default function error(state = initialState, action) {
    switch (action.type) {
        case FETCH_ALLOWED_CANCEL_LIST_FAILURE:
            return action.error;
        default:
            return state;
    }
}
