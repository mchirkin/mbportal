import { FETCH_SEND_TYPE_SUCCESS } from '../../constants';

const initialState = null;

export default function data(state = initialState, action) {
    switch (action.type) {
        case FETCH_SEND_TYPE_SUCCESS:
            return action.data;
        default:
            return state;
    }
}
