import { FETCH_SEND_TYPE_REQUEST } from '../../constants';

const initialState = false;

export default function isFetching(state = initialState, action) {
    switch (action.type) {
        case FETCH_SEND_TYPE_REQUEST:
            return action.isFetching;
        default:
            return state;
    }
}
