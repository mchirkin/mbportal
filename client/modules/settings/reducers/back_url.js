import { SET_BACK_URL } from '../constants';

export default function backUrl(state = '/home', action) {
    switch (action.type) {
        case SET_BACK_URL:
            return action.url;
        default:
            return state;
    }
}
