import { FETCH_TARIFFS_SUCCESS } from '../../constants';

const initialState = null;

export default function data(state = initialState, action) {
    switch (action.type) {
        case FETCH_TARIFFS_SUCCESS:
            return action.data;
        default:
            return state;
    }
}
