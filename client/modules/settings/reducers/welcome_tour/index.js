import { combineReducers } from 'redux';
import isVisible from './is_visible';

const reducers = combineReducers({
    isVisible,
});

export default reducers;
