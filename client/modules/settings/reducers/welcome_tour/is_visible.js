import { SET_CERTIFICATE_TOUR_VISIBILITY } from '../../constants';

const initialState = false;

export default function certificateTourVisible(state = initialState, action) {
    switch (action.type) {
        case SET_CERTIFICATE_TOUR_VISIBILITY:
            return action.isVisible;
        default:
            return state;
    }
}
