import { SET_ACCOUNT_PAGE_CARDS_SORT } from '../constants';

const initialState = {
    number: {
        enable: true,
        order: 'desc',
    },
    holder: {
        enable: false,
        order: 'desc',
    },
    type: {
        enable: false,
        order: 'desc',
    },
    expire: {
        enable: false,
        order: 'desc',
    },
    balance: {
        enable: false,
        order: 'desc',
    },
    status: {
        enable: false,
        order: 'desc',
    },
};

export default function sort(state = initialState, action) {
    const newSort = Object.assign({}, state);
    switch (action.type) {
        case SET_ACCOUNT_PAGE_CARDS_SORT:
            Object.keys(newSort).forEach((prop) => {
                newSort[prop].enable = false;
                if (prop === action.sort.field) {
                    newSort[prop] = {
                        enable: action.sort.enable,
                        order: action.sort.order || 'desc',
                    };
                }
            });
            return newSort;
        default:
            return state;
    }
}
