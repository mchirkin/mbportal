import { combineReducers } from 'redux';
import isFetching from './is_fetching';
import data from './data';
import error from './error';
import accountPageCardFilter from './account_page_card_filter';
import accountPageSort from './account_page_sort';

const cards = combineReducers({
    isFetching,
    data,
    error,
    accountPageCardFilter,
    accountPageSort,
});

export default cards;
