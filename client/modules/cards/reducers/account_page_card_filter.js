import { SET_ACCOUNT_PAGE_CARD_FILTER } from '../constants';

const initialState = {
    name: '',
    number: '',
    statusList: [],
    typeList: [],
};

export default function accountPageCardFilter(state = initialState, action) {
    switch (action.type) {
        case SET_ACCOUNT_PAGE_CARD_FILTER:
            return action.filter;
        default:
            return state;
    }
}
