import { FETCH_CARDS_REQUEST } from '../constants';

export default function requestCards(isFetching) {
    return {
        type: FETCH_CARDS_REQUEST,
        isFetching,
    };
}
