import fetch from 'utils/fetch_wrapper';
import requestCards from './request';
import receiveCards from './receive';
import rejectCards from './reject';

export default function fetchCards() {
    return (dispatch) => {
        dispatch(requestCards(true));

        const url = '/api/v1/products/cards/offline';
        const options = {
            credentials: 'include',
        };

        return fetch(url, options)
            .then((response) => {
                dispatch(rejectCards(null));
                if (response.ok) {
                    return response.json();
                }
                return Promise.reject('Network error');
            })
            .then((json) => {
                dispatch(requestCards(false));
                if (json) {
                    if (json.errorText) {
                        return Promise.reject(json.errorText);
                    }
                    const cards = typeof json.card.length !== 'undefined' ? json.card : [json.card];
                    dispatch(receiveCards(cards));
                    dispatch(rejectCards(null));
                }
            })
            .catch((error) => {
                dispatch(requestCards(false));
                dispatch(rejectCards({
                    offline: true,
                    error,
                }));
            });
    };
}
