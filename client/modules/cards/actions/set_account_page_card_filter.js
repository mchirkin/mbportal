import { SET_ACCOUNT_PAGE_CARD_FILTER } from '../constants';

export default function setAccountPageCardFilter(filter) {
    return {
        type: SET_ACCOUNT_PAGE_CARD_FILTER,
        filter,
    };
}
