import { SET_ACCOUNT_PAGE_CARDS_SORT } from '../constants';

export default function setCardsSort(sort) {
    return {
        type: SET_ACCOUNT_PAGE_CARDS_SORT,
        sort,
    };
}
