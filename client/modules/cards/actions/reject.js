import { FETCH_CARDS_FAILURE } from '../constants';

export default function rejectCards(error) {
    return {
        type: FETCH_CARDS_FAILURE,
        error,
    };
}
