import { FETCH_CARDS_SUCCESS } from '../constants';

export default function receiveCards(data) {
    return {
        type: FETCH_CARDS_SUCCESS,
        data,
    };
}
