import { FETCH_CERTIFICATES_SUCCESS } from '../../constants';

const initialState = null;

export default function data(state = initialState, action) {
    switch (action.type) {
        case FETCH_CERTIFICATES_SUCCESS:
            return action.data;
        default:
            return state;
    }
}
