import { combineReducers } from 'redux';
import certificates from './certificates';
import signModalIsVisible from './sign_modal_isvisible';
import documentForSign from './document_for_sign';
import signModalCallback from './sign_modal_callback';
import signModalSuccessMessage from './sign_modal_success_message';
import regen from './regen';

const crypto = combineReducers({
    certificates,
    signModalIsVisible,
    documentForSign,
    signModalCallback,
    signModalSuccessMessage,
    regen,
});

export default crypto;
