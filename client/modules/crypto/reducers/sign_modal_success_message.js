import { SET_SIGN_MODAL_SUCCESS_MESSAGE } from '../constants';

export default function signModalSuccessMessage(state = '', action) {
    switch (action.type) {
        case SET_SIGN_MODAL_SUCCESS_MESSAGE:
            return action.msg;
        default:
            return state;
    }
}
