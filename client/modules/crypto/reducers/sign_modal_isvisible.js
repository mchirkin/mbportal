import { SET_SIGN_MODAL_VISIBILITY } from '../constants';

const initialState = false;

export default function signModalIsVisible(state = initialState, action) {
    switch (action.type) {
        case SET_SIGN_MODAL_VISIBILITY:
            return action.isVisible;
        default:
            return state;
    }
}
