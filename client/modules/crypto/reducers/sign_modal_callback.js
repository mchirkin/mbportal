import { SET_SIGN_MODAL_CALLBACK } from '../constants';

export default function signModalCallback(state = null, action) {
    switch (action.type) {
        case SET_SIGN_MODAL_CALLBACK:
            return action.callback;
        default:
            return state;
    }
}
