import { SET_DOCUMENT_FOR_SIGN } from '../constants';

const initialState = null;

export default function documentForSign(state = initialState, action) {
    switch (action.type) {
        case SET_DOCUMENT_FOR_SIGN:
            return action.document;
        default:
            return state;
    }
}
