import { SET_SIGN_MODAL_CALLBACK } from '../constants';

export default function setSignModalCallback(cb) {
    return {
        type: SET_SIGN_MODAL_CALLBACK,
        callback: cb,
    };
}
