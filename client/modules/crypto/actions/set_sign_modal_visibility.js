import { SET_SIGN_MODAL_VISIBILITY } from '../constants';

export default function setSignModalVisibility(isVisible) {
    return {
        type: SET_SIGN_MODAL_VISIBILITY,
        isVisible,
    };
}
