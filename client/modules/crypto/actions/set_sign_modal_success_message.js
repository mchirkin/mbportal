import { SET_SIGN_MODAL_SUCCESS_MESSAGE } from '../constants';

export default function setSignModalSuccessMessage(msg) {
    return {
        type: SET_SIGN_MODAL_SUCCESS_MESSAGE,
        msg,
    };
}
