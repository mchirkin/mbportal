import { FETCH_CERTIFICATES_REQUEST } from '../../constants';

export default function request(isFetching) {
    return {
        type: FETCH_CERTIFICATES_REQUEST,
        isFetching,
    };
}
