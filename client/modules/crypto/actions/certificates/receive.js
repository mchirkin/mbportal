import { FETCH_CERTIFICATES_SUCCESS } from '../../constants';

export default function receive(data) {
    return {
        type: FETCH_CERTIFICATES_SUCCESS,
        data,
    };
}
