import { FETCH_CERTIFICATES_FAILURE } from '../../constants';

export default function reject(error) {
    return {
        type: FETCH_CERTIFICATES_FAILURE,
        error,
    };
}
