import { SET_DOCUMENT_FOR_SIGN } from '../constants';

export default function setDocumentForSign(document) {
    return {
        type: SET_DOCUMENT_FOR_SIGN,
        document,
    };
}
