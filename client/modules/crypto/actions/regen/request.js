import { FETCH_REGEN_CERTIFICATES_REQUEST } from '../../constants';

export default function request(isFetching) {
    return {
        type: FETCH_REGEN_CERTIFICATES_REQUEST,
        isFetching,
    };
}
