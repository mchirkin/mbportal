import { FETCH_REGEN_CERTIFICATES_SUCCESS } from '../../constants';

export default function receive(data) {
    return {
        type: FETCH_REGEN_CERTIFICATES_SUCCESS,
        data,
    };
}
