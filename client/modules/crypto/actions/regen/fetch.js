import fetch from 'utils/fetch_wrapper';
import checkResponseAndGetJson from 'utils/check_response_and_get_json';
import request from './request';
import receive from './receive';
import reject from './reject';

export default function fetchAction() {
    return async (dispatch) => {
        dispatch(request(true));

        const url = '/api/v1/crypto/regen';
        const options = {
            credentials: 'include',
        };

        try {
            const response = await fetch(url, options);
            dispatch(request(false));
            const json = await checkResponseAndGetJson(response, 'certificateRegenRequest', true);
            if (json) {
                dispatch(receive(json));
                dispatch(reject(null));
            }
        } catch (err) {
            dispatch(request(false));
            dispatch(reject(err));
        }
    };
}
