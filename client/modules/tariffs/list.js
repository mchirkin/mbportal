import { fetch } from 'utils';

const FETCH_TARIFF_LIST_REQUEST = 'TARIFF/LIST_FETCH/REQUEST';
const FETCH_TARIFF_LIST_SUCCESS = 'TARIFF/LIST_FETCH/SUCCESS';
const FETCH_TARIFF_LIST_FAILURE = 'TARIFF/LIST_FETCH/FAILURE';

const initialState = {
    data: null,
    error: null,
    isFetching: false,
};

export default function reducer(state = initialState, action = {}) {
    switch (action.type) {
        case FETCH_TARIFF_LIST_SUCCESS:
            return {
                ...state,
                data: action.data,
            };
        case FETCH_TARIFF_LIST_FAILURE:
            return {
                ...state,
                error: action.error,
            };
        case FETCH_TARIFF_LIST_REQUEST:
            return {
                ...state,
                isFetching: action.isFetching,
            };
        default:
            return state;
    }
}

export function receive(data) {
    return {
        type: FETCH_TARIFF_LIST_SUCCESS,
        data,
    };
}

export function reject(error) {
    return {
        type: FETCH_TARIFF_LIST_FAILURE,
        error,
    };
}

export function request(isFetching) {
    return {
        type: FETCH_TARIFF_LIST_REQUEST,
        isFetching,
    };
}

export function fetchTariffList() {
    return dispatch => {
        dispatch(request(true));

        const url = '/api/v1/tariff/list_cms';
        const options = {
            credentials: 'include',
        };

        return fetch(url, options)
            .then(response => {
                if (response.ok) {
                    return response.json();
                }
                return Promise.reject('Network error');
            })
            .then(json => {
                dispatch(request(false));
                if (json) {
                    if (json.errorText) {
                        return Promise.reject(json.errorText);
                    }
                    dispatch(receive(json));
                } else {
                    dispatch(receive([]));
                }
                dispatch(reject(null));
            })
            .catch(error => {
                dispatch(request(false));
                dispatch(receive([]));
                dispatch(reject(error));
            });
    };
}
