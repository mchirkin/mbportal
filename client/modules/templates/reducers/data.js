import { FETCH_TEMPLATES_LIST_SUCCESS } from '../constants';

const initialState = null;

export default function data(state = initialState, action) {
    switch (action.type) {
        case FETCH_TEMPLATES_LIST_SUCCESS:
            return action.data;
        default:
            return state;
    }
}
