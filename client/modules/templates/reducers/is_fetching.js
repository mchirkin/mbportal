import { FETCH_TEMPLATES_LIST_REQUEST } from '../constants';

const initialState = false;

export default function isFetching(state = initialState, action) {
    switch (action.type) {
        case FETCH_TEMPLATES_LIST_REQUEST:
            return action.isFetching;
        default:
            return state;
    }
}
