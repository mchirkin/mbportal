import fetch from 'utils/fetch_wrapper';
import request from './request';
import receive from './receive';
import reject from './reject';

export default function fetchTemplatesList() {
    return (dispatch) => {
        dispatch(request(true));

        const url = '/api/v1/templates/list/full';
        const options = {
            credentials: 'include',
        };

        return fetch(url, options)
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
                response.json().then(error => dispatch(reject(error)));
            })
            .then((json) => {
                dispatch(request(false));
                if (json) {
                // const templates = typeof json.template.length !== 'undefined' ? json.template : [json.template];
                // templateDocuments
                    const templates = typeof json.templateDocuments.length !== 'undefined'
                        ? json.templateDocuments
                        : [json.templateDocuments];
                    dispatch(receive(templates));
                    dispatch(reject(null));
                }
                if (json === null) {
                    dispatch(receive([]));
                    dispatch(reject(null));
                }
            })
            .catch((error) => {
                dispatch(request(false));
                dispatch(reject(error));
            });
    };
}
