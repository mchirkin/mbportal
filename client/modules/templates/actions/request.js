import { FETCH_TEMPLATES_LIST_REQUEST } from '../constants';

export default function request(isFetching) {
    return {
        type: FETCH_TEMPLATES_LIST_REQUEST,
        isFetching,
    };
}
