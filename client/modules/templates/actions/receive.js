import { FETCH_TEMPLATES_LIST_SUCCESS } from '../constants';

export default function receive(data) {
    return {
        type: FETCH_TEMPLATES_LIST_SUCCESS,
        data,
    };
}
