import { FETCH_TEMPLATES_LIST_FAILURE } from '../constants';

export default function reject(error) {
    return {
        type: FETCH_TEMPLATES_LIST_FAILURE,
        error,
    };
}
