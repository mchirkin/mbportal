import { SET_VISIBLE_CURRENCY } from '../../constants';

export default function setVisibleCurrency(payload) {
    return {
        type: SET_VISIBLE_CURRENCY,
        payload,
    };
}
