import { SET_NEW_TEMPLATE_MODAL_VISIBILITY } from '../constants';

export default function setNewTemplateModalVisibility(isVisible) {
    return {
        type: SET_NEW_TEMPLATE_MODAL_VISIBILITY,
        isVisible,
    };
}
