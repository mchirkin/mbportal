import { TOGGLE_SHOW_TEMPLATES_MODAL } from '../../constants';

export default function toggleTemplatesModal(show) {
    return {
        type: TOGGLE_SHOW_TEMPLATES_MODAL,
        show,
    };
}
