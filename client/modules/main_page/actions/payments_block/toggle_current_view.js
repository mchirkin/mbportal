import { SET_PAYMENT_BLOCK_CURRENT_VIEW } from '../../constants';

export default function toggleCurrentView() {
    return {
        type: SET_PAYMENT_BLOCK_CURRENT_VIEW,
    };
}
