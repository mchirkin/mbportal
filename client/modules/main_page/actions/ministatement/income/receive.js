import { FETCH_MINISTATEMENT_INCOME_SUCCESS } from '../../../constants';

export default function receive(data) {
    return {
        type: FETCH_MINISTATEMENT_INCOME_SUCCESS,
        data,
    };
}
