import { FETCH_MINISTATEMENT_INCOME_REQUEST } from '../../../constants';

export default function request(isFetching) {
    return {
        type: FETCH_MINISTATEMENT_INCOME_REQUEST,
        isFetching,
    };
}
