import fetch from 'utils/fetch_wrapper';
import checkResponseAndGetJson from 'utils/check_response_and_get_json';
import request from './request';
import receive from './receive';
import reject from './reject';

export default function fetchMinistatementIncome(accounts, count) {
    return async (dispatch) => {
        dispatch(request(true));
        try {
            const paymentsCount = count || 10;

            const response = await fetch(`/api/v1/ministatement/income/${paymentsCount}`, {
                method: 'POST',
                credentials: 'include',
                body: JSON.stringify({
                    accounts,
                }),
            });
            dispatch(request(false));
            const json = await checkResponseAndGetJson(response, 'operations', true);
            if (json) {
                dispatch(receive(json));
                dispatch(reject(null));
            } else {
                dispatch(receive([]));
                dispatch(reject(null));
            }
        } catch (err) {
            dispatch(request(false));
            dispatch(receive([]));
            dispatch(reject(err));
        }
    };
}
