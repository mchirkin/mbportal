import { FETCH_MINISTATEMENT_INCOME_FAILURE } from '../../../constants';

export default function reject(error) {
    return {
        type: FETCH_MINISTATEMENT_INCOME_FAILURE,
        error,
    };
}
