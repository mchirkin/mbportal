import { FETCH_MINISTATEMENT_OUTCOME_END_FAILURE } from '../../../../constants';

export default function reject(error) {
    return {
        type: FETCH_MINISTATEMENT_OUTCOME_END_FAILURE,
        error,
    };
}
