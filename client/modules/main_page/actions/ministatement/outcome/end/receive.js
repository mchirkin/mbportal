import { FETCH_MINISTATEMENT_OUTCOME_END_SUCCESS } from '../../../../constants';

export default function receiveMinistatementOutcomeEnd(data) {
    return {
        type: FETCH_MINISTATEMENT_OUTCOME_END_SUCCESS,
        data,
    };
}
