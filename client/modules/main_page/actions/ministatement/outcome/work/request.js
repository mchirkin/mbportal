import { FETCH_MINISTATEMENT_OUTCOME_WORK_REQUEST } from '../../../../constants';

export default function request(isFetching) {
    return {
        type: FETCH_MINISTATEMENT_OUTCOME_WORK_REQUEST,
        isFetching,
    };
}
