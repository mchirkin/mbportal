import { FETCH_MINISTATEMENT_OUTCOME_WORK_SUCCESS } from '../../../../constants';

export default function receiveMinistatementOutcomeEnd(data) {
    return {
        type: FETCH_MINISTATEMENT_OUTCOME_WORK_SUCCESS,
        data,
    };
}
