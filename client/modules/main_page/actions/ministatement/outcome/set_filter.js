import { SET_MINISTATEMENT_OUTCOME_FILTER } from '../../../constants';

export default function setFilter(filter) {
    return {
        type: SET_MINISTATEMENT_OUTCOME_FILTER,
        filter,
    };
}
