import { FETCH_MINISTATEMENT_OUTCOME_INPROGRESS_SUCCESS } from '../../../../constants';

export default function receive(data) {
    return {
        type: FETCH_MINISTATEMENT_OUTCOME_INPROGRESS_SUCCESS,
        data,
    };
}
