import { FETCH_MINISTATEMENT_OUTCOME_INPROGRESS_FAILURE } from '../../../../constants';

export default function reject(error) {
    return {
        type: FETCH_MINISTATEMENT_OUTCOME_INPROGRESS_FAILURE,
        error,
    };
}
