import { FETCH_MINISTATEMENT_OUTCOME_INPROGRESS_REQUEST } from '../../../../constants';

export default function request(isFetching) {
    return {
        type: FETCH_MINISTATEMENT_OUTCOME_INPROGRESS_REQUEST,
        isFetching,
    };
}
