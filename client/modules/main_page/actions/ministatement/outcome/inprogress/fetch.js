import fetch from 'utils/fetch_wrapper';
import checkResponseAndGetJson from 'utils/check_response_and_get_json';
import request from './request';
import receive from './receive';
import reject from './reject';

export default function fetchInProgress() {
    return async (dispatch) => {
        dispatch(request(true));
        try {
            const response = await fetch('/api/v1/ministatement/outcome/documents', {
                credentials: 'include',
                method: 'POST',
                body: JSON.stringify({
                    status: 'send',
                }),
            });
            dispatch(request(false));
            const json = await checkResponseAndGetJson(response, 'rosevroSimpleDocument', true);
            if (json) {
                dispatch(receive(json || []));
                dispatch(reject(null));
            } else {
                dispatch(receive([]));
            }
        } catch (err) {
            dispatch(request(false));
            dispatch(receive([]));
            dispatch(reject(err));
        }
    };
}
