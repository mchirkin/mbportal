import fetch from 'utils/fetch_wrapper';
import checkResponseAndGetJson from 'utils/check_response_and_get_json';
import request from './request';
import receive from './receive';
import reject from './reject';

export default function fetchMinistatementOutcomeError() {
    return async (dispatch) => {
        dispatch(request(true));
        try {
            let response = await fetch('/api/v1/ministatement/outcome/decline', {
                credentials: 'include',
            });
            let data = await checkResponseAndGetJson(response, 'rosevroLastDocument', true);

            if (!data) {
                dispatch(receive([]));
                dispatch(reject(null));
                return;
            }

            data = data.filter(el => el.viewed === 'false');
            response = await fetch('/api/v1/ministatement/outcome/getbyid', {
                credentials: 'include',
                method: 'POST',
                body: JSON.stringify(data),
            });

            const json = await checkResponseAndGetJson(response, 'rosevroSimpleDocument', true);
            dispatch(request(false));
            if (json) {
                dispatch(receive(json || []));
                dispatch(reject(null));
            } else {
                dispatch(receive([]));
            }
        } catch (err) {
            dispatch(request(false));
            dispatch(receive([]));
            dispatch(reject(err));
        }
    };
}
