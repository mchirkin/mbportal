import { FETCH_MINISTATEMENT_OUTCOME_ERROR_REQUEST } from '../../../../constants';

export default function request(isFetching) {
    return {
        type: FETCH_MINISTATEMENT_OUTCOME_ERROR_REQUEST,
        isFetching,
    };
}
