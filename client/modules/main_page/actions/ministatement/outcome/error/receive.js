import { FETCH_MINISTATEMENT_OUTCOME_ERROR_SUCCESS } from '../../../../constants';

export default function receiveMinistatementOutcomeError(data) {
    return {
        type: FETCH_MINISTATEMENT_OUTCOME_ERROR_SUCCESS,
        data,
    };
}
