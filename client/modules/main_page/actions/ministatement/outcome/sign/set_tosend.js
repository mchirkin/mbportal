import { FETCH_MINISTATEMENT_OUTCOME_TOSEND } from '../../../../constants';

export default function setToSend(data) {
    return {
        type: FETCH_MINISTATEMENT_OUTCOME_TOSEND,
        data,
    };
}
