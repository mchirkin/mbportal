import { FETCH_MINISTATEMENT_OUTCOME_SIGN_SUCCESS } from '../../../../constants';

export default function receiveMinistatementOutcomeSign(data) {
    return {
        type: FETCH_MINISTATEMENT_OUTCOME_SIGN_SUCCESS,
        data,
    };
}
