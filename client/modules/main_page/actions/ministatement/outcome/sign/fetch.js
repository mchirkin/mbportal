import fetch from 'utils/fetch_wrapper';
import checkResponseAndGetJson from 'utils/check_response_and_get_json';
import request from './request';
import receive from './receive';
import reject from './reject';

export default function fetchNew() {
    return async (dispatch) => {
        dispatch(request(true));
        try {
            const response = await fetch('/api/v1/ministatement/outcome/documents', {
                credentials: 'include',
                method: 'POST',
                body: JSON.stringify({
                    status: 'new;imported',
                }),
            });
            dispatch(request(false));
            const json = await checkResponseAndGetJson(response, 'rosevroSimpleDocument', true);
            if (json) {
                /*
                // фильтруем подписанные платежки
                const signedPayments = (json || []).filter(payment => payment.platporDocument.signStatus === '2');
                // фильтруем неподписанные документы
                const unsignedPayments = (json || []).filter(
                    payment => parseInt(payment.platporDocument.signStatus, 10) < 2
                );

                dispatch(setToSend(signedPayments || []));
                dispatch(receive(unsignedPayments || []));
                */
                dispatch(receive(json || []));

                dispatch(reject(null));
            } else {
                dispatch(receive([]));
            }
        } catch (err) {
            dispatch(request(false));
            dispatch(receive([]));
            dispatch(reject(err));
        }
    };
}
