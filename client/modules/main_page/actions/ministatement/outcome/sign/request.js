import { FETCH_MINISTATEMENT_OUTCOME_SIGN_REQUEST } from '../../../../constants';

export default function request(isFetching) {
    return {
        type: FETCH_MINISTATEMENT_OUTCOME_SIGN_REQUEST,
        isFetching,
    };
}
