import { MINISTATEMENT_SHOW_MORE } from '../../constants';

export default function showMoreAction(showMore) {
    return {
        type: MINISTATEMENT_SHOW_MORE,
        showMore,
    };
}
