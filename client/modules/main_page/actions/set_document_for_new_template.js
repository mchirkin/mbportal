import { SET_DOCUMENT_FOR_NEW_TEMPLATE } from '../constants';

export default function setDocumentForNewTemplate(data) {
    return {
        type: SET_DOCUMENT_FOR_NEW_TEMPLATE,
        docData: data,
    };
}
