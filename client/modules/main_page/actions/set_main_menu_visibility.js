import { SET_MAIN_MENU_VISIBILITY } from '../constants';

export default function setMainMenuVisibility(isVisible) {
    return {
        type: SET_MAIN_MENU_VISIBILITY,
        isVisible,
    };
}
