import { SET_NEW_TEMPLATE_MODAL_VISIBILITY } from '../constants';

const initialState = false;

export default function newTemplateModalVisibility(state = initialState, action) {
    switch (action.type) {
        case SET_NEW_TEMPLATE_MODAL_VISIBILITY:
            return action.isVisible;
        default:
            return state;
    }
}
