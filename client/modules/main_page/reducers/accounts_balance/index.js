import { combineReducers } from 'redux';
import visibleCurrencies from './visible_currencies';

const accountsBalance = combineReducers({
    visibleCurrencies,
});

export default accountsBalance;
