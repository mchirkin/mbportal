import { SET_VISIBLE_CURRENCY } from '../../constants';

const initialState = {
    RUB: true,
    USD: false,
    EUR: false,
};

export default function visibleCurrencies(state = initialState, action) {
    switch (action.type) {
        case SET_VISIBLE_CURRENCY:
            if (action.payload.currency.toUpperCase() in state) {
                const obj = {};
                obj[action.payload.currency.toUpperCase()] = action.payload.value;
                return Object.assign({}, state, obj);
            }
            return state;
        default:
            return state;
    }
}
