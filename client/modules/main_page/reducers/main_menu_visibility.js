import { SET_MAIN_MENU_VISIBILITY } from '../constants';

export default function mainMenuVisibility(state = false, action) {
    switch (action.type) {
        case SET_MAIN_MENU_VISIBILITY:
            return action.isVisible;
        default:
            return state;
    }
}
