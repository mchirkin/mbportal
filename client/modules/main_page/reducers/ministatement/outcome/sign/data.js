import { FETCH_MINISTATEMENT_OUTCOME_SIGN_SUCCESS } from '../../../../constants';

const initialState = null;

export default function data(state = initialState, action) {
    switch (action.type) {
        case FETCH_MINISTATEMENT_OUTCOME_SIGN_SUCCESS:
            return action.data;
        default:
            return state;
    }
}
