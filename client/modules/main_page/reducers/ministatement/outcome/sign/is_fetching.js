import { FETCH_MINISTATEMENT_OUTCOME_SIGN_REQUEST } from '../../../../constants';

const initialState = false;

export default function isFetching(state = initialState, action) {
    switch (action.type) {
        case FETCH_MINISTATEMENT_OUTCOME_SIGN_REQUEST:
            return action.isFetching;
        default:
            return state;
    }
}
