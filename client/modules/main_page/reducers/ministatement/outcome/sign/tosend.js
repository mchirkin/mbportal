import { FETCH_MINISTATEMENT_OUTCOME_TOSEND } from '../../../../constants';

const initialState = null;

export default function isFetching(state = initialState, action) {
    switch (action.type) {
        case FETCH_MINISTATEMENT_OUTCOME_TOSEND:
            return action.data;
        default:
            return state;
    }
}
