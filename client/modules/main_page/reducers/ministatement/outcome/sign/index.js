import { combineReducers } from 'redux';
import isFetching from './is_fetching';
import data from './data';
import error from './error';
import toSend from './tosend';

const reducers = combineReducers({
    isFetching,
    data,
    error,
    toSend,
});

export default reducers;
