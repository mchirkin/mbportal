import { FETCH_MINISTATEMENT_OUTCOME_SIGN_FAILURE } from '../../../../constants';

const initialState = null;

export default function error(state = initialState, action) {
    switch (action.type) {
        case FETCH_MINISTATEMENT_OUTCOME_SIGN_FAILURE:
            return action.error;
        default:
            return state;
    }
}
