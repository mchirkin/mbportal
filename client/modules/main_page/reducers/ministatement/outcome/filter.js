import { SET_MINISTATEMENT_OUTCOME_FILTER } from '../../../constants';

const initialState = 'decline';

export default function filter(state = initialState, action) {
    switch (action.type) {
        case SET_MINISTATEMENT_OUTCOME_FILTER:
            return action.filter;
        default:
            return state;
    }
}
