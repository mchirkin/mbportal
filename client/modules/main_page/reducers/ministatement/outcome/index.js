import { combineReducers } from 'redux';
import end from './end';
import decline from './error';
import sign from './sign';
import toSend from './sign/tosend';
import deffered from './work';
import inProgress from './inprogress';
import filter from './filter';

const reducers = combineReducers({
    end,
    decline,
    sign,
    toSend,
    deffered,
    inProgress,
    filter,
});

export default reducers;
