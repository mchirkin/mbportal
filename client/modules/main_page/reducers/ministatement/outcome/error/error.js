import { FETCH_MINISTATEMENT_OUTCOME_ERROR_FAILURE } from '../../../../constants';

const initialState = null;

export default function error(state = initialState, action) {
    switch (action.type) {
        case FETCH_MINISTATEMENT_OUTCOME_ERROR_FAILURE:
            return action.error;
        default:
            return state;
    }
}
