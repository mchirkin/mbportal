import { FETCH_MINISTATEMENT_OUTCOME_INPROGRESS_SUCCESS } from '../../../../constants';

const initialState = null;

export default function data(state = initialState, action) {
    switch (action.type) {
        case FETCH_MINISTATEMENT_OUTCOME_INPROGRESS_SUCCESS:
            return action.data;
        default:
            return state;
    }
}
