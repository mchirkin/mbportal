import { MINISTATEMENT_SHOW_MORE } from '../../constants';

export default function showMore(state = false, action) {
    switch (action.type) {
        case MINISTATEMENT_SHOW_MORE:
            return action.showMore;
        default:
            return state;
    }
}
