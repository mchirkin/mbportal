import { combineReducers } from 'redux';
import outcome from './outcome';
import income from './income';
import showMore from './show_more';

const reducers = combineReducers({
    outcome,
    income,
    showMore,
});

export default reducers;
