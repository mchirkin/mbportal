import { TOGGLE_SHOW_TEMPLATES_MODAL } from '../../constants';

const initialState = false;

export default function toggleTemplatesModal(state = initialState, action) {
    switch (action.type) {
        case TOGGLE_SHOW_TEMPLATES_MODAL:
            return action.show;
        default:
            return state;
    }
}
