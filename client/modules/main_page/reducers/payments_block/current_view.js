import { SET_PAYMENT_BLOCK_CURRENT_VIEW } from '../../constants';

const initialState = 'templates';

export default function currentView(state = initialState, action) {
    switch (action.type) {
        case SET_PAYMENT_BLOCK_CURRENT_VIEW:
            return state === 'templates' ? 'autopayments' : 'templates';
        default:
            return state;
    }
}
