import { combineReducers } from 'redux';
import currentView from './current_view';
import showTemplatesModal from './show_templates_modal';

const paymentBlock = combineReducers({
    currentView,
    showTemplatesModal,
});

export default paymentBlock;
