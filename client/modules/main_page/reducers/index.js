import { combineReducers } from 'redux';
import paymentsBlock from './payments_block';
import accountsBalance from './accounts_balance';
import ministatement from './ministatement';
import newTemplateModalVisibility from './new_template_modal_visibility';
import documentForNewTemplate from './document_for_new_template';
import mainMenuVisibility from './main_menu_visibility';

const mainPage = combineReducers({
    paymentsBlock,
    accountsBalance,
    ministatement,
    newTemplateModalVisibility,
    documentForNewTemplate,
    mainMenuVisibility,
});

export default mainPage;
