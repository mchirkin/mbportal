import { SET_DOCUMENT_FOR_NEW_TEMPLATE } from '../constants';

const initialState = null;

export default function documentForNewTemplate(state = initialState, action) {
    switch (action.type) {
        case SET_DOCUMENT_FOR_NEW_TEMPLATE:
            return action.docData;
        default:
            return state;
    }
}
