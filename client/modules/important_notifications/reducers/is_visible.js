import { SET_MODAL_VISIBILITY } from '../constants';

export default function isVisible(state = false, action) {
    switch (action.type) {
        case SET_MODAL_VISIBILITY:
            return action.isVisible;
        default:
            return state;
    }
}
