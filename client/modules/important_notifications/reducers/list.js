import { ADD_IMPORTANT_NOTIFICATION, DELETE_IMPORTANT_NOTIFICATION } from '../constants';

export default function list(state = [], action) {
    switch (action.type) {
        case ADD_IMPORTANT_NOTIFICATION:
            return [...state, action.data];
        case DELETE_IMPORTANT_NOTIFICATION:
            return state.filter(notification => notification !== action.data);
        default:
            return state;
    }
}
