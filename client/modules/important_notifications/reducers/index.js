import { combineReducers } from 'redux';
import isVisible from './is_visible';
import list from './list';

const reducers = combineReducers({
    isVisible,
    list,
});

export default reducers;
