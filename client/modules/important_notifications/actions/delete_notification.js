import { DELETE_IMPORTANT_NOTIFICATION } from '../constants';

export default function deleteNotification(data) {
    return {
        type: DELETE_IMPORTANT_NOTIFICATION,
        data,
    };
}
