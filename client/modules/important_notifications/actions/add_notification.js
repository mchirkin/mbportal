import uuid from 'uuid/v4';

import { ADD_IMPORTANT_NOTIFICATION } from '../constants';

export default function addNotification(data) {
    return {
        type: ADD_IMPORTANT_NOTIFICATION,
        data: {
            id: uuid(),
            ...data,
        },
    };
}
