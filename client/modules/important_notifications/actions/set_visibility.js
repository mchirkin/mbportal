import { SET_MODAL_VISIBILITY } from '../constants';

export default function setVisibilty(isVisible) {
    return {
        type: SET_MODAL_VISIBILITY,
        isVisible,
    };
}
