import { FETCH_ERROR_PAYMENTS_REQUEST } from '../constants';

export default function request(isFetching) {
    return {
        type: FETCH_ERROR_PAYMENTS_REQUEST,
        isFetching,
    };
}
