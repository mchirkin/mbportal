import { FETCH_ERROR_PAYMENTS_SUCCESS } from '../constants';

export default function receive(data) {
    return {
        type: FETCH_ERROR_PAYMENTS_SUCCESS,
        data,
    };
}
