import { FETCH_ERROR_PAYMENTS_FAILURE } from '../constants';

export default function reject(error) {
    return {
        type: FETCH_ERROR_PAYMENTS_FAILURE,
        error,
    };
}
