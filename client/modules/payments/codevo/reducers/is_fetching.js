import { FETCH_PAYMENTSMODAL_DATA_CODEVO_REQUEST } from '../constants';

const initialState = false;

export default function isFetching(state = initialState, action) {
    switch (action.type) {
        case FETCH_PAYMENTSMODAL_DATA_CODEVO_REQUEST:
            return action.isFetching;
        default:
            return state;
    }
}
