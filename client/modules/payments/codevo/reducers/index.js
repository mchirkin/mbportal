import { combineReducers } from 'redux';
import isFetching from './is_fetching';
import data from './data';
import error from './error';

const codevo = combineReducers({
    isFetching,
    data,
    error,
});

export default codevo;
