import fetch from 'utils/fetch_wrapper';
import fetchActionCreator from 'utils/redux/fetch_action_creator';
import checkResponseAndGetJson from 'utils/check_response_and_get_json';
import {
    FETCH_PAYMENTSMODAL_DATA_CODEVO_REQUEST,
    FETCH_PAYMENTSMODAL_DATA_CODEVO_SUCCESS,
    FETCH_PAYMENTSMODAL_DATA_CODEVO_FAILURE,
} from '../constants';

const request = fetchActionCreator.request(FETCH_PAYMENTSMODAL_DATA_CODEVO_REQUEST);
const receive = fetchActionCreator.receive(FETCH_PAYMENTSMODAL_DATA_CODEVO_SUCCESS);
const reject = fetchActionCreator.reject(FETCH_PAYMENTSMODAL_DATA_CODEVO_FAILURE);

export default () => async (dispatch) => {
    dispatch(request(true));
    try {
        const response = await fetch('/api/v1/products/getcodevo', {
            method: 'GET',
            credentials: 'include',
        });
        const json = await checkResponseAndGetJson(response);
        dispatch(request(false));
        dispatch(receive(json || {}));
        dispatch(reject(null));
    } catch (err) {
        dispatch(request(false));
        dispatch(reject(err));
    }
};
