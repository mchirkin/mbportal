import { FETCH_SEND_PAYMENTS_SUCCESS } from '../constants';

export default function receive(data) {
    return {
        type: FETCH_SEND_PAYMENTS_SUCCESS,
        data,
    };
}
