import { FETCH_SEND_PAYMENTS_REQUEST } from '../constants';

export default function request(isFetching) {
    return {
        type: FETCH_SEND_PAYMENTS_REQUEST,
        isFetching,
    };
}
