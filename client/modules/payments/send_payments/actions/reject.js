import { FETCH_SEND_PAYMENTS_FAILURE } from '../constants';

export default function reject(error) {
    return {
        type: FETCH_SEND_PAYMENTS_FAILURE,
        error,
    };
}
