import fetch from 'utils/fetch_wrapper';
import request from './request';
import receive from './receive';
import reject from './reject';

export default function fetchSendPayments() {
    return (dispatch) => {
        dispatch(request(true));

        const url = '/api/v1/payments/send';
        const options = {
            credentials: 'include',
        };

        return fetch(url, options)
            .then(response => response.json())
            .then((json) => {
                dispatch(request(false));
                if (json) {
                    const list = typeof json.rosevroSimpleDocument.length !== 'undefined'
                        ? json.rosevroSimpleDocument
                        : [json.rosevroSimpleDocument];
                    dispatch(receive(list));
                } else {
                    dispatch(receive([]));
                }
                dispatch(reject(null));
            })
            .catch((error) => {
                dispatch(request(false));
                dispatch(reject(error));
            });
    };
}
