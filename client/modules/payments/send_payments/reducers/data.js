import { FETCH_SEND_PAYMENTS_SUCCESS } from '../constants';

const initialState = null;

export default function data(state = initialState, action) {
    switch (action.type) {
        case FETCH_SEND_PAYMENTS_SUCCESS:
            return action.data;
        default:
            return state;
    }
}
