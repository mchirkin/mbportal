import { FETCH_SEND_PAYMENTS_FAILURE } from '../constants';

const initialState = null;

export default function error(state = initialState, action) {
    switch (action.type) {
        case FETCH_SEND_PAYMENTS_FAILURE:
            return action.error;
        default:
            return state;
    }
}
