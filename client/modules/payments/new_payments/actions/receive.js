import { FETCH_NEW_PAYMENTS_SUCCESS } from '../constants';

export default function receive(data) {
    return {
        type: FETCH_NEW_PAYMENTS_SUCCESS,
        data,
    };
}
