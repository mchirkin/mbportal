import { FETCH_NEW_PAYMENTS_REQUEST } from '../constants';

export default function request(isFetching) {
    return {
        type: FETCH_NEW_PAYMENTS_REQUEST,
        isFetching,
    };
}
