import fetch from 'utils/fetch_wrapper';
import request from './request';
import receive from './receive';
import reject from './reject';
import receiveSigned from './receive_signed';

export default function fetchNewPayments() {
    return (dispatch) => {
        dispatch(request(true));

        const url = '/api/v1/payments/new';
        const options = {
            credentials: 'include',
        };

        return fetch(url, options)
            .then(response => response.json())
            .then((json) => {
                dispatch(request(false));
                if (json) {
                    const list = typeof json.rosevroSimpleDocument.length !== 'undefined'
                        ? json.rosevroSimpleDocument
                        : [json.rosevroSimpleDocument];

                    const unsignedList = list.filter(el => el.platporDocument.signStatus !== '2');
                    const signedList = list.filter(el => el.platporDocument.signStatus === '2');

                    dispatch(receive(unsignedList));
                    dispatch(receiveSigned(signedList));
                } else {
                    dispatch(receive([]));
                }
                dispatch(reject(null));
            })
            .catch((error) => {
                dispatch(request(false));
                dispatch(reject(error));
            });
    };
}
