import { RECEIVE_SIGNED_PAYMENTS } from '../constants';

export default function receiveSigned(data) {
    return {
        type: RECEIVE_SIGNED_PAYMENTS,
        data,
    };
}
