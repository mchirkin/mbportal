import { FETCH_NEW_PAYMENTS_FAILURE } from '../constants';

export default function reject(error) {
    return {
        type: FETCH_NEW_PAYMENTS_FAILURE,
        error,
    };
}
