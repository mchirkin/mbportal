import { combineReducers } from 'redux';
import isFetching from './is_fetching';
import data from './data';
import error from './error';
import signed from './signed';

const reducers = combineReducers({
    isFetching,
    data,
    error,
    signed,
});

export default reducers;
