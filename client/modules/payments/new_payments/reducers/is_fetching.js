import { FETCH_NEW_PAYMENTS_REQUEST } from '../constants';

const initialState = false;

export default function isFetching(state = initialState, action) {
    switch (action.type) {
        case FETCH_NEW_PAYMENTS_REQUEST:
            return action.isFetching;
        default:
            return state;
    }
}
