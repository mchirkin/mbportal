import { RECEIVE_SIGNED_PAYMENTS } from '../constants';

const initialState = null;

export default function data(state = initialState, action) {
    switch (action.type) {
        case RECEIVE_SIGNED_PAYMENTS:
            return action.data;
        default:
            return state;
    }
}
