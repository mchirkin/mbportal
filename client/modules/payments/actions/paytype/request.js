import { FETCH_PAYMENTSMODAL_DATA_PAYTYPE_REQUEST } from '../../constants';

export default function requestPayments(isFetching) {
    return {
        type: FETCH_PAYMENTSMODAL_DATA_PAYTYPE_REQUEST,
        isFetching,
    };
}
