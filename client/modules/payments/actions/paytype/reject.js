import { FETCH_PAYMENTSMODAL_DATA_PAYTYPE_FAILURE } from '../../constants';

export default function rejectPayments(error) {
    return {
        type: FETCH_PAYMENTSMODAL_DATA_PAYTYPE_FAILURE,
        error,
    };
}
