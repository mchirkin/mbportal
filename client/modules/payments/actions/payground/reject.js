import { FETCH_PAYMENTSMODAL_DATA_PAY_GROUND_FAILURE } from '../../constants';

export default function rejectPayments(error) {
    return {
        type: FETCH_PAYMENTSMODAL_DATA_PAY_GROUND_FAILURE,
        error,
    };
}
