import fetch from 'utils/fetch_wrapper';
import requestPayments from './request';
import receivePayments from './receive';
import rejectPayments from './reject';

export default function fetchPayGround() {
    return (dispatch) => {
        dispatch(requestPayments(true));

        const url = '/api/v1/products/pay_ground';
        const options = {
            credentials: 'include',
        };

        return fetch(url, options)
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
                response.json().then(error => dispatch(rejectPayments(error)));
            })
            .then((json) => {
                dispatch(requestPayments(false));
                dispatch(receivePayments(json));
                dispatch(rejectPayments(null));
            })
            .catch((error) => {
                dispatch(requestPayments(false));
                dispatch(rejectPayments(error));
            });
    };
}
