import { FETCH_PAYMENTSMODAL_DATA_PAY_GROUND_REQUEST } from '../../constants';

export default function requestPayments(isFetching) {
    return {
        type: FETCH_PAYMENTSMODAL_DATA_PAY_GROUND_REQUEST,
        isFetching,
    };
}
