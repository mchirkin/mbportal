import { FETCH_PAYMENTSMODAL_DATA_PSTATUS_FAILURE } from '../../constants';

export default function rejectPayments(error) {
    return {
        type: FETCH_PAYMENTSMODAL_DATA_PSTATUS_FAILURE,
        error,
    };
}
