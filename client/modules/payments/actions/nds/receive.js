import { FETCH_PAYMENTSMODAL_DATA_NDS_SUCCESS } from '../../constants';

export default function receivePayments(data) {
    return {
        type: FETCH_PAYMENTSMODAL_DATA_NDS_SUCCESS,
        data,
    };
}
