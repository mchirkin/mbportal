import { FETCH_PAYMENTSMODAL_DATA_NDS_FAILURE } from '../../constants';

export default function rejectPayments(error) {
    return {
        type: FETCH_PAYMENTSMODAL_DATA_NDS_FAILURE,
        error,
    };
}
