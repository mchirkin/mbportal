import { SET_PAYMENT_MODAL_DATA } from '../constants';

const initialState = null;

export default function data(state = initialState, action) {
    switch (action.type) {
        case SET_PAYMENT_MODAL_DATA:
            return action.data;
        default:
            return state;
    }
}
