import { combineReducers } from 'redux';
import isVisible from './is_visible';
import data from './data';

const reducers = combineReducers({
    isVisible,
    data,
});

export default reducers;
