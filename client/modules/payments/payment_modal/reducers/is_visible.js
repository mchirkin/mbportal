import { SET_PAYMENT_MODAL_VISIBILITY } from '../constants';

const initialState = false;

export default function isVisible(state = initialState, action) {
    switch (action.type) {
        case SET_PAYMENT_MODAL_VISIBILITY:
            return action.isVisible;
        default:
            return state;
    }
}
