import { SET_PAYMENT_MODAL_VISIBILITY } from '../constants';

export default function setVisibility(isVisible) {
    return {
        type: SET_PAYMENT_MODAL_VISIBILITY,
        isVisible,
    };
}
