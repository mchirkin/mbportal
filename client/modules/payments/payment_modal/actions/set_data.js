import { SET_PAYMENT_MODAL_DATA } from '../constants';

export default function setData(data) {
    if (data) {
        window.paymentFormWillReceiveData = true;
    } else {
        window.paymentFormWillReceiveData = false;
    }

    return {
        type: SET_PAYMENT_MODAL_DATA,
        data,
    };
}
