import { SET_PAYMENTSMODAL_DATA_PAY_GROUND } from '../../constants';

const initialState = false;

export default function isFetching(state = initialState, action) {
    switch (action.type) {
        case SET_PAYMENTSMODAL_DATA_PAY_GROUND:
            return action.isFetching;
        default:
            return state;
    }
}
