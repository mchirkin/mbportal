import { combineReducers } from 'redux';
import paymentsPstatus from './pstatus';
import paymentsSorts from './sorts';
import paymentsTaxperiod from './taxperiod';
import paymentsPaytype from './paytype';
import paymentsNds from './nds';
import paymentsPayground from './payground';
import paymentModal from '../payment_modal/reducers';
import errorPayments from '../error_payments/reducers';
import newPayments from '../new_payments/reducers';
import sendPayments from '../send_payments/reducers';
import codevoPayments from '../codevo/reducers';

const reducers = combineReducers({
    paymentsPstatus,
    paymentsSorts,
    paymentsTaxperiod,
    paymentsPaytype,
    paymentsPayground,
    paymentsNds,
    paymentModal,
    errorPayments,
    newPayments,
    sendPayments,
    codevoPayments,
});

export default reducers;
