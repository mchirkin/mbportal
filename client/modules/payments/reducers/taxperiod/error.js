import { FETCH_PAYMENTSMODAL_DATA_TAXPERIOD_FAILURE } from '../../constants';

const initialState = null;

export default function error(state = initialState, action) {
    switch (action.type) {
        case FETCH_PAYMENTSMODAL_DATA_TAXPERIOD_FAILURE:
            return action.error;
        default:
            return state;
    }
}
