import { FETCH_PAYMENTSMODAL_DATA_NDS_SUCCESS } from '../../constants';

const initialState = null;

export default function data(state = initialState, action) {
    switch (action.type) {
        case FETCH_PAYMENTSMODAL_DATA_NDS_SUCCESS:
            return action.data;
        default:
            return state;
    }
}
