import { fetch } from 'utils';

const FETCH_INVOICE_TEMPLATE_REQUEST = 'INVOICE/TEMPLATE/FETCH/REQUEST';
const FETCH_INVOICE_TEMPLATE_SUCCESS = 'INVOICE/TEMPLATE/FETCH/SUCCESS';
const FETCH_INVOICE_TEMPLATE_FAILURE = 'INVOICE/TEMPLATE/FETCH/FAILURE';

const initialState = {
    data: null,
    error: null,
    isFetching: false,
};

export default function reducer(state = initialState, action = {}) {
    switch (action.type) {
        case FETCH_INVOICE_TEMPLATE_SUCCESS:
            return {
                ...state,
                data: action.data,
            };
        case FETCH_INVOICE_TEMPLATE_FAILURE:
            return {
                ...state,
                error: action.error,
            };
        case FETCH_INVOICE_TEMPLATE_REQUEST:
            return {
                ...state,
                isFetching: action.isFetching,
            };
        default: return state;
    }
}

export function receive(data) {
    return {
        type: FETCH_INVOICE_TEMPLATE_SUCCESS,
        data,
    };
}

export function reject(error) {
    return {
        type: FETCH_INVOICE_TEMPLATE_FAILURE,
        error,
    };
}

export function request(isFetching) {
    return {
        type: FETCH_INVOICE_TEMPLATE_REQUEST,
        isFetching,
    };
}

export function fetchInvoiceTemplates({ extCorporateRef }) {
    return (dispatch) => {
        dispatch(request(true));

        const url = '/api/v1/invoice/template/list';
        const options = {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify({
                extCorporateRef,
            }),
        };

        return fetch(url, options)
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
                return Promise.reject('Network error');
            })
            .then((json) => {
                dispatch(request(false));
                if (json) {
                    if (json.errorText) {
                        return Promise.reject(json.errorText);
                    }
                    dispatch(receive(json));
                } else {
                    dispatch(receive([]));
                }
                dispatch(reject(null));
            })
            .catch((error) => {
                dispatch(request(false));
                dispatch(receive([]));
                dispatch(reject(error));
            });
    };
}
