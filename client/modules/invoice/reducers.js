import { combineReducers } from 'redux';

import invoice from './invoice';
import templates from './templates';

const reducers = combineReducers({
    invoice,
    templates,
});

export default reducers;
