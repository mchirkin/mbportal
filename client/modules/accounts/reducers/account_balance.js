import { SET_ACCOUNT_BALANCE } from '../constants';

const initialState = {
    open: 0,
    close: 0,
};

export default function balance(state = initialState, action) {
    switch (action.type) {
        case SET_ACCOUNT_BALANCE:
            return action.balance;
        default:
            return state;
    }
}
