import { SET_CANCEL_DOCUMENT_MODAL_VISIBILITY } from '../constants';

export default function cancelDocumentModalVisibility(state = false, action) {
    switch (action.type) {
        case SET_CANCEL_DOCUMENT_MODAL_VISIBILITY:
            return action.isVisible;
        default:
            return state;
    }
}
