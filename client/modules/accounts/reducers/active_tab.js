import { SET_ACCOUNT_PAGE_ACTIVE_TAB } from '../constants';

export default function activeTab(state = 'statement', action) {
    switch (action.type) {
        case SET_ACCOUNT_PAGE_ACTIVE_TAB:
            return action.tab;
        default:
            return state;
    }
}
