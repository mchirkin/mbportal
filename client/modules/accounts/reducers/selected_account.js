import { SELECT_ACCOUNT } from '../constants';

const initialState = null;

export default function selectedAccount(state = initialState, action) {
    switch (action.type) {
        case SELECT_ACCOUNT:
            return action.data;
        default:
            return state;
    }
}
