import { SET_CANCEL_DOCUMENT_MODAL_CANCEL_ID } from '../constants';

export default function cancelId(state = null, action) {
    switch (action.type) {
        case SET_CANCEL_DOCUMENT_MODAL_CANCEL_ID:
            return action.id;
        default:
            return state;
    }
}
