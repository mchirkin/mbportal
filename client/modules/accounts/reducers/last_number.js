import { LAST_NUMBER_ACCOUNT } from '../constants';

const initialState = null;

export default function lastNumber(state = initialState, action) {
    switch (action.type) {
        case LAST_NUMBER_ACCOUNT:
            return action.number;
        default:
            return state;
    }
}
