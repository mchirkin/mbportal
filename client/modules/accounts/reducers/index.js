import { combineReducers } from 'redux';
import isFetching from './is_fetching';
import data from './data';
import error from './error';
import filter from './filter';
import selectedAccount from './selected_account';
import activeTab from './active_tab';
import cancelDocumentModalVisibility from './cancel_document_modal_visibility';
import cancelDocumentModalData from './cancel_document_modal_data';
import cancelId from './cancel_id';
import balance from './account_balance';
import stays from '../stays/reducers';

const accounts = combineReducers({
    isFetching,
    data,
    error,
    filter,
    selectedAccount,
    activeTab,
    cancelDocumentModalVisibility,
    cancelDocumentModalData,
    cancelId,
    balance,
    stays,
});

export default accounts;
