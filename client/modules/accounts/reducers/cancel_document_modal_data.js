import { SET_CANCEL_DOCUMENT_MODAL_DATA } from '../constants';

export default function cancelDocumentModalData(state = null, action) {
    switch (action.type) {
        case SET_CANCEL_DOCUMENT_MODAL_DATA:
            return action.data;
        default:
            return state;
    }
}
