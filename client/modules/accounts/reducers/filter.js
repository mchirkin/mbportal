import { SET_ACCOUNT_FILTER } from '../constants';

const initialState = 'ALL';

export default function filter(state = initialState, action) {
    switch (action.type) {
        case SET_ACCOUNT_FILTER:
            return action.filter;
        default:
            return state;
    }
}
