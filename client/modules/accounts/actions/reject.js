import { FETCH_ACCOUNTS_FAILURE } from '../constants';

export default function rejectAccounts(error) {
    return {
        type: FETCH_ACCOUNTS_FAILURE,
        error,
    };
}
