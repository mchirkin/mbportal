import { SET_CANCEL_DOCUMENT_MODAL_CANCEL_ID } from '../constants';

export default function setCancelId(id) {
    return {
        type: SET_CANCEL_DOCUMENT_MODAL_CANCEL_ID,
        id,
    };
}
