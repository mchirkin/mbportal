import fetch from 'utils/fetch_wrapper';
import requestAccounts from './request';
import receiveAccounts from './receive';
import rejectAccounts from './reject';

export default function fetchAccounts() {
    return (dispatch) => {
        dispatch(requestAccounts(true));

        const url = '/api/v1/products/accounts/offline';
        const options = {
            credentials: 'include',
        };

        return fetch(url, options)
            .then((response) => {
                dispatch(rejectAccounts(null));
                if (response.ok) {
                    return response.json();
                }
                return Promise.reject('Network error');
            })
            .then((json) => {
                dispatch(requestAccounts(false));
                if (json) {
                    const accounts = typeof json.account.length !== 'undefined' ? json.account : [json.account];
                    dispatch(receiveAccounts(accounts));
                    dispatch(rejectAccounts(null));
                }
            })
            .catch((error) => {
                dispatch(requestAccounts(false));
                dispatch(rejectAccounts({
                    offline: true,
                    error,
                }));
            });
    };
}
