import { FETCH_ACCOUNTS_REQUEST } from '../constants';

export default function requestAccounts(isFetching) {
    return {
        type: FETCH_ACCOUNTS_REQUEST,
        isFetching,
    };
}
