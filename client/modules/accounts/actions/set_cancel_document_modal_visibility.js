import { SET_CANCEL_DOCUMENT_MODAL_VISIBILITY } from '../constants';

export default function setCancelDocumentModalVisibility(isVisible) {
    return {
        type: SET_CANCEL_DOCUMENT_MODAL_VISIBILITY,
        isVisible,
    };
}
