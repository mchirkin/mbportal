import { SET_ACCOUNT_BALANCE } from '../constants';

export default function setAccountBalance(balance) {
    return {
        type: SET_ACCOUNT_BALANCE,
        balance,
    };
}
