import fetch from 'utils/fetch_wrapper';
import requestAccounts from './request';
import receiveAccounts from './receive';
import rejectAccounts from './reject';

export default function fetchAccounts() {
    return (dispatch) => {
        dispatch(requestAccounts(true));

        const url = '/api/v1/products/accounts';
        const options = {
            credentials: 'include',
        };

        return fetch(url, options)
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
                return Promise.reject('Network error');
            })
            .then((json) => {
                dispatch(requestAccounts(false));
                if (json) {
                    if (json.errorText) {
                        return Promise.reject(json.errorText);
                    }
                    const accounts = typeof json.account.length !== 'undefined' ? json.account : [json.account];
                    dispatch(receiveAccounts(accounts));
                } else {
                    dispatch(receiveAccounts([]));
                }
                dispatch(rejectAccounts(null));
            })
            .catch((error) => {
                dispatch(requestAccounts(false));
                dispatch(rejectAccounts(error));
            });
    };
}
