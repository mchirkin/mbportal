import { FETCH_ACCOUNTS_SUCCESS } from '../constants';

export default function receiveAccounts(data) {
    return {
        type: FETCH_ACCOUNTS_SUCCESS,
        data,
    };
}
