import { SET_CANCEL_DOCUMENT_MODAL_DATA } from '../constants';

export default function setCancelDocumentModalData(data) {
    return {
        type: SET_CANCEL_DOCUMENT_MODAL_DATA,
        data,
    };
}
