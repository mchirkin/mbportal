import { SET_ACCOUNT_FILTER } from '../constants';

export default function setAccountFilter(filter) {
    return {
        type: SET_ACCOUNT_FILTER,
        filter,
    };
}
