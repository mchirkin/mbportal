import { SET_ACCOUNT_PAGE_ACTIVE_TAB } from '../constants';

export default function setActiveTab(tab) {
    return {
        type: SET_ACCOUNT_PAGE_ACTIVE_TAB,
        tab,
    };
}
