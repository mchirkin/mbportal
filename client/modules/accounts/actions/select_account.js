import { SELECT_ACCOUNT } from '../constants';

export default function selectAccount(data) {
    return {
        type: SELECT_ACCOUNT,
        data,
    };
}
