import { FETCH_PAYMENT_DATA } from '../constants';

export default function fetchPayments(payments) {
    return {
        type: FETCH_PAYMENT_DATA,
        payments,
    };
}
