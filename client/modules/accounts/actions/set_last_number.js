import { LAST_NUMBER_ACCOUNT } from '../constants';

export default function setLastNumber(number) {
    return {
        type: LAST_NUMBER_ACCOUNT,
        number,
    };
}
