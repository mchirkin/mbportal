import fetch from 'utils/fetch_wrapper';
import request from './request';
import receive from './receive';
import reject from './reject';

export default function fetchStays(accounts) {
    return (dispatch) => {
        dispatch(request(true));


        const url = '/api/v1/accounts/stays';
        const options = {
            credentials: 'include',
            method: 'POST',
            body: JSON.stringify({
                accounts,
            }),
        };

        return fetch(url, options)
            .then(response => response.json())
            .then((json) => {
                dispatch(request(false));
                if (json) {
                    dispatch(receive(json));
                } else {
                    dispatch(receive([]));
                }
                dispatch(reject(null));
            })
            .catch((error) => {
                dispatch(request(false));
                dispatch(reject(error));
                dispatch(receive([]));
            });
    };
}
