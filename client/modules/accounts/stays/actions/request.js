import { FETCH_ACCOUNTS_STAYS_REQUEST } from '../constants';

export default function request(isFetching) {
    return {
        type: FETCH_ACCOUNTS_STAYS_REQUEST,
        isFetching,
    };
}
