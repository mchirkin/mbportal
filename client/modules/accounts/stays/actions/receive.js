import { FETCH_ACCOUNTS_STAYS_SUCCESS } from '../constants';

export default function receive(data) {
    return {
        type: FETCH_ACCOUNTS_STAYS_SUCCESS,
        data,
    };
}
