import { FETCH_ACCOUNTS_STAYS_FAILURE } from '../constants';

export default function reject(error) {
    return {
        type: FETCH_ACCOUNTS_STAYS_FAILURE,
        error,
    };
}
