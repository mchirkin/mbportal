import { TOGGLE_LOADER } from '../constants';

export default function toggleLoader(isVisible) {
    return {
        type: TOGGLE_LOADER,
        isVisible,
    };
}
