import { TOGGLE_LOADER } from '../constants';

const initialState = false;

export default function isVisible(state = initialState, action) {
    switch (action.type) {
        case TOGGLE_LOADER:
            return action.isVisible;
        default:
            return state;
    }
}
