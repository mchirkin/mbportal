import { SET_DATA } from '../constants';

const initialState = {
    isVisible: false,
};

export default function data(state = initialState, action) {
    switch (action.type) {
        case SET_DATA:
            return Object.assign({}, state, action.data);
        default:
            return state;
    }
}
