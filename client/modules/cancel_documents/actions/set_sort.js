import { SET_STATEMENT_SORT } from '../constants';

export default function setStatementSort(sort) {
    return {
        type: SET_STATEMENT_SORT,
        sort,
    };
}
