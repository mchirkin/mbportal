import { FETCH_CANCEL_DOCUMENTS_REQUEST } from '../constants';

export default function request(isFetching) {
    return {
        type: FETCH_CANCEL_DOCUMENTS_REQUEST,
        isFetching,
    };
}
