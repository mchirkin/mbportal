import { SET_CANCEL_DOCUMENTS_TABLE_FIELD_VISIBILITY } from '../constants';

export default function setFieldVisibility({ field, visible }) {
    return {
        type: SET_CANCEL_DOCUMENTS_TABLE_FIELD_VISIBILITY,
        field,
        visible,
    };
}
