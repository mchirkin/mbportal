import { FETCH_CANCEL_DOCUMENTS_SUCCESS } from '../constants';

export default function receive(data) {
    return {
        type: FETCH_CANCEL_DOCUMENTS_SUCCESS,
        data,
    };
}
