import { FETCH_CANCEL_DOCUMENTS_FAILURE } from '../constants';

export default function reject(error) {
    return {
        type: FETCH_CANCEL_DOCUMENTS_FAILURE,
        error,
    };
}
