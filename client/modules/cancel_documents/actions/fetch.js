import fetch from 'utils/fetch_wrapper';
import request from './request';
import receive from './receive';
import reject from './reject';

export default function fetchCancel(params) {
    return (dispatch) => {
        dispatch(request(true));

        const url = '/api/v1/cancel_documents/platpor/list';
        const options = {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify({
                count: params.count,
                accNumber: params.accNumber,
                dateFrom: params.dateFrom,
                dateTo: params.dateTo,
                statusList: params.statusList,
            }),
        };

        return fetch(url, options)
            .then(response => response.json())
            .then((json) => {
                dispatch(request(false));
                if (json.errorText) {
                    dispatch(receive([]));
                    dispatch(reject(json));
                    return;
                }
                if (json) {
                    dispatch(receive(json));
                    dispatch(reject(null));
                }
            })
            .catch((error) => {
                dispatch(request(false));
                dispatch(receive([]));
                dispatch(reject(error));
            });
    };
}
