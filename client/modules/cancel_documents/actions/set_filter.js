import { SET_CANCEL_DOCUMENTS_FILTER } from '../constants';

export default function setFilter(filter) {
    return {
        type: SET_CANCEL_DOCUMENTS_FILTER,
        filter,
    };
}
