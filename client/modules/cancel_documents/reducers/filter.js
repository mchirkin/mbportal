import { SET_CANCEL_DOCUMENTS_FILTER } from '../constants';

const initialState = {
    count: 20,
};

export default function filter(state = initialState, action) {
    switch (action.type) {
        case SET_CANCEL_DOCUMENTS_FILTER:
            return action.filter;
        default:
            return state;
    }
}
