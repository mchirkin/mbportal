import { SET_STATEMENT_SORT } from '../constants';

const initialState = {
    number: {
        enable: false,
        order: 'desc',
    },
    operationType: {
        enable: false,
        order: 'desc',
    },
    docDate: {
        enable: false,
        order: 'desc',
    },
    contragent: {
        enable: false,
        order: 'desc',
    },
    contragent_account: {
        enable: false,
        order: 'desc',
    },
    inn: {
        enable: false,
        order: 'desc',
    },
    bank_bik: {
        enable: false,
        order: 'desc',
    },
    debet: {
        enable: false,
        order: 'desc',
    },
    credit: {
        enable: false,
        order: 'desc',
    },

};

export default function sort(state = initialState, action) {
    const newSort = Object.assign({}, state);

    switch (action.type) {
        case SET_STATEMENT_SORT:
            Object.keys(newSort).forEach((prop) => {
                newSort[prop].enable = false;
                if (prop === action.sort.field) {
                    newSort[prop] = {
                        enable: action.sort.enable,
                        order: action.sort.order || 'desc',
                    };
                }
            });
            /*
            newSort[action.sort.field] = Object.assign({}, state[action.sort.field], {
                enable: action.sort.enable,
                order: action.sort.order || 'desc',
            });
            */
            return newSort;// Object.assign({}, state, newSort);
        default:
            return state;
    }
}
