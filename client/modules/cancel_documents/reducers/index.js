import { combineReducers } from 'redux';
import isFetching from './is_fetching';
import data from './data';
import error from './error';
import filter from './filter';
import fieldVisibility from './field_visibility';

const reducers = combineReducers({
    isFetching,
    data,
    error,
    filter,
    fieldVisibility,
});

export default reducers;
