import { FETCH_CANCEL_DOCUMENTS_FAILURE } from '../constants';

const initialState = null;

export default function error(state = initialState, action) {
    switch (action.type) {
        case FETCH_CANCEL_DOCUMENTS_FAILURE:
            return action.error;
        default:
            return state;
    }
}
