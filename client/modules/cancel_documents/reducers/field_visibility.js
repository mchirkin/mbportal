import { SET_CANCEL_DOCUMENTS_TABLE_FIELD_VISIBILITY } from '../constants';

const initialState = {
    amount: true,
    corrInn: true,
    corrFullname: true,
};

export default function fieldVisibility(state = initialState, action) {
    switch (action.type) {
        case SET_CANCEL_DOCUMENTS_TABLE_FIELD_VISIBILITY:
            return Object.assign({}, state, {
                [action.field]: action.visible,
            });
        default:
            return state;
    }
}
