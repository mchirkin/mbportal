import { FETCH_CANCEL_DOCUMENTS_REQUEST } from '../constants';

const initialState = false;

export default function isFetching(state = initialState, action) {
    switch (action.type) {
        case FETCH_CANCEL_DOCUMENTS_REQUEST:
            return action.isFetching;
        default:
            return state;
    }
}
