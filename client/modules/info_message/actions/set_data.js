import { SET_DATA } from '../constants';

export default function setData(data) {
    return {
        type: SET_DATA,
        data,
    };
}
