import { SET_DATA } from '../constants';

const initialState = {
    isVisible: false,
    btnText: 'Далее',
};

export default function data(state = initialState, action) {
    switch (action.type) {
        case SET_DATA:
            return action.data;
        default:
            return state;
    }
}
