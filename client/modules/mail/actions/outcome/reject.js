import { FETCH_MAIL_OUTCOME_FAILURE } from '../../constants';

export default function reject(error) {
    return {
        type: FETCH_MAIL_OUTCOME_FAILURE,
        error,
    };
}
