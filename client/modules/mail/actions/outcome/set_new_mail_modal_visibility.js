import { SET_NEW_MAIL_MODAL_VISIBILITY } from '../../constants';

export default function setNewMailModalVisibility(isVisible) {
    return {
        type: SET_NEW_MAIL_MODAL_VISIBILITY,
        isVisible,
    };
}
