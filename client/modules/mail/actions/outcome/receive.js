import { FETCH_MAIL_OUTCOME_SUCCESS } from '../../constants';

export default function receive(data) {
    return {
        type: FETCH_MAIL_OUTCOME_SUCCESS,
        data,
    };
}
