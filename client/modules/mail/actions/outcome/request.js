import { FETCH_MAIL_OUTCOME_REQUEST } from '../../constants';

export default function request(isFetching) {
    return {
        type: FETCH_MAIL_OUTCOME_REQUEST,
        isFetching,
    };
}
