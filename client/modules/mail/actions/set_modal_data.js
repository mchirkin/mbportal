import { SET_MAIL_MODAL_DATA } from '../constants';

export default function setModalData(data) {
    return {
        type: SET_MAIL_MODAL_DATA,
        data,
    };
}
