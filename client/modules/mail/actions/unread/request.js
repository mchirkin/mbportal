import { FETCH_MAIL_UNREAD_REQUEST } from '../../constants';

export default function request(isFetching) {
    return {
        type: FETCH_MAIL_UNREAD_REQUEST,
        isFetching,
    };
}
