import { FETCH_MAIL_UNREAD_FAILURE } from '../../constants';

export default function reject(error) {
    return {
        type: FETCH_MAIL_UNREAD_FAILURE,
        error,
    };
}
