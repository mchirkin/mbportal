import { FETCH_MAIL_UNREAD_SUCCESS } from '../../constants';

export default function receive(data) {
    return {
        type: FETCH_MAIL_UNREAD_SUCCESS,
        data,
    };
}
