import { fetch } from 'utils';
import request from './request';
import receive from './receive';
import reject from './reject';

export default function fetchUnread() {
    return async (dispatch) => {
        dispatch(request(true));

        const url = '/api/v1/msg/mail/unread';

        try {
            const response = await fetch(url);

            dispatch(request(false));

            if (!response.ok) {
                return Error('Network error');
            }

            const json = await response.json();
            if (json.errorText) {
                return Error(json.errorText);
            }

            dispatch(receive(json));
            dispatch(reject(null));
        } catch (error) {
            dispatch(request(false));
            dispatch(reject(error));
        }
    };
}
