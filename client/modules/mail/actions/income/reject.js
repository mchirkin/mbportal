import { FETCH_MAIL_INCOME_FAILURE } from '../../constants';

export default function reject(error) {
    return {
        type: FETCH_MAIL_INCOME_FAILURE,
        error,
    };
}
