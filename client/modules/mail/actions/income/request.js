import { FETCH_MAIL_INCOME_REQUEST } from '../../constants';

export default function request(isFetching) {
    return {
        type: FETCH_MAIL_INCOME_REQUEST,
        isFetching,
    };
}
