import { FETCH_MAIL_INCOME_SUCCESS } from '../../constants';

export default function receive(data) {
    return {
        type: FETCH_MAIL_INCOME_SUCCESS,
        data,
    };
}
