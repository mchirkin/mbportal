import fetch from 'utils/fetch_wrapper';
import request from './request';
import receive from './receive';
import reject from './reject';

export default function fetchOutcome(filter = false) {
    return (dispatch) => {
        dispatch(request(true));

        const url = '/api/v1/msg/mail/income';
        const options = {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify({
                filter,
            }),
        };

        return fetch(url, options)
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
                return Promise.reject('Network error');
            })
            .then((json) => {
                dispatch(request(false));
                if (json) {
                    if (json.errorText) {
                        return Promise.reject(json.errorText);
                    }
                    const mail = Array.isArray(json.mail2ClientUl) ? json.mail2ClientUl : [json.mail2ClientUl];
                    dispatch(receive(mail || []));
                    dispatch(reject(null));
                } else {
                    dispatch(receive([]));
                    dispatch(reject(null));
                }
            })
            .catch((error) => {
                dispatch(request(false));
                dispatch(reject(error));
            });
    };
}
