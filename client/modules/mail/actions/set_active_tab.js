import { SET_MAIL_ACTIVE_TAB } from '../constants';

export default function setActiveTab(tab) {
    return {
        type: SET_MAIL_ACTIVE_TAB,
        tab,
    };
}
