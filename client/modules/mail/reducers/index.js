import { combineReducers } from 'redux';
import outcome from './outcome';
import income from './income';
import unread from './unread';
import modalData from './modal_data';
import activeTab from './active_tab';

const mail = combineReducers({
    outcome,
    income,
    unread,
    modalData,
    activeTab,
});

export default mail;
