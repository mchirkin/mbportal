import { FETCH_MAIL_UNREAD_REQUEST } from '../../constants';

const initialState = false;

export default function isFetching(state = initialState, action) {
    switch (action.type) {
        case FETCH_MAIL_UNREAD_REQUEST:
            return action.isFetching;
        default:
            return state;
    }
}
