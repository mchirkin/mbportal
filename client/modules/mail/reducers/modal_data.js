import { SET_MAIL_MODAL_DATA } from '../constants';

export default function modalData(state = null, action) {
    switch (action.type) {
        case SET_MAIL_MODAL_DATA:
            return action.data;
        default:
            return state;
    }
}
