import { SET_NEW_MAIL_MODAL_VISIBILITY } from '../../constants';

export default function newMailModalVisible(state = false, action) {
    switch (action.type) {
        case SET_NEW_MAIL_MODAL_VISIBILITY:
            return action.isVisible;
        default:
            return state;
    }
}
