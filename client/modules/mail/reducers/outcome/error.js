import { FETCH_MAIL_OUTCOME_FAILURE } from '../../constants';

const initialState = null;

export default function error(state = initialState, action) {
    switch (action.type) {
        case FETCH_MAIL_OUTCOME_FAILURE:
            return action.error;
        default:
            return state;
    }
}
