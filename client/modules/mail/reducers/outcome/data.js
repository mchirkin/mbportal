import { FETCH_MAIL_OUTCOME_SUCCESS } from '../../constants';

const initialState = null;

export default function data(state = initialState, action) {
    switch (action.type) {
        case FETCH_MAIL_OUTCOME_SUCCESS:
            return action.data;
        default:
            return state;
    }
}
