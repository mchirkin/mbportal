import { FETCH_MAIL_INCOME_REQUEST } from '../../constants';

const initialState = false;

export default function isFetching(state = initialState, action) {
    switch (action.type) {
        case FETCH_MAIL_INCOME_REQUEST:
            return action.isFetching;
        default:
            return state;
    }
}
