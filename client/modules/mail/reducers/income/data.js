import { FETCH_MAIL_INCOME_SUCCESS } from '../../constants';

const initialState = null;

export default function data(state = initialState, action) {
    switch (action.type) {
        case FETCH_MAIL_INCOME_SUCCESS:
            return action.data;
        default:
            return state;
    }
}
