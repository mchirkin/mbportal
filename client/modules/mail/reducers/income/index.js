import { combineReducers } from 'redux';
import isFetching from './is_fetching';
import data from './data';
import error from './error';
import newMailModalVisible from './new_mail_modal_visible';

const income = combineReducers({
    isFetching,
    data,
    error,
    newMailModalVisible,
});

export default income;
