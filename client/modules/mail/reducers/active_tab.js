import { SET_MAIL_ACTIVE_TAB } from '../constants';

export default function activeTab(state = 'income', action) {
    switch (action.type) {
        case SET_MAIL_ACTIVE_TAB:
            return action.tab;
        default:
            return state;
    }
}
