import { fetch } from 'utils';

const FETCH_USER_NOTE_REQUEST = 'USER_NOTE/FETCH/REQUEST';
const FETCH_USER_NOTE_SUCCESS = 'USER_NOTE/FETCH/SUCCESS';
const FETCH_USER_NOTE_FAILURE = 'USER_NOTE/FETCH/FAILURE';

const initialState = {
    data: null,
    error: null,
    isFetching: false,
};

export default function reducer(state = initialState, action = {}) {
    switch (action.type) {
        case FETCH_USER_NOTE_SUCCESS:
            return {
                ...state,
                data: action.data,
            };
        case FETCH_USER_NOTE_FAILURE:
            return {
                ...state,
                error: action.error,
            };
        case FETCH_USER_NOTE_REQUEST:
            return {
                ...state,
                isFetching: action.isFetching,
            };
        default: return state;
    }
}

export function receive(data) {
    return {
        type: FETCH_USER_NOTE_SUCCESS,
        data,
    };
}

export function reject(error) {
    return {
        type: FETCH_USER_NOTE_FAILURE,
        error,
    };
}

export function request(isFetching) {
    return {
        type: FETCH_USER_NOTE_REQUEST,
        isFetching,
    };
}

export function fetchNote({ extRef }) {
    return (dispatch) => {
        dispatch(request(true));

        const url = '/api/v1/financial_calendar/user_note/list';
        const options = {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify({
                extRef,
            }),
        };

        return fetch(url, options)
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
                return Promise.reject('Network error');
            })
            .then((json) => {
                dispatch(request(false));
                if (json) {
                    if (json.errorText) {
                        return Promise.reject(json.errorText);
                    }
                    dispatch(receive(json));
                } else {
                    dispatch(receive([]));
                }
                dispatch(reject(null));
            })
            .catch((error) => {
                dispatch(request(false));
                dispatch(receive([]));
                dispatch(reject(error));
            });
    };
}
