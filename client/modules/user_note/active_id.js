const USER_NOTE_ACTIVE_ID = 'USER_NOTE_ACTIVE_ID';

const initialState = '';

export default function reducer(state = initialState, action = {}) {
    switch (action.type) {
        case USER_NOTE_ACTIVE_ID:
            return action.activeId;
        default:
            return state;
    }
}

export function setUserNoteActiveId(activeId) {
    return {
        type: USER_NOTE_ACTIVE_ID,
        activeId,
    };
}

