import { combineReducers } from 'redux';

import note from './note';
import activeId from './active_id';

const reducers = combineReducers({
    note,
    activeId,
});

export default reducers;
