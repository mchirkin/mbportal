import { combineReducers } from 'redux';

import isVisible from './toggle_calendar_widget';

const reducers = combineReducers({
    isVisible,
});

export default reducers;
