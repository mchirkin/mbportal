const TOGGLE_CALENDAR_WIDGET = 'TOGGLE_CALENDAR_WIDGET';

const initialState = false;

export default function reducer(state = initialState, action = {}) {
    switch (action.type) {
        case TOGGLE_CALENDAR_WIDGET:
            return action.isVisible;
        default:
            return state;
    }
}

export function toggleCalendarWidget(isVisible) {
    return {
        type: TOGGLE_CALENDAR_WIDGET,
        isVisible,
    };
}
