import { SET_STATEMENT_FILTER } from '../constants';

export default function filter(state = {}, action) {
    switch (action.type) {
        case SET_STATEMENT_FILTER:
            return action.filter;
        default:
            return state;
    }
}
