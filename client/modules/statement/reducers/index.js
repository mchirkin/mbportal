import { combineReducers } from 'redux';
import isFetching from './is_fetching';
import data from './data';
import error from './error';
import sort from './sort';
import fieldVisibility from './field_visibility';
import filter from './filter';

const statement = combineReducers({
    isFetching,
    data,
    error,
    sort,
    fieldVisibility,
    filter,
});

export default statement;
