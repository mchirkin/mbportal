import { SET_FIELD_VISIBILITY } from '../constants';

const initialState = {
    number: true,
    operationType: true,
    docDate: true,
    contragent: true,
    contragent_account: false,
    inn: true,
    bank_bik: false,
    bank_name: false,
    debet: true,
    credit: true,
    description: true,
};

export default function fieldVisibility(state = initialState, action) {
    switch (action.type) {
        case SET_FIELD_VISIBILITY:
            return Object.assign({}, state, {
                [action.field]: action.visible,
            });
        default:
            return state;
    }
}
