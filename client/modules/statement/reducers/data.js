import { FETCH_STATEMENT_SUCCESS } from '../constants';

const initialState = null;

export default function data(state = initialState, action) {
    switch (action.type) {
        case FETCH_STATEMENT_SUCCESS:
            if (state !== null && state.requestTime > action.data.requestTime) {
                return state;
            }
            return action.data;
        default:
            return state;
    }
}
