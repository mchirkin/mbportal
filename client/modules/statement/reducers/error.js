import { FETCH_STATEMENT_FAILURE } from '../constants';

const initialState = null;

export default function error(state = initialState, action) {
    switch (action.type) {
        case FETCH_STATEMENT_FAILURE:
            if (state !== null && state.requestTime > action.data.requestTime) {
                return state;
            }
            return action.error;
        default:
            return state;
    }
}
