import fetch from 'utils/fetch_wrapper';
import checkResponseAndGetJson from 'utils/check_response_and_get_json';
import fetchActionCreator from 'utils/redux/fetch_action_creator';
import {
    FETCH_STATEMENT_REQUEST,
    FETCH_STATEMENT_SUCCESS,
    FETCH_STATEMENT_FAILURE,
} from '../constants';

const request = fetchActionCreator.request(FETCH_STATEMENT_REQUEST);
const receive = fetchActionCreator.receive(FETCH_STATEMENT_SUCCESS);
const reject = fetchActionCreator.reject(FETCH_STATEMENT_FAILURE);

export default function fetchStatement(params) {
    return async (dispatch) => {
        dispatch(request(true));
        try {
            const response = await fetch('/api/v1/statement/offline', {
                method: 'POST',
                credentials: 'include',
                body: JSON.stringify({
                    accountId: params.accountId,
                    dateFrom: params.dateFrom,
                    dateTo: params.dateTo,
                }),
            });
            const json = await checkResponseAndGetJson(response);
            dispatch(request(false));
            if (json) {
                dispatch(receive(json));
                dispatch(reject(null));
            }
        } catch (err) {
            dispatch(request(false));
            dispatch(reject(err));
        }
    };
}
