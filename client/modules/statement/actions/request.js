import fetchActionCreator from 'utils/redux/fetch_action_creator';
import { FETCH_STATEMENT_REQUEST } from '../constants';

export default fetchActionCreator.request(FETCH_STATEMENT_REQUEST);
