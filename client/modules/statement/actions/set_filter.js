import { SET_STATEMENT_FILTER } from '../constants';

export default function setFilter(filter) {
    return {
        type: SET_STATEMENT_FILTER,
        filter,
    };
}
