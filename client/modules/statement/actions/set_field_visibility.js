import { SET_FIELD_VISIBILITY } from '../constants';

export default function setFieldVisibility({ field, visible }) {
    return {
        type: SET_FIELD_VISIBILITY,
        field,
        visible,
    };
}
