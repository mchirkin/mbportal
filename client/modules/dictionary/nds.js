import { fetch } from 'utils';

const FETCH_NDS_REQUEST = 'DICTIONARY/NDS/FETCH/REQUEST';
const FETCH_NDS_SUCCESS = 'DICTIONARY/NDS/FETCH/SUCCESS';
const FETCH_NDS_FAILURE = 'DICTIONARY/NDS/FETCH/FAILURE';

const initialState = {
    data: null,
    error: null,
    isFetching: false,
};

export default function reducer(state = initialState, action = {}) {
    switch (action.type) {
        case FETCH_NDS_SUCCESS:
            return {
                ...state,
                data: action.data,
            };
        case FETCH_NDS_FAILURE:
            return {
                ...state,
                error: action.error,
            };
        case FETCH_NDS_REQUEST:
            return {
                ...state,
                isFetching: action.isFetching,
            };
        default: return state;
    }
}

export function receive(data) {
    return {
        type: FETCH_NDS_SUCCESS,
        data,
    };
}

export function reject(error) {
    return {
        type: FETCH_NDS_FAILURE,
        error,
    };
}

export function request(isFetching) {
    return {
        type: FETCH_NDS_REQUEST,
        isFetching,
    };
}

export function fetchNds() {
    return (dispatch) => {
        dispatch(request(true));

        const url = '/api/v1/dictionary/nds/list';
        const options = {
            credentials: 'include',
        };

        return fetch(url, options)
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
                return Promise.reject('Network error');
            })
            .then((json) => {
                dispatch(request(false));
                if (json) {
                    if (json.errorText) {
                        return Promise.reject(json.errorText);
                    }
                    dispatch(receive(json));
                } else {
                    dispatch(receive([]));
                }
                dispatch(reject(null));
            })
            .catch((error) => {
                dispatch(request(false));
                dispatch(receive([]));
                dispatch(reject(error));
            });
    };
}
