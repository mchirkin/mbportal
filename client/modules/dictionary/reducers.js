import { combineReducers } from 'redux';

import nds from './nds';

const reducers = combineReducers({
    nds,
});

export default reducers;
