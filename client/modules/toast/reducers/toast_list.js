import { ADD_TOAST, DELETE_TOAST } from '../constants';

export default function toastList(state = [], action) {
    switch (action.type) {
        case ADD_TOAST:
            return [...state, action.toast];
        case DELETE_TOAST:
            return state.filter(item => item !== action.toast);
        default:
            return state;
    }
}
