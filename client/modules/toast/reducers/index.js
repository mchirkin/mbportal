import { combineReducers } from 'redux';
import toastList from './toast_list';

const reducers = combineReducers({
    toastList,
});

export default reducers;
