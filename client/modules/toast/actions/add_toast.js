import { ADD_TOAST } from '../constants';

export default function addToast(toast) {
    return {
        type: ADD_TOAST,
        toast,
    };
}
