import { DELETE_TOAST } from '../constants';

export default function deleteToast(toast) {
    return {
        type: DELETE_TOAST,
        toast,
    };
}
