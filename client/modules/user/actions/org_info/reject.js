import { FETCH_ORG_INFO_FAILURE } from '../../constants';

export default function rejectOrgInfo(error) {
    return {
        type: FETCH_ORG_INFO_FAILURE,
        error,
    };
}
