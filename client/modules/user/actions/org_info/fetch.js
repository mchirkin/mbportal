import fetch from 'utils/fetch_wrapper';
import requestOrgInfo from './request';
import receiveOrgInfo from './receive';
import rejectOrgInfo from './reject';

export default function fetchOrgInfo() {
    return (dispatch) => {
        dispatch(requestOrgInfo(true));

        const url = '/api/v1/org_info';
        const options = {
            credentials: 'include',
        };

        return fetch(url, options)
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
                response.json().then(error => dispatch(rejectOrgInfo(error)));
            })
            .then((json) => {
                dispatch(requestOrgInfo(false));
                if (json) {
                    dispatch(receiveOrgInfo(json));
                    dispatch(rejectOrgInfo(null));
                }
            })
            .catch((error) => {
                dispatch(requestOrgInfo(false));
                dispatch(rejectOrgInfo(error));
            });
    };
}
