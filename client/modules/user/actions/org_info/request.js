import { FETCH_ORG_INFO_REQUEST } from '../../constants';

export default function requestOrgInfo(isFetching) {
    return {
        type: FETCH_ORG_INFO_REQUEST,
        isFetching,
    };
}
