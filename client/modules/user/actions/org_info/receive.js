import { FETCH_ORG_INFO_SUCCESS } from '../../constants';

export default function receiveOrgInfo(data) {
    return {
        type: FETCH_ORG_INFO_SUCCESS,
        data,
    };
}
