import fetch from 'utils/fetch_wrapper';
import fetchActionCreator from 'utils/redux/fetch_action_creator';
import checkResponseAndGetJson from 'utils/check_response_and_get_json';

import {
    FETCH_MANAGERS_SUCCESS,
    FETCH_MANAGERS_REQUEST,
    FETCH_MANAGERS_FAILURE,
} from '../../constants';

const request = fetchActionCreator.request(FETCH_MANAGERS_REQUEST);
const receive = fetchActionCreator.receive(FETCH_MANAGERS_SUCCESS);
const reject = fetchActionCreator.reject(FETCH_MANAGERS_FAILURE);

export default function fetchAction() {
    return async (dispatch) => {
        dispatch(request(true));
        try {
            const response = await fetch('/api/v1/managers', {
                credentials: 'include',
            });
            const json = await checkResponseAndGetJson(response, 'personBankInfoJson', true);
            dispatch(request(false));
            if (json) {
                dispatch(receive(json));
            } else {
                dispatch(receive([]));
            }
            dispatch(reject(null));
        } catch (err) {
            dispatch(request(false));
            dispatch(reject(err));
            dispatch(receive([]));
        }
    };
}
