import { USER_LOGOUT } from '../constants';

export default function logout() {
    return {
        type: USER_LOGOUT,
    };
}
