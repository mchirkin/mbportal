import { FETCH_USER_INFO_SUCCESS } from '../../constants';

export default function receiveUserInfo(data) {
    return {
        type: FETCH_USER_INFO_SUCCESS,
        data,
    };
}
