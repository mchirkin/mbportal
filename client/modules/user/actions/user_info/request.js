import { FETCH_USER_INFO_REQUEST } from '../../constants';

export default function requestUserInfo(isFetching) {
    return {
        type: FETCH_USER_INFO_REQUEST,
        isFetching,
    };
}
