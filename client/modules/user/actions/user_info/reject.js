import { FETCH_USER_INFO_FAILURE } from '../../constants';

export default function rejectUserInfo(error) {
    return {
        type: FETCH_USER_INFO_FAILURE,
        error,
    };
}
