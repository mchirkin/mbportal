import fetch from 'utils/fetch_wrapper';
import requestUserInfo from './request';
import receiveUserInfo from './receive';
import rejectUserInfo from './reject';

export default function fetchUserInfo(params) {
    return (dispatch) => {
        dispatch(requestUserInfo(true));

        let url = '/api/v1/login';
        const options = {
            credentials: 'include',
        };

        if (params) {
            options.method = 'POST';
            options.body = JSON.stringify(params);
        } else {
            url = '/api/v1/user_info';
            options.method = 'GET';
        }

        return fetch(url, options)
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
                response.json().then(error => dispatch(rejectUserInfo(error)));
            })
            .then((json) => {
                dispatch(requestUserInfo(false));
                if (json) {
                    dispatch(receiveUserInfo(json));
                    dispatch(rejectUserInfo(null));
                }
            })
            .catch((error) => {
                dispatch(requestUserInfo(false));
                dispatch(rejectUserInfo(error));
            });
    };
}
