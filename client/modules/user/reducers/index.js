import { combineReducers } from 'redux';
import userInfo from './user_info';
import orgInfo from './org_info';
import managers from './managers';

const user = combineReducers({
    userInfo,
    orgInfo,
    managers,
});

export default user;
