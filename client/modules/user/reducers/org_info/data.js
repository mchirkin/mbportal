import { FETCH_ORG_INFO_SUCCESS } from '../../constants';

const initialState = null;

export default function data(state = initialState, action) {
    switch (action.type) {
        case FETCH_ORG_INFO_SUCCESS:
            return action.data;
        default:
            return state;
    }
}
