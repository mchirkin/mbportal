import { FETCH_ORG_INFO_REQUEST } from '../../constants';

const initialState = false;

export default function isFetching(state = initialState, action) {
    switch (action.type) {
        case FETCH_ORG_INFO_REQUEST:
            return action.isFetching;
        default:
            return state;
    }
}
