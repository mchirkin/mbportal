import { FETCH_USER_INFO_FAILURE } from '../../constants';

const initialState = null;

export default function error(state = initialState, action) {
    switch (action.type) {
        case FETCH_USER_INFO_FAILURE:
            return action.error;
        default:
            return state;
    }
}
