import { FETCH_USER_INFO_SUCCESS } from '../../constants';

const initialState = null;

export default function data(state = initialState, action) {
    switch (action.type) {
        case FETCH_USER_INFO_SUCCESS:
            return action.data;
        default:
            return state;
    }
}
