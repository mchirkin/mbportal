import { FETCH_ACCOUNTS_LIMITS_SUCCESS } from '../constants';

const initialState = null;

export default function data(state = initialState, action) {
    switch (action.type) {
        case FETCH_ACCOUNTS_LIMITS_SUCCESS:
            return action.data;
        default:
            return state;
    }
}
