import { FETCH_ACCOUNTS_LIMITS_REQUEST } from '../constants';

export default function request(isFetching) {
    return {
        type: FETCH_ACCOUNTS_LIMITS_REQUEST,
        isFetching,
    };
}
