import fetch from 'utils/fetch_wrapper';
import request from './request';
import receive from './receive';
import reject from './reject';

export default function fetchAccountsLimits(account, branch) {
    return (dispatch) => {
        dispatch(request(true));

        const url = `/api/v1/products/account_limits?account=${account}&branch=${branch}`;
        const options = {
            credentials: 'include',
        };

        return fetch(url, options)
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
                response.json().then(error => dispatch(reject(error)));
            })
            .then((json) => {
                dispatch(request(false));
                if (json) {
                    dispatch(receive(json));
                    dispatch(reject(null));
                }
            })
            .catch((error) => {
                dispatch(request(false));
                dispatch(reject(error));
            });
    };
}
