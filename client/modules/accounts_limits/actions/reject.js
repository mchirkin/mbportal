import { FETCH_ACCOUNTS_LIMITS_FAILURE } from '../constants';

export default function reject(error) {
    return {
        type: FETCH_ACCOUNTS_LIMITS_FAILURE,
        error,
    };
}
