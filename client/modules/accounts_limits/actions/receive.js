import { FETCH_ACCOUNTS_LIMITS_SUCCESS } from '../constants';

export default function receive(data) {
    return {
        type: FETCH_ACCOUNTS_LIMITS_SUCCESS,
        data,
    };
}
