import { combineReducers } from 'redux';
import modalVisibility from './modal_visibility';

const reducers = combineReducers({
    modalVisibility,
});

export default reducers;
