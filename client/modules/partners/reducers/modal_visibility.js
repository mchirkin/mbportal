import { SHOW_PARTNERS_MODAL } from '../constants';

export default function modalVisibility(state = false, action) {
    switch (action.type) {
        case SHOW_PARTNERS_MODAL:
            return action.isVisible;
        default:
            return state;
    }
}
