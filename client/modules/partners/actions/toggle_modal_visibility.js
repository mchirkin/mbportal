import { SHOW_PARTNERS_MODAL } from '../constants';

export default function toggleModalVisibility(isVisible) {
    return {
        type: SHOW_PARTNERS_MODAL,
        isVisible,
    };
}
