import { SHOW_NOTIFICATION, CLOSE_NOTIFICATION } from '../constants';

export default function notification(state = null, action) {
    switch (action.type) {
        case SHOW_NOTIFICATION:
            return action.data;
        case CLOSE_NOTIFICATION:
            return null;
        default:
            return state;
    }
}
