import { combineReducers } from 'redux';
import data from './notification';

const reducers = combineReducers({
    data,
});

export default reducers;
