import { SHOW_NOTIFICATION } from '../constants';

export default function showNotification(data) {
    return {
        type: SHOW_NOTIFICATION,
        data,
    };
}
