import { CLOSE_NOTIFICATION } from '../constants';

export default function closeNotification() {
    return {
        type: CLOSE_NOTIFICATION,
    };
}
