import { combineReducers } from 'redux';
import callbackModalVisibility from './callback_modal_visibility';

const reducers = combineReducers({
    callbackModalVisibility,
});

export default reducers;
