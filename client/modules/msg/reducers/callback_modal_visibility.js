import { SET_CALLBACK_MODAL_VISIBILITY } from '../constants';

export default function callbackModalVisibility(state = false, action) {
    switch (action.type) {
        case SET_CALLBACK_MODAL_VISIBILITY:
            return action.isVisible;
        default:
            return state;
    }
}
