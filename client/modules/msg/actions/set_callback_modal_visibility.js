import { SET_CALLBACK_MODAL_VISIBILITY } from '../constants';

export default function setCallbackModalVisibility(isVisible) {
    return {
        type: SET_CALLBACK_MODAL_VISIBILITY,
        isVisible,
    };
}
