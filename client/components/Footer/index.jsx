import React from 'react';
import styles from './footer.css';

export default function Footer() {
    return (
        <div className={styles.footer}>
            <div className={styles['gradient-top']} />
            <div className={styles['footer-blocks']}>
                <div className={styles['footer-block']}>
                    <div className={styles['footer-block-item']}>
                        <div>
                            © РосЕвроБанк 1994-2017
                        </div>
                        <div>
                            Генеральная лицензия Банка России № 3137
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
