import React from 'react';
import styles from './loader.css';

const Loader = ({ visible, text, style }) => (
    <div
        className={styles.wrapper}
        style={style}
        data-visible={visible}
    >
        <div className={styles.loader} />
        <div className={styles.text}>
            {text}
        </div>
    </div>
);

export default Loader;
