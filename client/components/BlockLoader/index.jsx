import React from 'react';
import classNames from 'classnames/bind';
import styles from './loader.css';

const cx = classNames.bind(styles);

/**
 * Компонент для отображения загрузчика поверх содержимого
 * @param {Object} props
 * @return {ReactElement}
 */
export default function BlockLoader(props) {
    const loaderClasses = cx({
        'loader-container': true,
        'loader--visible': props.isFetching,
    });

    return (
        <div className={loaderClasses}>
            <div className={styles.loader}>Loading...</div>
        </div>
    );
}

/**
 * propTypes
 * @property {boolean} isFetching - отображать загручик или нет
 */
BlockLoader.propTypes = {
    isFetching: React.PropTypes.bool.isRequired,
};
