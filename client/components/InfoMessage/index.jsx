import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Scrollbars } from 'react-custom-scrollbars';

import setMessageData from 'modules/info_message/actions/set_data';

import Btn from 'components/ui/Btn';

import styles from './info-message.css';

class InfoMessage extends Component {
    static propTypes = {
        data: PropTypes.object,
        setMessageData: PropTypes.func.isRequired,
    };

    static defaultProps = {
        data: {},
    };

    componentWillUnmount() {
        this.props.setMessageData({
            isVisible: false,
        });
    }

    render() {
        const {
            isVisible,
            header,
            body,
            btnText,
            callback,
            showSubmitBtn,
            submitBtnText,
            submitCallback,
        } = this.props.data;

        return (
            <div className={styles.wrapper} data-visible={isVisible}>
                <Scrollbars>
                    <div
                        style={{
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'center',
                            justifyContent: 'center',
                            width: '100%',
                            height: '100%',
                        }}
                    >
                        <div className={styles.header}>
                            {header}
                        </div>
                        <div className={styles.body}>
                            {body}
                        </div>
                        <div className={styles['btn-row']}>
                            <Btn
                                caption={btnText}
                                bgColor="grey"
                                onClick={callback}
                                style={{
                                    minWidth: 200,
                                }}
                            />
                            {showSubmitBtn
                                ? (
                                    <Btn
                                        caption={submitBtnText}
                                        bgColor="grey"
                                        onClick={submitCallback}
                                        style={{
                                            minWidth: 200,
                                            marginLeft: 25,
                                        }}
                                    />
                                )
                                : null}
                        </div>
                    </div>
                </Scrollbars>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        data: state.infoMessage.data,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        setMessageData: bindActionCreators(setMessageData, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(InfoMessage);
