import React from 'react';
import PropTypes from 'prop-types';
import styles from './toast.css';

export default function ToastItem({ data }) {
    const { text } = data;
    return (
        <div className={styles['toast-item-container']}>
            <div>
                {text}
            </div>
        </div>
    );
}

ToastItem.propTypes = {
    /** данные для отображения сообщения */
    data: PropTypes.object.isRequired,
};
