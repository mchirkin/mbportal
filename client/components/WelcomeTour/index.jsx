import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Joyride from 'react-joyride';

import 'react-joyride/lib/react-joyride-compiled.css';
import './joyride_overrides.css';

export default class WelcomeTour extends Component {
    static propTypes = {
        isTourOpen: PropTypes.bool,
        steps: PropTypes.array.isRequired,
        handleClose: PropTypes.func.isRequired,
    }

    static defaultProps = {
        isTourOpen: false,
    }

    callback = (e) => {
        console.log('Tour callback', e)
        if (e.action === 'close') {
            this.props.handleClose();
        }
    }

    render() {
        return (
            <Joyride
                autoStart
                steps={this.props.steps}
                run={this.props.isTourOpen}
                holePadding={0}
                scrollOffset={0}
                callback={this.callback}
                disableOverlay
                locale={{ close: 'Понятно', next: 'Далее', last: 'Понятно' }}
                type="continuous"
                tooltipOffset={this.props.tooltipOffset || 30}
            />
        );
    }
}
