import React from 'react';
import { Link } from 'react-router-dom';

/**
 * Страница 404
 */
export default function NotFound() {
    return (
        <div
            style={{
                marginTop: '3.125vh',
                marginLeft: '3.125vw',
            }}
        >
            <h1>404</h1>
            <div style={{ fontSize: 24 }}>
                Указанная страница не найдена
            </div>
            <Link
                to="/"
                style={{ fontSize: 16 }}
            >
                Перейти на главную
            </Link>
        </div>
    );
}
