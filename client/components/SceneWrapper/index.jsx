import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Scrollbars from 'react-custom-scrollbars';

import setBackUrl from 'modules/settings/actions/set_back_url';

import InfoMessage from '../InfoMessage';

import styles from './scene-wrapper.css';

class SceneWrapper extends Component {
    static contextTypes = {
        router: PropTypes.object.isRequired,
    };

    navigateToHomePage = () => {
        if (this.props.backUrl !== '/home') {
            this.context.router.history.push(this.props.backUrl);
            this.props.setBackUrl('/home');
        } else {
            this.context.router.history.push('/home');
        }
    }

    render() {
        const { children, showCloseButton } = this.props;

        return (
            <div className={styles.wrapper}>
                <InfoMessage />
                <Scrollbars
                    style={{
                        width: '100%',
                        maxWidth: '100%',
                        height: '100%',
                    }}
                >
                    <div className={styles['close-btn']} onClick={this.navigateToHomePage} />
                    {children}
                </Scrollbars>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        backUrl: state.settings.backUrl,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        setBackUrl: bindActionCreators(setBackUrl, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(SceneWrapper);
