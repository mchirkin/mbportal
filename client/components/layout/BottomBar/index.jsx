import React, { Component } from 'react';
import PropTypes from 'prop-types';
import User from './components/User';
import PaymentsInfo from './components/PaymentsInfo';
import MsgWithBank from './components/MsgWithBank';
import styles from './bottom-bar.css';

export default class BottomBar extends Component {
    static contextTypes = {
        router: PropTypes.object.isRequired,
    };

    static propTypes = {
        userInfo: PropTypes.object,
        newPayments: PropTypes.array,
        toSendPayments: PropTypes.array,
        errorPayments: PropTypes.array,
        notification: PropTypes.object,
        unreadMsg: PropTypes.object,
        setImportantModalVisibility: PropTypes.func,
        showImportantBtn: PropTypes.bool,
    };

    static defaultProps = {
        userInfo: {},
        newPayments: [],
        toSendPayments: [],
        errorPayments: [],
        notification: null,
        unreadMsg: null,
        showImportantBtn: false,
        setImportantModalVisibility: undefined,
    };

    navigateToSettings = () => {
        if (/settings/.test(document.location.href)) {
            this.context.router.history.goBack();
        } else {
            this.context.router.history.push('/home/settings/profile');
        }
    }

    navigateToNotification = (type) => {
        switch (type) {
            case 'sign':
                this.context.router.history.push('/home/sign_payments');
                break;
            case 'send':
                this.context.router.history.push('/home/send_payments');
                break;
            case 'error':
                this.context.router.history.push('/home/error_payments');
                break;
        }
    }

    showImportantModal = () => {
        this.props.setImportantModalVisibility(true);
    }

    render() {
        const {
            userInfo,
            newPayments,
            toSendPayments,
            errorPayments,
            notification,
            unreadMsg,
            showImportantBtn,
        } = this.props;

        return (
            <div className={styles['bottom-bar']}>
                <User data={userInfo} navigateToSettings={this.navigateToSettings} />
                <PaymentsInfo
                    newPayments={newPayments}
                    toSendPayments={toSendPayments}
                    errorPayments={errorPayments}
                    navigateToNotification={this.navigateToNotification}
                    notification={notification}
                    showImportantBtn={showImportantBtn}
                    showImportantModal={this.showImportantModal}
                />
                <MsgWithBank
                    unreadMsg={unreadMsg}
                />
            </div>
        );
    }
}
