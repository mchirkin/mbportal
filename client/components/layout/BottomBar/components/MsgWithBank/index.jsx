import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import styles from './msg-with-bank.css';

export default class MsgWithBank extends Component {
    render() {
        const {
            unreadMsg,
        } = this.props;

        return (
            <div className={styles.wrapper}>
                <Link to="/home/mail">
                    Связь с Банком
                </Link>
                {unreadMsg
                    ? (
                        <div className={styles['notification-sticker']}>
                            {unreadMsg.count > 0 ? unreadMsg.count : null}
                        </div>
                    )
                    : null
                }
            </div>
        );
    }
}
