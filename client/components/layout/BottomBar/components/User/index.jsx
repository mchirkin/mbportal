import React from 'react';
import PropTypes from 'prop-types';
import styles from './user.css';

/**
 * Информация о пользователе
 */
const User = ({ data, navigateToSettings }) => (
    <div className={styles.wrapper} onClick={navigateToSettings}>
        <div className={styles.avatar}>
            <div className={styles['avatar-border']} />
            <div className={styles['avatar-image']} />
        </div>
        <div className={styles['user-info']}>
            <div className={styles['user-name']}>
                {`${data.surname || ''} ${data.name || ''}`}
            </div>
            <div className={styles['user-job']}>
                {data.employeePosition || ''}
            </div>
        </div>
        <div className={styles.chevron} data-active={/settings/.test(document.location.href)} />
    </div>
);

User.propTypes = {
    /** данные о пользователе */
    data: PropTypes.object,
};

User.defaultProps = {
    data: {},
};

export default User;
