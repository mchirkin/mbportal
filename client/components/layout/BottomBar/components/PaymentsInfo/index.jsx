import React from 'react';
import PropTypes from 'prop-types';
import styles from './payments-info.css';

function getDocumentWord(number, forError = false) {
    let word = 'документов';

    if (number % 10 === 1) {
        word = forError ? 'документа' : 'документ';
    }

    if (number % 10 > 1 && number % 10 < 5) {
        word = forError ? 'документов' : 'документа';
    }

    if (number > 9 && number < 20) {
        word = 'документов';
    }

    return word;
}

const PaymentsInfo = ({
    newPayments,
    errorPayments,
    toSendPayments,
    navigateToNotification,
    notification,
    showImportantBtn,
    showImportantModal,
}) => (
    <div className={styles.wrapper}>
        {newPayments && newPayments.length > 0
            ? (
                <div className={styles.btn} data-status="sign" onClick={() => { navigateToNotification('sign'); }}>
                    <div />
                    <div className={styles['btn-text']}>
                        {newPayments.length} {getDocumentWord(newPayments.length)} ожидает подписи
                    </div>
                </div>
            )
            : null}
        {toSendPayments && toSendPayments.length > 0
            ? (
                <div className={styles.btn} data-status="send" onClick={() => { navigateToNotification('send'); }}>
                    <div />
                    <div className={styles['btn-text']}>
                        {toSendPayments.length} {getDocumentWord(toSendPayments.length)} ожидает отправки
                    </div>
                </div>
            )
            : null}
        {
            /* eslint-disable max-len */
            errorPayments && errorPayments.filter(p => p.viewed === 'false').length > 0
                ? (
                    <div className={styles.btn} data-status="error" onClick={() => { navigateToNotification('error'); }}>
                        <div />
                        <div className={styles['btn-text']}>
                            Ошибка отправки {errorPayments.filter(p => p.viewed === 'false').length} {getDocumentWord(errorPayments.filter(p => p.viewed === 'false').length, true)}
                        </div>
                    </div>
                )
                : null
            /* eslint-disable max-len */
        }
        {showImportantBtn && (
            <div
                className={styles['important-btn']}
                onClick={showImportantModal}
            >
                ВАЖНО!
            </div>
        )}
        {notification
            ? (
                <div className={styles.notification}>
                    <span>{notification.text}</span>
                </div>
            )
            : null}
    </div>
);

PaymentsInfo.propTypes = {
    /** список платежей в статусе "новый" */
    newPayments: PropTypes.array,
    /** список платежей в статусе "отказан" */
    errorPayments: PropTypes.array,
    /** список платежей в статусе "к отправке"(новые, но подписанные) */
    toSendPayments: PropTypes.array,
    /** функция для перехода на соответствующую страницу списка платежей */
    navigateToNotification: PropTypes.func,
    /** данные для отображения уведомления поверх всего нижнего меню */
    notification: PropTypes.object,
    /** показывать кнопку "Важно!" или нет */
    showImportantBtn: PropTypes.bool,
    /** функция для отображения модального окна с важными сообщениями */
    showImportantModal: PropTypes.func.isRequired,
};

PaymentsInfo.defaultProps = {
    newPayments: [],
    errorPayments: [],
    toSendPayments: [],
    navigateToNotification: undefined,
    notification: null,
    showImportantBtn: false,
};

export default PaymentsInfo;
