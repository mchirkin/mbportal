import React from 'react';
import { fetch } from 'utils';
import styles from './top-bar.css';

const handleLogoutClick = () => {
    fetch('/api/v1/logout', {
        method: 'POST',
        credentials: 'include',
    }).then(() => {
        localStorage.removeItem('csrfToken');
        localStorage.removeItem('login');
        window.location.href = '/login';
    }).catch((error) => {
        console.log('error', error);
    });
};

const Logout = () => (
    <div className={styles.wrapper} onClick={handleLogoutClick} />
);

export default Logout;
