import React from 'react';
import { NavLink as Link } from 'react-router-dom';
import styles from './top-bar.css';

const Menu = () => (
    <div className={styles.menu}>
        <Link to="/home" className={styles.link} activeClassName={styles['active-link']} exact>
            Деньги
        </Link>
        <Link to="/home/contacts" className={styles.link} activeClassName={styles['active-link']} exact>
            Контакты
        </Link>
        <Link to="/home/special" className={styles.link} activeClassName={styles['active-link']} exact>
            Спецпредложения
        </Link>
        <Link to="/home/services" className={styles.link} activeClassName={styles['active-link']}>
            Продукты и услуги
        </Link>
        <Link to="/home/invoices" className={styles.link} activeClassName={styles['active-link']} exact>
            Выставить счет <span style={{ position: 'relative', top: -8, fontSize: 12 }}>{'\u03b2'}</span>
        </Link>
    </div>
);

export default Menu;
