import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { NavLink as Link, withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment';

import { toggleCalendarWidget } from 'modules/calendar_widget/toggle_calendar_widget';

import Menu from './components/Menu';
import Logout from './components/Logout';
import styles from './top-bar.css';

function mapStateToProps(state) {
    return {
        calendarWidgetisVisible: state.calendarWidget.isVisible,
        isPaymentModalVisible: state.payments.paymentModal.isVisible,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        toggleCalendarWidget: bindActionCreators(toggleCalendarWidget, dispatch),
    };
}

@withRouter
@connect(mapStateToProps, mapDispatchToProps)
export default class TopBar extends Component {
    static propTypes = {
        toggleCalendarWidget: PropTypes.func,
        calendarWidgetisVisible: PropTypes.bool,
        history: PropTypes.object.isRequired,
        location: PropTypes.object.isRequired,
        isPaymentModalVisible: PropTypes.bool.isRequired,
    }

    static defaultProps = {
        toggleCalendarWidget: undefined,
        calendarWidgetisVisible: false,
    }

    static contextTypes = {
        router: PropTypes.object.isRequired,
    };

    componentWillMount() {
        if (this.props.toggleCalendarWidget !== undefined) {
            this.props.toggleCalendarWidget(false);
        }
    }

    handleCalendarClick = () => {
        const { history, location, isPaymentModalVisible } = this.props;

        if (location.pathname === '/home') {
            if (isPaymentModalVisible) {
                this.props.toggleCalendarWidget(!this.props.calendarWidgetisVisible);
            } else {
                history.push('/home/calendar');
            }
            return;
        }

        if (!/calendar/.test(location.pathname)) {
            this.props.toggleCalendarWidget(!this.props.calendarWidgetisVisible);
        }
    }

    render() {
        return (
            <div className={styles['top-bar']}>
                <Link
                    to="/home"
                    exact
                >
                    <div className={styles['reb-logo']} />
                </Link>
                <Menu />
                <div
                    className={styles['widget-link']}
                    onClick={this.handleCalendarClick}
                    style={{ cursor: /calendar/.test(window.location.pathname) ? 'default' : 'pointer' }}
                >
                    {moment().format('DD MMM YYYY')}
                </div>
                <Logout />
            </div>
        );
    }
}
