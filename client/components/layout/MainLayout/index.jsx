import React from 'react';
import SideMenu from 'containers/layout/SideMenu';
import ToggleMenuBtn from 'containers/ToggleMenuBtn';
import styles from './styles.css';

/**
 * Компонент для основной сетки приложения
 * @param {Object} props
 * @return {ReactElement}
 */
export default function MainLayout(props) {
    const date = new Date();
    let showNotification = true;
    if (date.getMonth() === 0 && date.getDate() === 9) {
        showNotification = false;
    }

    return (
        <div className={styles.wrapper}>
            <ToggleMenuBtn />
            <main className={styles['main-section']}>
                {localStorage.getItem('showNotification') === null && showNotification
                    ? (
                        <div className={styles.notification}>
                            <div
                                className={styles['close-notification']}
                                onClick={() => {
                                    localStorage.setItem('showNotification', false);
                                    document.querySelector(`.${styles.notification}`).style.display = 'none';
                                }}
                            >
                                &times;
                            </div>
                            Уважаемые клиенты!<br />
                            Обращаем ваше внимание на изменения в условиях приема и осуществления платежей в Интернет-банке для бизнеса в период официально отмечаемых в России и за рубежом новогодних праздников.
                            Платежные поручения будут приниматься до <strong>15:00 29.12.2017</strong>.
                            С <strong>09.01.2018 г.</strong> прием и исполнение платежей будет осуществляться в обычном режиме.
                        </div>
                    )
                    : null}
                {props.children}
            </main>
            <SideMenu location={props.location} />
        </div>
    );
}

/**
 * propTypes
 * @property {ReactElement} children
 * @property {Object} location - информация из React-Router о текущем местоположении пользователя в приложении
 */
MainLayout.propTypes = {
    children: React.PropTypes.node,
    location: React.PropTypes.object,
};

MainLayout.defaultProps = {
    children: null,
    location: {},
};
