import React from 'react';
import styles from './modal.css';

/**
 * Ряд кнопок со списком действий в модальном окне, расположенный внизу
 * @param {Object} props
 * @return {ReactElement}
 */
export default function ModalBtnRow(props) {
    return (
        <div className={styles['modal-btn-row']}>
            {props.children}
        </div>
    );
}

ModalBtnRow.propTypes = {
    children: React.PropTypes.node,
};

ModalBtnRow.defaultProps = {
    children: null,
};
