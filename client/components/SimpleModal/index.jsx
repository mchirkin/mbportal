import React from 'react';
import ModalBtnRow from 'components/ModalBtnRow';
import ModalBtn from 'components/ModalBtn';

export default function SimpleModal(props) {
    const bodyStyle = {
        marginBottom: 20,
        color: '#000',
        fontSize: 16,
        fontFamily: 'OpenSans-Regular',
        whiteSpace: 'pre-wrap',
    };

    const bodyHeaderStyle = {
        marginBottom: 5,
        fontSize: 18,
    };

    return (
        <div>
            <div style={bodyStyle}>
                {
                    props.bodyHeader && props.bodyHeader.length > 0
                        ? (
                            <div style={bodyHeaderStyle}>
                                {props.bodyHeader}
                            </div>
                        )
                        : null
                }
                {props.bodyText}
            </div>
            <ModalBtnRow>
                <ModalBtn
                    width={120}
                    btnTheme="gradient"
                    btnText={props.submitText}
                    handleClick={props.handleSubmit}
                />
                {props.showCancelBtn || typeof props.showCancelBtn === 'undefined'
                    ? (
                        <ModalBtn
                            width={120}
                            btnTheme="white"
                            btnText={props.cancelText}
                            handleClick={props.handleCancel}
                        />
                    )
                    : null
                }
            </ModalBtnRow>
        </div>
    );
}

SimpleModal.propTypes = {
    /** заголовок модального окна */
    bodyHeader: React.PropTypes.string,
    /** текст в модальном окне */
    bodyText: React.PropTypes.string,
    /** текст кнопки подтверждения */
    submitText: React.PropTypes.string,
    /** обработчик нажатия кнопки подтверждения */
    handleSubmit: React.PropTypes.func,
    /** показывать кнопку отмены или нет */
    showCancelBtn: React.PropTypes.bool,
    /** текст кнопки отмены */
    cancelText: React.PropTypes.string,
    /** обработчик нажатия кнопки отмены */
    handleCancel: React.PropTypes.func,
};

SimpleModal.defaultProps = {
    bodyHeader: '',
    bodyText: '',
    submitText: 'Хорошо',
    handleSubmit: undefined,
    showCancelBtn: undefined,
    cancelText: 'Отмена',
    handleCancel: undefined,
};
