import React from 'react';
import classNames from 'classnames/bind';
import styles from './styles.css';
import chevron from './chevron.png';

const cx = classNames.bind(styles);

export default class SlideUpAndDown extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            expanded: !this.props.initialClosed,
        };
        this.renderComplete = false;
    }

    componentDidMount() {
        if (this.props.slideUpOnload && !this.props.initialClosed) {
            setTimeout(() => {
                this.slideBody.style.maxHeight = this.getMinHeight();
                this.slideBody.style.overflow = 'hidden';
                this.setState({
                    expanded: false,
                });
            }, 300);
        }
        if (this.props.initialClosed) {
            // this.slideBody.style.maxHeight = `${this.props.minHeight}px`;
            this.slideBody.style.maxHeight = this.getMinHeight();
            this.slideBody.style.overflow = 'hidden';
        }

        window.addEventListener('resize', this.handleWindowResize);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleWindowResize);
    }

    handleWindowResize = () => {
        if (this.state.expanded) {
            this.slideBody.style.minHeight = this.getMinHeight();
            this.slideBody.style.maxHeight = this.getMaxHeight();
        } else {
            this.slideBody.style.minHeight = this.getMinHeight();
            this.slideBody.style.maxHeight = this.getMinHeight();
        }
    }

    toggleHeight = () => {
        this.renderComplete = true;
        if (parseInt(this.slideBody.style.maxHeight, 10) !== parseInt(this.getMinHeight(), 10)) {
            this.slideBody.style.maxHeight = this.getMinHeight();
            this.setState({
                expanded: false,
            });
            this.slideBody.style.overflow = 'hidden';
        } else {
            this.slideBody.style.maxHeight = this.getMaxHeight();
            this.setState({
                expanded: true,
            });
            setTimeout(() => {
                this.slideBody.style.overflow = 'visible';
            }, 500);
        }
    };

    getMinHeight = () => {
        if (!this.props.minHeight.responsive) {
            return `${this.props.minHeight}px`;
        }
        const widthList = Object.keys(this.props.minHeight.responsive).sort((a, b) => b - a);
        const width = widthList.find(w => window.matchMedia(`screen and (min-width: ${w}px)`).matches);
        return `${this.props.minHeight.responsive[width]}px`;
    }

    getMaxHeight = () => {
        if (!this.props.maxHeight.responsive) {
            return `${this.props.maxHeight}px`;
        }
        const widthList = Object.keys(this.props.maxHeight.responsive).sort((a, b) => b - a);
        const width = widthList.find(w => window.matchMedia(`screen and (min-width: ${w}px)`).matches);
        return `${this.props.maxHeight.responsive[width]}px`;
    }

    render() {
        const wrapperStyles = cx({
            'slide-wrapper': true,
            'slide-wrapper--no-overflow': this.state.expanded,
            'slide-wrapper--no-shadow': this.props.noShadow,
        });

        const bodyStyles = {
            minHeight: this.props.minHeight,
            maxHeight: this.props.maxHeight,
        };

        if (!this.renderComplete && this.props.initialClosed) {
            bodyStyles.maxHeight = this.props.minHeight;
        }

        const imgClasses = cx({
            chevron: true,
            'chevron--rotated': !this.state.expanded,
        });

        const toggleBtnClasses = cx({
            'toggle-btn': true,
            'toggle-btn-caption': this.props.caption,
        });

        return (
            <div className={wrapperStyles}>
                <div
                    className={styles['slide-body']}
                    style={bodyStyles}
                    ref={(slideBody) => { this.slideBody = slideBody; }}
                >
                    {this.props.children}
                </div>
                <div className={styles['slide-footer']} style={{ backgroundColor: this.props.toggleLineColor }}>
                    <div className={toggleBtnClasses} style={this.props.toggleBtnStyle} onClick={this.toggleHeight}>
                        {this.props.caption
                            ? (
                                <span>{this.state.expanded ? 'Скрыть' : this.props.caption}</span>
                            )
                            : null}
                        <img className={imgClasses} src={chevron} alt="" />
                    </div>
                </div>
            </div>
        );
    }
}

/**
 * propTypes
 * @property {boolean} slideUpOnload - закрыть при загрузке
 * @property {number} minHeight - высота в закрытом состоянии
 * @property {number} maxHeight - высота в раскрытом состоянии
 * @property {boolean} noShadow - показывать тень
 * @property {string} caption - называние кнопки раскрывающей компонент
 * @property {boolean} initialClosed - изначально свернут или нет
 * @property {string} toggleLineColor - цвет полоски за кнопкой разворачивания/сворачивания
 * @property {Object} toggleBtnStyle - стили кнопки разворачивания/сворачивания
 * @property {ReactElement} children
 */
SlideUpAndDown.propTypes = {
    slideUpOnload: React.PropTypes.bool,
    minHeight: React.PropTypes.oneOfType([
        React.PropTypes.number,
        React.PropTypes.object,
    ]),
    maxHeight: React.PropTypes.oneOfType([
        React.PropTypes.number,
        React.PropTypes.object,
    ]).isRequired,
    noShadow: React.PropTypes.bool,
    caption: React.PropTypes.string,
    initialClosed: React.PropTypes.bool,
    toggleLineColor: React.PropTypes.string,
    toggleBtnStyle: React.PropTypes.object,
    children: React.PropTypes.node,
};

SlideUpAndDown.defaultProps = {
    slideUpOnload: false,
    minHeight: 0,
    noShadow: false,
    caption: undefined,
    initialClosed: false,
    toggleLineColor: undefined,
    toggleBtnStyle: {},
    children: null,
};
