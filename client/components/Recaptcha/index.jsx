import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from './recaptcha.css';

export default class Recaptcha extends Component {
    static propTypes = {
        verifyCallback: PropTypes.func.isRequired,
        sitekey: PropTypes.string.isRequired,
    }

    shouldComponentUpdate() {
        return false;
    }

    componentWillMount() {
        window.recaptchaOnloadCallback = () => {
            grecaptcha.render('recaptcha', {
                sitekey: this.props.sitekey,
                callback: response => this.props.verifyCallback(response),
            });
        };

        const recaptchaScript = document.createElement('script');
        recaptchaScript.setAttribute('src', 'https://www.google.com/recaptcha/api.js?onload=recaptchaOnloadCallback&hl=ru&render=explicit');
        recaptchaScript.setAttribute('defer', '');
        recaptchaScript.setAttribute('async', '');
        document.head.appendChild(recaptchaScript);
    }

    render() {
        const isServerInLAN = /10\.1\.20/.test(document.location.origin) || /10\.1\.16/.test(document.location.origin);

        if (isServerInLAN) {
            return null;
        }

        return (
            <div className={styles.content}>
                <div id="recaptcha" />
            </div>
        );
    }
}
