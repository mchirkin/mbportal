import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Btn from 'components/ui/Btn';
import styles from './popup.css';

const PopupNotification = ({ data }) => {
    const {
        isVisible,
        headerText,
        message,
        submitText,
        handleClose,
        showSubmit,
        showClose,
        handleSubmit,
    } = data;
    return (
        <div className={styles.container} data-visible={isVisible}>
            <div
                className={styles.background}
                onClick={handleClose}
            />
            <div className={styles.wrap}>
                <div className={styles.content}>
                    {showClose
                        ? (
                            <div className={styles.close} onClick={handleClose}>×</div>
                        )
                        : null
                    }
                    <div className={styles.header}>
                        {headerText}
                    </div>
                    <div className={styles.message}>
                        {message}
                    </div>
                    {showSubmit
                        ? (
                            <Btn
                                style={{ display: 'inline-flex' }}
                                caption={submitText || 'ОК'}
                                bgColor="grey"
                                onClick={handleSubmit}
                            />
                        )
                        : null
                    }
                </div>
            </div>
        </div>
    );
};

PopupNotification.propTypes = {
    isVisible: PropTypes.bool,
    headerText: PropTypes.string,
    message: PropTypes.string,
    submitText: PropTypes.string,
    showSubmit: PropTypes.bool,
    showClose: PropTypes.bool,
    handleClose: PropTypes.func,
    handleSubmit: PropTypes.func,
};

PopupNotification.defaultProps = {
    isVisible: false,
    headerText: '',
    message: '',
    submitText: '',
    handleClose: undefined,
    handleSubmit: undefined,
    showSubmit: false,
    showClose: false,
};

function mapStateToProps(state) {
    return {
        data: state.popupNotification.data,
    };
}

export default connect(mapStateToProps)(PopupNotification);
