import React from 'react';
import styles from './styles.css';

export default function RebLogo() {
    return (
        <div className={styles['logo-wrapper']}>
            <div className={styles.logo} />
        </div>
    );
}
