import React from 'react';
import styles from './styles.css';

export default function Chat({ openNewMailModal }) {
    return (
        <div className={styles['chat-wrapper']}>
            <div className={styles['chat-link']} onClick={openNewMailModal}>
                <div className={styles['chat-icon']} />
            </div>
            <div className={styles['chat-text']}>
                Если есть вопросы,<br />
                напишите нам
            </div>
        </div>
    );
}
