import React from 'react';
import styles from './styles.css';

export default function Contacts() {
    return (
        <div className={styles['contacts-wrapper']}>
            <div className={styles.phone}>
                +7 495 777 11 11
            </div>
            <div className={styles.phone}>
                8 (800) 500-77-78
            </div>
            <div className={styles.phone}>
                <a href="mailto:k-b@roseurobank.ru">
                    k-b@roseurobank.ru
                </a>
            </div>
        </div>
    );
}
