import React from 'react';
import styles from './styles.css';

/**
 * Информация о сотруднике в боковом меню
 * @param {Object} props
 * @return {ReactElement}
 */
export default function EmployeeInfo(props) {
    return (
        <div className={styles['employee-info-wrapper']}>
            <div className={styles.name}>
                {props.name} {props.surname}
            </div>
            <div className={styles.position}>
                {props.employeePosition}
            </div>
        </div>
    );
}

/**
 * propTypes
 * @property {string} name - имя сотрудника
 * @property {string} surname - фамилия сотрудника
 * @property {string} employeePosition - должность сотрудника
 */
EmployeeInfo.propTypes = {
    name: React.PropTypes.string.isRequired,
    surname: React.PropTypes.string.isRequired,
    employeePosition: React.PropTypes.string,
};

EmployeeInfo.defaultProps = {
    employeePosition: '',
};
