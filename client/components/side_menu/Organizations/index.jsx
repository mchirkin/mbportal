import React from 'react';
import styles from './styles.css';

export default function Organizations() {
    return (
        <div className={styles['organizations-wrapper']}>
            <div className={styles['organization-logo']} />
        </div>
    );
}
