import React from 'react';
import { Link } from 'react-router';
import styles from './styles.css';

/**
 * Пункт "настройки" в боковом меню
 */
export default function Settings() {
    return (
        <Link to="/main/settings/profile" className={styles.link} activeClassName={styles['active-link']}>
            <div className={styles.settings}>
                Настройки
            </div>
        </Link>
    );
}
