import React from 'react';
import styles from './styles.css';

/**
 * Название организации в боковом меню
 * @param {Object} props
 * @return {ReactElement}
 */
export default function OrganizationName(props) {
    return (
        <div className={styles['orgname-wrapper']}>
            {props.orgName}
        </div>
    );
}

/**
 * propTypes
 * @property {string} orgName - название организации
 */
OrganizationName.propTypes = {
    orgName: React.PropTypes.string.isRequired,
};
