import React from 'react';
import { Link } from 'react-router';
import styles from './styles.css';

/**
 * Навигационное меню
 * @param {Object} props
 * @return {ReactElement}
 */
export default function Menu(props) {
    return (
        <ul className={styles.menu}>
            <Link to="/main" activeClassName={styles['active-link']} onlyActiveOnIndex>
                <li className={styles['menu-item']}>
                    Главная
                </li>
            </Link>
            <Link to="/main/accounts" activeClassName={styles['active-link']} onlyActiveOnIndex>
                <li className={styles['menu-item']}>
                    Счета
                </li>
            </Link>
            {props.cards && props.cards.length > 0
                ? (
                    <Link to="/main/cards" activeClassName={styles['active-link']} onlyActiveOnIndex>
                        <li className={styles['menu-item']}>
                            Карты
                        </li>
                    </Link>
                )
                : null}
            <Link to="/main/msg/mail" activeClassName={styles['active-link']} onlyActiveOnIndex>
                <li className={styles['menu-item']} style={{ justifyContent: 'space-between' }}>
                    Связь с банком
                    {props.unreadMsg && props.unreadMsg.count && props.unreadMsg.count !== '0'
                        ? (
                            <div className={styles['unread-count']}>
                                {props.unreadMsg.count}
                            </div>
                        )
                        : null}
                </li>
            </Link>
        </ul>
    );
}

Menu.contextTypes = {
    router: React.PropTypes.object.isRequired,
    history: React.PropTypes.object,
};

/**
 * propTypes
 * @property {Array} cards - массив с информацией о картах клиента
 */
Menu.propTypes = {
    cards: React.PropTypes.array,
};

Menu.defaultProps = {
    cards: [],
};
