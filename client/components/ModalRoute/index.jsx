import React from 'react';
import { Route } from 'react-router-dom';
import { TransitionGroup, CSSTransition } from 'react-transition-group';

const ModalRoute = (props) => {
    const Component = props.screen;

    return (
        <Route
            {...props}
        />
    );
}

export default ModalRoute;
