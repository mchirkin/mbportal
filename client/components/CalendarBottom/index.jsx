import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import styles from './calendar-bottom.css';

const CalendarBottom = ({ showPeriods, onCancel, onSubmit, onSetPeriod }) => (
    <div>
        {showPeriods && (
            <div className={styles.wrapper}>
                <div
                    className={styles['period-btn']}
                    onClick={() => {
                        onSetPeriod(moment(), moment());
                    }}
                >
                    День
                </div>
                <div
                    className={styles['period-btn']}
                    onClick={() => {
                        onSetPeriod(moment().subtract(7, 'days'), moment());
                    }}
                >
                    Неделя
                </div>
                <div
                    className={styles['period-btn']}
                    onClick={() => {
                        onSetPeriod(moment().startOf('month'), moment());
                    }}
                >
                    Месяц
                </div>
                <div
                    className={styles['period-btn']}
                    onClick={() => {
                        onSetPeriod(
                            moment().startOf('year'),
                            moment(),
                        );
                    }}
                >
                    Год
                </div>
            </div>
        )}
        <div className={styles.wrapper}>
            <div className={styles['cancel-btn']} onClick={onCancel}>
                Сбросить
            </div>
            <div className={styles['accept-btn']} onClick={onSubmit}>
                Применить
            </div>
        </div>
    </div>
);

CalendarBottom.propTypes = {
    showPeriods: PropTypes.bool,
    onCancel: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
    onSetPeriod: PropTypes.func,
};

CalendarBottom.defaultProps = {
    showPeriods: true,
    onSetPeriod: undefined,
};

export default CalendarBottom;
