import React, { Component } from 'react';
import _ from 'lodash';
import styles from './modal.css';

/**
 * Кнопка для списка действий, расположенных внизу модального окна
 * @param {Object} props
 * @return {ReactElement}
 */
export default class ModalBtn extends Component {
    shouldComponentUpdate(nextProps) {
        if (!_.isEqual(this.props, nextProps)) {
            return true;
        }
        return false;
    }

    render() {
        const props = this.props;

        const btnStyles = {
            width: props.width,
            color: props.textColor,
            backgroundColor: props.bgColor,
        };

        return (
            <div
                className={styles['modal-btn']}
                style={btnStyles}
                onClick={props.handleClick}
                data-theme={props.btnTheme}
            >
                {props.btnText}
            </div>
        );
    }
}

/**
 * propTypes
 * @property {number|string} width - ширина кнопки
 * @property {string} textColor - цвет текста кнопки
 * @property {string} bgColor - цвет фона кнопки
 * @property {Function} handleClick - обработчик нажатия на кнопку
 * @property {string} btnText - текст кнопки
 * @property {string} btnTheme - цветовая тема кнопки
 */
ModalBtn.propTypes = {
    width: React.PropTypes.oneOfType([
        React.PropTypes.number,
        React.PropTypes.string,
    ]),
    textColor: React.PropTypes.string,
    bgColor: React.PropTypes.string,
    handleClick: React.PropTypes.func,
    btnText: React.PropTypes.string,
    btnTheme: React.PropTypes.string,
};

ModalBtn.defaultProps = {
    width: 'auto',
    textColor: undefined,
    bgColor: undefined,
    handleClick: undefined,
    btnText: '',
    btnTheme: 'white',
};
