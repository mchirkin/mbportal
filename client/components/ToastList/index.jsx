import React from 'react';
import PropTypes from 'prop-types';
import ToastItem from 'components/ToastItem';
import styles from './toast.css';

export default function ToastList({ data }) {
    return (
        <div className={styles['toast-list-container']}>
            <div>
                {data.map(item => (
                    <ToastItem data={item} />
                ))}
            </div>
        </div>
    );
}

ToastList.propTypes = {
    data: PropTypes.array,
};

ToastList.defaultProps = {
    data: [],
};
