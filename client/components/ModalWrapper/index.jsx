import React from 'react';
import classNames from 'classnames/bind';
import MountToBody from '../MountToBody';
import styles from './modal.css';

const cx = classNames.bind(styles);

/**
 * Компонент-обертка для модального окна
 * @param {Object} props
 * @return {ReactElement}
 */
export default function ModalWrapper(props) {
    const wrapperClasses = cx({
        'modal-wrapper': true,
        'modal-wrapper--visible': props.isVisible,
    });

    const wrapperStyles = {
        zIndex: props.zIndex,
    };

    const containerStyles = {
        width: props.width ? props.width : 'auto',
        minWidth: props.minWidth,
        height: props.height ? props.height : 'auto',
    };

    return (
        <MountToBody id={props.id}>
            <div className={wrapperClasses} style={wrapperStyles}>
                <div
                    className={styles['modal-container']}
                    style={containerStyles}
                >
                    <div
                        className={styles['close-modal']}
                        onClick={props.closeModal}
                    >
                        &times;
                    </div>
                    {props.header && props.header.length > 0
                        ? (
                            <h3>
                                {props.header}
                            </h3>
                        )
                        : null}
                    {props.children}
                </div>
            </div>
        </MountToBody>
    );
}

/**
 * propTypes
 * @property {string} id - идентификатор модального окна
 * @property {boolean} isVisible - видимость модального окна
 * @property {number|string} width - ширина модального окна
 * @property {number|string} height - высота модального окна
 * @property {number} zIndex - значение z-index для модального окна
 * @property {Function} closeModal - обработчик закрытия модального окна
 * @property {string} header - заголовок модального окна
 */
ModalWrapper.propTypes = {
    id: React.PropTypes.string.isRequired,
    isVisible: React.PropTypes.bool.isRequired,
    width: React.PropTypes.oneOfType([
        React.PropTypes.number,
        React.PropTypes.string,
    ]),
    minWidth: React.PropTypes.oneOfType([
        React.PropTypes.number,
        React.PropTypes.string,
    ]),
    height: React.PropTypes.oneOfType([
        React.PropTypes.number,
        React.PropTypes.string,
    ]),
    zIndex: React.PropTypes.number,
    closeModal: React.PropTypes.func.isRequired,
    header: React.PropTypes.string,
    children: React.PropTypes.node,
};

ModalWrapper.defaultProps = {
    width: 'auto',
    minWidth: undefined,
    height: 'auto',
    header: '',
    zIndex: undefined,
    children: null,
};
