import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import formatNumber from 'utils/format_number';
import styles from './ui.css';

/**
 * Компонент для поля ввода суммы
 */
export default class InputAmount extends Component {
    static propTypes = {
        /** идентификатор поля ввода */
        id: PropTypes.string.isRequired,
        /** стили div'а обертки */
        style: PropTypes.object,
        /** стили input'a */
        inputStyle: PropTypes.object,
        /** название поля ввода */
        caption: PropTypes.string,
        /** заблокировано поле ввода или нет */
        disabled: PropTypes.bool,
        /** значение поля ввода */
        value: PropTypes.string.isRequired,
        /** первоначальное значение поля ввода */
        defaultValue: PropTypes.string,
        /** максимальная длина поля ввода */
        maxLength: PropTypes.number,
        /** обработчик изменения поля ввода */
        onChange: PropTypes.func.isRequired,
        /** стили для названия поля */
        labelStyle: PropTypes.object,
        /** ошибка ввода значения поля */
        invalid: PropTypes.bool,
    };

    static defaultProps = {
        style: {},
        inputStyle: {},
        caption: undefined,
        disabled: false,
        defaultValue: undefined,
        maxLength: undefined,
        labelStyle: undefined,
        invalid: false,
    };

    constructor(props) {
        super(props);
        /**
         * @property {string} prevValue предыдущее отформатированное значение суммы
         * @property {number} caretPosition текущее положение каретки
         */
        this.state = {
            prevValue: '',
            caretPosition: 0,
        };
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (!_.isEqual(this.props, nextProps) || !_.isEqual(this.state, nextState)) {
            return true;
        }
        return false;
    }

    componentDidUpdate() {
        const pos = this.state.caretPosition;
        const oldValue = this.state.prevValue;
        const newValue = this.props.value;

        if (this.state.prevValue === this.props.value) {
            this.input.setSelectionRange(this.state.caretPosition, this.state.caretPosition);
            return;
        }

        // количество пробелов до текущей позиции в предыдущем значении
        const oldSpaces = (oldValue.substr(0, pos - 1).match(/ /g) || []).length;
        // количество пробелов до текущей позиции в новом значении
        const newSpaces = (newValue.substr(0, pos - 1).match(/ /g) || []).length;
        // новая позиция каретки
        const newPos = pos + newSpaces - oldSpaces;
        if (newSpaces === oldSpaces) {
            // newPos = pos + newValue.length - oldValue.length;
        }
        this.input.setSelectionRange(newPos, newPos);

        this.onUpdate({
            prevValue: this.props.value,
            caretPosition: newPos,
        });
    }

    onUpdate = (newState) => {
        this.setState(newState);
    }

    /**
     * Обработка изменения значения поля ввода
     * @param {Event} e
     */
    handleChange = (e) => {
        const value = e.target.value;
        const input = e.target;

        // позиция каретки
        const pos = input.selectionStart;
        this.setState({
            caretPosition: pos,
        });

        const newValue = formatNumber(value.replace(/\./, ',').replace(/[^0-9,.]/g, ''), true, 2);

        this.props.onChange({
            target: {
                value: newValue,
            },
            id: this.props.id,
            value: newValue,
        });
    }

    /**
     * Обработчик потери фокуса из поля ввода
     */
    handleBlur = () => {
        // если значение поля ввода заканчивается на ",", то добавляем дробную часть "00"
        if (/,$/.test(this.props.value)) {
            this.props.onChange({
                target: {
                    value: formatNumber(this.props.value.replace(/,/, '')),
                },
            });
        }
    }

    render() {
        const style = Object.assign({}, this.props.style) || {};
        const inputStyle = Object.assign({}, this.props.inputStyle) || {};
        // Я не понял зачем это, а мне мешало
        // if (!this.props.caption) {
        //     style.height = 40;
        //     style.paddingTop = 8;
        // }
        return (
            <div
                className={styles['input-group']}
                style={style}
                data-disabled={this.props.disabled}
                data-invalid={!!this.props.invalid}
            >
                {this.props.caption
                    ? (
                        <label htmlFor={this.props.id} style={this.props.labelStyle}>
                            {this.props.caption}
                        </label>
                    )
                    : null}
                <input
                    style={inputStyle}
                    disabled={this.props.disabled}
                    value={this.props.value}
                    onChange={this.handleChange}
                    onBlur={this.handleBlur}
                    ref={(input) => { this.input = input; }}
                />
            </div>
        );
    }
}
