import React from 'react';
import PropTypes from 'prop-types';
import styles from './ui-btn.css';

const Btn = ({
    caption,
    bgColor,
    style,
    onClick,
    showLoader,
    loaderColor,
    disabled,
}) => (
    <div
        className={styles.wrapper}
        data-bg={bgColor}
        data-disabled={disabled}
        style={style}
        onClick={disabled ? undefined : onClick}
    >
        <div className={styles.caption}>
            {!showLoader
                ? caption
                : (
                    <div className={styles.spinner}>
                        <div className={styles.bounce1} style={{ background: loaderColor }} />
                        <div className={styles.bounce2} style={{ background: loaderColor }} />
                        <div className={styles.bounce3} style={{ background: loaderColor }} />
                    </div>
                )}
        </div>
    </div>
);

Btn.propTypes = {
    caption: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.node,
    ]),
    bgColor: PropTypes.string,
    style: PropTypes.object,
    onClick: PropTypes.func,
    showLoader: PropTypes.bool,
    loaderColor: PropTypes.string,
    disabled: PropTypes.bool,
};

Btn.defaultProps = {
    caption: undefined,
    bgColor: 'gradient',
    style: {},
    showLoader: false,
    loaderColor: '#fff',
    onClick: undefined,
    disabled: false,
};

export default Btn;
