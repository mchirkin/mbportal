import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import sinon from 'sinon';
import Button from './index';
import styles from './ui.css';

describe('ui:Button', () => {
    it('renders blue button by default', () => {
        const wrapper = shallow(<Button caption="test" />);
        expect(wrapper.hasClass(styles['btn-blue'])).to.equal(true);
    });

    it('renders white button with prop color="white"', () => {
        const wrapper = shallow(<Button caption="test" color="white" />);
        expect(wrapper.hasClass(styles['btn-white'])).to.equal(true);
    });

    it('has style if passed styles property', () => {
        const wrapper = shallow(<Button caption="test" styles={{ width: 250 }} />);
        expect(wrapper.props().style.width).to.equal(250);
    });

    it('call handleClick when click on Button', () => {
        const onBtnClick = sinon.spy();
        const wrapper = shallow(<Button caption="test" handleClick={onBtnClick} />);
        wrapper.simulate('click');
        expect(onBtnClick).to.have.property('callCount', 1);
    });
});
