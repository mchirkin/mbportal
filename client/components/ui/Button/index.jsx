import React from 'react';
import PropTypes from 'prop-types';
import styles from './ui.css';

const Button = (props) => {
    let className = styles.btn;
    switch (props.color) {
        case 'gradient':
        case 'blue':
            className += ` ${styles['btn-blue']}`;
            break;
        case 'white':
            className += ` ${styles['btn-white']}`;
            break;
        case 'more':
            className += ` ${styles['btn-more']}`;
            break;
        default:
            className += ` ${styles['btn-blue']}`;
            break;
    }
    return (
        <div
            className={className}
            onClick={props.handleClick}
            style={props.styles}
        >
            <span>
                {props.caption}
            </span>
        </div>
    );
}

Button.propTypes = {
    /** цвет кнопки */
    color: PropTypes.string,
    /** текст кнопки */
    caption: PropTypes.string.isRequired,
    /** обработчик нажатия на кнопку */
    handleClick: PropTypes.func,
    /** стили кнопки */
    styles: PropTypes.object,
};

Button.defaultProps = {
    color: 'blue',
    handleClick: () => null,
    styles: {},
};

export default Button;
