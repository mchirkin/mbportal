import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Scrollbars } from 'react-custom-scrollbars';

export default class ScrollableTextarea extends PureComponent {
    componentWillReceiveProps(nextProps) {
        const range = window.getSelection().getRangeAt(0);
        this.caretPosition = range.startOffset;
        console.log(range, nextProps, nextProps.value);
        this.container.innerText = nextProps.value;
    }

    componentDidUpdate(prevProps) {
        const range = window.getSelection().getRangeAt(0);
        console.log('[did]', range, this.container.childNodes);

        range.setStart(this.container.childNodes[0], this.caretPosition);
    }

    handleKeyPress = (e) => {
        console.log(e.target.innerText);
        this.props.onChange(e);
    }

    render() {
        return (
            <Scrollbars
                style={{
                    width: 300,
                    height: 200,
                }}
                ref={(scroll) => { this.scroll = scroll; }}
            >
                <div
                    contentEditable
                    style={{
                        width: '100%',
                        height: '100%',
                        whiteSpace: 'pre-line',
                        border: '1px solid',
                    }}
                    onInput={this.handleKeyPress}
                    suppressContentEditableWarning
                    ref={(el) => { this.container = el; }}
                />
            </Scrollbars>
        );
    }
}
