import React from 'react';
import PropTypes from 'prop-types';
import styles from './marquee.css';

/**
 * Бегущая строка
 */
export default class Marquee extends React.Component {
    static propTypes = {
        text: PropTypes.string.isRequired,
    };

    constructor(props) {
        super(props);
        this.textIndentWidth = null;
        /** время в секундах */
        this.animationTime = 3;
    }

    onMouseEnterHandler = () => {
        if (!this.textIndentWidth) {
            this.textIndentWidth = this.marqueeText.offsetWidth - this.marquee.offsetWidth + 3; // 3 - ellipsis width

            const rate = (this.marqueeText.offsetWidth - this.marquee.offsetWidth) / this.marquee.offsetWidth;

            if (rate > 1 && rate < 5) {
                this.animationTime = 10;
            } else if (rate >= 5) {
                this.animationTime = 15;
            } else {
                this.animationTime = rate * 5;
            }

            this.marqueeText.style.transitionDuration = `${this.animationTime}s`;

            this.marqueeText.style.display = 'block';
        }

        if (this.textIndentWidth > 0) {
            this.marqueeText.style.textIndent = `-${this.textIndentWidth}px`;
        }
    }

    onMouseLeaveHandler = () => {
        if (this.marqueeText.style.textIndent) {
            this.marqueeText.style.textIndent = 0;
        }
    }

    render() {
        return (
            <p
                className={styles['ui-marquee']}
                ref={(marquee) => { this.marquee = marquee; }}
            >
                <span
                    className={styles['ui-marquee__text']}
                    onMouseEnter={this.onMouseEnterHandler}
                    onMouseLeave={this.onMouseLeaveHandler}
                    ref={(marqueeText) => { this.marqueeText = marqueeText; }}
                    style={{ transitionDuration: `${this.animationTime}s` }}
                >
                    {this.props.text}
                </span>
            </p>
        );
    }
}
