import React from 'react';
import CheckBox from 'components/ui/CheckBox';
import styles from './mail.css';

/**
 * Компонент для выбора статуса в фильтре
 */
export default class FilterStatusItem extends React.Component {
    /**
     * Установка значения чекбокса
     * @param {boolean} value - значение чекбокса
     */
    setCheckboxValue = (value) => {
        if (value) {
            this.checkbox.setCheckboxValue();
        }
        if (!value) {
            if (this.checkbox.state.status) {
                this.checkbox.setCheckboxValue(false);
            } else {
                this.checkbox.setCheckboxValue(true);
            }
        }
    }

    render() {
        return (
            <div
                className={styles['filter-status']}
                onClick={() => this.setCheckboxValue()}
                style={this.props.styles}
            >
                <div>{this.props.statusCaption}</div>
                <CheckBox
                    id={this.props.id}
                    ref={(checkbox) => { this.checkbox = checkbox; }}
                    handleChange={this.props.handleChange}
                    defaultValue={this.props.defaultValue || false}
                    value={this.props.statusList.includes(this.props.id)}
                />
            </div>
        );
    }
}

/**
 * propTypes
 * @property {Object} styles - стили для div'а обертки
 * @property {string} statusCaption - название статуса
 * @property {Function} handleChange - callback изменения чекбокса статуса
 * @property {boolean} defaultValue - первоначальное значение чекбокса
 */
FilterStatusItem.propTypes = {
    styles: React.PropTypes.object,
    statusCaption: React.PropTypes.string.isRequired,
    id: React.PropTypes.oneOfType([
        React.PropTypes.string,
        React.PropTypes.number,
        React.PropTypes.object,
    ]).isRequired,
    handleChange: React.PropTypes.func.isRequired,
    defaultValue: React.PropTypes.bool,
    statusList: React.PropTypes.array.isRequired,
};

FilterStatusItem.defaultProps = {
    styles: {},
    defaultValue: undefined,
};
