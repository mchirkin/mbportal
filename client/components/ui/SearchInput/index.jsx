import React from 'react';
import PropTypes from 'prop-types';
import styles from './search-input.css';

const SearchInput = ({
    value,
    onChange,
    placeholder,
    style,
    iconStyle,
    inputStyle,
}) => (
    <div className={styles.wrapper} style={style}>
        <div className={styles['search-icon']} style={iconStyle} />
        <input
            className={styles['search-input']}
            value={value}
            onChange={onChange}
            placeholder={placeholder}
            style={inputStyle}
        />
    </div>
);

SearchInput.propTypes = {
    value: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    placeholder: PropTypes.string,
    style: PropTypes.object,
    iconStyle: PropTypes.object,
    inputStyle: PropTypes.object,
};

SearchInput.defaultProps = {
    value: '',
    placeholder: 'Поиск',
    style: {},
    iconStyle: {},
    inputStyle: {},
};

export default SearchInput;
