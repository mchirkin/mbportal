import React, { Component } from 'react';
import { render, unmountComponentAtNode } from 'react-dom';
import PropTypes from 'prop-types';
import Dropzone from 'react-dropzone';

import styles from './ui.css';

export default class InputFile extends Component {
    static propTypes = {
        handleFileChange: PropTypes.func.isRequired,
        deleteFile: PropTypes.func.isRequired,
        description: PropTypes.string,
        fileList: PropTypes.array,
        /** текст подсказки */
        hint: PropTypes.node,
        /** не показывать подсказку */
        hintDisabled: PropTypes.bool,
        /** содержимое подсказки */
        hintContainer: PropTypes.oneOfType([
            PropTypes.object,
            PropTypes.node,
            PropTypes.string,
        ]),
        invalid: PropTypes.oneOfType([
            PropTypes.bool,
            PropTypes.string,
        ]),
    };

    static defaultProps = {
        description: '',
        fileList: [],
        hint: '',
        hintDisabled: false,
        hintContainer: null,
        invalid: false,
    };

    renderHintContainer = (content, isInvalid = false) => {
        const inputOffsetTop = this.group.offsetTop;
        let hintPosition = inputOffsetTop + 11;

        if (/union/.test(this.group.parentNode.className) && this.group.parentNode.offsetTop > 0) {
            hintPosition += this.group.parentNode.offsetTop;
        }

        this.props.hintContainer.innerHTML = '';

        const hintContent = (
            <div
                style={{
                    position: 'absolute',
                    top: `${hintPosition}px`,
                    color: isInvalid && '#ff3b30',
                }}
            >
                {content}
            </div>
        );

        render(hintContent, this.props.hintContainer);
    }

    handleMouseEnter = () => {
        if (this.props.hintContainer && this.props.invalid && typeof this.props.invalid === 'string') {
            this.renderHintContainer(this.props.invalid, true);
            return;
        }

        if (this.props.hint && this.props.hintContainer && !this.props.hintDisabled) {
            this.renderHintContainer(this.props.hint);
        }
    }

    handleMouseLeave = () => {
        if (this.props.hintContainer && !this.props.hintDisabled) {
            unmountComponentAtNode(this.props.hintContainer);
            this.props.hintContainer.innerHTML = /undefined/.test(this.previousHintContainerHTML)
                ? ''
                : this.previousHintContainerHTML;
            this.props.hintContainer.style.paddingTop = this.previousHintContainerPadding;
        }
    }

    render() {
        const dropzoneStyle = {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            width: '100%',
            height: 90,
            border: '1px dashed #d8d8d8',
            borderRadius: 8,
            cursor: 'pointer',
        };

        if (!!this.props.invalid) {
            dropzoneStyle.border = '1px dashed #ff3b30';
        }

        return (
            <div
                className={styles['dropzone-group']}
                onMouseEnter={this.handleMouseEnter}
                onMouseLeave={this.handleMouseLeave}
                data-invalid={!!this.props.invalid}
                ref={(group) => { this.group = group; }}
            >
                <Dropzone onDrop={e => this.props.handleFileChange(e, 'fileList')} style={dropzoneStyle}>
                    <div className={styles['dropzone-text']}>
                        <div>
                            <span>Выберите файл</span>
                        </div>
                        <div>или перетащите в эту область</div>
                    </div>
                </Dropzone>
                {this.props.description !== ''
                    && (
                        <div className={styles['file-info']}>
                            {this.props.description}
                        </div>
                    )
                }
                {this.props.fileList.length > 0
                    && (
                        <div className={styles['file-list']}>
                            {this.props.fileList.map((file, i) => (
                                <div
                                    key={i}
                                    className={styles['file-item']}
                                >
                                    <div className={styles['file-icon']} />

                                    <div className={styles['file-wrap']}>
                                        <div className={styles['file-name']}>
                                            {file.name}
                                        </div>
                                        <div
                                            className={styles['file-delete']}
                                            onClick={() => { this.props.deleteFile(file); }}
                                        >
                                            &times;
                                        </div>
                                    </div>
                                </div>
                            ))}
                        </div>
                    )
                }
            </div>
        );
    }
}
