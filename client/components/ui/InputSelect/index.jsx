import React, { Component } from 'react';
import { render, unmountComponentAtNode } from 'react-dom';
import PropTypes from 'prop-types';
import { Scrollbars } from 'react-custom-scrollbars';
import classNames from 'classnames/bind';
import _ from 'lodash';
import InputElement from 'react-input-mask';

import { findTargetInChildNode } from 'utils/dom';
import colors from 'constants/colors';

import Hint from '../Hint';

import styles from './ui.css';

const cx = classNames.bind(styles);

/**
 * Выпадающий список
 */
export default class InputSelect extends Component {
    static propTypes = {
        id: PropTypes.string.isRequired,
        style: PropTypes.object,
        caption: PropTypes.string,
        type: PropTypes.string,
        value: PropTypes.object.isRequired,
        valueCaption: PropTypes.string,
        onChange: PropTypes.func.isRequired,
        options: PropTypes.array.isRequired,
        disabled: PropTypes.bool,
        disableWhenOneElement: PropTypes.bool,
        labelStyle: PropTypes.object,
        chevronStyle: PropTypes.object,
        optionsListStyle: PropTypes.object,
        /** возможно ли редактирование */
        editable: PropTypes.bool,
        /** маска на поле ввода */
        mask: PropTypes.string,
        /** символ для маски */
        maskChar: PropTypes.string,
        hint: PropTypes.node,
        invalid: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.bool,
        ]),
    };

    static defaultProps = {
        style: {},
        caption: undefined,
        type: 'text',
        valueCaption: '',
        disabled: false,
        disableWhenOneElement: false,
        labelStyle: undefined,
        chevronStyle: undefined,
        optionsListStyle: undefined,
        editable: false,
        mask: null,
        maskChar: null,
        hint: null,
        invalid: '',
    };

    constructor(props) {
        super(props);
        /**
         * @property {boolean} expanded - раскрыт ли выпадающий список
         */
        this.state = {
            expanded: false,
        };
    }

    componentDidMount() {
        // закрытие выпадающего списка при клике вне его
        document.querySelector('body').addEventListener('click', (e) => {
            if (!findTargetInChildNode(this.selectContainer, e.target)) {
                if (this.selectContainer) {
                    this.setState({
                        expanded: false,
                    });
                }
            }
        });
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (
            !_.isEqual(this.props, nextProps) ||
            this.state.expanded !== nextState.expanded
        ) {
            return true;
        }
        return false;
    }

    /**
     * Изменить видимость выпадающего списка
     */
    toggleSelect = () => {
        if (this.props.disabled) return;
        if (this.props.disableWhenOneElement && this.props.options.length < 2 && this.props.value) {
            return;
        }
        this.setState({
            expanded: !this.state.expanded,
        }, () => {
            /*
            if (this.state.expanded) {
                if (this.props.hint && this.props.hintContainer) {
                    const offsetTop = this.selectContainer.offsetTop;
                    this.props.hintContainer.style.paddingTop = `${offsetTop - 45}px`;
                    render(this.props.hint, this.props.hintContainer);
                }
            }
            */
        });
    }

    handleInputChange = (e) => {
        if (!this.props.editable) return;

        this.props.onChange({
            id: this.props.id,
            el: {
                value: e.target.value,
            },
            type: 'select',
        });
    }

    handleInputBlur = () => {
        setTimeout(() => {
            this.setState({
                expanded: false,
            });
        }, 100);
    }

    renderHintContainer = (content) => {
        const offsetTop = this.selectContainer.offsetTop; //e.target.parentNode.offsetTop;

        this.previousHintContainerHTML = this.props.hintContainer.innerHTML;
        this.previousHintContainerPadding = this.props.hintContainer.style.paddingTop;

        this.props.hintContainer.innerHTML = '';

        const hintContainerHeight = this.props.hintContainer.clientHeight;

        this.props.hintContainer.style.paddingTop = `${offsetTop - 45}px`;

        render(content, this.props.hintContainer);

        if (this.props.hintContainer.clientHeight > hintContainerHeight) {
            const heightDiff = this.props.hintContainer.clientHeight - hintContainerHeight;
            this.props.hintContainer.style.paddingTop = `${offsetTop - 45 - heightDiff}px`;
        }
    }

    handleMouseEnter = (e) => {
        if (this.props.hintContainer && this.props.invalid && typeof this.props.invalid === 'string') {
            this.renderHintContainer(
                <div style={{ color: colors.ERROR_RED }}>
                    {this.props.invalid}
                </div>
            );
            return;
        }

        if (this.props.hint && this.props.hintContainer && !this.props.hintDisabled) {
            this.renderHintContainer(this.props.hint);
        }
    }

    handleMouseLeave = (e) => {
        if (this.props.hint && this.props.hintContainer && !this.props.hintDisabled) {
            unmountComponentAtNode(this.props.hintContainer);
            this.props.hintContainer.innerHTML = this.previousHintContainerHTML;
            this.props.hintContainer.style.paddingTop = this.previousHintContainerPadding;
            //render(<div />, this.props.hintContainer);
        }
    }

    render() {
        const chevronHidden = (
            this.props.disableWhenOneElement &&
            this.props.options.length < 2 &&
            this.props.value
        );

        return (
            <div
                className={styles['input-select']}
                style={this.props.style}
                onClick={this.toggleSelect}
                onMouseEnter={this.handleMouseEnter}
                onMouseLeave={this.handleMouseLeave}
                ref={(selectContainer) => { this.selectContainer = selectContainer; }}
                data-disabled={this.props.disabled}
                data-expanded={this.state.expanded}
                data-invalid={!!this.props.invalid}
            >
                {this.props.caption || this.props.hint
                    ? (
                        <div className={styles['input-top']}>
                            {this.props.caption
                                ? (
                                    <label htmlFor={this.props.id} style={this.props.labelStyle}>
                                        {this.props.caption}
                                    </label>
                                )
                                : null}
                            {this.props.hint && false
                                ? (
                                    <Hint text={this.props.hint} style={{ top: -1 }} />
                                )
                                : null}
                        </div>
                    )
                    : null}
                {this.props.mask
                    ? (
                        <InputElement
                            id={this.props.id}
                            type={this.props.type}
                            onChange={this.handleInputChange}
                            onBlur={this.handleInputBlur}
                            ref={(input) => { this.input = input; }}
                            disabled={!this.props.editable || this.props.disabled}
                            value={this.props.value ? this.props.value.value : ''}
                            mask={this.props.mask}
                            maskChar={this.props.maskChar}
                        />
                    )
                    : (
                        <input
                            id={this.props.id}
                            type={this.props.type}
                            onChange={this.handleInputChange}
                            onBlur={this.handleInputBlur}
                            ref={(input) => { this.input = input; }}
                            disabled={!this.props.editable || this.props.disabled}
                            value={this.props.value ? this.props.value.value : ''}
                        />
                    )}
                <div
                    className={styles.chevron}
                    style={this.props.chevronStyle}
                    data-hidden={!!chevronHidden || this.props.disabled}
                >
                    <div className={styles['chevron-icon']} />
                </div>
                <div
                    className={styles.options}
                    data-visible={this.state.expanded}
                    style={this.props.optionsListStyle}
                >
                    <Scrollbars
                        autoHeight
                        autoHeightMax={150}
                        renderView={props => <div {...props} className={styles.scrollbar} />}
                        renderTrackHorizontal={() => <div style={{ display: 'none' }} />}
                        renderThumbHorizontal={() => <div style={{ display: 'none' }} />}
                        ref={(scroll) => { this.scroll = scroll; }}
                    >
                        {this.props.options
                            ? this.props.options.map((el) => {
                                const classes = cx({
                                    'option-item': true,
                                    'option-item-active': this.props.value && el.value === this.props.value.value,
                                });

                                return (
                                    <div
                                        key={el.option}
                                        className={classes}
                                        onClick={() => this.props.onChange({
                                            id: this.props.id,
                                            el,
                                            type: 'select',
                                        })}
                                    >
                                        {el.valueCaption || el.value}
                                    </div>
                                );
                            })
                            : null}
                    </Scrollbars>
                </div>
            </div>
        );
    }
}
