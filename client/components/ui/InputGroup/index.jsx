import React, { Component } from 'react';
import { render, unmountComponentAtNode } from 'react-dom';
import PropTypes from 'prop-types';
import InputElement from 'react-input-mask';
import { Scrollbars } from 'react-custom-scrollbars';
import classNames from 'classnames/bind';
import _ from 'lodash';
import Hint from '../Hint';
import styles from './ui.css';

const cx = classNames.bind(styles);

// Ключи для регулярных выражений для маскированного ввода
const formatChars = {
    9: '[0-9]',
    a: '[A-Za-z]',
    '*': '[A-Za-z0-9]',
    r: '[А-Яа-яA-Za-z0-9]',
};

/**
 * Компонент для поля ввода
 * Поддерживается автодополнение
 */
export default class InputGroup extends Component {
    static propTypes = {
        /** идентификатор поля ввода */
        id: PropTypes.string,
        /** стили div'а обертки */
        style: PropTypes.object,
        /** название поля ввода */
        caption: PropTypes.string,
        /** маска поля ввода */
        mask: PropTypes.string,
        /** символ для заполнения невведенных символов */
        maskChar: PropTypes.string,
        /** тип поля ввода */
        type: PropTypes.string,
        /** заблокировано поле ввода или нет */
        disabled: PropTypes.bool,
        /** значение поля ввода */
        value: PropTypes.string,
        /** первоначальное значение поля ввода */
        defaultValue: PropTypes.string,
        /** максимальная длина поля ввода */
        maxLength: PropTypes.number,
        /** обработчик изменения поля ввода */
        onChange: PropTypes.func.isRequired,
        /** список значений для автодополнения */
        autocomplete: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.array,
        ]),
        /** обработчик выбора из автодополнения */
        handleAutocompleteSelect: PropTypes.func,
        /** стили для названия поля */
        labelStyle: PropTypes.object,
        /** placeholder для поля ввода */
        placeholder: PropTypes.string,
        /** поле доступно только для чтения */
        readOnly: PropTypes.bool,
        /** показывать оставшееся количество символов */
        showSymbolCounter: PropTypes.bool,
        /** текст подсказки */
        hint: PropTypes.node,
        /** расположение подсказки */
        hintPosition: PropTypes.string,
        /** не показывать подсказку */
        hintDisabled: PropTypes.bool,
        /** содержимое подсказки */
        hintContainer: PropTypes.oneOfType([
            PropTypes.object,
            PropTypes.node,
            PropTypes.string,
        ]),
        noAutoSelectAfterBlur: PropTypes.bool,
        invalid: PropTypes.oneOfType([
            PropTypes.bool,
            PropTypes.string,
        ]),
        filterAutocompleteList: PropTypes.bool,
        autocompleteFilterFunction: PropTypes.func,
    };

    static defaultProps = {
        style: {},
        caption: undefined,
        mask: undefined,
        maskChar: '_',
        type: 'text',
        disabled: false,
        defaultValue: undefined,
        maxLength: undefined,
        autocomplete: undefined,
        handleAutocompleteSelect: undefined,
        labelStyle: undefined,
        placeholder: '',
        readOnly: false,
        showSymbolCounter: false,
        hint: '',
        hintPosition: 'bottom',
        hintDisabled: false,
        id: '',
        hintContainer: null,
        noAutoSelectAfterBlur: false,
        value: '',
        invalid: false,
        filterAutocompleteList: false,
        autocompleteFilterFunction: undefined,
    };

    constructor(props) {
        super(props);
        /**
         * @property {number} autocompleteIndex - индекс выбранного элемента из автодополнения или -1
         */
        this.state = {
            autocompleteIndex: -1,
            isHintVisible: false,
            autocompleteIsVisible: false,
            isFocused: false,
        };
    }

    componentWillReceiveProps(nextProps) {
        const newState = {};

        if (this.props.value !== nextProps.value) {
            newState.autocompleteIndex = -1;
        }

        if (
            this.state.isFocused &&
            nextProps.autocomplete &&
            nextProps.autocomplete.length > 0 &&
            nextProps.value !== this.selectedValue
        ) {
            newState.autocompleteIsVisible = true;
        }

        this.setState(newState);
    }

    componentDidMount() {
        document.addEventListener('click', (e) => {
            if (this.group && !this.group.contains(e.target)) {
                this.handleOutsideClick();
            }
        });
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (
            !_.isEqual(this.props, nextProps) ||
            !_.isEqual(this.state, nextState)
        ) {
            return true;
        }
        return false;
    }

    componentDidUpdate() {
        if (
            this.state.autocompleteIsVisible &&
            this.autocomplete &&
            this.props.autocomplete &&
            this.props.autocomplete.length > 0
        ) {
            this.scrollSelectedItemIntoView();
        }
    }

    /**
     * Установка значения поля ввода
     * @param {string} value - значение поля ввода
     */
    setValue = (value) => {
        this.input.value = value;
    }

    /**
     * Обработчик события нажатия клавиши клавиатуры
     * @param {Event} e
     */
    handleKeyDown = (e) => {
        // Если нет автодополнения, то никак не обрабатываем
        if (!this.props.autocomplete || this.props.autocomplete.length === 0) {
            return true;
        }

        // Обработка нажатия клавишы вверх
        if (e.which === 40) {
            this.setState({
                autocompleteIndex: this.state.autocompleteIndex + 1 >= this.props.autocomplete.length
                    ? this.props.autocomplete.length - 1
                    : this.state.autocompleteIndex + 1,
            });
        }

        // Обработка нажатия клавишы вниз
        if (e.which === 38) {
            this.setState({
                autocompleteIndex: this.state.autocompleteIndex - 1 > 0 ? this.state.autocompleteIndex - 1 : 0,
            });
        }

        // Обработка нажатия клавишы Enter
        if (e.which === 13) {
            // this.props.handleAutocompleteSelect(this.props.autocomplete[this.state.autocompleteIndex]);
            const selectedValue = this.getAutocompleteList()[this.state.autocompleteIndex];
            this.handleAutocompleteSelect(selectedValue);

            this.setState({
                autocompleteIndex: -1,
            });
        }

        // Обработка нажатия клавиши Tab
        if (e.which === 9) {
            this.setState({
                autocompleteIsVisible: false,
            });
        }
    }

    /**
     * Перемещение выбранного элемента в видимую область
     */
    scrollSelectedItemIntoView = () => {
        const item = this.autocomplete.querySelector(`.${styles['autocomplete-item-active']}`);
        if (!item) return;

        const autocompleteScrollTop = this.scroll.getScrollTop();
        const autocompleteClientHeight = this.scroll.getClientHeight();

        const isOutOfUpperView = item.offsetTop < autocompleteScrollTop;
        const isOutOfLowerView = item.offsetTop + item.clientHeight > autocompleteScrollTop + autocompleteClientHeight;

        if (isOutOfUpperView) {
            this.scroll.scrollTop(item.offsetTop);
        } else if (isOutOfLowerView) {
            this.scroll.scrollTop(item.offsetTop + item.clientHeight - autocompleteClientHeight);
        }
    }

    setFocusOnInput = () => {
        this.input.focus();
    }

    handleFocus = () => {
        const newState = {
            isFocused: true,
        };

        if (
            this.props.autocomplete &&
            this.props.autocomplete.length > 0
        ) {
            newState.autocompleteIsVisible = true;
        }

        this.setState(newState);

        /*
        if (this.props.hint && this.props.hintContainer) {
            const offsetTop = e.target.parentNode.offsetTop;
            this.props.hintContainer.style.paddingTop = `${offsetTop - 45}px`;
            render(this.props.hint, this.props.hintContainer);
        }
        */
    }

    renderHintContainer = (content, isInvalid = false) => {
        const inputOffsetTop = this.group.offsetTop;
        let hintPosition = inputOffsetTop + 11;

        if (/union/.test(this.group.parentNode.className) && this.group.parentNode.offsetTop > 0) {
            hintPosition += this.group.parentNode.offsetTop;
        }

        this.previousHintContainerHTML = this.props.hintContainer.innerHTML;
        this.previousHintContainerPadding = this.props.hintContainer.style.paddingTop;

        this.props.hintContainer.innerHTML = '';

        const hintContent = (
            <div
                style={{
                    position: 'absolute',
                    top: `${hintPosition}px`,
                    color: isInvalid && '#ff3b30',
                }}
            >
                {content}
            </div>
        );

        render(hintContent, this.props.hintContainer);
    }

    handleMouseEnter = () => {
        if (this.props.hintContainer && this.props.invalid && typeof this.props.invalid === 'string') {
            this.renderHintContainer(this.props.invalid, true);
            return;
        }

        if (this.props.hint && this.props.hintContainer && !this.props.hintDisabled) {
            this.renderHintContainer(this.props.hint);
        }
    }

    handleMouseLeave = () => {
        if (this.props.hintContainer && !this.props.hintDisabled) {
            unmountComponentAtNode(this.props.hintContainer);
            this.props.hintContainer.innerHTML = /undefined/.test(this.previousHintContainerHTML)
                ? ''
                : this.previousHintContainerHTML;
            this.props.hintContainer.style.paddingTop = this.previousHintContainerPadding;
        }
    }

    handleBlur = () => {
        /*
        setTimeout(() => {
            this.setState({
                autocompleteIsVisible: false,
                isFocused: false,
            });
        }, 0);
        */

        if (
            this.state.isFocused &&
            this.props.autocomplete &&
            this.props.autocomplete.length === 1 &&
            !this.props.noAutoSelectAfterBlur
        ) {
            this.setState({
                isFocused: false,
            });
            this.props.handleAutocompleteSelect(this.props.autocomplete[0]);
        } else {
            this.setState({
                isFocused: false,
            });
        }
    }

    handleOutsideClick = () => {
        this.setState({
            autocompleteIsVisible: false,
            // isFocused: false,
        });
    }

    handleAutocompleteSelect = (el) => {
        this.setState({
            autocompleteIsVisible: false,
            // isFocused: false,
        });
        this.selectedValue = el.name;
        this.props.handleAutocompleteSelect(el);
    }

    getAutocompleteList = () => {
        const autocompleteList = (
            this.props.autocomplete &&
            this.props.autocomplete.length > 0 &&
            this.props.autocomplete
                .filter((el) => {
                    if (!this.props.value || !this.props.filterAutocompleteList) {
                        return true;
                    }

                    if (this.props.autocompleteFilterFunction) {
                        return this.props.autocompleteFilterFunction(el.name, this.props.value);
                    }
                    return el.name.toLowerCase().includes(this.props.value.toLowerCase());
                })
        );

        return autocompleteList || [];
    }

    render() {
        // console.log(`[InputGroup, id ${this.props.id}]`, this.state, this.props);
        const style = Object.assign({}, this.props.style) || {};

        if (!this.props.caption) {
            style.height = 40;
            style.paddingTop = 8;
        }

        let symbolLeft = 0;

        if (this.props.maxLength && this.props.showSymbolCounter) {
            const currentValueLength = this.props.value && this.props.value.length
                ? this.props.value.length
                : 0;
            symbolLeft = this.props.maxLength - currentValueLength;
        }

        const autocompleteList = this.getAutocompleteList();

        return (
            <div
                className={styles['input-group']}
                style={style}
                onMouseEnter={this.handleMouseEnter}
                onMouseLeave={this.handleMouseLeave}
                data-disabled={this.props.disabled}
                data-show-symbol={this.props.showSymbolCounter}
                data-invalid={!!this.props.invalid}
                ref={(group) => { this.group = group; }}
            >
                {this.props.caption || this.props.hint
                    ? (
                        <div className={styles['input-top']} style={this.props.labelStyle}>
                            {this.props.caption
                                ? (
                                    <label htmlFor={this.props.id}>
                                        {this.props.caption}
                                    </label>
                                )
                                : null}
                            {this.props.hint && false
                                ? (
                                    <Hint
                                        text={this.props.hint}
                                        hintPosition={this.props.hintPosition}
                                        style={{ top: -1 }}
                                    />
                                )
                                : null}
                        </div>
                    )
                    : null}
                {this.props.mask
                    ? (
                        <InputElement
                            id={this.props.id}
                            type={this.props.type}
                            onChange={this.props.onChange}
                            onKeyDown={this.handleKeyDown}
                            onFocus={this.handleFocus}
                            onBlur={this.handleBlur}
                            ref={(input) => { this.input = input; }}
                            disabled={this.props.disabled}
                            value={this.props.value}
                            defaultValue={this.props.defaultValue}
                            maxLength={this.props.maxLength}
                            placeholder={this.props.placeholder}
                            readOnly={this.props.readOnly}
                            mask={this.props.mask}
                            maskChar={this.props.maskChar}
                            formatChars={formatChars}
                            autoComplete="off"
                        />
                    )
                    : (
                        <input
                            id={this.props.id}
                            type={this.props.type}
                            onChange={this.props.onChange}
                            onKeyDown={this.handleKeyDown}
                            onFocus={this.handleFocus}
                            onBlur={this.handleBlur}
                            ref={(input) => { this.input = input; }}
                            disabled={this.props.disabled}
                            value={this.props.value}
                            defaultValue={this.props.defaultValue}
                            maxLength={this.props.maxLength}
                            placeholder={this.props.placeholder}
                            readOnly={this.props.readOnly}
                            autoComplete="off"
                        />
                    )}
                {
                    autocompleteList &&
                    autocompleteList.length > 0 &&
                    this.state.autocompleteIsVisible &&
                    (
                        <div
                            className={styles.autocomplete}
                            data-visible="true"
                            ref={(autocomplete) => { this.autocomplete = autocomplete; }}
                        >
                            <Scrollbars
                                className={styles.scrollbar}
                                autoHeight
                                autoHeightMax={150}
                                ref={(scroll) => { this.scroll = scroll; }}
                            >
                                {autocompleteList.map((el, index) => {
                                    const classes = cx({
                                        'autocomplete-item': true,
                                        'autocomplete-item-active': index === this.state.autocompleteIndex,
                                    });

                                    return (
                                        <div
                                            className={classes}
                                            onClick={() => this.handleAutocompleteSelect(el)}
                                        >
                                            <span>
                                                {el.name}
                                            </span>
                                        </div>
                                    );
                                })}
                            </Scrollbars>
                        </div>
                    )}
                {
                    this.props.showSymbolCounter &&
                    this.props.maxLength &&
                    !this.props.disabled
                        ? (
                            <div className={styles['symbol-left']} data-show-warning={symbolLeft <= 0}>
                                {symbolLeft}
                            </div>
                        )
                        : null
                }
            </div>
        );
    }
}
