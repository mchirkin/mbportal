import React, { Component } from 'react';
import { render, unmountComponentAtNode } from 'react-dom';
import PropTypes from 'prop-types';

import styles from './ui.css';

export default class InputRadio extends Component {
    static propTypes = {
        handleOptionChange: PropTypes.func.isRequired,
        radioList: PropTypes.array.isRequired,
        selectOption: PropTypes.string,
        radioGroupName: PropTypes.string,
        /** текст подсказки */
        hint: PropTypes.node,
        /** не показывать подсказку */
        hintDisabled: PropTypes.bool,
        /** содержимое подсказки */
        hintContainer: PropTypes.oneOfType([
            PropTypes.object,
            PropTypes.node,
            PropTypes.string,
        ]),
        invalid: PropTypes.oneOfType([
            PropTypes.bool,
            PropTypes.string,
        ]),
    };

    static defaultProps = {
        selectOption: '',
        radioGroupName: 'radio-group',
        hint: '',
        hintDisabled: false,
        hintContainer: null,
        invalid: false,
    };

    renderHintContainer = (content, isInvalid = false) => {
        const inputOffsetTop = this.group.offsetTop;
        let hintPosition = inputOffsetTop + 11;

        if (/union/.test(this.group.parentNode.className) && this.group.parentNode.offsetTop > 0) {
            hintPosition += this.group.parentNode.offsetTop;
        }

        this.props.hintContainer.innerHTML = '';

        const hintContent = (
            <div
                style={{
                    position: 'absolute',
                    top: `${hintPosition}px`,
                    color: isInvalid && '#ff3b30',
                }}
            >
                {content}
            </div>
        );

        render(hintContent, this.props.hintContainer);
    }

    handleMouseEnter = () => {
        if (this.props.hintContainer && this.props.invalid && typeof this.props.invalid === 'string') {
            this.renderHintContainer(this.props.invalid, true);
            return;
        }

        if (this.props.hint && this.props.hintContainer && !this.props.hintDisabled) {
            this.renderHintContainer(this.props.hint);
        }
    }

    handleMouseLeave = () => {
        if (this.props.hintContainer && !this.props.hintDisabled) {
            unmountComponentAtNode(this.props.hintContainer);
            this.props.hintContainer.innerHTML = /undefined/.test(this.previousHintContainerHTML)
                ? ''
                : this.previousHintContainerHTML;
            this.props.hintContainer.style.paddingTop = this.previousHintContainerPadding;
        }
    }

    render() {
        return (
            <div
                className={styles.container}
                onMouseEnter={this.handleMouseEnter}
                onMouseLeave={this.handleMouseLeave}
                data-invalid={!!this.props.invalid}
                ref={(group) => { this.group = group; }}
            >
                {this.props.radioList.map(item => (
                    <div className={styles.radio} key={item.id}>
                        <input
                            className={styles['radio-source']}
                            type="radio"
                            id={item.id}
                            name={this.props.radioGroupName}
                            checked={this.props.selectOption === item.id}
                            onChange={this.props.handleOptionChange}
                        />
                        <label
                            className={styles['radio-icon']}
                            htmlFor={item.id}
                            onClick={this.props.handleOptionChange}
                            data-name={this.props.radioGroupName}
                        />
                        <label
                            className={styles['radio-label']}
                            htmlFor={item.id}
                            onClick={this.props.handleOptionChange}
                            data-name={this.props.radioGroupName}
                        >
                            {item.label}
                        </label>
                    </div>
                ))}
            </div>
        );
    }
}
