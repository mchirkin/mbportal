import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './ui.css';

export default class Carousel extends Component {
    static propTypes = {
        /** Слайды для карусели */
        items: PropTypes.array.isRequired,
        /** Раз в сколько секунд меняется слайд */
        intervalTime: PropTypes.number,
        /** Время на смену слайда */
        transitionTime: PropTypes.number,
        /** Ширина div'a обертки */
        width: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.number,
        ]),
        /** Высота div'a обертки */
        height: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.number,
        ]),
    };

    static defaultProps = {
        intervalTime: 2,
        transitionTime: 1,
        width: '100%',
        height: '100%',
    };

    constructor(props) {
        super(props);

        this.state = {
            currentItem: 0,
            transitionTime: null,
        };
    }

    componentDidMount() {
        this.startCarousel();
    }

    componentWillUnmount() {
        this.stopCarousel();
    }

    componentDidUpdate() {
        if (this.state.currentItem === this.props.items.length) {
            /**
             * При переходе с последнего слайда на склонированный первый через 100 миллисекунд
             * сменить слайд на первый без анимации
             */
            setTimeout(() => {
                this.setState({
                    currentItem: 0,
                    transitionTime: 0,
                });
            }, this.props.transitionTime * 1000 + 100);
        }
        // Возвращаем анимацию переходов между слайдами
        if (this.state.transitionTime !== null) {
            setTimeout(() => {
                this.setState({
                    transitionTime: null,
                });
            }, 100);
        }
    }

    /**
     * Старт переключения между слайдами
     */
    startCarousel = () => {
        const { intervalTime, transitionTime } = this.props;
        const intervalTimeValue = (intervalTime + transitionTime) * 1000;

        this.interval = setInterval(() => {
            const newItemNumber = this.state.currentItem + 1;

            this.setState({
                currentItem: newItemNumber,
            });
        }, intervalTimeValue);
    }

    /**
     * Остановка переключения между слайдами
     */
    stopCarousel = () => {
        clearInterval(this.interval);
    }

    /**
     * Смена слайда по клику на переключатель "точку"
     * @param {Event} e
     */
    changeSlide = (e) => {
        const slideNumber = e.target.getAttribute('data-number');
        this.stopCarousel();
        this.setState({
            currentItem: parseInt(slideNumber, 10),
        }, () => {
            this.startCarousel();
        });
    }

    render() {
        const {
            items,
            transitionTime,
            width,
            height,
        } = this.props;

        const transitionTimeValue = this.state.transitionTime !== null ? this.state.transitionTime : transitionTime;

        const itemsWrapperStyles = {
            transform: `translateX(${-100 * this.state.currentItem}%)`,
            transition: `transform ${transitionTimeValue}s ease-in`,
        };

        const wrapperStyles = {
            width,
            height,
        };

        const itemsList = [...items, items[0]];

        return (
            <div
                className={styles.wrapper}
                style={wrapperStyles}
            >
                <div
                    className={styles['items-wrapper']}
                    style={itemsWrapperStyles}
                >
                    {
                        itemsList.map((item, index) => (
                            <div className={styles.item} key={index}>
                                {item}
                            </div>
                        ))
                    }
                </div>
                <div className={styles.dots}>
                    {
                        items.map((item, index) => {
                            let isActive = index === this.state.currentItem;
                            if (index === 0 && this.state.currentItem === items.length) {
                                isActive = true;
                            }

                            return (
                                <div
                                    className={styles.dot}
                                    key={index}
                                    onClick={this.changeSlide}
                                    data-number={index}
                                    data-active={isActive}
                                />
                            );
                        })
                    }
                </div>
            </div>
        );
    }
}
