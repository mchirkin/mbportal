import React, { PureComponent } from 'react';
import { render, unmountComponentAtNode } from 'react-dom';
import PropTypes from 'prop-types';
import TextareaAutosize from 'react-textarea-autosize';
import Hint from 'components/ui/Hint';
import styles from './ui.css';

/**
 * Компонент обертка для textarea
 */
export default class InputTextarea extends PureComponent {
    static propTypes = {
        /** идентификатор поля ввода */
        id: PropTypes.string,
        /** стили для div'a обертки */
        style: PropTypes.object,
        /** стили для textarea */
        textareaStyle: PropTypes.object,
        /** стили для названия поля */
        labelStyle: PropTypes.object,
        /** название текстового поля */
        caption: PropTypes.string,
        /** обработчик изменения значения поля ввода */
        onChange: PropTypes.func,
        /** заблокировано поле или нет */
        disabled: PropTypes.bool,
        /** значение поля ввода */
        value: PropTypes.string,
        /** значение по умолчанию */
        defaultValue: PropTypes.string,
        /** высота поля ввода */
        height: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.number,
        ]),
        /** максимальная длина поля ввода */
        maxLength: PropTypes.number,
        /** показывать оставшееся количество символов */
        showSymbolCounter: PropTypes.bool,
        hint: PropTypes.node,
        autosize: PropTypes.bool,
        /** содержимое подсказки */
        hintContainer: PropTypes.oneOfType([
            PropTypes.object,
            PropTypes.node,
            PropTypes.string,
        ]),
        /** не показывать подсказку */
        hintDisabled: PropTypes.bool,
        /** признак ошибки ввода */
        invalid: PropTypes.string,
    };

    static defaultProps = {
        style: {},
        textareaStyle: {},
        labelStyle: {},
        caption: undefined,
        disabled: false,
        defaultValue: undefined,
        height: 100,
        maxLength: undefined,
        showSymbolCounter: false,
        autosize: false,
        hint: undefined,
        id: '',
        hintContainer: null,
        hintDisabled: false,
        onChange: undefined,
        value: '',
        invalid: null,
    };

    handleFocus = () => {
        /*
        if (this.props.hint && this.props.hintContainer) {
            const offsetTop = e.target.parentNode.offsetTop;
            this.props.hintContainer.style.paddingTop = `${offsetTop - 45}px`;
            render(this.props.hint, this.props.hintContainer);
        }
        */
    }

    renderHintContainer = (content, isInvalid = false) => {
        const inputOffsetTop = this.group.offsetTop;
        let hintPosition = inputOffsetTop + 11;

        if (/union/.test(this.group.parentNode.className) && this.group.parentNode.offsetTop > 0) {
            hintPosition += this.group.parentNode.offsetTop;
        }

        this.previousHintContainerHTML = this.props.hintContainer.innerHTML;
        this.previousHintContainerPadding = this.props.hintContainer.style.paddingTop;

        this.props.hintContainer.innerHTML = '';

        const hintContent = (
            <div
                style={{
                    position: 'absolute',
                    top: `${hintPosition}px`,
                    color: isInvalid && '#ff3b30',
                }}
            >
                {content}
            </div>
        );

        render(hintContent, this.props.hintContainer);
    }

    handleMouseEnter = () => {
        if (this.props.hintContainer && this.props.invalid && typeof this.props.invalid === 'string') {

            this.renderHintContainer(this.props.invalid, true);
            return;
        }

        if (this.props.hint && this.props.hintContainer && !this.props.hintDisabled) {
            this.renderHintContainer(this.props.hint);
        }
    }

    handleMouseLeave = () => {
        if (this.props.hintContainer && !this.props.hintDisabled) {
            unmountComponentAtNode(this.props.hintContainer);
            this.props.hintContainer.innerHTML = /undefined/.test(this.previousHintContainerHTML)
                ? ''
                : this.previousHintContainerHTML;
            this.props.hintContainer.style.paddingTop = this.previousHintContainerPadding;
        }
    }

    render() {
        let symbolLeft = 0;

        if (this.props.maxLength && this.props.showSymbolCounter) {
            const currentValueLength = this.props.value && this.props.value.length
                ? this.props.value.length
                : 0;
            symbolLeft = this.props.maxLength - currentValueLength;
        }

        return (
            <div
                ref={(group) => { this.group = group; }}
                className={styles['input-textarea']}
                style={this.props.style}
                onMouseEnter={this.handleMouseEnter}
                onMouseLeave={this.handleMouseLeave}
                data-disabled={this.props.disabled}
                data-invalid={!!this.props.invalid}
                data-show-symbol={this.props.showSymbolCounter}
            >
                {this.props.caption || this.props.hint
                    ? (
                        <div className={styles['input-top']}>
                            {this.props.caption
                                ? (
                                    <label htmlFor={this.props.id} style={this.props.labelStyle}>
                                        {this.props.caption}
                                    </label>
                                )
                                : null}
                            {false && this.props.hint
                                ? (
                                    <Hint text={this.props.hint} style={{ top: -1 }} />
                                )
                                : null}
                        </div>
                    )
                    : null}
                {this.props.autosize
                    ? (
                        <TextareaAutosize
                            id={this.props.id}
                            onChange={this.props.onChange}
                            onFocus={this.handleFocus}
                            ref={(textarea) => { this.textarea = textarea; }}
                            disabled={this.props.disabled}
                            defaultValue={this.props.defaultValue}
                            value={this.props.value}
                            style={Object.assign({ minHeight: this.props.height }, this.props.textareaStyle)}
                            maxLength={this.props.maxLength}
                        />
                    )
                    : (
                        <textarea
                            id={this.props.id}
                            onChange={this.props.onChange}
                            onFocus={this.handleFocus}
                            ref={(textarea) => { this.textarea = textarea; }}
                            disabled={this.props.disabled}
                            defaultValue={this.props.defaultValue}
                            value={this.props.value}
                            style={Object.assign({ height: this.props.height }, this.props.textareaStyle)}
                            maxLength={this.props.maxLength}
                        />
                    )}
                {
                    this.props.showSymbolCounter &&
                    this.props.maxLength &&
                    !this.props.disabled
                        ? (
                            <div className={styles['symbol-left']} data-show-warning={symbolLeft <= 0}>
                                {symbolLeft}
                            </div>
                        )
                        : null
                }
            </div>
        );
    }
}
