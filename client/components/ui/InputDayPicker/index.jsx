import React from 'react';
import { render, unmountComponentAtNode } from 'react-dom';
import PropTypes from 'prop-types';
import { DayPickerSingleDateController } from 'react-dates';
import moment from 'moment';

import styles from './ui.css';

moment.locale('ru');

const now = moment();

/**
 * Daypicker single date controller
 */
export default class InputDayPicker extends React.Component {
    static propTypes = {
        id: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
        value: PropTypes.string,
        getDateValue: PropTypes.func.isRequired,
        /** текст подсказки */
        hint: PropTypes.node,
        /** расположение подсказки */
        hintPosition: PropTypes.string,
        /** не показывать подсказку */
        hintDisabled: PropTypes.bool,
        /** содержимое подсказки */
        hintContainer: PropTypes.oneOfType([
            PropTypes.object,
            PropTypes.node,
            PropTypes.string,
        ]),
        invalid: PropTypes.oneOfType([
            PropTypes.bool,
            PropTypes.string,
        ]),
    };

    static defaultProps = {
        value: '',
        hint: '',
        hintPosition: 'bottom',
        hintDisabled: false,
        hintContainer: null,
        invalid: false,
    }

    constructor(props) {
        super(props);

        this.state = {
            date: this.props.value,
            showCalendar: false,
        };
    }

    setDate = (date) => {
        this.setState({
            date,
        });
    }

    renderHintContainer = (content, isInvalid = false) => {
        const inputOffsetTop = this.group.offsetTop;
        let hintPosition = inputOffsetTop + 11;

        if (/union/.test(this.group.parentNode.className) && this.group.parentNode.offsetTop > 0) {
            hintPosition += this.group.parentNode.offsetTop;
        }

        this.props.hintContainer.innerHTML = '';

        const hintContent = (
            <div
                style={{
                    position: 'absolute',
                    top: `${hintPosition}px`,
                    color: isInvalid && '#ff3b30',
                }}
            >
                {content}
            </div>
        );

        render(hintContent, this.props.hintContainer);
    }

    handleMouseEnter = () => {
        if (this.props.hintContainer && this.props.invalid && typeof this.props.invalid === 'string') {
            this.renderHintContainer(this.props.invalid, true);
            return;
        }

        if (this.props.hint && this.props.hintContainer && !this.props.hintDisabled) {
            this.renderHintContainer(this.props.hint);
        }
    }

    handleMouseLeave = () => {
        if (this.props.hintContainer && !this.props.hintDisabled) {
            unmountComponentAtNode(this.props.hintContainer);
            this.props.hintContainer.innerHTML = /undefined/.test(this.previousHintContainerHTML)
                ? ''
                : this.previousHintContainerHTML;
            this.props.hintContainer.style.paddingTop = this.previousHintContainerPadding;
        }
    }

    render() {
        return (
            <div className={styles.container}>
                <div
                    className={styles.input}
                    onMouseEnter={this.handleMouseEnter}
                    onMouseLeave={this.handleMouseLeave}
                    data-invalid={!!this.props.invalid}
                    ref={(group) => { this.group = group; }}
                    onClick={() => {
                        if (this.state.showCalendar) return;

                        this.setState({
                            showCalendar: true,
                            date: null,
                        }, () => {
                            this.props.getDateValue(this.state.date, this.props.id);
                        });
                    }}
                >
                    <p className={styles['input-title']}>
                        {this.props.title}
                    </p>
                    <input
                        className={styles['input-source']}
                        value={this.state.date === null ? this.props.value || '' : this.state.date}
                        id={this.props.id}
                        readOnly
                    />
                </div>
                <div className={styles.calendar}>
                    {this.state.showCalendar
                        ? (
                            <div className={styles.daypicker}>
                                <div className={styles['daypicker-source']}>
                                    <DayPickerSingleDateController
                                        date={this.state.date}
                                        onDateChange={(date) => {
                                            this.setState({
                                                date: moment(date).format('DD.MM.YYYY'),
                                                showCalendar: false,
                                            }, () => {
                                                this.props.getDateValue(this.state.date, this.props.id);
                                            });
                                        }}
                                        firstDayOfWeek={1}
                                        isOutsideRange={() => false}
                                        numberOfMonths={1}
                                        daySize={35}
                                        onOutsideClick={() => this.setState({ showCalendar: false })}
                                        isDayHighlighted={day => day.isSame(now, 'd')}
                                        hideKeyboardShortcutsPanel
                                    />
                                </div>
                            </div>
                        )
                        : null
                    }
                </div>
            </div>
        );
    }
}
