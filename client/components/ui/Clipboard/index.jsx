import React, { Component } from 'react';
import ClipboardJS from 'clipboard';

export default class Clipboard extends Component {
    componentDidMount() {
        new ClipboardJS(this.wrapper);
    }

    render() {
        const { children, clipboardText, style } = this.props;

        return (
            <div
                style={style}
                data-clipboard-text={clipboardText}
                ref={(c) => { this.wrapper = c; }}
            >
                {children}
            </div>
        );
    }
}
