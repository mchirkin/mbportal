import React from 'react';
import styles from './ui.css';

const Hint = ({ text, withAnimation, hintPosition, style }) => (
    <div className={styles['hint-wrapper']} style={style}>
        <div className={styles['hint-icon']} />
        <div
            className={styles['hint-content']}
            data-with-animation={withAnimation || 'false'}
            data-hint-position={hintPosition}
        >
            {text}
        </div>
    </div>
);

export default Hint;
