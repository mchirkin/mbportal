import React, { Component } from 'react';
import { render, unmountComponentAtNode } from 'react-dom';
import PropTypes from 'prop-types';
import Highlighter from 'react-highlight-words';
import Hint from 'components/ui/Hint';

import styles from './ui.css';

export default class InputDadata extends Component {
    static propTypes = {
        /** дадатовский токен */
        token: PropTypes.string,
        /** плейсхолдер поля ввода */
        placeholder: PropTypes.string,
        /** значение поля ввода */
        value: PropTypes.string,
        /** запрос на получение подсказок будет инициирован в фоне сразу, после монтирования компонента или нет */
        autoload: PropTypes.bool,
        /** обработчик изменения поля ввода */
        onChange: PropTypes.func,
        /** обработчик изменения поля ввода и сохранение дадатовских данных */
        saveDadata: PropTypes.func,
        /** идентификатор поля ввода */
        id: PropTypes.string,
        /** тип поля ввода */
        type: PropTypes.string,
        /** заблокировано поле ввода или нет */
        disabled: PropTypes.bool,
        /** максимальная длина поля ввода */
        maxLength: PropTypes.number,
        /** поле доступно только для чтения */
        readOnly: PropTypes.bool,
        /** название поля ввода */
        caption: PropTypes.string,
        /** стили для названия поля */
        labelStyle: PropTypes.object,
        /** стили div'а обертки */
        style: PropTypes.object,
        /** выбор дадатовского сервиса address, bank, fio, email, party */
        service: PropTypes.string,
        /** количество выводимых дадатовских предложений */
        count: PropTypes.number,
        /** уточнение для дадатовского сервиса */
        parts: PropTypes.string,
        /** текст подсказки */
        hint: PropTypes.node,
        /** расположение подсказки */
        hintPosition: PropTypes.string,
        /** не показывать подсказку */
        hintDisabled: PropTypes.bool,
        /** содержимое подсказки */
        hintContainer: PropTypes.oneOfType([
            PropTypes.object,
            PropTypes.node,
            PropTypes.string,
        ]),
        invalid: PropTypes.oneOfType([
            PropTypes.bool,
            PropTypes.string,
        ]),
    }

    static defaultProps = {
        token: '31252ccf4e4102d476fb19248a12931451a2897d',
        placeholder: '',
        value: '',
        autoload: false,
        onChange: undefined,
        saveDadata: undefined,
        type: 'text',
        disabled: false,
        maxLength: undefined,
        readOnly: false,
        id: '',
        caption: undefined,
        labelStyle: undefined,
        style: undefined,
        service: 'address',
        count: 10,
        parts: '',
        hint: '',
        hintPosition: 'bottom',
        hintDisabled: false,
        hintContainer: null,
        invalid: false,
    }

    constructor(props) {
        super(props);

        this.state = {
            value: this.props.value ? this.props.value : '',
            inputValue: this.props.value ? this.props.value : '',
            inputFocused: false,
            suggestions: [],
            suggestionIndex: -1,
            suggestionsVisible: true,
        };
    }

    componentDidMount() {
        if (this.props.autoload && this.state.value) {
            this.fetchSuggestions();
        }
    }

    onInputFocus = () => {
        this.setState({ inputFocused: true });
        if (this.state.suggestions.length === 0) {
            this.fetchSuggestions();
        }
    };

    onInputBlur = () => {
        this.setState({ inputFocused: false });
        if (this.state.suggestions.length === 0) {
            this.fetchSuggestions();
        }
    };

    onInputChange = (event) => {
        const value = event.target.value;
        this.setState({
            value,
            inputValue: value,
            suggestionsVisible: true,
        }, () => this.fetchSuggestions());

        if (this.props.onChange) {
            this.props.onChange(event);
        }
    };

    onKeyPress = (event) => {
        if (event.which === 40) {
            // Arrow down
            event.preventDefault();
            if (this.state.suggestionIndex < this.state.suggestions.length) {
                const newSuggestionIndex = this.state.suggestionIndex + 1;
                const newinputValue = this.state.suggestions[newSuggestionIndex].value;
                this.setState({ suggestionIndex: newSuggestionIndex, value: newinputValue });
            }
        } else if (event.which === 38) {
            // Arrow up
            event.preventDefault();
            if (this.state.suggestionIndex >= 0) {
                const newSuggestionIndex = this.state.suggestionIndex - 1;
                const newinputValue = newSuggestionIndex === -1
                    ? this.state.inputValue
                    : this.state.suggestions[newSuggestionIndex].value;
                this.setState({ suggestionIndex: newSuggestionIndex, value: newinputValue });
            }
        } else if (event.which === 13) {
            // Enter
            event.preventDefault();
            if (this.state.suggestionIndex >= 0) {
                this.selectSuggestion(this.state.suggestionIndex);
            }
        }
    };

    fetchSuggestions = () => {
        if (this.xhr) {
            this.xhr.abort();
        }
        this.xhr = new XMLHttpRequest();
        if (/10\.1\./.test(document.location.origin)) {
            this.xhr.open('POST', `/dadata/suggest/${this.props.service}`);
        } else {
            this.xhr.open('POST', `https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/${this.props.service}`);
        }
        this.xhr.setRequestHeader('Accept', 'application/json');
        this.xhr.setRequestHeader('Authorization', `Token ${this.props.token}`);
        this.xhr.setRequestHeader('Content-Type', 'application/json');
        this.xhr.send(JSON.stringify({
            query: this.state.value,
            count: 5 || this.props.count,
            parts: [`${this.props.parts.toUpperCase()}`],
        }));

        this.xhr.onreadystatechange = () => {
            if (this.xhr.readyState !== 4) {
                return;
            }

            if (this.xhr.status === 200) {
                const responseJson = JSON.parse(this.xhr.response);
                if (responseJson && responseJson.suggestions) {
                    this.setState({ suggestions: responseJson.suggestions, suggestionIndex: -1 });
                }
            }
        };
    };

    onSuggestionClick = (event, index) => {
        event.stopPropagation();
        this.selectSuggestion(index);
    };

    selectSuggestion = (index) => {
        if (this.state.suggestions.length >= index - 1) {
            this.setState({
                value: this.state.suggestions[index].value,
                suggestionsVisible: false,
                inputValue: this.state.suggestions[index].value,
            }, () => {
                this.fetchSuggestions();
                setTimeout(() => this.setCursorToEnd(this.textInput), 100);
            });

            if (this.props.saveDadata) {
                this.props.saveDadata(this.state.suggestions[index], this.textInput.id);
            }
        }
    };

    setCursorToEnd = (element) => {
        const valueLength = element.value.length;
        if (element.selectionStart || element.selectionStart === '0') {
            // Firefox/Chrome
            element.selectionStart = valueLength;
            element.selectionEnd = valueLength;
            element.focus();
        }
    };

    getHighlightWords = () => {
        const wordsToPass = [
            'г', 'респ', 'ул',
            'р-н', 'село', 'деревня',
            'поселок', 'пр-д', 'пл',
            'к', 'кв', 'обл', 'д',
        ];

        let words = this.state.inputValue.replace(',', '').split(' ');
        words = words.filter(word => wordsToPass.indexOf(word) < 0);
        return words;
    };

    renderHintContainer = (content, isInvalid = false) => {
        const inputOffsetTop = this.group.offsetTop;
        let hintPosition = inputOffsetTop + 11;

        if (/union/.test(this.group.parentNode.className) && this.group.parentNode.offsetTop > 0) {
            hintPosition += this.group.parentNode.offsetTop;
        }

        this.props.hintContainer.innerHTML = '';

        const hintContent = (
            <div
                style={{
                    position: 'absolute',
                    top: `${hintPosition}px`,
                    color: isInvalid && '#ff3b30',
                }}
            >
                {content}
            </div>
        );

        render(hintContent, this.props.hintContainer);
    }

    handleMouseEnter = () => {
        if (this.props.hintContainer && this.props.invalid && typeof this.props.invalid === 'string') {
            this.renderHintContainer(this.props.invalid, true);
            return;
        }

        if (this.props.hint && this.props.hintContainer && !this.props.hintDisabled) {
            this.renderHintContainer(this.props.hint);
        }
    }

    handleMouseLeave = () => {
        if (this.props.hintContainer && !this.props.hintDisabled) {
            unmountComponentAtNode(this.props.hintContainer);
            this.props.hintContainer.innerHTML = /undefined/.test(this.previousHintContainerHTML)
                ? ''
                : this.previousHintContainerHTML;
            this.props.hintContainer.style.paddingTop = this.previousHintContainerPadding;
        }
    }

    render() {
        return (
            <div
                className={styles['input-group']}
                style={this.props.style}
                onMouseEnter={this.handleMouseEnter}
                onMouseLeave={this.handleMouseLeave}
                data-disabled={this.props.disabled}
                data-invalid={!!this.props.invalid}
                ref={(group) => { this.group = group; }}
            >
                {this.props.caption || this.props.hint
                    ? (
                        <div className={styles['input-top']} style={this.props.labelStyle}>
                            {this.props.caption
                                ? (
                                    <label htmlFor={this.props.id}>
                                        {this.props.caption}
                                    </label>
                                )
                                : null}
                            {this.props.hint && false
                                ? (
                                    <Hint
                                        text={this.props.hint}
                                        hintPosition={this.props.hintPosition}
                                        style={{ top: -1 }}
                                    />
                                )
                                : null}
                        </div>
                    )
                    : null}
                <input
                    id={this.props.id}
                    type={this.props.type}
                    onChange={this.onInputChange}
                    onKeyPress={this.onKeyPress}
                    onKeyDown={this.onKeyPress}
                    onFocus={this.onInputFocus}
                    onBlur={this.onInputBlur}
                    ref={(input) => { this.textInput = input; }}
                    disabled={this.props.disabled}
                    value={this.state.value}
                    maxLength={this.props.maxLength}
                    placeholder={this.props.placeholder}
                    readOnly={this.props.readOnly}
                    autoComplete="off"
                />
                {
                    this.state.inputFocused &&
                    this.state.suggestionsVisible &&
                    this.state.suggestions &&
                    this.state.suggestions.length > 0 &&
                        (
                            <div className={styles.autocomplete}>
                                {this.state.suggestions.map((suggestion, index) => {
                                    return (
                                        <div
                                            key={JSON.stringify(suggestion.data)}
                                            onMouseDown={e => this.onSuggestionClick(e, index)}
                                            className={styles['autocomplete-item']}
                                        >
                                            <Highlighter
                                                highlightClassName={styles['autocomplete-highlight']}
                                                searchWords={this.getHighlightWords()}
                                                textToHighlight={suggestion.value}
                                            />
                                        </div>
                                    );
                                })}
                            </div>
                        )
                }
            </div>
        );
    }
}
