import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { DayPickerRangeController } from 'react-dates';
import CalendarBottom from 'components/CalendarBottom';
import moment from 'moment';

import styles from './select-period.css';

const initialState = {
    focusedInput: 'startDate',
    showCalendar: false,
    analyticsPeriodOption: 'week',
    startDate: moment().subtract(7, 'days'),
    endDate: moment(),
};

export default class SelectPeriod extends Component {
    constructor(props) {
        super(props);

        this.state = initialState;
    }

    static propTypes = {
        fetchData: PropTypes.func,
    }

    static defaultProps = {
        fetchData: undefined,
    }

    handleOptionChange = (e) => {
        const value = e.target.value;

        this.setState({
            analyticsPeriodOption: value,
        });
    }

    handlePeriodSet = (startDate, endDate) => {
        this.setState({
            startDate,
            endDate,
            showCalendar: false,
        });

        this.props.fetchData(startDate, endDate);
    }

    handleCalendarIconClick = bool => this.setState({ showCalendar: bool });

    handleCalendarFocusChange = (focusedInput) => {
        this.setState({
            focusedInput: !focusedInput ? 'startDate' : focusedInput,
        });
    }

    handleDatesChange = ({ startDate, endDate }) => {
        this.setState({
            startDate,
            endDate,
        });
    }

    handleCalendarPeriodSubmit = () => {
        this.setState({
            showCalendar: false,
        });

        this.props.fetchData(this.state.startDate, this.state.endDate);
    }

    handleCalendarPeriodClear = () => {
        this.setState({
            startDate: initialState.startDate,
            endDate: initialState.endDate,
            showCalendar: false,
        });

        this.props.fetchData(initialState.startDate, this.state.endDate);
    }

    render() {
        return (
            <div className={styles.container}>
                <div className={styles.buttons}>
                    <div className={styles['radio-item']}>
                        <input
                            className={styles['radio-input']}
                            type="radio"
                            value="week"
                            id="weekOption"
                            checked={this.state.analyticsPeriodOption === 'week'}
                            onChange={this.handleOptionChange}
                            onClick={() => {
                                const start = moment().subtract(7, 'days');
                                const end = moment();

                                this.handlePeriodSet(start, end);
                            }}
                        />
                        <label
                            className={styles['radio-label']}
                            htmlFor="weekOption"
                        >
                            Неделя
                        </label>
                    </div>
                    <div className={styles['radio-item']}>
                        <input
                            className={styles['radio-input']}
                            type="radio"
                            value="month"
                            id="monthOption"
                            checked={this.state.analyticsPeriodOption === 'month'}
                            onChange={this.handleOptionChange}
                            onClick={() => {
                                const start = moment().startOf('month');
                                const end = moment();

                                this.handlePeriodSet(start, end);
                            }}
                        />
                        <label
                            className={styles['radio-label']}
                            htmlFor="monthOption"
                        >
                            Месяц
                        </label>
                    </div>
                    <div className={styles['radio-item']}>
                        <input
                            className={styles['radio-input']}
                            type="radio"
                            value="quarter"
                            id="quarterOption"
                            checked={this.state.analyticsPeriodOption === 'quarter'}
                            onChange={this.handleOptionChange}
                            onClick={() => {
                                const quarter = moment().quarter();
                                const start = moment().quarter(quarter).startOf('quarter');
                                const end = moment().quarter(quarter).endOf('quarter');

                                this.handlePeriodSet(start, end);
                            }}
                        />
                        <label
                            className={styles['radio-label']}
                            htmlFor="quarterOption"
                        >
                            Квартал
                        </label>
                    </div>
                </div>
                <div className={styles.calendar}>
                    <div className={styles['radio-item']}>
                        <input
                            className={styles['radio-input']}
                            type="radio"
                            value="custom"
                            id="customOption"
                            checked={this.state.analyticsPeriodOption === 'custom'}
                            onChange={this.handleOptionChange}
                            onClick={() => this.handleCalendarIconClick(true)}
                        />
                        <label
                            className={styles['radio-label']}
                            htmlFor="customOption"
                        >
                            <span className={styles['calendar-icon']} />
                        </label>
                    </div>
                    {this.state.showCalendar && (
                        <div className={styles.daypicker}>
                            <DayPickerRangeController
                                startDate={this.state.startDate}
                                endDate={this.state.endDate}
                                focusedInput={this.state.focusedInput}
                                onFocusChange={this.handleCalendarFocusChange}
                                onDatesChange={this.handleDatesChange}
                                firstDayOfWeek={1}
                                isOutsideRange={() => false}
                                numberOfMonths={2}
                                minimumNights={0}
                                daySize={35}
                                hideKeyboardShortcutsPanel
                                onOutsideClick={() => this.handleCalendarIconClick(false)}
                                renderCalendarInfo={() => (
                                    <CalendarBottom
                                        showPeriods={false}
                                        onSubmit={this.handleCalendarPeriodSubmit}
                                        onCancel={this.handleCalendarPeriodClear}
                                    />
                                )}
                            />
                        </div>
                    )}
                </div>
            </div>
        );
    }
}
