import React, { Component } from 'react';
import PropTypes from 'prop-types';
import BasicInput from '../BasicInput';
import styles from './ui.css';

export default class IconedInput extends Component {
    static propTypes = {
        className: PropTypes.string,
        style: PropTypes.object,
        icon: PropTypes.string,
        iconBackgroundColor: PropTypes.string,
        inputProps: PropTypes.object,
        errorMsg: PropTypes.string,
    };

    static defaultProps = {
        className: '',
        style: {},
        icon: '',
        iconBackgroundColor: '#3f3149',
        inputProps: {},
        errorMsg: '',
    };

    render() {
        const {
            className,
            style,
            icon,
            iconBackgroundColor,
            iconBackgroundSize,
            inputProps,
            errorMsg,
            noIcon,
        } = this.props;

        const wrapperClass = className ? `${styles.wrapper} ${className}` : styles.wrapper;

        const iconStyles = {
            backgroundColor: iconBackgroundColor,
            backgroundImage: `url(${icon})`,
            backgroundSize: iconBackgroundSize,
        };

        return (
            <div className={wrapperClass} style={style}>
                {!noIcon
                    ? (
                        <div className={styles['icon-wrapper']} style={iconStyles} />
                    )
                    : null}
                <BasicInput
                    className={styles.input}
                    {...inputProps}
                />
                {
                    errorMsg && errorMsg.length > 0
                        ? (
                            <div className={styles['error-msg']}>
                                {errorMsg}
                            </div>
                        )
                        : null
                }
            </div>
        );
    }
}
