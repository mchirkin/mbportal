import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './ui.css';

export default class Toggle extends Component {
    static propTypes = {
        disabled: PropTypes.bool,
        values: PropTypes.array.isRequired,
        activeValue: PropTypes.string.isRequired,
        hidden: PropTypes.bool,
        onChange: PropTypes.func.isRequired,
    };

    static defaultProps = {
        disabled: false,
        hidden: false,
    };

    constructor(props) {
        super(props);

        this.state = {
            toggled: false,
        };
    }

    toggle = () => {
        if (this.props.disabled) {
            return;
        }
        const newValue = this.props.values.find(el => el.id !== this.props.activeValue);
        this.props.onChange(newValue);
    }

    render() {
        const toggled = this.props.values.findIndex(el => el.id === this.props.activeValue) === 1;
        return (
            <div
                className={styles.toggle}
                data-disabled={this.props.disabled}
                data-hidden={this.props.hidden}
            >
                <div style={{ marginRight: 15 }}>
                    {this.props.values[0].name}
                </div>
                <div className={styles['toggle-track']} onClick={this.toggle} style={{ marginRight: 15 }}>
                    <div className={styles['toggle-thumb']} data-toggled={toggled} />
                </div>
                <div
                    style={{
                        color: this.props.disabled ? '#ccc' : '#000',
                    }}
                >
                    {this.props.values[1].name}
                </div>
            </div>
        );
    }
}
