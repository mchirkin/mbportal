import React from 'react';
import classNames from 'classnames/bind';
import styles from './main.css';

const cx = classNames.bind(styles);

/**
 * Компонент для чекбокса
 */
export default class CheckBox extends React.Component {
    constructor(props) {
        super(props);
        /**
         * @property {boolean} status - отмечен чекбокс или нет
         */
        this.state = {
            status: false,
        };
    }

    componentWillMount() {
        // Установка значения по умолчанию
        if (this.props.defaultValue) {
            this.setState({
                status: true,
            });
        }
    }

    componentWillReceiveProps(props) {
        // Устанавливаем статус чекбокса, если пришло свойство value
        if (typeof props.value !== 'undefined') {
            this.setState({
                status: props.value,
            });
        }
    }

    /**
     * Установка значения чекбокса
     * @param {boolean} value - значение чекбокса
     */
    setCheckboxValue = (value) => {
        this.setState({
            status: value,
        });
        // Вызываем обработчик изменения чекбокса, если передан в свойствах
        if (this.props.handleChange) {
            this.props.handleChange({
                id: this.props.id,
                value,
            });
        }
    }

    /**
     * Обработчик нажатия на чекбокс
     * @param {Event} e
     */
    handleCheckboxClick = (e) => {
        e.stopPropagation();

        const newStatus = !this.state.status;

        this.setState({
            status: newStatus,
        });

        if (this.props.handleChange) {
            this.props.handleChange({
                id: this.props.id,
                value: newStatus,
            });
        }
    }

    render() {
        const imgBoxStyles = cx({
            imgBox: true,
            imgBox_active: this.state.status === true,
        });

        return (
            <div className={styles['checkbox-container']} style={this.props.style}>
                <div className={imgBoxStyles} onClick={this.handleCheckboxClick} />
                {this.props.caption
                    ? (
                        <div className={styles.caption} onClick={this.handleCheckboxClick}>
                            {this.props.caption}
                        </div>
                    )
                    : null}
                <input className={styles.input} type="checkbox" name="check" checked={this.state.status} readOnly />
            </div>
        );
    }
}

/**
 * propTypes
 * @property {boolean} defaultValue - значение чекбокса по умолчанию
 * @property {boolean} value - значение чекбокса
 * @property {string} caption - название чекбокса
 * @property {Object} style - стили для div'a обертки
 * @property {Function} handleChange - callback изменения чекбокса
 * @property {string|number|Object} id - идентификатор чекбокса
 */
CheckBox.propTypes = {
    defaultValue: React.PropTypes.bool,
    value: React.PropTypes.bool,
    caption: React.PropTypes.string,
    style: React.PropTypes.object,
    handleChange: React.PropTypes.func.isRequired,
    id: React.PropTypes.oneOfType([
        React.PropTypes.string,
        React.PropTypes.number,
        React.PropTypes.object,
    ]).isRequired,
};

CheckBox.defaultProps = {
    defaultValue: false,
    value: undefined,
    caption: undefined,
    style: {},
};
