import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import sinon from 'sinon';
import CheckBox from './index';
import styles from './main.css';

describe('ui:CheckBox', () => {
    it('it has id property', () => {
        const wrapper = shallow(<CheckBox id="some-id" />);
        expect(wrapper.instance().props.id).to.equal('some-id');
    });

    it('is checked if prop defaultValue={true}', () => {
        const wrapper = shallow(<CheckBox defaultValue />);
        expect(wrapper.state('status')).to.equal(true);
    });

    it('is unchecked if prop value={false}', () => {
        const wrapper = shallow(<CheckBox defaultValue />);
        wrapper.setProps({
            value: false,
        });
        expect(wrapper.state('status')).to.equal(false);
    });

    it('call handleChange when click on checkbox', () => {
        const onCheckboxClick = sinon.spy();
        const wrapper = shallow(<CheckBox handleChange={onCheckboxClick} />);
        wrapper.find(`.${styles.imgBox}`).simulate('click', { stopPropagation: () => undefined });
        expect(onCheckboxClick).to.have.property('callCount', 1);
    });

    it('call handleChange when click on label', () => {
        const onCheckboxClick = sinon.spy();
        const wrapper = shallow(<CheckBox handleChange={onCheckboxClick} caption="Label" />);
        wrapper.find(`.${styles.caption}`).simulate('click', { stopPropagation: () => undefined });
        expect(onCheckboxClick).to.have.property('callCount', 1);
    });
});
