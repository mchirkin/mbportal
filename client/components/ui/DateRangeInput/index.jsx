import React from 'react';
import Datepicker from '../Datepicker';
import styles from './input.css';

/**
 * Компонент выбора временного промежутка
 */
export default class DateRangeInput extends React.Component {
    /**
     * Установить значение даты указанного календаря
     * @param {string} ref - указатель на календарь
     * @param {string} value - значение даты в формате dd.mm.YYYY
     */
    setDate = (ref, value) => {
        this[ref].setSelectedDateString(null, value);
    }

    /**
     * Обнулить значения всех календарей
     */
    clearDateRange = () => {
        this.from.setSelectedDateString(null, '');
        this.to.setSelectedDateString(null, '');
    }

    render() {
        return (
            <div
                className={`${styles['input-group']} ${styles['input-group-date']}`}
                style={this.props.styles}
            >
                <span className={styles['input-label']}>{this.props.caption}</span>
                <span style={{ marginRight: 10 }}>с</span>
                <Datepicker
                    key="from"
                    ref={(from) => { this.from = from; }}
                    filterName="from"
                    onChange={this.props.setDateFilter}
                    initialDate={this.props.initialFromDate}
                    calendarStyles={this.props.firstDatepickerCalendarStyles}
                    inputStyles={{ borderBottom: '1px solid #e8e8e8' }}
                />
                <span style={{ margin: '0 10px' }}>по</span>
                <Datepicker
                    key="to"
                    ref={(to) => { this.to = to; }}
                    filterName="to"
                    onChange={this.props.setDateFilter}
                    initialDate={this.props.initialToDate}
                    calendarStyles={this.props.secondDatepickerCalendarStyles}
                    inputStyles={{ borderBottom: '1px solid #e8e8e8' }}
                />
            </div>
        );
    }
}

/**
 * propTypes
 * @property {Object} styles - стили для div'a обертки
 * @property {string} caption - название
 * @property {Date} initialFromDate - первоначальная дата календаря начала периода
 * @property {Date} initialToDate - первоначальная дата календаря конца периода
 * @property {Function} setDateFilter - callback при изменении значения одного из календарей
 * @property {Object} firstDatepickerCalendarStyles - стили для календаря начала периода
 * @property {Object} secondDatepickerCalendarStyles - стили для календаря окончания периода
 */
DateRangeInput.propTypes = {
    styles: React.PropTypes.object,
    caption: React.PropTypes.string,
    initialFromDate: React.PropTypes.instanceOf(Date),
    initialToDate: React.PropTypes.instanceOf(Date),
    setDateFilter: React.PropTypes.func,
    firstDatepickerCalendarStyles: React.PropTypes.object,
    secondDatepickerCalendarStyles: React.PropTypes.object,
};

DateRangeInput.defaultProps = {
    styles: {},
    caption: undefined,
    initialFromDate: undefined,
    initialToDate: undefined,
    setDateFilter: undefined,
    firstDatepickerCalendarStyles: {},
    secondDatepickerCalendarStyles: {},
};
