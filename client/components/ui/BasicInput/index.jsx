import React, { Component } from 'react';
import PropTypes from 'prop-types';
import InputElement from 'react-input-mask';
import _ from 'lodash';
import styles from './ui.css';

/**
 * Компонент для поля ввода
 */
export default class BasicInput extends Component {
    static propTypes = {
        id: PropTypes.string.isRequired,
        className: PropTypes.string,
        style: PropTypes.object,
        placeholder: PropTypes.string,
        mask: PropTypes.string,
        maskChar: PropTypes.string,
        type: PropTypes.string,
        disabled: PropTypes.bool,
        readOnly: PropTypes.bool,
        value: PropTypes.string.isRequired,
        defaultValue: PropTypes.string,
        maxLength: PropTypes.number,
        onChange: PropTypes.func.isRequired,
        tabIndex: PropTypes.number,
    };

    static defaultProps = {
        className: '',
        style: {},
        placeholder: '',
        mask: undefined,
        maskChar: '_',
        type: 'text',
        disabled: false,
        readOnly: false,
        defaultValue: undefined,
        maxLength: undefined,
        tabIndex: undefined,
    };

    shouldComponentUpdate(nextProps) {
        if (!_.isEqual(this.props, nextProps)) {
            return true;
        }
        return false;
    }

    render() {
        // const type = this.props.type === 'password' ? 'text' : this.props.type;

        const element = this.props.mask
            ? (
                <InputElement
                    id={this.props.id}
                    type={this.props.type}
                    className={this.props.className}
                    onChange={this.props.onChange}
                    ref={(input) => { this.input = input; }}
                    disabled={this.props.disabled}
                    value={this.props.value}
                    defaultValue={this.props.defaultValue}
                    maxLength={this.props.maxLength}
                    placeholder={this.props.placeholder}
                    readOnly={this.props.readOnly}
                    mask={this.props.mask}
                    maskChar={this.props.maskChar}
                    tabIndex={this.props.tabIndex}
                />
            )
            : (
                <input
                    id={this.props.id}
                    type={this.props.type}
                    className={this.props.className}
                    onChange={this.props.onChange}
                    ref={(input) => { this.input = input; }}
                    disabled={this.props.disabled}
                    value={this.props.value}
                    defaultValue={this.props.defaultValue}
                    maxLength={this.props.maxLength}
                    placeholder={this.props.placeholder}
                    readOnly={this.props.readOnly}
                    tabIndex={this.props.tabIndex}
                />
            );

        return element;
    }
}
