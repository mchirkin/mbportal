import React from 'react';
import { shallow, mount } from 'enzyme';
import { expect } from 'chai';
import sinon from 'sinon';
import Datepicker from './index';
import styles from './datepicker.css';

describe('ui:Datepicker', () => {
    it('set today to current date', () => {
        const wrapper = shallow(<Datepicker />);
        const today = new Date();
        today.setMilliseconds(0);
        today.setSeconds(0);
        today.setMinutes(0);
        today.setHours(0);
        expect(wrapper.state().today.getTime()).to.equal(today.getTime());
    });

    it('set selectedDate and selectedDateString when initialDate prop is passed', () => {
        const initialDate = new Date('2017-03-16');
        const wrapper = mount(<Datepicker initialDate={initialDate} />);
        expect(wrapper.state().selectedDate.getTime()).to.equal(initialDate.getTime());
        expect(wrapper.state().selectedDateString).to.equal('16.03.2017');
    });

    it('has right dates array for different monthes', () => {
        const initialDate = new Date('2017-03-01');
        const wrapper = shallow(<Datepicker initialDate={initialDate} />);
        expect(wrapper.state().dates).to.have.length(35);
        wrapper.instance().setSelectedDateString(null, '01.01.2017');
        expect(wrapper.state().dates).to.have.length(42);
        wrapper.instance().setSelectedDateString(null, '01.06.2014');
        expect(wrapper.state().dates).to.have.length(42);
    });

    it('changeMonth function works correctly', () => {
        const initialDate = new Date('2017-03-01');
        const wrapper = shallow(<Datepicker initialDate={initialDate} />);
        expect(wrapper.state().currentMonth).to.equal(2);
        wrapper.instance().changeMonth(1);
        expect(wrapper.state().currentMonth).to.equal(3);
        wrapper.instance().changeMonth(-1);
        expect(wrapper.state().currentMonth).to.equal(2);
        wrapper.instance().changeMonth(null, 5);
        expect(wrapper.state().currentMonth).to.equal(5);
    });

    it('show year and month list when state.showMonthList={true}', () => {
        const wrapper = shallow(<Datepicker />);
        expect(wrapper.find(`.${styles['year-select-list']}`)).to.have.length(0);
        expect(wrapper.find(`.${styles['month-select-list']}`)).to.have.length(0);
        wrapper.setState({
            showMonthList: true,
        });
        expect(wrapper.find(`.${styles['year-select-list']}`)).to.have.length(1);
        expect(wrapper.find(`.${styles['month-select-list']}`)).to.have.length(1);
    });

    it('show year and month list when click on month/year', () => {
        const wrapper = shallow(<Datepicker />);
        expect(wrapper.find(`.${styles['year-select-list']}`)).to.have.length(0);
        expect(wrapper.find(`.${styles['month-select-list']}`)).to.have.length(0);
        wrapper.find(`.${styles['select-month']}`).simulate('click');
        expect(wrapper.find(`.${styles['year-select-list']}`)).to.have.length(1);
        expect(wrapper.find(`.${styles['month-select-list']}`)).to.have.length(1);
    });

    it('change month when click on left and right chevron', () => {
        const initialDate = new Date('2017-03-01');
        const wrapper = shallow(<Datepicker initialDate={initialDate} />);
        expect(wrapper.state().currentMonth).to.equal(2);
        wrapper.find(`.${styles['chevron-left']}`).simulate('click');
        expect(wrapper.state().currentMonth).to.equal(1);
        wrapper.find(`.${styles['chevron-right']}`).simulate('click');
        expect(wrapper.state().currentMonth).to.equal(2);
    });

    it('change year when decrease month from january to december', () => {
        const initialDate = new Date('2017-01-01');
        const wrapper = shallow(<Datepicker initialDate={initialDate} />);
        expect(wrapper.state().currentMonth).to.equal(0);
        wrapper.find(`.${styles['chevron-left']}`).simulate('click');
        expect(wrapper.state().currentMonth).to.equal(11);
        expect(wrapper.state().currentYear).to.equal(2016);
    });

    it('change year when increase month from december to january', () => {
        const initialDate = new Date('2016-12-01');
        const wrapper = shallow(<Datepicker initialDate={initialDate} />);
        expect(wrapper.state().currentMonth).to.equal(11);
        wrapper.find(`.${styles['chevron-right']}`).simulate('click');
        expect(wrapper.state().currentMonth).to.equal(0);
        expect(wrapper.state().currentYear).to.equal(2017);
    });

    it('call onChange when date changed', () => {
        const onChange = sinon.spy();
        const wrapper = shallow(<Datepicker onChange={onChange} />);
        wrapper.instance().setSelectedDateString(null, '01.01.2017');
        expect(onChange).to.have.property('callCount', 1);
    });
});
