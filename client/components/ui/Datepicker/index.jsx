import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import MaskedInput from 'react-maskedinput';
import _ from 'lodash';
import { findTargetInChildNode } from 'utils/dom';
import getMonthName from 'utils/get_month_name';
import styles from './datepicker.css';

const cx = classNames.bind(styles);

/**
 * Компонент выбора даты
 */
export default class Datepicker extends Component {
    static propTypes = {
        /** первоначальная дата */
        initialDate: PropTypes.instanceOf(Date),
        /** стили для div'а обертки */
        styles: PropTypes.object,
        /** стили для поля ввода */
        inputStyles: PropTypes.object,
        /** стили для div'а календаря */
        calendarStyles: PropTypes.object,
        /** запрещено ли редактирование значения календаря */
        disabled: PropTypes.bool,
        /** обработчик изменения значения календаря */
        onChange: PropTypes.func,
        /** идентификатор календаря, передается в onChange */
        filterName: PropTypes.string,
        /** можно ли поставить дату 0 */
        zeroDate: PropTypes.bool,
    };

    static defaultProps = {
        initialDate: undefined,
        styles: {},
        inputStyles: {},
        calendarStyles: {},
        disabled: false,
        onChange: () => null,
        filterName: '',
        zeroDate: false,
    };


    constructor(props) {
        super(props);

        const currentDate = this.props.initialDate ? this.props.initialDate : new Date();
        currentDate.setMilliseconds(0);
        currentDate.setSeconds(0);
        currentDate.setMinutes(0);
        currentDate.setHours(0);

        const today = new Date();
        today.setMilliseconds(0);
        today.setSeconds(0);
        today.setMinutes(0);
        today.setHours(0);

        this.state = {
            today,
            currentMonth: currentDate.getMonth(),
            currentYear: currentDate.getFullYear(),
            dates: this.getMonth(currentDate.getMonth(), currentDate.getFullYear()),
            selectedDate: this.props.initialDate ? this.props.initialDate : null,
            selectedDateString: '',
            calendarVisible: false,
            showMonthList: false,
        };
    }

    componentDidMount() {
        document.querySelector('body').addEventListener('click', (e) => {
            if (!findTargetInChildNode(this.datepickerContainer, e.target)) {
                if (this.datepickerContainer) {
                    this.setState({
                        calendarVisible: false,
                    });
                }
            }
        });

        if (this.props.initialDate) {
            const date = this.props.initialDate;

            let dateOfMonth = date.getDate();
            dateOfMonth = dateOfMonth.toString().length === 1 ? `0${dateOfMonth}` : dateOfMonth;

            let month = date.getMonth() + 1;
            month = month.toString().length === 1 ? `0${month}` : month;

            const dateStr = `${dateOfMonth}.${month}.${date.getFullYear()}`;
            this.setSelectedDateString(null, dateStr);
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (!_.isEqual(this.props, nextProps) || !_.isEqual(this.state, nextState)) {
            return true;
        }
        return false;
    }

    /**
     * Установка значения по умолчанию
     */
    setDefaultValue = () => {
        const currentDate = this.props.initialDate ? this.props.initialDate : new Date();
        currentDate.setMilliseconds(0);
        currentDate.setSeconds(0);
        currentDate.setMinutes(0);
        currentDate.setHours(0);

        const today = new Date();
        today.setMilliseconds(0);
        today.setSeconds(0);
        today.setMinutes(0);
        today.setHours(0);

        this.setState({
            today,
            currentMonth: currentDate.getMonth(),
            currentYear: currentDate.getFullYear(),
            dates: this.getMonth(currentDate.getMonth(), currentDate.getFullYear()),
            selectedDate: this.props.initialDate ? this.props.initialDate : null,
            selectedDateString: '',
            calendarVisible: false,
        });
    }

    /**
     * Получение дат в заданном месяце
     * @param {number} monthNumber - номер месяца (от 0 до 11)
     * @param {number} year - год
     * @return Array
     */
    getMonth = (monthNumber, year) => {
        const firstDayOfMonth = new Date(year, monthNumber, 1);

        let dayOfWeek = firstDayOfMonth.getDay();
        // ставим воскресенье 7ым днем
        dayOfWeek = dayOfWeek === 0 ? 7 : dayOfWeek;

        const result = [];
        // заполняем первую неделю числами с прошлого месяца
        for (let i = dayOfWeek - 1; i > 0; i -= 1) {
            const day = new Date(year, monthNumber, 1 - i);
            result.push(day);
        }

        let currentDate = 1;

        /**
         * заполняем месяц и последнюю неделю числами со следующего месяца
         * 35 - максимальное количество дней с учетом дополнения последней недели числами следующего месяца
         */
        for (let i = 0; i <= 35; i += 1) {
            const day = new Date(year, monthNumber, currentDate);
            currentDate += 1;
            if (day.getMonth() === monthNumber || day.getDay() !== 1) {
                result.push(day);
            } else {
                break;
            }
        }

        return result;
    }

    /**
     * Изменить текущий месяц
     * @param {number} direction - увеличивать или уменьшить текущий месяц
     * @param {number|boolean} month - месяц
     * @param {number|boolean} year - год
     */
    changeMonth = (direction, month = false, year = false) => {
        let newYear = year || this.state.currentYear;
        let newMonth = month || this.state.currentMonth;

        if (direction && direction > 0) {
            if (this.state.currentMonth + 1 > 11) {
                newYear = this.state.currentYear + 1;
                newMonth = 0;
            } else {
                newMonth = this.state.currentMonth + 1;
            }
        }
        if (direction && direction < 0) {
            if (this.state.currentMonth - 1 < 0) {
                newYear = this.state.currentYear - 1;
                newMonth = 11;
            } else {
                newMonth = this.state.currentMonth - 1;
            }
        }
        this.setState({
            currentMonth: newMonth,
            currentYear: newYear,
            dates: this.getMonth(newMonth, newYear),
        });
    }

    /**
     * Установка новой выбранной даты календаря
     * @param {Date} date - дата для установки выбранной датой календаря
     */
    setSelectedDate = (date) => {
        this.setState({
            selectedDate: date,
            calendarVisible: false,
        });

        let dateOfMonth = date.getDate();
        dateOfMonth = dateOfMonth.toString().length === 1 ? `0${dateOfMonth}` : dateOfMonth;

        let month = date.getMonth() + 1;
        month = month.toString().length === 1 ? `0${month}` : month;

        const dateStr = `${dateOfMonth}.${month}.${date.getFullYear()}`;
        this.setSelectedDateString(null, dateStr);
        this.setState({
            currentMonth: date.getMonth(),
            currentYear: date.getFullYear(),
            dates: this.getMonth(date.getMonth(), date.getFullYear()),
        });
    }

    /**
     * Установка новой выбранной даты из строки
     * @param {Event|null} e
     * @param {string} str - строка с датой в формате dd.mm.YYYY
     */
    setSelectedDateString = (e, str) => {
        const newSelectedDateString = e ? e.target.value : str;
        if (newSelectedDateString.replace(/\s/g, '').length === 10) {
            const dateData = newSelectedDateString.split('.');
            const newSelectedDate = new Date(
                parseInt(dateData[2], 10),
                parseInt(dateData[1], 10) - 1,
                parseInt(dateData[0], 10),
            );
            this.setState({
                selectedDate: newSelectedDate,
                selectedDateString: newSelectedDateString,
                currentMonth: newSelectedDate.getMonth(),
                currentYear: newSelectedDate.getFullYear(),
                dates: this.getMonth(newSelectedDate.getMonth(), newSelectedDate.getFullYear()),
                showMonthList: false,
            });
            this.props.onChange(this.props.filterName, newSelectedDateString);
        }
        if (this.props.zeroDate && newSelectedDateString.replace(/[\s.]/g, '') === '0') {
            this.setState({
                selectedDateString: '0',
            });
            this.props.onChange(this.props.filterName, '0');
        }
        if (newSelectedDateString.replace(/\s/g, '').length === 0) {
            this.setDefaultValue();
        }
    }

    /**
     * Показать выбор месяца
     */
    showMonthList = () => {
        this.setState({
            showMonthList: true,
        });
    }

    /**
     * Обработчик нажатия на месяц
     * @param {number} index - номер месяца
     */
    handleMonthListItemClick = (index) => {
        this.setState({
            currentMonth: index,
            dates: this.getMonth(index, this.state.currentYear),
            showMonthList: false,
        });
    }

    /**
     * Обработчик нажатия на год
     * @param {number} year - год
     */
    handleYearListItemClick = (year) => {
        const yearNumber = parseInt(year, 10);
        this.setState({
            currentYear: yearNumber,
            dates: this.getMonth(this.state.currentMonth, yearNumber),
            showMonthList: false,
        });
    }

    /**
     * Получить разметку для списка месяцев
     * @return {Array} массив ReactElement'ов
     */
    getMonthListMarkup = () => {
        const monthNames = [
            'Январь',
            'Февраль',
            'Март',
            'Апрель',
            'Май',
            'Июнь',
            'Июль',
            'Август',
            'Сентябрь',
            'Октябрь',
            'Ноябрь',
            'Декабрь',
        ];
        return monthNames.map((m, index) => (
            <div
                key={m}
                className={styles['month-select-list-item']}
                onClick={() => this.handleMonthListItemClick(index)}
            >
                {m}
            </div>
        ));
    }

    /**
     * Получить разметку для списка годов
     * Возвращает 5 годов +- 2 от текущего выбранного
     * @return {Array} массив ReactElement'ов
     */
    getYearListMarkup = () => {
        const year = this.state.currentYear;
        const yearList = [year - 2, year - 1, year, year + 1, year + 2];
        return yearList.map(y => (
            <div
                key={y}
                className={styles['year-select-list-item']}
                onClick={() => this.handleYearListItemClick(y)}
            >
                {y}
            </div>
        ));
    }

    /**
     * Показать календарь, если разрешено редактирование
     */
    toggleCalendarVisibility = () => {
        if (this.props.disabled) {
            return;
        }
        this.setState({
            calendarVisible: !this.state.calendarVisible,
        });
    }

    render() {
        const calendarStyles = cx({
            'datepicker-calendar': true,
            'datepicker-calendar--visible': this.state.calendarVisible,
        });

        return (
            <div
                className={styles.datepicker}
                ref={(datepickerContainer) => { this.datepickerContainer = datepickerContainer; }}
                style={this.props.styles}
            >
                <MaskedInput
                    className={styles['datepicker-input']}
                    type="text"
                    mask="11.11.1111"
                    placeholderChar=" "
                    maxLength="10"
                    value={this.state.selectedDateString}
                    onChange={this.setSelectedDateString}
                    disabled={this.props.disabled}
                    style={this.props.inputStyles}
                    ref={(dateInput) => { this.dateInput = dateInput; }}
                />
                <div className={styles['datepicker-icon']} onClick={this.toggleCalendarVisibility} />
                <div className={calendarStyles} style={this.props.calendarStyles}>
                    {!this.state.showMonthList
                        ? (
                            <div className={styles['calendar-top']}>
                                <div className={styles['chevron-left']} onClick={() => this.changeMonth(-1)} />
                                <div className={styles['select-month']} onClick={this.showMonthList}>
                                    {`${getMonthName(this.state.currentMonth)} ${this.state.currentYear}`}
                                </div>
                                <div className={styles['chevron-right']} onClick={() => this.changeMonth(1)} />
                            </div>
                        )
                        : null}
                    {this.state.showMonthList
                        ? (
                            <div style={{ width: '100%' }}>
                                <div className={styles['year-select-list']}>
                                    {this.getYearListMarkup()}
                                </div>
                                <div className={styles['month-select-list']}>
                                    {this.getMonthListMarkup()}
                                </div>
                            </div>
                        )
                        : (
                            <div className={styles['date-list']}>
                                {this.state.dates.map((date) => {
                                    const dateStyles = cx({
                                        date: true,
                                        'date--grey': this.state.currentMonth !== date.getMonth(),
                                        'date--current': (
                                            this.state.selectedDate !== null &&
                                            this.state.selectedDate.getTime() === date.getTime()
                                        ),
                                        'date--today': this.state.today.getTime() === date.getTime(),
                                    });
                                    return (
                                        <div
                                            key={date.toLocaleString()}
                                            className={dateStyles}
                                            onClick={() => this.setSelectedDate(date)}
                                        >
                                            {date.getDate()}
                                        </div>
                                    );
                                })}
                            </div>
                        )}
                </div>
            </div>
        );
    }
}
