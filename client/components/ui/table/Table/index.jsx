import React from 'react';
import { Scrollbars } from 'react-custom-scrollbars';
import TableSettingsFieldList from '../TableSettingsFieldList';
import styles from './table.css';

/**
 * Компонент-обертка для таблицы
 */
export default function Table(props) {
    return (
        <div className={styles['table-container']}>
            {!props.noElements
                ? (
                    <div>
                        <TableSettingsFieldList
                            fieldList={props.settingsFieldList}
                            wrapperStyle={props.settingsWrapperStyle}
                            setFieldVisibility={props.setFieldVisibility}
                            fieldVisibility={props.fieldVisibility}
                        />
                        <Scrollbars
                            autoHeight
                            autoHeightMax={'none'}
                            style={{ height: 'auto', width: '100%' }}
                        >
                            <table className={styles.table}>
                                {props.children}
                            </table>
                        </Scrollbars>
                    </div>
                )
                : (
                    <h3 style={{ marginTop: 25 }}>
                        {props.noElementsText}
                    </h3>
                )}
        </div>
    );
}

Table.propTypes = {
    noElements: React.PropTypes.bool,
    settingsFieldList: React.PropTypes.array,
    settingsWrapperStyle: React.PropTypes.object,
    setFieldVisibility: React.PropTypes.func,
    fieldVisibility: React.PropTypes.object,
    children: React.PropTypes.node.isRequired,
    noElementsText: React.PropTypes.string,
};

Table.defaultProps = {
    noElements: false,
    settingsFieldList: [],
    settingsWrapperStyle: {},
    setFieldVisibility: undefined,
    fieldVisibility: {},
    noElementsText: 'Данные отсутствуют',
};
