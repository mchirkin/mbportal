import React from 'react';
import styles from './table.css';

/**
 * Компонент для выделения группы в таблице
 */
export default function TableGroup(props) {
    return (
        <tr className={styles['table-group']}>
            <td colSpan={props.colSpan}>
                <span className={styles['group-name']}>{props.groupName}</span>
            </td>
            {props.children}
        </tr>
    );
}

/**
 * propTypes
 */
TableGroup.propTypes = {
    colSpan: React.PropTypes.number,
    groupName: React.PropTypes.string.isRequired,
    children: React.PropTypes.node,
};

TableGroup.defaultProps = {
    colSpan: 1,
    children: null,
};
