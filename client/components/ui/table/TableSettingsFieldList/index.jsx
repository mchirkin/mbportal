import React from 'react';
import classNames from 'classnames/bind';
import { findTargetInChildNode } from 'utils/dom';
import CheckBox from 'components/ui/CheckBox';
import styles from './table.css';

const cx = classNames.bind(styles);

/**
 * Компонент для настройки видимости полей таблицы
 */
export default class TableSettingsFieldList extends React.Component {
    constructor(props) {
        super(props);
        /**
         * @property {boolean} fieldListVisible - видимость блока
         */
        this.state = {
            fieldListVisible: false,
        };
    }

    componentDidMount() {
        document.querySelector('body').addEventListener('click', (e) => {
            if (!findTargetInChildNode(this.settingsWrapper, e.target)) {
                this.closeFieldList();
            }
        });
    }

    /**
     * Обработчик нажатия на иконку шестеренки
     * @property {Event} e
     */
    handleBtnClick = (e) => {
        e.stopPropagation();
        this.setState({
            fieldListVisible: !this.state.fieldListVisible,
        });
    }

    /**
     * Обработчик изменения значения чекбокса видимости поля
     * @property {Event} e
     */
    handleCheckboxChange = (e) => {
        this.props.setFieldVisibility({
            field: e.id,
            visible: e.value,
        });
    }

    /**
     * Закрыть блок
     */
    closeFieldList = () => {
        if (this.settingsWrapper) {
            this.setState({
                fieldListVisible: false,
            });
        }
    }

    render() {
        const listClasses = cx({
            'field-list-container': true,
            'field-list-container--visible': this.state.fieldListVisible,
        });

        return (
            <div
                className={styles['settings-wrapper']}
                ref={(settingsWrapper) => { this.settingsWrapper = settingsWrapper; }}
                style={this.props.wrapperStyle}
            >
                <div className={styles['settings-btn']} onClick={this.handleBtnClick} />
                <div className={listClasses}>
                    <div className={styles['close-btn']} onClick={this.closeFieldList}>&times;</div>
                    <ul className={styles['field-list']}>
                        {this.props.fieldList.map(field => (
                            <li key={field.id}>
                                <CheckBox
                                    handleChange={this.handleCheckboxChange}
                                    id={field.id}
                                    value={this.props.fieldVisibility[field.id]}
                                />
                                <div>{field.name}</div>
                            </li>
                        ))}
                    </ul>
                </div>
            </div>
        );
    }
}

/**
 * propTypes
 * @property {Object} wrapperStyle - стили для div'а обертки
 * @property {Array} fieldList - массив полей таблицы для настройки видимости
 * @property {Object} fieldVisibility - настройки видимости полей
 * @property {Function} setFieldVisibility - функция для установки видимости поля
 */
TableSettingsFieldList.propTypes = {
    wrapperStyle: React.PropTypes.object,
    fieldList: React.PropTypes.array.isRequired,
    fieldVisibility: React.PropTypes.object.isRequired,
    setFieldVisibility: React.PropTypes.func.isRequired,
};

TableSettingsFieldList.defaultProps = {
    wrapperStyle: {},
};
