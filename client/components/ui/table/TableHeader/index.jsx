import React from 'react';
import styles from './table.css';

/**
 * Компонент-обертка для шапки таблицы
 */
export default function TableHeader(props) {
    return (
        <thead className={styles['table-header']}>
            <tr>
                {props.children}
            </tr>
        </thead>
    );
}

/**
 * propTypes
 */
TableHeader.propTypes = {
    children: React.PropTypes.node.isRequired,
};
