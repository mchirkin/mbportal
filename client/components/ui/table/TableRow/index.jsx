import React from 'react';
import PropTypes from 'prop-types';
import styles from './table.css';

/**
 * Компонент-обертка для записи в таблице
 */
export default function TableRow(props) {
    return (
        <tr
            className={styles['table-row']}
            onClick={props.rowClickHandler}
            onDoubleClick={props.rowDblClickHandler}
            data-clickable={props.clickable}
        >
            {props.children}
        </tr>
    );
}

TableRow.propTypes = {
    /** Обработчик нажатия на запись в таблице */
    rowClickHandler: PropTypes.func,
    /** Обработчик двойного нажатия на запись в таблице */
    rowDblClickHandler: PropTypes.func,
    /** Можно ли нажать на запись в таблице */
    clickable: PropTypes.bool,
    children: PropTypes.node.isRequired,
};

TableRow.defaultProps = {
    rowClickHandler: undefined,
    rowDblClickHandler: undefined,
    clickable: false,
};
