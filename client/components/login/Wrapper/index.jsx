import React from 'react';
import PropTypes from 'prop-types';
import { Scrollbars } from 'react-custom-scrollbars';
import BlockLoader from 'components/BlockLoader';
import Banner from '../Banner';
import ProductsMenu from '../ProductsMenu';
import Footer from '../Footer';
import styles from './login.css';

export default function Wrapper({ children, isFetching, large, style }) {
    return (
        <Scrollbars
            style={{
                width: '100%',
                backgroundColor: '#f9f9f9',
            }}
        >
            <div className={styles.wrapper} style={{ minHeight: large ? 1060 : 700, ...style }}>
                <Banner />
                <div className={styles['main-content']}>
                    <div className={styles['content-wrapper']}>
                        <BlockLoader isFetching={isFetching} />
                        {children}
                        <ProductsMenu />
                    </div>
                    <Footer />
                </div>
            </div>
        </Scrollbars>
    );
}

Wrapper.propTypes = {
    children: PropTypes.node.isRequired,
    isFetching: PropTypes.bool,
    large: PropTypes.bool,
    style: PropTypes.object,
};

Wrapper.defaultProps = {
    isFetching: false,
    large: false,
    style: {},
};
