import React, { Component } from 'react';
import { findTargetInChildNode } from 'utils/dom';
import styles from './login.css';

export default class ProductsMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: false,
        };
    }

    componentDidMount() {
        document.addEventListener('click', this.handleOutsideClick);
    }

    componentWillUnmount() {
        document.removeEventListener('click', this.handleOutsideClick);
    }

    handleOutsideClick = (e) => {
        if (!findTargetInChildNode(this.wrapper, e.target)) {
            this.setState({
                visible: false,
            });
        }
    }

    handleMenuBtnClick = () => {
        this.setState({
            visible: !this.state.visible,
        });
    }

    render() {
        return (
            <div className={styles.wrapper} ref={(c) => { this.wrapper = c; }}>
                <div
                    className={styles['menu-btn']}
                    onClick={this.handleMenuBtnClick}
                    data-active={this.state.visible}
                >
                    <div />
                    <div />
                    <div />
                </div>
                <div className={styles.menu} data-visible={this.state.visible}>
                    <a href="https://b2b.rosevrobank.ru">
                        <div className={styles['menu-item']}>
                            <div className={styles['menu-item-name']}>
                                Банковские гарантии
                            </div>
                            <div className={styles['menu-item-desc']}>
                                Личный кабинет агентов. Оформление и получение банковских гарантий
                            </div>
                        </div>
                    </a>
                    <a href="https://rko.rosevrobank.ru">
                        <div className={styles['menu-item']}>
                            <div className={styles['menu-item-name']}>
                                Личный кабинет для юр. лиц
                            </div>
                            <div className={styles['menu-item-desc']}>
                                Открытие счета для бизнеса
                            </div>
                        </div>
                    </a>
                    <div className={`${styles['menu-item']} ${styles['menu-item--active']}`}>
                        <div className={styles['menu-item-name']}>
                            Интернет-банк для бизнеса
                        </div>
                        <div className={styles['menu-item-desc']}>
                            Платежи и управление счетами для бизнеса
                        </div>
                    </div>
                    <a href="https://vb.rosevrobank.ru">
                        <div className={styles['menu-item']}>
                            <div className={styles['menu-item-name']}>
                                Виртуальный офис
                            </div>
                            <div className={styles['menu-item-desc']}>
                                Платежи, переводы, управление счетами для физических лиц
                            </div>
                        </div>
                    </a>
                    <a href="https://www.rosevrobank.ru">
                        <div className={styles['menu-item']}>
                            <div className={styles['menu-item-name']}>
                                Сайт Банка
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        );
    }
}
