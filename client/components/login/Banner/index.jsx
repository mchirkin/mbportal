import React from 'react';
import styles from './login.css';

/**
 * Shuffles array in place.
 * @param {Array} a items An array containing the items.
 */
function shuffle(a) {
    const b = a;
    for (let i = b.length - 1; i > 0; i -= 1) {
        const j = Math.floor(Math.random() * (i + 1));
        [b[i], b[j]] = [b[j], b[i]];
    }
    return b;
}

const BannerItem = ({ imgSrc, title, description, link }) => (
    <div className={styles.banner__item}>
        <div className={styles.banner__background}>
            <img
                className={styles['banner__background-source']}
                alt="Фоновая картинка баннера"
                src={imgSrc}
            />
        </div>
        <div className={styles.banner__content}>
            <a
                className={styles.banner__title}
                href={link}
                target="_blank"
            >
                {title}
            </a>
            <a
                className={styles.banner__description}
                href={link}
                target="_blank"
            >
                {description}
            </a>
            <a
                className={styles.banner__button}
                href={link}
                target="_blank"
            >
                Подробнее
            </a>
        </div>
    </div>
);

BannerItem.propTypes = {
    imgSrc: React.PropTypes.string.isRequired,
    title: React.PropTypes.string.isRequired,
    description: React.PropTypes.string.isRequired,
    link: React.PropTypes.string.isRequired,
};

const bannerArray = [
    <BannerItem
        imgSrc={require('./img/93.jpg')}
        title="Кредиты малому бизнесу"
        description={'Без залога. \n Решение за 15 минут.'}
        link="https://www.rosevrobank.ru/msb/kredity-na-razvitie-biznesa/"
        key="kredityNaRazvitieBiznesa"
    />,
    <BannerItem
        imgSrc={require('./img/94.jpg')}
        title="Кредиты участникам госзакупок"
        description={'Кредиты участникам госзакупок. \n Без залога. \n Выдача за 3 дня.'}
        link="https://www.rosevrobank.ru/msb/uchastnikam-gos-zakupok/kredit-na-ispolnenie-kontrakta/"
        key="kreditNaIspolnenieKontrakta"
    />,
    <BannerItem
        imgSrc={require('./img/92.jpg')}
        title="Банковские гарантии"
        description={'Для участников закупок по ФЗ-223 и ФЗ-44. \n Решение за 15 минут.'}
        link="https://www.rosevrobank.ru/msb/uchastnikam-gos-zakupok/"
        key="uchastnikamGosZakupok"
    />,
];

const banners = shuffle(bannerArray);

const Banner = () => (
    <div className={styles.banner}>
        {banners}
    </div>
);

export default Banner;
