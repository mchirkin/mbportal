import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import styles from './styles.css';

export default class UserBlocked extends Component {
    static propTypes = {
        error: PropTypes.object,
    };

    static defaultProps = {
        error: {},
    };

    render() {
        const getLoginEnable = this.props.error.blockLevel && parseInt(this.props.error.blockLevel, 10) < 3;

        return (
            <div className={styles['modal-wrapper']}>
                <div className={styles.modal}>
                    <div className={styles['form-header']}>
                        <div className={styles['reb-logo']} />
                        <div className={styles['product-name']}>
                            Интернет-банк для бизнеса
                        </div>
                    </div>
                    <h3 className={styles['modal-header']}>
                        {getLoginEnable
                            ? 'Ваш аккаунт временно заблокирован!'
                            : 'Ваш аккаунт заблокирован!'}
                    </h3>
                    {getLoginEnable
                        ? (
                            <Link className={styles['get-login']} to="get_login">
                                <div className={styles['get-login-btn']}>
                                    Восстановить логин и пароль
                                </div>
                            </Link>
                        )
                        : (
                            <div className={styles.message}>
                                <div>
                                    Пожалуйста свяжитесь со службой поддержки
                                </div>
                                <div>
                                    <strong>8 (800) 500-77-78, 8 (495) 777 1111</strong>
                                </div>
                            </div>
                        )}
                </div>
            </div>
        );
    }
}
