import React from 'react';
import styles from './login.css';

export default function Footer() {
    return (
        <div className={styles.footer}>
            <div className={styles['footer-content']}>
                <div className={styles['bank-phones']}>
                    <div>
                        Бесплатно по России<br />
                        8 (800) 500-77-78
                    </div>
                    <div>
                        Для звонков из Москвы<br />
                        8 (495) 777-11-11
                    </div>
                </div>
                <div className={styles['mb-info']}>
                    <a
                        href="https://www.rosevrobank.ru/msb/mobilnyy-bank/"
                        target="blank"
                        className={styles['mb-info__link']}
                    >
                        Мобильный банк
                    </a>
                    <div className={styles['mb-info__content']}>
                        <p className={styles['mb-info__title']}>
                            Мобильный банк для бизнеса
                        </p>
                        <div className={styles['mb-info__buttons']}>
                            <a
                                href="https://itunes.apple.com/ru/app/бизнес/id1329515308?mt=8"
                                target="blank"
                                className={`${styles['mb-info__button']} ${styles['mb-info__button_appstore']}`}
                            >
                                Скачайте в App Store
                            </a>
                            <a
                                href="https://play.google.com/store/apps/details?id=ru.rosevrobank.business"
                                target="blank"
                                className={`${styles['mb-info__button']} ${styles['mb-info__button_gplay']}`}
                            >
                                Скачайте в Google play
                            </a>
                        </div>
                    </div>

                </div>
                {/* <div className={styles['social-links']}>
                    <div>
                        Мы в социальных сетях
                    </div>
                    <div className={styles['social-icons']}>
                        <a
                            href="https://www.facebook.com/rosevrobank?fref=ts"
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            <div className={styles.fb} />
                        </a>
                        <a
                            href="https://twitter.com/RosEvroBankRu"
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            <div className={styles.tw} />
                        </a>
                        <a
                            href="https://vk.com/roseurobank_ru"
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            <div className={styles.vk} />
                        </a>
                    </div>
                </div> */}
                <div className={styles['bank-links']}>
                    <div>
                        <a href="https://www.rosevrobank.ru">
                            Главная страница Банка
                        </a>
                    </div>
                    <div>
                        <a href="https://vb.rosevrobank.ru">
                            Виртуальный Офис
                        </a>
                    </div>
                </div>
                <div className={styles.license}>
                    <div>© РосЕвроБанк 1994-2017</div>
                    <div>
                        Генеральная лицензия Банка<br />
                        России № 3137
                    </div>
                </div>
            </div>
        </div>
    );
}
