import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { CSSTransition } from 'react-transition-group';
import styles from './kontur.css';

const KonturFocusModal = ({
    factsColor,
    facts,
    inn,
    ogrn,
    nodata,
}) => {
    let colorText;
    switch (factsColor) {
        case 'red':
            colorText = 'найдены красные факты';
            break;
        case 'yellow':
            colorText = 'найдены желтые факты';
            break;
        case 'green':
            colorText = 'найдены зеленые факты';
            break;
        case 'grey':
            colorText = 'существенных фактов не найдено';
            break;
        default:
            colorText = 'информация отсутствует';
    }

    if (nodata) {
        colorText = 'информация отсутстует';
    }

    return (
        <CSSTransition
            timeout={0}
            key="kontur"
            classNames="fadeappear"
            appear
            in
        >
            <div className={styles['kontur-modal-wrapper']} data-color={factsColor}>
                {
                    factsColor === 'red' || factsColor === 'yellow'
                        ? (
                            <div className={styles.attention}>
                                Внимание!
                            </div>
                        )
                        : null
                }
                <div className={styles['kontur-text']}>
                    По фирме{inn ? ` ИНН ${inn}` : ''}{ogrn ? ` ОГРН ${ogrn}` : ''} {colorText}
                </div>
                <div className={styles['kontur-facts-list']}>
                    {
                        facts.map((fact, i) => (
                            <div key={i} className={styles['kontur-fact']}>
                                &bull; {fact.statement}
                            </div>
                        ))
                    }
                </div>
            </div>
        </CSSTransition>
    );
};

KonturFocusModal.propTypes = {
    factsColor: PropTypes.string,
    facts: PropTypes.array,
    inn: PropTypes.string,
    ogrn: PropTypes.string,
    nodata: PropTypes.bool,
};

KonturFocusModal.defaultProps = {
    factsColor: undefined,
    facts: [],
    inn: undefined,
    ogrn: undefined,
    nodata: false,
};

export default KonturFocusModal;
