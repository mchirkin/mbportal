import React from 'react';
import { render } from 'react-dom';

/**
 * Компонент для рендеринга в body
 */
export default class MountToBody extends React.Component {
    componentWillMount() {
        this.div = document.createElement('div');
        if (this.props.id) {
            this.div.setAttribute('id', this.props.id);
        }
        document.body.appendChild(this.div);
    }

    componentDidMount() {
        this.renderComponent();
    }

    componentDidUpdate() {
        this.renderComponent();
    }

    /**
     * Рендеринг переданного компонента
     */
    renderComponent() {
        const markup = (
            <div>
                {this.props.children}
            </div>
        );
        render(markup, this.div);
    }

    render() {
        return null;
    }
}

/**
 * propTypes
 * @property {string} id - идентификатор монтируемого компонента
 */
MountToBody.propTypes = {
    id: React.PropTypes.string.isRequired,
    children: React.PropTypes.node,
};

MountToBody.defaultProps = {
    children: null,
};
