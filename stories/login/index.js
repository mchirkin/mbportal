import React from 'react';
import { storiesOf } from '@storybook/react';
import addWithInfo from '../add_with_info';

import PasswordChangedMessage from '../../client/routes/login/components/PasswordChangedMessage';
import PasswordExpiredMessage from '../../client/routes/login/components/PasswordExpiredMessage';
import UserBlocked from '../../client/routes/login/components/UserBlocked';

storiesOf('Login/PasswordChangedMessage', module)
    .add(
        'usage',
        addWithInfo({
            text: 'Сообщение об успешности изменения пароля',
            content: (
                <PasswordChangedMessage />
            ),
        })
    );

storiesOf('Login/PasswordExpiredMessage', module)
    .add(
        'usage',
        addWithInfo({
            text: 'Сообщение о просроченном пароле',
            content: (
                <PasswordExpiredMessage
                    hideMessage={() => { console.log('Закрыто'); }}
                />
            ),
        })
    );

storiesOf('Login/UserBlocked', module)
    .add(
        'Временная блокировка',
        addWithInfo({
            text: 'Сообщение о блокировке пользователя',
            content: (
                <UserBlocked
                    error={{
                        blockLevel: '1',
                    }}
                />
            ),
        })
    )
    .add(
        'Постоянная блокировка',
        addWithInfo({
            text: 'Сообщение о блокировке пользователя',
            content: (
                <UserBlocked />
            ),
        })
    );
