import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';

import './ui';
import './login';
import './main_page';
import './layout';
import './home';
