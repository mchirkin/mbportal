import React from 'react';
import { withInfo } from '@storybook/addon-info';

export default function addWithInfo({
    text,
    content,
    components = [],
    exclude = []
}) {
    return withInfo({
        header: true,
        inline: true,
        source: true,
        styles: {},
        propTables: components,
        propTablesExclude: exclude,
        maxPropsIntoLine: 1,
        text,
    })(() => (
        <div
            style={{
                display: 'flex',
                alignItems: 'flex-start',
                padding: 20,
            }}
        >
            {content}
        </div>
    ));
}
