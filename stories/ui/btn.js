import React from 'react';
import { storiesOf } from '@storybook/react';
import addWithInfo from '../add_with_info';

import Btn from '../../client/components/ui/Btn';

storiesOf('UI/Btn', module)
    .add(
        'usage',
        addWithInfo({
            text: 'Кнопка',
            content: [
                <Btn
                    caption="Заплатить"
                    bgColor="gradient"
                    style={{ width: 390, marginRight: 10 }}
                />,
                <Btn
                    caption="Заплатить"
                    bgColor="white"
                    style={{ width: 140 }}
                />,
            ],
        })
    );
