import React, { Component } from 'react';
import { storiesOf } from '@storybook/react';
import addWithInfo from '../add_with_info';

import InputGroup from '../../client/components/ui/InputGroup';

class TestContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: null,
        };
    }

    handleInputChange = (e) => {
        this.setState({
            value: e.target.value,
        });
    }

    render() {
        return (
            <InputGroup
                id="test-input"
                caption="Наименование получателя"
                value={this.state.value}
                onChange={this.handleInputChange}
                maxLength={160}
                showSymbolCounter
            />
        );
    }
}

storiesOf('UI/Input', module)
    .add(
        'usage',
        addWithInfo({
            text: 'Поле ввода',
            content: (
                <TestContainer />
            ),
            components: [
                InputGroup,
            ],
            exclude: [
                TestContainer,
            ],
        })
    )
    .add(
        'with hint',
        addWithInfo({
            text: 'Поле ввода c подсказкой',
            content: [
                <InputGroup
                    id="test-input"
                    caption="Наименование получателя"
                    hint={'Тут будет текст подсказки\nС описанием того, зачем сие поле нужно'}
                    style={{
                        width: 400,
                        marginRight: 20,
                    }}
                />,
                <InputGroup
                    id="test-input"
                    caption="Наименование получателя"
                    hint={'Тут будет текст подсказки\nС описанием того, зачем сие поле нужно'}
                    hintNoAnimation
                    style={{
                        width: 400,
                    }}
                />
            ],
            components: [
                InputGroup,
            ],
            exclude: [
                TestContainer,
            ],
        })
    );
