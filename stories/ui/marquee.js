import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';

import Marquee from '../../client/components/ui/Marquee';

storiesOf('UI/Marquee', module)
    .add('usage', withInfo({
        header: true,
        inline: true,
        source: true,
        text: 'Компонент для бегущей строки'
    })(() => (
        <div>
            <div style={{'maxWidth': 100}}>
                <Marquee text='Test'/>
                <Marquee text='Test asda sdas das das dasd asdasd asdaksdh ajksdh akjsd hjkasdh asjjj'/>
                <Marquee text='Test asda sdas das das'/>
                <Marquee text='Test asda sdas das das dasd asdasd asdaksdh ajksdh akjsd hjkasdh asjjj Test asda sdas das das '/>
                <Marquee text='Test asda sdas das das dasd asdasd asdaksdh ajksdh akjsd hjkasdh asjjj  Test asda sdas das das dasd asdasd asdaksdh ajksdh akjsd hjkasdh asjjj Test asda sdas das das dasd asdasd asdaksdh ajksdh akjsd hjkasdh asjjj'/>
            </div>
            <div style={{'maxWidth': 400}}>
                <Marquee text='Test'/>
                <Marquee text='Test asda sdas das das dasd asdasd asdaksdh ajksdh akjsd hjkasdh asjjj'/>
                <Marquee text='Test asda sdas das das'/>
                <Marquee text='Test asda sdas das das dasd asdasd asdaksdh ajksdh akjsd hjkasdh asjjj Test asda sdas das das '/>
                <Marquee text='Test asda sdas das das dasd asdasd asdaksdh ajksdh akjsd hjkasdh asjjj  Test asda sdas das das dasd asdasd asdaksdh ajksdh akjsd hjkasdh asjjj Test asda sdas das das dasd asdasd asdaksdh ajksdh akjsd hjkasdh asjjj'/>
            </div>
        </div>
    )));
