import React, { Component } from 'react';
import { storiesOf } from '@storybook/react';
import addWithInfo from '../add_with_info';

import InputTextarea from '../../client/components/ui/InputTextarea';

class TestContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: null,
        };
    }

    handleInputChange = (e) => {
        this.setState({
            value: e.target.value,
        });
    }

    render() {
        return (
            <InputTextarea
                id="test-textarea"
                caption="Назначение платежа"
                value={this.state.value}
                onChange={this.handleInputChange}
                maxLength={210}
                showSymbolCounter
            />
        );
    }
}

storiesOf('UI/Textarea', module)
    .add(
        'usage',
        addWithInfo({
            text: 'Компонент для ввода текста в textarea',
            content: (
                <TestContainer />
            ),
            components: [
                InputTextarea,
            ],
            exclude: [
                TestContainer,
            ],
        })
    );
