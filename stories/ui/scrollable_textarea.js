import React, { Component } from 'react';
import { storiesOf } from '@storybook/react';
import addWithInfo from '../add_with_info';

import ScrollableTextarea from '../../client/components/ui/ScrollableTextarea';

class TestContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: '',
        };
    }

    handleInputChange = (e) => {
        console.log(e.target, e.target.innerText);
        this.setState({
            value: e.target.innerText,
        });
    }

    render() {
        return (
            <ScrollableTextarea
                id="test-textarea"
                caption="Назначение платежа"
                value={this.state.value}
                onChange={this.handleInputChange}
            />
        );
    }
}

storiesOf('UI/ScrollableTextarea', module)
    .add(
        'usage',
        addWithInfo({
            text: 'Компонент для ввода текста в textarea',
            content: (
                <TestContainer />
            ),
            components: [
                ScrollableTextarea,
            ],
            exclude: [
                TestContainer,
            ],
        })
    );
