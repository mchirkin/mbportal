import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';

import Button from '../../client/components/ui/Button';

storiesOf('UI/Button', module)
    .add('simple usage', withInfo({
        header: true,
        inline: true,
        source: true,
        styles: {},
        maxPropsIntoLine: 1,
        text: 'Компонент для отображения кнопки'
    })(() => (
        <div style={{ display: 'flex', marginTop: 20, }}>
            <Button
                caption="Сохранить"
                styles={{
                    width: 220,
                    marginRight: 20,
                }}
            />
            <Button
                caption="Отмена"
                color="white"
            />
        </div>
    )));
