import React, { Component } from 'react';
import { storiesOf } from '@storybook/react';
import addWithInfo from '../add_with_info';

import InputSelect from '../../client/components/ui/InputSelect';

class TestContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedValue: null,
        };
    }

    handleSelectChange = (value) => {
        this.setState({
            selectedValue: value.el,
        });
    }

    render() {
        const { editable } = this.props;

        const options = [
            {
                value: 'Тест 1',
            },
            {
                value: 'Тест 2',
            },
            {
                value: 'Тест 3',
            },
        ];

        return (
            <InputSelect
                id="test-select"
                caption="Выпадающий список"
                options={options}
                value={this.state.selectedValue}
                onChange={this.handleSelectChange}
                editable={editable}
            />
        );
    }
}

storiesOf('UI/Select', module)
    .add(
        'usage',
        addWithInfo({
            text: 'Выпадающий список',
            content: (
                [
                    <TestContainer editable={false} />,
                    <TestContainer editable={true} />,
                ]
            ),
            components: [
                InputSelect,
            ],
            exclude: [
                TestContainer,
            ],
        })
    );
