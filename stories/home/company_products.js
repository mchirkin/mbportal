import React from 'react';
import { storiesOf } from '@storybook/react';
import addWithInfo from '../add_with_info';

import CompanyProducts from '../../client/scenes/Home/components/CompanyProducts';

import { orgInfo, accounts, cards } from '../example_data';

storiesOf('Home/CompanyProducts', module)
    .add(
        'usage',
        addWithInfo({
            text: 'Блок с продуктами организации',
            content: (
                <CompanyProducts
                    orgInfo={orgInfo}
                    accounts={accounts}
                    cards={cards}
                />
            ),
        })
    );
