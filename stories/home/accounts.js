import React from 'react';
import { storiesOf } from '@storybook/react';
import addWithInfo from '../add_with_info';

import Accounts from '../../client/scenes/Home/components/Accounts';

import { accounts, cards } from '../example_data';

storiesOf('Home/Accounts', module)
    .add(
        'usage',
        addWithInfo({
            text: 'Список счетов и карт',
            content: (
                <Accounts accounts={accounts} cards={cards} />
            ),
        })
    );
