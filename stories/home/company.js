import React from 'react';
import { storiesOf } from '@storybook/react';
import addWithInfo from '../add_with_info';

import Company from '../../client/scenes/Home/components/Company';

import { orgInfo } from '../example_data';

storiesOf('Home/Company', module)
    .add(
        'usage',
        addWithInfo({
            text: 'Название организации',
            content: (
                <Company data={orgInfo} />
            ),
        })
    );
