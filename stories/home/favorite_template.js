import React from 'react';
import { storiesOf } from '@storybook/react';
import addWithInfo from '../add_with_info';

import FavoriteTemplate from '../../client/scenes/Home/components/FavoriteTemplate';

storiesOf('Home/FavoriteTemplate', module)
    .add(
        'usage',
        addWithInfo({
            text: 'Избранный шаблон платежа',
            content: (
                <FavoriteTemplate
                    data={{
                        name: 'Платеж в налоговую',
                    }}
                />
            ),
        })
    );
