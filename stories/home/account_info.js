import React from 'react';
import { storiesOf } from '@storybook/react';
import addWithInfo from '../add_with_info';

import AccountInfo from '../../client/scenes/Home/components/AccountInfo';

storiesOf('Home/AccountInfo', module)
    .add(
        'usage',
        addWithInfo({
            text: 'Информация по счету',
            content: (
                <AccountInfo />
            ),
        })
    );
