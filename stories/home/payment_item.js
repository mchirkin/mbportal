import React from 'react';
import { storiesOf } from '@storybook/react';
import addWithInfo from '../add_with_info';

import PaymentItem from '../../client/scenes/Home/components/Payments/components/PaymentItem';

storiesOf('Home/PaymentItem', module)
    .add(
        'usage',
        addWithInfo({
            text: 'Платеж',
            content: (
                <div style={{ width: '100%', }}>
                    <PaymentItem
                        data={{
                            "acc_number": "40702810900100430859",
                            "amount": "1670000.00",
                            "bank_bik": "044525836",
                            "bank_corr_account": "30101810445250000836",
                            "bank_name": "АКБ \"РосЕвроБанк\" (АО) г.Москва",
                            "branch_id": "4",
                            "corr_acc_number": "40702810600100150154",
                            "corr_bank_bik": "044525836",
                            "corr_bank_corr_account": "30101810445250000836",
                            "corr_bank_name": "АКБ \"РосЕвроБанк\" (АО) г.Москва",
                            "corr_fullname": "ООО \"СК Лабиринт\"",
                            "corr_inn": "7731658253",
                            "corr_kpp": "773101001",
                            "curr_code": "810",
                            "curr_code_iso": "RUR",
                            "doc_date": "2016-08-03",
                            "doc_number": "5545",
                            "fullname": "ООО \"ТД Лабиринт\"",
                            "id": "22451",
                            "inn": "7721279665",
                            "kpp": "772101001",
                            "line_amount_literaly": "Один миллион шестьсот семьдесят тысяч рублей 00 копеек",
                            "line_description": "Оплата за аренду нежилых помещений по договору  аренды ТД/15 от 01.10.2015г. В том числе НДС18% - 254745-76",
                            "line_is_debet": "0",
                            "line_operation_kind": "01",
                            "line_payment_urgent": "5",
                            "line_send_type": "электронно",
                            "line_tax": "",
                            "line_value_date": "2016-08-03"
                        }}
                    />
                    <PaymentItem
                        data={{
                            "acc_number": "40702810600100150154",
                            "amount": "1596600.00",
                            "bank_bik": "044525836",
                            "bank_corr_account": "30101810445250000836",
                            "bank_name": "АКБ \"РосЕвроБанк\" (АО) г.Москва",
                            "branch_id": "4",
                            "corr_acc_number": "40702810638110021005",
                            "corr_bank_bik": "044525225",
                            "corr_bank_corr_account": "30101810400000000225",
                            "corr_bank_name": "ПАО СБЕРБАНК Г МОСКВА",
                            "corr_fullname": "ООО \"Серпуховской Двор - II\"",
                            "corr_inn": "7725817787",
                            "corr_kpp": "772501001",
                            "curr_code": "810",
                            "curr_code_iso": "RUR",
                            "doc_date": "2016-08-03",
                            "doc_number": "139",
                            "fullname": "ООО \"СК Лабиринт\"",
                            "id": "22450",
                            "inn": "7731658253",
                            "kpp": "773101001",
                            "line_amount_literaly": "Один миллион пятьсот девяносто шесть тысяч шестьсот рублей 00 копеек",
                            "line_description": "Оплата по сч. 2231 от 06.07.2016г. арендная плата с 01.08.16 по 31.08.16 по дог. 55 от 01.03.2016г.    В том числе НДС18% - 243549.15",
                            "line_is_debet": "1",
                            "line_operation_kind": "01",
                            "line_payment_urgent": "5",
                            "line_send_type": "электронно",
                            "line_tax": "",
                            "line_value_date": "2016-08-03"
                        }}
                    />
                </div>
            ),
        })
    );
