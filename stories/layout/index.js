import React from 'react';
import { storiesOf } from '@storybook/react';
import addWithInfo from '../add_with_info';

import TopBar from '../../client/components/layout/TopBar';
import BottomBar from '../../client/components/layout/BottomBar';

storiesOf('Layout/TopBar', module)
    .add(
        'usage',
        addWithInfo({
            text: 'TopBar',
            content: (
                <TopBar />
            ),
            components: [
                TopBar,
            ],
        })
    );

storiesOf('Layout/BottomBar', module)
    .add(
        'usage',
        addWithInfo({
            text: 'BottomBar',
            content: (
                <div
                    style={{
                        width: '100%',
                        height: 120,
                        paddingTop: 20,
                        backgroundColor: '#f1f5f8',
                    }}
                >
                    <BottomBar />
                </div>
            ),
            components: [
                BottomBar,
            ],
        })
    );
