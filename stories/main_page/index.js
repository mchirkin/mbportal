import React from 'react';
import { storiesOf } from '@storybook/react';
import addWithInfo from '../add_with_info';

import AllAccountsBalance from '../../client/routes/main/components/AllAccountsBalance';

storiesOf('MainPage/AllAccountsBalance', module)
    .add(
        'usage',
        addWithInfo({
            text: 'Отображение балланса на всех счетах',
            content: (
                [
                    <AllAccountsBalance key="positive" rubAmmount={(123456.78).toFixed(2)} />,
                    <AllAccountsBalance key="negative" rubAmmount={(-123456.78).toFixed(2)} />,
                ]
            ),
        })
    );
