module.exports = {
    plugins: [
        require('postcss-modules-values'),
        require('postcss-cssnext')(),
    ],
};
