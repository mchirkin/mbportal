function validateNumber(number) {
    return !/[^\d]/.test(number);
}

function validateNumberCommaList(string) {
    return /^(\d+,?)+$/.test(string);
}

function validateDate(date, format) {
    switch (format) {
        case 'dd.mm.YYYY':
            return /^\d{2}\.\d{2}\.\d{4}$/.test(date);
        case 'YYYY-mm-dd':
            return /^\d{4}-\d{2}-\d{2}$/.test(date);
        case 'YYYYmmdd':
            return /^\d{4}\d{2}\d{2}$/.test(date);
        default:
            return /^\d{2}\.\d{2}\.\d{4}$/.test(date);
    }
}

function validateAlphanum(str) {
    return !/[^\w\d\-_.,;]/.test(str);
}

module.exports = {
    number: validateNumber,
    numberCommaList: validateNumberCommaList,
    date: validateDate,
    alphanum: validateAlphanum,
};
