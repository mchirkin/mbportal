const env = require('../env.json');

const NODE_ENV = process.env.NODE_ENV || 'development';

let host;
switch (NODE_ENV) {
    case 'development':
        host = '10.1.20.100';
        break;
    case 'test':
        host = '10.1.16.100';
        break;
    case 'production':
        host = '10.1.2.181';
        break;
    default:
        host = '10.1.20.100';
}

module.exports = {
    isimple: {
        host,
        port: 8080,
    },
    session: {
        passwordForCookie: env.cookiePassword,
    },
};
