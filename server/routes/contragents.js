const requestToISimple = require('../middleware/isimple/request/request');

exports.register = (server, options, next) => {
    server.route([
        {
            method: 'GET',
            path: '/api/v1/contragent/list',
            handler(request, reply) {
                const contragentName = request.query.name ? `?name=${request.query.name}` : '';

                let params = {
                    path: encodeURI(`/rest/stateful/corp/dic/corr${contragentName}`),
                    method: 'GET',
                };
                params = Object.assign(params, request.auth);
                console.log(params);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/contragent/create',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;
                let params = {
                    path: '/rest/stateful/corp/dic/corr/v2',
                    method: 'POST',
                    postData: {
                        id: payload.id ? payload.id : '',
                        corrType: payload.inn && payload.inn.length === 12 ? 'IP' : 'UL',
                        description: payload['contragent-description'],
                        fullname: payload.contragent,
                        inn: payload.inn,
                        kpp: payload.kpp,
                        email: payload.email,
                        phone: payload.phone,
                    },
                };

                if (payload.inn && payload.inn.length === 12) {
                    params.postData.firstname = payload.contragent.replace(/^ИП\s+/, '').trim();
                }

                if (payload.account || payload.bik) {
                    params.postData.accList = [
                        {
                            id: payload['payment-id'] ? payload['payment-id'] : '',
                            accNumber: payload.account,
                            bankBik: payload.bik,
                            accDescription: payload['payment-description'],
                        },
                    ];
                }
                params = Object.assign(params, request.auth);

                if (!payload.contragent || payload.contragent.length === 0) {
                    return reply(JSON.stringify({
                        errorCode: 1008,
                        errorText: 'Не заполнено наименование получателя',
                    })).code(400).type('application/json');
                }

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/contragent/get_facts',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;
                let params = {
                    path: `/rest/stateful/corp/kontur_focus/get_facts?inn=${payload.inn}`,
                    method: 'GET',
                };

                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/contragent/send_requisites',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;
                let params = {
                    path: '/rest/stateful/corp/send/transfer_detail/product',
                    method: 'POST',
                    postData: payload,
                };

                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
    ]);

    next();
};

exports.register.attributes = {
    name: 'contragent',
};
