const crypto = require('crypto');
const ip = require('ip');

const requestToISimple = require('../middleware/isimple/request/request');

const env = require('../env.json');
const packageJSON = require('../../package.json');

const HMR = !!process.env.HMR || false;
const HOST = ip.address();

function checkAuth(request) {
    if (!/home/.test(request.path)) {
        return Promise.resolve(true);
    }

    let params = {
        path: '/rest/stateful/corp/ping',
        method: 'GET',
    };

    params = Object.assign(params, request.auth);

    return requestToISimple(params)
        .then((res) => {
            if (res.statusCode === 200) {
                return true;
            }

            return false;
        })
        .catch(() => {
            return false;
        });
}

exports.register = (server, options, next) => {
    server.route([
        {
            method: 'GET',
            path: '/{p*}',
            handler(request, reply) {
                if (HMR && /hot-update/.test(request.path)) {
                    const path = request.path.split('/').pop();
                    const uri = encodeURI(`http://${HOST}:5000/${path}`);

                    return reply.proxy({
                        uri,
                        passThrough: true,
                        xforward: true,
                    });
                }

                checkAuth(request)
                    .then((result) => {
                        if (result) {
                            const salt = crypto.randomBytes(16).toString('base64');
                            const secret = env.csrfPassword;
                            const saltedSecret = crypto.createHash('md5').update(`${salt}:${secret}`).digest('hex');
                            const csrfToken = `${salt}:${saltedSecret}`;
                            const appVersion = packageJSON.version;
                            return reply.view('index', {
                                csrfToken,
                                appVersion,
                                needStyle: !HMR,
                            });
                        }

                        return reply.redirect('/login');
                    });
            },
        },
    ]);

    next();
};

exports.register.attributes = {
    name: 'client',
};
