const moment = require('moment');

const requestToISimple = require('../middleware/isimple/request/request');

function makeArray(arr) {
    const result = Array.isArray(arr) ? arr : [arr];
    return result;
}

function prepareDataForBFM(statement, dateFrom) {
    const lines = statement.lines ? makeArray(statement.lines) : [];

    const data = {
        openingBalance: parseFloat(statement.head_opening_balance, 10),
        closeBalance: parseFloat(statement.head_closing_balance, 10),
        dates: {},
        contragents: {
            plus: {},
            minus: {},
        },
        plus: parseFloat(statement.head_credit_turnover, 10),
        minus: parseFloat(statement.head_debet_turnover, 10),
    };

    const startDate = moment(dateFrom, 'DD.MM.YYYY');

    lines.forEach((line) => {
        const date = line.line_value_date || '';
        const dateFormatted = date.replace(/^(\d{4})-(\d{2})-(\d{2})$/, '$3.$2.$1');

        const dateObject = moment(dateFormatted, 'DD.MM.YYYY');
        if (startDate > dateObject) {
            return;
        }

        let contragent = line.line_is_debet === '0' ? line.fullname : line.corr_fullname;
        contragent = contragent.replace(/^Общество с ограниченной ответственностью/i, 'ООО');
        contragent = contragent.replace(/^Закрытое акционерное общество/i, 'ЗАО');

        const lineShorted = {
            docNumber: line.doc_number,
            docDate: line.doc_date,
            description: line.line_description,
            amount: parseFloat(line.amount, 10),
        };

        if (!data.dates[dateFormatted]) {
            data.dates[dateFormatted] = {
                plus: 0,
                minus: 0,
                plusCount: 0,
                minusCount: 0,
            };
        }

        if (line.line_is_debet === '0') {
            data.dates[dateFormatted].plus += parseFloat(line.amount, 10);
            data.dates[dateFormatted].plusCount += 1;

            if (!data.contragents.plus[contragent]) {
                data.contragents.plus[contragent] = {
                    payments: [lineShorted],
                    total: parseFloat(line.amount, 10),
                };
            } else {
                data.contragents.plus[contragent].payments.push(lineShorted);
                data.contragents.plus[contragent].total += parseFloat(line.amount, 10);
            }
        } else {
            data.dates[dateFormatted].minus += parseFloat(line.amount, 10);
            data.dates[dateFormatted].minusCount += 1;

            if (!data.contragents.minus[contragent]) {
                data.contragents.minus[contragent] = {
                    payments: [lineShorted],
                    total: parseFloat(line.amount, 10),
                };
            } else {
                data.contragents.minus[contragent].payments.push(lineShorted);
                data.contragents.minus[contragent].total += parseFloat(line.amount, 10);
            }
        }
    });

    let currentBalance = data.openingBalance;
    let relativeBalance = 0;

    const dataByDates = Object.keys(data.dates)
        .sort((a, b) => moment(a, 'DD.MM.YYYY') - moment(b, 'DD.MM.YYYY'))
        .map((date) => {
            const dateData = data.dates[date];
            dateData.date = date;

            currentBalance = currentBalance + dateData.plus - dateData.minus;
            relativeBalance = relativeBalance + dateData.plus - dateData.minus;
            dateData.balance = currentBalance;
            dateData.relativeBalance = relativeBalance;

            return dateData;
        });

    data.dates = dataByDates;

    return data;
}

exports.register = (server, options, next) => {
    server.route([
        {
            method: 'POST',
            path: '/api/v1/bfm/data',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;
                const dateFrom = payload.dateFrom.replace(/^(\d+).(\d+).(\d+)$/, '$3-$2-$1');
                const dateTo = payload.dateTo.replace(/^(\d+).(\d+).(\d+)$/, '$3-$2-$1');

                let params = {
                    // eslint-disable-next-line
                    path: `/rest/stateful/corp/statement/offline/ul_get_stmt?acc_id=${payload.accountId}&begin_date=${dateFrom}&end_date=${dateTo}`,
                    method: 'GET',
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        // console.log('[BFM Statement]', res.result);

                        const statement = JSON.parse(res.result);

                        const statementAnswer = statement && statement.statementAnswer
                            ? statement.statementAnswer
                            : null;

                        if (statementAnswer === null) {
                            return reply('{ "errorCode": 1009 }').code(500).type('application/json');
                        }

                        const data = prepareDataForBFM(statementAnswer, payload.dateFrom);
                        reply(JSON.stringify(data)).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        console.log(rej);
                        reply('{ "errorCode": 1009 }').code(500).type('application/json');
                    });
            },
        },
    ]);

    next();
};

exports.register.attributes = {
    name: 'bfm',
};
