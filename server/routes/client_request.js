const requestToISimple = require('../middleware/isimple/request/request');
const getBranch = require('../middleware/isimple/request/get_branch');

exports.register = (server, options, next) => {
    server.route([
        {
            method: 'GET',
            path: '/api/v1/client_request/types',
            handler(request, reply) {
                let params = {
                    path: '/rest/stateful/corp/client_request/types',
                    method: 'GET',
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/client_request/callback/create',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;
                let params = {
                    path: '/rest/stateful/corp/client_request',
                    method: 'POST',
                    postData: {},
                };
                params = Object.assign(params, request.auth);

                getBranch(request.auth).then((branch) => {
                    let typeId;
                    switch (process.env.NODE_ENV) {
                        case 'development':
                            typeId = '1315863';
                            break;
                        case 'test':
                            typeId = '';
                            break;
                        case 'production':
                            typeId = '';
                            break;
                        default:
                            typeId = '1315863';
                    }
                    params.postData = {
                        requestBranchId: branch.id,
                        place: branch.code,
                        typeId,
                        paramValues: [
                            {
                                key: 'FIO',
                                value: payload.fio,
                            },
                            {
                                key: 'phone',
                                value: payload.phone,
                            },
                            {
                                key: 'theme',
                                value: payload.theme,
                            },
                            {
                                key: 'org_name',
                                value: payload.orgName,
                            },
                        ],
                    };
                    requestToISimple(params)
                        .then((res) => {
                            reply(res.result).code(res.statusCode).type('application/json');
                        })
                        .catch((rej) => {
                            reply(rej.toString());
                        });
                });
            },
        },
    ]);

    next();
};

exports.register.attributes = {
    name: 'client_request',
};
