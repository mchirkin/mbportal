const fetch = require('isomorphic-fetch');

const NODE_ENV = process.env.NODE_ENV || 'development';

const apiUrl = NODE_ENV === 'production'
    ? 'https://edpa.roseurobank.ru/prod/cms/mb-api'
    : 'http://10.1.20.240/mb-api';

exports.register = (server, options, next) => {
    server.route([
        {
            method: 'GET',
            path: '/api/v1/dictionary/nds/list',
            handler(request, reply) {
                fetch(`${apiUrl}/api/dictionary/nds/list`)
                    .then(response => response.json())
                    .then((json) => {
                        reply(JSON.stringify(json)).code(200).type('application/json');
                    })
                    .catch((err) => {
                        console.log(err);
                        reply('{ "success": "false" }').code(200).type('application/json');
                    });
            },
        },
        {
            method: 'GET',
            path: '/api/v1/dictionary/okei/list',
            handler(request, reply) {
                const search = request.query && request.query.search ? request.query.search : '';

                fetch(encodeURI(`${apiUrl}/api/dictionary/okei/list?search=${search}`))
                    .then(response => response.json())
                    .then((json) => {
                        reply(JSON.stringify(json)).code(200).type('application/json');
                    })
                    .catch((err) => {
                        console.log(err);
                        reply('{ "success": "false" }').code(200).type('application/json');
                    });
            },
        },
    ]);

    next();
};

exports.register.attributes = {
    name: 'dictionary',
};
