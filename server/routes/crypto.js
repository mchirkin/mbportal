const xss = require('xss');
const requestToISimple = require('../middleware/isimple/request/request');
const validate = require('../utils/validate');

exports.register = (server, options, next) => {
    server.route([
        {
            method: 'GET',
            path: '/api/v1/crypto/certificate',
            handler(request, reply) {
                let params = {
                    path: '/rest/stateful/corp/crypto/certificate',
                    method: 'GET',
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then(res => {
                        reply(res.result)
                            .code(res.statusCode)
                            .type('application/json');
                    })
                    .catch(rej => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'GET',
            path: '/api/v1/crypto/certificate/{id}',
            handler(request, reply) {
                if (!validate.number(request.params.id)) {
                    return reply(
                        JSON.stringify({
                            errorCode: '400',
                            errorText: 'Invalid payload',
                        })
                    ).code(400);
                }
                let params = {
                    path: `/rest/stateful/corp/crypto/certificate/${
                        request.params.id
                    }`,
                    method: 'GET',
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then(res => {
                        const result = Buffer.from(
                            res.result,
                            'hex'
                        ).toString();
                        reply(result)
                            .code(res.statusCode)
                            .type('text/plain');
                    })
                    .catch(rej => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'GET',
            path: '/api/v1/crypto/certificate/params/{id}',
            handler(request, reply) {
                if (!validate.number(request.params.id)) {
                    return reply(
                        JSON.stringify({
                            errorCode: '400',
                            errorText: 'Invalid payload',
                        })
                    ).code(400);
                }
                let params = {
                    path: `/rest/stateful/corp/crypto/certificate/${
                        request.params.id
                    }/params`,
                    method: 'GET',
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then(res => {
                        reply(res.result)
                            .code(res.statusCode)
                            .type('application/json');
                    })
                    .catch(rej => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'GET',
            path: '/api/v1/crypto/certificate/activate/{id}',
            handler(request, reply) {
                if (!validate.number(request.params.id)) {
                    return reply(
                        JSON.stringify({
                            errorCode: '400',
                            errorText: 'Invalid payload',
                        })
                    ).code(400);
                }
                let params = {
                    path: `/rest/stateful/corp/crypto/certificate/${
                        request.params.id
                    }/activate`,
                    method: 'POST',
                    postData: {},
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then(res => {
                        reply(res.result)
                            .code(res.statusCode)
                            .type('application/json');
                    })
                    .catch(rej => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/crypto/verify',
            handler(request, reply) {
                const payload =
                    typeof request.payload === 'string'
                        ? JSON.parse(request.payload)
                        : request.payload;

                if (
                    !validate.number(payload.id) ||
                    !validate.number(payload.libId)
                ) {
                    return reply(
                        JSON.stringify({
                            errorCode: '400',
                            errorText: 'Invalid payload',
                        })
                    ).code(400);
                }

                const signedData = Buffer.from(payload.signedData, 'base64');
                const docData = Buffer.from(payload.data, 'base64');
                payload.signedData = Array.from(new Uint8Array(signedData));
                payload.data = Array.from(new Uint8Array(docData));

                payload.docModule = xss(payload.docModule);
                payload.docType = xss(payload.docType);
                payload.certSerial = xss(payload.certSerial);

                let params = {
                    path:
                        '/rest/stateful/corp/document/signature/verifyandwrite',
                    method: 'POST',
                    postData: payload,
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then(res => {
                        reply(res.result)
                            .code(res.statusCode)
                            .type('application/json');
                    })
                    .catch(rej => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/crypto/certificate_regen_request/create',
            handler(request, reply) {
                const payload =
                    typeof request.payload === 'string'
                        ? JSON.parse(request.payload)
                        : request.payload;

                const Pkcs10Request = payload.request
                    .replace(/-*BEGIN CERTIFICATE REQUEST-*/, '')
                    .replace(/-*END CERTIFICATE REQUEST-*/, '')
                    .trim();
                const filedata = Buffer.from(Pkcs10Request);

                if (!validate.number(payload.certificate.libId)) {
                    return reply(
                        JSON.stringify({
                            errorCode: '400',
                            errorText: 'Invalid payload',
                        })
                    ).code(400);
                }

                let params = {
                    path:
                        '/rest/stateful/corp/crypto/certificate_regen_request/create',
                    method: 'POST',
                    postData: {
                        cryptoId: payload.certificate.libId,
                        oldCryptoUuid: xss(payload.certificate.serial),
                        filedata: filedata.toString('hex'),
                        deviceId: xss(payload.deviceSerial),
                    },
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then(res => {
                        reply(res.result)
                            .code(res.statusCode)
                            .type('application/json');
                    })
                    .catch(rej => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'GET',
            path: '/api/v1/crypto/regen',
            handler(request, reply) {
                let params = {
                    path: '/rest/stateful/corp/document/fordates_filter',
                    method: 'POST',
                    postData: {
                        docModule: 'doc',
                        docType: 'cert_regen_req',
                    },
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then(res => {
                        reply(res.result)
                            .code(res.statusCode)
                            .type('application/json');
                    })
                    .catch(rej => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/crypto/sign/sms',
            handler(request, reply) {
                const payload =
                    typeof request.payload === 'string'
                        ? JSON.parse(request.payload)
                        : request.payload;

                if (payload.docId && !validate.number(payload.docId)) {
                    return reply(
                        JSON.stringify({
                            errorCode: '400',
                            errorText: 'Invalid payload',
                        })
                    ).code(400);
                }

                let params = {
                    path: '/rest/stateful/corp/document/sign',
                    method: 'POST',
                    postData: {
                        certId: payload.certId,
                        docId: payload.docId,
                        docIds: payload.docIds,
                        docModule: xss(payload.docModule),
                        docType: xss(payload.docType),
                    },
                };
                params = Object.assign(params, request.auth);

                if (payload.inputCode) {
                    params.postData.inputCode = payload.inputCode;
                }

                requestToISimple(params)
                    .then(res => {
                        if (
                            res.headers['need-validkey'] &&
                            res.headers['need-validkey'] === 'true'
                        ) {
                            res.result = JSON.stringify({
                                errorCode: '500',
                                errorText: 'Неверный смс-код',
                            });
                        }
                        reply(res.result)
                            .code(res.statusCode)
                            .type('application/json');
                    })
                    .catch(rej => {
                        reply(rej.toString());
                    });
            },
        },
    ]);

    next();
};

exports.register.attributes = {
    name: 'crypto',
};
