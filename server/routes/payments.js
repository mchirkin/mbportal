const moment = require('moment');

const requestToISimple = require('../middleware/isimple/request/request');

exports.register = (server, options, next) => {
    server.route([
        {
            method: 'POST',
            path: '/api/v1/products/getcorrnameinfo',
            handler(request, reply) {
                const url = '/rest/stateful/corp/dic/corr?name=';
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;
                const resUrl = url + payload.value;

                let params = {
                    path: encodeURI(resUrl),
                    method: 'GET',
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },

        /** ============= */
        {
            method: 'GET',
            path: '/api/v1/payments/{status}',
            handler(request, reply) {
                const status = request.params.status;

                let filter;
                switch (status) {
                    case 'new':
                        filter = 'status like \'new\' or status like \'imported\'';
                        break;
                    case 'send':
                        filter = 'status like \'%send%\'';
                        break;
                    case 'error':
                        filter = 'status like \'decline\'';
                        break;
                }

                const endDate = moment().add(1, 'y');

                let params = {
                    path: '/rest/stateful/corp/rosevro/document/fordates_filter',
                    method: 'POST',
                    postData: {
                        docModule: 'ibankul',
                        docType: 'doc_platpor',
                        filter,
                        dateTo: endDate.format('YYYYMMDD'),
                    },
                };

                if (request.query && request.query.dateFrom) {
                    params.postData.dateFrom = request.query.dateFrom;
                    params.postData.dateTo = request.query.dateTo;
                }

                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString()).code(400);
                    });
            },
        },
    ]);

    next();
};

exports.register.attributes = {
    name: 'payments',
};
