const validate = require('../utils/validate');
const requestToISimple = require('../middleware/isimple/request/request');

exports.register = (server, options, next) => {
    server.route([
        {
            method: 'POST',
            path: '/api/v1/statement',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;
                const dateFrom = payload.dateFrom.replace(/^(\d+).(\d+).(\d+)$/, '$3$2$1');
                const dateTo = payload.dateTo.replace(/^(\d+).(\d+).(\d+)$/, '$3$2$1');

                if (
                    !validate.number(payload.accountId) ||
                    !validate.date(dateFrom, 'YYYYmmdd') ||
                    !validate.date(dateTo, 'YYYYmmdd')
                ) {
                    return reply(JSON.stringify({
                        errorCode: '400',
                        errorText: 'Invalid payload',
                    })).code(400);
                }

                let params = {
                    // eslint-disable-next-line
                    path: `/rest/stateful/corp/account/statement/?account_id=${payload.accountId}&date_from=${dateFrom}&date_to=${dateTo}`,
                    method: 'GET',
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/statement/offline',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;
                const dateFrom = payload.dateFrom.replace(/^(\d+).(\d+).(\d+)$/, '$3-$2-$1');
                const dateTo = payload.dateTo.replace(/^(\d+).(\d+).(\d+)$/, '$3-$2-$1');

                if (
                    !validate.number(payload.accountId) ||
                    !validate.date(dateFrom, 'YYYY-mm-dd') ||
                    !validate.date(dateTo, 'YYYY-mm-dd')
                ) {
                    return reply(JSON.stringify({
                        errorCode: '400',
                        errorText: 'Invalid payload',
                    })).code(400);
                }

                let params = {
                    // eslint-disable-next-line
                    path: `/rest/stateful/corp/statement/offline/ul_get_stmt?acc_id=${payload.accountId}&begin_date=${dateFrom}&end_date=${dateTo}`,
                    method: 'GET',
                };
                params = Object.assign(params, request.auth);

                const requestTime = new Date().getTime();

                requestToISimple(params)
                    .then((res) => {
                        const _result = Object.assign(JSON.parse(res.result), {
                            requestTime,
                        });
                        reply(_result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'GET',
            path: '/api/v1/statement/print/{id}',
            handler(request, reply) {
                if (!validate.number(request.params.id)) {
                    return reply(JSON.stringify({
                        errorCode: '400',
                        errorText: 'Bad request',
                    })).code(400);
                }

                let params = {
                    path: `/rest/stateful/corp/print/stm_pdf?doc_ids=${request.params.id}&print_mode=with_lines`,
                    method: 'GET',
                    binary: true,
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).bytes(res.result.length).code(res.statusCode).type('application/pdf');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'GET',
            path: '/api/v1/statement/print/doc/{id}',
            handler(request, reply) {
                if (!validate.number(request.params.id)) {
                    return reply(JSON.stringify({
                        errorCode: '400',
                        errorText: 'Bad request',
                    })).code(400);
                }

                let params = {
                    path: `/rest/stateful/corp/print/stm_docx?doc_ids=${request.params.id}&print_mode=with_lines`,
                    method: 'GET',
                    binary: true,
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result)
                            .bytes(res.result.length)
                            .code(res.statusCode)
                            .type('application/vnd.openxmlformats-officedocument.wordprocessingml.document');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'GET',
            path: '/api/v1/statement/print/xls/{id}',
            handler(request, reply) {
                if (!validate.number(request.params.id)) {
                    return reply(JSON.stringify({
                        errorCode: '400',
                        errorText: 'Bad request',
                    })).code(400);
                }

                let params = {
                    path: `/rest/stateful/corp/print/stm_xlsx?doc_ids=${request.params.id}&print_mode=with_lines`,
                    method: 'GET',
                    binary: true,
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result)
                            .bytes(res.result.length)
                            .code(res.statusCode)
                            .type('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/statement/export',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;
                const beginDate = payload.from.replace(/^(\d+).(\d+).(\d+)$/, '$3-$2-$1');
                const endDate = payload.to.replace(/^(\d+).(\d+).(\d+)$/, '$3-$2-$1');

                if (
                    !validate.number(payload.accId) ||
                    !validate.date(beginDate, 'YYYY-mm-dd') ||
                    !validate.date(endDate, 'YYYY-mm-dd')
                ) {
                    return reply(JSON.stringify({
                        errorCode: '400',
                        errorText: 'Invalid payload',
                    })).code(400);
                }

                let params = {
                    // eslint-disable-next-line
                    path: `/rest/stateful/corp/statement/offline/ul_get_stmt_1c?acc_id=${payload.accId}&begin_date=${beginDate}&end_date=${endDate}`,
                    method: 'GET',
                    binary: true,
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result)
                            .bytes(res.result.length)
                            .code(res.statusCode)
                            .type('octet/stream;charset=windows-1251');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
    ]);

    next();
};

exports.register.attributes = {
    name: 'statement',
};
