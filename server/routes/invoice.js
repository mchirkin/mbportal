const fetch = require('isomorphic-fetch');

const NODE_ENV = process.env.NODE_ENV || 'development';

const apiUrl = NODE_ENV === 'production'
    ? 'https://edpa.roseurobank.ru/prod/cms/mb-api'
    : 'http://10.1.20.240/mb-api';

exports.register = (server, options, next) => {
    server.route([
        {
            method: 'GET',
            path: '/invoice/{id}',
            handler(request, reply) {
                fetch(`${apiUrl}/api/invoice/get_by_id/${request.params.id}`)
                    .then(response => response.json())
                    .then((json) => {
                        reply.view('invoice', {
                            data: JSON.stringify(json),
                        });
                    })
                    .catch((err) => {
                        console.log(err);
                    });
            },
        },
        {
            method: 'POST',
            path: '/invoice/docx',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;

                fetch(`${apiUrl}/api/invoice/docx`, {
                    method: 'POST',
                    headers: {
                        'Content-type': 'application/json',
                    },
                    body: JSON.stringify(payload),
                })
                    .then(response => response.buffer())
                    .then((blob) => {
                        reply(blob);
                    })
                    .catch((err) => {
                        console.log(err);
                        reply('{ "success": "false" }').code(200).type('application/json');
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/invoice/list',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;

                fetch(`${apiUrl}/api/invoice/list`, {
                    method: 'POST',
                    headers: {
                        'Content-type': 'application/json',
                    },
                    body: JSON.stringify({
                        extCorporateRef: payload.extCorporateRef,
                    }),
                })
                    .then(response => response.json())
                    .then((json) => {
                        reply(json).code(200).type('application/json');
                    })
                    .catch((err) => {
                        console.log(err);
                        reply('{ "success": false }').code(200).type('application/json');
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/invoice/number',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;

                fetch(`${apiUrl}/api/invoice/number`, {
                    method: 'POST',
                    headers: {
                        'Content-type': 'application/json',
                    },
                    body: JSON.stringify({
                        extCorporateRef: payload.extCorporateRef,
                    }),
                })
                    .then(response => response.json())
                    .then((json) => {
                        reply(json).code(200).type('application/json');
                    })
                    .catch((err) => {
                        console.log(err);
                        reply('{ "success": false }').code(200).type('application/json');
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/invoice/create',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;

                fetch(`${apiUrl}/api/invoice/create`, {
                    method: 'POST',
                    headers: {
                        'Content-type': 'application/json',
                    },
                    body: JSON.stringify(payload),
                })
                    .then(response => response.json())
                    .then((json) => {
                        reply(json).code(200).type('application/json');
                    })
                    .catch((err) => {
                        console.log(err);
                        reply('{ "success": false }').code(200).type('application/json');
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/invoice/template/create',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;

                fetch(`${apiUrl}/api/invoice/template/create`, {
                    method: 'POST',
                    headers: {
                        'Content-type': 'application/json',
                    },
                    body: JSON.stringify(payload),
                })
                    .then(response => response.json())
                    .then((json) => {
                        reply(json).code(200).type('application/json');
                    })
                    .catch((err) => {
                        console.log(err);
                        reply('{ "success": false }').code(200).type('application/json');
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/invoice/template/update',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;

                fetch(`${apiUrl}/api/invoice/template/update`, {
                    method: 'POST',
                    headers: {
                        'Content-type': 'application/json',
                    },
                    body: JSON.stringify(payload),
                })
                    .then(response => response.json())
                    .then((json) => {
                        reply(json).code(200).type('application/json');
                    })
                    .catch((err) => {
                        console.log(err);
                        reply('{ "success": false }').code(200).type('application/json');
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/invoice/template/delete',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;

                fetch(`${apiUrl}/api/invoice/template/delete/${payload.id}`, {
                    method: 'DELETE',
                })
                    .then(response => response.json())
                    .then((json) => {
                        reply(json).code(200).type('application/json');
                    })
                    .catch((err) => {
                        console.log(err);
                        reply('{ "success": false }').code(200).type('application/json');
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/invoice/template/list',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;

                fetch(`${apiUrl}/api/invoice/template/list`, {
                    method: 'POST',
                    headers: {
                        'Content-type': 'application/json',
                    },
                    body: JSON.stringify({
                        extCorporateRef: payload.extCorporateRef,
                    }),
                })
                    .then(response => response.json())
                    .then((json) => {
                        reply(json).code(200).type('application/json');
                    })
                    .catch((err) => {
                        console.log(err);
                        reply('{ "success": false }').code(200).type('application/json');
                    });
            },
        },
    ]);

    next();
};

exports.register.attributes = {
    name: 'invoice',
};
