const requestToISimple = require('../middleware/isimple/request/request.js');

exports.register = (server, options, next) => {
    server.route([
        {
            method: 'POST',
            path: '/api/v1/cancel_documents/platpor/list',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;
                let statuses = '';
                if (payload.statusList && payload.statusList.length > 0) {
                    payload.statusList.forEach((status) => {
                        if (status === 'for-send') {
                            statuses += 'send;for_send_abs;for_send;';
                        } else if (status !== 'all') {
                            statuses += `${status};`;
                        }
                    });
                }
                if (statuses.length > 0) {
                    statuses = `&list_of_statuses=${statuses}`;
                }
                let params = {
                    // eslint-disable-next-line
                    path: `/rest/stateful/corp/rosevro/last_documents/doc_types?number_of_entries=${payload.count}${statuses}`,
                    method: 'POST',
                    postData: {
                        docTypes: [
                            {
                                docModule: 'ibankul',
                                docType: 'request_cancel',
                            },
                        ],
                        dateFrom: payload.dateFrom ? payload.dateFrom.replace(/^(\d+)\.(\d+)\.(\d+)$/, '$3$2$1') : '',
                        dateTo: payload.dateTo ? payload.dateTo.replace(/^(\d+)\.(\d+)\.(\d+)$/, '$3$2$1') : '',
                        filter: "cancelDocType='doc_platpor'",
                    },
                };
                params = Object.assign(params, request.auth);

                let cancelList = {};
                requestToISimple(params)
                    .then((res) => {
                        cancelList = JSON.parse(res.result);
                        if (!cancelList) {
                            return null;
                        }
                        cancelList = Array.isArray(cancelList.rosevroLastDocument)
                            ? cancelList.rosevroLastDocument
                            : [cancelList.rosevroLastDocument];
                        cancelList.forEach((el) => {
                            const info = el.document.docInfo.split('//==//');
                            if (info.length > 1) {
                                let cancelDocId = info[0];
                                const cause = info[2];
                                cancelDocId = cancelDocId.replace(/ |\u00a0/g, '');
                                el.document.cancelDocId = cancelDocId;
                                el.document.cause = cause.trim();
                            } else {
                                el.document.cancelDocId = info[0].match(/ [\d \u00a0]+ /)[0].replace(/[^\d]/g, '');
                            }
                        });
                        const docIds = cancelList.map(el => el.document.cancelDocId).join(';');
                        console.log('[docIds]', docIds);
                        let params = {
                            // eslint-disable-next-line
                            path: `/rest/stateful/corp/rosevro/document/byid?doc_module=ibankul&doc_type=doc_platpor&doc_ids=${docIds}&include_clarify=true`,
                            method: 'GET',
                        };
                        params = Object.assign(params, request.auth);
                        return requestToISimple(params);
                    })
                    .then((res) => {
                        if (!res) {
                            reply(null).code(200).type('application/json');
                            return;
                        }

                        let platporDocuments = JSON.parse(res.result);

                        if (platporDocuments) {
                            platporDocuments = Array.isArray(platporDocuments.rosevroSimpleDocument)
                                ? platporDocuments.rosevroSimpleDocument
                                : [platporDocuments.rosevroSimpleDocument];

                            platporDocuments.forEach((doc) => {
                                const indexList = cancelList.map((cancel, index) => {
                                    if (cancel.document.cancelDocId === doc.platporDocument.id) {
                                        return index;
                                    }
                                    return null;
                                });
                                if (indexList && indexList.length > 0) {
                                    indexList.forEach((index) => {
                                        if (typeof index !== 'undefined') {
                                            cancelList[index].platporDocument = doc.platporDocument;
                                        }
                                    });
                                }
                            });
                        }
                        reply(cancelList).code(res.statusCode).type('application/json');
                    })
                    .catch((err) => {
                        console.log(err);
                        reply(err.toString());
                    });
            },
        },
    ]);

    next();
};

exports.register.attributes = {
    name: 'cancel_documents',
};
