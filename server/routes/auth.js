const crypto = require('crypto');
const getIP = require('ip');

const requestToISimple = require('../middleware/isimple/request/request');
const requestDigest = require('../middleware/isimple/request/request_digest');
const env = require('../env.json');
const requestAuth = require('../middleware/isimple/auth/fetch_login');

const NODE_ENV = process.env.NODE_ENV || 'development';
const serverIP = getIP.address();
const isServerInLAN = /10\.1\.16/.test(serverIP) || /10\.1\.20/.test(serverIP);

let captchaServiceUrl = 'http://10.1.20.240/captcha-service';
if (NODE_ENV === 'production') {
    captchaServiceUrl = 'https://edpa.roseurobank.ru/prod/cms/captcha-service';
}

function getPasswordRestrictionsAndHistory(authParams) {
    let params = {
        path: '/rest/stateful/corp/userlogindata/passwd/change/v2',
        method: 'POST',
        postData: {},
    };
    params = Object.assign(params, authParams);

    return requestToISimple(params).then((res) => {
        let json;
        if (res.result.length > 0) {
            json = JSON.parse(res.result);
            return Promise.resolve(json);
        }
        return Promise.reject('Empty response');
    });
}

function checkForCaptcha({ ip, login, isSuccess }) {
    if (true || isServerInLAN) {
        return Promise.resolve({
            needCaptcha: false,
        });
    }

    return fetch(`${captchaServiceUrl}/captcha/auth_attempts`, {
        method: 'POST',
        headers: {
            'content-type': 'application/json',
        },
        body: JSON.stringify({
            ip,
            isSuccess,
            login,
        }),
    })
        .then(response => response.json());
}

function checkIfCaptchaIsNeeded({ ip, login }) {
    if (true || isServerInLAN) {
        return Promise.resolve({
            needCaptcha: false,
        });
    }

    return fetch(`${captchaServiceUrl}/captcha/current_attempts`, {
        method: 'POST',
        headers: {
            'content-type': 'application/json',
        },
        body: JSON.stringify({
            ip,
            login,
        }),
    })
        .then(response => response.json());
}

function checkCaptcha(response) {
    if (true || isServerInLAN) {
        return Promise.resolve({
            needCaptcha: false,
        });
    }

    return fetch(`${captchaServiceUrl}/captcha/check/recaptcha`, {
        method: 'POST',
        headers: {
            'content-type': 'application/json',
        },
        body: JSON.stringify({
            response,
        }),
    })
        .then(response => response.json());
}

/**
 * Проверить новый пароль на ограничения
 * @return {boolean}
 */
function checkNewPassword(password, passwordRestrictions) {
    const minPassLength = parseInt(passwordRestrictions.passwordMinLength, 10);
    if (password.length < minPassLength) {
        return false;
    }
    if (passwordRestrictions.usePasswordDifferentRegister === 'true') {
        if (
            password === password.toLowerCase() ||
            password === password.toUpperCase() ||
            !/[А-Яа-яA-Za-z]/.test(password)
        ) {
            return false;
        }
    }
    if (passwordRestrictions.usePasswordDigits === 'true') {
        if (!/\d/.test(password)) {
            return false;
        }
    }
    if (/[А-Яа-я]/.test(password)) {
        return false;
    }
    return true;
}

function callChangePassword(login, password, hashedPassword, salt, hashAlg) {
    const params = {
        path: '/rest/corp/userlogindata/passwd/change/v2',
        username: login,
        password,
        method: 'POST',
        postData: {
            newHashedPassword: hashedPassword,
            newHashAlg: hashAlg,
            newSalt: salt,
        },
    };
    return requestDigest(params);
}

exports.register = (server, options, next) => {
    server.route([
        {
            method: 'POST',
            path: '/api/v1/login',
            handler(request, reply) {
                let params = {
                    path: '/rest/stateful/corp/login',
                    method: 'POST',
                };
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;
                params = Object.assign(
                    params,
                    payload,
                    {
                        ClientIP: request.ip,
                        userAgent: request.userAgent,
                    }
                );

                checkIfCaptchaIsNeeded({ ip: request.ip, login: payload.username })
                    .then((result) => {
                        console.log(result);
                        if (result && result.needCaptcha) {
                            if (!payload.responseCaptcha) {
                                return Promise.reject('{ "needCaptcha": true }');
                            }
                            return {
                                checkCaptcha: true,
                                responseCaptcha: payload.responseCaptcha,
                            };
                        }
                        return requestAuth(Object.assign({}, params, payload));
                    })
                    .then((result) => {
                        if (result && result.checkCaptcha) {
                            return checkCaptcha(result.responseCaptcha);
                        }
                        return result;
                    })
                    .then((result) => {
                        if (result.success) {
                            return requestAuth(Object.assign({}, params, payload));
                        }
                        if (typeof result.success !== 'undefined' && result.success === false) {
                            return Promise.reject('{ "needCaptcha": true, "captchaWrong": true }');
                        }
                        return result;
                    })
                    // requestToISimple(params)
                    // requestAuth(Object.assign({}, params, payload))
                    .then((res) => {
                        console.log('step 5', res);
                        console.log(JSON.stringify(res.headers, null, 2));

                        // Генерация CSRF токена
                        const salt = crypto.randomBytes(16).toString('base64');
                        const secret = env.csrfPassword;
                        const saltedSecret = crypto.createHash('md5').update(`${salt}:${secret}`).digest('hex');
                        const csrfToken = `${salt}:${saltedSecret}`;

                        let isSuccess = false;

                        let JSESSIONID = '';

                        if (res.statusCode === 200 || res.statusCode === 461) {
                            const responseCookie = [res.headers['set-cookie']];
                            // let JSESSIONID = '';
                            if (responseCookie) {
                                responseCookie.forEach((c) => {
                                    if (/JSESSIONID/.test(c)) {
                                        const matches = c.match(/JSESSIONID=([^;$]*)/);
                                        JSESSIONID = matches[1];
                                    }
                                });
                            }
                        }

                        if (res.statusCode === 200 || res.statusCode === 461) {
                            isSuccess = true;

                            const ip = request.headers['x-forwarded-for']
                                ? request.headers['x-forwarded-for'].split(',')[0]
                                : request.info.remoteAddress;

                            res.result = JSON.stringify({
                                csrfToken,
                            });

                            request.yar.set('user', {
                                useragent: request.plugins.scooter.toString(),
                                JSESSIONID,
                                login: payload.username,
                                ClientIP: ip,
                                csrfToken,
                            });
                        }

                        if (res.statusCode === 461) {
                            isSuccess = true;
                            res.result = JSON.stringify({
                                twoFa: true,
                                csrfToken,
                            });

                            requestToISimple({
                                path: '/rest/stateful/corp/security/authentication/additional',
                                method: 'POST',
                                postData: '{}',
                                useragent: request.plugins.scooter.toString(),
                                JSESSIONID,
                                login: payload.username,
                                ClientIP: '',
                            });
                        }

                        if (res.statusCode === 500) {
                            res.statusCode = 401;
                            res.result = '{ "sessionExpired": true }';
                        }

                        if (res.headers.passwordexpired && res.headers.passwordexpired === 'true') {
                            const _result = {
                                passwordExpired: true,
                                csrfToken,
                            };
                            return checkForCaptcha({
                                ip: request.ip,
                                login: payload.username,
                                isSuccess: true,
                            })
                                .then(() => {
                                    return reply(JSON.stringify(_result)).code(res.statusCode).type('application/json');
                                });
                            // return reply(JSON.stringify(_result)).code(res.statusCode).type('application/json');
                        }

                        if (res.headers['error-code'] === '1005') {
                            const blockLevel = res.headers['www-authenticate'].match(/blockLevel="(\d+)"/);
                            const unblockTime = res.headers['www-authenticate'].match(/unblockTime="([^,]+)"/);

                            let blockLevelValue;
                            let unblockTimeValue;
                            if (blockLevel && blockLevel.length > 0) {
                                blockLevelValue = blockLevel[1];
                            }
                            if (unblockTime && unblockTime.length > 0) {
                                unblockTimeValue = unblockTime[1];
                            }

                            res.result = JSON.stringify({
                                error: 'user blocked',
                                errorCode: '1005',
                                blockLevel: blockLevelValue,
                                unblockTime: unblockTimeValue,
                            });
                        }


                        return checkForCaptcha({
                            ip: request.ip,
                            login: payload.username,
                            isSuccess,
                        })
                            .then((result) => {
                                const mergedResult = Object.assign({}, JSON.parse(res.result), result);

                                if (res.headers['www-authenticate']) {
                                    const leftAttempts = res.headers['www-authenticate'].match(/leftAttempts="(\d+)"/);
                                    let leftAttemptsValue;
                                    if (leftAttempts && leftAttempts.length > 0) {
                                        leftAttemptsValue = leftAttempts[1];
                                    }
                                    if (leftAttemptsValue) {
                                        mergedResult.leftAttempts = leftAttemptsValue;
                                    }
                                }

                                reply(JSON.stringify(mergedResult)).code(res.statusCode).type('application/json');
                            });

                        // reply(res.result).code(res.statusCode).type('application/json');


                        const result = res.result ? JSON.parse(res.result) : {};

                        /*
                        if (res.headers['www-authenticate']) {
                            const leftAttempts = res.headers['www-authenticate'].match(/leftAttempts="(\d+)"/);
                            let leftAttemptsValue;
                            if (leftAttempts && leftAttempts.length > 0) {
                                leftAttemptsValue = leftAttempts[1];
                            }
                            if (leftAttemptsValue) {
                                result.leftAttempts = leftAttemptsValue;
                            }
                        }
                        */

                        reply(JSON.stringify(result)).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString()).code(401).type('application/json');
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/two_fa',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;
                let params = {
                    path: `/rest/stateful/corp/security/authentication/additional/notification?key=${payload.key}`,
                    method: 'POST',
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'GET',
            path: '/api/v1/two_fa/resend',
            handler(request, reply) {
                let params = {
                    path: '/rest/stateful/corp/security/authentication/additional?restart=true',
                    method: 'POST',
                    postData: {
                        restart: true,
                    },
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/change_password',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;
                let saltedPassword = '';
                const salt = crypto
                    .createHash('sha256')
                    .update(new Date().toLocaleString())
                    .digest('hex')
                    .substr(0, 10);
                if (payload && payload.newPassword) {
                    saltedPassword = `${payload.newPassword}{${salt}}`;
                    if (payload.newPassword === payload.currentPassword) {
                        return reply(JSON.stringify({
                            errorCode: 400,
                            errorText: 'Новый пароль совпадает с текущим',
                        })).code(400).type('application/json');
                    }
                    return getPasswordRestrictionsAndHistory(request.auth).then((pswdParams) => {
                        if (!checkNewPassword(payload.newPassword, pswdParams)) {
                            return reply(JSON.stringify({
                                errorCode: 400,
                                errorText: 'Пароль не соответствует парольной политике',
                            })).code(400).type('application/json');
                        }
                        const newHashedPassword = crypto.createHash('sha256').update(saltedPassword).digest('hex');
                        return callChangePassword(
                            request.auth.login,
                            payload.currentPassword,
                            newHashedPassword,
                            salt,
                            'sha256'
                        ).then((response) => {
                            return reply(response.result).code(response.statusCode).type('application/json');
                        });
                    }).catch((err) => {
                        reply(err.toString());
                    });
                }

                const postData = payload
                    ? {
                        newHashedPassword: crypto.createHash('sha256').update(saltedPassword).digest('hex'),
                        newHashAlg: 'sha256',
                        newSalt: salt,
                    }
                    : {};
                let params = {
                    path: '/rest/stateful/corp/userlogindata/passwd/change/v2',
                    method: 'POST',
                    postData,
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        let json;
                        if (res.result.length > 0) {
                            json = JSON.parse(res.result);
                            json.passwdHistoryData = undefined;
                            json.saltPassword = undefined;
                        }
                        const result = json ? JSON.stringify(json) : res.result;
                        reply(result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/logout',
            handler(request, reply) {
                let params = {
                    path: '/rest/stateful/corp/logout',
                    method: 'GET',
                };
                params = Object.assign(params, request.auth);
                requestToISimple(params);
                request.yar.reset();
                reply().code(200);
            },
        },
        {
            method: 'GET',
            path: '/api/v1/check_session',
            handler(request, reply) {
                let params = {
                    path: '/rest/stateful/corp/ping',
                    method: 'GET',
                };

                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        if (res.statusCode === 200) {
                            reply('{ "sessionExpired": false }').code(res.statusCode).type('application/json');
                        } else {
                            reply('{ "sessionExpired": true }').code(res.statusCode).type('application/json');
                        }
                        // reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'GET',
            path: '/api/v1/user_info',
            handler(request, reply) {
                let params = {
                    path: '/rest/stateful/corp/rosevro/employee',
                    method: 'GET',
                };

                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'GET',
            path: '/api/v1/org_info',
            handler(request, reply) {
                let params = {
                    path: '/rest/stateful/corp/rosevro/client_ul',
                    method: 'GET',
                };

                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/change_login',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;

                let params = {
                    path: `/rest/stateful/corp/userlogindata/login/change?new_username=${payload.newLogin}`,
                    method: 'POST',
                    postData: {},
                };
                params = Object.assign(params, request.auth);

                if (/[А-Яа-я]/.test(payload.newLogin)) {
                    return reply(`{
                        "errorCode": 1000,
                        "errorText": "В логине должны отсутствовать символы кириллицы."
                    }`)
                        .code(200)
                        .type('application/json');
                }

                requestToISimple(params)
                    .then((res) => {
                        const user = request.yar.get('user');

                        if (res.statusCode === 200 && !/errorCode/.test(res.result)) {
                            request.yar.set('user', {
                                useragent: user.useragent,
                                JSESSIONID: user.JSESSIONID,
                                login: payload.newLogin,
                                ClientIP: user.ClientIP,
                                csrfToken: user.csrfToken,
                            });
                        }

                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/confirm_register_ul',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;

                let portalId;
                switch (process.env.NODE_ENV) {
                    case 'development':
                        portalId = '1365944';
                        break;
                    case 'test':
                        portalId = '41947';
                        break;
                    case 'production':
                        portalId = '127093';
                        break;
                    default:
                        portalId = '1365944';
                }

                /* eslint-disable max-len */
                const params = {
                    path: `/rest/public/confirm_req_employee_ul/request?inn=${payload.inn}&phone=${payload.phone}&portalSolutionId=${portalId}`,
                    method: 'POST',
                    postData: {},
                };
                /* eslint-enable max-len */

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/confirm_register_ul/confirm',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;

                const params = {
                    path: '/rest/public/confirm_req_employee_ul/confirm',
                    method: 'POST',
                    postData: {
                        docId: payload.docId,
                        type: payload.confirmType,
                        result: payload.confirmCode,
                    },
                };

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
    ]);

    next();
};

exports.register.attributes = {
    name: 'auth',
};
