const isProduction = process.env.NODE_ENV === 'production';

exports.register = (server, options, next) => {
    server.route([
        {
            method: '*',
            path: '/api/v1/captcha/{p*}',
            handler(request, reply) {
                const query = Object.keys(request.query).map(q => `${q}=${request.query[q]}`).join('&');

                const uri = isProduction
                    ? encodeURI(`https://edpa.roseurobank.ru/prod/piwik/analytics/${request.params.p}?${query}`)
                    : encodeURI(`http://10.1.20.240:5000/${request.params.p}?${query}`);

                return reply.proxy({
                    uri,
                    passThrough: true,
                    xforward: true,
                });
            },
            config: {
                payload: {
                    parse: false,
                },
            },
        },
    ]);

    next();
};

exports.register.attributes = {
    name: 'captcha',
};
