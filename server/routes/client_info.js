const requestToISimple = require('../middleware/isimple/request/request');

exports.register = (server, options, next) => {
    server.route([
        {
            method: 'GET',
            path: '/api/v1/managers',
            handler(request, reply) {
                let params = {
                    path: '/rest/stateful/corp/rosevro/person_bank_info',
                    method: 'GET',
                };

                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
    ]);

    next();
};

exports.register.attributes = {
    name: 'client_info',
};
