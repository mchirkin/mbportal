const nodemailer = require('nodemailer');

const requestToISimple = require('../middleware/isimple/request/request');
const validate = require('../utils/validate');

const smtpTransport = nodemailer.createTransport({
    host: '127.0.0.1',
    port: '25',
});

const NODE_ENV = process.env.NODE_ENV || 'development';

exports.register = (server, options, next) => {
    server.route([
        {
            method: 'GET',
            path: '/api/v1/msg/mail/outcome',
            handler(request, reply) {
                let params = {
                    path: '/rest/stateful/corp/document/fordates_filter',
                    method: 'POST',
                    postData: {
                        docModule: 'ibankul',
                        docType: 'mail2bank',
                    },
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/msg/mail/outcome',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;
                let params = {
                    path: '/rest/stateful/corp/document/fordates_filter',
                    method: 'POST',
                    postData: {
                        docModule: 'ibankul',
                        docType: 'mail2bank',
                        dateFrom: payload.filter.from && validate.date(payload.filter.from)
                            ? payload.filter.from.replace(/^(\d+).(\d+).(\d+)$/, '$3$2$1')
                            : '',
                        dateTo: payload.filter.to && validate.date(payload.filter.to)
                            ? payload.filter.to.replace(/^(\d+).(\d+).(\d+)$/, '$3$2$1')
                            : '',
                        filter: payload.filter.theme && payload.filter.theme.length > 0
                            ? `caption like '%${payload.filter.theme}%'`
                            : '',
                    },
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/msg/mail/income',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;
                let params = {
                    path: '/rest/stateful/corp/document/fordates_filter',
                    method: 'POST',
                    postData: {
                        docModule: 'ibankul',
                        docType: 'mail2client',
                        dateFrom: payload.filter.from && validate.date(payload.filter.from)
                            ? payload.filter.from.replace(/^(\d+).(\d+).(\d+)$/, '$3$2$1')
                            : '',
                        dateTo: payload.filter.to && validate.date(payload.filter.to)
                            ? payload.filter.to.replace(/^(\d+).(\d+).(\d+)$/, '$3$2$1')
                            : '',
                        filter: payload.filter.theme && payload.filter.theme.length > 0
                            ? `caption like '%${payload.filter.theme}%'`
                            : '',
                    },
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/msg/mail/create',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;

                if (payload.id && !validate.number(payload.id)) {
                    return reply(JSON.stringify({
                        errorCode: 400,
                        errorText: 'Invalid payload',
                    })).code(400).type('application/json');
                }

                let params = {
                    path: '/rest/stateful/corp/mail2bank',
                    method: 'POST',
                    postData: {
                        id: payload.id,
                        caption: payload.caption,
                        description: payload.description,
                    },
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/msg/mail/attachment/delete',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;

                if (!validate.number(payload.id) || !validate.number(payload.attachmentId)) {
                    return reply(JSON.stringify({
                        errorCode: 400,
                        errorText: 'Invalid payload',
                    })).code(400).type('application/json');
                }

                let params = {
                    path: `/rest/stateful/corp/mail2bank/${payload.id}/attachment/${payload.attachmentId}`,
                    method: 'DELETE',
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: ['GET', 'POST'],
            path: '/api/v1/msg/mail/attachment/{mailId}/{id}',
            handler(request, reply) {
                let params = {
                    path: `/rest/stateful/corp/mail2bank/${request.params.mailId}/attachment/${request.params.id}`,
                    method: 'GET',
                    binary: true,
                };
                params = Object.assign(params, request.auth);

                if (!validate.number(request.params.mailId) || !validate.number(request.params.id)) {
                    return reply(JSON.stringify({
                        errorCode: 400,
                        errorText: 'Bad request',
                    })).code(400).type('application/json');
                }

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).bytes(res.result.length).code(res.statusCode).type('octet/stream');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'GET',
            path: '/api/v1/msg/mail/unread',
            handler(request, reply) {
                let params = {
                    path: '/rest/stateful/corp/mail2client/unread',
                    method: 'GET',
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'PUT',
            path: '/api/v1/msg/mail/unread/{id}',
            handler(request, reply) {
                let params = {
                    path: `/rest/stateful/corp/mail2client/unread/${request.params.id}`,
                    method: 'PUT',
                };
                params = Object.assign(params, request.auth);

                if (!validate.number(request.params.id)) {
                    return reply(JSON.stringify({
                        errorCode: 400,
                        errorText: 'Bad request',
                    })).code(400).type('application/json');
                }

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/msg/mail/send',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;

                const { user, org, managers, partner } = payload;

                const emailList = NODE_ENV === 'development' || NODE_ENV === 'test'
                    ? [
                        'm.chirkin@roseurobank.ru',
                        'a.ivanov@roseurobank.ru',
                    ]
                    : [
                        'm.chirkin@roseurobank.ru',
                        'a.ivanov@roseurobank.ru',
                        's.kochubin@roseurobank.ru',
                        'd.kozyrkov@roseurobank.ru',
                    ];

                if (managers && managers.length > 0) {
                    managers.forEach((manager) => {
                        emailList.push(manager.email);
                    });
                }

                const msgBody = `
                    Клиент <b>${user.surname} ${user.name} ${user.patronymic}</b> из организации
                    <b>${org.fullName}</b> хочет воспользоваться спецпредложением от партнера <b>${partner}</b>
                    <br /><b>Телефон клиента:</b>${user.phoneNumber}
                `;

                const mailOptions = {
                    from: 'Интернет-банк для Бизнеса <mb@rosevrobank.ru>', // sender address
                    to: emailList.join(', '), // list of receivers
                    subject: 'Спецпредложение', // Subject line
                    html: msgBody, // html body
                };

                if (NODE_ENV !== 'production') {
                    mailOptions.from = `Интернет-банк для Бизнеса[${NODE_ENV}] <mb@rosevrobank.ru>`;
                }

                smtpTransport.sendMail(mailOptions, (error) => {
                    if (error) {
                        reply('{ "error": "opps..." }').code(200).type('application/json');
                    } else {
                        reply('{}').code(200).type('application/json');
                    }

                    smtpTransport.close();
                });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/msg/acquiring_request',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;

                const { org } = payload;

                const emailList = NODE_ENV === 'development' || NODE_ENV === 'test'
                    ? [
                        'm.chirkin@roseurobank.ru',
                        'v.tamazov@roseurobank.ru',
                        'a.ivanov@roseurobank.ru',
                        'm.timchenko@roseurobank.ru',
                    ]
                    : [
                        'm.chirkin@roseurobank.ru',
                        'v.tamazov@roseurobank.ru',
                        'web_rko@roseurobank.ru',
                    ];

                const msgBody = `
                    Клиент <b>${org.fullName}</b> ИНН <b>${org.INN}</b> подал заявку на Интерент-эквайринг.
                     Необходимо передать заявку в отдел эквайринга
                `;

                const mailOptions = {
                    from: 'Интернет-банк для Бизнеса <mb@rosevrobank.ru>',
                    to: emailList.join(', '),
                    subject: 'Заявка на интернет-эквайринг',
                    html: msgBody,
                };

                if (NODE_ENV !== 'production') {
                    mailOptions.from = `Интернет-банк для Бизнеса[${NODE_ENV}] <mb@rosevrobank.ru>`;
                }

                smtpTransport.sendMail(mailOptions, (error) => {
                    if (error) {
                        reply('{ "error": "opps..." }').code(200).type('application/json');
                    } else {
                        reply('{}').code(200).type('application/json');
                    }

                    smtpTransport.close();
                });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/msg/corp_card_request',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;

                const { org } = payload;

                const emailList = NODE_ENV === 'development' || NODE_ENV === 'test'
                    ? [
                        'm.chirkin@roseurobank.ru',
                        'v.tamazov@roseurobank.ru',
                        'a.ivanov@roseurobank.ru',
                        'm.timchenko@roseurobank.ru',
                    ]
                    : [
                        'm.chirkin@roseurobank.ru',
                        'v.tamazov@roseurobank.ru',
                        'web_rko@roseurobank.ru',
                    ];

                /* eslint-disable */
                const msgBody = `
                    Клиент <b>${org.fullName}</b> ИНН <b>${org.INN}</b> на ТП "название ТП" изъявил желание заказать дополнительную карту.
                     Необходимо связаться с клиентом и пояснить возможный порядок действий.
                `;
                /* eslint-enable */

                const mailOptions = {
                    from: 'Интернет-банк для Бизнеса <mb@rosevrobank.ru>',
                    to: emailList.join(', '),
                    subject: 'Заявка на корпоративную карту',
                    html: msgBody,
                };

                if (NODE_ENV !== 'production') {
                    mailOptions.from = `Интернет-банк для Бизнеса[${NODE_ENV}] <mb@rosevrobank.ru>`;
                }

                smtpTransport.sendMail(mailOptions, (error) => {
                    if (error) {
                        reply('{ "error": "opps..." }').code(200).type('application/json');
                    } else {
                        reply('{}').code(200).type('application/json');
                    }

                    smtpTransport.close();
                });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/msg/mail/operation_notify',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;
                let params = {
                    path: '/rest/stateful/corp/rosevro/document/fordates_filter',
                    method: 'POST',
                    postData: {
                        docModule: 'ibankul',
                        docType: 'operation_notify',
                        dateFrom: payload.from && validate.date(payload.from)
                            ? payload.from.replace(/^(\d+).(\d+).(\d+)$/, '$3$2$1')
                            : '',
                        dateTo: payload.to && validate.date(payload.to)
                            ? payload.to.replace(/^(\d+).(\d+).(\d+)$/, '$3$2$1')
                            : '',
                    },
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
    ]);

    next();
};

exports.register.attributes = {
    name: 'msg',
};
