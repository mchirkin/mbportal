const xss = require('xss');
const validate = require('../utils/validate');
const requestToISimple = require('../middleware/isimple/request/request');
const httpGet = require('../middleware/httpget');

exports.register = (server, options, next) => {
    server.route([
        {
            method: 'POST',
            path: '/api/v1/documents/delete',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;

                if (!validate.number(payload.documentId)) {
                    return reply(JSON.stringify({
                        errorCode: '400',
                        errorText: 'Invalid payload',
                    })).code(400);
                }

                let params = {
                    path: `/rest/stateful/corp/delete/document/${payload.documentId}`,
                    method: 'DELETE',
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/documents/copy',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;

                if (!validate.number(payload.documentId)) {
                    return reply(JSON.stringify({
                        errorCode: '400',
                        errorText: 'Invalid payload',
                    })).code(400);
                }

                let params = {
                    path: `/rest/stateful/corp/copy/document/${payload.documentId}`,
                    method: 'POST',
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/documents/data',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;

                if (!validate.number(payload.docId) || !validate.number(payload.libId)) {
                    return reply(JSON.stringify({
                        errorCode: '400',
                        errorText: 'Invalid payload',
                    })).code(400);
                }

                payload.docModule = xss(payload.docModule);
                payload.docType = xss(payload.docType);

                /* eslint-disable max-len */
                let params = {
                    path: `/rest/stateful/corp/document/signature/data?doc_module=${payload.docModule}&doc_type=${payload.docType}&doc_id=${payload.docId}&lib_id=${payload.libId}`,
                    method: 'GET',
                    binary: true,
                    inBase64: true,
                };
                /* eslint-enable max-len */
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/documents/send2bank',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;

                if (!validate.number(payload.docId)) {
                    return reply(JSON.stringify({
                        errorCode: '400',
                        errorText: 'Invalid payload',
                    })).code(400);
                }

                payload.docModule = xss(payload.docModule);
                payload.docType = xss(payload.docType);

                /* eslint-disable max-len */
                let params = {
                    path: `/rest/stateful/corp/document/send?doc_module=${payload.docModule}&doc_type=${payload.docType}&doc_ids=${payload.docId}`,
                    method: 'PUT',
                };
                /* eslint-enable max-len */
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/documents/create_template',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;

                if (!validate.number(payload.docId)) {
                    return reply(JSON.stringify({
                        errorCode: '400',
                        errorText: 'Invalid payload',
                    })).code(400);
                }

                let params = {
                    path: `/rest/stateful/corp/template/create_by_id?document_id=${payload.docId}`,
                    method: 'POST',
                    postData: {
                        name: payload.name,
                    },
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: ['GET', 'POST'],
            path: '/api/v1/documents/get_pdf/{docId}',
            handler(request, reply) {
                if (!validate.numberCommaList(request.params.docId)) {
                    return reply(JSON.stringify({
                        errorCode: '400',
                        errorText: 'Invalid payload',
                    })).code(400);
                }

                let params = {
                    path: `/rest/stateful/corp/print/pdf?doc_ids=${request.params.docId}`,
                    method: 'GET',
                    binary: true,
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).bytes(res.result.length).code(res.statusCode).type('application/pdf');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'GET',
            path: '/api/v1/documents/get_docx/{docId}',
            handler(request, reply) {
                if (!validate.numberCommaList(request.params.docId)) {
                    return reply(JSON.stringify({
                        errorCode: '400',
                        errorText: 'Invalid payload',
                    })).code(400);
                }

                let params = {
                    path: `/rest/stateful/corp/print/docx?doc_ids=${request.params.docId}`,
                    method: 'GET',
                    binary: true,
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).bytes(res.result.length).code(res.statusCode).type('octet/stream');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'GET',
            path: '/api/v1/documents/get_html/{docId}',
            handler(request, reply) {
                if (!validate.numberCommaList(request.params.docId)) {
                    return reply(JSON.stringify({
                        errorCode: '400',
                        errorText: 'Bad request',
                    })).code(400);
                }

                let params = {
                    path: `/rest/stateful/corp/print/html?doc_ids=${request.params.docId}`,
                    method: 'GET',
                    binary: true,
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).bytes(res.result.length).code(res.statusCode).type('octet/stream');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/documents/set_viewed',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;

                if (!validate.number(payload.docId)) {
                    return reply(JSON.stringify({
                        errorCode: '400',
                        errorText: 'Bad request',
                    })).code(400);
                }

                let params = {
                    path: `/rest/stateful/corp/rosevro/doc/${payload.docId}/set_viewed`,
                    method: 'POST',
                    postData: {},
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/documents/attachment',
            config: {
                payload: {
                    output: 'stream',
                    parse: true,
                    maxBytes: '20971520', // 20 MB
                },
            },
            handler(request, reply) {
                if (!validate.number(request.payload.id)) {
                    return reply(JSON.stringify({
                        errorCode: '400',
                        errorText: 'Bad request',
                    })).code(400);
                }

                const data = [];

                request.payload.file.on('data', (chunk) => {
                    data.push(chunk);
                });

                request.payload.file.on('end', () => {
                    const buf = Buffer.concat(data);
                    let params = {
                        path: '/rest/stateful/corp/attachment',
                        method: 'POST',
                        postData: {
                            id: request.payload.id,
                            file: {
                                value: buf,
                                options: {
                                    filename: request.payload.file.hapi.filename,
                                    contentType: request.payload.file.hapi.headers['content-type'],
                                },
                            },
                        },
                        multipart: true,
                    };
                    params = Object.assign(params, request.auth);

                    requestToISimple(params)
                        .then((res) => {
                            reply(res.result).code(res.statusCode).type('application/json');
                        })
                        .catch((rej) => {
                            reply(rej.toString());
                        });
                });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/documents/attachment/type',
            config: {
                payload: {
                    output: 'stream',
                    parse: true,
                    maxBytes: '20971520', // 20 MB
                },
            },
            handler(request, reply) {
                if (!validate.number(request.payload.clientRequest)) {
                    return reply(JSON.stringify({
                        errorCode: '400',
                        errorText: 'Bad request',
                    })).code(400);
                }

                const data = [];

                request.payload.file.on('data', (chunk) => {
                    data.push(chunk);
                });

                request.payload.file.on('end', () => {
                    const buf = Buffer.concat(data);
                    let params = {
                        path: '/rest/stateful/corp/client_request/rosevro/attachment',
                        method: 'POST',
                        postData: {
                            clientRequest: request.payload.clientRequest,
                            typeId: request.payload.typeId,
                            file: {
                                value: buf,
                                options: {
                                    filename: request.payload.file.hapi.filename,
                                    contentType: request.payload.file.hapi.headers['content-type'],
                                },
                            },
                        },
                        multipart: true,
                    };
                    params = Object.assign(params, request.auth);

                    requestToISimple(params)
                        .then((res) => {
                            reply(res.result).code(res.statusCode).type('application/json');
                        })
                        .catch((rej) => {
                            reply(rej.toString());
                        });
                });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/documents/attachment/mail2bank',
            config: {
                payload: {
                    output: 'stream',
                    parse: true,
                    maxBytes: '20971520', // 20 MB
                },
            },
            handler(request, reply) {
                if (!validate.number(request.payload.id)) {
                    return reply(JSON.stringify({
                        errorCode: '400',
                        errorText: 'Bad request',
                    })).code(400);
                }

                const data = [];

                request.payload.file.on('data', (chunk) => {
                    data.push(chunk);
                });

                request.payload.file.on('end', () => {
                    const buf = Buffer.concat(data);
                    let params = {
                        path: '/rest/stateful/corp/mail2bank/attachment',
                        method: 'POST',
                        postData: {
                            mail2BankId: request.payload.id,
                            file: {
                                value: buf,
                                options: {
                                    filename: request.payload.file.hapi.filename,
                                    contentType: request.payload.file.hapi.headers['content-type'],
                                },
                            },
                        },
                        multipart: true,
                    };
                    params = Object.assign(params, request.auth);

                    requestToISimple(params)
                        .then((res) => {
                            reply(res.result).code(res.statusCode).type('application/json');
                        })
                        .catch((rej) => {
                            reply(rej.toString());
                        });
                });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/documents/cancel',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;

                if (
                    (payload.id && !validate.number(payload.id)) ||
                    (payload.date && !validate.date(payload.docDate, 'YYYY-mm-dd')) ||
                    !validate.number(payload.cancelDocId)
                ) {
                    return reply(JSON.stringify({
                        errorCode: '400',
                        errorText: 'Bad request',
                    })).code(400);
                }

                let params = {
                    path: '/rest/stateful/corp/request/process/ul_request_cancel',
                    method: 'POST',
                    postData: {
                        id: payload.id,
                        docDate: payload.docDate,
                        cancelDocId: payload.cancelDocId,
                        cause: payload.cause,
                    },
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/documents/getbyid',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;

                if (!validate.numberCommaList(payload.id)) {
                    return reply(JSON.stringify({
                        errorCode: '400',
                        errorText: 'Bad request',
                    })).code(400);
                }

                payload.docModule = xss(payload.docModule);
                payload.docType = xss(payload.docType);

                let params = {
                    /* eslint-disable max-len */
                    path: `/rest/stateful/corp/rosevro/document/byid?doc_module=${payload.docModule}&doc_type=${payload.docType}&doc_ids=${payload.id}&include_clarify=true`,
                    method: 'GET',
                    /* eslint-enable max-len */
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'GET',
            path: '/api/v1/documents/list_for_cancel',
            handler(request, reply) {
                httpGet('/public/json?service=doc_types_for_info_and_inquiry&allow_cancel=true').then((res) => {
                    reply(res.result).code(res.statusCode).type('application/json');
                });
            },
        },
    ]);

    next();
};

exports.register.attributes = {
    name: 'documents',
};
