const requestToISimple = require('../middleware/isimple/request/request');
const httpGet = require('../middleware/httpget');
const fetch = require('isomorphic-fetch');
const ip = require('ip');

const NODE_ENV = process.env.NODE_ENV || 'development';

let syncWithABS = NODE_ENV !== 'development' ? 'true' : 'false';

if (NODE_ENV === 'production') {
    syncWithABS = 'true';
}

if (/10\.1\.121/.test(ip.address())) {
    syncWithABS = 'false';
}

exports.register = (server, options, next) => {
    server.route([
        {
            method: 'GET',
            path: '/api/v1/products/accounts',
            handler(request, reply) {
                let params = {
                    path: `/rest/stateful/corp/rosevro/products/account?sync_accounts=${syncWithABS}&with_blocked=true`,
                    method: 'GET',
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        console.log(res);
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'GET',
            path: '/api/v1/products/accounts/offline',
            handler(request, reply) {
                let params = {
                    path: '/rest/stateful/corp/rosevro/products/account?sync_accounts=false&with_blocked=true',
                    method: 'GET',
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'GET',
            path: '/api/v1/products/cards',
            handler(request, reply) {
                let params = {
                    path: `/rest/stateful/corp/rosevro/products/card?sync_cards=${syncWithABS}&with_blocked=true`,
                    method: 'GET',
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'GET',
            path: '/api/v1/products/cards/offline',
            handler(request, reply) {
                let params = {
                    path: '/rest/stateful/corp/rosevro/products/card?sync_cards=false&with_blocked=true',
                    method: 'GET',
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/products/import',
            config: {
                payload: {
                    output: 'stream',
                    parse: true,
                    maxBytes: '20971520', // 20 MB
                },
            },
            handler(request, reply) {
                const data = [];

                request.payload.file.on('data', (chunk) => {
                    data.push(chunk);
                });

                request.payload.file.on('end', () => {
                    const buf = Buffer.concat(data);
                    let params = {
                        path: '/rest/stateful/corp/document/import/platpor/1c',
                        method: 'POST',
                        postData: {
                            file: {
                                value: buf,
                                options: {
                                    filename: request.payload.file.hapi.filename,
                                    contentType: request.payload.file.hapi.headers['content-type'],
                                },
                            },
                        },
                        multipart: true,
                    };
                    params = Object.assign(params, request.auth);

                    requestToISimple(params)
                        .then((res) => {
                            reply(res.result).code(res.statusCode).type('application/json');
                        })
                        .catch((rej) => {
                            reply(rej.toString());
                        });
                });
            },
        },
        {
            method: 'GET',
            path: '/api/v1/products/getnds',
            handler(request, reply) {
                const url = '/public/json?service=ru_rko_nds_type';
                httpGet(url).then((res) => {
                    reply(res.result).code(res.statusCode).type('application/json');
                });
            },
        },
        {
            method: 'GET',
            path: '/api/v1/products/getsorts',
            handler(request, reply) {
                const url = '/public/json?service=ru_rko_urgent_type';
                httpGet(url).then((res) => {
                    reply(res.result).code(res.statusCode).type('application/json');
                });
            },
        },
        {
            method: 'GET',
            path: '/api/v1/products/getcodevo',
            handler(request, reply) {
                const url = '/public/json?service=cur_rko_oper_type';
                httpGet(url).then((res) => {
                    reply(res.result).code(res.statusCode).type('application/json');
                });
            },
        },
        {
            method: 'GET',
            path: '/api/v1/products/gettaxperiod',
            handler(request, reply) {
                const url = '/public/json?service=pay_taxperiod';
                httpGet(url).then((res) => {
                    reply(res.result).code(res.statusCode).type('application/json');
                });
            },
        },
        {
            method: 'GET',
            path: '/api/v1/products/getpstatus',
            handler(request, reply) {
                const url = '/public/json?service=pay_stat';
                httpGet(url).then((res) => {
                    reply(res.result).code(res.statusCode).type('application/json');
                });
            },
        },
        {
            method: 'GET',
            path: '/api/v1/products/getptype',
            handler(request, reply) {
                const url = '/public/json?service=pay_type';
                httpGet(url).then((res) => {
                    reply(res.result).code(res.statusCode).type('application/json');
                });
            },
        },
        {
            method: 'GET',
            path: '/api/v1/products/pay_ground',
            handler(request, reply) {
                const url = '/public/json?service=pay_ground';
                httpGet(url).then((res) => {
                    reply(res.result).code(res.statusCode).type('application/json');
                });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/products/create',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;
                let params = {
                    path: '/rest/stateful/corp/document/create',
                    method: 'POST',
                    postData: payload,
                };
                params = Object.assign(params, request.auth);

                if (payload.corrType && payload.corrType === 'NALOG') {
                    if (!payload.kpp || payload.kpp === '') {
                        params.postData.kpp = '0';
                    }
                }

                // заглушка на праздники
                const startDate = new Date('2017-12-29T15:00:00');
                const endDate = new Date('2018-01-09T00:00:00');

                const today = new Date();

                if (today > startDate && today < endDate) {
                    /* eslint-disable max-len */
                    return reply(`{
                        "errorCode": 1007,
                        "errorText": "В период официально отмечаемых в России и за рубежом новогодних праздников прием платежных поручений не осуществляется.\\nС 9 января 2018 г проведение операций будет осуществляться в обычном режиме."
                    }`).code(200).type('application/json');
                    /* eslint-enable max-len */
                }

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/products/getbikinfo',
            handler(request, reply) {
                const url = '/public/json?service=banks_ru&bik=';
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;
                const resUrl = url + payload.value;

                httpGet(resUrl).then((res) => {
                    reply(res.result).code(res.statusCode).type('application/json');
                });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/products/getbanknameinfo',
            handler(request, reply) {
                const url = '/public/json?service=banks_ru&name=';
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;
                const resUrl = url + payload.value;

                httpGet(resUrl).then((res) => {
                    const list = JSON.parse(res.result).slice(0, 20);
                    reply(JSON.stringify(list)).code(res.statusCode).type('application/json');
                });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/products/banklist',
            handler(request, reply) {
                const url = '/public/json?service=banks_ru&context=';
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;
                const resUrl = url + payload.context;

                httpGet(resUrl).then((res) => {
                    const list = JSON.parse(res.result).slice(0, 20);
                    reply(JSON.stringify(list)).code(res.statusCode).type('application/json');
                });
            },
        },
        {
            method: 'GET',
            path: '/api/v1/products/gettariffs',
            handler(request, reply) {
                const url = '/public/json?service=tarif_rosevro';
                httpGet(url).then((res) => {
                    reply(res.result).code(res.statusCode).type('application/json');
                });
            },
        },
        {
            method: 'GET',
            path: '/api/v1/products/getsendtype',
            handler(request, reply) {
                const url = '/public/json?service=ru_rko_send_type';
                httpGet(url).then((res) => {
                    reply(res.result).code(res.statusCode).type('application/json');
                });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/products/change_alias',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;

                let params = {
                    path: '/rest/stateful/corp/product/change_alias',
                    method: 'PUT',
                    postData: payload,
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'GET',
            path: '/api/v1/products/account_limits',
            handler(request, reply) {
                const { account, branch } = request.query;

                /* eslint-disable max-len */
                let url = `http://10.1.20.240/bank-api/api/v1/account_limits?account=${account}&branch=${branch}`;
                if (NODE_ENV === 'production') {
                    url = `https://edpa.roseurobank.ru/prod/cms/bank-api/api/v1/account_limits?account=${account}&branch=${branch}`;
                }
                /* eslint-enable max-len */

                fetch(url)
                    .then(response => response.json())
                    .then((json) => {
                        reply(JSON.stringify(json)).code(200).type('application/json');
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/accounts/stays',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;

                const staysRequests = payload.accounts.map((acc) => {
                    let params = {
                        path: `/rest/stateful/corp/rosevro/account/stays?account_number=${acc.number}`,
                        method: 'GET',
                    };

                    params = Object.assign(params, request.auth);

                    return requestToISimple(params)
                        .then(result => result);
                });

                Promise.all(staysRequests)
                    .then((values) => {
                        const result = {};
                        payload.accounts.forEach((acc, index) => {
                            result[acc.number] = JSON.parse(values[index].result);
                        });
                        reply(JSON.stringify(result)).code(200).type('application/json');
                    })
                    .catch((err) => {
                        reply(err.toString());
                    });
            },
        },
    ]);

    next();
};

exports.register.attributes = {
    name: 'products',
};
