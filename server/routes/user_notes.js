const fetch = require('isomorphic-fetch');

const NODE_ENV = process.env.NODE_ENV || 'development';

const apiUrl = NODE_ENV === 'production' ? 'https://edpa.roseurobank.ru/prod/cms/mb-api' : 'http://10.1.20.240/mb-api';

exports.register = (server, options, next) => {
    server.route([
        {
            method: 'POST',
            path: '/api/v1/financial_calendar/user_note/list',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;

                fetch(`${apiUrl}/api/financial_calendar/user_note/list`, {
                    method: 'POST',
                    headers: {
                        'Content-type': 'application/json',
                    },
                    body: JSON.stringify({
                        extRef: payload.extRef,
                    }),
                })
                    .then(response => response.json())
                    .then(json => {
                        reply(json)
                            .code(200)
                            .type('application/json');
                    })
                    .catch(err => {
                        console.log(err);
                        reply('{ "success": false }')
                            .code(200)
                            .type('application/json');
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/financial_calendar/user_note/create',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;

                fetch(`${apiUrl}/api/financial_calendar/user_note/create`, {
                    method: 'POST',
                    headers: {
                        'Content-type': 'application/json',
                    },
                    body: JSON.stringify(payload),
                })
                    .then(response => response.json())
                    .then(json => {
                        reply(json)
                            .code(200)
                            .type('application/json');
                    })
                    .catch(err => {
                        console.log(err);
                        reply('{ "success": false }')
                            .code(200)
                            .type('application/json');
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/financial_calendar/user_note/update',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;

                fetch(`${apiUrl}/api/financial_calendar/user_note/update`, {
                    method: 'POST',
                    headers: {
                        'Content-type': 'application/json',
                    },
                    body: JSON.stringify(payload),
                })
                    .then(response => response.json())
                    .then(json => {
                        reply(json)
                            .code(200)
                            .type('application/json');
                    })
                    .catch(err => {
                        console.log(err);
                        reply('{ "success": false }')
                            .code(200)
                            .type('application/json');
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/financial_calendar/user_note/delete',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;

                fetch(`${apiUrl}/api/financial_calendar/user_note/delete/${payload.id}`, {
                    method: 'DELETE',
                })
                    .then(response => response.json())
                    .then(json => {
                        reply(json)
                            .code(200)
                            .type('application/json');
                    })
                    .catch(err => {
                        console.log(err);
                        reply('{ "success": false }')
                            .code(200)
                            .type('application/json');
                    });
            },
        },
    ]);

    next();
};

exports.register.attributes = {
    name: 'user_note',
};
