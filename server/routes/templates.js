const requestToISimple = require('../middleware/isimple/request/request');
const validate = require('../utils/validate');

exports.register = (server, options, next) => {
    server.route([
        {
            method: 'GET',
            path: '/api/v1/templates/list',
            handler(request, reply) {
                let params = {
                    path: '/rest/stateful/corp/template/list',
                    method: 'GET',
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'GET',
            path: '/api/v1/templates/list/full',
            handler(request, reply) {
                let params = {
                    path: '/rest/stateful/corp/rosevro/template/list/filtered',
                    method: 'POST',
                    postData: {
                        docModule: 'ibankul',
                        docType: 'doc_platpor',
                    },
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'GET',
            path: '/api/v1/templates/get_by_id/{id}',
            handler(request, reply) {
                if (!validate.number(request.params.id)) {
                    return reply(JSON.stringify({
                        errorCode: 400,
                        errorText: 'Bad request',
                    })).code(400).type('application/json');
                }

                let params = {
                    path: `/rest/stateful/corp/template/get_by_id?template_id=${request.params.id}`,
                    method: 'GET',
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/templates/delete',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;

                if (!validate.number(payload.templateId)) {
                    return reply(JSON.stringify({
                        errorCode: 400,
                        errorText: 'Bad request',
                    })).code(400).type('application/json');
                }

                let params = {
                    path: `/rest/stateful/corp/template/delete?template_id=${payload.templateId}`,
                    method: 'POST',
                    postData: {},
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/templates/modify',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;

                if (!validate.number(payload.id)) {
                    return reply(JSON.stringify({
                        errorCode: 400,
                        errorText: 'Bad request',
                    })).code(400).type('application/json');
                }

                let params = {
                    path: '/rest/stateful/corp/template/modify',
                    method: 'POST',
                    postData: {
                        id: payload.id,
                        name: payload.name,
                    },
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/templates/create',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;
                let params = {
                    path: '/rest/stateful/corp/document/create/template',
                    method: 'POST',
                    postData: payload,
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
    ]);

    next();
};

exports.register.attributes = {
    name: 'templates',
};
