const requestLib = require('request');

exports.register = (server, options, next) => {
    server.route([
        {
            method: 'POST',
            path: '/dadata/suggest/fio',
            handler(request, reply) {
                const payload = typeof request.payload === 'string'
                    ? JSON.parse(request.payload)
                    : request.payload;

                const requestParams = {
                    url: encodeURI('https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/fio'),
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-type': 'application/json',
                        Authorization: 'Token 31252ccf4e4102d476fb19248a12931451a2897d',
                    },
                    encoding: 'utf8',
                    body: JSON.stringify(payload),
                    proxy: 'http://mchirkin:zz1234@proxy.roseurobank.ru:3128',
                };

                requestLib(requestParams, (err, res, body) => {
                    if (err) {
                        reply('[]').code(200).type('application/json');
                    }
                    reply(body).code(res.statusCode).type('application/json');
                });
            },
        },
        {
            method: 'POST',
            path: '/dadata/suggest/address',
            handler(request, reply) {
                const payload = typeof request.payload === 'string'
                    ? JSON.parse(request.payload)
                    : request.payload;

                const requestParams = {
                    url: encodeURI('https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address'),
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-type': 'application/json',
                        Authorization: 'Token 31252ccf4e4102d476fb19248a12931451a2897d',
                    },
                    encoding: 'utf8',
                    body: JSON.stringify(payload),
                    proxy: 'http://mchirkin:zz1234@proxy.roseurobank.ru:3128',
                };

                requestLib(requestParams, (err, res, body) => {
                    if (err) {
                        reply('[]').code(200).type('application/json');
                    }
                    reply(body).code(res.statusCode).type('application/json');
                });
            },
        },
    ]);

    next();
};

exports.register.attributes = {
    name: 'dadata',
};
