const xss = require('xss');
const requestToISimple = require('../middleware/isimple/request/request');
const validate = require('../utils/validate');

exports.register = (server, options, next) => {
    server.route([
        {
            method: 'GET',
            path: '/api/v1/ministatement/outcome/{status}/{count}',
            handler(request, reply) {
                if (!validate.alphanum(request.params.status)) {
                    return reply(JSON.stringify({
                        errorCode: '400',
                        errorText: 'Bad request',
                    })).code(400);
                }

                /* eslint-disable max-len */
                const path = `/rest/stateful/corp/rosevro/last_documents/doc_types?number_of_entries=${request.params.count}&list_of_statuses=${request.params.status}`;
                /* eslint-enable max-len */
                let params = {
                    path,
                    method: 'POST',
                    postData: {
                        docTypes: [
                            {
                                docModule: 'ibankul',
                                docType: 'doc_platpor',
                            },
                        ],
                        includeClarify: true,
                    },
                };

                if (request.query.accNumber) {
                    params.postData.accNumber = request.query.accNumber;
                }

                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/ministatement/outcome/getbyid',
            handler(request, reply) {
                const payload = JSON.parse(request.payload) || [];
                const ids = payload.map(el => el.document.id).join(';');

                if (ids !== xss(ids)) {
                    return reply(JSON.stringify({
                        errorCode: '400',
                        errorText: 'Bad request',
                    })).code(400);
                }

                /* eslint-disable max-len */
                let params = {
                    path: `/rest/stateful/corp/rosevro/document/byid?doc_module=ibankul&doc_type=doc_platpor&doc_ids=${ids}&include_clarify=true`,
                    method: 'GET',
                };
                /* eslint-enable max-len */
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/ministatement/outcome/documents',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;

                if (!validate.alphanum(payload.status)) {
                    return reply(JSON.stringify({
                        errorCode: '400',
                        errorText: 'Bad request',
                    })).code(400);
                }

                const statusList = payload.status.split(';');
                const filter = statusList.map(status => `status like '%${status}%'`).join(' or ');

                let params = {
                    path: '/rest/stateful/corp/rosevro/document/fordates_filter',
                    method: 'POST',
                    postData: {
                        docModule: 'ibankul',
                        docType: 'doc_platpor',
                        filter, // `status like '%${payload.status}%'`,
                        includeClarify: true,
                    },
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/ministatement/income/{count}',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;

                let invalidPayload = false;
                payload.accounts.forEach((acc) => {
                    if (!validate.number(acc)) {
                        invalidPayload = true;
                    }
                });

                if (invalidPayload) {
                    return reply(JSON.stringify({
                        errorCode: '400',
                        errorText: 'Bad request',
                    })).code(400);
                }

                /* eslint-disable max-len */
                const path = `/rest/stateful/corp/rosevro/credit_operations?count=${request.params.count || 10}&account=${payload.accounts.join('&account=')}`;
                /* eslint-enable max-len */
                let params = {
                    path,
                    method: 'GET',
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then((res) => {
                        console.log(res.result);
                        reply(res.result).code(res.statusCode).type('application/json');
                    })
                    .catch((rej) => {
                        reply(rej.toString());
                    });
            },
        },
    ]);

    next();
};

exports.register.attributes = {
    name: 'ministatement',
};
