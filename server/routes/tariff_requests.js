const nodemailer = require('nodemailer');

const requestToISimple = require('../middleware/isimple/request/request');
const getBranch = require('../middleware/isimple/request/get_branch');

const smtpTransport = nodemailer.createTransport({
    host: '127.0.0.1',
    port: '25',
});

const NODE_ENV = process.env.NODE_ENV || 'development';

const apiUrl = NODE_ENV === 'production' ? 'https://edpa.roseurobank.ru/prod/cms/mb-api' : 'http://10.1.20.240/mb-api';

/**
 * Создание запроса
 * @param {Object} data данные для создания запроса
 * @param {Object} auth данные для авторизации
 * @return {Promise}
 */
function createRequest(data, auth) {
    let params = {
        path: '/rest/stateful/corp/client_request',
        method: 'POST',
    };
    params = Object.assign(params, auth);

    return new Promise((resolve, reject) => {
        getBranch(auth, data.branchCode).then(branch => {
            params.postData = {
                requestBranchId: branch.id,
                place: branch.code,
                typeId: data.typeId,
                paramValues: [
                    {
                        key: 'period',
                        value: data.period,
                    },
                    {
                        key: 'tariff_plan',
                        value: data.tariff_plan,
                    },
                    {
                        key: 'operation',
                        value: data.operation,
                    },
                ],
            };
            requestToISimple(params)
                .then(res => {
                    resolve(res);
                })
                .catch(rej => {
                    reject(rej);
                });
        });
    });
}

/**
 * Создание сущности запроса
 * @param {string} requestId id запроса в iSimple
 * @param {Object} data данные для создания сущности
 * @param {Object} auth данные для авторизации
 * @return {Promise}
 */
function createRequestEntity(requestId, data, auth) {
    let params = {
        path: `/rest/stateful/corp/client_request/${requestId}/entity`,
        method: 'POST',
    };
    params = Object.assign(params, auth);
    const paramValues = Object.keys(data.values).map(key => {
        return {
            key,
            value: data.values[key],
        };
    });

    params.postData = {
        typeId: data.entityId,
        attributeId: data.id,
        paramValues,
    };

    return new Promise((resolve, reject) => {
        requestToISimple(params)
            .then(res => {
                console.log(res.result);
                resolve(res);
            })
            .catch(rej => {
                reject(rej);
            });
    });
}

/* eslint-disable */
exports.register = function(server, options, next) {
    server.route([
        {
            method: 'GET',
            path: '/api/v1/tariff/get_product_list',
            handler(request, reply) {
                let params = {
                    path: '/rest/stateful/corp/client_request/types',
                    method: 'GET',
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then(res => {
                        const requestTypes = JSON.parse(res.result);
                        const tariffPackagesRequests = requestTypes.requestTypeGroupJson.filter(
                            group => group.caption === 'Заявки на тарифные пакеты'
                        )[0];
                        const tariffPlans = Array.isArray(tariffPackagesRequests.requestTypes)
                            ? tariffPackagesRequests.requestTypes
                            : [tariffPackagesRequests.requestTypes];

                        const productsByTariff = tariffPlans.map(tariff => {
                            const tariffInfo = {
                                caption: tariff.caption,
                                code: tariff.code,
                                id: tariff.id,
                            };

                            tariff.paramGroups = Array.isArray(tariff.paramGroups)
                                ? tariff.paramGroups
                                : [tariff.paramGroups];
                            const productList = tariff.paramGroups
                                .filter(group => group.parameters.typeCode === 'REFENTITY_UL')
                                .map(group => {
                                    const entityId = group.parameters.properties.filter(
                                        prop => prop.key === 'ENTITY_TYPE'
                                    )[0].value;

                                    const groupInfo = {
                                        id: group.parameters.id,
                                        caption: group.parameters.caption,
                                        code: group.parameters.attributeName,
                                        entityId,
                                    };

                                    return groupInfo;
                                });

                            tariffInfo.productList = productList;
                            return tariffInfo;
                        });

                        reply(JSON.stringify(productsByTariff))
                            .code(res.statusCode)
                            .type('application/json');
                    })
                    .catch(rej => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/tariff/create_request',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;
                createRequest(payload, request.auth).then(res => {
                    const result = JSON.parse(res.result);
                    const requestId = result.id;

                    // Создаем сущности
                    if (payload.products && payload.products.length > 0) {
                        const entityPromises = payload.products.map(product => {
                            return createRequestEntity(requestId, product, request.auth);
                        });
                        Promise.all(entityPromises)
                            .then(values => {
                                reply(res.result)
                                    .code(res.statusCode)
                                    .type('application/json');
                            })
                            .catch(err => {
                                console.log(err);
                                reply(JSON.stringify(err))
                                    .code(res.statusCode)
                                    .type('application/json');
                            });
                    } else {
                        reply(res.result)
                            .code(res.statusCode)
                            .type('application/json');
                    }
                });
            },
        },
        {
            method: 'GET',
            path: '/api/v1/tariff/client_request/{id}',
            handler(request, reply) {
                let params = {
                    path: `/rest/stateful/corp/client_request/${request.params.id}`,
                    method: 'GET',
                };
                params = Object.assign(params, request.auth);
                requestToISimple(params)
                    .then(res => {
                        reply(res.result)
                            .code(res.statusCode)
                            .type('application/json');
                    })
                    .catch(rej => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'GET',
            path: '/api/v1/tariff/client_request/list',
            handler(request, reply) {
                const payload = JSON.parse(request.payload);

                let params = {
                    path: '/rest/stateful/corp/rosevro/document/fordates_filter',
                    method: 'POST',
                    postData: {
                        docModule: 'ibankul_req',
                        docType: 'client_request',
                        filter: `typeCode like '%TP_%'`,
                        includeClarify: true,
                    },
                };
                params = Object.assign(params, request.auth);
                requestToISimple(params)
                    .then(res => {
                        reply(res.result)
                            .code(res.statusCode)
                            .type('application/json');
                    })
                    .catch(rej => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/tariff/send2bank',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;
                let params = {
                    path: `/rest/stateful/corp/document/send?doc_module=${payload.docModule}&doc_type=${
                        payload.docType
                    }&doc_ids=${payload.docId}`,
                    method: 'PUT',
                };
                params = Object.assign(params, request.auth);

                requestToISimple(params)
                    .then(res => {
                        reply(res.result)
                            .code(res.statusCode)
                            .type('application/json');
                    })
                    .catch(rej => {
                        reply(rej.toString());
                    });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/tariff/attachment',
            config: {
                payload: {
                    output: 'stream',
                    parse: true,
                    maxBytes: '20971520', // 20 MB
                },
            },
            handler(request, reply) {
                const data = [];
                request.payload.file.on('data', chunk => {
                    data.push(chunk);
                });
                request.payload.file.on('end', () => {
                    const buf = Buffer.concat(data);
                    let params = {
                        path: '/rest/stateful/corp/attachment',
                        method: 'POST',
                        postData: {
                            id: request.payload.id,
                            file: {
                                value: buf,
                                options: {
                                    filename: request.payload.file.hapi.filename,
                                    contentType: request.payload.file.hapi.headers['content-type'],
                                },
                            },
                        },
                        multipart: true,
                    };
                    params = Object.assign(params, request.auth);
                    requestToISimple(params)
                        .then(res => {
                            reply(res.result)
                                .code(res.statusCode)
                                .type('application/json');
                        })
                        .catch(rej => {
                            reply(rej.toString());
                        });
                });
            },
        },
        {
            method: 'POST',
            path: '/api/v1/tariff/change_from_default_tarif',
            handler(request, reply) {
                const payload = typeof request.payload === 'string' ? JSON.parse(request.payload) : request.payload;

                const { org } = payload;

                const emailList =
                    NODE_ENV === 'development' || NODE_ENV === 'test'
                        ? [
                              'm.chirkin@roseurobank.ru',
                              'v.tamazov@roseurobank.ru',
                              'a.ivanov@roseurobank.ru',
                              'm.timchenko@roseurobank.ru',
                          ]
                        : ['m.chirkin@roseurobank.ru', 'v.tamazov@roseurobank.ru', 'web_rko@roseurobank.ru'];

                const msgBody = `
                    Клиент <b>${org.fullName}</b> ИНН <b>${
                    org.INN
                }</b> изъявил желание сменить текущий Тарифный план Стандартный для малого бизнеса на ТП с абонентской платой.
                     Необходимо связаться с клиентом и оформить смену тарифного плана.
                `;

                const mailOptions = {
                    from: 'Интернет-банк для Бизнеса <mb@rosevrobank.ru>', // sender address
                    to: emailList.join(', '), // list of receivers
                    subject: 'Смена тарифного плана', // Subject line
                    html: msgBody, // html body
                };

                if (NODE_ENV !== 'production') {
                    mailOptions.from = `Интернет-банк для Бизнеса[${NODE_ENV}] <mb@rosevrobank.ru>`;
                }

                smtpTransport.sendMail(mailOptions, error => {
                    if (error) {
                        reply('{ "error": "opps..." }')
                            .code(200)
                            .type('application/json');
                    } else {
                        reply('{}')
                            .code(200)
                            .type('application/json');
                    }

                    smtpTransport.close();
                });
            },
        },
        {
            method: 'GET',
            path: '/api/v1/tariff/list_cms',
            handler(request, reply) {
                fetch(`${apiUrl}/api/tariffs/list`)
                    .then(response => response.json())
                    .then(json => {
                        reply(json)
                            .code(200)
                            .type('application/json');
                    })
                    .catch(err => {
                        console.log(err);
                        reply('{ "success": false }')
                            .code(200)
                            .type('application/json');
                    });
            },
        },
    ]);

    next();
};

exports.register.attributes = {
    name: 'tariff_requests',
};
