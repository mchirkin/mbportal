const ip = require('ip');

const HMR = process.env.HMR || false;
const HOST = ip.address();

exports.register = (server, options, next) => {
    const routes = [
        {
            method: 'GET',
            path: '/img/{p*}',
            handler: {
                directory: {
                    path: `${__dirname}/../../public/img`,
                },
            },
        },
        {
            method: 'GET',
            path: '/server/img/{p*}',
            handler: {
                directory: {
                    lookupCompressed: true,
                    path: `${__dirname}/../templates/img`,
                },
            },
        },
        {
            method: 'GET',
            path: '/fonts/{p*}',
            handler: {
                directory: {
                    path: `${__dirname}/../../public/fonts`,
                },
            },
        },
        {
            method: 'GET',
            path: '/favicon.ico',
            handler: {
                file: {
                    path: './favicon.ico',
                    confine: false,
                },
            },
        },
    ];

    if (HMR) {
        routes.push({
            method: 'GET',
            path: '/public/{p*}',
            handler(request, reply) {
                const query = Object.keys(request.query).map(q => `${q}=${request.query[q]}`).join('&');

                const uri = encodeURI(`http://${HOST}:5000/${request.params.p}?${query}`);

                return reply.proxy({
                    uri,
                    passThrough: true,
                    xforward: true,
                });
            },
        });
    } else {
        routes.push({
            method: 'GET',
            path: '/public/{p*}',
            handler: {
                directory: {
                    lookupCompressed: true,
                    path: `${__dirname}/../../public`,
                },
            },
        });
    }

    server.route(routes);

    next();
};

exports.register.attributes = {
    name: 'static files',
};
