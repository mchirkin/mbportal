const crypto = require('crypto');

/**
 * Возвращает MD5-hash строки
 * @param {string} value - исходная строка
 * @return {string}
 */
function md5(value) {
    return crypto.createHash('md5').update(value).digest('hex');
}

/**
 * Возвращает SHA256-hash строки
 * @param {string} value - исходная строка
 * @return {string}
 */
function sha256(value) {
    return crypto.createHash('sha256').update(value).digest('hex');
}

/**
 * Функция для генерации клиентской соли
 * @return {string} - md5 hash
 */
function generateCNonce() {
    const secretPhrase = 'somesecretphrase';
    const result = secretPhrase + (new Date()).getTime();
    return md5(result);
}

/**
 * Генерация параметра A1 для Digest Auth
 * @param {Array} params - параметры для авторизации
 * @return {string}
 */
function encodeA1(params) {
    let _passwordAndSalt = params.password;

    if (params.salt && params.salt.length > 0) {
        const _base64Salt = Buffer.from(params.salt, 'base64');
        _passwordAndSalt = `${params.password}{${_base64Salt}}`;
    }

    const hashalg = params.hashalg || params.hashAlg;
    let _passwordAndSaltEnc = '';
    if (hashalg === 'sha256') {
        _passwordAndSaltEnc = sha256(_passwordAndSalt);
    } else {
        _passwordAndSaltEnc = md5(_passwordAndSalt);
    }

    const A1 = `${params.username}:${params.realm}:${_passwordAndSaltEnc}`;

    return md5(A1);
}

/**
 * Генерация параметра response для Digest Auth
 * @param {Object} params - параметры для авторизации
 * @return {string} параметр response для Digest Auth
 */
function generateResponse(params) {
    const _ha1 = encodeA1(params);
    const _path = params.path.replace(/\?.*$/, '');
    const _a2 = `${params.method}:${_path}`;
    const _ha2 = md5(_a2);

    return md5(`${_ha1}:${params.nonce}:${params.nc}:${params.cnonce}:${params.qop}:${_ha2}`);
}

/**
 * Генерация заголовка Authorization для Digest авторизации в системе iSimple
 * @param {Object} params - параметры для авторизации
 * @return {string} залоголовок авторизации
 */
function generateAuthorizationHeader(params) {
    params.cnonce = generateCNonce();
    const response = generateResponse(params);
    const _path = params.path.replace(/\?.*$/, '');
    const authorizationHeader = `SP Digest username="${params.username}", realm="${params.realm}", ` +
                              `nonce="${params.nonce}", uri="${_path}", response="${response}", ` +
                              `qop="${params.qop}", nc="${params.nc}", cnonce="${params.cnonce}"`;
    return authorizationHeader;
}

module.exports = {
    generateAuthorizationHeader,
};
