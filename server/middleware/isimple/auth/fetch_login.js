const fetch = require('isomorphic-fetch');
const CONFIG = require('../../../config/config');

/**
 * Вызов метода авторизации через fetch.
 * При использовании request/request.js наблюдается странный баг:
 * при совпадении имени пользователя и учетной записи в isimple возвращается идентификатор сессии,
 * с которым ничего не работает
 * Magic
 */
/* eslint-disable */
module.exports = function(params) {
    const basicAuth = Buffer.from(`${params.username}:${params.password}`).toString('base64');

    return new Promise((resolve, reject) => {
        fetch(`http://${CONFIG.isimple.host}:${CONFIG.isimple.port}/rest/stateful/corp/login`, {
            credentials: 'include',
            method: 'POST',
            headers: {
                Authorization: `Basic ${basicAuth}`,
                'Content-type': 'application/json',
                'x-forwarded-for': params.ClientIP,
                'User-Agent': params.userAgent,
            },
            body: '{}',
        }).then((response) => {
            console.log('[fetch auth]', response);
            let body = '';
            if (response.status === 401) {
                if (response.headers.get('error-code') && response.headers.get('error-code') === '1005') {
                    body = JSON.stringify({
                        error: 'user blocked',
                        errorCode: '1005',
                    });
                } else {
                    body = JSON.stringify({
                        sessionExpired: true,
                    });
                }
            }
            resolve({
                statusCode: response.status,
                headers: {
                    'set-cookie': response.headers.get('set-cookie'),
                    passwordexpired: response.headers.get('passwordexpired'),
                    'error-code': response.headers.get('error-code'),
                    'www-authenticate': response.headers.get('www-authenticate'),
                },
                result: body,
            });
        }).catch((err) => {
            reject(err);
        });
    });
}
