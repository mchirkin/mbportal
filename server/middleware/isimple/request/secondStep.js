const http = require('http');
const request = require('request');
const CONFIG = require('./../../../config/config');

/**
 * Второй шаг авторизации в iSimple
 * Отправляем запрос, указывая в заголовке Authorization необходимые данные для Digest авторизации
 * @param {Object} params - параметры для выполнения запроса
 * @param {string} params.path - адрес rest-сервиса в iSimple
 * @param {string} params.method - метод HTTP-запроса
 * @param {string} params.authorizationHeader - строка с данными для авторизации
 * @param {Object} params.postData - данные для передачи методом POST
 * @return {Promise}
 */
function secondStep(params) {
    const options = {
        hostname: CONFIG.isimple.host,
        port: CONFIG.isimple.port,
        path: params.path,
        method: params.method.toUpperCase(),
        headers: {
            Authorization: params.authorizationHeader,
            'Content-type': 'application/json',
        },
    };

    if (params.multipart) {
        return new Promise((resolve, reject) => {
            request.post({
                url: `http://${options.hostname}:${options.port}${options.path}`,
                headers: {
                    Authorization: options.headers.Authorization,
                },
                formData: params.postData,
            }, (err, res, body) => {
                if (err) {
                    reject(err);
                    return console.error('upload failed:', err);
                }
                resolve({
                    statusCode: res.statusCode,
                    headers: res.headers,
                    result: body,
                });
            });
        });
    }

    return new Promise((resolve, reject) => {
        const req = http.request(options, (res) => {
            if (!params.binary) {
                res.setEncoding('utf8');
            }
            let result = '';
            const data = [];
            res.on('data', (chunk) => {
                if (params.binary) {
                    data.push(chunk);
                } else {
                    result += chunk;
                }
            });
            res.on('end', () => {
                let buf;
                if (params.binary) {
                    buf = Buffer.concat(data);
                }
                let responseResult = params.binary ? buf : result;
                responseResult = params.inBase64 ? buf.toString('base64') : result;
                resolve({
                    statusCode: res.statusCode,
                    headers: res.headers,
                    result: responseResult,
                });
            });
        });

        req.on('error', (e) => {
            console.log(`problem with request: ${e.message}`);
            reject(e.message);
        });

        // write data to request body
        if (params.method.toUpperCase() === 'POST' && params.postData) {
            req.write(JSON.stringify(params.postData));
            console.log('POST DATA', JSON.stringify(params.postData));
        }
        req.end();
    });
}

module.exports = secondStep;
