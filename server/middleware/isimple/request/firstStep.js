const http = require('http');
const CONFIG = require('./../../../config/config');

/**
 * Первый шаг для авторизации в iSimple
 * Необходим для получения данных для Digest авторизации
 * (передаются в заголовках)
 * @param {Object} params - объект, содержащий параметры для запроса
 * @param {string} params.path - адрес rest-сервис в iSimple
 * @param {string} params.username - логин пользователя в системе iSimple
 * @return {Promise} resolve - параметры для авторизации
 */
function firstStep(params) {
    const config = {
        hostname: CONFIG.isimple.host,
        port: CONFIG.isimple.port,
        path: params.path,
        headers: {
            Authorization: `SP Digest username="${params.username}"`,
        },
    };

    return new Promise((resolve, reject) => {
        http.get(config, (response) => {
            const authHeaders = response.headers['www-authenticate'];
            const matches = authHeaders.match(/([^ \t\r\n]+)=("[^,\r\n]+")/g);
            const authParams = {};
            matches.forEach((param) => {
                const arr = param.replace(/"/g, '').replace(/={1,1}/, '||').split('||');
                authParams[arr[0]] = arr[1];
            });
            resolve(authParams);
        }).on('error', (e) => {
            console.log(`Got error: ${e.message}`);
            reject(e.message);
        });
    });
}

module.exports = firstStep;
