const requestToISimple = require('./request');

module.exports = (auth) => {
    let params = {
        path: '/rest/stateful/corp/client_request',
        method: 'POST',
        postData: {},
    };
    params = Object.assign(params, auth);

    return requestToISimple(params)
        .then((res) => {
            // TODO: разобраться до релиза
            return {
                id: 4,
                code: 'Москва',
            };
            const json = JSON.parse(res.result);
            if (!Array.isArray(json.requestCityWithBranchesJson)) {
                return {
                    id: json.requestCityWithBranchesJson.branches.id,
                    code: json.requestCityWithBranchesJson.code,
                };
            }
            return {
                id: json.requestCityWithBranchesJson[0].branches[0].id,
                code: json.requestCityWithBranchesJson[0].code,
            };
        })
        .catch(rej => rej);
};
