const firstStep = require('./firstStep');
const secondStep = require('./secondStep');
const digestAuth = require('./../auth/digestAuth');
/**
 * Осуществляет запрос в iSimple,
 * используя Digest авторизацию
 *
 * @param {Object} params - параметры запроса
 * @param {string} params.path - адрес rest сервиса в iSimple
 * @param {string} params.method - метод HTTP-запроса
 * @param {Object} params.postData - данные для передачи методом POST
 * @return {Promise}
 */
function requestDigest(params) {
    return new Promise((resolve, reject) => {
        firstStep(params)
            .then((authParams) => {
                params.nc = '00000001';
                params.authorizationHeader = digestAuth.generateAuthorizationHeader(Object.assign(params, authParams));
                return params;
            })
            .then(res => secondStep(res))
            .then(res => resolve(res))
            .catch((rej) => {
                console.log(rej);
                reject(rej);
            });
    });
}

module.exports = requestDigest;
