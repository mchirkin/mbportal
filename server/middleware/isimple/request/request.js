const request = require('request');
const mime = require('mime-types');
const path = require('path');
const CONFIG = require('./../../../config/config');

/**
 * Запрос в iSimple
 * @param {Object} params - параметры запроса
 * @param {string} params.path - адрес rest сервиса в iSimple
 * @param {string} params.method - метод HTTP-запроса
 * @param {Object} params.postData - данные для передачи методом POST
 * @param {string} params.ClientIP - ip клиента
 * @param {string} params.JSESSIONID - идентификатор сессии клиента
 * @param {string} params.userAgent - userAgent клиента
 * @param {boolean} params.binary - возвращать бинарные данные
 * @param {boolean} params.inBase64 - возвращать данные в Base64
 * @return {Promise}
 */
function requestToISimple(params) {
    return new Promise((resolve, reject) => {
        const options = {
            hostname: CONFIG.isimple.host,
            port: CONFIG.isimple.port,
            path: params.path,
            method: params.method.toUpperCase(),
            headers: {
                'Content-type': 'application/json',
                'x-forwarded-for': params.ClientIP,
                'User-Agent': params.userAgent,
            },
        };

        if (/corp\/login/.test(params.path)) {
            const basicAuth = Buffer.from(`${params.username}:${params.password}`).toString('base64');
            options.headers.Authorization = `Basic ${basicAuth}`;
        } else {
            options.headers.Cookie = `JSESSIONID=${params.JSESSIONID};`;
        }

        const requestParams = {
            url: `http://${options.hostname}:${options.port}${options.path}`,
            method: options.method,
            headers: options.headers,
            encoding: params.binary || params.inBase64 ? null : 'utf8',
        };

        if (params.multipart) {
            requestParams.formData = params.postData;
            /** Проверяем, что тип файла совпадает с Content-type и не попадает в black-list */
            const { filename, contentType } = params.postData.file.options;
            const fileExt = path.extname(filename).substr(1).toLowerCase();
            const extList = mime.extensions[contentType];
            // для сертификатов добавляем расширение cer
            if (contentType === 'application/x-x509-ca-cert') {
                extList.push('cer');
            }
            // black-list расширений файлов
            const restrictionExt = ['exe', 'bat', 'com', 'html', 'xml', 'xsl', 'xsd', 'rng', 'shtml'];

            if (!extList.includes(fileExt) || restrictionExt.includes(fileExt)) {
                return resolve({
                    statusCode: 400,
                    headers: {
                        'content-type': 'application/json',
                    },
                    result: JSON.stringify({
                        error: 'Bad request',
                        errorCode: 400,
                    }),
                });
            }

            requestParams.headers['Content-type'] = 'multipart/form-data';
        } else {
            requestParams.body = JSON.stringify(params.postData);
        }

        request(requestParams, (err, res, body) => {
            if (err) {
                return reject(err);
            }
            if (
                res.statusCode === 401 &&
                !/\/security\/authentication\/additional\/notification/.test(options.path)
            ) {
                if (res.headers['error-code'] && res.headers['error-code'] === '1005') {
                    body = JSON.stringify({
                        error: 'user blocked',
                        errorCode: '1005',
                    });
                } else {
                    body = JSON.stringify({
                        sessionExpired: true,
                    });
                }
            }

            resolve({
                statusCode: res.statusCode,
                headers: res.headers,
                result: params.inBase64 ? body.toString('base64') : body,
            });
        });
    });
}

module.exports = requestToISimple;
