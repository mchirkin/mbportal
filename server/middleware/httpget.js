const http = require('http');
const CONFIG = require('./../config/config');

function httpGet(url) {
    const config = {
        hostname: CONFIG.isimple.host,
        port: CONFIG.isimple.port,
        path: encodeURI(url),
    };

    return new Promise((resolve) => {
        http.get(config, (response) => {
            let result = '';
            response.on('data', (chunk) => {
                result += chunk;
            });
            response.on('end', () => {
                resolve({
                    statusCode: response.statusCode,
                    headers: response.headers,
                    result,
                });
            });
        });
    });
}

module.exports = httpGet;
