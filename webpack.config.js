const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CompressionPlugin = require("compression-webpack-plugin");

const NODE_ENV = process.env.NODE_ENV || 'development';

const plugins = [
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.DefinePlugin({
        NODE_ENV: JSON.stringify(NODE_ENV),
        'process.env': {
            NODE_ENV: JSON.stringify(NODE_ENV),
        }
    }),
    new ExtractTextPlugin({
        filename:  (getPath) => {
            return getPath('/css/[name].css').replace('css/js', 'css');
        },
        // filename: '/css/styles.css',
        allChunks: true,
    }),
    new CompressionPlugin({
        asset: "[path].gz[query]",
        algorithm: "gzip",
        test: /\.js$/,
        threshold: 10240,
        minRatio: 0.8
    }),
];

function _path(p) {
    return path.join(__dirname, p);
}

if (NODE_ENV === 'production') {
    plugins.push(
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            }
        })
    );
}

module.exports = {
    entry: {
        build: [
            'babel-polyfill',
            path.resolve(__dirname, 'client', 'app.jsx'),
        ],
        invoice: path.resolve(__dirname, 'client', 'invoice.jsx'),
    },
    output: {
        path: path.resolve(__dirname, 'public'),
        filename: 'js/[name].js',
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'babel-loader',
                    }
                ],
            },
            {
                test: /\.css$/,
                exclude: /node_modules/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                modules: true,
                                importLoaders: 1,
                                localIdentName: '[name]__[local]___[hash:base64:5]',
                            }
                        },
                        {
                            loader: 'postcss-loader',
                        }
                    ],
                }),
            },
            {
                test: /\.css$/,
                include: /node_modules/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                        },
                    ],
                }),
            },
            {
                test: /\.(jpg|png|svg|gif)$/,
                use: [
                    {
                        loader: 'file-loader?name=/img/[name]-[hash].[ext]'
                    }
                ],
            },
            {
                test: /\.(ttf|eot|woff)$/,
                use: [
                    {
                        loader: 'file-loader?name=/fonts/[name]-[hash].[ext]'
                    }
                ],
            }
        ]
    },
    watch: NODE_ENV === 'development',
    devtool: NODE_ENV === 'development' ? 'eval' : false,
    plugins: plugins,
    resolve: {
        extensions: ['.js', '.jsx'],
        modules: [
            path.join(__dirname, 'client'),
            'node_modules'
        ],
    },
    externals: {
        'react/addons': true,
        'react/lib/ExecutionEnvironment': true,
        'react/lib/ReactContext': true
    }
};
