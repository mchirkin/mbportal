const path = require('path');

module.exports = {
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    {
                        loader: 'style-loader',
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            modules: true,
                            importLoaders: 1,
                            localIdentName: 'stotybook_[name]__[local]___[hash:base64:5]',
                        }
                    },
                    {
                        loader: 'postcss-loader',
                    }
                ],
            },
            {
                test: /\.(jpg|png|svg|gif)$/,
                use: [
                    {
                        loader: 'file-loader?name=img/[name]-[hash].[ext]',
                    }
                ],
            },
            {
                test: /\.(ttf|eot|woff)$/,
                use: [
                    {
                        loader: 'file-loader?name=fonts/[name]-[hash].[ext]',
                    }
                ],
            },
        ],
    },
    resolve: {
        extensions: ['.js', '.jsx'],
        modules: [
            path.join('..', 'client'),
            'node_modules',
        ],
    },
}
