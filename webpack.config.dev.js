'use strict';

const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const DiskPlugin = require('webpack-disk-plugin');

const NODE_ENV = process.env.NODE_ENV || 'development';

const plugins = [
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.DefinePlugin({
        NODE_ENV: JSON.stringify(NODE_ENV),
        'process.env': {
            'NODE_ENV': JSON.stringify(NODE_ENV)
        }
    }),
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new DiskPlugin({ output: { path: 'public' } }),
];

function _path(p) {
    return path.join(__dirname, p);
}

const PORT = process.env.PORT || 5000;

module.exports = {
    entry: [
        'babel-polyfill',
        'react-hot-loader/patch',
        path.resolve(__dirname, 'client', 'app.jsx'),
    ],
    output: {
        path: path.resolve(__dirname, 'public'),
        filename: 'js/build.js',
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'babel-loader',
                    },
                ],
            },
            {
                test: /\.css$/,
                exclude: /node_modules/,
                use: [
                    'style-loader',
                    {
                        loader: 'css-loader',
                        options: {
                            modules: true,
                            importLoaders: 1,
                            localIdentName: '[name]__[local]___[hash:base64:5]',
                        },
                    },
                    {
                        loader: 'postcss-loader',
                    },
                ],
            },
            {
                test: /\.css$/,
                include: /node_modules/,
                use: [
                    'style-loader',
                    'css-loader',
                ],
            },
            {
                test: /\.(jpg|png|svg|gif)$/,
                use: [
                    {
                        loader: 'file-loader?name=/img/[name]-[hash].[ext]'
                    }
                ],
            },
            {
                test: /\.(ttf|eot|woff)$/,
                use: [
                    {
                        loader: 'file-loader?name=/fonts/[name]-[hash].[ext]'
                    }
                ],
            }
        ]
    },
    watch: true,
    devtool: 'eval',
    devServer: {
        contentBase: path.join(__dirname, 'public'),
        //publicPath: path.join(__dirname, 'public'),
        host: require('ip').address(),
        port: PORT,
        hot: true,
        headers: {
            'Access-Control-Allow-Origin': '*',
        },
    },
    plugins: plugins,
    resolve: {
        extensions: ['.js', '.jsx'],
        modules: [
            path.join(__dirname, 'client'),
            'node_modules',
        ],
    },
    externals: {
        'react/addons': true,
        'react/lib/ExecutionEnvironment': true,
        'react/lib/ReactContext': true,
    }
};
