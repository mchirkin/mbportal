const fs = require('fs');
const path = require('path');
const crypto = require('crypto');

/** Генерация пароля для cookie */
const cookiePassword = crypto.createHash('md5').update(`${new Date().toLocaleString()}6RNX2m3d`).digest('hex');

/** Генерация пароля для CSRF токена */
const csrfPassword = crypto.createHash('md5').update(`${new Date().toLocaleString()}uLsF63wM`).digest('hex');

const env = {
    cookiePassword,
    csrfPassword,
};

fs.writeFileSync(
    path.resolve(__dirname, 'server', 'env.json'),
    JSON.stringify(env, null, 2)
);
