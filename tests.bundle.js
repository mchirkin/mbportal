const context = require.context('./client', true, /.+\.spec\.jsx?$/);
context.keys().forEach(context);

const Enzyme = require('enzyme');
const Adapter = require('enzyme-adapter-react-15');

Enzyme.configure({ adapter: new Adapter() });

module.exports = context;
